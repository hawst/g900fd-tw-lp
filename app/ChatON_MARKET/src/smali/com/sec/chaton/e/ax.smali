.class public interface abstract Lcom/sec/chaton/e/ax;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;

.field public static final h:Ljava/lang/String;

.field public static final i:Ljava/lang/String;

.field public static final j:Ljava/lang/String;

.field public static final k:Ljava/lang/String;

.field public static final l:Ljava/lang/String;

.field public static final m:Ljava/lang/String;

.field public static final n:Ljava/lang/String;

.field public static final o:Ljava/lang/String;

.field public static final p:Ljava/lang/String;

.field public static final q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1643
    const/16 v0, 0x222

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "a.*,"

    aput-object v1, v0, v5

    const-string v1, "CASE WHEN "

    aput-object v1, v0, v6

    const-string v1, "b."

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, " FROM ("

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "cnt"

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "buddy_msg_send"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, "buddy_msg_received"

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, " FROM ("

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, "ifnull"

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, "9999"

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "ifnull"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "\'Not assigned\'"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, "buddy_msg_send"

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, "buddy_msg_received"

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0xea

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xec

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0xed

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xee

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xef

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    const-string v2, " a"

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xff

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x100

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x101

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x102

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x103

    const-string v2, "buddy_group"

    aput-object v2, v0, v1

    const/16 v1, 0x104

    const-string v2, " c"

    aput-object v2, v0, v1

    const/16 v1, 0x105

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x106

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x107

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x108

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x109

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    const-string v2, "9999"

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x110

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x111

    const-string v2, "\'Not assigned\'"

    aput-object v2, v0, v1

    const/16 v1, 0x112

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x113

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x114

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x115

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x116

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x117

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x118

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x119

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x120

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x121

    const-string v2, "buddy_msg_send"

    aput-object v2, v0, v1

    const/16 v1, 0x122

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x123

    const-string v2, "buddy_msg_received"

    aput-object v2, v0, v1

    const/16 v1, 0x124

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x125

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x126

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x127

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x128

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x129

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12b

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x12c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12d

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x12e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12f

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x130

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x131

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x132

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x133

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x134

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x135

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x136

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x137

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x138

    const-string v2, " IN (SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x139

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x13a

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x13b

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0x13c

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x13d

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x13e

    const-string v2, " = 1"

    aput-object v2, v0, v1

    const/16 v1, 0x13f

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x140

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x141

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x142

    const-string v2, " NOT IN (SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x143

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x144

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x145

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0x146

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x147

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x148

    const-string v2, " != 1"

    aput-object v2, v0, v1

    const/16 v1, 0x149

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x14a

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x14b

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x14c

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x14d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x14e

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x14f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x150

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x151

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x152

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x153

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x154

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x155

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x156

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x157

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x158

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x159

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x15a

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x15b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x15c

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x15d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x15e

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x15f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x160

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x161

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x162

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x163

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x164

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x165

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x166

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x167

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x168

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x169

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x16a

    const-string v2, "buddy_msg_send"

    aput-object v2, v0, v1

    const/16 v1, 0x16b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x16c

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x16d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x16e

    const-string v2, "buddy_msg_received"

    aput-object v2, v0, v1

    const/16 v1, 0x16f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x170

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0x171

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x172

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x173

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x174

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x175

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x176

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x177

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x178

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x179

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x17a

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x17b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17c

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x17d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x17e

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x17f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x180

    const-string v2, "\'Y\'"

    aput-object v2, v0, v1

    const/16 v1, 0x181

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x182

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x183

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x184

    sget-object v2, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NOT_CHANGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v2}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x185

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x186

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x187

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x188

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0x189

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x18a

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x18b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18c

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0x18d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x18e

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x18f

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x190

    const-string v2, "buddy_group"

    aput-object v2, v0, v1

    const/16 v1, 0x191

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x192

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x193

    const-string v2, " NOT IN ("

    aput-object v2, v0, v1

    const/16 v1, 0x194

    const-string v2, "SELECT"

    aput-object v2, v0, v1

    const/16 v1, 0x195

    const-string v2, " DISTINCT "

    aput-object v2, v0, v1

    const/16 v1, 0x196

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x197

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x198

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0x199

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x19a

    const-string v2, ") d"

    aput-object v2, v0, v1

    const/16 v1, 0x19b

    const-string v2, " LEFT OUTER JOIN ("

    aput-object v2, v0, v1

    const/16 v1, 0x19c

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x19d

    const-string v2, "h."

    aput-object v2, v0, v1

    const/16 v1, 0x19e

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x19f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a0

    const-string v2, "h."

    aput-object v2, v0, v1

    const/16 v1, 0x1a1

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1a2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a3

    const-string v2, "count("

    aput-object v2, v0, v1

    const/16 v1, 0x1a4

    const-string v2, "h."

    aput-object v2, v0, v1

    const/16 v1, 0x1a5

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1a6

    const-string v2, ") AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1a7

    const-string v2, "cnt"

    aput-object v2, v0, v1

    const/16 v1, 0x1a8

    const-string v2, " FROM ("

    aput-object v2, v0, v1

    const/16 v1, 0x1a9

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x1aa

    const-string v2, "ifnull"

    aput-object v2, v0, v1

    const/16 v1, 0x1ab

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x1ac

    const-string v2, "f."

    aput-object v2, v0, v1

    const/16 v1, 0x1ad

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1ae

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1af

    const-string v2, "9999"

    aput-object v2, v0, v1

    const/16 v1, 0x1b0

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x1b1

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1b2

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1b3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1b4

    const-string v2, "ifnull"

    aput-object v2, v0, v1

    const/16 v1, 0x1b5

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x1b6

    const-string v2, "g."

    aput-object v2, v0, v1

    const/16 v1, 0x1b7

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1b8

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1b9

    const-string v2, "3"

    aput-object v2, v0, v1

    const/16 v1, 0x1ba

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x1bb

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1bc

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1bd

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1be

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x1bf

    const-string v2, " e"

    aput-object v2, v0, v1

    const/16 v1, 0x1c0

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x1c1

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0x1c2

    const-string v2, " f"

    aput-object v2, v0, v1

    const/16 v1, 0x1c3

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x1c4

    const-string v2, "e."

    aput-object v2, v0, v1

    const/16 v1, 0x1c5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x1c6

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x1c7

    const-string v2, "f."

    aput-object v2, v0, v1

    const/16 v1, 0x1c8

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x1c9

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x1ca

    const-string v2, "buddy_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1cb

    const-string v2, " g"

    aput-object v2, v0, v1

    const/16 v1, 0x1cc

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x1cd

    const-string v2, "f."

    aput-object v2, v0, v1

    const/16 v1, 0x1ce

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1cf

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x1d0

    const-string v2, "g."

    aput-object v2, v0, v1

    const/16 v1, 0x1d1

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1d2

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x1d3

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x1d4

    const-string v2, "9999"

    aput-object v2, v0, v1

    const/16 v1, 0x1d5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1d6

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1d7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1d8

    const-string v2, "3"

    aput-object v2, v0, v1

    const/16 v1, 0x1d9

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1da

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1db

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1dc

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x1dd

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x1de

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x1df

    const-string v2, " IN (SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x1e0

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x1e1

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1e2

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0x1e3

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x1e4

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1e5

    const-string v2, " = 1"

    aput-object v2, v0, v1

    const/16 v1, 0x1e6

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x1e7

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x1e8

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x1e9

    const-string v2, " NOT IN (SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x1ea

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x1eb

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1ec

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0x1ed

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x1ee

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1ef

    const-string v2, " != 1"

    aput-object v2, v0, v1

    const/16 v1, 0x1f0

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x1f1

    const-string v2, ") h"

    aput-object v2, v0, v1

    const/16 v1, 0x1f2

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x1f3

    const-string v2, "h."

    aput-object v2, v0, v1

    const/16 v1, 0x1f4

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1f5

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x1f6

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x1f7

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1f8

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1f9

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1fa

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1fb

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1fc

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1fd

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x1fe

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1ff

    const-string v2, "cnt"

    aput-object v2, v0, v1

    const/16 v1, 0x200

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x201

    const-string v2, "buddy_group"

    aput-object v2, v0, v1

    const/16 v1, 0x202

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x203

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x204

    const-string v2, " NOT IN ("

    aput-object v2, v0, v1

    const/16 v1, 0x205

    const-string v2, "SELECT"

    aput-object v2, v0, v1

    const/16 v1, 0x206

    const-string v2, " DISTINCT "

    aput-object v2, v0, v1

    const/16 v1, 0x207

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x208

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x209

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0x20a

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x20b

    const-string v2, ") i"

    aput-object v2, v0, v1

    const/16 v1, 0x20c

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x20d

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0x20e

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x20f

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x210

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x211

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x212

    const-string v2, ") a"

    aput-object v2, v0, v1

    const/16 v1, 0x213

    const-string v2, " LEFT OUTER JOIN ("

    aput-object v2, v0, v1

    const/16 v1, 0x214

    const-string v2, "SELECT * FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x215

    const-string v2, "relation"

    aput-object v2, v0, v1

    const/16 v1, 0x216

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x217

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0x218

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x219

    const-string v2, "DATE(\'now\',\'localtime\',\'0 day\')"

    aput-object v2, v0, v1

    const/16 v1, 0x21a

    const-string v2, ") b"

    aput-object v2, v0, v1

    const/16 v1, 0x21b

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x21c

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x21d

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x21e

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x21f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x220

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0x221

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->a:Ljava/lang/String;

    .line 1677
    const/16 v0, 0x105

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "a.*"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "CASE WHEN "

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "1 AS "

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "\'"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "Favorites"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "\' AS "

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "1 AS "

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, " FROM ( SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, " = 1 ) a"

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, " JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string v2, "2"

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0xea

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xec

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xed

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0xee

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xef

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    const-string v2, ") a"

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    const-string v2, "(SELECT * FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    const-string v2, "relation"

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    const-string v2, "date(\'now\',\'localtime\',\'0 day\')) b"

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xff

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x100

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x101

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x102

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x103

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0x104

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->b:Ljava/lang/String;

    .line 1817
    const/16 v0, 0x1b0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "a.*"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "CASE WHEN "

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "buddyno_list"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, "b.buddygroup_list"

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "buddyno_list"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, "buddyname_list"

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, "buddy_hanzitopinyin"

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "group_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "b.buddyextras_list AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "null AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, "null AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, "null AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string v2, "buddy_group"

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string v2, " a"

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string v2, " LEFT OUTER JOIN"

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, "group_concat("

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, ", \'%%\' || "

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, " || \'%%\') AS buddyno_list"

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, "group_concat("

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, ", \'%%\' || "

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, " || \'%%\') AS buddyname_list"

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    const-string v2, "( SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    const-string v2, ") AS buddygroup_list"

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    const-string v2, "group_concat("

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    const-string v2, ", \'%%\' || "

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xea

    const-string v2, " || \'%%\') AS buddyextras_list"

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xec

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xed

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xee

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xef

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    const-string v2, "TRIM("

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    const-string v2, ") AS "

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    const-string v2, " a,"

    aput-object v2, v0, v1

    const/16 v1, 0xff

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x100

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0x101

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x102

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x103

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x104

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x105

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x106

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x107

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x108

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x109

    const-string v2, " != 1"

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    const-string v2, ") b"

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x110

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x111

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x112

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x113

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x114

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x115

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x116

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x117

    const-string v2, " != 1"

    aput-object v2, v0, v1

    const/16 v1, 0x118

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x119

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    const-string v2, "1 AS "

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    const-string v2, "\'"

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    const-string v2, "Favorites"

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    const-string v2, "\' AS "

    aput-object v2, v0, v1

    const/16 v1, 0x120

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x121

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x122

    const-string v2, "1 AS "

    aput-object v2, v0, v1

    const/16 v1, 0x123

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x124

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x125

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x126

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x127

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x128

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x129

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x12a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x12c

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x12d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x12f

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x130

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x131

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x132

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x133

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x134

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x135

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x136

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x137

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x138

    const-string v2, "buddy_hanzitopinyin"

    aput-object v2, v0, v1

    const/16 v1, 0x139

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x13a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x13b

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x13c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x13d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x13e

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x13f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x140

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x141

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x142

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x143

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x144

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x145

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x146

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x147

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x148

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x149

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x14a

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x14b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x14c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x14d

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x14e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x14f

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x150

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x151

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0x152

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x153

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0x154

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x155

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x156

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x157

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0x158

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x159

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x15a

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x15b

    const-string v2, " FROM ( SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x15c

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x15d

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x15e

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0x15f

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x160

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x161

    const-string v2, " = 1 ) a"

    aput-object v2, v0, v1

    const/16 v1, 0x162

    const-string v2, " JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x163

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x164

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0x165

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x166

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x167

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x168

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x169

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x16a

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x16b

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x16c

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x16d

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x16e

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x16f

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x170

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x171

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x172

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x173

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x174

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x175

    const-string v2, "2"

    aput-object v2, v0, v1

    const/16 v1, 0x176

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x177

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x178

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x179

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x17a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17b

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x17c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17d

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x17e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17f

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x180

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x181

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x182

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x183

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x184

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x185

    const-string v2, "buddy_hanzitopinyin"

    aput-object v2, v0, v1

    const/16 v1, 0x186

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x187

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x188

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x189

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x18a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18b

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x18c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18d

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x18e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18f

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x190

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x191

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x192

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x193

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x194

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x195

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x196

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x197

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0x198

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x199

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0x19a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x19b

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x19c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x19d

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0x19e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x19f

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x1a0

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1a1

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x1a2

    const-string v2, ") a"

    aput-object v2, v0, v1

    const/16 v1, 0x1a3

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x1a4

    const-string v2, "( SELECT * FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1a5

    const-string v2, "relation"

    aput-object v2, v0, v1

    const/16 v1, 0x1a6

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x1a7

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0x1a8

    const-string v2, " = \'1\' ) b "

    aput-object v2, v0, v1

    const/16 v1, 0x1a9

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x1aa

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x1ab

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x1ac

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x1ad

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x1ae

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1af

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->c:Ljava/lang/String;

    .line 1925
    const/16 v0, 0x121

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "a.*"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "CASE WHEN "

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "2"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, " a"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, " SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "2"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xea

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xec

    sget-object v2, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NOT_CHANGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v2}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xed

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xee

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0xef

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    const-string v2, "\'unknown_buddy\' AS "

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xff

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x100

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x101

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0x102

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x103

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0x104

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x105

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x106

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x107

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x108

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x109

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    const-string v2, " NOT IN ("

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x110

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x111

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x112

    const-string v2, ") a"

    aput-object v2, v0, v1

    const/16 v1, 0x113

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x114

    const-string v2, "(SELECT * FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x115

    const-string v2, "relation"

    aput-object v2, v0, v1

    const/16 v1, 0x116

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x117

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0x118

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x119

    const-string v2, "date(\'now\',\'localtime\',\'0 day\')) b"

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0x120

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->d:Ljava/lang/String;

    .line 1965
    const/16 v0, 0x121

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "a.*"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "CASE WHEN "

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "2"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, " a"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, " SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "2"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, "userid"

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "username"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "userid"

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, "username"

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    const-string v2, "userid"

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xea

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xec

    sget-object v2, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NOT_CHANGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v2}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xed

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xee

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0xef

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    const-string v2, "\'unknown_buddy\' AS "

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xff

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x100

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x101

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0x102

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x103

    const-string v2, "\'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0x104

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x105

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x106

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x107

    const-string v2, "calllog"

    aput-object v2, v0, v1

    const/16 v1, 0x108

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x109

    const-string v2, "userid"

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    const-string v2, " NOT IN ("

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x110

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x111

    const-string v2, "userid"

    aput-object v2, v0, v1

    const/16 v1, 0x112

    const-string v2, ") a"

    aput-object v2, v0, v1

    const/16 v1, 0x113

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x114

    const-string v2, "(SELECT * FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x115

    const-string v2, "relation"

    aput-object v2, v0, v1

    const/16 v1, 0x116

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x117

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0x118

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x119

    const-string v2, "date(\'now\',\'localtime\',\'0 day\')) b"

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0x120

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->e:Ljava/lang/String;

    .line 2009
    const/16 v0, 0x213

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "a.*"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "CASE WHEN "

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "1 AS "

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "\'"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "Favorites"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "\' AS "

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "1 AS "

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "buddy_msisdns"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "buddy_phonenumber_external_use"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, " FROM ( SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, " = 1 ) a"

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, " JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, "( SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xea

    const-string v2, "buddy_msisdns"

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xec

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xed

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xee

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0xef

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    const-string v2, "buddy_phonenumber_external_use"

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    const-string v2, " UNION ALL SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    const-string v2, "msgstatus"

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xff

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x100

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x101

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x102

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x103

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x104

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x105

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x106

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x107

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x108

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x109

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x110

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x111

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x112

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x113

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x114

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x115

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x116

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x117

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x118

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x119

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x120

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x121

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x122

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x123

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x124

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x125

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x126

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x127

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x128

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x129

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12a

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x12b

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x12c

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0x12d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12e

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x12f

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0x130

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x131

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x132

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x133

    const-string v2, "buddy_msisdns"

    aput-object v2, v0, v1

    const/16 v1, 0x134

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x135

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x136

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x137

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x138

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x139

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x13a

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x13b

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0x13c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x13d

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x13e

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x13f

    const-string v2, "buddy_phonenumber_external_use"

    aput-object v2, v0, v1

    const/16 v1, 0x140

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x141

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x142

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x143

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x144

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x145

    const-string v2, "specialbuddy"

    aput-object v2, v0, v1

    const/16 v1, 0x146

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x147

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0x148

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x149

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x14a

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x14b

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x14c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x14d

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x14e

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x14f

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x150

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x151

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x152

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x153

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x154

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x155

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x156

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/16 v1, 0x157

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x158

    const-string v2, "2"

    aput-object v2, v0, v1

    const/16 v1, 0x159

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x15a

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x15b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x15c

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x15d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x15e

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x15f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x160

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x161

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x162

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x163

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x164

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x165

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x166

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x167

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x168

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x169

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x16a

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x16b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x16c

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x16d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x16e

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x16f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x170

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x171

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x172

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x173

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x174

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x175

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x176

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x177

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x178

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0x179

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17a

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0x17b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17c

    const-string v2, "buddy_msisdns"

    aput-object v2, v0, v1

    const/16 v1, 0x17d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17e

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x17f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x180

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0x181

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x182

    const-string v2, "buddy_phonenumber_external_use"

    aput-object v2, v0, v1

    const/16 v1, 0x183

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x184

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x185

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x186

    const-string v2, "( SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x187

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x188

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x189

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x18a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18b

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x18c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18d

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x18e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18f

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x190

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x191

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x192

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x193

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x194

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x195

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x196

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x197

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x198

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x199

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x19a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x19b

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x19c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x19d

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x19e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x19f

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x1a0

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a1

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x1a2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a3

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0x1a4

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a5

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0x1a6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a7

    const-string v2, "buddy_msisdns"

    aput-object v2, v0, v1

    const/16 v1, 0x1a8

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a9

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1aa

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1ab

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0x1ac

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1ad

    const-string v2, "buddy_phonenumber_external_use"

    aput-object v2, v0, v1

    const/16 v1, 0x1ae

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1af

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x1b0

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1b1

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x1b2

    const-string v2, " UNION ALL SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x1b3

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x1b4

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1b5

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1b6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1b7

    const-string v2, "msgstatus"

    aput-object v2, v0, v1

    const/16 v1, 0x1b8

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1b9

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x1ba

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1bb

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1bc

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1bd

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x1be

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1bf

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1c0

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1c1

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x1c2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1c3

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1c4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1c5

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x1c6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1c7

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1c8

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1c9

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1ca

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1cb

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1cc

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1cd

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x1ce

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1cf

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1d0

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1d1

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1d2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1d3

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1d4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1d5

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1d6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1d7

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1d8

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1d9

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x1da

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1db

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1dc

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1dd

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x1de

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1df

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1e0

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1e1

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x1e2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1e3

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1e4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1e5

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x1e6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1e7

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1e8

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1e9

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0x1ea

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1eb

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1ec

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1ed

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0x1ee

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1ef

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1f0

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1f1

    const-string v2, "buddy_msisdns"

    aput-object v2, v0, v1

    const/16 v1, 0x1f2

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1f3

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1f4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1f5

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1f6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1f7

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1f8

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1f9

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0x1fa

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1fb

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1fc

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1fd

    const-string v2, "buddy_phonenumber_external_use"

    aput-object v2, v0, v1

    const/16 v1, 0x1fe

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1ff

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x200

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x201

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x202

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x203

    const-string v2, "specialbuddy"

    aput-object v2, v0, v1

    const/16 v1, 0x204

    const-string v2, ")) a"

    aput-object v2, v0, v1

    const/16 v1, 0x205

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x206

    const-string v2, "(SELECT * FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x207

    const-string v2, "relation"

    aput-object v2, v0, v1

    const/16 v1, 0x208

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x209

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0x20a

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x20b

    const-string v2, "date(\'now\',\'localtime\',\'0 day\')) b"

    aput-object v2, v0, v1

    const/16 v1, 0x20c

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x20d

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x20e

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x20f

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x210

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x211

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0x212

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->f:Ljava/lang/String;

    .line 2096
    const/16 v0, 0x2c

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "buddy_no"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "buddy_name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, " NOT IN "

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "(SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->g:Ljava/lang/String;

    .line 2100
    const/16 v0, 0x1a3

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "b."

    aput-object v1, v0, v5

    const-string v1, "buddy_no"

    aput-object v1, v0, v6

    const-string v1, ","

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "buddy_msg_send"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "buddy_msg_received"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xad

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, "(SELECT * FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, ") a"

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, " JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, " ( SELECT * FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, "relation"

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, " = \'1\' ) c "

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string v2, "\'1\'"

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string v2, " OR "

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, " is null"

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, ") d"

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, "SELECT *"

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0xea

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xec

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0xed

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xee

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xef

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    const-string v2, "buddy_msg_send"

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    const-string v2, "buddy_msg_received"

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0xff

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x100

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x101

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x102

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x103

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x104

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x105

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x106

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x107

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x108

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x109

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x110

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0x111

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x112

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x113

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0x114

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x115

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x116

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x117

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x118

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x119

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x120

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x121

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x122

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x123

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x124

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x125

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x126

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x127

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x128

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x129

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x12a

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x12b

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x12c

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x12d

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x12e

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x12f

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x130

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x131

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x132

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x133

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x134

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x135

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x136

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x137

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x138

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x139

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x13a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x13b

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x13c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x13d

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x13e

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x13f

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x140

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x141

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x142

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x143

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x144

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x145

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x146

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x147

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x148

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x149

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x14a

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x14b

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x14c

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x14d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x14e

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x14f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x150

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x151

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x152

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x153

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x154

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x155

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x156

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x157

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x158

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x159

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x15a

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x15b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x15c

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x15d

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x15e

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x15f

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x160

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x161

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x162

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x163

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x164

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x165

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x166

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x167

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x168

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x169

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x16a

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x16b

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x16c

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x16d

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x16e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x16f

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x170

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x171

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x172

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x173

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x174

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x175

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x176

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x177

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x178

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x179

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x17a

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x17b

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x17c

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x17d

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x17e

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x17f

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x180

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x181

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x182

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x183

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x184

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x185

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x186

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x187

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x188

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x189

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x18a

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x18b

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x18c

    const-string v2, " a"

    aput-object v2, v0, v1

    const/16 v1, 0x18d

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x18e

    const-string v2, " ( SELECT * FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x18f

    const-string v2, "relation"

    aput-object v2, v0, v1

    const/16 v1, 0x190

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x191

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0x192

    const-string v2, " = \'1\' ) b "

    aput-object v2, v0, v1

    const/16 v1, 0x193

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x194

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x195

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x196

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x197

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x198

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0x199

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x19a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x19b

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0x19c

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x19d

    const-string v2, "\'1\'"

    aput-object v2, v0, v1

    const/16 v1, 0x19e

    const-string v2, " OR "

    aput-object v2, v0, v1

    const/16 v1, 0x19f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x1a0

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0x1a1

    const-string v2, " is null"

    aput-object v2, v0, v1

    const/16 v1, 0x1a2

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->h:Ljava/lang/String;

    .line 2217
    const/16 v0, 0xe2

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "a.*"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "CASE WHEN "

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "b."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " IS NULL THEN \'"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "N"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\' ELSE \'"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "\' END AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "is_favorite"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "buddy_orginal_numbers"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "buddy_msisdns"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "buddy_multidevice"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "buddy_is_profile_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "buddy_is_status_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "buddy_account_info"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "buddy_sainfo"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "CASE WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, " IS NULL THEN "

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, " WHEN "

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, "\' THEN "

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, " ELSE "

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, " END"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, "( SELECT * FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, "relation"

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, " =  \'1\' ) c"

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string v2, ") a"

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, ") b"

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->i:Ljava/lang/String;

    .line 2263
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "buddy_no"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "buddy_orginal_number"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " = ?"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->j:Ljava/lang/String;

    .line 2265
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "buddy_no"

    aput-object v1, v0, v5

    const-string v1, ","

    aput-object v1, v0, v6

    const-string v1, "buddy_orginal_number"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_extra_info"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->k:Ljava/lang/String;

    .line 2270
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SELECT "

    aput-object v1, v0, v3

    const-string v1, "count(*) AS "

    aput-object v1, v0, v4

    const-string v1, "count"

    aput-object v1, v0, v5

    const-string v1, " FROM "

    aput-object v1, v0, v6

    const-string v1, "buddy"

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->l:Ljava/lang/String;

    .line 2272
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "(SELECT "

    aput-object v1, v0, v3

    const-string v1, "count(*) AS "

    aput-object v1, v0, v4

    const-string v1, "count"

    aput-object v1, v0, v5

    const-string v1, " FROM "

    aput-object v1, v0, v6

    const-string v1, "buddy"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "=\'Y\')"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->m:Ljava/lang/String;

    .line 2274
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "(SELECT "

    aput-object v1, v0, v3

    const-string v1, "buddy_no"

    aput-object v1, v0, v4

    const-string v1, ","

    aput-object v1, v0, v5

    const-string v1, "buddy_name"

    aput-object v1, v0, v6

    const-string v1, ","

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "buddy_is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "=\'Y\'"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " UNION ALL SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "specialbuddy"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "is_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "=\'Y\');"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->n:Ljava/lang/String;

    .line 2277
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SELECT "

    aput-object v1, v0, v3

    const-string v1, "count(*) AS "

    aput-object v1, v0, v4

    const-string v1, "count"

    aput-object v1, v0, v5

    const-string v1, " FROM "

    aput-object v1, v0, v6

    const-string v1, "specialbuddy"

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->o:Ljava/lang/String;

    .line 2279
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "(SELECT "

    aput-object v1, v0, v3

    const-string v1, "g._id"

    aput-object v1, v0, v4

    const-string v1, ","

    aput-object v1, v0, v5

    const-string v1, "group_name"

    aput-object v1, v0, v6

    const-string v1, ","

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_group g left outer join grouprelation r on g._id = r.group_relation_group order by g._id)"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->p:Ljava/lang/String;

    .line 2284
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SELECT "

    aput-object v1, v0, v3

    const-string v1, "count(*) AS "

    aput-object v1, v0, v4

    const-string v1, "count"

    aput-object v1, v0, v5

    const-string v1, " FROM "

    aput-object v1, v0, v6

    const-string v1, "recommendee"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "=\'"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\' AND "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "> ?"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/ax;->q:Ljava/lang/String;

    return-void
.end method
