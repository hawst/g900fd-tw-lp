.class public interface abstract Lcom/sec/chaton/e/bc;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1617
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE INDEX "

    aput-object v1, v0, v3

    const-string v1, "msg_server_no_index"

    aput-object v1, v0, v4

    const-string v1, " ON "

    aput-object v1, v0, v5

    const-string v1, "message"

    aput-object v1, v0, v6

    const-string v1, " ("

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "message_sever_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ");"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bc;->a:Ljava/lang/String;

    .line 1619
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE INDEX "

    aput-object v1, v0, v3

    const-string v1, "msg_inbox_no_index_1"

    aput-object v1, v0, v4

    const-string v1, " ON "

    aput-object v1, v0, v5

    const-string v1, "message"

    aput-object v1, v0, v6

    const-string v1, " ("

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "message_is_truncated"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ");"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bc;->b:Ljava/lang/String;

    .line 1621
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE INDEX "

    aput-object v1, v0, v3

    const-string v1, "msg_inbox_no_index_2"

    aput-object v1, v0, v4

    const-string v1, " ON "

    aput-object v1, v0, v5

    const-string v1, "message"

    aput-object v1, v0, v6

    const-string v1, " ("

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "message_content_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ");"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bc;->c:Ljava/lang/String;

    .line 1623
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE INDEX "

    aput-object v1, v0, v3

    const-string v1, "participants_inbox_no_index"

    aput-object v1, v0, v4

    const-string v1, " ON "

    aput-object v1, v0, v5

    const-string v1, "participant"

    aput-object v1, v0, v6

    const-string v1, " ("

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ");"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bc;->d:Ljava/lang/String;

    return-void
.end method
