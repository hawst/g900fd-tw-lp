.class public final enum Lcom/sec/chaton/e/l;
.super Ljava/lang/Enum;
.source "ChatONContract.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/e/l;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/e/l;

.field public static final enum b:Lcom/sec/chaton/e/l;

.field public static final enum c:Lcom/sec/chaton/e/l;

.field private static final synthetic e:[Lcom/sec/chaton/e/l;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 120
    new-instance v0, Lcom/sec/chaton/e/l;

    const-string v1, "FAIL"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/chaton/e/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/l;->a:Lcom/sec/chaton/e/l;

    .line 121
    new-instance v0, Lcom/sec/chaton/e/l;

    const-string v1, "INSERT"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/chaton/e/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/l;->b:Lcom/sec/chaton/e/l;

    .line 122
    new-instance v0, Lcom/sec/chaton/e/l;

    const-string v1, "UPDATE"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/chaton/e/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/l;->c:Lcom/sec/chaton/e/l;

    .line 119
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/e/l;

    sget-object v1, Lcom/sec/chaton/e/l;->a:Lcom/sec/chaton/e/l;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/e/l;->b:Lcom/sec/chaton/e/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/e/l;->c:Lcom/sec/chaton/e/l;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/chaton/e/l;->e:[Lcom/sec/chaton/e/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 127
    iput p3, p0, Lcom/sec/chaton/e/l;->d:I

    .line 128
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/e/l;
    .locals 1

    .prologue
    .line 119
    const-class v0, Lcom/sec/chaton/e/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/l;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/e/l;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/sec/chaton/e/l;->e:[Lcom/sec/chaton/e/l;

    invoke-virtual {v0}, [Lcom/sec/chaton/e/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/e/l;

    return-object v0
.end method
