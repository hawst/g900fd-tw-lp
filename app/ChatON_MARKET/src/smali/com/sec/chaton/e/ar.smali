.class public final enum Lcom/sec/chaton/e/ar;
.super Ljava/lang/Enum;
.source "ChatONContract2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/e/ar;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/e/ar;

.field public static final enum b:Lcom/sec/chaton/e/ar;

.field public static final enum c:Lcom/sec/chaton/e/ar;

.field public static final enum d:Lcom/sec/chaton/e/ar;

.field public static final enum e:Lcom/sec/chaton/e/ar;

.field public static final enum f:Lcom/sec/chaton/e/ar;

.field public static final enum g:Lcom/sec/chaton/e/ar;

.field public static final enum h:Lcom/sec/chaton/e/ar;

.field private static final synthetic j:[Lcom/sec/chaton/e/ar;


# instance fields
.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 281
    new-instance v0, Lcom/sec/chaton/e/ar;

    const-string v1, "Default"

    const-string v2, "-1"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/e/ar;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/e/ar;->a:Lcom/sec/chaton/e/ar;

    .line 282
    new-instance v0, Lcom/sec/chaton/e/ar;

    const-string v1, "AmsBackground"

    const-string v2, "amsbackground"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/chaton/e/ar;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/e/ar;->b:Lcom/sec/chaton/e/ar;

    .line 283
    new-instance v0, Lcom/sec/chaton/e/ar;

    const-string v1, "AmsStamp"

    const-string v2, "amsstamp"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/chaton/e/ar;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/e/ar;->c:Lcom/sec/chaton/e/ar;

    .line 284
    new-instance v0, Lcom/sec/chaton/e/ar;

    const-string v1, "AmsTemplate"

    const-string v2, "amstemplate"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/chaton/e/ar;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/e/ar;->d:Lcom/sec/chaton/e/ar;

    .line 285
    new-instance v0, Lcom/sec/chaton/e/ar;

    const-string v1, "Anicon"

    const-string v2, "anicon_package"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/chaton/e/ar;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    .line 286
    new-instance v0, Lcom/sec/chaton/e/ar;

    const-string v1, "Skin"

    const/4 v2, 0x5

    const-string v3, "skin"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/ar;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    .line 287
    new-instance v0, Lcom/sec/chaton/e/ar;

    const-string v1, "Font"

    const/4 v2, 0x6

    const-string v3, "font"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/ar;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    .line 288
    new-instance v0, Lcom/sec/chaton/e/ar;

    const-string v1, "Sound"

    const/4 v2, 0x7

    const-string v3, "sound"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/ar;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    .line 280
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/chaton/e/ar;

    sget-object v1, Lcom/sec/chaton/e/ar;->a:Lcom/sec/chaton/e/ar;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/e/ar;->b:Lcom/sec/chaton/e/ar;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/e/ar;->c:Lcom/sec/chaton/e/ar;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/e/ar;->d:Lcom/sec/chaton/e/ar;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/e/ar;->j:[Lcom/sec/chaton/e/ar;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 292
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 293
    iput-object p3, p0, Lcom/sec/chaton/e/ar;->i:Ljava/lang/String;

    .line 294
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/e/ar;
    .locals 4

    .prologue
    .line 301
    sget-object v0, Lcom/sec/chaton/e/ar;->b:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    sget-object v0, Lcom/sec/chaton/e/ar;->b:Lcom/sec/chaton/e/ar;

    .line 314
    :goto_0
    return-object v0

    .line 303
    :cond_0
    sget-object v0, Lcom/sec/chaton/e/ar;->c:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 304
    sget-object v0, Lcom/sec/chaton/e/ar;->c:Lcom/sec/chaton/e/ar;

    goto :goto_0

    .line 305
    :cond_1
    sget-object v0, Lcom/sec/chaton/e/ar;->d:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 306
    sget-object v0, Lcom/sec/chaton/e/ar;->d:Lcom/sec/chaton/e/ar;

    goto :goto_0

    .line 307
    :cond_2
    sget-object v0, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 308
    sget-object v0, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    goto :goto_0

    .line 309
    :cond_3
    sget-object v0, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 310
    sget-object v0, Lcom/sec/chaton/e/ar;->g:Lcom/sec/chaton/e/ar;

    goto :goto_0

    .line 311
    :cond_4
    sget-object v0, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 312
    sget-object v0, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    goto :goto_0

    .line 313
    :cond_5
    sget-object v0, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    invoke-virtual {v0}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 314
    sget-object v0, Lcom/sec/chaton/e/ar;->h:Lcom/sec/chaton/e/ar;

    goto :goto_0

    .line 316
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "Unknown item type. "

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/e/ar;
    .locals 1

    .prologue
    .line 280
    const-class v0, Lcom/sec/chaton/e/ar;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/ar;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/e/ar;
    .locals 1

    .prologue
    .line 280
    sget-object v0, Lcom/sec/chaton/e/ar;->j:[Lcom/sec/chaton/e/ar;

    invoke-virtual {v0}, [Lcom/sec/chaton/e/ar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/e/ar;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/chaton/e/ar;->i:Ljava/lang/String;

    return-object v0
.end method
