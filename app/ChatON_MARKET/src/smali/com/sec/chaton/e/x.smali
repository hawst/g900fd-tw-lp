.class public final enum Lcom/sec/chaton/e/x;
.super Ljava/lang/Enum;
.source "ChatONContract.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/e/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/e/x;

.field public static final enum b:Lcom/sec/chaton/e/x;

.field public static final enum c:Lcom/sec/chaton/e/x;

.field public static final enum d:Lcom/sec/chaton/e/x;

.field private static final synthetic f:[Lcom/sec/chaton/e/x;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1091
    new-instance v0, Lcom/sec/chaton/e/x;

    const-string v1, "UNDEFINED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/chaton/e/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/x;->a:Lcom/sec/chaton/e/x;

    .line 1092
    new-instance v0, Lcom/sec/chaton/e/x;

    const-string v1, "UNREAD"

    invoke-direct {v0, v1, v4, v3}, Lcom/sec/chaton/e/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/x;->b:Lcom/sec/chaton/e/x;

    .line 1093
    new-instance v0, Lcom/sec/chaton/e/x;

    const-string v1, "READ_LOCAL"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/e/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/x;->c:Lcom/sec/chaton/e/x;

    .line 1094
    new-instance v0, Lcom/sec/chaton/e/x;

    const-string v1, "READ_SERVER"

    invoke-direct {v0, v1, v6, v5}, Lcom/sec/chaton/e/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/x;->d:Lcom/sec/chaton/e/x;

    .line 1090
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/e/x;

    sget-object v1, Lcom/sec/chaton/e/x;->a:Lcom/sec/chaton/e/x;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/e/x;->b:Lcom/sec/chaton/e/x;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/e/x;->c:Lcom/sec/chaton/e/x;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/e/x;->d:Lcom/sec/chaton/e/x;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/e/x;->f:[Lcom/sec/chaton/e/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1098
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1099
    iput p3, p0, Lcom/sec/chaton/e/x;->e:I

    .line 1100
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/e/x;
    .locals 1

    .prologue
    .line 1090
    const-class v0, Lcom/sec/chaton/e/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/x;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/e/x;
    .locals 1

    .prologue
    .line 1090
    sget-object v0, Lcom/sec/chaton/e/x;->f:[Lcom/sec/chaton/e/x;

    invoke-virtual {v0}, [Lcom/sec/chaton/e/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/e/x;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1103
    iget v0, p0, Lcom/sec/chaton/e/x;->e:I

    return v0
.end method
