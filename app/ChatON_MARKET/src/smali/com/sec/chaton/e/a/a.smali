.class public Lcom/sec/chaton/e/a/a;
.super Ljava/lang/Object;
.source "AmsItemDatabaseHelper.java"


# direct methods
.method public static a(Lcom/sec/chaton/io/entry/inner/AmsItem;)Landroid/content/ContentProviderOperation;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 34
    if-nez p0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return-object v0

    .line 37
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->amstype:Lcom/sec/chaton/d/e;

    if-eqz v1, :cond_0

    .line 40
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->amstype:Lcom/sec/chaton/d/e;

    invoke-virtual {v1}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/e/ar;->a:Lcom/sec/chaton/e/ar;

    if-eq v1, v2, :cond_0

    .line 44
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 46
    const-string v1, "item_id"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-string v1, "item_type"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->amstype:Lcom/sec/chaton/d/e;

    invoke-virtual {v2}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v1, "expiration_time"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->expirationdate:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 49
    const-string v1, "new"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->newitem:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 50
    const-string v1, "special"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->special:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 51
    const-string v1, "extras"

    invoke-static {p0}, Lcom/sec/chaton/e/a/a;->b(Lcom/sec/chaton/io/entry/inner/AmsItem;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->amstype:Lcom/sec/chaton/d/e;

    invoke-virtual {v1}, Lcom/sec/chaton/d/e;->b()Lcom/sec/chaton/e/ar;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/al;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/sec/chaton/io/entry/inner/AmsItem;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 66
    const-string v1, "thumbnailUrl"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->thumbnailurl:Ljava/lang/String;

    .line 67
    const-string v1, "filesize"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->filesize:Ljava/lang/Long;

    .line 68
    return-void
.end method

.method public static b(Lcom/sec/chaton/io/entry/inner/AmsItem;)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 78
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 80
    const-string v1, "thumbnailUrl"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->thumbnailurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 81
    const-string v1, "filesize"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/AmsItem;->filesize:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 83
    return-object v0
.end method
