.class public Lcom/sec/chaton/e/a/b;
.super Lcom/sec/chaton/e/a/j;
.source "AniconDatabaseHelper.java"


# direct methods
.method public static a(Lcom/sec/chaton/io/entry/inner/Anicon;)Landroid/content/ContentProviderOperation;
    .locals 3

    .prologue
    .line 113
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 114
    const-string v1, "anicon_id"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Anicon;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v1, "package_id"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Anicon;->packageid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v1, "zip_url"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Anicon;->aniconzipurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v1, "delegate_url"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Anicon;->delegateimage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v1, "cd_proxy_url"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Anicon;->cdproxyurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    sget-object v1, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-static {v1}, Lcom/sec/chaton/util/al;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/io/entry/inner/Package;)Landroid/content/ContentProviderOperation;
    .locals 3

    .prologue
    .line 58
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 59
    const-string v1, "item_type"

    sget-object v2, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v2}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "item_id"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/Package;->categoryid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    const-string v1, "reference_id"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->categoryid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    const-string v1, "name"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v1, "new"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->_new:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 68
    const-string v1, "special"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->special:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 69
    const-string v1, "extras"

    invoke-static {p0}, Lcom/sec/chaton/e/a/b;->b(Lcom/sec/chaton/io/entry/inner/Package;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v1, "expiration_time"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->expirationdate:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 71
    const-string v1, "down_rank"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->down_rank:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 72
    const-string v1, "data1"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->data1:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v1, "data2"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->data2:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v1, "data3"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->data3:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/al;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/e/a/c;
    .locals 4

    .prologue
    .line 131
    new-instance v0, Lcom/sec/chaton/e/a/c;

    invoke-direct {v0}, Lcom/sec/chaton/e/a/c;-><init>()V

    .line 133
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 135
    const-string v2, "preview"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/a/c;->a(Ljava/lang/String;)V

    .line 136
    const-string v2, "thumbnail"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/a/c;->b(Ljava/lang/String;)V

    .line 137
    const-string v2, "count"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/e/a/c;->a(I)V

    .line 138
    const-string v2, "volume"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/e/a/c;->a(J)V

    .line 139
    const-string v2, "panel"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/e/a/c;->c(Ljava/lang/String;)V

    .line 141
    return-object v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 168
    .line 171
    :try_start_0
    new-instance v0, Lcom/sec/chaton/io/entry/inner/Package;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/Package;-><init>()V

    .line 172
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 174
    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "SELECT * FROM "

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "anicon_package"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, " WHERE "

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "install"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, " != ?"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 176
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 178
    const-string v3, "category_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->categoryid:Ljava/lang/String;

    .line 179
    const-string v3, "package_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->id:Ljava/lang/String;

    .line 180
    const-string v3, "name"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->name:Ljava/lang/String;

    .line 181
    const-string v3, "preview_url"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->previewurl:Ljava/lang/String;

    .line 182
    const-string v3, "thumbnail_url"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->thumbnailurl:Ljava/lang/String;

    .line 183
    const-string v3, "anicon_count"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->aniconcount:Ljava/lang/Integer;

    .line 184
    const-string v3, "volume"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->volume:Ljava/lang/Long;

    .line 185
    const-string v3, "expiration_time"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->expirationdate:Ljava/lang/Long;

    .line 186
    const-string v3, "special"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->special:Ljava/lang/Integer;

    .line 190
    const-string v3, "new"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 191
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 192
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->_new:Ljava/lang/Integer;

    .line 199
    :goto_1
    const-string v3, ""

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->panelurl:Ljava/lang/String;

    .line 200
    const-string v3, "install"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 203
    const-string v5, "item_type"

    sget-object v6, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v6}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v5, "item_id"

    iget-object v6, v0, Lcom/sec/chaton/io/entry/inner/Package;->id:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v5, "name"

    iget-object v6, v0, Lcom/sec/chaton/io/entry/inner/Package;->name:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string v5, "special"

    iget-object v6, v0, Lcom/sec/chaton/io/entry/inner/Package;->special:Ljava/lang/Integer;

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 208
    const-string v5, "new"

    iget-object v6, v0, Lcom/sec/chaton/io/entry/inner/Package;->_new:Ljava/lang/Integer;

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209
    const-string v5, "install"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 210
    const-string v3, "expiration_time"

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/Package;->expirationdate:Ljava/lang/Long;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 211
    const-string v3, "extras"

    invoke-static {v0}, Lcom/sec/chaton/e/a/b;->b(Lcom/sec/chaton/io/entry/inner/Package;)Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->categoryid:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 214
    const-string v3, "reference_id"

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/Package;->categoryid:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_0
    const-string v3, "download_item"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 221
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 222
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 221
    :cond_1
    throw v0

    .line 195
    :cond_2
    const/4 v3, 0x0

    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Package;->_new:Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 221
    :cond_3
    if-eqz v1, :cond_4

    .line 222
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 225
    :cond_4
    return-void
.end method

.method public static b(Lcom/sec/chaton/io/entry/inner/Package;)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 152
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 154
    const-string v1, "preview"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->previewurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 155
    const-string v1, "thumbnail"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->thumbnailurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 156
    const-string v1, "count"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->aniconcount:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 157
    const-string v1, "volume"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->volume:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 158
    const-string v1, "panel"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Package;->panelurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 160
    return-object v0
.end method
