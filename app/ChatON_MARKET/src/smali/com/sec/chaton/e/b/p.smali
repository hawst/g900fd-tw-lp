.class public Lcom/sec/chaton/e/b/p;
.super Ljava/lang/Object;
.source "PushReceivedTask.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/e/b/o;

.field private b:Z

.field private c:Lcom/sec/chaton/io/entry/PushEntry;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:Lcom/sec/chaton/e/w;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:J


# direct methods
.method public constructor <init>(Lcom/sec/chaton/e/b/o;Ljava/lang/String;ZLcom/sec/chaton/io/entry/PushEntry;Ljava/lang/String;IILcom/sec/chaton/e/w;ZLjava/lang/String;ZJ)V
    .locals 0

    .prologue
    .line 450
    iput-object p1, p0, Lcom/sec/chaton/e/b/p;->a:Lcom/sec/chaton/e/b/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451
    iput-boolean p3, p0, Lcom/sec/chaton/e/b/p;->b:Z

    .line 452
    iput-object p4, p0, Lcom/sec/chaton/e/b/p;->c:Lcom/sec/chaton/io/entry/PushEntry;

    .line 453
    iput-object p2, p0, Lcom/sec/chaton/e/b/p;->d:Ljava/lang/String;

    .line 454
    iput-object p5, p0, Lcom/sec/chaton/e/b/p;->e:Ljava/lang/String;

    .line 455
    iput p6, p0, Lcom/sec/chaton/e/b/p;->f:I

    .line 456
    iput p7, p0, Lcom/sec/chaton/e/b/p;->g:I

    .line 457
    iput-object p8, p0, Lcom/sec/chaton/e/b/p;->h:Lcom/sec/chaton/e/w;

    .line 458
    iput-boolean p9, p0, Lcom/sec/chaton/e/b/p;->i:Z

    .line 459
    iput-object p10, p0, Lcom/sec/chaton/e/b/p;->j:Ljava/lang/String;

    .line 460
    iput-boolean p11, p0, Lcom/sec/chaton/e/b/p;->k:Z

    .line 461
    iput-wide p12, p0, Lcom/sec/chaton/e/b/p;->l:J

    .line 462
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/sec/chaton/e/b/p;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 469
    iget-boolean v0, p0, Lcom/sec/chaton/e/b/p;->b:Z

    return v0
.end method

.method public c()Lcom/sec/chaton/io/entry/PushEntry;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/sec/chaton/e/b/p;->c:Lcom/sec/chaton/io/entry/PushEntry;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/sec/chaton/e/b/p;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 485
    iget v0, p0, Lcom/sec/chaton/e/b/p;->g:I

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 493
    iget-boolean v0, p0, Lcom/sec/chaton/e/b/p;->i:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/sec/chaton/e/b/p;->j:Ljava/lang/String;

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 501
    iget-boolean v0, p0, Lcom/sec/chaton/e/b/p;->k:Z

    return v0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 505
    iget-wide v0, p0, Lcom/sec/chaton/e/b/p;->l:J

    return-wide v0
.end method
