.class public Lcom/sec/chaton/e/a/t;
.super Ljava/lang/Object;
.source "MoreAppsDatabaseHelper.java"


# direct methods
.method public static a()Landroid/content/ContentProviderOperation;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/chaton/e/av;->a:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/io/entry/inner/MoreAppList;)Landroid/content/ContentProviderOperation;
    .locals 3

    .prologue
    .line 23
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 25
    const-string v1, "id"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/MoreAppList;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const-string v1, "title"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/MoreAppList;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const-string v1, "priority"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/MoreAppList;->priority:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v1, "type"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/MoreAppList;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v1, "contenturl"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/MoreAppList;->contenturl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    const-string v1, "appid"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/MoreAppList;->appid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const-string v1, "linkurl"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/MoreAppList;->linkurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string v1, "samsungappsurl"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/MoreAppList;->samsungappsurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v1, "downloadurl"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/MoreAppList;->downloadurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    sget-object v1, Lcom/sec/chaton/e/av;->a:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method
