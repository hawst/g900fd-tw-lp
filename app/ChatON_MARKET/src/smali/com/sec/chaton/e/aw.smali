.class public Lcom/sec/chaton/e/aw;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DatabaseHelper.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/chaton/e/aw;


# instance fields
.field private c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    const-class v0, Lcom/sec/chaton/e/aw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/aw;->a:Ljava/lang/String;

    .line 191
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/e/aw;->b:Lcom/sec/chaton/e/aw;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 201
    const-string v0, "chaton.db"

    const/4 v1, 0x0

    const/16 v2, 0x55

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 202
    iput-object p1, p0, Lcom/sec/chaton/e/aw;->c:Landroid/content/Context;

    .line 203
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;
    .locals 2

    .prologue
    .line 214
    const-class v1, Lcom/sec/chaton/e/aw;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/e/aw;->b:Lcom/sec/chaton/e/aw;

    if-nez v0, :cond_0

    .line 215
    new-instance v0, Lcom/sec/chaton/e/aw;

    invoke-direct {v0, p0}, Lcom/sec/chaton/e/aw;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/chaton/e/aw;->b:Lcom/sec/chaton/e/aw;

    .line 217
    :cond_0
    sget-object v0, Lcom/sec/chaton/e/aw;->b:Lcom/sec/chaton/e/aw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    const-string v0, "chaton.db"

    return-object v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 264
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 265
    const-string v1, "group_name"

    const-string v2, "Favorites"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string v1, "group_type"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 267
    const-string v1, "buddy_group"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 268
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;IZ)V
    .locals 6

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    const/4 v5, 0x1

    .line 293
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 296
    if-ge p2, v0, :cond_0

    .line 299
    :try_start_0
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_is_profile_updated CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 300
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_is_status_updated CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 303
    :cond_0
    if-ge p2, v1, :cond_1

    .line 305
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_is_new CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 309
    :cond_1
    if-ge p2, v2, :cond_2

    .line 310
    const-string v0, "DROP TABLE IF EXISTS memo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 311
    const-string v0, "DROP TABLE IF EXISTS memo_sessions"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 312
    sget-object v0, Lcom/sec/chaton/e/bg;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 313
    sget-object v0, Lcom/sec/chaton/e/bg;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 318
    :cond_2
    const/4 v0, 0x5

    if-ge p2, v0, :cond_3

    .line 320
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_trunk_unread_count INTEGER NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 324
    :cond_3
    const/4 v0, 0x6

    if-ge p2, v0, :cond_4

    .line 326
    const-string v0, "ALTER TABLE relation ADD COLUMN relation_last_msg_time TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 330
    :cond_4
    const/4 v0, 0x7

    if-ge p2, v0, :cond_5

    .line 332
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_show_phone_number INTEGER DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 338
    :cond_5
    const/16 v0, 0x8

    if-ge p2, v0, :cond_6

    .line 339
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_valid CHAR(1) NOT NULL DEFAULT \'Y\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 342
    invoke-static {p1}, Lcom/sec/chaton/e/a/n;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 346
    :cond_6
    const/16 v0, 0x9

    if-ge p2, v0, :cond_7

    .line 347
    const-string v0, "ALTER TABLE buddy_group ADD COLUMN group_is_new CHAR(1) NOT NULL DEFAULT \'Y\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 348
    const-string v0, "UPDATE buddy_group SET group_is_new = \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 352
    :cond_7
    const/16 v0, 0xa

    if-ge p2, v0, :cond_8

    .line 353
    invoke-static {}, Lcom/sec/chaton/account/i;->a()I

    .line 358
    :cond_8
    const/16 v0, 0xb

    if-ge p2, v0, :cond_9

    .line 360
    invoke-static {p1}, Lcom/sec/chaton/e/a/q;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 365
    :cond_9
    const/16 v0, 0xc

    if-ge p2, v0, :cond_a

    .line 366
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_extra_info VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 371
    :cond_a
    const/16 v0, 0xd

    if-ge p2, v0, :cond_b

    .line 373
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_enable_noti CHAR(1) NOT NULL DEFAULT \'Y\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 379
    :cond_b
    const/16 v0, 0xe

    if-ge p2, v0, :cond_c

    .line 380
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ALTER TABLE inbox_buddy_relation ADD COLUMN chat_type INTEGER NOT NULL DEFAULT \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 381
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UPDATE inbox_buddy_relation SET chat_type = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 384
    invoke-direct {p0}, Lcom/sec/chaton/e/aw;->d()Ljava/lang/String;

    move-result-object v0

    .line 385
    if-eqz v0, :cond_c

    .line 387
    const-string v1, "country_name"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_c
    const/16 v0, 0xf

    if-ge p2, v0, :cond_d

    .line 396
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_last_timestamp NUMBER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 400
    :cond_d
    const/16 v0, 0x10

    if-ge p2, v0, :cond_e

    .line 401
    const-string v0, "DROP TABLE IF EXISTS recommendee"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 402
    sget-object v0, Lcom/sec/chaton/e/bg;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 408
    :cond_e
    const/16 v0, 0x11

    if-ge p2, v0, :cond_f

    .line 409
    sget-object v0, Lcom/sec/chaton/e/bg;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 410
    sget-object v0, Lcom/sec/chaton/e/bg;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 411
    sget-object v0, Lcom/sec/chaton/e/bg;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 417
    :cond_f
    const/16 v0, 0x12

    if-ge p2, v0, :cond_10

    .line 418
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "ALTER TABLE "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "anicon_package"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, " ADD COLUMN "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "special"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, " INTEGER NOT NULL DEFAULT(0)"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 423
    :cond_10
    const/16 v0, 0x13

    if-ge p2, v0, :cond_11

    .line 424
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ALTER TABLE message ADD COLUMN message_is_file_upload INTEGER NOT NULL DEFAULT("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 425
    invoke-static {p1}, Lcom/sec/chaton/e/a/q;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 430
    :cond_11
    const/16 v0, 0x14

    if-ge p2, v0, :cond_12

    .line 431
    const-string v0, "ALTER TABLE message ADD COLUMN message_is_truncated CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 437
    :cond_12
    const/16 v0, 0x15

    if-ge p2, v0, :cond_13

    .line 485
    :cond_13
    const/16 v0, 0x16

    if-ge p2, v0, :cond_14

    .line 500
    :cond_14
    const/16 v0, 0x17

    if-ge p2, v0, :cond_15

    .line 501
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_is_change_skin CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 503
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_background_style VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 505
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_send_bubble_style VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 507
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_receive_bubble_style VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 512
    :cond_15
    const/16 v0, 0x18

    if-ge p2, v0, :cond_16

    .line 519
    :cond_16
    const/16 v0, 0x19

    if-ge p2, v0, :cond_17

    .line 520
    const-string v0, "ALTER TABLE recommendee ADD COLUMN description TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 521
    const-string v0, "ALTER TABLE recommendee ADD COLUMN followcount VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 522
    const-string v0, "ALTER TABLE recommendee ADD COLUMN likecount VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 523
    const-string v0, "ALTER TABLE recommendee ADD COLUMN msgstatus VARCHAR(256)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 524
    const-string v0, "ALTER TABLE recommendee ADD COLUMN photoloaded VARCHAR(25)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 525
    const-string v0, "ALTER TABLE recommendee ADD COLUMN status TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 526
    const-string v0, "ALTER TABLE recommendee ADD COLUMN url VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 529
    const-string v0, "DROP TABLE IF EXISTS specialbuddy"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 530
    sget-object v0, Lcom/sec/chaton/e/bg;->D:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 536
    :cond_17
    const/16 v0, 0x1a

    if-ge p2, v0, :cond_18

    .line 537
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_is_entered CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 540
    :cond_18
    const/16 v0, 0x1b

    if-ge p2, v0, :cond_19

    .line 541
    const-string v0, "ALTER TABLE specialbuddy ADD COLUMN islike CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 545
    :cond_19
    const/16 v0, 0x1c

    if-ge p2, v0, :cond_1a

    .line 547
    const-string v0, "DROP TABLE IF EXISTS theme"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 550
    sget-object v0, Lcom/sec/chaton/e/bg;->C:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 552
    iget-object v0, p0, Lcom/sec/chaton/e/aw;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/cd;->b(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/Skin;

    .line 553
    const/16 v2, 0x21

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "INSERT INTO "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "skin"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "("

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "skin_id"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, ","

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "special"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, ","

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "thumbnail_url"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ","

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "bg_type"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ","

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "expiration_date"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ","

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "install"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ","

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "new"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ") VALUES ("

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/Skin;->id:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string v4, "\',"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/Skin;->special:Ljava/lang/Integer;

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string v4, ","

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/Skin;->thumbnailurl:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string v4, "\',"

    aput-object v4, v2, v3

    const/16 v3, 0x19

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/Skin;->bgtype:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    const-string v4, "\',"

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Skin;->expirationdate:Ljava/lang/Long;

    aput-object v0, v2, v3

    const/16 v0, 0x1d

    const-string v3, ","

    aput-object v3, v2, v0

    const/16 v0, 0x1e

    const-string v3, "1,"

    aput-object v3, v2, v0

    const/16 v0, 0x1f

    const-string v3, "0"

    aput-object v3, v2, v0

    const/16 v0, 0x20

    const-string v3, ")"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 1026
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1030
    const-string v1, "get_all_unread_message_timestamp"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    throw v0

    .line 559
    :cond_1a
    const/16 v0, 0x1d

    if-ge p2, v0, :cond_1b

    .line 560
    :try_start_1
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_is_name_updated CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 563
    :cond_1b
    const/16 v0, 0x1e

    if-ge p2, v0, :cond_1c

    .line 564
    const-string v0, "ALTER TABLE specialbuddy ADD COLUMN weburl VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 565
    const-string v0, "ALTER TABLE recommendee ADD COLUMN weburl VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 570
    :cond_1c
    const/16 v0, 0x1f

    if-ge p2, v0, :cond_1d

    .line 571
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_web_url TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 573
    :cond_1d
    const/16 v0, 0x20

    if-ge p2, v0, :cond_1e

    .line 574
    const-string v0, "ALTER TABLE specialbuddy ADD COLUMN isNew CHAR(1) DEFAULT \'Y\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 577
    :cond_1e
    const/16 v0, 0x21

    if-ge p2, v0, :cond_1f

    .line 578
    const-string v0, "DROP INDEX IF EXISTS msg_server_no_index"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 579
    const-string v0, "DROP INDEX IF EXISTS msg_inbox_no_index_1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 580
    const-string v0, "DROP INDEX IF EXISTS msg_inbox_no_index_2"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 581
    sget-object v0, Lcom/sec/chaton/e/bc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 582
    sget-object v0, Lcom/sec/chaton/e/bc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 586
    :cond_1f
    const/16 v0, 0x22

    if-ge p2, v0, :cond_20

    .line 593
    :cond_20
    const/16 v0, 0x23

    if-ge p2, v0, :cond_21

    .line 594
    sget-object v0, Lcom/sec/chaton/e/bg;->F:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 598
    :cond_21
    const/16 v0, 0x24

    if-ge p2, v0, :cond_22

    .line 600
    const-string v0, "DROP TABLE IF EXISTS memo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 601
    const-string v0, "DROP TABLE IF EXISTS memo_sessions"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 604
    sget-object v0, Lcom/sec/chaton/e/bg;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 605
    sget-object v0, Lcom/sec/chaton/e/bg;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609
    :cond_22
    const/16 v0, 0x25

    if-ge p2, v0, :cond_23

    .line 612
    :try_start_2
    invoke-static {p1}, Lcom/sec/chaton/e/a/b;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 615
    invoke-static {p1}, Lcom/sec/chaton/e/a/ab;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 617
    const-string v0, "DROP TABLE IF EXISTS anicon_package"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 618
    const-string v0, "DROP TABLE IF EXISTS skin"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 629
    :cond_23
    const/16 v0, 0x26

    if-ge p2, v0, :cond_24

    .line 630
    :try_start_3
    invoke-static {p1}, Lcom/sec/chaton/e/a/d;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 635
    :cond_24
    const/16 v0, 0x27

    if-ge p2, v0, :cond_25

    .line 636
    sget-object v0, Lcom/sec/chaton/e/bg;->G:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 638
    sget-object v0, Lcom/sec/chaton/e/bg;->H:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 640
    sget-object v0, Lcom/sec/chaton/e/bg;->I:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 642
    sget-object v0, Lcom/sec/chaton/e/bg;->J:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 644
    sget-object v0, Lcom/sec/chaton/e/bi;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 648
    :cond_25
    const/16 v0, 0x28

    if-ge p2, v0, :cond_26

    .line 650
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_updated_timestamp VARCHAR(80) NOT NULL DEFAULT \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 654
    :cond_26
    const/16 v0, 0x29

    if-ge p2, v0, :cond_27

    .line 655
    const-string v0, "ALTER TABLE download_item ADD COLUMN down_rank INTEGER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 656
    const-string v0, "ALTER TABLE download_item ADD COLUMN data1 TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 657
    const-string v0, "ALTER TABLE download_item ADD COLUMN data2 TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 658
    const-string v0, "ALTER TABLE download_item ADD COLUMN data3 TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 662
    :cond_27
    const/16 v0, 0x2a

    if-ge p2, v0, :cond_28

    .line 663
    const-string v0, "ALTER TABLE specialbuddy ADD COLUMN tel VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 665
    const-string v0, "ALTER TABLE recommendee ADD COLUMN tel VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 669
    :cond_28
    const/16 v0, 0x2b

    if-ge p2, v0, :cond_29

    .line 670
    const-string v0, "ALTER TABLE recommendee ADD COLUMN timestamp INTEGER DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 675
    :cond_29
    const/16 v0, 0x2c

    if-ge p2, v0, :cond_2a

    .line 676
    const-string v0, "ALTER TABLE poll ADD COLUMN poll_remain_time INTEGER DEFAULT(0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 681
    :cond_2a
    const/16 v0, 0x2d

    if-ge p2, v0, :cond_2b

    .line 682
    sget-object v0, Lcom/sec/chaton/e/bg;->L:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 689
    :cond_2b
    const/16 v0, 0x2e

    if-ge p2, v0, :cond_2c

    .line 690
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2c

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 691
    const-string v0, "chaton_id"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DB chatonid : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    const-string v0, "upgrade_multi_device_version"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 700
    :cond_2c
    const/16 v0, 0x2f

    if-ge p2, v0, :cond_2d

    .line 702
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_orginal_numbers TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 703
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_msisdns TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 704
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_multidevice CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 707
    :cond_2d
    const/16 v0, 0x30

    if-ge p2, v0, :cond_2e

    .line 708
    const-string v0, "ALTER TABLE inbox ADD COLUMN profile_url TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 709
    const-string v0, "UPDATE inbox SET profile_url = \'NA\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 712
    :cond_2e
    const/16 v0, 0x31

    if-ge p2, v0, :cond_2f

    .line 713
    const-string v0, "ALTER TABLE inbox ADD COLUMN lasst_session_merge_time NUMBER DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 718
    :cond_2f
    const/16 v0, 0x32

    if-ge p2, v0, :cond_30

    .line 719
    const-string v0, "CREATE TABLE message_notification (noti_inbox_no TEXT,noti_buddy_no TEXT,noti_message TEXT,noti_sent_time NUMBER,noti_message_id NUMBER,noti_chat_type INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 723
    :cond_30
    const/16 v0, 0x33

    if-ge p2, v0, :cond_31

    .line 724
    const-string v0, "ALTER TABLE message_notification ADD COLUMN noti_msg_type INTEGER DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 728
    :cond_31
    const/16 v0, 0x34

    if-ge p2, v0, :cond_32

    .line 729
    const-string v0, "ALTER TABLE participant ADD COLUMN participants_country_code TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 731
    const-string v0, "ALTER TABLE participant ADD COLUMN participants_is_auth CHAR(1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 733
    const-string v0, "ALTER TABLE participant ADD COLUMN participants_status INTEGER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 737
    :cond_32
    const/16 v0, 0x35

    if-ge p2, v0, :cond_33

    .line 738
    const-string v0, "ALTER TABLE recommendee ADD COLUMN chatonid TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 742
    :cond_33
    const/16 v0, 0x36

    if-ge p2, v0, :cond_34

    .line 743
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_old_session_id TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 745
    const-string v0, "ALTER TABLE participant ADD COLUMN participants_old_buddy_no TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 747
    const-string v0, "ALTER TABLE message ADD COLUMN message_old_session_id TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 748
    const-string v0, "ALTER TABLE message ADD COLUMN message_old_sender TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 753
    :cond_34
    const/16 v0, 0x37

    if-ge p2, v0, :cond_35

    .line 756
    const-string v0, "ALTER TABLE participant RENAME TO participant_OLD"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 757
    sget-object v0, Lcom/sec/chaton/e/bg;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 758
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INSERT INTO participant SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/bg;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participant"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_OLD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 759
    const-string v0, "DROP TABLE participant_OLD"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 762
    const-string v0, "DELETE FROM inbox WHERE inbox_title IS NULL AND inbox_last_chat_type=12"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 765
    sget-object v0, Lcom/sec/chaton/e/bg;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 769
    :cond_35
    const/16 v0, 0x38

    if-ge p2, v0, :cond_36

    .line 770
    sget-object v0, Lcom/sec/chaton/e/bc;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 774
    :cond_36
    const/16 v0, 0x39

    if-ge p2, v0, :cond_37

    .line 775
    const-string v0, "CREATE TABLE inbox_session_id_mapping (inbox_session_id TEXT NOT NULL,inbox_old_session_id TEXT NOT NULL,UNIQUE (inbox_old_session_id) ON CONFLICT REPLACE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 779
    :cond_37
    const/16 v0, 0x3a

    if-ge p2, v0, :cond_38

    .line 780
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_old_no TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 783
    :cond_38
    const/16 v0, 0x3b

    if-ge p2, v0, :cond_39

    .line 790
    :cond_39
    const/16 v0, 0x3c

    if-ge p2, v0, :cond_3a

    .line 791
    invoke-static {p1}, Lcom/sec/chaton/e/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 796
    :cond_3a
    const/16 v0, 0x3d

    if-ge p2, v0, :cond_3b

    .line 797
    invoke-direct {p0}, Lcom/sec/chaton/e/aw;->c()V

    .line 802
    :cond_3b
    const/16 v0, 0x3e

    if-ge p2, v0, :cond_3d

    .line 803
    invoke-static {}, Lcom/sec/chaton/util/am;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v5, :cond_3d

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3d

    .line 806
    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v0

    .line 807
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3c

    .line 808
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 811
    :cond_3c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update Regi ISO2 : null -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    const-string v1, "country_ISO"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    :cond_3d
    const/16 v0, 0x3f

    if-ge p2, v0, :cond_3e

    .line 818
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_sainfo TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 825
    :cond_3e
    const/16 v0, 0x40

    if-ge p2, v0, :cond_3f

    .line 826
    const-string v0, "ALTER TABLE download_item ADD COLUMN newly_installed INTEGER DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 830
    :cond_3f
    const/16 v0, 0x41

    if-ge p2, v0, :cond_42

    .line 831
    invoke-static {}, Lcom/sec/chaton/settings/downloads/az;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/az;

    .line 832
    const/16 v2, 0x33

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "INSERT INTO "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "download_item"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, " ( "

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "reference_id"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, ", "

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "item_id"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, ", "

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "item_type"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "name"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "install"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "expiration_time"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "new"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string v4, "special"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string v4, "extras"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string v4, "newly_installed"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string v4, " ) values ( "

    aput-object v4, v2, v3

    const/16 v3, 0x17

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x19

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/az;->m:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/az;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x20

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x21

    const-string v4, "\""

    aput-object v4, v2, v3

    const/16 v3, 0x22

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/az;->o:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x23

    const-string v4, "\""

    aput-object v4, v2, v3

    const/16 v3, 0x24

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x25

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x26

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x27

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-wide v4, 0x7fffffffffffffffL

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x28

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x29

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/az;->c:Ljava/lang/Integer;

    aput-object v4, v2, v3

    const/16 v3, 0x2c

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x2d

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x2e

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/az;->q:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x2f

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x30

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x31

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/16 v0, 0x32

    const-string v3, " ) "

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 865
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "execute. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/aw;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 619
    :catch_0
    move-exception v0

    .line 620
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_40

    .line 621
    sget-object v1, Lcom/sec/chaton/e/aw;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 624
    :cond_40
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 868
    :cond_41
    invoke-static {}, Lcom/sec/chaton/settings/downloads/az;->b()V

    .line 872
    :cond_42
    const/16 v0, 0x42

    if-ge p2, v0, :cond_43

    .line 873
    const-string v0, "ALTER TABLE message ADD COLUMN message_content_translated TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 874
    const-string v0, "ALTER TABLE message ADD COLUMN message_from_lang TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 875
    const-string v0, "ALTER TABLE message ADD COLUMN message_to_lang TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 876
    const-string v0, "ALTER TABLE message ADD COLUMN message_is_spoken CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 878
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_translate_my_language TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 879
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_translate_buddy_language TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 880
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_enable_translate TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 881
    const-string v0, "ALTER TABLE inbox ADD COLUMN translate_outgoing_message TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 886
    :cond_43
    const/16 v0, 0x43

    if-ge p2, v0, :cond_44

    .line 887
    const-string v0, "DROP TABLE IF EXISTS calllog"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 888
    const-string v0, "CREATE TABLE calllog (_id INTEGER PRIMARY KEY AUTOINCREMENT,calldate TEXT NOT NULL,duration INTEGER\tNOT NULL DEFAULT 0,calllogtype INTEGER\tNOT NULL DEFAULT 0,callmethod INTEGER NOT NULL DEFAULT 0,userno TEXT NOT NULL DEFAULT \'\',username TEXT,userid TEXT,phoneno TEXT,countrycode TEXT,groupcallkey INTEGER NOT NULL DEFAULT 0,rejectmsg TEXT,groupid INTEGER)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 893
    :cond_44
    const/16 v0, 0x44

    if-ge p2, v0, :cond_45

    .line 894
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_account_info INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 899
    :cond_45
    const/16 v0, 0x45

    if-ge p2, v0, :cond_46

    .line 900
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_last_tid TEXT DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 905
    :cond_46
    const/16 v0, 0x46

    if-ge p2, v0, :cond_47

    .line 906
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_original_name TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 910
    :cond_47
    const/16 v0, 0x47

    if-ge p2, v0, :cond_48

    .line 911
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->e()V

    .line 915
    :cond_48
    const/16 v0, 0x48

    if-ge p2, v0, :cond_49

    .line 916
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_using_contact_name CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 921
    :cond_49
    const/16 v0, 0x49

    if-ge p2, v0, :cond_4a

    .line 922
    const-string v0, "ALTER TABLE contacts ADD COLUMN conatct_normalized_number TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 923
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "buddy_delete_copied_contacts"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 928
    :cond_4a
    const/16 v0, 0x4a

    if-ge p2, v0, :cond_4b

    .line 929
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "update_disclaimer_status"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "RUN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4b

    .line 930
    const-string v0, "agree_disclaimer"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 937
    :cond_4b
    const/16 v0, 0x4b

    if-ge p2, v0, :cond_4c

    .line 938
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_is_hide CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 939
    const-string v0, "ALTER TABLE specialbuddy ADD COLUMN is_hide CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 940
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_hanzitopinyin VARCHAR(80) NOT NULL DEFAULT \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 943
    sget-object v0, Lcom/sec/chaton/e/bg;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 944
    sget-object v0, Lcom/sec/chaton/e/bg;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 945
    sget-object v0, Lcom/sec/chaton/e/bg;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 946
    sget-object v0, Lcom/sec/chaton/e/bg;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 947
    const-string v0, "DROP TABLE IF EXISTS poston"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 953
    :cond_4c
    const/16 v0, 0x4c

    if-ge p2, v0, :cond_4d

    .line 954
    sget-object v0, Lcom/sec/chaton/e/bg;->M:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 956
    const-string v0, "ALTER TABLE poston_comments ADD COLUMN commentid  VARCHAR(25) NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 961
    :cond_4d
    const/16 v0, 0x4d

    if-ge p2, v0, :cond_4e

    .line 962
    const-string v0, "ALTER TABLE specialbuddy ADD COLUMN usertype INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 963
    const-string v0, "ALTER TABLE recommendee ADD COLUMN usertype INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 965
    invoke-static {p1}, Lcom/sec/chaton/e/a/af;->a(Landroid/database/sqlite/SQLiteDatabase;)Z

    .line 970
    :cond_4e
    const/16 v0, 0x4e

    if-ge p2, v0, :cond_4f

    .line 971
    const-string v0, "ALTER TABLE recommendee ADD COLUMN rank INTEGER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 972
    const-string v0, "ALTER TABLE recommendee ADD COLUMN targetiso2 VARCHAR(25)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 977
    :cond_4f
    const/16 v0, 0x4f

    if-ge p2, v0, :cond_50

    .line 978
    const-string v0, "ALTER TABLE appmanage ADD COLUMN messageTypeFlag TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 982
    :cond_50
    const/16 v0, 0x50

    if-ge p2, v0, :cond_51

    .line 983
    sget-object v0, Lcom/sec/chaton/e/bg;->N:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 988
    :cond_51
    const/16 v0, 0x51

    if-ge p2, v0, :cond_52

    .line 989
    sget-object v0, Lcom/sec/chaton/e/bg;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 994
    :cond_52
    const/16 v0, 0x52

    if-ge p2, v0, :cond_53

    .line 995
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_phonenumber_external_use TEXT DEFAULT \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 996
    invoke-static {p1}, Lcom/sec/chaton/e/a/d;->c(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1000
    :cond_53
    const/16 v0, 0x53

    if-ge p2, v0, :cond_54

    .line 1002
    :try_start_4
    invoke-static {p1}, Lcom/sec/chaton/e/a/ab;->b(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1013
    :cond_54
    const/16 v0, 0x54

    if-ge p2, v0, :cond_55

    .line 1014
    :try_start_5
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_coverstory_meta_id TEXT DEFAULT \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1019
    :cond_55
    const/16 v0, 0x55

    if-ge p2, v0, :cond_56

    .line 1020
    const-string v0, "ALTER TABLE message ADD COLUMN message_is_read INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1021
    invoke-static {p1}, Lcom/sec/chaton/e/a/q;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1024
    :cond_56
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1026
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1030
    const-string v0, "get_all_unread_message_timestamp"

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1034
    return-void

    .line 1004
    :catch_1
    move-exception v0

    .line 1005
    :try_start_6
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_57

    .line 1006
    sget-object v1, Lcom/sec/chaton/e/aw;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1008
    :cond_57
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1204
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "ChatON"

    const-string v2, "com.sec.chaton"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1206
    const/4 v2, 0x0

    invoke-virtual {v1, v0, p2, v2}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 1208
    if-eqz p3, :cond_0

    .line 1209
    const-string v1, "com.android.contacts"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 1213
    :goto_0
    return-void

    .line 1211
    :cond_0
    const-string v1, "com.android.contacts"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static b()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3277
    const-string v0, "TEST"

    const-string v2, "copyDatabaseToSdcard start"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3280
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/e/aw;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 3284
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3291
    :goto_0
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "chatondb.db"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 3298
    :goto_1
    if-eqz v0, :cond_0

    if-nez v2, :cond_1

    .line 3333
    :cond_0
    :goto_2
    return-void

    .line 3285
    :catch_0
    move-exception v0

    .line 3287
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    .line 3292
    :catch_1
    move-exception v2

    .line 3294
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v2, v1

    goto :goto_1

    .line 3304
    :cond_1
    const/16 v1, 0x400

    new-array v1, v1, [B

    .line 3307
    :goto_3
    :try_start_2
    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_2

    .line 3308
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    .line 3310
    :catch_2
    move-exception v1

    .line 3312
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 3315
    :cond_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 3321
    :goto_4
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 3327
    :goto_5
    :try_start_5
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 3332
    :goto_6
    const-string v0, "TEST"

    const-string v1, "copyDatabaseToSdcard end"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 3316
    :catch_3
    move-exception v1

    .line 3318
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 3322
    :catch_4
    move-exception v1

    .line 3324
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 3328
    :catch_5
    move-exception v0

    .line 3330
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;IZ)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1041
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1048
    :try_start_0
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_sainfo TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1049
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_account_info INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1050
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_is_hide CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1051
    const-string v0, "ALTER TABLE buddy ADD COLUMN buddy_hanzitopinyin VARCHAR(80) NOT NULL DEFAULT \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1054
    const-string v0, "ALTER TABLE participant ADD COLUMN participants_country_code TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1055
    const-string v0, "ALTER TABLE participant ADD COLUMN participants_is_auth CHAR(1)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1056
    const-string v0, "ALTER TABLE participant ADD COLUMN participants_status INTEGER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1059
    const-string v0, "ALTER TABLE poston_comments ADD COLUMN commentid  VARCHAR(25) NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1062
    const-string v0, "ALTER TABLE recommendee ADD COLUMN usertype INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1063
    const-string v0, "ALTER TABLE recommendee ADD COLUMN rank INTEGER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1064
    const-string v0, "ALTER TABLE recommendee ADD COLUMN targetiso2 VARCHAR(25)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1067
    const-string v0, "ALTER TABLE specialbuddy ADD COLUMN is_hide CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1068
    const-string v0, "ALTER TABLE specialbuddy ADD COLUMN usertype INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1071
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_is_change_skin CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1072
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_background_style VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1073
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_send_bubble_style VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1074
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_receive_bubble_style VARCHAR(80)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1075
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_translate_my_language TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1076
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_translate_buddy_language TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1077
    const-string v0, "ALTER TABLE inbox ADD COLUMN inbox_enable_translate TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1078
    const-string v0, "ALTER TABLE inbox ADD COLUMN translate_outgoing_message TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1081
    const-string v0, "ALTER TABLE message ADD COLUMN message_content_translated TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1082
    const-string v0, "ALTER TABLE message ADD COLUMN message_from_lang TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1083
    const-string v0, "ALTER TABLE message ADD COLUMN message_to_lang TEXT"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1084
    const-string v0, "ALTER TABLE message ADD COLUMN message_is_spoken CHAR(1) NOT NULL DEFAULT \'N\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1089
    sget-object v0, Lcom/sec/chaton/e/bg;->G:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1090
    sget-object v0, Lcom/sec/chaton/e/bg;->H:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1091
    sget-object v0, Lcom/sec/chaton/e/bg;->I:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1092
    sget-object v0, Lcom/sec/chaton/e/bg;->J:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1093
    sget-object v0, Lcom/sec/chaton/e/bi;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1096
    const-string v0, "CREATE TABLE message_notification (noti_inbox_no TEXT,noti_buddy_no TEXT,noti_message TEXT,noti_sent_time NUMBER,noti_message_id NUMBER,noti_chat_type INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1099
    const-string v0, "CREATE TABLE calllog (_id INTEGER PRIMARY KEY AUTOINCREMENT,calldate TEXT NOT NULL,duration INTEGER\tNOT NULL DEFAULT 0,calllogtype INTEGER\tNOT NULL DEFAULT 0,callmethod INTEGER NOT NULL DEFAULT 0,userno TEXT NOT NULL DEFAULT \'\',username TEXT,userid TEXT,phoneno TEXT,countrycode TEXT,groupcallkey INTEGER NOT NULL DEFAULT 0,rejectmsg TEXT,groupid INTEGER)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1102
    sget-object v0, Lcom/sec/chaton/e/bg;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1103
    sget-object v0, Lcom/sec/chaton/e/bg;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1104
    sget-object v0, Lcom/sec/chaton/e/bg;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1105
    sget-object v0, Lcom/sec/chaton/e/bg;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1106
    const-string v0, "DROP TABLE IF EXISTS poston"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1109
    sget-object v0, Lcom/sec/chaton/e/bg;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1112
    sget-object v0, Lcom/sec/chaton/e/bg;->M:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1115
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->e()V

    .line 1118
    invoke-static {p1}, Lcom/sec/chaton/e/a/af;->a(Landroid/database/sqlite/SQLiteDatabase;)Z

    .line 1121
    invoke-static {}, Lcom/sec/chaton/settings/downloads/az;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/az;

    .line 1122
    const/16 v2, 0x33

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "INSERT INTO "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "download_item"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, " ( "

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "reference_id"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, ", "

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "item_id"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, ", "

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "item_type"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "name"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "install"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "expiration_time"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "new"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string v4, "special"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string v4, "extras"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string v4, "newly_installed"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string v4, " ) values ( "

    aput-object v4, v2, v3

    const/16 v3, 0x17

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x19

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/az;->m:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/az;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x20

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x21

    const-string v4, "\""

    aput-object v4, v2, v3

    const/16 v3, 0x22

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/az;->o:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x23

    const-string v4, "\""

    aput-object v4, v2, v3

    const/16 v3, 0x24

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x25

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x26

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x27

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-wide v4, 0x7fffffffffffffffL

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x28

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x29

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/az;->c:Ljava/lang/Integer;

    aput-object v4, v2, v3

    const/16 v3, 0x2c

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x2d

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x2e

    iget-object v4, v0, Lcom/sec/chaton/settings/downloads/az;->q:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x2f

    const-string v4, "\'"

    aput-object v4, v2, v3

    const/16 v3, 0x30

    const-string v4, ", "

    aput-object v4, v2, v3

    const/16 v3, 0x31

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/16 v0, 0x32

    const-string v3, " ) "

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1155
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "execute. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/e/aw;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 1168
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1172
    const-string v1, "get_all_unread_message_timestamp"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    throw v0

    .line 1158
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/chaton/settings/downloads/az;->b()V

    .line 1161
    const/16 v0, 0x50

    if-ge p2, v0, :cond_1

    .line 1162
    sget-object v0, Lcom/sec/chaton/e/bg;->N:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1166
    :cond_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1168
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1172
    const-string v0, "get_all_unread_message_timestamp"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1176
    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1180
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 1181
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1182
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "samsung_account_email"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1183
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1184
    const-string v4, "com.sec.chaton"

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 1186
    array-length v4, v0

    if-gtz v4, :cond_2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "uid"

    invoke-virtual {v4, v5}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1187
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1188
    invoke-direct {p0, v1, v3, v6}, Lcom/sec/chaton/e/aw;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1201
    :cond_0
    :goto_0
    return-void

    .line 1189
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1190
    invoke-direct {p0, v2, v3, v7}, Lcom/sec/chaton/e/aw;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1192
    :cond_2
    array-length v2, v0

    if-lez v2, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "uid"

    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1193
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1195
    aget-object v1, v0, v6

    const-string v2, "com.android.contacts"

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_0

    .line 1196
    aget-object v1, v0, v6

    const-string v2, "com.android.contacts"

    invoke-static {v1, v2, v7}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 1197
    aget-object v0, v0, v6

    const-string v1, "com.android.contacts"

    invoke-static {v0, v1, v7}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private d()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1232
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "country_name"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1234
    if-nez v1, :cond_1

    .line 1271
    :cond_0
    :goto_0
    return-object v0

    .line 1238
    :cond_1
    const-string v2, "Aland Islands"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1239
    const-string v0, "Aland Islands"

    goto :goto_0

    .line 1240
    :cond_2
    const-string v2, "Argentina"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1241
    const-string v0, "Argentine Republic"

    goto :goto_0

    .line 1242
    :cond_3
    const-string v2, "Azerbaijan"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1243
    const-string v0, "Republic of Azerbaijan"

    goto :goto_0

    .line 1244
    :cond_4
    const-string v2, "Congo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1245
    const-string v0, "Republic of the Congo"

    goto :goto_0

    .line 1246
    :cond_5
    const-string v2, "Cote d\'Ivoire"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1247
    const-string v0, "Cote d\'Ivoire"

    goto :goto_0

    .line 1248
    :cond_6
    const-string v2, "Gabon"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1249
    const-string v0, "Gabonese Republic"

    goto :goto_0

    .line 1250
    :cond_7
    const-string v2, "Guinea-bissau"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1251
    const-string v0, "Guinea-Bissau"

    goto :goto_0

    .line 1252
    :cond_8
    const-string v2, "Kyrgyzstan"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1253
    const-string v0, "Kyrgyz Republic"

    goto :goto_0

    .line 1254
    :cond_9
    const-string v2, "Macedonia"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1255
    const-string v0, "Republic of Macedonia"

    goto :goto_0

    .line 1256
    :cond_a
    const-string v2, "Reunion"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1257
    const-string v0, "Reunion"

    goto :goto_0

    .line 1258
    :cond_b
    const-string v2, "Rwanda"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1259
    const-string v0, "Republic of Rwanda"

    goto :goto_0

    .line 1260
    :cond_c
    const-string v2, "Sao Tome and Principe"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1261
    const-string v0, "Sao Tome and Principe"

    goto/16 :goto_0

    .line 1262
    :cond_d
    const-string v2, "Svalbard and Jan Mayen Islands"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1263
    const-string v0, "Svalbard and Jan Mayen"

    goto/16 :goto_0

    .line 1264
    :cond_e
    const-string v2, "Togo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1265
    const-string v0, "Togolese Republic"

    goto/16 :goto_0

    .line 1266
    :cond_f
    const-string v2, "United States"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1267
    const-string v0, "United States of America"

    goto/16 :goto_0

    .line 1268
    :cond_10
    const-string v2, "US Virgin Islands"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1269
    const-string v0, "United States Virgin Islands"

    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 272
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/sec/chaton/e/aw;->b:Lcom/sec/chaton/e/aw;

    .line 273
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    monitor-exit p0

    return-void

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 1279
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 1284
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 229
    sget-object v0, Lcom/sec/chaton/e/bg;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 230
    sget-object v0, Lcom/sec/chaton/e/bg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 231
    sget-object v0, Lcom/sec/chaton/e/bg;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 232
    sget-object v0, Lcom/sec/chaton/e/bg;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 233
    sget-object v0, Lcom/sec/chaton/e/bg;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 234
    sget-object v0, Lcom/sec/chaton/e/bg;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 235
    sget-object v0, Lcom/sec/chaton/e/bg;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 236
    sget-object v0, Lcom/sec/chaton/e/bg;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 238
    sget-object v0, Lcom/sec/chaton/e/bc;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 256
    invoke-direct {p0, p1}, Lcom/sec/chaton/e/aw;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 259
    invoke-direct {p0, p1, v1, v1}, Lcom/sec/chaton/e/aw;->a(Landroid/database/sqlite/SQLiteDatabase;IZ)V

    .line 261
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1219
    const-string v0, "ChatON"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DatabaseHelper] Upgrading from : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1223
    const-string v0, "tabletdb_interg_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x31

    if-ge p2, v0, :cond_0

    .line 1224
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/chaton/e/aw;->b(Landroid/database/sqlite/SQLiteDatabase;IZ)V

    .line 1228
    :goto_0
    return-void

    .line 1226
    :cond_0
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/chaton/e/aw;->a(Landroid/database/sqlite/SQLiteDatabase;IZ)V

    goto :goto_0
.end method
