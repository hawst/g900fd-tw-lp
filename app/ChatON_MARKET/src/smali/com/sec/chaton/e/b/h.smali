.class public Lcom/sec/chaton/e/b/h;
.super Lcom/sec/chaton/e/b/a;
.source "BuddyBlockTask.java"


# instance fields
.field private c:Lcom/sec/chaton/buddy/a/c;

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>(Lcom/sec/chaton/e/b/d;Lcom/sec/chaton/buddy/a/c;IZ)V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/e/b/a;-><init>(Lcom/sec/chaton/e/b/d;Landroid/os/Handler;)V

    .line 26
    iput-object p2, p0, Lcom/sec/chaton/e/b/h;->c:Lcom/sec/chaton/buddy/a/c;

    .line 27
    iput p3, p0, Lcom/sec/chaton/e/b/h;->e:I

    .line 28
    iput-boolean p4, p0, Lcom/sec/chaton/e/b/h;->d:Z

    .line 29
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 34
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 37
    iget v1, p0, Lcom/sec/chaton/e/b/h;->e:I

    if-ne v1, v2, :cond_1

    .line 38
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/e/b/h;->c:Lcom/sec/chaton/buddy/a/c;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/d;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/buddy/a/c;)Landroid/database/Cursor;

    move-result-object v0

    .line 39
    if-nez v0, :cond_0

    .line 41
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 83
    :cond_0
    :goto_0
    return-object v0

    .line 44
    :cond_1
    iget v1, p0, Lcom/sec/chaton/e/b/h;->e:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 45
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 46
    iget-object v2, p0, Lcom/sec/chaton/e/b/h;->c:Lcom/sec/chaton/buddy/a/c;

    iget-boolean v3, p0, Lcom/sec/chaton/e/b/h;->d:Z

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/e/a/d;->a(Landroid/content/ContentResolver;Lcom/sec/chaton/buddy/a/c;Z)V

    goto :goto_0
.end method
