.class public interface abstract Lcom/sec/chaton/e/bh;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 2541
    const/16 v0, 0xca

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v5

    const-string v1, "*"

    aput-object v1, v0, v4

    const-string v1, "FROM"

    aput-object v1, v0, v6

    const-string v1, "("

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "message_content"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "message_content_type"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "message_download_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "message_content_translated"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "message_from_lang"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "message_to_lang"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "message_is_spoken"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "message_formatted"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "message_read_status"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "message_sever_id"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "message_session_id"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "message_time"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "message_translated"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "message_stored_ext"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "message_is_failed"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "message_is_file_upload"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "message"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, " a"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, " NOT IN "

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "(SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, ") b"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, ") c"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, " IN "

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "(SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "inbox"

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "inbox_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, ") b"

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, " a"

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, ") d"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, "d."

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bh;->a:Ljava/lang/String;

    .line 2589
    const/16 v0, 0xc5

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SELECT "

    aput-object v1, v0, v3

    const-string v1, "*"

    aput-object v1, v0, v5

    const-string v1, " FROM"

    aput-object v1, v0, v4

    const-string v1, "("

    aput-object v1, v0, v6

    const-string v1, "SELECT "

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "c."

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x25

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "message_content"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "message_content_type"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "message_content_translated"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "message_from_lang"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "message_to_lang"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "message_is_spoken"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "message_download_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "message_formatted"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "message_read_status"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "message_sever_id"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "message_session_id"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "message_time"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "message_translated"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "message_stored_ext"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "message_is_failed"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "message_is_file_upload"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "( "

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "message"

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, " = ?"

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, " AND ("

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "message_is_truncated"

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, " = \'N\'"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, " OR "

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "message_content_type"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, " ORDER BY "

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "message_is_failed"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, " DESC , "

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "message_time"

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, " DESC , "

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, " DESC"

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, " LIMIT ?"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, " ) a"

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, ") AS "

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, "buddy_show_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, " p"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, " = ?"

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, " OR "

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, " = \'\'"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, ") c"

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, ") d"

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, " ORDER BY "

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "message_is_failed"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, " , "

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "message_time"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, " , "

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, "_id"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bh;->b:Ljava/lang/String;

    .line 2623
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT * FROM(SELECT ifnull(c.buddy_name,\'\') AS buddy_name,ifnull(c.buddy_no,a.message_sender) AS buddy_no,ifnull(c.buddy_status_message,\'\') AS buddy_status_message,a.message_content,a.message_content_translated,a.message_from_lang,a.message_to_lang,a.message_is_spoken,a.message_content_type,a.message_download_uri,a.message_formatted,a._id,a.message_inbox_no,a.message_read_status,a.message_sender,a.message_sever_id,a.message_session_id,a.message_time,a.message_translated,a.message_stored_ext,a.message_is_failed,a.message_is_file_upload,a.message_type,a.message_is_truncated FROM ( SELECT * FROM message WHERE message_inbox_no = ? AND message_type = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_is_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIMIT ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ) a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ifnull("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "b."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "b."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_status_message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participant"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " p"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_status_message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " UNION ALL "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "msgstatus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_status_message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "specialbuddy"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "b."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_inbox_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_inbox_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GROUP BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") c"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_sender"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "c."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bh;->c:Ljava/lang/String;

    .line 2801
    const/16 v0, 0x80

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v5

    const-string v1, "ifnull("

    aput-object v1, v0, v4

    const-string v1, "b."

    aput-object v1, v0, v6

    const-string v1, "buddy_no"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "c."

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "rowid"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "rowid"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "participants_country_code"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, ",\'\') AS "

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "participants_country_code"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "participants_is_auth"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, ",\'\') AS "

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "participants_is_auth"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "participants_status"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, ",0) AS "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "participants_status"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, ",\'\') AS "

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "rowid"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "participants_country_code"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, ",\'\') AS "

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "participants_country_code"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "participants_is_auth"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, ",\'\') AS "

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "participants_is_auth"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "participants_status"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, ",0) AS "

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "participants_status"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, ") c"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "( "

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "msgstatus"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "specialbuddy"

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, " )"

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bh;->d:Ljava/lang/String;

    .line 2824
    const/16 v0, 0x48

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v3

    const-string v1, "SELECT "

    aput-object v1, v0, v5

    const-string v1, "a."

    aput-object v1, v0, v4

    const-string v1, "buddy_no"

    aput-object v1, v0, v6

    const-string v1, ","

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "a."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "specialbuddy"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, " NOT IN "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "( "

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "specialbuddy"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, ") a"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " a."

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bh;->e:Ljava/lang/String;

    .line 2833
    const/16 v0, 0x37

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SELECT "

    aput-object v1, v0, v3

    const-string v1, "message_total_count"

    aput-object v1, v0, v5

    const-string v1, ", "

    aput-object v1, v0, v4

    const-string v1, "ifnull("

    aput-object v1, v0, v6

    const-string v1, "message_sent_count"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, ", 0) AS "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "message_sent_count"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, ") AS "

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "( "

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "( "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "SELECT count(*) AS "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "message_total_count"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "message"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, " = ? AND "

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "message_content_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, " != "

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, " ) a"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "( "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "SELECT count(*) AS "

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "message_sent_count"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "message"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, " = ? AND "

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "message_content_type"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, " != "

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    invoke-virtual {v2}, Lcom/sec/chaton/e/w;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, " = ? "

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, " ) b"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " ON a."

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, " = b."

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, " )"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bh;->f:Ljava/lang/String;

    .line 2840
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " SELECT * FROM(SELECT ifnull(c.buddy_name,\'\') AS buddy_name,ifnull(c.buddy_no,a.message_sender) AS buddy_no,ifnull(c.buddy_status_message,\'\') AS buddy_status_message,ifnull(c.buddy_show_phone_number,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_show_phone_number"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ifnull("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "c."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_profile_status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_profile_status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_content"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_content_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_download_uri"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_formatted"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_inbox_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_read_status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_sender"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_sever_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_session_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_translated"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_stored_ext"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_is_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_is_file_upload"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_inbox_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_is_truncated"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'N\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_content_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_content_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_content_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_is_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ) a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ifnull("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "b."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "b."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_status_message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "b."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_show_phone_number"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "b."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_profile_status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participant"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " p"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "b."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_inbox_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "participants_inbox_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GROUP BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") c"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "a."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_sender"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "c."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "buddy_no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_is_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bh;->g:Ljava/lang/String;

    return-void
.end method
