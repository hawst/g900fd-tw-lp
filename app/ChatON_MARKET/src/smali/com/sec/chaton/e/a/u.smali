.class public Lcom/sec/chaton/e/a/u;
.super Landroid/content/AsyncQueryHandler;
.source "NotifyingAsyncQueryHandler.java"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/chaton/e/a/v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 53
    invoke-virtual {p0, p2}, Lcom/sec/chaton/e/a/u;->a(Lcom/sec/chaton/e/a/v;)V

    .line 54
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/e/a/u;->a:Ljava/lang/ref/WeakReference;

    .line 62
    return-void
.end method

.method public a(Lcom/sec/chaton/e/a/v;)V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/chaton/e/a/u;->a:Ljava/lang/ref/WeakReference;

    .line 58
    return-void
.end method

.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/chaton/e/a/u;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 96
    :goto_0
    if-eqz v0, :cond_0

    .line 97
    invoke-interface {v0, p1, p2, p3}, Lcom/sec/chaton/e/a/v;->onDeleteComplete(ILjava/lang/Object;I)V

    .line 99
    :cond_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/e/a/u;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/a/v;

    goto :goto_0
.end method

.method protected onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/chaton/e/a/u;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 80
    :goto_0
    if-eqz v0, :cond_0

    .line 81
    invoke-interface {v0, p1, p2, p3}, Lcom/sec/chaton/e/a/v;->onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V

    .line 83
    :cond_0
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/e/a/u;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/a/v;

    goto :goto_0
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/chaton/e/a/u;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 70
    :goto_0
    if-eqz v0, :cond_2

    .line 71
    invoke-interface {v0, p1, p2, p3}, Lcom/sec/chaton/e/a/v;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 75
    :cond_0
    :goto_1
    return-void

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/e/a/u;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/a/v;

    goto :goto_0

    .line 72
    :cond_2
    if-eqz p3, :cond_0

    .line 73
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method protected onUpdateComplete(ILjava/lang/Object;I)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/chaton/e/a/u;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 88
    :goto_0
    if-eqz v0, :cond_0

    .line 89
    invoke-interface {v0, p1, p2, p3}, Lcom/sec/chaton/e/a/v;->onUpdateComplete(ILjava/lang/Object;I)V

    .line 91
    :cond_0
    return-void

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/e/a/u;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/a/v;

    goto :goto_0
.end method
