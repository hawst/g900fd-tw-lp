.class public interface abstract Lcom/sec/chaton/e/az;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2484
    const/16 v0, 0x2d

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "("

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "a."

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "a."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "cnt"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "0) AS "

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "cnt"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "buddy_group"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " a"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " LEFT OUTER JOIN ("

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "COUNT(*) AS "

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "cnt"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "grouprelation"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, " HAVING "

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, " > 1"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, ") b"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, " != 1"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, ")f"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/az;->a:Ljava/lang/String;

    return-void
.end method
