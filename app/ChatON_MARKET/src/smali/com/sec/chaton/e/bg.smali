.class public interface abstract Lcom/sec/chaton/e/bg;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final A:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final B:Ljava/lang/String;

.field public static final C:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final D:Ljava/lang/String;

.field public static final E:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final F:Ljava/lang/String;

.field public static final G:Ljava/lang/String;

.field public static final H:Ljava/lang/String;

.field public static final I:Ljava/lang/String;

.field public static final J:Ljava/lang/String;

.field public static final K:Ljava/lang/String;

.field public static final L:Ljava/lang/String;

.field public static final M:Ljava/lang/String;

.field public static final N:Ljava/lang/String;

.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;

.field public static final h:Ljava/lang/String;

.field public static final i:Ljava/lang/String;

.field public static final j:Ljava/lang/String;

.field public static final k:Ljava/lang/String;

.field public static final l:Ljava/lang/String;

.field public static final m:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final n:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final o:Ljava/lang/String;

.field public static final p:Ljava/lang/String;

.field public static final q:Ljava/lang/String;

.field public static final r:Ljava/lang/String;

.field public static final s:Ljava/lang/String;

.field public static final t:Ljava/lang/String;

.field public static final u:Ljava/lang/String;

.field public static final v:Ljava/lang/String;

.field public static final w:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final x:Ljava/lang/String;

.field public static final y:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final z:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1359
    const/16 v0, 0x26

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "buddy"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " VARCHAR(255),"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(80),"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " VARCHAR(80),"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " VARCHAR(25),"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "buddy_birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " VARCHAR(10) NOT NULL DEFAULT \'\',"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "buddy_msg_send"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " INTEGER DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "buddy_msg_received"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " INTEGER DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, " CHAR(1) NOT NULL DEFAULT \'N\',"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "buddy_raw_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " NUMBER DEFAULT 0, "

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "buddy_push_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " VARCHAR(25),"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "buddy_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, " CHAR(1) NOT NULL DEFAULT \'Y\',"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, " INTEGER NOT NULL DEFAULT "

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NONE_PROFILE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v2}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->a:Ljava/lang/String;

    .line 1372
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "buddy_group"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "group_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " INTEGER NOT NULL DEFAULT 2,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ") ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->b:Ljava/lang/String;

    .line 1379
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "grouprelation"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(25) NOT NULL REFERENCES "

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "),"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(25) NOT NULL REFERENCES "

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "buddy_group"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "),"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "group_relation_group"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "group_relation_buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ") ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->c:Ljava/lang/String;

    .line 1383
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "relation"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " INTEGER NOT NULL DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " INTEGER NOT NULL DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " INTEGER NOT NULL DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " INTEGER NOT NULL DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " INTEGER NOT NULL DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " INTEGER NOT NULL DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ") ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->d:Ljava/lang/String;

    .line 1392
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "contacts"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "contacts_id"

    aput-object v1, v0, v6

    const-string v1, " NUMBER,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "contact_raw_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " NUMBER,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "conatct_number"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "contacts_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "contacts_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "contact_raw_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ") ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->e:Ljava/lang/String;

    .line 1400
    const/16 v0, 0x2c

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "inbox"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "inbox_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "inbox_unread_count"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " INTEGER NOT NULL DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "inbox_last_message"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "inbox_title"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "inbox_last_time"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " NUMBER,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "inbox_lang_from"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "inbox_lang_to"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "inbox_translated"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " TEXT NOT NULL DEFAULT \'N\',"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "inbox_server_ip"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "inbox_server_port"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " NUMBER,"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "inbox_participants"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "inbox_session_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "inbox_last_msg_no"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, " NUMBER,"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "inbox_last_msg_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "inbox_title_fixed"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, " TEXT NOT NULL DEFAULT \'N\',"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "inbox_last_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " NUMBER,"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "inbox_last_temp_msg"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, " TEXT NOT NULL DEFAULT \'\',"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, ") ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->f:Ljava/lang/String;

    .line 1422
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "inbox_buddy_relation"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "inbox_no"

    aput-object v1, v0, v6

    const-string v1, " TEXT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ") ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->g:Ljava/lang/String;

    .line 1433
    const/16 v0, 0x2a

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "message"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "message_sever_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "message_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "message_session_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " INTEGER,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "message_read_status"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " INTEGER,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "message_content_type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " INTEGER DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "message_time"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " NUMBER,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "message_content"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "message_translated"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "message_type"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " INTEGER,"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "message_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "message_download_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "message_formatted"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "message_tid"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "message_time_text"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "message_stored_ext"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, " INTEGER DEFAULT 1,"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "message_need_update"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, " TEXT NOT NULL DEFAULT \'Y\',"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "message_is_failed"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " INTEGER DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, ") ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->h:Ljava/lang/String;

    .line 1438
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "participant"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "participants_buddy_no"

    aput-object v1, v0, v6

    const-string v1, " TEXT NOT NULL,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ") ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->i:Ljava/lang/String;

    .line 1442
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "participant"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "participants_buddy_no"

    aput-object v1, v0, v6

    const-string v1, " TEXT NOT NULL,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "participants_country_code"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "participants_is_auth"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " CHAR(1),"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "participants_status"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " INTEGER,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ") ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->j:Ljava/lang/String;

    .line 1447
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "participants_buddy_no"

    aput-object v1, v0, v3

    const-string v1, ", "

    aput-object v1, v0, v4

    const-string v1, "participants_inbox_no"

    aput-object v1, v0, v5

    const-string v1, ", "

    aput-object v1, v0, v6

    const-string v1, "participants_buddy_name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, ", "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "participants_country_code"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "participants_is_auth"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "participants_status"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->k:Ljava/lang/String;

    .line 1450
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "participant_mapping"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "participants_buddy_no"

    aput-object v1, v0, v6

    const-string v1, " TEXT NOT NULL,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "participants_old_buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "participants_old_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ") ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->l:Ljava/lang/String;

    .line 1455
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "memo"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(25),"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(80),"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "memo"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " VARCHAR(255),"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(80)"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ", UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->m:Ljava/lang/String;

    .line 1461
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "memo_sessions"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(25),"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(80),"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "starttime"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " VARCHAR(80),"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "endtime"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(80),"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "isblind"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " VARCHAR(80)"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ", UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->n:Ljava/lang/String;

    .line 1466
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "poston"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "poston"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "isread"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "unread_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "read_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " VARCHAR(80) NOT NULL"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ", UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->o:Ljava/lang/String;

    .line 1471
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "my_poston"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "image_status"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "poston_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "isread"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "read_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "unread_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "unread_mood_count"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "total_mood_count"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "poston"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "multimedia_list"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " TEXT"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, ", UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->p:Ljava/lang/String;

    .line 1478
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "buddy_poston"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "image_status"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "poston_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "isread"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "read_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "unread_comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "unread_mood_count"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "total_mood_count"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "poston"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "multimedia_list"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " TEXT"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, ", UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->q:Ljava/lang/String;

    .line 1487
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "poston_comments"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "comment"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "isread"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " VARCHAR(80) NOT NULL"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ", UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->r:Ljava/lang/String;

    .line 1492
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "my_multimedia_poston"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "poston_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "poston_metaid"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "poston_metatype"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "poston_url"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "poston_seq"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " TEXT NOT NULL"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ", UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "poston_url"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->s:Ljava/lang/String;

    .line 1495
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "buddy_multimedia_poston"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "poston_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "poston_metaid"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "poston_metatype"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "poston_url"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "poston_seq"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " TEXT NOT NULL"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ", UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "poston_url"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->t:Ljava/lang/String;

    .line 1500
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "cover_story_sample"

    aput-object v1, v0, v4

    const-string v1, "("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "coverstory_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "coverstory_thumb_url"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "coverstory_filename"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ", UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "coverstory_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->u:Ljava/lang/String;

    .line 1505
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "recommendee"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " INTEGER DEFAULT "

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "ignore"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " INTEGER DEFAULT 0,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->v:Ljava/lang/String;

    .line 1511
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "anicon_category"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "category_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "thumbnail_url"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "category_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->w:Ljava/lang/String;

    .line 1515
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "anicon_category"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "category_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "thumbnail_url"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "category_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->x:Ljava/lang/String;

    .line 1519
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "anicon_package"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "package_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "category_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "preview_url"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "thumbnail_url"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "package_zip_url"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "anicon_count"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "volume"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "expiration_time"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "package_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->y:Ljava/lang/String;

    .line 1528
    const/16 v0, 0x1e

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "anicon_package"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "package_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "category_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "preview_url"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "thumbnail_url"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "package_zip_url"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "anicon_count"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "volume"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "expiration_time"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "special"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "package_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->z:Ljava/lang/String;

    .line 1537
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "anicon_item"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "anicon_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "package_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(255),"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "delegate_url"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "cd_proxy_url"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "zip_url"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "sent_time"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "anicon_id"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->A:Ljava/lang/String;

    .line 1543
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "anicon_item"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "anicon_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "package_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " INTEGER,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "delegate_url"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "cd_proxy_url"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "zip_url"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "sent_time"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "anicon_id"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->B:Ljava/lang/String;

    .line 1549
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "skin"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "skin_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "special"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "thumbnail_url"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "bg_type"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "expiration_date"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "new"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "skin_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->C:Ljava/lang/String;

    .line 1554
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "specialbuddy"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "followcount"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(80),"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "likecount"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " VARCHAR(80),"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "msgstatus"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " VARCHAR(256),"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "photoloaded"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " VARCHAR(25),"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "status"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " VARCHAR(25),"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "url"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " VARCHAR(80),"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->D:Ljava/lang/String;

    .line 1561
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "ams_item"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "item_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "item_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "thumbnail_url"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "expiration_time"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "new"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "special"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "item_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->E:Ljava/lang/String;

    .line 1567
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "download_item"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "reference_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "item_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "item_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(255),"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "install"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "expiration_time"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "new"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "special"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "extras"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "item_id"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "item_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->F:Ljava/lang/String;

    .line 1573
    const/16 v0, 0x28

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "poll"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "poll_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "poll_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "poll_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "poll_creator_no"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "poll_creator_name"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "poll_status"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " INTEGER NOT NULL DEFAULT(1),"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "poll_question_type"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "poll_question_text"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "poll_question_image"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "poll_answer_list"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "poll_is_private_result"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, " CHAR(1) DEFAULT \'N\',"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "poll_create_time"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "poll_expected_end_time"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "poll_end_time"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "poll_voter_total_count"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "poll_voter_answer_count"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, " INTEGER NOT NULL DEFAULT(0),"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "poll_id"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->G:Ljava/lang/String;

    .line 1581
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "poll_voter"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "voter_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " VARCHAR(80) NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->H:Ljava/lang/String;

    .line 1584
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "poll_relation_voter"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "poll_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "poll_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->I:Ljava/lang/String;

    .line 1587
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "answer_relation_voter"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "answer_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(25) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "answer_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "voter_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->J:Ljava/lang/String;

    .line 1591
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "poll_inbox"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "poll_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "poll_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "poll_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->K:Ljava/lang/String;

    .line 1595
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "font_filter"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "filter_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "filter_title"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " VARCHAR(255) NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " UNIQUE ("

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "filter_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "))"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->L:Ljava/lang/String;

    .line 1600
    new-array v0, v7, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "appmanage"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "packageName TEXT, UNIQUE(packageName))"

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->M:Ljava/lang/String;

    .line 1604
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "CREATE TABLE "

    aput-object v1, v0, v3

    const-string v1, "more_apps"

    aput-object v1, v0, v4

    const-string v1, " ("

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "title"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "priority"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " INTEGER NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "contenturl"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " TEXT NOT NULL,"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "appid"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "linkurl"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "samsungappsurl"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " TEXT,"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "downloadurl"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, " TEXT"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bg;->N:Ljava/lang/String;

    return-void
.end method
