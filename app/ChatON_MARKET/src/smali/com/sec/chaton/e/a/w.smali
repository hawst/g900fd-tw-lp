.class public Lcom/sec/chaton/e/a/w;
.super Landroid/content/AsyncQueryHandler;
.source "NotifyingAsyncTaskWorker.java"


# instance fields
.field private a:Landroid/os/Handler;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 32
    return-void
.end method

.method public static a()Lcom/sec/chaton/e/a/w;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/sec/chaton/e/a/w;

    invoke-direct {v0}, Lcom/sec/chaton/e/a/w;-><init>()V

    return-object v0
.end method

.method private final a(ILcom/sec/chaton/e/b/a;)V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 61
    iput p1, v0, Landroid/os/Message;->what:I

    .line 62
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 65
    iget-object v1, p0, Lcom/sec/chaton/e/a/w;->a:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/sec/chaton/e/a/w;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 69
    :cond_0
    return-void
.end method

.method public static a(Lcom/sec/chaton/e/a/w;ILcom/sec/chaton/e/b/a;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/e/a/w;->a(ILcom/sec/chaton/e/b/a;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected createHandler(Landroid/os/Looper;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/sec/chaton/e/a/x;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/e/a/x;-><init>(Lcom/sec/chaton/e/a/w;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/e/a/w;->a:Landroid/os/Handler;

    .line 37
    iget-object v0, p0, Lcom/sec/chaton/e/a/w;->a:Landroid/os/Handler;

    return-object v0
.end method
