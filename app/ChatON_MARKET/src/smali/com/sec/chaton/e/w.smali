.class public final enum Lcom/sec/chaton/e/w;
.super Ljava/lang/Enum;
.source "ChatONContract.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/e/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/e/w;

.field public static final enum b:Lcom/sec/chaton/e/w;

.field public static final enum c:Lcom/sec/chaton/e/w;

.field public static final enum d:Lcom/sec/chaton/e/w;

.field public static final enum e:Lcom/sec/chaton/e/w;

.field public static final enum f:Lcom/sec/chaton/e/w;

.field public static final enum g:Lcom/sec/chaton/e/w;

.field public static final enum h:Lcom/sec/chaton/e/w;

.field public static final enum i:Lcom/sec/chaton/e/w;

.field public static final enum j:Lcom/sec/chaton/e/w;

.field public static final enum k:Lcom/sec/chaton/e/w;

.field public static final enum l:Lcom/sec/chaton/e/w;

.field public static final enum m:Lcom/sec/chaton/e/w;

.field public static final enum n:Lcom/sec/chaton/e/w;

.field public static final enum o:Lcom/sec/chaton/e/w;

.field public static final enum p:Lcom/sec/chaton/e/w;

.field public static final enum q:Lcom/sec/chaton/e/w;

.field public static final enum r:Lcom/sec/chaton/e/w;

.field private static final synthetic t:[Lcom/sec/chaton/e/w;


# instance fields
.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1123
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "SYSTEM"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    .line 1124
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    .line 1125
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v6, v5}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    .line 1126
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    .line 1127
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "GEO"

    invoke-direct {v0, v1, v8, v7}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    .line 1128
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "AUDIO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    .line 1129
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "CONTACT"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    .line 1130
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "CALENDAR"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    .line 1132
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "ANICON"

    const/16 v2, 0x8

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    .line 1134
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "DOCUMENT"

    const/16 v2, 0x9

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    .line 1135
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "APPLINK"

    const/16 v2, 0xa

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    .line 1136
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "POLL"

    const/16 v2, 0xb

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    .line 1137
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "FILE"

    const/16 v2, 0xc

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    .line 1138
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "AMS"

    const/16 v2, 0xd

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    .line 1139
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "LIVESHARE"

    const/16 v2, 0xe

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    .line 1140
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "LIVERECOMMEND"

    const/16 v2, 0xf

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    .line 1141
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "LIVECONTENTS"

    const/16 v2, 0x10

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->q:Lcom/sec/chaton/e/w;

    .line 1142
    new-instance v0, Lcom/sec/chaton/e/w;

    const-string v1, "UNDEFINED"

    const/16 v2, 0x11

    const/16 v3, 0x63

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/w;->r:Lcom/sec/chaton/e/w;

    .line 1122
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/chaton/e/w;->q:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/chaton/e/w;->r:Lcom/sec/chaton/e/w;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/e/w;->t:[Lcom/sec/chaton/e/w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1146
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1147
    iput p3, p0, Lcom/sec/chaton/e/w;->s:I

    .line 1148
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/e/w;
    .locals 1

    .prologue
    .line 1155
    sparse-switch p0, :sswitch_data_0

    .line 1195
    sget-object v0, Lcom/sec/chaton/e/w;->r:Lcom/sec/chaton/e/w;

    :goto_0
    return-object v0

    .line 1157
    :sswitch_0
    sget-object v0, Lcom/sec/chaton/e/w;->a:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1159
    :sswitch_1
    sget-object v0, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1161
    :sswitch_2
    sget-object v0, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1163
    :sswitch_3
    sget-object v0, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1165
    :sswitch_4
    sget-object v0, Lcom/sec/chaton/e/w;->e:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1167
    :sswitch_5
    sget-object v0, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1169
    :sswitch_6
    sget-object v0, Lcom/sec/chaton/e/w;->g:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1171
    :sswitch_7
    sget-object v0, Lcom/sec/chaton/e/w;->h:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1174
    :sswitch_8
    sget-object v0, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1177
    :sswitch_9
    sget-object v0, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1179
    :sswitch_a
    sget-object v0, Lcom/sec/chaton/e/w;->k:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1181
    :sswitch_b
    sget-object v0, Lcom/sec/chaton/e/w;->l:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1183
    :sswitch_c
    sget-object v0, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1185
    :sswitch_d
    sget-object v0, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1187
    :sswitch_e
    sget-object v0, Lcom/sec/chaton/e/w;->o:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1189
    :sswitch_f
    sget-object v0, Lcom/sec/chaton/e/w;->p:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1191
    :sswitch_10
    sget-object v0, Lcom/sec/chaton/e/w;->q:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1193
    :sswitch_11
    sget-object v0, Lcom/sec/chaton/e/w;->r:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 1155
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x5 -> :sswitch_6
        0x6 -> :sswitch_7
        0x7 -> :sswitch_8
        0x8 -> :sswitch_9
        0x9 -> :sswitch_a
        0xa -> :sswitch_b
        0xb -> :sswitch_c
        0xc -> :sswitch_d
        0xd -> :sswitch_e
        0xe -> :sswitch_f
        0xf -> :sswitch_10
        0x63 -> :sswitch_11
    .end sparse-switch
.end method

.method public static a(Lcom/sec/chaton/e/w;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1343
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/e/w;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1348
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/chaton/e/w;->a(Lcom/sec/chaton/e/w;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/chaton/e/w;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 8

    .prologue
    const v7, 0x7f0b00d0

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1226
    const/4 v0, 0x0

    .line 1229
    sget-object v1, Lcom/sec/chaton/e/h;->a:[I

    invoke-virtual {p0}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1338
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 1231
    :pswitch_1
    if-eqz p3, :cond_1

    .line 1232
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00d2

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1234
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00cb

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1238
    :pswitch_2
    if-eqz p3, :cond_2

    .line 1239
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00d3

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1241
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00cc

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1245
    :pswitch_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00cd

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1248
    :pswitch_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00d6

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1251
    :pswitch_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00ce

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1254
    :pswitch_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00cf

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1258
    :pswitch_7
    if-eqz p3, :cond_3

    .line 1259
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00d4

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1261
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01d3

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1285
    :pswitch_8
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v5

    const-string v2, ""

    aput-object v2, v1, v4

    invoke-virtual {v0, v7, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1286
    if-eqz p1, :cond_0

    .line 1287
    const-string v1, "\n"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1288
    if-eqz v1, :cond_0

    array-length v2, v1

    if-le v2, v6, :cond_0

    .line 1289
    aget-object v0, v1, v6

    aget-object v1, v1, v6

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1290
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {v1, v7, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1295
    :pswitch_9
    if-eqz p3, :cond_4

    .line 1296
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00d5

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1298
    :cond_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00d1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1303
    :pswitch_a
    invoke-static {p1, v4}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v1

    .line 1304
    if-eqz v1, :cond_0

    .line 1305
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b03c8

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v5

    iget-object v1, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->title:Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1310
    :pswitch_b
    invoke-static {p1, v4}, Lcom/sec/chaton/specialbuddy/g;->b(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v1

    .line 1311
    if-eqz v1, :cond_0

    .line 1312
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b03c9

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v5

    iget-object v1, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1317
    :pswitch_c
    invoke-static {p1}, Lcom/sec/chaton/specialbuddy/g;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1322
    :pswitch_d
    :try_start_0
    invoke-static {p1}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->parse(Ljava/lang/String;)Lcom/sec/chaton/io/entry/MessageType4Entry;

    move-result-object v0

    .line 1323
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00d7

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/sec/chaton/io/entry/MessageType4Entry;->getDisplayMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 1324
    :catch_0
    move-exception v0

    .line 1325
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_5

    .line 1326
    const-class v1, Lcom/sec/chaton/e/g;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_5
    move-object v0, p1

    .line 1331
    goto/16 :goto_0

    .line 1333
    :pswitch_e
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1229
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_7
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_8
        :pswitch_8
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static a(Lcom/sec/chaton/e/w;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x3

    .line 1352
    const-string v0, ""

    .line 1353
    sget-object v1, Lcom/sec/chaton/e/h;->a:[I

    invoke-virtual {p0}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1434
    :pswitch_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01a0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1437
    :cond_0
    :goto_0
    return-object v0

    .line 1355
    :pswitch_1
    if-eqz p2, :cond_1

    .line 1356
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0052

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1358
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0049

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1362
    :pswitch_2
    if-eqz p2, :cond_2

    .line 1363
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0053

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1365
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b004a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1369
    :pswitch_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b004c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1372
    :pswitch_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b004b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1375
    :pswitch_5
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b009f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1378
    :pswitch_6
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1381
    :pswitch_7
    if-eqz p2, :cond_3

    .line 1382
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0055

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1384
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01cd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1388
    :pswitch_8
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b022e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1392
    :pswitch_9
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b004f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1394
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1395
    const-string v1, "\n"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1396
    if-eqz v1, :cond_0

    array-length v2, v1

    if-le v2, v3, :cond_0

    .line 1397
    aget-object v2, v1, v3

    aget-object v1, v1, v3

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 1398
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1399
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1406
    :pswitch_a
    if-eqz p2, :cond_4

    .line 1407
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0054

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1409
    :cond_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0051

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1413
    :pswitch_b
    invoke-static {p1, v4}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v1

    .line 1414
    if-eqz v1, :cond_0

    .line 1415
    iget-object v0, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 1420
    :pswitch_c
    invoke-static {p1, v4}, Lcom/sec/chaton/specialbuddy/g;->b(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v1

    .line 1421
    if-eqz v1, :cond_0

    .line 1422
    iget-object v0, v1, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 1427
    :pswitch_d
    invoke-static {p1}, Lcom/sec/chaton/specialbuddy/g;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1431
    :pswitch_e
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1353
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_7
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_9
        :pswitch_9
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_8
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public static a(Lcom/sec/chaton/e/w;[Ljava/lang/String;Z)Ljava/lang/String;
    .locals 10

    .prologue
    const v9, 0x7f0b0035

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x1

    .line 1441
    const-string v0, ""

    .line 1442
    const-string v1, ""

    .line 1443
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b02b2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1445
    sget-object v3, Lcom/sec/chaton/e/h;->a:[I

    invoke-virtual {p0}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1597
    :cond_0
    :goto_0
    return-object v1

    .line 1447
    :pswitch_0
    if-eqz p1, :cond_1

    array-length v1, p1

    if-lt v1, v6, :cond_1

    .line 1448
    aget-object v0, p1, v8

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1451
    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1452
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1453
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1458
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1461
    :pswitch_1
    if-eqz p2, :cond_2

    .line 1462
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0052

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1464
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0049

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1468
    :pswitch_2
    if-eqz p2, :cond_3

    .line 1469
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0053

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1471
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b004a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1475
    :pswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b004c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1478
    :pswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b004b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1481
    :pswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b009f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1484
    :pswitch_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1487
    :pswitch_7
    if-eqz p2, :cond_4

    .line 1488
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0055

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1490
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b01cd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1494
    :pswitch_8
    if-eqz p1, :cond_5

    array-length v1, p1

    if-lt v1, v6, :cond_5

    .line 1495
    aget-object v0, p1, v8

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1497
    :cond_5
    const-string v1, ""

    .line 1499
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1500
    const-string v0, "push_message"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1501
    const-string v2, "content_type"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1502
    const-string v2, "result"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1503
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b022f

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1510
    :goto_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-virtual {v1, v9, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v1, v0

    .line 1512
    goto/16 :goto_0

    .line 1505
    :cond_6
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b022e

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 1507
    :catch_0
    move-exception v0

    .line 1508
    :try_start_2
    const-string v2, "getLastMessageMe"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1510
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v1, v2, v7

    invoke-virtual {v0, v9, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v7

    invoke-virtual {v2, v9, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    throw v0

    .line 1519
    :pswitch_9
    if-eqz p1, :cond_7

    array-length v0, p1

    if-ge v0, v6, :cond_8

    .line 1520
    :cond_7
    const-string v0, ""

    .line 1537
    :goto_3
    invoke-static {v0}, Lcom/sec/chaton/io/entry/ApplinkMsgEntry;->getDisplayMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1538
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-virtual {v1, v9, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1522
    :cond_8
    aget-object v0, p1, v8

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1542
    :pswitch_a
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00d9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1543
    if-eqz p1, :cond_d

    array-length v1, p1

    if-lt v1, v6, :cond_d

    aget-object v1, p1, v8

    if-eqz v1, :cond_d

    .line 1545
    aget-object v1, p1, v8

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1546
    if-eqz v1, :cond_a

    array-length v2, v1

    if-le v2, v6, :cond_a

    .line 1547
    aget-object v2, v1, v6

    aget-object v1, v1, v6

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 1548
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1549
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v7

    invoke-virtual {v0, v9, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_9
    :goto_4
    move-object v1, v0

    .line 1554
    goto/16 :goto_0

    .line 1552
    :cond_a
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    aget-object v2, p1, v8

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v9, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 1557
    :pswitch_b
    if-eqz p2, :cond_b

    .line 1558
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0054

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1560
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0051

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1564
    :pswitch_c
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lt v0, v6, :cond_0

    .line 1565
    aget-object v0, p1, v8

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1566
    invoke-static {v0, v5}, Lcom/sec/chaton/specialbuddy/g;->a(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;

    move-result-object v0

    .line 1567
    if-eqz v0, :cond_c

    .line 1568
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyShareContentsEntry;->title:Ljava/lang/String;

    aput-object v0, v2, v7

    invoke-virtual {v1, v9, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    move-object v1, v0

    .line 1570
    goto/16 :goto_0

    .line 1574
    :pswitch_d
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lt v0, v6, :cond_0

    .line 1575
    aget-object v0, p1, v8

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1576
    invoke-static {v0, v5}, Lcom/sec/chaton/specialbuddy/g;->b(Ljava/lang/String;Z)Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;

    move-result-object v0

    .line 1577
    if-eqz v0, :cond_0

    .line 1578
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/specialbuddy/SpecialBuddyRecommendBuddyEntry;->name:Ljava/lang/String;

    aput-object v0, v2, v7

    invoke-virtual {v1, v9, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1585
    :pswitch_e
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lt v0, v6, :cond_0

    .line 1586
    aget-object v0, p1, v8

    invoke-static {v0}, Lcom/sec/chaton/chat/eq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1587
    invoke-static {v0}, Lcom/sec/chaton/specialbuddy/g;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1593
    :pswitch_f
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_c
    move-object v0, v1

    goto :goto_5

    :cond_d
    move-object v1, v0

    goto/16 :goto_0

    .line 1445
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_7
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_a
        :pswitch_a
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_8
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_9
        :pswitch_f
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/e/w;
    .locals 1

    .prologue
    .line 1122
    const-class v0, Lcom/sec/chaton/e/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/w;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/e/w;
    .locals 1

    .prologue
    .line 1122
    sget-object v0, Lcom/sec/chaton/e/w;->t:[Lcom/sec/chaton/e/w;

    invoke-virtual {v0}, [Lcom/sec/chaton/e/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/e/w;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1151
    iget v0, p0, Lcom/sec/chaton/e/w;->s:I

    return v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 1206
    sget-object v0, Lcom/sec/chaton/e/h;->a:[I

    invoke-virtual {p0}, Lcom/sec/chaton/e/w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1220
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1217
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1206
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
