.class public interface abstract Lcom/sec/chaton/e/bf;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2906
    const/16 v0, 0x76

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "("

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const-string v1, "b."

    aput-object v1, v0, v3

    const/4 v1, 0x3

    const-string v2, "relation_date"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "a."

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "buddy_email"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "buddy_samsung_email"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "buddy_orginal_number"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "buddy_msg_send"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "buddy_msg_received"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "buddy_relation_hide"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "IFNULL("

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "relation_send"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "IFNULL("

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "relation_received"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "IFNULL("

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "relation_point"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "IFNULL("

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "6"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "relation_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "IFNULL("

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "relation_increase"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "IFNULL("

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "9999999"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "relation_rank"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "IFNULL("

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "buddy_profile_status"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "IFNULL("

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "relation_last_msg_time"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "DATETIME(\'NOW\', \'localtime\')) AS "

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "relation_last_msg_time"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x66

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "group_type"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, " a"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, " JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "relation"

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "relation_buddy_id"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bf;->a:Ljava/lang/String;

    return-void
.end method
