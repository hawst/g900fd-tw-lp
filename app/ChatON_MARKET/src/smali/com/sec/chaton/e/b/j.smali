.class public Lcom/sec/chaton/e/b/j;
.super Lcom/sec/chaton/e/b/a;
.source "BuddyGroupAddTask.java"


# instance fields
.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:I


# direct methods
.method public constructor <init>(Lcom/sec/chaton/e/b/d;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/e/b/a;-><init>(Lcom/sec/chaton/e/b/d;Landroid/os/Handler;)V

    .line 22
    iput-object p2, p0, Lcom/sec/chaton/e/b/j;->d:Ljava/lang/String;

    .line 23
    iput p3, p0, Lcom/sec/chaton/e/b/j;->f:I

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/e/b/d;Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/e/b/a;-><init>(Lcom/sec/chaton/e/b/d;Landroid/os/Handler;)V

    .line 44
    iput p4, p0, Lcom/sec/chaton/e/b/j;->f:I

    .line 45
    iput-object p2, p0, Lcom/sec/chaton/e/b/j;->d:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/e/b/j;->e:Ljava/util/ArrayList;

    .line 49
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/sec/chaton/e/b/j;->e:Ljava/util/ArrayList;

    aget-object v2, p3, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/e/b/d;[Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 28
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/e/b/a;-><init>(Lcom/sec/chaton/e/b/d;Landroid/os/Handler;)V

    .line 30
    iput p3, p0, Lcom/sec/chaton/e/b/j;->f:I

    .line 31
    const/4 v0, 0x0

    aget-object v0, p2, v0

    iput-object v0, p0, Lcom/sec/chaton/e/b/j;->d:Ljava/lang/String;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/e/b/j;->e:Ljava/util/ArrayList;

    .line 35
    const/4 v0, 0x1

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 36
    iget-object v1, p0, Lcom/sec/chaton/e/b/j;->e:Ljava/util/ArrayList;

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 57
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 59
    iget v1, p0, Lcom/sec/chaton/e/b/j;->f:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 60
    iget-object v1, p0, Lcom/sec/chaton/e/b/j;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/f;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/net/Uri;

    .line 62
    iget-object v1, p0, Lcom/sec/chaton/e/b/j;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/f;->d(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/e/b/j;->c:I

    .line 65
    iget v1, p0, Lcom/sec/chaton/e/b/j;->c:I

    iget-object v2, p0, Lcom/sec/chaton/e/b/j;->e:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;ILjava/util/Collection;)V

    .line 67
    iget v0, p0, Lcom/sec/chaton/e/b/j;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    .line 69
    :cond_0
    iget v1, p0, Lcom/sec/chaton/e/b/j;->f:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 71
    iget v1, p0, Lcom/sec/chaton/e/b/j;->c:I

    if-nez v1, :cond_1

    .line 72
    iget-object v1, p0, Lcom/sec/chaton/e/b/j;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/f;->g(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_1
    iget v1, p0, Lcom/sec/chaton/e/b/j;->c:I

    iget-object v2, p0, Lcom/sec/chaton/e/b/j;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/f;->b(Landroid/content/ContentResolver;ILjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 76
    :cond_2
    iget v1, p0, Lcom/sec/chaton/e/b/j;->f:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_4

    .line 79
    iget v1, p0, Lcom/sec/chaton/e/b/j;->c:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/e/b/j;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 81
    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 82
    iget v1, p0, Lcom/sec/chaton/e/b/j;->c:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 84
    iget-object v1, p0, Lcom/sec/chaton/e/b/j;->e:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/e/b/j;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 86
    iget v1, p0, Lcom/sec/chaton/e/b/j;->c:I

    iget-object v2, p0, Lcom/sec/chaton/e/b/j;->e:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/e/a/f;->a(Landroid/content/ContentResolver;ILjava/util/Collection;)V

    .line 91
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/e/b/j;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
