.class public final enum Lcom/sec/chaton/e/r;
.super Ljava/lang/Enum;
.source "ChatONContract.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/e/r;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/e/r;

.field public static final enum b:Lcom/sec/chaton/e/r;

.field public static final enum c:Lcom/sec/chaton/e/r;

.field public static final enum d:Lcom/sec/chaton/e/r;

.field public static final enum e:Lcom/sec/chaton/e/r;

.field public static final enum f:Lcom/sec/chaton/e/r;

.field private static final synthetic h:[Lcom/sec/chaton/e/r;


# instance fields
.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 878
    new-instance v0, Lcom/sec/chaton/e/r;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/e/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    .line 879
    new-instance v0, Lcom/sec/chaton/e/r;

    const-string v1, "ONETOONE"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/e/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    .line 880
    new-instance v0, Lcom/sec/chaton/e/r;

    const-string v1, "GROUPCHAT"

    invoke-direct {v0, v1, v6, v5}, Lcom/sec/chaton/e/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    .line 881
    new-instance v0, Lcom/sec/chaton/e/r;

    const-string v1, "BROADCAST"

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/chaton/e/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    .line 882
    new-instance v0, Lcom/sec/chaton/e/r;

    const-string v1, "BROADCAST2"

    invoke-direct {v0, v1, v8, v7}, Lcom/sec/chaton/e/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    .line 883
    new-instance v0, Lcom/sec/chaton/e/r;

    const-string v1, "WEB_AUTH"

    const/4 v2, 0x5

    const/16 v3, 0x270f

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/e/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    .line 877
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/chaton/e/r;

    sget-object v1, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/e/r;->h:[Lcom/sec/chaton/e/r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 887
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 888
    iput p3, p0, Lcom/sec/chaton/e/r;->g:I

    .line 889
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/e/r;
    .locals 1

    .prologue
    .line 896
    sparse-switch p0, :sswitch_data_0

    .line 908
    sget-object v0, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    :goto_0
    return-object v0

    .line 898
    :sswitch_0
    sget-object v0, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    goto :goto_0

    .line 900
    :sswitch_1
    sget-object v0, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    goto :goto_0

    .line 902
    :sswitch_2
    sget-object v0, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    goto :goto_0

    .line 904
    :sswitch_3
    sget-object v0, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    goto :goto_0

    .line 906
    :sswitch_4
    sget-object v0, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    goto :goto_0

    .line 896
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x270f -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(Lcom/sec/chaton/e/r;)Z
    .locals 1

    .prologue
    .line 912
    sget-object v0, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-ne p0, v0, :cond_1

    .line 913
    :cond_0
    const/4 v0, 0x1

    .line 915
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/sec/chaton/e/r;)Z
    .locals 1

    .prologue
    .line 919
    sget-object v0, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-ne p0, v0, :cond_0

    .line 920
    const/4 v0, 0x1

    .line 922
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/e/r;
    .locals 1

    .prologue
    .line 877
    const-class v0, Lcom/sec/chaton/e/r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/e/r;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/e/r;
    .locals 1

    .prologue
    .line 877
    sget-object v0, Lcom/sec/chaton/e/r;->h:[Lcom/sec/chaton/e/r;

    invoke-virtual {v0}, [Lcom/sec/chaton/e/r;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/e/r;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 892
    iget v0, p0, Lcom/sec/chaton/e/r;->g:I

    return v0
.end method
