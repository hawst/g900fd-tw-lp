.class public abstract Lcom/sec/chaton/e/b/a;
.super Ljava/lang/Object;
.source "AbstractQueryTask.java"


# instance fields
.field public a:Landroid/os/Handler;

.field protected b:Z

.field private c:Lcom/sec/chaton/e/b/d;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/e/b/d;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/sec/chaton/e/b/a;->c:Lcom/sec/chaton/e/b/d;

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/e/b/a;->b:Z

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/e/b/d;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/sec/chaton/e/b/a;->c:Lcom/sec/chaton/e/b/d;

    .line 27
    iput-object p2, p0, Lcom/sec/chaton/e/b/a;->a:Landroid/os/Handler;

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/e/b/a;->b:Z

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/e/b/a;)Lcom/sec/chaton/e/b/d;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/chaton/e/b/a;->c:Lcom/sec/chaton/e/b/d;

    return-object v0
.end method


# virtual methods
.method public abstract a()Ljava/lang/Object;
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/chaton/e/b/a;->a()Ljava/lang/Object;

    move-result-object v1

    .line 40
    iget-object v0, p0, Lcom/sec/chaton/e/b/a;->c:Lcom/sec/chaton/e/b/d;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/chaton/e/b/a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/e/b/a;->a:Landroid/os/Handler;

    new-instance v2, Lcom/sec/chaton/e/b/b;

    invoke-direct {v2, p0, p1, v1}, Lcom/sec/chaton/e/b/b;-><init>(Lcom/sec/chaton/e/b/a;ILjava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/sec/chaton/e/b/a;->a:Landroid/os/Handler;

    new-instance v2, Lcom/sec/chaton/e/b/c;

    invoke-direct {v2, p0, p1, v1}, Lcom/sec/chaton/e/b/c;-><init>(Lcom/sec/chaton/e/b/a;ILjava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/e/b/a;->c:Lcom/sec/chaton/e/b/d;

    iget-boolean v2, p0, Lcom/sec/chaton/e/b/a;->b:Z

    invoke-interface {v0, p1, v2, v1}, Lcom/sec/chaton/e/b/d;->a(IZLjava/lang/Object;)V

    goto :goto_0
.end method
