.class public Lcom/sec/chaton/e/a/ab;
.super Lcom/sec/chaton/e/a/j;
.source "SkinDatabaseHelper.java"


# static fields
.field static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/chaton/e/a/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/a/ab;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/chaton/e/a/j;-><init>()V

    .line 192
    return-void
.end method

.method public static a(Lcom/sec/chaton/io/entry/inner/Skin;)Landroid/content/ContentProviderOperation;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 45
    const-string v1, "item_type"

    sget-object v2, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-virtual {v2}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v1, "item_id"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Skin;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-string v1, "new"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Skin;->newitem:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 48
    const-string v1, "special"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Skin;->special:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 49
    const-string v1, "expiration_time"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Skin;->expirationdate:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 50
    const-string v1, "extras"

    invoke-static {p0}, Lcom/sec/chaton/e/a/ab;->b(Lcom/sec/chaton/io/entry/inner/Skin;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/al;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/chaton/e/a/ac;
    .locals 4

    .prologue
    .line 73
    new-instance v1, Lcom/sec/chaton/e/a/ac;

    invoke-direct {v1}, Lcom/sec/chaton/e/a/ac;-><init>()V

    .line 75
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 77
    const-string v2, "bgtype"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/e/a/ac;->b(Ljava/lang/String;)V

    .line 78
    const-string v2, "thumbnailurl"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/e/a/ac;->a(Ljava/lang/String;)V

    .line 83
    :try_start_0
    const-string v2, "detailviewurl"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/e/a/ac;->c(Ljava/lang/String;)V

    .line 84
    const-string v2, "filesize"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/e/a/ac;->a(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :cond_0
    :goto_0
    return-object v1

    .line 85
    :catch_0
    move-exception v0

    .line 86
    const-string v2, "(empty)"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/e/a/ac;->c(Ljava/lang/String;)V

    .line 87
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/e/a/ac;->a(J)V

    .line 88
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_0

    .line 89
    sget-object v2, Lcom/sec/chaton/e/a/ab;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 128
    .line 131
    :try_start_0
    new-instance v4, Lcom/sec/chaton/io/entry/inner/Skin;

    invoke-direct {v4}, Lcom/sec/chaton/io/entry/inner/Skin;-><init>()V

    .line 132
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 134
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "SELECT * FROM "

    aput-object v7, v0, v6

    const/4 v6, 0x1

    const-string v7, "skin"

    aput-object v7, v0, v6

    const/4 v6, 0x2

    const-string v7, " WHERE "

    aput-object v7, v0, v6

    const/4 v6, 0x3

    const-string v7, "install"

    aput-object v7, v0, v6

    const/4 v6, 0x4

    const-string v7, " != ?"

    aput-object v7, v0, v6

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "0"

    aput-object v8, v6, v7

    invoke-virtual {p0, v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 136
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    const-string v0, "skin_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/sec/chaton/io/entry/inner/Skin;->id:Ljava/lang/String;

    .line 139
    const-string v0, "new"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Lcom/sec/chaton/io/entry/inner/Skin;->newitem:Ljava/lang/Boolean;

    .line 140
    const-string v0, "special"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/sec/chaton/io/entry/inner/Skin;->special:Ljava/lang/Integer;

    .line 141
    const-string v0, "thumbnail_url"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/sec/chaton/io/entry/inner/Skin;->thumbnailurl:Ljava/lang/String;

    .line 142
    const-string v0, "bg_type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/sec/chaton/io/entry/inner/Skin;->bgtype:Ljava/lang/String;

    .line 143
    const-string v0, "expiration_date"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/sec/chaton/io/entry/inner/Skin;->expirationdate:Ljava/lang/Long;

    .line 144
    const-string v0, "install"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 147
    const-string v0, "item_type"

    sget-object v8, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-virtual {v8}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v0, "item_id"

    iget-object v8, v4, Lcom/sec/chaton/io/entry/inner/Skin;->id:Ljava/lang/String;

    invoke-virtual {v5, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v0, "new"

    iget-object v8, v4, Lcom/sec/chaton/io/entry/inner/Skin;->newitem:Ljava/lang/Boolean;

    invoke-virtual {v5, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 150
    const-string v0, "special"

    iget-object v8, v4, Lcom/sec/chaton/io/entry/inner/Skin;->special:Ljava/lang/Integer;

    invoke-virtual {v5, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 151
    const-string v0, "extras"

    invoke-static {v4}, Lcom/sec/chaton/e/a/ab;->b(Lcom/sec/chaton/io/entry/inner/Skin;)Lorg/json/JSONObject;

    move-result-object v8

    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v0, "expiration_time"

    iget-object v8, v4, Lcom/sec/chaton/io/entry/inner/Skin;->expirationdate:Ljava/lang/Long;

    invoke-virtual {v5, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 153
    const-string v0, "install"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 156
    const-string v0, "download_item"

    const/4 v6, 0x0

    invoke-virtual {p0, v0, v6, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 159
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 160
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 159
    :cond_0
    throw v0

    :cond_1
    move v0, v3

    .line 139
    goto/16 :goto_1

    .line 159
    :cond_2
    if-eqz v1, :cond_3

    .line 160
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 163
    :cond_3
    return-void
.end method

.method public static b(Lcom/sec/chaton/io/entry/inner/Skin;)Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 104
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 106
    const-string v1, "bgtype"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Skin;->bgtype:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 107
    const-string v1, "thumbnailurl"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Skin;->thumbnailurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 108
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/Skin;->detailviewurl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    const-string v1, "detailviewurl"

    const-string v2, "(empty)"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 115
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/io/entry/inner/Skin;->volume:Ljava/lang/Long;

    if-nez v1, :cond_1

    .line 116
    const-string v1, "filesize"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 121
    :goto_1
    return-object v0

    .line 112
    :cond_0
    const-string v1, "detailviewurl"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Skin;->detailviewurl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 119
    :cond_1
    const-string v1, "filesize"

    iget-object v2, p0, Lcom/sec/chaton/io/entry/inner/Skin;->volume:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1
.end method

.method public static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 166
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 169
    new-instance v1, Lcom/sec/chaton/io/entry/inner/Skin;

    invoke-direct {v1}, Lcom/sec/chaton/io/entry/inner/Skin;-><init>()V

    .line 170
    const-string v2, "-5"

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->id:Ljava/lang/String;

    .line 171
    const-string v2, "pa"

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->bgtype:Ljava/lang/String;

    .line 172
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->newitem:Ljava/lang/Boolean;

    .line 173
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->special:Ljava/lang/Integer;

    .line 174
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "android.resource"

    aput-object v3, v2, v4

    const-string v3, "://"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "/drawable/default_preview_05"

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->thumbnailurl:Ljava/lang/String;

    .line 175
    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/chaton/io/entry/inner/Skin;->expirationdate:Ljava/lang/Long;

    .line 177
    const-string v2, "item_type"

    sget-object v3, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-virtual {v3}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v2, "item_id"

    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/Skin;->id:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v2, "new"

    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/Skin;->newitem:Ljava/lang/Boolean;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 180
    const-string v2, "special"

    iget-object v3, v1, Lcom/sec/chaton/io/entry/inner/Skin;->special:Ljava/lang/Integer;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 181
    const-string v2, "extras"

    invoke-static {v1}, Lcom/sec/chaton/e/a/ab;->b(Lcom/sec/chaton/io/entry/inner/Skin;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v2, "expiration_time"

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/Skin;->expirationdate:Ljava/lang/Long;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 183
    const-string v1, "install"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 185
    const-string v1, "download_item"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 187
    return-void
.end method
