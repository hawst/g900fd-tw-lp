.class public interface abstract Lcom/sec/chaton/e/bb;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0xc

    .line 2301
    const/16 v0, 0xb3

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v4

    const-string v1, "SELECT "

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, ","

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "inbox_chat_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "inbox_last_message"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "inbox_title"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ","

    aput-object v2, v0, v1

    const-string v1, "inbox_title_fixed"

    aput-object v1, v0, v3

    const/16 v1, 0xd

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "inbox_last_time"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "inbox_lang_from"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "inbox_unread_count"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "inbox_lang_to"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "inbox_translated"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "inbox_server_ip"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "inbox_server_port"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "inbox_participants"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "inbox_session_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "inbox_last_msg_no"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "inbox_last_msg_sender"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "inbox_last_temp_msg"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "inbox_last_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "inbox_is_new"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "inbox_trunk_unread_count"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "inbox_valid"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "inbox_enable_noti"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "inbox_is_change_skin"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "inbox_background_style"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "inbox_send_bubble_style"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "inbox_receive_bubble_style"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "inbox_last_tid"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "inbox_enable_translate"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "translate_outgoing_message"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "inbox_translate_my_language"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "inbox_translate_buddy_language"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "inbox_is_entered"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "inbox_last_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "inbox_web_url"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "weburl"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "lasst_session_merge_time"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "inbox"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, " a"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "weburl"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "msgstatus"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "weburl"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "specialbuddy"

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "buddy_status_message"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "\'\'"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "weburl"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, " NOT IN "

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, "(SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "specialbuddy"

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, " = ( SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, "inbox_buddy_relation"

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, " ) "

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bb;->a:Ljava/lang/String;

    .line 2448
    const/16 v0, 0x60

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "a.*, "

    aput-object v1, v0, v5

    const-string v1, "r."

    aput-object v1, v0, v6

    const-string v1, "buddy_no"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "relation_buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "\tFROM ( "

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "j."

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "j."

    aput-object v2, v0, v1

    const-string v1, "buddy_name"

    aput-object v1, v0, v3

    const/16 v1, 0xd

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "i.*"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "inbox"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, " i"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "( "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, " p"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, " b "

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, " NOT null"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, " ) j "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "j."

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "inbox_last_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " != "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "inbox_valid"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "\' "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, " != ?"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "inbox_web_url"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, " IS null"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " ) a "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "inbox_buddy_relation"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, " r "

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "r."

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, " ORDER BY "

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "inbox_last_time"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " DESC, a._id"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bb;->b:Ljava/lang/String;

    .line 2457
    const/16 v0, 0x61

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SELECT "

    aput-object v1, v0, v4

    const-string v1, "a.*, "

    aput-object v1, v0, v5

    const-string v1, "r."

    aput-object v1, v0, v6

    const-string v1, "buddy_no"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "relation_buddy_no"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "\tFROM ( "

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "j."

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "j."

    aput-object v2, v0, v1

    const-string v1, "buddy_name"

    aput-object v1, v0, v3

    const/16 v1, 0xd

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "i.*"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "inbox"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, " i"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "( "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, " p"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, " b "

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, " NOT null"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, " ) j "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "j."

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "inbox_last_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " != "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "inbox_valid"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " = \'"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "\' "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "inbox_web_url"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, " IS null"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " AND "

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "inbox_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, " != "

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "i."

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, " ) a "

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "inbox_buddy_relation"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, " r "

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, " = "

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "r."

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, " ORDER BY "

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "inbox_last_time"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, " DESC, a._id DESC"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bb;->c:Ljava/lang/String;

    .line 2465
    const/16 v0, 0x6d

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "("

    aput-object v1, v0, v4

    const-string v1, "SELECT "

    aput-object v1, v0, v5

    const-string v1, "c."

    aput-object v1, v0, v6

    const-string v1, "participants_buddy_name"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "inbox_title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ","

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "inbox"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, " a,"

    aput-object v2, v0, v1

    const-string v1, "("

    aput-object v1, v0, v3

    const/16 v1, 0xd

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "group_concat("

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, ", \", \") AS "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "ifnull("

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "MAX("

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, ")) AS "

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "participants_buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "rowid"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "participant"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, " p"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, " LEFT OUTER JOIN "

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "( "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "buddy"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, " UNION ALL "

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "SELECT "

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, ", "

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, " AS "

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "buddy_name"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, " FROM "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "specialbuddy"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, " )"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " b"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, " ON "

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "b."

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "participants_buddy_no"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, " ORDER BY "

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "p."

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "rowid"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, " DESC"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, ") "

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, " GROUP BY "

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, ") c"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, " WHERE "

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "c."

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "participants_inbox_no"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, " AND ("

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "inbox_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, " OR "

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "inbox_chat_type"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, ") AND "

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "a."

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "inbox_title_fixed"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "\'"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "N"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "\'"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/e/bb;->d:Ljava/lang/String;

    return-void
.end method
