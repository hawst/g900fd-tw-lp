.class public Lcom/sec/chaton/util/aq;
.super Ljava/lang/Object;
.source "HFrameworkUtil.java"


# static fields
.field private static volatile a:Lcom/samsung/android/sdk/look/Slook;


# direct methods
.method public static a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 31
    invoke-static {}, Lcom/sec/chaton/util/aq;->b()Lcom/samsung/android/sdk/look/Slook;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/look/Slook;->getVersionCode()I

    move-result v1

    if-lt v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized b()Lcom/samsung/android/sdk/look/Slook;
    .locals 3

    .prologue
    .line 20
    const-class v1, Lcom/sec/chaton/util/aq;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/util/aq;->a:Lcom/samsung/android/sdk/look/Slook;

    if-nez v0, :cond_1

    .line 21
    const-class v2, Lcom/samsung/android/sdk/look/Slook;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 22
    :try_start_1
    sget-object v0, Lcom/sec/chaton/util/aq;->a:Lcom/samsung/android/sdk/look/Slook;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/look/Slook;

    invoke-direct {v0}, Lcom/samsung/android/sdk/look/Slook;-><init>()V

    sput-object v0, Lcom/sec/chaton/util/aq;->a:Lcom/samsung/android/sdk/look/Slook;

    .line 25
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 27
    :cond_1
    :try_start_2
    sget-object v0, Lcom/sec/chaton/util/aq;->a:Lcom/samsung/android/sdk/look/Slook;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v1

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 20
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
