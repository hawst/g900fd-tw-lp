.class public Lcom/sec/chaton/util/aa;
.super Ljava/lang/Object;
.source "ChatONPref.java"


# static fields
.field private static a:Lcom/sec/chaton/util/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/util/aa;->a:Lcom/sec/chaton/util/ab;

    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/util/ab;
    .locals 2

    .prologue
    .line 26
    const-class v1, Lcom/sec/chaton/util/aa;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/util/aa;->a:Lcom/sec/chaton/util/ab;

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Lcom/sec/chaton/util/ab;

    invoke-direct {v0}, Lcom/sec/chaton/util/ab;-><init>()V

    sput-object v0, Lcom/sec/chaton/util/aa;->a:Lcom/sec/chaton/util/ab;

    .line 29
    :cond_0
    sget-object v0, Lcom/sec/chaton/util/aa;->a:Lcom/sec/chaton/util/ab;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 65
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 74
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 56
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public static declared-synchronized b()V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lcom/sec/chaton/util/aa;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-object v1, Lcom/sec/chaton/util/aa;->a:Lcom/sec/chaton/util/ab;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    monitor-exit v0

    return-void

    .line 33
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
