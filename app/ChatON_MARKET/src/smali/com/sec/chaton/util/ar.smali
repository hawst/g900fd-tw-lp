.class public Lcom/sec/chaton/util/ar;
.super Landroid/os/Handler;
.source "HandlerTimeOut.java"


# instance fields
.field b:Z

.field final c:I

.field final d:I

.field final e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 22
    const/16 v0, -0x270f

    iput v0, p0, Lcom/sec/chaton/util/ar;->c:I

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/util/ar;->d:I

    .line 29
    const-class v0, Lcom/sec/chaton/util/ar;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/util/ar;->e:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "startTimer : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/util/ar;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_0
    iput-boolean v2, p0, Lcom/sec/chaton/util/ar;->b:Z

    .line 55
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 56
    const/16 v1, -0x270f

    iput v1, v0, Landroid/os/Message;->what:I

    .line 57
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 58
    const/4 v1, -0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 59
    int-to-long v1, p1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/util/ar;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 60
    return-void
.end method

.method public a(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "startTimer : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", what : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/util/ar;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :cond_0
    iput-boolean v2, p0, Lcom/sec/chaton/util/ar;->b:Z

    .line 71
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 72
    const/16 v1, -0x270f

    iput v1, v0, Landroid/os/Message;->what:I

    .line 73
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 74
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 75
    int-to-long v1, p2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/util/ar;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 76
    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/16 v2, -0x270f

    .line 81
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "didTimeOut "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/util/ar;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , what : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", arg1 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/util/ar;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v2, :cond_4

    .line 86
    iget-boolean v0, p0, Lcom/sec/chaton/util/ar;->b:Z

    if-eqz v0, :cond_2

    .line 88
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 89
    const-string v0, "Already time out, so ignore reponse about original msg"

    iget-object v1, p0, Lcom/sec/chaton/util/ar;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_1
    :goto_0
    return-void

    .line 93
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 94
    const-string v0, "Excepted msg was came, so cancel to timer"

    iget-object v1, p0, Lcom/sec/chaton/util/ar;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/chaton/util/ar;->removeMessages(I)V

    .line 97
    invoke-virtual {p0, p1}, Lcom/sec/chaton/util/ar;->a(Landroid/os/Message;)V

    goto :goto_0

    .line 100
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/util/ar;->b:Z

    .line 101
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 102
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p1, Landroid/os/Message;->what:I

    .line 104
    :cond_5
    invoke-virtual {p0, p1}, Lcom/sec/chaton/util/ar;->a(Landroid/os/Message;)V

    goto :goto_0
.end method
