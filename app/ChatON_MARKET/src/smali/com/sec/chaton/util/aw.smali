.class public Lcom/sec/chaton/util/aw;
.super Ljava/lang/Object;
.source "ImageDownloader.java"


# static fields
.field private static a:Lcom/sec/chaton/util/aw;

.field private static final b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/os/Handler;

.field private final g:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    sput-object v0, Lcom/sec/chaton/util/aw;->b:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/util/aw;->c:Ljava/lang/String;

    .line 75
    new-instance v0, Lcom/sec/chaton/util/ax;

    const/4 v1, 0x2

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/chaton/util/ax;-><init>(Lcom/sec/chaton/util/aw;IFZ)V

    iput-object v0, p0, Lcom/sec/chaton/util/aw;->e:Ljava/util/HashMap;

    .line 329
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/util/aw;->f:Landroid/os/Handler;

    .line 331
    new-instance v0, Lcom/sec/chaton/util/ay;

    invoke-direct {v0, p0}, Lcom/sec/chaton/util/ay;-><init>(Lcom/sec/chaton/util/aw;)V

    iput-object v0, p0, Lcom/sec/chaton/util/aw;->g:Ljava/lang/Runnable;

    .line 92
    return-void
.end method

.method public static a()Lcom/sec/chaton/util/aw;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/sec/chaton/util/aw;->a:Lcom/sec/chaton/util/aw;

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Lcom/sec/chaton/util/aw;

    invoke-direct {v0}, Lcom/sec/chaton/util/aw;-><init>()V

    .line 98
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/chaton/util/aw;->a:Lcom/sec/chaton/util/aw;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/util/bb;)V
    .locals 3

    .prologue
    .line 189
    if-nez p1, :cond_0

    .line 194
    :goto_0
    return-void

    .line 192
    :cond_0
    new-instance v0, Lcom/sec/chaton/util/az;

    invoke-direct {v0, p0, p3}, Lcom/sec/chaton/util/az;-><init>(Lcom/sec/chaton/util/aw;Lcom/sec/chaton/util/bb;)V

    .line 193
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/az;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic c()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/chaton/util/aw;->b:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/chaton/util/aw;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/util/aw;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/util/aw;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/util/aw;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 341
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)Lcom/sec/chaton/a/a/j;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 198
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-static {}, Lcom/sec/chaton/j/c;->b()Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v0

    .line 202
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "execute to : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->getURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ImageDownloader"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-interface {v0, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 204
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 205
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "execute end, status code : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ImageDownloader"

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const/16 v5, 0xc8

    if-ne v2, v5, :cond_8

    .line 207
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    .line 208
    if-eqz v5, :cond_d

    .line 211
    :try_start_1
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v2

    .line 216
    :try_start_2
    new-instance v1, Lcom/sec/chaton/util/ae;

    invoke-direct {v1, v2}, Lcom/sec/chaton/util/ae;-><init>(Ljava/io/InputStream;)V

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    .line 221
    :try_start_3
    iget-object v1, p0, Lcom/sec/chaton/util/aw;->c:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 222
    iget-object v1, p0, Lcom/sec/chaton/util/aw;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 224
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 225
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 226
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/chaton/util/aw;->c:Ljava/lang/String;

    invoke-direct {v7, v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/chaton/util/aw;->d:Ljava/lang/String;

    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x64

    invoke-static {v6, v7, v1, v8, v9}, Lcom/sec/chaton/util/ad;->a(Landroid/graphics/Bitmap;Ljava/io/File;Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;I)V

    .line 241
    :cond_0
    :goto_0
    new-instance v1, Lcom/sec/chaton/a/a/j;

    sget-object v7, Lcom/sec/chaton/util/ba;->a:Lcom/sec/chaton/util/ba;

    invoke-direct {v1, v6, v7}, Lcom/sec/chaton/a/a/j;-><init>(Landroid/graphics/Bitmap;Lcom/sec/chaton/util/ba;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 243
    if-eqz v2, :cond_1

    .line 244
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 246
    :cond_1
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 264
    instance-of v2, v0, Landroid/net/http/AndroidHttpClient;

    if-eqz v2, :cond_2

    .line 265
    check-cast v0, Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_2
    move-object v0, v1

    .line 268
    :goto_1
    return-object v0

    .line 217
    :catch_0
    move-exception v1

    .line 218
    :try_start_5
    new-instance v1, Lcom/sec/chaton/a/a/j;

    const/4 v6, 0x0

    sget-object v7, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    invoke-direct {v1, v6, v7}, Lcom/sec/chaton/a/a/j;-><init>(Landroid/graphics/Bitmap;Lcom/sec/chaton/util/ba;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 243
    if-eqz v2, :cond_3

    .line 244
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 246
    :cond_3
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 264
    instance-of v2, v0, Landroid/net/http/AndroidHttpClient;

    if-eqz v2, :cond_4

    .line 265
    check-cast v0, Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_4
    move-object v0, v1

    .line 218
    goto :goto_1

    .line 231
    :cond_5
    :try_start_7
    iget-object v1, p0, Lcom/sec/chaton/util/aw;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 233
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234
    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    .line 235
    new-instance v7, Ljava/io/File;

    const-string v8, "/profilehistory/"

    invoke-direct {v7, v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/chaton/util/aw;->d:Ljava/lang/String;

    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x64

    invoke-static {v6, v7, v1, v8, v9}, Lcom/sec/chaton/util/ad;->a(Landroid/graphics/Bitmap;Ljava/io/File;Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 243
    :catchall_0
    move-exception v1

    :goto_2
    if-eqz v2, :cond_6

    .line 244
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 246
    :cond_6
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 243
    throw v1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 254
    :catch_1
    move-exception v1

    .line 255
    :try_start_9
    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "I/O error while retrieving bitmap from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ImageDownloader"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 264
    instance-of v1, v0, Landroid/net/http/AndroidHttpClient;

    if-eqz v1, :cond_7

    .line 265
    check-cast v0, Landroid/net/http/AndroidHttpClient;

    :goto_3
    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 268
    :cond_7
    new-instance v0, Lcom/sec/chaton/a/a/j;

    sget-object v1, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    invoke-direct {v0, v3, v1}, Lcom/sec/chaton/a/a/j;-><init>(Landroid/graphics/Bitmap;Lcom/sec/chaton/util/ba;)V

    goto :goto_1

    .line 249
    :cond_8
    const/16 v1, 0xcc

    if-ne v2, v1, :cond_a

    .line 250
    :try_start_a
    new-instance v1, Lcom/sec/chaton/a/a/j;

    const/4 v2, 0x0

    sget-object v5, Lcom/sec/chaton/util/ba;->b:Lcom/sec/chaton/util/ba;

    invoke-direct {v1, v2, v5}, Lcom/sec/chaton/a/a/j;-><init>(Landroid/graphics/Bitmap;Lcom/sec/chaton/util/ba;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 264
    instance-of v2, v0, Landroid/net/http/AndroidHttpClient;

    if-eqz v2, :cond_9

    .line 265
    check-cast v0, Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_9
    move-object v0, v1

    .line 250
    goto/16 :goto_1

    .line 252
    :cond_a
    :try_start_b
    new-instance v1, Lcom/sec/chaton/a/a/j;

    const/4 v2, 0x0

    sget-object v5, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    invoke-direct {v1, v2, v5}, Lcom/sec/chaton/a/a/j;-><init>(Landroid/graphics/Bitmap;Lcom/sec/chaton/util/ba;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 264
    instance-of v2, v0, Landroid/net/http/AndroidHttpClient;

    if-eqz v2, :cond_b

    .line 265
    check-cast v0, Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_b
    move-object v0, v1

    .line 252
    goto/16 :goto_1

    .line 257
    :catch_2
    move-exception v1

    .line 258
    :try_start_c
    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Incorrect URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ImageDownloader"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 264
    instance-of v1, v0, Landroid/net/http/AndroidHttpClient;

    if-eqz v1, :cond_7

    .line 265
    check-cast v0, Landroid/net/http/AndroidHttpClient;

    goto :goto_3

    .line 260
    :catch_3
    move-exception v1

    .line 261
    :try_start_d
    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 262
    const-string v1, "Error while retrieving bitmap"

    const-string v2, "ImageDownloader"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 264
    instance-of v1, v0, Landroid/net/http/AndroidHttpClient;

    if-eqz v1, :cond_7

    .line 265
    check-cast v0, Landroid/net/http/AndroidHttpClient;

    goto :goto_3

    .line 264
    :catchall_1
    move-exception v1

    instance-of v2, v0, Landroid/net/http/AndroidHttpClient;

    if-eqz v2, :cond_c

    .line 265
    check-cast v0, Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 264
    :cond_c
    throw v1

    :cond_d
    instance-of v1, v0, Landroid/net/http/AndroidHttpClient;

    if-eqz v1, :cond_7

    .line 265
    check-cast v0, Landroid/net/http/AndroidHttpClient;

    goto :goto_3

    .line 243
    :catchall_2
    move-exception v1

    move-object v2, v3

    goto/16 :goto_2
.end method

.method public a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 276
    if-eqz p2, :cond_0

    .line 277
    iget-object v1, p0, Lcom/sec/chaton/util/aw;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 278
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/util/aw;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    monitor-exit v1

    .line 281
    :cond_0
    return-void

    .line 279
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;Lcom/sec/chaton/util/bb;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 110
    iput-object p3, p0, Lcom/sec/chaton/util/aw;->c:Ljava/lang/String;

    .line 111
    iput-object p4, p0, Lcom/sec/chaton/util/aw;->d:Ljava/lang/String;

    .line 113
    invoke-direct {p0}, Lcom/sec/chaton/util/aw;->d()V

    .line 114
    invoke-virtual {p0, p2}, Lcom/sec/chaton/util/aw;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 116
    if-nez v0, :cond_1

    .line 117
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/profile/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/util/aw;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 122
    invoke-virtual {p0, p2, v0}, Lcom/sec/chaton/util/aw;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 123
    const-string v1, "Profile image, use exist image"

    const-string v2, "ImageDownloader"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    sget-object v1, Lcom/sec/chaton/util/ba;->a:Lcom/sec/chaton/util/ba;

    invoke-interface {p6, p1, v0, v1}, Lcom/sec/chaton/util/bb;->a(Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/chaton/util/ba;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 148
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    const-string v1, "ImageDownloader"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 127
    sget-object v0, Lcom/sec/chaton/util/ba;->a:Lcom/sec/chaton/util/ba;

    invoke-interface {p6, p1, v3, v0}, Lcom/sec/chaton/util/bb;->a(Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/chaton/util/ba;)V

    goto :goto_0

    .line 128
    :catch_1
    move-exception v0

    .line 129
    const-string v1, "ImageDownloader"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 130
    sget-object v0, Lcom/sec/chaton/util/ba;->a:Lcom/sec/chaton/util/ba;

    invoke-interface {p6, p1, v3, v0}, Lcom/sec/chaton/util/bb;->a(Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/chaton/util/ba;)V

    goto :goto_0

    .line 141
    :cond_0
    const-string v0, "Profile image, download from Server"

    const-string v1, "ImageDownloader"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-direct {p0, p2, p1, p6}, Lcom/sec/chaton/util/aw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/util/bb;)V

    goto :goto_0

    .line 145
    :cond_1
    const-string v1, "Profile image, use from cache"

    const-string v2, "ImageDownloader"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    sget-object v1, Lcom/sec/chaton/util/ba;->a:Lcom/sec/chaton/util/ba;

    invoke-interface {p6, p1, v0, v1}, Lcom/sec/chaton/util/bb;->a(Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/chaton/util/ba;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 289
    iget-object v1, p0, Lcom/sec/chaton/util/aw;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 290
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/util/aw;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 291
    if-eqz v0, :cond_1

    .line 292
    iget-object v2, p0, Lcom/sec/chaton/util/aw;->e:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    iget-object v2, p0, Lcom/sec/chaton/util/aw;->e:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    monitor-exit v1

    .line 309
    :cond_0
    :goto_0
    return-object v0

    .line 296
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    sget-object v0, Lcom/sec/chaton/util/aw;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 300
    if-eqz v0, :cond_2

    .line 301
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 302
    if-nez v0, :cond_0

    .line 305
    sget-object v0, Lcom/sec/chaton/util/aw;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 296
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 317
    iget-object v1, p0, Lcom/sec/chaton/util/aw;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 319
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/util/aw;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 321
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    sget-object v0, Lcom/sec/chaton/util/aw;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 324
    return-void

    .line 321
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
