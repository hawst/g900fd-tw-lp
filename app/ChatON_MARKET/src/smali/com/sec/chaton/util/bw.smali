.class public final enum Lcom/sec/chaton/util/bw;
.super Ljava/lang/Enum;
.source "ProfileImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/util/bw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/util/bw;

.field public static final enum b:Lcom/sec/chaton/util/bw;

.field public static final enum c:Lcom/sec/chaton/util/bw;

.field public static final enum d:Lcom/sec/chaton/util/bw;

.field private static final synthetic e:[Lcom/sec/chaton/util/bw;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    new-instance v0, Lcom/sec/chaton/util/bw;

    const-string v1, "BUDDY"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/util/bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/util/bw;->a:Lcom/sec/chaton/util/bw;

    .line 80
    new-instance v0, Lcom/sec/chaton/util/bw;

    const-string v1, "BROADCAST"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/util/bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/util/bw;->b:Lcom/sec/chaton/util/bw;

    .line 81
    new-instance v0, Lcom/sec/chaton/util/bw;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/util/bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/util/bw;->c:Lcom/sec/chaton/util/bw;

    .line 82
    new-instance v0, Lcom/sec/chaton/util/bw;

    const-string v1, "CHAT_GROUP"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/util/bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/util/bw;->d:Lcom/sec/chaton/util/bw;

    .line 78
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/util/bw;

    sget-object v1, Lcom/sec/chaton/util/bw;->a:Lcom/sec/chaton/util/bw;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/util/bw;->b:Lcom/sec/chaton/util/bw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/util/bw;->c:Lcom/sec/chaton/util/bw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/util/bw;->d:Lcom/sec/chaton/util/bw;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/chaton/util/bw;->e:[Lcom/sec/chaton/util/bw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/util/bw;
    .locals 1

    .prologue
    .line 78
    const-class v0, Lcom/sec/chaton/util/bw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/util/bw;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/util/bw;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/sec/chaton/util/bw;->e:[Lcom/sec/chaton/util/bw;

    invoke-virtual {v0}, [Lcom/sec/chaton/util/bw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/util/bw;

    return-object v0
.end method
