.class Lcom/sec/chaton/util/by;
.super Landroid/os/HandlerThread;
.source "ProfileImageLoader.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final synthetic a:Lcom/sec/chaton/util/bt;

.field private final b:Landroid/content/Context;

.field private c:Landroid/os/Handler;

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/util/bz;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/util/bz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/chaton/util/bt;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 722
    iput-object p1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    .line 723
    const-string v0, "ProfileDownloader"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 719
    invoke-static {}, Lcom/sec/chaton/util/ag;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/util/by;->d:Ljava/util/ArrayList;

    .line 720
    invoke-static {}, Lcom/sec/chaton/util/ag;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/util/by;->e:Ljava/util/ArrayList;

    .line 724
    iput-object p2, p0, Lcom/sec/chaton/util/by;->b:Landroid/content/Context;

    .line 725
    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const v2, 0x7f0201bb

    .line 745
    iget-object v0, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    iget-object v1, p0, Lcom/sec/chaton/util/by;->d:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/chaton/util/by;->e:Ljava/util/ArrayList;

    invoke-static {v0, v1, v3}, Lcom/sec/chaton/util/bt;->a(Lcom/sec/chaton/util/bt;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 748
    iget-object v0, p0, Lcom/sec/chaton/util/by;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/util/bz;

    .line 753
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->b()I

    move-result v1

    sget-object v4, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->GROUP_IMAGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v4}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v4

    if-ne v1, v4, :cond_0

    .line 754
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    iget-object v4, p0, Lcom/sec/chaton/util/by;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/sec/chaton/util/bt;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 765
    :goto_1
    if-eqz v1, :cond_3

    .line 766
    iget-object v4, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 767
    iget-object v1, p0, Lcom/sec/chaton/util/by;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 755
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->b()I

    move-result v1

    sget-object v4, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->ME_IMAGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v4}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v4

    if-ne v1, v4, :cond_1

    .line 756
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    iget-object v4, p0, Lcom/sec/chaton/util/by;->b:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/sec/chaton/util/bt;->a(Lcom/sec/chaton/util/bt;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 758
    :cond_1
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->c()Lcom/sec/chaton/e/r;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    if-eq v1, v4, :cond_2

    .line 759
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    iget-object v4, p0, Lcom/sec/chaton/util/by;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->c()Lcom/sec/chaton/e/r;

    move-result-object v6

    invoke-static {v1, v4, v5, v6}, Lcom/sec/chaton/util/bt;->a(Lcom/sec/chaton/util/bt;Landroid/content/Context;Ljava/lang/String;Lcom/sec/chaton/e/r;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 761
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    iget-object v4, p0, Lcom/sec/chaton/util/by;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 772
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->b()I

    move-result v1

    sget-object v4, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NOT_CHANGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v4}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v4

    if-eq v1, v4, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->b()I

    move-result v1

    sget-object v4, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_DELETED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v4}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v4

    if-eq v1, v4, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->b()I

    move-result v1

    sget-object v4, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_UPDATED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v4}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v4

    if-ne v1, v4, :cond_b

    .line 774
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->c()Lcom/sec/chaton/e/r;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    if-eq v1, v4, :cond_6

    .line 775
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->c()Lcom/sec/chaton/e/r;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v4, v5, v6}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;Ljava/lang/String;)Lcom/sec/chaton/a/a/j;

    move-result-object v1

    .line 781
    :goto_2
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/j;->b()Lcom/sec/chaton/util/ba;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    if-eq v4, v5, :cond_8

    .line 782
    invoke-virtual {v1}, Lcom/sec/chaton/a/a/j;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 783
    if-eqz v1, :cond_5

    .line 784
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->c()Lcom/sec/chaton/e/r;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-ne v4, v5, :cond_7

    .line 785
    iget-object v4, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcom/sec/chaton/util/bt;->c(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 790
    :cond_5
    :goto_3
    iget-object v4, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 791
    iget-object v1, p0, Lcom/sec/chaton/util/by;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 777
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/chaton/util/bt;->g(Ljava/lang/String;)Lcom/sec/chaton/a/a/j;

    move-result-object v1

    goto :goto_2

    .line 787
    :cond_7
    iget-object v4, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcom/sec/chaton/util/bt;->b(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    goto :goto_3

    .line 795
    :cond_8
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->c()Lcom/sec/chaton/e/r;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    if-eq v1, v4, :cond_9

    .line 796
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/bt;->a(Lcom/sec/chaton/util/bt;Ljava/lang/String;)V

    .line 833
    :cond_9
    :goto_4
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 834
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    .line 835
    const-string v4, ""

    .line 836
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 837
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/e/a/f;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 838
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "_group_profile.png_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 839
    if-eqz v5, :cond_a

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 841
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/util/by;->b:Landroid/content/Context;

    const/high16 v4, 0x425c0000    # 55.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    const/high16 v6, 0x425c0000    # 55.0f

    invoke-static {v6}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v6

    float-to-int v6, v6

    invoke-static {v1, v5, v4, v6}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 843
    iget-object v4, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 844
    iget-object v1, p0, Lcom/sec/chaton/util/by;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 847
    :catch_0
    move-exception v1

    .line 848
    sget-boolean v4, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v4, :cond_a

    .line 849
    const-string v4, "ProfileImageLoader"

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 857
    :cond_a
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->c()Lcom/sec/chaton/e/r;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/e/r;->a:Lcom/sec/chaton/e/r;

    if-eq v1, v4, :cond_12

    .line 859
    sget-object v1, Lcom/sec/chaton/util/bv;->b:[I

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->c()Lcom/sec/chaton/e/r;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/chaton/e/r;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    move v1, v2

    .line 873
    :goto_5
    iget-object v4, p0, Lcom/sec/chaton/util/by;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 874
    if-eqz v1, :cond_12

    .line 875
    iget-object v4, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 876
    iget-object v1, p0, Lcom/sec/chaton/util/by;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 800
    :cond_b
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->b()I

    move-result v1

    sget-object v4, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->GROUP_IMAGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v4}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v4

    if-ne v1, v4, :cond_e

    .line 801
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/chaton/util/bt;->f(Ljava/lang/String;)Lcom/sec/chaton/a/a/j;

    move-result-object v1

    .line 802
    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/j;->b()Lcom/sec/chaton/util/ba;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    if-eq v4, v5, :cond_d

    .line 803
    invoke-virtual {v1}, Lcom/sec/chaton/a/a/j;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 804
    if-eqz v1, :cond_c

    .line 805
    iget-object v4, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcom/sec/chaton/util/bt;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 807
    :cond_c
    iget-object v4, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 808
    iget-object v1, p0, Lcom/sec/chaton/util/by;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 812
    :cond_d
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/bt;->a(Lcom/sec/chaton/util/bt;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 814
    :cond_e
    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->b()I

    move-result v1

    sget-object v4, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->ME_IMAGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v4}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v4

    if-ne v1, v4, :cond_11

    .line 815
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v1}, Lcom/sec/chaton/util/bt;->d()Lcom/sec/chaton/a/a/j;

    move-result-object v1

    .line 816
    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/j;->b()Lcom/sec/chaton/util/ba;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    if-eq v4, v5, :cond_10

    .line 817
    invoke-virtual {v1}, Lcom/sec/chaton/a/a/j;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 818
    if-eqz v1, :cond_f

    .line 819
    iget-object v4, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v4, v1}, Lcom/sec/chaton/util/bt;->a(Landroid/graphics/Bitmap;)V

    .line 821
    :cond_f
    iget-object v4, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 822
    iget-object v1, p0, Lcom/sec/chaton/util/by;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 826
    :cond_10
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/chaton/util/bt;->a(Lcom/sec/chaton/util/bt;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 829
    :cond_11
    iget-object v1, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-virtual {v0}, Lcom/sec/chaton/util/bz;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/sec/chaton/util/bt;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto/16 :goto_4

    :pswitch_0
    move v1, v2

    .line 862
    goto/16 :goto_5

    .line 865
    :pswitch_1
    const v1, 0x7f0201bc

    .line 866
    goto/16 :goto_5

    .line 870
    :pswitch_2
    const v1, 0x7f020151

    goto/16 :goto_5

    .line 880
    :cond_12
    iget-object v1, p0, Lcom/sec/chaton/util/by;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 882
    :cond_13
    return-void

    .line 859
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 728
    iget-object v0, p0, Lcom/sec/chaton/util/by;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 729
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/sec/chaton/util/by;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/chaton/util/by;->c:Landroid/os/Handler;

    .line 731
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/util/by;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 732
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2

    .prologue
    .line 736
    invoke-direct {p0}, Lcom/sec/chaton/util/by;->b()V

    .line 737
    iget-object v0, p0, Lcom/sec/chaton/util/by;->a:Lcom/sec/chaton/util/bt;

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Lcom/sec/chaton/util/bt;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 738
    const/4 v0, 0x1

    return v0
.end method
