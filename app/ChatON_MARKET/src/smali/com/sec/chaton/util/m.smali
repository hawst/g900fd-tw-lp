.class public Lcom/sec/chaton/util/m;
.super Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;
.source "ChatONAirButtonAdapter.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/e/w;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/util/n;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/chaton/util/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/util/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/e/w;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    .line 42
    iput-object p1, p0, Lcom/sec/chaton/util/m;->b:Lcom/sec/chaton/e/w;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/util/m;->d:Ljava/util/HashMap;

    .line 44
    return-void
.end method

.method private b()V
    .locals 15

    .prologue
    const-wide/16 v13, 0x0

    const/4 v11, 0x7

    const/4 v3, 0x0

    const/4 v9, 0x0

    .line 82
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "recentused"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "anicon_id"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 86
    if-eqz v8, :cond_2

    .line 87
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, v11, :cond_2

    .line 88
    const-string v0, "anicon_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 90
    if-eqz v3, :cond_0

    .line 91
    iget-object v10, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/util/n;

    sget-object v2, Lcom/sec/chaton/util/o;->b:Lcom/sec/chaton/util/o;

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/util/n;-><init>(Lcom/sec/chaton/util/m;Lcom/sec/chaton/util/o;Ljava/lang/String;IJI)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_1

    .line 98
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 97
    :cond_1
    throw v0

    :cond_2
    if-eqz v8, :cond_3

    .line 98
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 102
    :cond_3
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/h;->b()Lcom/sec/common/f/b;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lcom/sec/common/f/b;->a()[Ljava/lang/String;

    move-result-object v10

    .line 105
    array-length v0, v10

    invoke-static {v11, v0}, Ljava/lang/Math;->min(II)I

    move-result v11

    move v8, v9

    .line 107
    :goto_1
    if-ge v8, v11, :cond_5

    .line 108
    aget-object v0, v10, v8

    invoke-static {v0}, Lcom/sec/chaton/multimedia/emoticon/h;->a(Ljava/lang/String;)I

    move-result v4

    .line 109
    if-lez v4, :cond_4

    .line 110
    iget-object v12, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/util/n;

    sget-object v2, Lcom/sec/chaton/util/o;->c:Lcom/sec/chaton/util/o;

    aget-object v3, v10, v8

    move-object v1, p0

    move-wide v5, v13

    move v7, v9

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/util/n;-><init>(Lcom/sec/chaton/util/m;Lcom/sec/chaton/util/o;Ljava/lang/String;IJI)V

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    :cond_4
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 114
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/util/m;->e:I

    .line 116
    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 122
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v1

    const/4 v0, 0x1

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v1, "orientation"

    aput-object v1, v2, v0

    .line 123
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "datetaken DESC LIMIT 15"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 127
    if-eqz v8, :cond_1

    .line 128
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    const-string v0, "_data"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 130
    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 131
    const-string v0, "orientation"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 133
    iget-object v9, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/util/n;

    sget-object v2, Lcom/sec/chaton/util/o;->a:Lcom/sec/chaton/util/o;

    const/4 v4, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/util/n;-><init>(Lcom/sec/chaton/util/m;Lcom/sec/chaton/util/o;Ljava/lang/String;IJI)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 137
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_0

    .line 138
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 137
    :cond_0
    throw v0

    :cond_1
    if-eqz v8, :cond_2

    .line 138
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 141
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/util/m;->e:I

    .line 142
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/sec/chaton/util/m;->e:I

    return v0
.end method

.method public getItem(I)Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 147
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getItem : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/m;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/util/n;

    .line 152
    iget-object v1, v0, Lcom/sec/chaton/util/n;->a:Lcom/sec/chaton/util/o;

    sget-object v3, Lcom/sec/chaton/util/o;->a:Lcom/sec/chaton/util/o;

    if-ne v1, v3, :cond_3

    .line 154
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-wide v3, v0, Lcom/sec/chaton/util/n;->d:J

    const/4 v5, 0x3

    invoke-static {v1, v3, v4, v5, v2}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 155
    iget v3, v0, Lcom/sec/chaton/util/n;->e:I

    if-lez v3, :cond_7

    .line 156
    iget v3, v0, Lcom/sec/chaton/util/n;->e:I

    invoke-static {v1, v3}, Lcom/sec/chaton/util/ad;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v3, v1

    .line 158
    :goto_0
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_6

    .line 159
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v1, v4, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 160
    if-eqz v1, :cond_1

    .line 161
    iget-object v3, p0, Lcom/sec/chaton/util/m;->d:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/sec/chaton/util/n;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    :cond_1
    :goto_1
    new-instance v3, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v0, v0, Lcom/sec/chaton/util/n;->b:Ljava/lang/String;

    invoke-direct {v3, v1, v2, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    move-object v2, v3

    .line 182
    :cond_2
    :goto_2
    return-object v2

    .line 166
    :cond_3
    iget-object v1, v0, Lcom/sec/chaton/util/n;->a:Lcom/sec/chaton/util/o;

    sget-object v3, Lcom/sec/chaton/util/o;->b:Lcom/sec/chaton/util/o;

    if-ne v1, v3, :cond_5

    .line 168
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v3, v0, Lcom/sec/chaton/util/n;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/settings/downloads/u;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v3

    .line 169
    if-eqz v3, :cond_4

    .line 170
    iget-object v1, p0, Lcom/sec/chaton/util/m;->d:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/sec/chaton/util/n;->b:Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    :cond_4
    new-instance v1, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v0, v0, Lcom/sec/chaton/util/n;->b:Ljava/lang/String;

    invoke-direct {v1, v3, v2, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    move-object v2, v1

    goto :goto_2

    .line 173
    :cond_5
    iget-object v1, v0, Lcom/sec/chaton/util/n;->a:Lcom/sec/chaton/util/o;

    sget-object v3, Lcom/sec/chaton/util/o;->c:Lcom/sec/chaton/util/o;

    if-ne v1, v3, :cond_2

    .line 176
    :try_start_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    iget v3, v0, Lcom/sec/chaton/util/n;->c:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 180
    :goto_3
    new-instance v3, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v0, v0, Lcom/sec/chaton/util/n;->b:Ljava/lang/String;

    invoke-direct {v3, v1, v2, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    move-object v2, v3

    goto :goto_2

    .line 177
    :catch_0
    move-exception v1

    .line 178
    sget-object v3, Lcom/sec/chaton/util/m;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_3

    :cond_6
    move-object v1, v2

    goto :goto_1

    :cond_7
    move-object v3, v1

    goto :goto_0
.end method

.method public onDismiss(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/chaton/util/m;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 51
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/sec/common/util/j;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/util/m;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 55
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;->onDismiss(Landroid/view/View;)V

    .line 56
    return-void
.end method

.method public onHide(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 76
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;->onHide(Landroid/view/View;)V

    .line 77
    return-void
.end method

.method public onShow(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/util/m;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 64
    iget-object v0, p0, Lcom/sec/chaton/util/m;->b:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_1

    .line 65
    invoke-virtual {p0}, Lcom/sec/chaton/util/m;->a()V

    .line 69
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;->onShow(Landroid/view/View;)V

    .line 70
    return-void

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/util/m;->b:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->i:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/sec/chaton/util/m;->b()V

    goto :goto_0
.end method
