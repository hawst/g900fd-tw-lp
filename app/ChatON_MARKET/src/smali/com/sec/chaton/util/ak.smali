.class public Lcom/sec/chaton/util/ak;
.super Ljava/lang/Object;
.source "CommonNotificationManager.java"


# static fields
.field public static a:I

.field public static b:I

.field public static c:I

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field private static h:Lcom/sec/chaton/util/ak;


# instance fields
.field private final g:Ljava/lang/String;

.field private i:Landroid/app/NotificationManager;

.field private j:Landroid/app/Notification;

.field private k:I

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x65

    sput v0, Lcom/sec/chaton/util/ak;->a:I

    .line 29
    const/16 v0, 0x66

    sput v0, Lcom/sec/chaton/util/ak;->b:I

    .line 30
    const/16 v0, 0x67

    sput v0, Lcom/sec/chaton/util/ak;->c:I

    .line 33
    const-string v0, "action_sso"

    sput-object v0, Lcom/sec/chaton/util/ak;->d:Ljava/lang/String;

    .line 34
    const-string v0, "action_chatonv"

    sput-object v0, Lcom/sec/chaton/util/ak;->e:Ljava/lang/String;

    .line 35
    const-string v0, "action_autobackup"

    sput-object v0, Lcom/sec/chaton/util/ak;->f:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-class v0, Lcom/sec/chaton/util/ak;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/util/ak;->g:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/sec/chaton/util/ak;->i:Landroid/app/NotificationManager;

    .line 38
    iput-object v1, p0, Lcom/sec/chaton/util/ak;->j:Landroid/app/Notification;

    .line 43
    iput-object v1, p0, Lcom/sec/chaton/util/ak;->n:Ljava/lang/String;

    .line 48
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/sec/chaton/util/ak;->i:Landroid/app/NotificationManager;

    .line 50
    const v0, 0x7f020166

    iput v0, p0, Lcom/sec/chaton/util/ak;->k:I

    .line 51
    const v0, 0x7f020163

    iput v0, p0, Lcom/sec/chaton/util/ak;->l:I

    .line 53
    const-string v0, "ChatON"

    iput-object v0, p0, Lcom/sec/chaton/util/ak;->m:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/chaton/util/ak;
    .locals 2

    .prologue
    .line 58
    const-class v1, Lcom/sec/chaton/util/ak;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/chaton/util/ak;->h:Lcom/sec/chaton/util/ak;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/sec/chaton/util/ak;

    invoke-direct {v0}, Lcom/sec/chaton/util/ak;-><init>()V

    sput-object v0, Lcom/sec/chaton/util/ak;->h:Lcom/sec/chaton/util/ak;

    .line 62
    :cond_0
    sget-object v0, Lcom/sec/chaton/util/ak;->h:Lcom/sec/chaton/util/ak;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 120
    invoke-static {p1}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/chaton/chat/notification/a;->a(I)V

    .line 121
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 67
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Message : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " action : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/util/ak;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/HomeActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    const/4 v1, 0x1

    invoke-virtual {v0, p3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 79
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 81
    iget-object v1, p0, Lcom/sec/chaton/util/ak;->n:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 82
    iget-object v1, p0, Lcom/sec/chaton/util/ak;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    :cond_1
    const/4 v1, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 87
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_3

    .line 88
    new-instance v1, Landroid/app/Notification;

    iget v2, p0, Lcom/sec/chaton/util/ak;->l:I

    invoke-direct {v1, v2, p2, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iput-object v1, p0, Lcom/sec/chaton/util/ak;->j:Landroid/app/Notification;

    .line 89
    iget-object v1, p0, Lcom/sec/chaton/util/ak;->j:Landroid/app/Notification;

    iget-object v2, p0, Lcom/sec/chaton/util/ak;->m:Ljava/lang/String;

    invoke-virtual {v1, p1, v2, p2, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/util/ak;->j:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x18

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 109
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/util/ak;->j:Landroid/app/Notification;

    if-eqz v0, :cond_5

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/util/ak;->i:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/sec/chaton/util/ak;->j:Landroid/app/Notification;

    invoke-virtual {v0, p4, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 117
    :cond_2
    :goto_1
    return-void

    .line 92
    :cond_3
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/sec/chaton/util/ak;->l:I

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 96
    iget-object v3, p0, Lcom/sec/chaton/util/ak;->m:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/util/ak;->k:I

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 98
    invoke-virtual {v1, p2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 100
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v0, v2, :cond_4

    .line 102
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/util/ak;->j:Landroid/app/Notification;

    goto :goto_0

    .line 105
    :cond_4
    new-instance v0, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v0, v1}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v0, p2}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/util/ak;->j:Landroid/app/Notification;

    goto :goto_0

    .line 113
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 114
    const-string v0, "noti is null"

    iget-object v1, p0, Lcom/sec/chaton/util/ak;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 131
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setTitle : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/util/ak;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/util/ak;->m:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 143
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setAction : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/util/ak;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/util/ak;->n:Ljava/lang/String;

    .line 147
    return-void
.end method
