.class public Lcom/sec/chaton/util/be;
.super Ljava/lang/Object;
.source "IndicUtil.java"


# direct methods
.method public static a(C)I
    .locals 1

    .prologue
    .line 20
    const/16 v0, 0x900

    if-lt p0, v0, :cond_0

    const/16 v0, 0x97f

    if-gt p0, v0, :cond_0

    .line 21
    const/4 v0, 0x1

    .line 41
    :goto_0
    return v0

    .line 22
    :cond_0
    const/16 v0, 0x980

    if-lt p0, v0, :cond_1

    const/16 v0, 0x9ff

    if-gt p0, v0, :cond_1

    .line 23
    const/4 v0, 0x2

    goto :goto_0

    .line 24
    :cond_1
    const/16 v0, 0xb80

    if-lt p0, v0, :cond_2

    const/16 v0, 0xbff

    if-gt p0, v0, :cond_2

    .line 25
    const/4 v0, 0x3

    goto :goto_0

    .line 26
    :cond_2
    const/16 v0, 0xc00

    if-lt p0, v0, :cond_3

    const/16 v0, 0xc7f

    if-gt p0, v0, :cond_3

    .line 27
    const/4 v0, 0x4

    goto :goto_0

    .line 28
    :cond_3
    const/16 v0, 0xc80

    if-lt p0, v0, :cond_4

    const/16 v0, 0xcff

    if-gt p0, v0, :cond_4

    .line 29
    const/4 v0, 0x5

    goto :goto_0

    .line 30
    :cond_4
    const/16 v0, 0xa00

    if-lt p0, v0, :cond_5

    const/16 v0, 0xa7f

    if-gt p0, v0, :cond_5

    .line 31
    const/4 v0, 0x6

    goto :goto_0

    .line 32
    :cond_5
    const/16 v0, 0xa80

    if-lt p0, v0, :cond_6

    const/16 v0, 0xaff

    if-gt p0, v0, :cond_6

    .line 33
    const/4 v0, 0x7

    goto :goto_0

    .line 34
    :cond_6
    const/16 v0, 0xd00

    if-lt p0, v0, :cond_7

    const/16 v0, 0xd7f

    if-gt p0, v0, :cond_7

    .line 35
    const/16 v0, 0x8

    goto :goto_0

    .line 36
    :cond_7
    const/16 v0, 0xd80

    if-lt p0, v0, :cond_8

    const/16 v0, 0xdff

    if-gt p0, v0, :cond_8

    .line 37
    const/16 v0, 0x9

    goto :goto_0

    .line 38
    :cond_8
    const/16 v0, 0xb00

    if-lt p0, v0, :cond_9

    const/16 v0, 0xb7f

    if-gt p0, v0, :cond_9

    .line 39
    const/16 v0, 0xa

    goto :goto_0

    .line 41
    :cond_9
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 201
    move v1, v0

    .line 203
    :goto_0
    add-int/lit8 v2, p1, -0x2

    const/4 v3, -0x1

    if-le v2, v3, :cond_0

    add-int/lit8 v2, p1, -0x1

    :try_start_0
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/util/be;->c(C)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v2, p1, -0x2

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/util/be;->d(C)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 204
    add-int/lit8 v1, v1, 0x2

    .line 205
    add-int/lit8 p1, p1, -0x2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 211
    :goto_1
    return v0

    .line 209
    :catch_0
    move-exception v1

    .line 210
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;II)I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 154
    .line 158
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/util/be;->a(C)I

    move-result v1

    if-gt v1, v4, :cond_5

    .line 193
    :cond_0
    :goto_0
    return v0

    .line 173
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 174
    add-int/lit8 p1, p1, 0x1

    .line 176
    add-int/lit8 v3, p2, -0x1

    if-ge p1, v3, :cond_2

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/util/be;->a(C)I

    move-result v3

    if-eq v3, v4, :cond_2

    .line 178
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/util/be;->c(C)Z

    move-result v3

    if-ne v3, v2, :cond_2

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/util/be;->d(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 180
    add-int/lit8 v1, v1, 0x1

    .line 181
    add-int/lit8 p1, p1, 0x1

    .line 161
    :cond_2
    :goto_1
    add-int/lit8 v3, p2, -0x1

    if-ge p1, v3, :cond_4

    .line 163
    add-int/lit8 v3, p1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/util/be;->a(C)I

    move-result v3

    if-gt v3, v4, :cond_3

    move v0, v1

    .line 165
    goto :goto_0

    .line 167
    :cond_3
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/util/be;->c(C)Z

    move-result v3

    if-nez v3, :cond_1

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/sec/chaton/util/be;->b(C)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 169
    goto :goto_0

    :cond_4
    move v0, v1

    .line 186
    goto :goto_0

    .line 188
    :catch_0
    move-exception v1

    .line 189
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 190
    add-int/lit8 v1, p2, -0x1

    if-ge p1, v1, :cond_0

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/util/be;->b(C)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 191
    goto :goto_0

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method public static b(C)Z
    .locals 4

    .prologue
    const/16 v3, 0x903

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50
    invoke-static {p0}, Lcom/sec/chaton/util/be;->a(C)I

    move-result v2

    .line 52
    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 148
    :cond_0
    :goto_0
    return v0

    .line 55
    :pswitch_0
    const/16 v2, 0x901

    if-lt p0, v2, :cond_1

    if-le p0, v3, :cond_0

    :cond_1
    const/16 v2, 0x93e

    if-lt p0, v2, :cond_2

    const/16 v2, 0x94d

    if-le p0, v2, :cond_0

    :cond_2
    const/16 v2, 0x951

    if-lt p0, v2, :cond_3

    const/16 v2, 0x954

    if-le p0, v2, :cond_0

    :cond_3
    const/16 v2, 0x93c

    if-eq p0, v2, :cond_0

    const/16 v2, 0x962

    if-eq p0, v2, :cond_0

    const/16 v2, 0x963

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 62
    goto :goto_0

    .line 65
    :pswitch_1
    const/16 v2, 0x981

    if-lt p0, v2, :cond_4

    const/16 v2, 0x983

    if-le p0, v2, :cond_0

    :cond_4
    const/16 v2, 0x9bc

    if-lt p0, v2, :cond_5

    const/16 v2, 0x9cd

    if-le p0, v2, :cond_0

    :cond_5
    const/16 v2, 0x9e2

    if-lt p0, v2, :cond_6

    const/16 v2, 0x9e3

    if-le p0, v2, :cond_0

    :cond_6
    const/16 v2, 0x9d7

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 71
    goto :goto_0

    .line 74
    :pswitch_2
    const/16 v2, 0xa01

    if-lt p0, v2, :cond_7

    const/16 v2, 0xa03

    if-le p0, v2, :cond_0

    :cond_7
    const/16 v2, 0xa3e

    if-lt p0, v2, :cond_8

    const/16 v2, 0xa4d

    if-le p0, v2, :cond_0

    :cond_8
    const/16 v2, 0xa70

    if-lt p0, v2, :cond_9

    const/16 v2, 0xa71

    if-le p0, v2, :cond_0

    :cond_9
    const/16 v2, 0xa3c

    if-eq p0, v2, :cond_0

    const/16 v2, 0xa51

    if-eq p0, v2, :cond_0

    const/16 v2, 0xa75

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 81
    goto :goto_0

    .line 84
    :pswitch_3
    const/16 v2, 0xa81

    if-lt p0, v2, :cond_a

    const/16 v2, 0xa83

    if-le p0, v2, :cond_0

    :cond_a
    const/16 v2, 0xabe

    if-lt p0, v2, :cond_b

    const/16 v2, 0xacd

    if-le p0, v2, :cond_0

    :cond_b
    const/16 v2, 0xabc

    if-eq p0, v2, :cond_0

    const/16 v2, 0xae2

    if-eq p0, v2, :cond_0

    const/16 v2, 0xae3

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 90
    goto/16 :goto_0

    .line 93
    :pswitch_4
    const/16 v2, 0xb82

    if-lt p0, v2, :cond_c

    const/16 v2, 0xb83

    if-le p0, v2, :cond_0

    :cond_c
    const/16 v2, 0xbbe

    if-lt p0, v2, :cond_d

    const/16 v2, 0xbcd

    if-le p0, v2, :cond_0

    :cond_d
    const/16 v2, 0xbd7

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 99
    goto/16 :goto_0

    .line 102
    :pswitch_5
    const/16 v2, 0xc01

    if-lt p0, v2, :cond_e

    const/16 v2, 0xc03

    if-le p0, v2, :cond_0

    :cond_e
    const/16 v2, 0xc3e

    if-lt p0, v2, :cond_f

    const/16 v2, 0xc4d

    if-le p0, v2, :cond_0

    :cond_f
    const/16 v2, 0xc55

    if-lt p0, v2, :cond_10

    const/16 v2, 0xc56

    if-le p0, v2, :cond_0

    :cond_10
    const/16 v2, 0xc62

    if-eq p0, v2, :cond_0

    const/16 v2, 0xc63

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 109
    goto/16 :goto_0

    .line 112
    :pswitch_6
    const/16 v2, 0xc82

    if-lt p0, v2, :cond_11

    const/16 v2, 0xc83

    if-le p0, v2, :cond_0

    :cond_11
    const/16 v2, 0xcbe

    if-lt p0, v2, :cond_12

    const/16 v2, 0xccd

    if-le p0, v2, :cond_0

    :cond_12
    const/16 v2, 0xcd5

    if-lt p0, v2, :cond_13

    const/16 v2, 0xcd6

    if-le p0, v2, :cond_0

    :cond_13
    const/16 v2, 0xcbc

    if-eq p0, v2, :cond_0

    const/16 v2, 0xce2

    if-eq p0, v2, :cond_0

    const/16 v2, 0xce3

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 119
    goto/16 :goto_0

    .line 121
    :pswitch_7
    const/16 v2, 0xd02

    if-lt p0, v2, :cond_14

    const/16 v2, 0xd03

    if-le p0, v2, :cond_0

    :cond_14
    const/16 v2, 0xd3e

    if-lt p0, v2, :cond_15

    const/16 v2, 0xd4d

    if-le p0, v2, :cond_0

    :cond_15
    const/16 v2, 0xd62

    if-lt p0, v2, :cond_16

    const/16 v2, 0xd63

    if-le p0, v2, :cond_0

    :cond_16
    const/16 v2, 0xd57

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 127
    goto/16 :goto_0

    .line 130
    :pswitch_8
    const/16 v2, 0xd82

    if-lt p0, v2, :cond_17

    const/16 v2, 0xd83

    if-le p0, v2, :cond_0

    :cond_17
    const/16 v2, 0xdcf

    if-lt p0, v2, :cond_18

    const/16 v2, 0xddf

    if-le p0, v2, :cond_0

    :cond_18
    const/16 v2, 0xdf2

    if-lt p0, v2, :cond_19

    const/16 v2, 0xdf3

    if-le p0, v2, :cond_0

    :cond_19
    const/16 v2, 0xdca

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 136
    goto/16 :goto_0

    .line 138
    :pswitch_9
    const/16 v2, 0xb01

    if-lt p0, v2, :cond_1a

    if-le p0, v3, :cond_0

    :cond_1a
    const/16 v2, 0xb3c

    if-lt p0, v2, :cond_1b

    const/16 v2, 0xb4d

    if-le p0, v2, :cond_0

    :cond_1b
    const/16 v2, 0xb56

    if-lt p0, v2, :cond_1c

    const/16 v2, 0xb57

    if-le p0, v2, :cond_0

    :cond_1c
    const/16 v2, 0xb61

    if-eq p0, v2, :cond_0

    const/16 v2, 0xb62

    if-eq p0, v2, :cond_0

    const/16 v2, 0xb63

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 146
    goto/16 :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static c(C)Z
    .locals 1

    .prologue
    .line 216
    const/16 v0, 0x94d

    if-eq p0, v0, :cond_0

    const/16 v0, 0xccd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc4d

    if-eq p0, v0, :cond_0

    const/16 v0, 0xacd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xbcd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd4d

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa4d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9cd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xb4d

    if-eq p0, v0, :cond_0

    const/16 v0, 0xdca

    if-ne p0, v0, :cond_1

    .line 226
    :cond_0
    const/4 v0, 0x1

    .line 228
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(C)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 233
    invoke-static {p0}, Lcom/sec/chaton/util/be;->a(C)I

    move-result v2

    .line 235
    packed-switch v2, :pswitch_data_0

    .line 334
    :cond_0
    :goto_0
    return v0

    .line 238
    :pswitch_0
    const/16 v2, 0x901

    if-lt p0, v2, :cond_1

    const/16 v2, 0x914

    if-le p0, v2, :cond_0

    :cond_1
    const/16 v2, 0x93e

    if-lt p0, v2, :cond_2

    const/16 v2, 0x94d

    if-le p0, v2, :cond_0

    :cond_2
    const/16 v2, 0x951

    if-lt p0, v2, :cond_3

    const/16 v2, 0x954

    if-le p0, v2, :cond_0

    :cond_3
    const/16 v2, 0x93c

    if-eq p0, v2, :cond_0

    const/16 v2, 0x962

    if-eq p0, v2, :cond_0

    const/16 v2, 0x963

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 245
    goto :goto_0

    .line 248
    :pswitch_1
    const/16 v2, 0x981

    if-lt p0, v2, :cond_4

    const/16 v2, 0x994

    if-le p0, v2, :cond_0

    :cond_4
    const/16 v2, 0x9bc

    if-lt p0, v2, :cond_5

    const/16 v2, 0x9cd

    if-le p0, v2, :cond_0

    :cond_5
    const/16 v2, 0x9e2

    if-lt p0, v2, :cond_6

    const/16 v2, 0x9e3

    if-le p0, v2, :cond_0

    :cond_6
    const/16 v2, 0x9d7

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 254
    goto :goto_0

    .line 257
    :pswitch_2
    const/16 v2, 0xa01

    if-lt p0, v2, :cond_7

    const/16 v2, 0xa14

    if-le p0, v2, :cond_0

    :cond_7
    const/16 v2, 0xa3e

    if-lt p0, v2, :cond_8

    const/16 v2, 0xa4d

    if-le p0, v2, :cond_0

    :cond_8
    const/16 v2, 0xa70

    if-lt p0, v2, :cond_9

    const/16 v2, 0xa71

    if-le p0, v2, :cond_0

    :cond_9
    const/16 v2, 0xa3c

    if-eq p0, v2, :cond_0

    const/16 v2, 0xa51

    if-eq p0, v2, :cond_0

    const/16 v2, 0xa75

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 264
    goto :goto_0

    .line 267
    :pswitch_3
    const/16 v2, 0xa81

    if-lt p0, v2, :cond_a

    const/16 v2, 0xa94

    if-le p0, v2, :cond_0

    :cond_a
    const/16 v2, 0xabe

    if-lt p0, v2, :cond_b

    const/16 v2, 0xacd

    if-le p0, v2, :cond_0

    :cond_b
    const/16 v2, 0xabc

    if-eq p0, v2, :cond_0

    const/16 v2, 0xae2

    if-eq p0, v2, :cond_0

    const/16 v2, 0xae3

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 273
    goto/16 :goto_0

    .line 276
    :pswitch_4
    const/16 v2, 0xb82

    if-lt p0, v2, :cond_c

    const/16 v2, 0xb94

    if-le p0, v2, :cond_0

    :cond_c
    const/16 v2, 0xbbe

    if-lt p0, v2, :cond_d

    const/16 v2, 0xbcd

    if-le p0, v2, :cond_0

    :cond_d
    const/16 v2, 0xbd7

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 282
    goto/16 :goto_0

    .line 285
    :pswitch_5
    const/16 v2, 0xc01

    if-lt p0, v2, :cond_e

    const/16 v2, 0xc14

    if-le p0, v2, :cond_0

    :cond_e
    const/16 v2, 0xc3e

    if-lt p0, v2, :cond_f

    const/16 v2, 0xc4d

    if-le p0, v2, :cond_0

    :cond_f
    const/16 v2, 0xc55

    if-lt p0, v2, :cond_10

    const/16 v2, 0xc56

    if-le p0, v2, :cond_0

    :cond_10
    const/16 v2, 0xc62

    if-eq p0, v2, :cond_0

    const/16 v2, 0xc63

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 292
    goto/16 :goto_0

    .line 295
    :pswitch_6
    const/16 v2, 0xc82

    if-lt p0, v2, :cond_11

    const/16 v2, 0xc94

    if-le p0, v2, :cond_0

    :cond_11
    const/16 v2, 0xcbe

    if-lt p0, v2, :cond_12

    const/16 v2, 0xccd

    if-le p0, v2, :cond_0

    :cond_12
    const/16 v2, 0xcd5

    if-lt p0, v2, :cond_13

    const/16 v2, 0xcd6

    if-le p0, v2, :cond_0

    :cond_13
    const/16 v2, 0xcbc

    if-eq p0, v2, :cond_0

    const/16 v2, 0xce2

    if-eq p0, v2, :cond_0

    const/16 v2, 0xce3

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 302
    goto/16 :goto_0

    .line 305
    :pswitch_7
    const/16 v2, 0xd02

    if-lt p0, v2, :cond_14

    const/16 v2, 0xd14

    if-le p0, v2, :cond_0

    :cond_14
    const/16 v2, 0xd3e

    if-lt p0, v2, :cond_15

    const/16 v2, 0xd4d

    if-le p0, v2, :cond_0

    :cond_15
    const/16 v2, 0xd62

    if-lt p0, v2, :cond_16

    const/16 v2, 0xd63

    if-le p0, v2, :cond_0

    :cond_16
    const/16 v2, 0xd57

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 312
    goto/16 :goto_0

    .line 315
    :pswitch_8
    const/16 v2, 0xd82

    if-lt p0, v2, :cond_17

    const/16 v2, 0xd94

    if-le p0, v2, :cond_0

    :cond_17
    const/16 v2, 0xdcf

    if-lt p0, v2, :cond_18

    const/16 v2, 0xddf

    if-le p0, v2, :cond_0

    :cond_18
    const/16 v2, 0xdf2

    if-lt p0, v2, :cond_19

    const/16 v2, 0xdf3

    if-le p0, v2, :cond_0

    :cond_19
    const/16 v2, 0xdca

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 322
    goto/16 :goto_0

    .line 324
    :pswitch_9
    const/16 v2, 0xb01

    if-lt p0, v2, :cond_1a

    const/16 v2, 0x903

    if-le p0, v2, :cond_0

    :cond_1a
    const/16 v2, 0xb3c

    if-lt p0, v2, :cond_1b

    const/16 v2, 0xb4d

    if-le p0, v2, :cond_0

    :cond_1b
    const/16 v2, 0xb56

    if-lt p0, v2, :cond_1c

    const/16 v2, 0xb57

    if-le p0, v2, :cond_0

    :cond_1c
    const/16 v2, 0xb61

    if-eq p0, v2, :cond_0

    const/16 v2, 0xb62

    if-eq p0, v2, :cond_0

    const/16 v2, 0xb63

    if-eq p0, v2, :cond_0

    move v0, v1

    .line 332
    goto/16 :goto_0

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
