.class public final enum Lcom/sec/chaton/util/cc;
.super Ljava/lang/Enum;
.source "RegistrationUtil.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/util/cc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/util/cc;

.field public static final enum b:Lcom/sec/chaton/util/cc;

.field public static final enum c:Lcom/sec/chaton/util/cc;

.field public static final enum d:Lcom/sec/chaton/util/cc;

.field private static final synthetic f:[Lcom/sec/chaton/util/cc;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 140
    new-instance v0, Lcom/sec/chaton/util/cc;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/chaton/util/cc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/util/cc;->a:Lcom/sec/chaton/util/cc;

    .line 141
    new-instance v0, Lcom/sec/chaton/util/cc;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/chaton/util/cc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/util/cc;->b:Lcom/sec/chaton/util/cc;

    .line 142
    new-instance v0, Lcom/sec/chaton/util/cc;

    const-string v1, "GOING"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/chaton/util/cc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/util/cc;->c:Lcom/sec/chaton/util/cc;

    .line 143
    new-instance v0, Lcom/sec/chaton/util/cc;

    const-string v1, "OTHER"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/chaton/util/cc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/util/cc;->d:Lcom/sec/chaton/util/cc;

    .line 139
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/chaton/util/cc;

    sget-object v1, Lcom/sec/chaton/util/cc;->a:Lcom/sec/chaton/util/cc;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/util/cc;->b:Lcom/sec/chaton/util/cc;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/util/cc;->c:Lcom/sec/chaton/util/cc;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/util/cc;->d:Lcom/sec/chaton/util/cc;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/util/cc;->f:[Lcom/sec/chaton/util/cc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 148
    iput p3, p0, Lcom/sec/chaton/util/cc;->e:I

    .line 149
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/sec/chaton/util/cc;->e:I

    return v0
.end method

.method public static a(Lcom/sec/chaton/util/cc;)I
    .locals 1

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/sec/chaton/util/cc;->a()I

    move-result v0

    return v0
.end method

.method public static a(I)Lcom/sec/chaton/util/cc;
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lcom/sec/chaton/util/cc;->d:Lcom/sec/chaton/util/cc;

    .line 157
    sget-object v1, Lcom/sec/chaton/util/cc;->b:Lcom/sec/chaton/util/cc;

    invoke-direct {v1}, Lcom/sec/chaton/util/cc;->a()I

    move-result v1

    if-ne v1, p0, :cond_1

    .line 158
    sget-object v0, Lcom/sec/chaton/util/cc;->b:Lcom/sec/chaton/util/cc;

    .line 164
    :cond_0
    :goto_0
    return-object v0

    .line 159
    :cond_1
    sget-object v1, Lcom/sec/chaton/util/cc;->a:Lcom/sec/chaton/util/cc;

    invoke-direct {v1}, Lcom/sec/chaton/util/cc;->a()I

    move-result v1

    if-ne v1, p0, :cond_2

    .line 160
    sget-object v0, Lcom/sec/chaton/util/cc;->a:Lcom/sec/chaton/util/cc;

    goto :goto_0

    .line 161
    :cond_2
    sget-object v1, Lcom/sec/chaton/util/cc;->c:Lcom/sec/chaton/util/cc;

    invoke-direct {v1}, Lcom/sec/chaton/util/cc;->a()I

    move-result v1

    if-ne v1, p0, :cond_0

    .line 162
    sget-object v0, Lcom/sec/chaton/util/cc;->c:Lcom/sec/chaton/util/cc;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/util/cc;
    .locals 1

    .prologue
    .line 139
    const-class v0, Lcom/sec/chaton/util/cc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/util/cc;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/util/cc;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/sec/chaton/util/cc;->f:[Lcom/sec/chaton/util/cc;

    invoke-virtual {v0}, [Lcom/sec/chaton/util/cc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/util/cc;

    return-object v0
.end method
