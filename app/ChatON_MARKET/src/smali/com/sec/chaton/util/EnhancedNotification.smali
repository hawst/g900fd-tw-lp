.class public Lcom/sec/chaton/util/EnhancedNotification;
.super Lcom/sec/chaton/base/BaseActivity;
.source "EnhancedNotification.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/util/EnhancedNotification;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/chaton/util/EnhancedNotification;->b:Ljava/lang/String;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 57
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 58
    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/util/EnhancedNotification;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f020382

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->c(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/util/ap;

    invoke-direct {v2, p0}, Lcom/sec/chaton/util/ap;-><init>(Lcom/sec/chaton/util/EnhancedNotification;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/util/ao;

    invoke-direct {v2, p0}, Lcom/sec/chaton/util/ao;-><init>(Lcom/sec/chaton/util/EnhancedNotification;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/util/EnhancedNotification;->d:Landroid/app/Dialog;

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/util/EnhancedNotification;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 75
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/util/EnhancedNotification;)I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/sec/chaton/util/EnhancedNotification;->c:I

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/util/EnhancedNotification;->requestWindowFeature(I)Z

    .line 28
    invoke-virtual {p0}, Lcom/sec/chaton/util/EnhancedNotification;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x280080

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 30
    invoke-virtual {p0}, Lcom/sec/chaton/util/EnhancedNotification;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 32
    const-string v1, "Content"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/util/EnhancedNotification;->a:Ljava/lang/String;

    .line 33
    const-string v1, "InboxNO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/util/EnhancedNotification;->b:Ljava/lang/String;

    .line 34
    const-string v1, "Chattype"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/util/EnhancedNotification;->c:I

    .line 36
    invoke-direct {p0}, Lcom/sec/chaton/util/EnhancedNotification;->a()V

    .line 37
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 42
    invoke-virtual {p0, p1}, Lcom/sec/chaton/util/EnhancedNotification;->setIntent(Landroid/content/Intent;)V

    .line 43
    iget-object v0, p0, Lcom/sec/chaton/util/EnhancedNotification;->d:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/chaton/util/EnhancedNotification;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/util/EnhancedNotification;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 49
    const-string v1, "Content"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/util/EnhancedNotification;->a:Ljava/lang/String;

    .line 50
    const-string v1, "InboxNO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/util/EnhancedNotification;->b:Ljava/lang/String;

    .line 51
    const-string v1, "Chattype"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/util/EnhancedNotification;->c:I

    .line 53
    invoke-direct {p0}, Lcom/sec/chaton/util/EnhancedNotification;->a()V

    .line 54
    return-void
.end method
