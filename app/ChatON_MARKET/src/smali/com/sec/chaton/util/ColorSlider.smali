.class public Lcom/sec/chaton/util/ColorSlider;
.super Landroid/view/View;
.source "ColorSlider.java"


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/Shader;

.field private c:[I

.field private d:F

.field private e:Landroid/graphics/PorterDuffXfermode;

.field private f:Lcom/sec/chaton/util/aj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->a()V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->a()V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->a()V

    .line 35
    return-void
.end method

.method private a(IIF)I
    .locals 1

    .prologue
    .line 121
    sub-int v0, p2, p1

    int-to-float v0, v0

    mul-float/2addr v0, p3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method private a([IF)I
    .locals 7

    .prologue
    .line 125
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    .line 126
    const/4 v0, 0x0

    aget v0, p1, v0

    .line 144
    :goto_0
    return v0

    .line 128
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_1

    .line 129
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget v0, p1, v0

    goto :goto_0

    .line 132
    :cond_1
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, p2

    .line 133
    float-to-int v1, v0

    .line 134
    int-to-float v2, v1

    sub-float/2addr v0, v2

    .line 137
    aget v2, p1, v1

    .line 138
    add-int/lit8 v1, v1, 0x1

    aget v1, p1, v1

    .line 139
    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-direct {p0, v3, v4, v0}, Lcom/sec/chaton/util/ColorSlider;->a(IIF)I

    move-result v3

    .line 140
    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v4

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v5

    invoke-direct {p0, v4, v5, v0}, Lcom/sec/chaton/util/ColorSlider;->a(IIF)I

    move-result v4

    .line 141
    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v5

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v6

    invoke-direct {p0, v5, v6, v0}, Lcom/sec/chaton/util/ColorSlider;->a(IIF)I

    move-result v5

    .line 142
    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    invoke-direct {p0, v2, v1, v0}, Lcom/sec/chaton/util/ColorSlider;->a(IIF)I

    move-result v0

    .line 144
    invoke-static {v3, v4, v5, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    .line 49
    iget-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    const/high16 v2, -0x1000000

    aput v2, v0, v5

    .line 50
    iget-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    const/4 v2, -0x1

    aput v2, v0, v4

    .line 51
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/chaton/util/ColorSlider;->d:F

    .line 53
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 56
    new-instance v0, Landroid/graphics/LinearGradient;

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getWidth()I

    move-result v2

    int-to-float v3, v2

    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    aget v5, v2, v5

    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    aget v6, v2, v4

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->b:Landroid/graphics/Shader;

    .line 57
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->XOR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    iput-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->e:Landroid/graphics/PorterDuffXfermode;

    .line 58
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->b:Landroid/graphics/Shader;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 69
    iget v0, p0, Lcom/sec/chaton/util/ColorSlider;->d:F

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    .line 70
    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 71
    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 72
    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 73
    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/chaton/util/ColorSlider;->e:Landroid/graphics/PorterDuffXfermode;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 74
    sub-float v3, v0, v8

    add-float v5, v0, v8

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v6, v2

    iget-object v7, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 75
    sub-float v1, v0, v9

    add-float v3, v0, v9

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    .line 77
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 82
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-static {v0}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v0

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x2

    const/high16 v1, 0x41d80000    # 27.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/util/ColorSlider;->setMeasuredDimension(II)V

    .line 84
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 180
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 182
    if-nez p3, :cond_0

    if-nez p4, :cond_0

    .line 185
    iput-object v6, p0, Lcom/sec/chaton/util/ColorSlider;->b:Landroid/graphics/Shader;

    .line 186
    new-instance v0, Landroid/graphics/LinearGradient;

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getWidth()I

    move-result v2

    int-to-float v3, v2

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getHeight()I

    move-result v2

    int-to-float v4, v2

    iget-object v5, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->b:Landroid/graphics/Shader;

    .line 188
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 153
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 156
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 175
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 160
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 161
    iget-object v1, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/util/ColorSlider;->a([IF)I

    move-result v1

    .line 165
    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->f:Lcom/sec/chaton/util/aj;

    if-eqz v2, :cond_0

    .line 166
    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->f:Lcom/sec/chaton/util/aj;

    invoke-interface {v2, p0, v1, v0}, Lcom/sec/chaton/util/aj;->a(Lcom/sec/chaton/util/ColorSlider;IF)V

    .line 168
    :cond_0
    iput v0, p0, Lcom/sec/chaton/util/ColorSlider;->d:F

    .line 169
    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->invalidate()V

    goto :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setColors(II)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 87
    iput-object v5, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    .line 88
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    aput p1, v0, v4

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    aput p2, v0, v6

    .line 93
    const-string v0, "ColorSlider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setColors : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iput-object v5, p0, Lcom/sec/chaton/util/ColorSlider;->b:Landroid/graphics/Shader;

    .line 97
    new-instance v0, Landroid/graphics/LinearGradient;

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getWidth()I

    move-result v2

    int-to-float v3, v2

    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    aget v5, v2, v4

    iget-object v2, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    aget v6, v2, v6

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->b:Landroid/graphics/Shader;

    .line 99
    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->invalidate()V

    .line 100
    return-void
.end method

.method public setColors([I)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 103
    iput-object v6, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    .line 104
    invoke-virtual {p1}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    .line 107
    const-string v0, "ColorSlider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setColors : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iput-object v6, p0, Lcom/sec/chaton/util/ColorSlider;->b:Landroid/graphics/Shader;

    .line 111
    new-instance v0, Landroid/graphics/LinearGradient;

    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->getWidth()I

    move-result v2

    int-to-float v3, v2

    iget-object v5, p0, Lcom/sec/chaton/util/ColorSlider;->c:[I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/sec/chaton/util/ColorSlider;->b:Landroid/graphics/Shader;

    .line 113
    invoke-virtual {p0}, Lcom/sec/chaton/util/ColorSlider;->invalidate()V

    .line 114
    return-void
.end method

.method public setSliderColorChangedListener(Lcom/sec/chaton/util/aj;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/chaton/util/ColorSlider;->f:Lcom/sec/chaton/util/aj;

    .line 118
    return-void
.end method
