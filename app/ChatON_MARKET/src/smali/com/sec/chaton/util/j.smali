.class final enum Lcom/sec/chaton/util/j;
.super Ljava/lang/Enum;
.source "BuddyProfileImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/util/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/util/j;

.field public static final enum b:Lcom/sec/chaton/util/j;

.field public static final enum c:Lcom/sec/chaton/util/j;

.field private static final synthetic d:[Lcom/sec/chaton/util/j;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 78
    new-instance v0, Lcom/sec/chaton/util/j;

    const-string v1, "CACHED"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/util/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/util/j;->a:Lcom/sec/chaton/util/j;

    .line 79
    new-instance v0, Lcom/sec/chaton/util/j;

    const-string v1, "NO_CACHED"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/util/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/util/j;->b:Lcom/sec/chaton/util/j;

    .line 80
    new-instance v0, Lcom/sec/chaton/util/j;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/util/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/util/j;->c:Lcom/sec/chaton/util/j;

    .line 77
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/util/j;

    sget-object v1, Lcom/sec/chaton/util/j;->a:Lcom/sec/chaton/util/j;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/util/j;->b:Lcom/sec/chaton/util/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/util/j;->c:Lcom/sec/chaton/util/j;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/chaton/util/j;->d:[Lcom/sec/chaton/util/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/util/j;
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/sec/chaton/util/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/util/j;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/util/j;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/sec/chaton/util/j;->d:[Lcom/sec/chaton/util/j;

    invoke-virtual {v0}, [Lcom/sec/chaton/util/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/util/j;

    return-object v0
.end method
