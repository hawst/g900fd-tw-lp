.class public Lcom/sec/chaton/util/EllipsisTextView;
.super Lcom/sec/chaton/widget/AdaptableTextView;
.source "EllipsisTextView.java"


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/CharSequence;

.field private d:Z

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 32
    invoke-direct {p0, p1}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;)V

    .line 21
    iput v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->a:I

    .line 23
    iput v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->b:I

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->c:Ljava/lang/CharSequence;

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->d:Z

    .line 29
    const-string v0, "..."

    iput-object v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->e:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->a:I

    .line 23
    iput v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->b:I

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->c:Ljava/lang/CharSequence;

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->d:Z

    .line 29
    const-string v0, "..."

    iput-object v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->e:Ljava/lang/String;

    .line 38
    invoke-direct {p0, p2}, Lcom/sec/chaton/util/EllipsisTextView;->a(Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    iput v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->a:I

    .line 23
    iput v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->b:I

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->c:Ljava/lang/CharSequence;

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->d:Z

    .line 29
    const-string v0, "..."

    iput-object v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->e:Ljava/lang/String;

    .line 44
    invoke-direct {p0, p2}, Lcom/sec/chaton/util/EllipsisTextView;->a(Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 56
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 58
    const-string v1, "maxLines"

    invoke-interface {p1, v0}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    invoke-interface {p1, v0, v3}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/util/EllipsisTextView;->a:I

    .line 56
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_1
    const-string v1, "lines"

    invoke-interface {p1, v0}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    invoke-interface {p1, v0, v3}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/util/EllipsisTextView;->b:I

    goto :goto_1

    .line 64
    :cond_2
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->b:I

    return v0
.end method

.method public getMaxLines()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->a:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 142
    iget-boolean v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->d:Z

    if-nez v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/util/EllipsisTextView;->c:Ljava/lang/CharSequence;

    check-cast v0, Ljava/lang/String;

    .line 146
    if-eqz v0, :cond_1

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\n"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 148
    invoke-virtual {p0}, Lcom/sec/chaton/util/EllipsisTextView;->getMaxLines()I

    move-result v2

    .line 150
    invoke-virtual {p0}, Lcom/sec/chaton/util/EllipsisTextView;->a()I

    move-result v1

    .line 153
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const-string v4, "("

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    sub-int v4, v3, v4

    .line 155
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    .line 156
    const-string v3, ""

    .line 157
    const/4 v6, 0x1

    if-le v1, v6, :cond_0

    if-le v5, v4, :cond_0

    .line 158
    sub-int v3, v5, v4

    invoke-virtual {v0, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 165
    :cond_0
    if-eq v1, v7, :cond_3

    .line 171
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/util/EllipsisTextView;->getLineCount()I

    move-result v2

    if-le v2, v1, :cond_1

    if-eq v1, v7, :cond_1

    .line 173
    invoke-virtual {p0}, Lcom/sec/chaton/util/EllipsisTextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 175
    if-eqz v2, :cond_1

    .line 178
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {v2, v6}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v2

    .line 192
    :try_start_0
    iget-object v6, p0, Lcom/sec/chaton/util/EllipsisTextView;->e:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v2, v6

    sub-int/2addr v2, v4

    :goto_1
    if-ge v2, v5, :cond_1

    .line 194
    const/4 v4, 0x0

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 199
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/chaton/util/EllipsisTextView;->e:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p0, v4, v6}, Lcom/sec/chaton/util/EllipsisTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 204
    invoke-virtual {p0}, Lcom/sec/chaton/util/EllipsisTextView;->getLineCount()I

    move-result v4

    if-le v4, v1, :cond_2

    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/util/EllipsisTextView;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/util/EllipsisTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    :cond_1
    :goto_2
    invoke-super {p0, p1}, Lcom/sec/chaton/widget/AdaptableTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 227
    return-void

    .line 192
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 213
    :catch_0
    move-exception v1

    .line 214
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 215
    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/util/EllipsisTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto :goto_2

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method public setEllipsisText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    if-eqz p1, :cond_0

    .line 50
    iput-object p1, p0, Lcom/sec/chaton/util/EllipsisTextView;->e:Ljava/lang/String;

    .line 52
    :cond_0
    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 116
    invoke-super {p0, p1}, Lcom/sec/chaton/widget/AdaptableTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 117
    return-void
.end method

.method public setLines(I)V
    .locals 0

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/sec/chaton/widget/AdaptableTextView;->setLines(I)V

    .line 90
    iput p1, p0, Lcom/sec/chaton/util/EllipsisTextView;->b:I

    .line 91
    return-void
.end method

.method public setMaxLines(I)V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/sec/chaton/widget/AdaptableTextView;->setMaxLines(I)V

    .line 74
    iput p1, p0, Lcom/sec/chaton/util/EllipsisTextView;->a:I

    .line 75
    return-void
.end method

.method public setSingleLine(Z)V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/sec/chaton/widget/AdaptableTextView;->setSingleLine(Z)V

    .line 128
    iput-boolean p1, p0, Lcom/sec/chaton/util/EllipsisTextView;->d:Z

    .line 129
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/widget/AdaptableTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 102
    sget-object v0, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    if-eq p2, v0, :cond_0

    .line 103
    iput-object p1, p0, Lcom/sec/chaton/util/EllipsisTextView;->c:Ljava/lang/CharSequence;

    .line 106
    :cond_0
    return-void
.end method
