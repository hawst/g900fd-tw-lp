.class public Lcom/sec/chaton/util/ae;
.super Ljava/io/FilterInputStream;
.source "ChatOnGraphics.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 948
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 949
    return-void
.end method


# virtual methods
.method public skip(J)J
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 953
    move-wide v2, v4

    .line 954
    :goto_0
    cmp-long v0, v2, p1

    if-gez v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/sec/chaton/util/ae;->in:Ljava/io/InputStream;

    sub-long v6, p1, v2

    invoke-virtual {v0, v6, v7}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 956
    cmp-long v6, v0, v4

    if-nez v6, :cond_2

    .line 957
    invoke-virtual {p0}, Lcom/sec/chaton/util/ae;->read()I

    move-result v0

    .line 958
    if-gez v0, :cond_1

    .line 966
    :cond_0
    return-wide v2

    .line 961
    :cond_1
    const-wide/16 v0, 0x1

    .line 964
    :cond_2
    add-long/2addr v0, v2

    move-wide v2, v0

    .line 965
    goto :goto_0
.end method
