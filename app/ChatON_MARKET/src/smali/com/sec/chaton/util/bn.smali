.class public Lcom/sec/chaton/util/bn;
.super Landroid/text/InputFilter$LengthFilter;
.source "ObservableLengthFilter.java"


# instance fields
.field private a:I

.field private b:Lcom/sec/chaton/util/bo;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/util/bn;-><init>(ILcom/sec/chaton/util/bo;)V

    .line 20
    return-void
.end method

.method public constructor <init>(ILcom/sec/chaton/util/bo;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    .line 25
    iput p1, p0, Lcom/sec/chaton/util/bn;->a:I

    .line 26
    iput-object p2, p0, Lcom/sec/chaton/util/bn;->b:Lcom/sec/chaton/util/bo;

    .line 27
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/chaton/util/bn;->a:I

    return v0
.end method

.method public a(Lcom/sec/chaton/util/bo;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/chaton/util/bn;->b:Lcom/sec/chaton/util/bo;

    .line 35
    return-void
.end method

.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 43
    invoke-super/range {p0 .. p6}, Landroid/text/InputFilter$LengthFilter;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 45
    if-nez v0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-object v0

    .line 50
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/util/bn;->b:Lcom/sec/chaton/util/bo;

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/sec/chaton/util/bn;->b:Lcom/sec/chaton/util/bo;

    iget v2, p0, Lcom/sec/chaton/util/bn;->a:I

    invoke-interface {v1, v2}, Lcom/sec/chaton/util/bo;->a(I)V

    goto :goto_0
.end method
