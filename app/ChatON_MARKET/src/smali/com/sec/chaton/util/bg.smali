.class public Lcom/sec/chaton/util/bg;
.super Ljava/lang/Object;
.source "LatinUtil.java"


# direct methods
.method public static a(C)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 5
    const/16 v1, 0xd7

    if-eq p0, v1, :cond_0

    const/16 v1, 0xf7

    if-ne p0, v1, :cond_1

    .line 10
    :cond_0
    :goto_0
    return v0

    .line 7
    :cond_1
    const/16 v1, 0xc0

    if-lt p0, v1, :cond_0

    const/16 v1, 0x24f

    if-gt p0, v1, :cond_0

    .line 8
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(C)C
    .locals 1

    .prologue
    .line 15
    const/16 v0, 0xc0

    if-lt p0, v0, :cond_0

    const/16 v0, 0xc6

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x100

    if-lt p0, v0, :cond_3

    const/16 v0, 0x105

    if-gt p0, v0, :cond_3

    .line 16
    :cond_1
    const/16 p0, 0x41

    .line 50
    :cond_2
    :goto_0
    return p0

    .line 17
    :cond_3
    const/16 v0, 0xc7

    if-eq p0, v0, :cond_4

    const/16 v0, 0x106

    if-lt p0, v0, :cond_5

    const/16 v0, 0x10d

    if-gt p0, v0, :cond_5

    .line 18
    :cond_4
    const/16 p0, 0x43

    goto :goto_0

    .line 19
    :cond_5
    const/16 v0, 0xd0

    if-eq p0, v0, :cond_6

    const/16 v0, 0x10e

    if-lt p0, v0, :cond_7

    const/16 v0, 0x111

    if-gt p0, v0, :cond_7

    .line 20
    :cond_6
    const/16 p0, 0x44

    goto :goto_0

    .line 21
    :cond_7
    const/16 v0, 0xc8

    if-lt p0, v0, :cond_8

    const/16 v0, 0xcb

    if-le p0, v0, :cond_9

    :cond_8
    const/16 v0, 0x112

    if-lt p0, v0, :cond_a

    const/16 v0, 0x11b

    if-gt p0, v0, :cond_a

    .line 22
    :cond_9
    const/16 p0, 0x45

    goto :goto_0

    .line 23
    :cond_a
    const/16 v0, 0x11c

    if-lt p0, v0, :cond_b

    const/16 v0, 0x123

    if-gt p0, v0, :cond_b

    .line 24
    const/16 p0, 0x47

    goto :goto_0

    .line 25
    :cond_b
    const/16 v0, 0x124

    if-lt p0, v0, :cond_c

    const/16 v0, 0x127

    if-gt p0, v0, :cond_c

    .line 26
    const/16 p0, 0x48

    goto :goto_0

    .line 27
    :cond_c
    const/16 v0, 0xcc

    if-lt p0, v0, :cond_d

    const/16 v0, 0xcf

    if-le p0, v0, :cond_e

    :cond_d
    const/16 v0, 0x128

    if-lt p0, v0, :cond_f

    const/16 v0, 0x133

    if-gt p0, v0, :cond_f

    .line 28
    :cond_e
    const/16 p0, 0x49

    goto :goto_0

    .line 29
    :cond_f
    const/16 v0, 0x136

    if-lt p0, v0, :cond_10

    const/16 v0, 0x138

    if-gt p0, v0, :cond_10

    .line 30
    const/16 p0, 0x4b

    goto :goto_0

    .line 31
    :cond_10
    const/16 v0, 0x139

    if-lt p0, v0, :cond_11

    const/16 v0, 0x142

    if-gt p0, v0, :cond_11

    .line 32
    const/16 p0, 0x4c

    goto :goto_0

    .line 33
    :cond_11
    const/16 v0, 0xd1

    if-eq p0, v0, :cond_12

    const/16 v0, 0x143

    if-lt p0, v0, :cond_13

    const/16 v0, 0x14b

    if-gt p0, v0, :cond_13

    .line 34
    :cond_12
    const/16 p0, 0x4e

    goto :goto_0

    .line 35
    :cond_13
    const/16 v0, 0xd2

    if-lt p0, v0, :cond_14

    const/16 v0, 0xd8

    if-le p0, v0, :cond_15

    :cond_14
    const/16 v0, 0x14c

    if-lt p0, v0, :cond_16

    const/16 v0, 0x153

    if-gt p0, v0, :cond_16

    .line 36
    :cond_15
    const/16 p0, 0x4f

    goto/16 :goto_0

    .line 37
    :cond_16
    const/16 v0, 0x154

    if-lt p0, v0, :cond_17

    const/16 v0, 0x159

    if-gt p0, v0, :cond_17

    .line 38
    const/16 p0, 0x52

    goto/16 :goto_0

    .line 39
    :cond_17
    const/16 v0, 0x15a

    if-lt p0, v0, :cond_18

    const/16 v0, 0x161

    if-gt p0, v0, :cond_18

    .line 40
    const/16 p0, 0x53

    goto/16 :goto_0

    .line 41
    :cond_18
    const/16 v0, 0x162

    if-lt p0, v0, :cond_19

    const/16 v0, 0x167

    if-gt p0, v0, :cond_19

    .line 42
    const/16 p0, 0x54

    goto/16 :goto_0

    .line 43
    :cond_19
    const/16 v0, 0xd9

    if-lt p0, v0, :cond_1a

    const/16 v0, 0xdc

    if-le p0, v0, :cond_1b

    :cond_1a
    const/16 v0, 0x168

    if-lt p0, v0, :cond_1c

    const/16 v0, 0x173

    if-gt p0, v0, :cond_1c

    .line 44
    :cond_1b
    const/16 p0, 0x55

    goto/16 :goto_0

    .line 45
    :cond_1c
    const/16 v0, 0xdd

    if-eq p0, v0, :cond_1d

    const/16 v0, 0x176

    if-lt p0, v0, :cond_1e

    const/16 v0, 0x178

    if-gt p0, v0, :cond_1e

    .line 46
    :cond_1d
    const/16 p0, 0x59

    goto/16 :goto_0

    .line 47
    :cond_1e
    const/16 v0, 0x179

    if-lt p0, v0, :cond_2

    const/16 v0, 0x17e

    if-gt p0, v0, :cond_2

    .line 48
    const/16 p0, 0x5a

    goto/16 :goto_0
.end method

.method public static c(C)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 55
    const/16 v2, 0xd7

    if-eq p0, v2, :cond_0

    const/16 v2, 0xf7

    if-ne p0, v2, :cond_2

    :cond_0
    move v0, v1

    .line 66
    :cond_1
    :goto_0
    return v0

    .line 57
    :cond_2
    if-ltz p0, :cond_5

    const/16 v2, 0xbf

    if-gt p0, v2, :cond_5

    .line 58
    const/16 v2, 0x41

    if-lt p0, v2, :cond_3

    const/16 v2, 0x5a

    if-le p0, v2, :cond_1

    :cond_3
    const/16 v2, 0x61

    if-lt p0, v2, :cond_4

    const/16 v2, 0x7a

    if-le p0, v2, :cond_1

    :cond_4
    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :cond_5
    const/16 v2, 0x2000

    if-lt p0, v2, :cond_6

    const/16 v2, 0x27bf

    if-le p0, v2, :cond_7

    :cond_6
    const/16 v2, 0x3000

    if-lt p0, v2, :cond_1

    const/16 v2, 0x303f

    if-gt p0, v2, :cond_1

    :cond_7
    move v0, v1

    .line 64
    goto :goto_0
.end method
