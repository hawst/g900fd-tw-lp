.class Lcom/sec/chaton/util/k;
.super Landroid/os/HandlerThread;
.source "BuddyProfileImageLoader.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final synthetic a:Lcom/sec/chaton/util/f;

.field private final b:Landroid/content/Context;

.field private c:Landroid/os/Handler;

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/util/l;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/util/l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/chaton/util/f;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 560
    iput-object p1, p0, Lcom/sec/chaton/util/k;->a:Lcom/sec/chaton/util/f;

    .line 561
    const-string v0, "ProfileDownloader"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 557
    invoke-static {}, Lcom/sec/chaton/util/ag;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/util/k;->d:Ljava/util/ArrayList;

    .line 558
    invoke-static {}, Lcom/sec/chaton/util/ag;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/util/k;->e:Ljava/util/ArrayList;

    .line 562
    iput-object p2, p0, Lcom/sec/chaton/util/k;->b:Landroid/content/Context;

    .line 563
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 583
    iget-object v0, p0, Lcom/sec/chaton/util/k;->a:Lcom/sec/chaton/util/f;

    iget-object v1, p0, Lcom/sec/chaton/util/k;->d:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/chaton/util/k;->e:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/util/f;->a(Lcom/sec/chaton/util/f;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 586
    iget-object v0, p0, Lcom/sec/chaton/util/k;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/util/l;

    .line 591
    iget-object v2, p0, Lcom/sec/chaton/util/k;->a:Lcom/sec/chaton/util/f;

    iget-object v3, p0, Lcom/sec/chaton/util/k;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/chaton/util/l;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/chaton/util/f;->a(Lcom/sec/chaton/util/f;Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 592
    if-eqz v2, :cond_0

    .line 593
    iget-object v3, p0, Lcom/sec/chaton/util/k;->a:Lcom/sec/chaton/util/f;

    invoke-virtual {v0}, Lcom/sec/chaton/util/l;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/sec/chaton/util/f;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 594
    iget-object v2, p0, Lcom/sec/chaton/util/k;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 599
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/util/l;->b()I

    move-result v2

    sget-object v3, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->NOT_CHANGE:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v3}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v3

    if-eq v2, v3, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/util/l;->b()I

    move-result v2

    sget-object v3, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_DELETED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v3}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v3

    if-eq v2, v3, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/util/l;->b()I

    move-result v2

    sget-object v3, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->PROFILE_UPDATED:Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;

    invoke-virtual {v3}, Lcom/sec/chaton/io/entry/inner/Buddy$BuddyImageStatus;->getCode()I

    move-result v3

    if-ne v2, v3, :cond_4

    .line 600
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/util/k;->a:Lcom/sec/chaton/util/f;

    invoke-virtual {v0}, Lcom/sec/chaton/util/l;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/f;->f(Ljava/lang/String;)Lcom/sec/chaton/a/a/j;

    move-result-object v2

    .line 602
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/sec/chaton/a/a/j;->b()Lcom/sec/chaton/util/ba;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    if-eq v3, v4, :cond_3

    .line 603
    invoke-virtual {v2}, Lcom/sec/chaton/a/a/j;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 604
    if-eqz v2, :cond_2

    .line 605
    iget-object v3, p0, Lcom/sec/chaton/util/k;->a:Lcom/sec/chaton/util/f;

    invoke-virtual {v0}, Lcom/sec/chaton/util/l;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/sec/chaton/util/f;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 607
    :cond_2
    iget-object v3, p0, Lcom/sec/chaton/util/k;->a:Lcom/sec/chaton/util/f;

    invoke-virtual {v0}, Lcom/sec/chaton/util/l;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/sec/chaton/util/f;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 615
    :goto_1
    iget-object v2, p0, Lcom/sec/chaton/util/k;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 610
    :cond_3
    iget-object v2, p0, Lcom/sec/chaton/util/k;->a:Lcom/sec/chaton/util/f;

    invoke-virtual {v0}, Lcom/sec/chaton/util/l;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/f;->a(Lcom/sec/chaton/util/f;Ljava/lang/String;)V

    goto :goto_1

    .line 613
    :cond_4
    iget-object v2, p0, Lcom/sec/chaton/util/k;->a:Lcom/sec/chaton/util/f;

    invoke-virtual {v0}, Lcom/sec/chaton/util/l;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/f;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 617
    :cond_5
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 566
    iget-object v0, p0, Lcom/sec/chaton/util/k;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 567
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/sec/chaton/util/k;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/chaton/util/k;->c:Landroid/os/Handler;

    .line 569
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/util/k;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 570
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2

    .prologue
    .line 574
    invoke-direct {p0}, Lcom/sec/chaton/util/k;->b()V

    .line 575
    iget-object v0, p0, Lcom/sec/chaton/util/k;->a:Lcom/sec/chaton/util/f;

    invoke-static {v0}, Lcom/sec/chaton/util/f;->a(Lcom/sec/chaton/util/f;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 576
    const/4 v0, 0x1

    return v0
.end method
