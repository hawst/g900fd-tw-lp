.class public final enum Lcom/sec/chaton/util/cg;
.super Ljava/lang/Enum;
.source "ServerAddressMgr.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/util/cg;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/util/cg;

.field public static final enum b:Lcom/sec/chaton/util/cg;

.field public static final enum c:Lcom/sec/chaton/util/cg;

.field public static final enum d:Lcom/sec/chaton/util/cg;

.field public static final enum e:Lcom/sec/chaton/util/cg;

.field public static final enum f:Lcom/sec/chaton/util/cg;

.field public static final enum g:Lcom/sec/chaton/util/cg;

.field public static final enum h:Lcom/sec/chaton/util/cg;

.field public static final enum i:Lcom/sec/chaton/util/cg;

.field public static final enum j:Lcom/sec/chaton/util/cg;

.field public static final enum k:Lcom/sec/chaton/util/cg;

.field public static final enum l:Lcom/sec/chaton/util/cg;

.field public static final enum m:Lcom/sec/chaton/util/cg;

.field private static final synthetic p:[Lcom/sec/chaton/util/cg;


# instance fields
.field n:Z

.field o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "GLD"

    invoke-direct {v0, v1, v3, v3, v4}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->a:Lcom/sec/chaton/util/cg;

    .line 32
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "CONTACT"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    .line 33
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "FILE"

    invoke-direct {v0, v1, v5, v4, v4}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->c:Lcom/sec/chaton/util/cg;

    .line 34
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "SMS_GATEWAY"

    invoke-direct {v0, v1, v6, v4, v4}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->d:Lcom/sec/chaton/util/cg;

    .line 35
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "ADMIN_MOBILEWEB"

    invoke-direct {v0, v1, v7, v4, v4}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->e:Lcom/sec/chaton/util/cg;

    .line 36
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "LIVE_MOBILEWEB"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4, v4}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->f:Lcom/sec/chaton/util/cg;

    .line 37
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "APPS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->g:Lcom/sec/chaton/util/cg;

    .line 38
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "BYPASS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->h:Lcom/sec/chaton/util/cg;

    .line 41
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "SSO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->i:Lcom/sec/chaton/util/cg;

    .line 42
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "SSO_API"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->j:Lcom/sec/chaton/util/cg;

    .line 46
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "CHATONV"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->k:Lcom/sec/chaton/util/cg;

    .line 49
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "TRANSLATION_AUTH"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->l:Lcom/sec/chaton/util/cg;

    .line 50
    new-instance v0, Lcom/sec/chaton/util/cg;

    const-string v1, "TRANSLATION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/sec/chaton/util/cg;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/sec/chaton/util/cg;->m:Lcom/sec/chaton/util/cg;

    .line 30
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/sec/chaton/util/cg;

    sget-object v1, Lcom/sec/chaton/util/cg;->a:Lcom/sec/chaton/util/cg;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/util/cg;->c:Lcom/sec/chaton/util/cg;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/util/cg;->d:Lcom/sec/chaton/util/cg;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/util/cg;->e:Lcom/sec/chaton/util/cg;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/util/cg;->f:Lcom/sec/chaton/util/cg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/util/cg;->g:Lcom/sec/chaton/util/cg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/util/cg;->h:Lcom/sec/chaton/util/cg;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/util/cg;->i:Lcom/sec/chaton/util/cg;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/chaton/util/cg;->j:Lcom/sec/chaton/util/cg;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/chaton/util/cg;->k:Lcom/sec/chaton/util/cg;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/chaton/util/cg;->l:Lcom/sec/chaton/util/cg;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/chaton/util/cg;->m:Lcom/sec/chaton/util/cg;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/util/cg;->p:[Lcom/sec/chaton/util/cg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput-boolean p3, p0, Lcom/sec/chaton/util/cg;->n:Z

    .line 57
    iput-boolean p4, p0, Lcom/sec/chaton/util/cg;->o:Z

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/util/cg;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/chaton/util/cg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/util/cg;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/util/cg;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/chaton/util/cg;->p:[Lcom/sec/chaton/util/cg;

    invoke-virtual {v0}, [Lcom/sec/chaton/util/cg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/util/cg;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/sec/chaton/util/cg;->n:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/util/cg;->o:Z

    return v0
.end method
