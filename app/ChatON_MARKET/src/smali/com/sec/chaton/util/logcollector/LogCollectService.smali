.class public Lcom/sec/chaton/util/logcollector/LogCollectService;
.super Landroid/app/IntentService;
.source "LogCollectService.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static c:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-class v0, Lcom/sec/chaton/util/logcollector/LogCollectService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Thread"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/util/logcollector/LogCollectService;->b:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/util/logcollector/LogCollectService;->c:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/chaton/util/logcollector/LogCollectService;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public static final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-static {p0, v0, v0}, Lcom/sec/chaton/util/logcollector/LogCollectService;->a(Landroid/content/Context;ZZ)V

    .line 52
    return-void
.end method

.method public static final a(Landroid/content/Context;ZZ)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/sec/chaton/util/logcollector/LogCollectService;->a(Landroid/content/Context;ZZZ)V

    .line 56
    return-void
.end method

.method public static final a(Landroid/content/Context;ZZZ)V
    .locals 2

    .prologue
    .line 59
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "Send gathering task to queue."

    sget-object v1, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/util/logcollector/LogCollectService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    const-string v1, "gathering"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    const-string v1, "forceSave"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 67
    const-string v1, "withCallback"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 68
    const-string v1, "stopGathering"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 70
    return-void
.end method

.method public static final a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 47
    sput-object p0, Lcom/sec/chaton/util/logcollector/LogCollectService;->c:Landroid/os/Handler;

    .line 48
    return-void
.end method

.method private a(ZZZ)V
    .locals 12

    .prologue
    .line 104
    .line 105
    invoke-direct {p0, p0}, Lcom/sec/chaton/util/logcollector/LogCollectService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    const-string v0, "null"

    .line 127
    :cond_0
    const-string v1, "Version: %s / ChatON account: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "2.6.1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 129
    const/4 v0, 0x0

    .line 131
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_1

    .line 132
    const-string v1, "Start logcat gathering...(Extra: %s)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_1
    const-string v1, "ChatON:d ActivityManager:e *:s"

    const/4 v3, 0x1

    invoke-static {p0, v1, v2, v3}, Lcom/sec/chaton/util/logcollector/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 136
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_2

    .line 137
    const-string v1, "End logcat gathering... Rescheduling gathering after %d seconds."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide/32 v5, 0xea60

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_2
    const/4 v1, 0x0

    .line 143
    if-eqz p1, :cond_8

    .line 144
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_3

    .line 145
    const-string v1, "Force save flag is true."

    sget-object v3, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_3
    const/4 v1, 0x1

    .line 168
    :cond_4
    :goto_0
    if-eqz v1, :cond_6

    .line 174
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_5

    .line 175
    const-string v1, "Copy internal log file to sdcard."

    sget-object v3, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/util/logcollector/LogCollectService;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 178
    const-string v0, "ChatON:d ActivityManager:e *:s"

    const/4 v1, 0x0

    invoke-static {p0, v0, v2, v1}, Lcom/sec/chaton/util/logcollector/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 180
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/chaton/util/logcollector/a;->b(J)V

    .line 182
    const/4 v0, 0x1

    .line 187
    :cond_6
    if-eqz p3, :cond_b

    .line 188
    invoke-static {p0}, Lcom/sec/chaton/util/logcollector/scheduler/GatheringScheduler;->a(Landroid/content/Context;)V

    .line 194
    :goto_1
    if-eqz p2, :cond_7

    sget-object v1, Lcom/sec/chaton/util/logcollector/LogCollectService;->c:Landroid/os/Handler;

    if-eqz v1, :cond_7

    .line 195
    sget-object v1, Lcom/sec/chaton/util/logcollector/LogCollectService;->c:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 196
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/sec/chaton/util/logcollector/LogCollectService;->c:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 205
    :cond_7
    return-void

    .line 151
    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 152
    invoke-static {v3, v4}, Lcom/sec/chaton/util/logcollector/a;->a(J)J

    move-result-wide v5

    .line 154
    sget-boolean v7, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v7, :cond_9

    .line 155
    const-string v7, "Current time(%s) - Last upload time(%s) = %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10, v3, v4}, Ljava/util/Date;-><init>(J)V

    aput-object v10, v8, v9

    const/4 v9, 0x1

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10, v5, v6}, Ljava/util/Date;-><init>(J)V

    aput-object v10, v8, v9

    const/4 v9, 0x2

    sub-long v10, v3, v5

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_9
    sub-long/2addr v3, v5

    const-wide/32 v5, 0x36ee80

    cmp-long v3, v3, v5

    if-ltz v3, :cond_4

    .line 161
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_a

    .line 162
    const-string v1, "Current time - Last upload time is bigger than upload interval."

    sget-object v3, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :cond_a
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 191
    :cond_b
    const-wide/32 v1, 0xea60

    invoke-static {p0, v1, v2}, Lcom/sec/chaton/util/logcollector/scheduler/GatheringScheduler;->a(Landroid/content/Context;J)V

    goto :goto_1
.end method

.method private a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 271
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_0

    .line 272
    const-string v1, "Start save log to sdcard..."

    sget-object v2, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    .line 275
    const-string v2, "mounted"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 276
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 277
    const-string v1, "Couldn\'t find sdcard."

    sget-object v2, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_1
    :goto_0
    return v0

    .line 282
    :cond_2
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "chatonlog"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 284
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 285
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_3

    .line 286
    const-string v2, "Log directory is created."

    sget-object v3, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_4

    .line 291
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 292
    const-string v1, "Can\'t access log directory."

    sget-object v2, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    :cond_4
    new-instance v2, Ljava/io/File;

    invoke-direct {p0}, Lcom/sec/chaton/util/logcollector/LogCollectService;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 300
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_5

    .line 301
    const-string v1, "Log file path: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_5
    invoke-static {p0, v2}, Lcom/sec/chaton/util/logcollector/b;->a(Landroid/content/Context;Ljava/io/File;)Z

    move-result v0

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 342
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 343
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMdd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 345
    const-string v2, "chaton_%s_%s.txt"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    const-string v1, "kkmmss"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 308
    .line 309
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 310
    const-string v2, "com.sec.chaton"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 312
    array-length v2, v0

    if-ge v4, v2, :cond_6

    aget-object v0, v0, v4

    .line 313
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 317
    :goto_0
    invoke-static {v1}, Lcom/sec/chaton/util/logcollector/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 318
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    .line 319
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 320
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_1

    .line 321
    const-string v0, "Couldn\'t find ChatON account."

    sget-object v2, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v0, v1

    .line 337
    :cond_2
    :goto_1
    return-object v0

    .line 326
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v0, :cond_4

    .line 327
    const-string v0, "Couldn\'t find ChatON account. Using ChatON account(%s) in preference."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/logcollector/LogCollectService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 330
    goto :goto_1

    .line 334
    :cond_5
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 335
    invoke-static {v0}, Lcom/sec/chaton/util/logcollector/a;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 88
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 90
    const-string v1, "gathering"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    const-string v0, "forceSave"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 92
    const-string v1, "withCallback"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 93
    const-string v2, "stopGathering"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 95
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/util/logcollector/LogCollectService;->a(ZZZ)V

    .line 101
    :cond_0
    return-void
.end method
