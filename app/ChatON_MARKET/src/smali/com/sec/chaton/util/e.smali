.class public final Lcom/sec/chaton/util/e;
.super Ljava/lang/Object;
.source "Base64.java"


# static fields
.field private static final a:[B

.field private static final b:[C


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x3f

    const/16 v7, 0x3e

    const/16 v6, 0x2f

    const/16 v5, 0x2b

    const/4 v0, 0x0

    .line 47
    const/16 v1, 0x80

    new-array v1, v1, [B

    sput-object v1, Lcom/sec/chaton/util/e;->a:[B

    .line 48
    const/16 v1, 0x40

    new-array v1, v1, [C

    sput-object v1, Lcom/sec/chaton/util/e;->b:[C

    move v1, v0

    .line 52
    :goto_0
    const/16 v2, 0x80

    if-ge v1, v2, :cond_0

    .line 53
    sget-object v2, Lcom/sec/chaton/util/e;->a:[B

    const/4 v3, -0x1

    aput-byte v3, v2, v1

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 55
    :cond_0
    const/16 v1, 0x5a

    :goto_1
    const/16 v2, 0x41

    if-lt v1, v2, :cond_1

    .line 56
    sget-object v2, Lcom/sec/chaton/util/e;->a:[B

    add-int/lit8 v3, v1, -0x41

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 55
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 58
    :cond_1
    const/16 v1, 0x7a

    :goto_2
    const/16 v2, 0x61

    if-lt v1, v2, :cond_2

    .line 59
    sget-object v2, Lcom/sec/chaton/util/e;->a:[B

    add-int/lit8 v3, v1, -0x61

    add-int/lit8 v3, v3, 0x1a

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 58
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 62
    :cond_2
    const/16 v1, 0x39

    :goto_3
    const/16 v2, 0x30

    if-lt v1, v2, :cond_3

    .line 63
    sget-object v2, Lcom/sec/chaton/util/e;->a:[B

    add-int/lit8 v3, v1, -0x30

    add-int/lit8 v3, v3, 0x34

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 62
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 66
    :cond_3
    sget-object v1, Lcom/sec/chaton/util/e;->a:[B

    aput-byte v7, v1, v5

    .line 67
    sget-object v1, Lcom/sec/chaton/util/e;->a:[B

    aput-byte v8, v1, v6

    move v1, v0

    .line 69
    :goto_4
    const/16 v2, 0x19

    if-gt v1, v2, :cond_4

    .line 70
    sget-object v2, Lcom/sec/chaton/util/e;->b:[C

    add-int/lit8 v3, v1, 0x41

    int-to-char v3, v3

    aput-char v3, v2, v1

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 73
    :cond_4
    const/16 v1, 0x1a

    move v2, v1

    move v1, v0

    :goto_5
    const/16 v3, 0x33

    if-gt v2, v3, :cond_5

    .line 74
    sget-object v3, Lcom/sec/chaton/util/e;->b:[C

    add-int/lit8 v4, v1, 0x61

    int-to-char v4, v4

    aput-char v4, v3, v2

    .line 73
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 77
    :cond_5
    const/16 v1, 0x34

    :goto_6
    const/16 v2, 0x3d

    if-gt v1, v2, :cond_6

    .line 78
    sget-object v2, Lcom/sec/chaton/util/e;->b:[C

    add-int/lit8 v3, v0, 0x30

    int-to-char v3, v3

    aput-char v3, v2, v1

    .line 77
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 80
    :cond_6
    sget-object v0, Lcom/sec/chaton/util/e;->b:[C

    aput-char v5, v0, v7

    .line 81
    sget-object v0, Lcom/sec/chaton/util/e;->b:[C

    aput-char v6, v0, v8

    .line 83
    return-void
.end method

.method protected static a([C)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 298
    if-nez p0, :cond_1

    .line 310
    :cond_0
    return v1

    .line 304
    :cond_1
    array-length v3, p0

    move v2, v1

    .line 305
    :goto_0
    if-ge v2, v3, :cond_0

    .line 306
    aget-char v0, p0, v2

    invoke-static {v0}, Lcom/sec/chaton/util/e;->a(C)Z

    move-result v0

    if-nez v0, :cond_2

    .line 307
    add-int/lit8 v0, v1, 0x1

    aget-char v4, p0, v2

    aput-char v4, p0, v1

    .line 305
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected static a(C)Z
    .locals 1

    .prologue
    .line 86
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)[B
    .locals 14

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 203
    if-nez p0, :cond_1

    .line 287
    :cond_0
    :goto_0
    return-object v0

    .line 207
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 209
    invoke-static {v6}, Lcom/sec/chaton/util/e;->a([C)I

    move-result v1

    .line 211
    rem-int/lit8 v2, v1, 0x4

    if-nez v2, :cond_0

    .line 215
    div-int/lit8 v7, v1, 0x4

    .line 217
    if-nez v7, :cond_2

    .line 218
    new-array v0, v3, [B

    goto :goto_0

    .line 228
    :cond_2
    mul-int/lit8 v1, v7, 0x3

    new-array v1, v1, [B

    move v2, v3

    move v4, v3

    move v5, v3

    .line 230
    :goto_1
    add-int/lit8 v8, v7, -0x1

    if-ge v5, v8, :cond_3

    .line 232
    add-int/lit8 v8, v2, 0x1

    aget-char v9, v6, v2

    invoke-static {v9}, Lcom/sec/chaton/util/e;->c(C)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v2, v8, 0x1

    aget-char v8, v6, v8

    invoke-static {v8}, Lcom/sec/chaton/util/e;->c(C)Z

    move-result v10

    if-eqz v10, :cond_0

    add-int/lit8 v10, v2, 0x1

    aget-char v11, v6, v2

    invoke-static {v11}, Lcom/sec/chaton/util/e;->c(C)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v2, v10, 0x1

    aget-char v10, v6, v10

    invoke-static {v10}, Lcom/sec/chaton/util/e;->c(C)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 237
    sget-object v12, Lcom/sec/chaton/util/e;->a:[B

    aget-byte v9, v12, v9

    .line 238
    sget-object v12, Lcom/sec/chaton/util/e;->a:[B

    aget-byte v8, v12, v8

    .line 239
    sget-object v12, Lcom/sec/chaton/util/e;->a:[B

    aget-byte v11, v12, v11

    .line 240
    sget-object v12, Lcom/sec/chaton/util/e;->a:[B

    aget-byte v10, v12, v10

    .line 242
    add-int/lit8 v12, v4, 0x1

    shl-int/lit8 v9, v9, 0x2

    shr-int/lit8 v13, v8, 0x4

    or-int/2addr v9, v13

    int-to-byte v9, v9

    aput-byte v9, v1, v4

    .line 243
    add-int/lit8 v9, v12, 0x1

    and-int/lit8 v4, v8, 0xf

    shl-int/lit8 v4, v4, 0x4

    shr-int/lit8 v8, v11, 0x2

    and-int/lit8 v8, v8, 0xf

    or-int/2addr v4, v8

    int-to-byte v4, v4

    aput-byte v4, v1, v12

    .line 244
    add-int/lit8 v4, v9, 0x1

    shl-int/lit8 v8, v11, 0x6

    or-int/2addr v8, v10

    int-to-byte v8, v8

    aput-byte v8, v1, v9

    .line 230
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 247
    :cond_3
    add-int/lit8 v7, v2, 0x1

    aget-char v2, v6, v2

    invoke-static {v2}, Lcom/sec/chaton/util/e;->c(C)Z

    move-result v8

    if-eqz v8, :cond_0

    add-int/lit8 v8, v7, 0x1

    aget-char v7, v6, v7

    invoke-static {v7}, Lcom/sec/chaton/util/e;->c(C)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 251
    sget-object v9, Lcom/sec/chaton/util/e;->a:[B

    aget-byte v2, v9, v2

    .line 252
    sget-object v9, Lcom/sec/chaton/util/e;->a:[B

    aget-byte v7, v9, v7

    .line 254
    add-int/lit8 v9, v8, 0x1

    aget-char v8, v6, v8

    .line 255
    add-int/lit8 v10, v9, 0x1

    aget-char v6, v6, v9

    .line 256
    invoke-static {v8}, Lcom/sec/chaton/util/e;->c(C)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-static {v6}, Lcom/sec/chaton/util/e;->c(C)Z

    move-result v9

    if-nez v9, :cond_6

    .line 257
    :cond_4
    invoke-static {v8}, Lcom/sec/chaton/util/e;->b(C)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-static {v6}, Lcom/sec/chaton/util/e;->b(C)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 258
    and-int/lit8 v6, v7, 0xf

    if-nez v6, :cond_0

    .line 261
    mul-int/lit8 v0, v5, 0x3

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [B

    .line 262
    mul-int/lit8 v5, v5, 0x3

    invoke-static {v1, v3, v0, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 263
    shl-int/lit8 v1, v2, 0x2

    shr-int/lit8 v2, v7, 0x4

    or-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, v0, v4

    goto/16 :goto_0

    .line 265
    :cond_5
    invoke-static {v8}, Lcom/sec/chaton/util/e;->b(C)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-static {v6}, Lcom/sec/chaton/util/e;->b(C)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 266
    sget-object v6, Lcom/sec/chaton/util/e;->a:[B

    aget-byte v6, v6, v8

    .line 267
    and-int/lit8 v8, v6, 0x3

    if-nez v8, :cond_0

    .line 270
    mul-int/lit8 v0, v5, 0x3

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    .line 271
    mul-int/lit8 v5, v5, 0x3

    invoke-static {v1, v3, v0, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 272
    add-int/lit8 v1, v4, 0x1

    shl-int/lit8 v2, v2, 0x2

    shr-int/lit8 v3, v7, 0x4

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v4

    .line 273
    and-int/lit8 v2, v7, 0xf

    shl-int/lit8 v2, v2, 0x4

    shr-int/lit8 v3, v6, 0x2

    and-int/lit8 v3, v3, 0xf

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    goto/16 :goto_0

    .line 279
    :cond_6
    sget-object v0, Lcom/sec/chaton/util/e;->a:[B

    aget-byte v0, v0, v8

    .line 280
    sget-object v3, Lcom/sec/chaton/util/e;->a:[B

    aget-byte v3, v3, v6

    .line 281
    add-int/lit8 v5, v4, 0x1

    shl-int/lit8 v2, v2, 0x2

    shr-int/lit8 v6, v7, 0x4

    or-int/2addr v2, v6

    int-to-byte v2, v2

    aput-byte v2, v1, v4

    .line 282
    add-int/lit8 v2, v5, 0x1

    and-int/lit8 v4, v7, 0xf

    shl-int/lit8 v4, v4, 0x4

    shr-int/lit8 v6, v0, 0x2

    and-int/lit8 v6, v6, 0xf

    or-int/2addr v4, v6

    int-to-byte v4, v4

    aput-byte v4, v1, v5

    .line 283
    add-int/lit8 v4, v2, 0x1

    shl-int/lit8 v0, v0, 0x6

    or-int/2addr v0, v3

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    move-object v0, v1

    .line 287
    goto/16 :goto_0
.end method

.method protected static b(C)Z
    .locals 1

    .prologue
    .line 90
    const/16 v0, 0x3d

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static c(C)Z
    .locals 2

    .prologue
    .line 94
    const/16 v0, 0x80

    if-ge p0, v0, :cond_0

    sget-object v0, Lcom/sec/chaton/util/e;->a:[B

    aget-byte v0, v0, p0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
