.class public final Lcom/sec/chaton/util/b;
.super Ljava/lang/Object;
.source "AnimationBuilder.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/chaton/util/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/util/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 123
    const/4 v0, 0x0

    :try_start_0
    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 125
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 131
    :goto_0
    return v0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_0

    .line 128
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/chaton/util/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 131
    :cond_0
    const/16 v0, 0x1f4

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/io/File;Ljava/io/FilenameFilter;II)Landroid/graphics/drawable/AnimationDrawable;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 40
    invoke-static {p0, p1, p2, p3, p4}, Lcom/sec/chaton/util/b;->b(Landroid/content/Context;Ljava/io/File;Ljava/io/FilenameFilter;II)Ljava/util/List;

    move-result-object v1

    .line 42
    new-instance v2, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    .line 44
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/util/d;

    .line 45
    invoke-virtual {v0}, Lcom/sec/chaton/util/d;->b()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/chaton/util/d;->a()I

    move-result v0

    invoke-virtual {v2, v4, v0}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0

    .line 48
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 51
    invoke-virtual {v2, v5}, Landroid/graphics/drawable/AnimationDrawable;->selectDrawable(I)Z

    .line 52
    invoke-virtual {v2, v5}, Landroid/graphics/drawable/AnimationDrawable;->setOneShot(Z)V

    .line 54
    return-object v2
.end method

.method public static b(Landroid/content/Context;Ljava/io/File;Ljava/io/FilenameFilter;II)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/io/File;",
            "Ljava/io/FilenameFilter;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/util/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "Argument isn\'t directory. "

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_1
    if-nez p2, :cond_2

    .line 65
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 70
    :goto_0
    array-length v1, v0

    if-nez v1, :cond_3

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "The directory is empty. "

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v1}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_2
    invoke-virtual {p1, p2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_3
    new-instance v1, Lcom/sec/chaton/util/c;

    invoke-direct {v1}, Lcom/sec/chaton/util/c;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 81
    new-instance v2, Ljava/util/ArrayList;

    array-length v1, v0

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 83
    array-length v3, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v0, v1

    .line 84
    new-instance v5, Lcom/sec/chaton/util/d;

    invoke-direct {v5}, Lcom/sec/chaton/util/d;-><init>()V

    .line 87
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/chaton/util/b;->a(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/chaton/util/d;->a(I)V

    .line 93
    :try_start_0
    invoke-static {p0, v4, p3, p4}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 103
    const/16 v7, 0xa0

    invoke-virtual {v6, v7}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 105
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v7, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 106
    const/4 v6, 0x1

    invoke-virtual {v7, v6}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    .line 108
    invoke-virtual {v5, v7}, Lcom/sec/chaton/util/d;->a(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 110
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    sget-object v6, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v6, v6, Lcom/sec/common/c/a;->b:Z

    if-eqz v6, :cond_4

    .line 113
    sget-object v6, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v7, Lcom/sec/chaton/util/b;->a:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, " Frame >> Path: "

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v4}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v4

    aput-object v4, v8, v9

    const/4 v4, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ", Duration: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lcom/sec/chaton/util/d;->a()I

    move-result v5

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    invoke-static {v8}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 95
    :catch_0
    move-exception v4

    .line 96
    sget-object v4, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v4, v4, Lcom/sec/common/c/a;->e:Z

    if-eqz v4, :cond_4

    .line 97
    sget-object v4, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v5, Lcom/sec/chaton/util/b;->a:Ljava/lang/String;

    const-string v6, "Can\'t resize bimtap."

    invoke-virtual {v4, v5, v6}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 117
    :cond_5
    return-object v2
.end method
