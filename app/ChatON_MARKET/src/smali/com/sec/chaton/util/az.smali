.class Lcom/sec/chaton/util/az;
.super Landroid/os/AsyncTask;
.source "ImageDownloader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/sec/chaton/a/a/j;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/util/aw;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/sec/chaton/util/bb;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/util/aw;Lcom/sec/chaton/util/bb;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/sec/chaton/util/az;->a:Lcom/sec/chaton/util/aw;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 352
    iput-object p2, p0, Lcom/sec/chaton/util/az;->d:Lcom/sec/chaton/util/bb;

    .line 353
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Lcom/sec/chaton/a/a/j;
    .locals 3

    .prologue
    .line 360
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/sec/chaton/util/az;->b:Ljava/lang/String;

    .line 361
    const/4 v0, 0x1

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/sec/chaton/util/az;->c:Ljava/lang/String;

    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/util/az;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&r="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 363
    iget-object v1, p0, Lcom/sec/chaton/util/az;->a:Lcom/sec/chaton/util/aw;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/util/aw;->a(Ljava/lang/String;)Lcom/sec/chaton/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/sec/chaton/a/a/j;)V
    .locals 4

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/sec/chaton/util/az;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/chaton/a/a/j;->a(Landroid/graphics/Bitmap;)V

    .line 374
    :cond_0
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/j;->b()Lcom/sec/chaton/util/ba;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/ba;->c:Lcom/sec/chaton/util/ba;

    if-eq v0, v1, :cond_1

    .line 375
    iget-object v0, p0, Lcom/sec/chaton/util/az;->a:Lcom/sec/chaton/util/aw;

    iget-object v1, p0, Lcom/sec/chaton/util/az;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/j;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/aw;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 377
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/util/az;->d:Lcom/sec/chaton/util/bb;

    iget-object v1, p0, Lcom/sec/chaton/util/az;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/j;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/j;->b()Lcom/sec/chaton/util/ba;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/util/bb;->a(Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/chaton/util/ba;)V

    .line 378
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 343
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/util/az;->a([Ljava/lang/String;)Lcom/sec/chaton/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 343
    check-cast p1, Lcom/sec/chaton/a/a/j;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/util/az;->a(Lcom/sec/chaton/a/a/j;)V

    return-void
.end method
