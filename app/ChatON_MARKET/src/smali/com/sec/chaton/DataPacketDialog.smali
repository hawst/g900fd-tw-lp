.class public Lcom/sec/chaton/DataPacketDialog;
.super Landroid/app/Activity;
.source "DataPacketDialog.java"


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/CheckBox;

.field private c:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/DataPacketDialog;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/DataPacketDialog;->b:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 224
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-virtual {p0}, Lcom/sec/chaton/DataPacketDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 227
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 228
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 229
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    invoke-virtual {p0, v1}, Lcom/sec/chaton/DataPacketDialog;->startActivity(Landroid/content/Intent;)V

    .line 233
    :cond_0
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    .line 240
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 242
    const v0, 0x7f07013a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/DataPacketDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 243
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 244
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 246
    const/4 v1, 0x0

    .line 247
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    .line 248
    invoke-virtual {p0}, Lcom/sec/chaton/DataPacketDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 249
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    .line 251
    iget v5, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 257
    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v1, v4

    add-float/2addr v1, v7

    float-to-int v1, v1

    .line 261
    :cond_0
    :goto_0
    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 262
    invoke-virtual {v2, v3, v3, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 263
    const/16 v1, 0x11

    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 264
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 265
    return-void

    .line 258
    :cond_1
    iget v5, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 259
    const/high16 v1, 0x43a00000    # 320.0f

    mul-float/2addr v1, v4

    add-float/2addr v1, v7

    float-to-int v1, v1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v3, -0x2

    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0, v8}, Lcom/sec/chaton/DataPacketDialog;->requestWindowFeature(I)Z

    .line 37
    const v0, 0x7f030032

    invoke-virtual {p0, v0}, Lcom/sec/chaton/DataPacketDialog;->setContentView(I)V

    .line 40
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 43
    invoke-virtual {p0, v2}, Lcom/sec/chaton/DataPacketDialog;->setFinishOnTouchOutside(Z)V

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/DataPacketDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setLayout(II)V

    .line 51
    const v0, 0x7f07013a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/DataPacketDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 52
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 53
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 56
    invoke-virtual {p0}, Lcom/sec/chaton/DataPacketDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 59
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    .line 60
    invoke-virtual {p0}, Lcom/sec/chaton/DataPacketDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 61
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    .line 63
    iget v6, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 69
    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v1, v5

    add-float/2addr v1, v9

    float-to-int v1, v1

    .line 73
    :goto_0
    iput v1, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 74
    invoke-virtual {v3, v4, v4, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 75
    const/16 v1, 0x11

    iput v1, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 76
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    const v0, 0x7f07013d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/DataPacketDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/DataPacketDialog;->a:Landroid/widget/Button;

    .line 80
    const v0, 0x7f07013c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/DataPacketDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/DataPacketDialog;->b:Landroid/widget/CheckBox;

    .line 81
    const v0, 0x7f07013b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/DataPacketDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/DataPacketDialog;->c:Landroid/widget/LinearLayout;

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/DataPacketDialog;->c:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/chaton/o;

    invoke-direct {v1, p0}, Lcom/sec/chaton/o;-><init>(Lcom/sec/chaton/DataPacketDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/DataPacketDialog;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/DataPacketDialog;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/DataPacketDialog;->a:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/p;

    invoke-direct {v1, p0}, Lcom/sec/chaton/p;-><init>(Lcom/sec/chaton/DataPacketDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    return-void

    .line 70
    :cond_1
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v8, :cond_2

    .line 71
    const/high16 v1, 0x43a00000    # 320.0f

    mul-float/2addr v1, v5

    add-float/2addr v1, v9

    float-to-int v1, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 218
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 220
    invoke-direct {p0}, Lcom/sec/chaton/DataPacketDialog;->a()V

    .line 221
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 161
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/sec/chaton/DataPacketDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 201
    :goto_0
    return-void

    .line 168
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
