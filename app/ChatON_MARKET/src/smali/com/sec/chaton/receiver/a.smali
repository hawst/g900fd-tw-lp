.class Lcom/sec/chaton/receiver/a;
.super Ljava/lang/Object;
.source "PushReceiver.java"

# interfaces
.implements Lcom/sec/chaton/e/b/d;


# instance fields
.field final synthetic a:Lcom/sec/chaton/receiver/PushReceiver;


# direct methods
.method constructor <init>(Lcom/sec/chaton/receiver/PushReceiver;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/chaton/receiver/a;->a:Lcom/sec/chaton/receiver/PushReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IZLjava/lang/Object;)V
    .locals 23

    .prologue
    .line 138
    if-eqz p3, :cond_7

    .line 139
    check-cast p3, Lcom/sec/chaton/e/b/p;

    .line 142
    if-eqz p2, :cond_6

    .line 144
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->c()Lcom/sec/chaton/io/entry/PushEntry;

    move-result-object v21

    .line 146
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 152
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    .line 157
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->f()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 160
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->g()Ljava/lang/String;

    move-result-object v11

    .line 161
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->e()I

    move-result v3

    .line 164
    const/4 v2, 0x1

    .line 165
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/chaton/io/entry/PushEntry;->chatType:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    if-ne v4, v5, :cond_1

    .line 166
    const/4 v2, 0x0

    .line 167
    const-string v11, "ChatON"

    .line 168
    const/4 v3, 0x1

    .line 173
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/chaton/receiver/a;->a:Lcom/sec/chaton/receiver/PushReceiver;

    invoke-static {v4}, Lcom/sec/chaton/receiver/PushReceiver;->a(Lcom/sec/chaton/receiver/PushReceiver;)Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/sec/chaton/IntentControllerActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v4

    .line 174
    const-string v5, "callChatList"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 178
    const/4 v9, 0x0

    .line 179
    sget-object v2, Lcom/sec/chaton/receiver/b;->a:[I

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/chaton/io/entry/PushEntry;->chatType:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/e/r;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .line 213
    :goto_0
    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/sec/chaton/io/entry/PushEntry;->message:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/chaton/io/entry/PushEntry;->msgType:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v2, v4}, Lcom/sec/chaton/e/a/q;->a(Ljava/lang/String;I)Lcom/sec/chaton/e/w;

    move-result-object v8

    .line 222
    sget-boolean v2, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v2, :cond_3

    .line 223
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/chaton/io/entry/PushEntry;->message:Ljava/lang/String;

    .line 224
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    .line 225
    const/4 v5, 0x3

    if-le v2, v5, :cond_2

    .line 226
    const/4 v2, 0x3

    .line 228
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 229
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "InboxNO: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", MsgID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/chaton/io/entry/PushEntry;->msgID:Ljava/lang/Long;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", sender: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", MsgType: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", unreadCount: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/receiver/a;->a:Lcom/sec/chaton/receiver/PushReceiver;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_3
    sget-boolean v2, Lcom/sec/chaton/util/y;->f:Z

    if-eqz v2, :cond_4

    .line 233
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    const-string v3, "ch@t[c <~~ s]push received= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "MsgID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/chaton/io/entry/PushEntry;->msgID:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "SessionID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/chaton/io/entry/PushEntry;->sessionID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/receiver/a;->a:Lcom/sec/chaton/receiver/PushReceiver;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/receiver/a;->a:Lcom/sec/chaton/receiver/PushReceiver;

    invoke-static {v2}, Lcom/sec/chaton/receiver/PushReceiver;->a(Lcom/sec/chaton/receiver/PushReceiver;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v2

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/chaton/io/entry/PushEntry;->message:Ljava/lang/String;

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->d()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/sec/chaton/io/entry/PushEntry;->msgID:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v21

    iget-object v10, v0, Lcom/sec/chaton/io/entry/PushEntry;->sessionID:Ljava/lang/String;

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->h()Z

    move-result v12

    move-object/from16 v0, v21

    iget-object v13, v0, Lcom/sec/chaton/io/entry/PushEntry;->sentTime:Ljava/lang/Long;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    move-object/from16 v0, v21

    iget-object v15, v0, Lcom/sec/chaton/io/entry/PushEntry;->IP:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/chaton/io/entry/PushEntry;->PORT:Ljava/lang/Integer;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v16

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/chaton/io/entry/PushEntry;->receiverCount:Ljava/lang/Integer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->i()J

    move-result-wide v18

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/chaton/io/entry/PushEntry;->truncated:Ljava/lang/Boolean;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v20

    const/16 v22, 0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_9

    const-string v20, "Y"

    :goto_1
    invoke-virtual/range {v2 .. v20}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/sec/chaton/e/w;ILjava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IIJLjava/lang/String;)V

    .line 265
    :cond_5
    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/sec/chaton/io/entry/PushEntry;->chatType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v7

    .line 266
    invoke-static {}, Lcom/sec/chaton/receiver/PushReceiver;->a()Lcom/sec/chaton/chat/fi;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/chat/notification/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 269
    invoke-static {}, Lcom/sec/chaton/receiver/PushReceiver;->a()Lcom/sec/chaton/chat/fi;

    move-result-object v2

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/sec/chaton/io/entry/PushEntry;->IP:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/chaton/io/entry/PushEntry;->PORT:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/sec/chaton/io/entry/PushEntry;->sessionID:Ljava/lang/String;

    invoke-interface/range {v2 .. v7}, Lcom/sec/chaton/chat/fi;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/r;)V

    .line 271
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "chaton_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {}, Lcom/sec/chaton/receiver/PushReceiver;->a()Lcom/sec/chaton/chat/fi;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/chaton/chat/fi;->s()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->f()Z

    move-result v2

    if-nez v2, :cond_6

    .line 274
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/e/a/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 275
    invoke-static {}, Lcom/sec/chaton/receiver/PushReceiver;->a()Lcom/sec/chaton/chat/fi;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/chaton/io/entry/PushEntry;->message:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/sec/chaton/io/entry/PushEntry;->msgType:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v6

    invoke-interface {v3, v4, v5, v2, v6}, Lcom/sec/chaton/chat/fi;->a(ILjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;)V

    .line 293
    :cond_6
    :goto_2
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 295
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/chaton/d/a;->a(Ljava/lang/String;)V

    .line 296
    const-string v2, "push.notiAck( )"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/receiver/a;->a:Lcom/sec/chaton/receiver/PushReceiver;

    invoke-static {v2}, Lcom/sec/chaton/receiver/PushReceiver;->b(Lcom/sec/chaton/receiver/PushReceiver;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 302
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/receiver/a;->a:Lcom/sec/chaton/receiver/PushReceiver;

    invoke-static {v2}, Lcom/sec/chaton/receiver/PushReceiver;->b(Lcom/sec/chaton/receiver/PushReceiver;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 304
    :cond_8
    return-void

    .line 182
    :pswitch_0
    const-string v2, "inboxNO"

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    const-string v2, "chatType"

    sget-object v5, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v5}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 184
    const-string v2, "fromPush"

    const/4 v5, 0x1

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 186
    const-string v2, "buddyNO"

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v9

    goto/16 :goto_0

    .line 191
    :pswitch_1
    const-string v2, "inboxNO"

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    const-string v2, "chatType"

    sget-object v5, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v5}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 193
    const-string v2, "fromPush"

    const/4 v5, 0x1

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 195
    const-string v2, "buddyNO"

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/chaton/io/entry/PushEntry;->senderID:Ljava/lang/String;

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v9

    goto/16 :goto_0

    .line 202
    :pswitch_2
    const-string v2, "inboxNO"

    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    const-string v2, "chatType"

    sget-object v5, Lcom/sec/chaton/e/r;->f:Lcom/sec/chaton/e/r;

    invoke-virtual {v5}, Lcom/sec/chaton/e/r;->a()I

    move-result v5

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 204
    const-string v2, "fromPush"

    const/4 v5, 0x1

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 205
    const-string v2, "Content"

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/chaton/io/entry/PushEntry;->message:Ljava/lang/String;

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    const-string v2, "com.sec.chaton.action.NOTI_WEB"

    invoke-virtual {v4, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/sec/chaton/io/entry/PushEntry;->chatType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v9

    goto/16 :goto_0

    .line 241
    :cond_9
    const-string v20, "N"

    goto/16 :goto_1

    .line 284
    :cond_a
    invoke-virtual/range {p3 .. p3}, Lcom/sec/chaton/e/b/p;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/d/o;->d(Ljava/lang/String;)Lcom/sec/chaton/d/o;

    move-result-object v2

    .line 285
    if-eqz v2, :cond_6

    .line 286
    invoke-virtual {v2}, Lcom/sec/chaton/d/o;->j()V

    goto/16 :goto_2

    .line 179
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
