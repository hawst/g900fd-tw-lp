.class public Lcom/sec/chaton/receiver/CallLogReceiveService;
.super Landroid/app/IntentService;
.source "CallLogReceiveService.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/chaton/receiver/CallLogReceiveService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/receiver/CallLogReceiveService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/chaton/receiver/CallLogReceiveService;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    .line 24
    const-string v0, "calllog_type"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 25
    const-string v0, "calllog_data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 26
    if-nez v2, :cond_1

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 31
    const/4 v3, 0x0

    .line 32
    new-instance v5, Lcom/sec/chaton/calllog/manager/a;

    invoke-direct {v5}, Lcom/sec/chaton/calllog/manager/a;-><init>()V

    .line 34
    const/4 v0, -0x1

    .line 37
    if-eqz v1, :cond_7

    .line 38
    invoke-virtual {v5}, Lcom/sec/chaton/calllog/manager/a;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    move v1, v0

    .line 41
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/coolots/sso/calllog/ChatONCallLogData;

    .line 43
    invoke-virtual {v0, v1}, Lcom/coolots/sso/calllog/ChatONCallLogData;->a(I)V

    .line 44
    new-instance v2, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;

    invoke-direct {v2, v0}, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;-><init>(Lcom/coolots/sso/calllog/ChatONCallLogData;)V

    .line 45
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    iget v0, v2, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    if-ne v0, v7, :cond_6

    move-object v0, v2

    :goto_3
    move-object v3, v0

    .line 51
    goto :goto_2

    .line 53
    :cond_2
    invoke-virtual {v5, v4}, Lcom/sec/chaton/calllog/manager/a;->a(Ljava/util/ArrayList;)I

    move-result v0

    .line 54
    if-gez v0, :cond_3

    .line 55
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "fail to insert data"

    sget-object v1, Lcom/sec/chaton/receiver/CallLogReceiveService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 61
    const-string v0, "succeed to insert data"

    sget-object v1, Lcom/sec/chaton/receiver/CallLogReceiveService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_4
    if-eqz v3, :cond_0

    .line 65
    invoke-static {}, Lcom/sec/chaton/calllog/manager/noti/a;->a()Lcom/sec/chaton/calllog/manager/noti/a;

    move-result-object v0

    .line 67
    iget v1, v3, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    if-ne v1, v7, :cond_5

    .line 68
    iget v1, v3, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->duration:I

    int-to-long v1, v1

    iget-object v3, v3, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/calllog/manager/noti/a;->a(JLjava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_5
    iget v1, v3, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    if-nez v1, :cond_0

    iget v1, v3, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->callmethod:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    .line 70
    iget v1, v3, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->duration:I

    int-to-long v1, v1

    iget-object v4, v3, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    iget-object v3, v3, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/chaton/calllog/manager/noti/a;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    move-object v0, v3

    goto :goto_3

    :cond_7
    move v1, v0

    goto :goto_1
.end method
