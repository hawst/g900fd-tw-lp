.class public Lcom/sec/chaton/AdminMenu;
.super Lcom/sec/chaton/base/BaseActivity;
.source "AdminMenu.java"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/Button;

.field private B:Landroid/widget/Button;

.field private C:Landroid/widget/Button;

.field private D:Landroid/content/Context;

.field private E:Landroid/app/ProgressDialog;

.field private F:Z

.field private G:Landroid/widget/TabHost;

.field private H:Landroid/widget/EditText;

.field private I:Landroid/widget/EditText;

.field private J:Z

.field private K:I

.field private L:Landroid/widget/EditText;

.field private M:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private N:Lcom/sec/chaton/d/o;

.field private O:I

.field private P:I

.field private Q:Landroid/os/Handler;

.field private R:Landroid/view/View$OnClickListener;

.field final a:Landroid/os/Handler;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/EditText;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/EditText;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/Button;

.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/Button;

.field private o:Landroid/widget/Button;

.field private p:Landroid/widget/Button;

.field private q:Landroid/widget/Button;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/Button;

.field private t:Landroid/widget/Button;

.field private u:Landroid/widget/Button;

.field private v:Landroid/widget/Button;

.field private w:Landroid/widget/Button;

.field private x:Landroid/widget/Button;

.field private y:Landroid/widget/Button;

.field private z:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 75
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "chatondev@samsung.com"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/AdminMenu;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->E:Landroid/app/ProgressDialog;

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/AdminMenu;->F:Z

    .line 328
    new-instance v0, Lcom/sec/chaton/c;

    invoke-direct {v0, p0}, Lcom/sec/chaton/c;-><init>(Lcom/sec/chaton/AdminMenu;)V

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    .line 574
    new-instance v0, Lcom/sec/chaton/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/d;-><init>(Lcom/sec/chaton/AdminMenu;)V

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->a:Landroid/os/Handler;

    .line 854
    return-void
.end method

.method static synthetic A(Lcom/sec/chaton/AdminMenu;)Lcom/sec/chaton/d/o;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->N:Lcom/sec/chaton/d/o;

    return-object v0
.end method

.method static synthetic B(Lcom/sec/chaton/AdminMenu;)I
    .locals 2

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/chaton/AdminMenu;->O:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/chaton/AdminMenu;->O:I

    return v0
.end method

.method static synthetic C(Lcom/sec/chaton/AdminMenu;)I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/chaton/AdminMenu;->P:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/AdminMenu;I)I
    .locals 0

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/chaton/AdminMenu;->O:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/AdminMenu;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/chaton/AdminMenu;->E:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->r:Landroid/widget/Button;

    return-object v0
.end method

.method public static a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 747
    const-string v0, "TEST"

    const-string v2, "copyPreferenceToSdcard start"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    new-instance v2, Ljava/io/File;

    const-string v0, "data/data/com.sec.chaton/shared_prefs/"

    const-string v3, "ChatON.xml"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 758
    :goto_0
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/chaton.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v1, v2

    .line 763
    :goto_1
    const/4 v2, 0x1

    new-array v2, v2, [B

    .line 766
    if-eqz v0, :cond_1

    .line 767
    :cond_0
    :goto_2
    :try_start_2
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_1

    .line 768
    if-eqz v1, :cond_0

    .line 769
    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 773
    :catch_0
    move-exception v2

    .line 775
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 778
    :cond_1
    if-eqz v1, :cond_2

    .line 779
    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 786
    :cond_2
    :goto_3
    if-eqz v0, :cond_3

    .line 787
    :try_start_4
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 789
    :cond_3
    if-eqz v1, :cond_4

    .line 790
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 797
    :cond_4
    :goto_4
    if-eqz v0, :cond_5

    .line 798
    :try_start_5
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 800
    :cond_5
    if-eqz v1, :cond_6

    .line 801
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 807
    :cond_6
    :goto_5
    const-string v0, "TEST"

    const-string v1, "copyPreferenceToSdcard end"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    return-void

    .line 752
    :catch_1
    move-exception v0

    .line 754
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    .line 759
    :catch_2
    move-exception v2

    .line 761
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 781
    :catch_3
    move-exception v2

    .line 783
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 792
    :catch_4
    move-exception v2

    .line 794
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 803
    :catch_5
    move-exception v0

    .line 805
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5
.end method

.method static synthetic a(Lcom/sec/chaton/AdminMenu;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/chaton/AdminMenu;->F:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/AdminMenu;I)I
    .locals 0

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/chaton/AdminMenu;->P:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/AdminMenu;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->M:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static b()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 811
    const-string v0, "TEST"

    const-string v1, "copyAutoResendTraceToSdcard start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/msgsend/q;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 814
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 815
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v3, "try to copy to sdcard, but not exist. make empty trace file now"

    invoke-static {v1, v3}, Lcom/sec/chaton/msgsend/q;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 821
    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 822
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 823
    const/16 v0, 0x400

    :try_start_2
    new-array v0, v0, [B

    .line 825
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_3

    .line 826
    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 829
    :catch_0
    move-exception v0

    move-object v2, v3

    .line 830
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 834
    if-eqz v1, :cond_1

    .line 836
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 842
    :cond_1
    :goto_2
    if-eqz v2, :cond_2

    .line 844
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 851
    :cond_2
    :goto_3
    const-string v0, "TEST"

    const-string v1, "copyAutoResendTraceToSdcard end"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    return-void

    .line 828
    :cond_3
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 834
    if-eqz v1, :cond_4

    .line 836
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9

    .line 842
    :cond_4
    :goto_4
    if-eqz v3, :cond_2

    .line 844
    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_3

    .line 845
    :catch_1
    move-exception v0

    .line 847
    :goto_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 834
    :catchall_0
    move-exception v0

    move-object v3, v2

    :goto_6
    if-eqz v2, :cond_5

    .line 836
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .line 842
    :cond_5
    :goto_7
    if-eqz v3, :cond_6

    .line 844
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    .line 834
    :cond_6
    :goto_8
    throw v0

    .line 837
    :catch_2
    move-exception v1

    .line 839
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 845
    :catch_3
    move-exception v1

    .line 847
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 837
    :catch_4
    move-exception v0

    .line 839
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 845
    :catch_5
    move-exception v0

    goto :goto_5

    .line 831
    :catch_6
    move-exception v0

    move-object v3, v2

    .line 834
    :goto_9
    if-eqz v2, :cond_7

    .line 836
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 842
    :cond_7
    :goto_a
    if-eqz v3, :cond_2

    .line 844
    :try_start_c
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    goto :goto_3

    .line 845
    :catch_7
    move-exception v0

    goto :goto_5

    .line 837
    :catch_8
    move-exception v0

    .line 839
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 837
    :catch_9
    move-exception v0

    .line 839
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 834
    :catchall_1
    move-exception v0

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_6

    :catchall_3
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    goto :goto_6

    .line 831
    :catch_a
    move-exception v0

    goto :goto_9

    :catch_b
    move-exception v0

    move-object v2, v1

    goto :goto_9

    .line 829
    :catch_c
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :catch_d
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/chaton/AdminMenu;I)I
    .locals 0

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/chaton/AdminMenu;->K:I

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/AdminMenu;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/AdminMenu;->f()V

    return-void
.end method

.method static synthetic c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/chaton/AdminMenu;->b:[Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 599
    new-instance v1, Lcom/sec/chaton/widget/f;

    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->D:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/chaton/widget/f;-><init>(Landroid/content/Context;)V

    .line 600
    const-string v0, "DB File SendEmail"

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/f;->setTitle(Ljava/lang/CharSequence;)V

    .line 601
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->D:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 602
    const v2, 0x7f03000b

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 604
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/chaton/widget/f;->setInverseBackgroundForced(Z)V

    .line 605
    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/f;->setView(Landroid/view/View;)V

    .line 607
    const v2, 0x7f07002e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->L:Landroid/widget/EditText;

    .line 609
    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/sec/chaton/AdminMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0037

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/e;

    invoke-direct {v3, p0}, Lcom/sec/chaton/e;-><init>(Lcom/sec/chaton/AdminMenu;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/widget/f;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 650
    const/4 v0, -0x2

    invoke-virtual {p0}, Lcom/sec/chaton/AdminMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/f;

    invoke-direct {v3, p0}, Lcom/sec/chaton/f;-><init>(Lcom/sec/chaton/AdminMenu;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/widget/f;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 658
    invoke-virtual {v1, v4}, Lcom/sec/chaton/widget/f;->setCancelable(Z)V

    .line 659
    invoke-virtual {v1, v4}, Lcom/sec/chaton/widget/f;->setCanceledOnTouchOutside(Z)V

    .line 660
    invoke-virtual {v1}, Lcom/sec/chaton/widget/f;->show()V

    .line 662
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/AdminMenu;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/AdminMenu;->h()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/AdminMenu;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->D:Landroid/content/Context;

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 665
    new-instance v1, Lcom/sec/chaton/widget/f;

    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->D:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/chaton/widget/f;-><init>(Landroid/content/Context;)V

    .line 666
    const-string v0, "Enter Password"

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/f;->setTitle(Ljava/lang/CharSequence;)V

    .line 667
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->D:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 668
    const v2, 0x7f03000c

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 670
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/widget/f;->setInverseBackgroundForced(Z)V

    .line 671
    invoke-virtual {v1, v2}, Lcom/sec/chaton/widget/f;->setView(Landroid/view/View;)V

    .line 673
    const v0, 0x7f07002f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->H:Landroid/widget/EditText;

    .line 674
    const v0, 0x7f070030

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->I:Landroid/widget/EditText;

    .line 676
    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/sec/chaton/AdminMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0037

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/g;

    invoke-direct {v3, p0}, Lcom/sec/chaton/g;-><init>(Lcom/sec/chaton/AdminMenu;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/widget/f;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 712
    const/4 v0, -0x2

    invoke-virtual {p0}, Lcom/sec/chaton/AdminMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/h;

    invoke-direct {v3, p0}, Lcom/sec/chaton/h;-><init>(Lcom/sec/chaton/AdminMenu;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/chaton/widget/f;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 720
    invoke-virtual {v1, v4}, Lcom/sec/chaton/widget/f;->setCancelable(Z)V

    .line 721
    invoke-virtual {v1, v4}, Lcom/sec/chaton/widget/f;->setCanceledOnTouchOutside(Z)V

    .line 722
    invoke-virtual {v1}, Lcom/sec/chaton/widget/f;->show()V

    .line 724
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/AdminMenu;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->E:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private f()V
    .locals 6

    .prologue
    .line 868
    const/16 v1, 0xc7

    .line 872
    const-string v2, "827799990"

    .line 873
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 874
    const/16 v3, 0xa

    if-ge v0, v3, :cond_0

    .line 875
    iget-object v3, p0, Lcom/sec/chaton/AdminMenu;->M:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "00"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 873
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 876
    :cond_0
    const/16 v3, 0x64

    if-ge v0, v3, :cond_1

    .line 877
    iget-object v3, p0, Lcom/sec/chaton/AdminMenu;->M:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 879
    :cond_1
    iget-object v3, p0, Lcom/sec/chaton/AdminMenu;->M:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 882
    :cond_2
    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/AdminMenu;)I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/chaton/AdminMenu;->K:I

    return v0
.end method

.method private g()V
    .locals 7

    .prologue
    .line 885
    invoke-static {}, Lcom/sec/chaton/util/bd;->a()Ljava/lang/String;

    move-result-object v1

    .line 886
    sget-object v3, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    .line 887
    invoke-static {v1, v3}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/r;)Lcom/sec/chaton/d/o;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->N:Lcom/sec/chaton/d/o;

    .line 889
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->N:Lcom/sec/chaton/d/o;

    if-eqz v0, :cond_0

    .line 890
    const/16 v0, 0xc7

    new-array v4, v0, [Ljava/lang/String;

    .line 891
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->N:Lcom/sec/chaton/d/o;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/o;->e(Ljava/lang/String;)V

    .line 892
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->N:Lcom/sec/chaton/d/o;

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/util/bk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/bi;->a()Lcom/sec/chaton/util/bi;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bi;->c()Lcom/sec/chaton/util/bk;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/util/bk;->b()I

    move-result v5

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 897
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->N:Lcom/sec/chaton/d/o;

    iget-object v2, p0, Lcom/sec/chaton/AdminMenu;->Q:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/o;->a(Landroid/os/Handler;)Z

    .line 898
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->N:Lcom/sec/chaton/d/o;

    invoke-virtual {v0}, Lcom/sec/chaton/d/o;->k()V

    .line 900
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 901
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->N:Lcom/sec/chaton/d/o;

    sget-object v2, Lcom/sec/chaton/e/w;->b:Lcom/sec/chaton/e/w;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "i-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/chaton/AdminMenu;->O:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/d/o;->a(Ljava/lang/String;Lcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    .line 903
    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->n:Landroid/widget/Button;

    return-object v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 906
    new-instance v0, Lcom/sec/chaton/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/i;-><init>(Lcom/sec/chaton/AdminMenu;)V

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->Q:Landroid/os/Handler;

    .line 990
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/AdminMenu;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 994
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->D:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 995
    const v1, 0x7f0300a3

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 997
    const v0, 0x7f07030b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 998
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "admin_mcc"

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1000
    iget-object v2, p0, Lcom/sec/chaton/AdminMenu;->D:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b003b

    new-instance v3, Lcom/sec/chaton/b;

    invoke-direct {v3, p0, v0}, Lcom/sec/chaton/b;-><init>(Lcom/sec/chaton/AdminMenu;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/j;

    invoke-direct {v2, p0}, Lcom/sec/chaton/j;-><init>(Lcom/sec/chaton/AdminMenu;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 1020
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1022
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->d:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->f:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->t:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->u:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->v:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/chaton/AdminMenu;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/AdminMenu;->i()V

    return-void
.end method

.method static synthetic r(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->x:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic s(Lcom/sec/chaton/AdminMenu;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->y:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic t(Lcom/sec/chaton/AdminMenu;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/AdminMenu;->d()V

    return-void
.end method

.method static synthetic u(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->L:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic v(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->H:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic w(Lcom/sec/chaton/AdminMenu;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->I:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic x(Lcom/sec/chaton/AdminMenu;)Landroid/widget/TabHost;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    return-object v0
.end method

.method static synthetic y(Lcom/sec/chaton/AdminMenu;)I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/chaton/AdminMenu;->O:I

    return v0
.end method

.method static synthetic z(Lcom/sec/chaton/AdminMenu;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/chaton/AdminMenu;->g()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 733
    const-string v0, ""

    .line 735
    :try_start_0
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 736
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 737
    new-instance v2, Ljava/math/BigInteger;

    const/4 v3, 0x1

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    invoke-direct {v2, v3, v1}, Ljava/math/BigInteger;-><init>(I[B)V

    .line 738
    const/16 v1, 0x10

    invoke-virtual {v2, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 743
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 739
    :catch_0
    move-exception v1

    .line 741
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 149
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 152
    iput-object p0, p0, Lcom/sec/chaton/AdminMenu;->D:Landroid/content/Context;

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->M:Ljava/util/ArrayList;

    .line 154
    const v0, 0x7f03000d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->setContentView(I)V

    .line 155
    const v0, 0x7f070031

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    const-string v1, "Tab1"

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    .line 160
    const-string v1, "GENERAL"

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    .line 161
    const v1, 0x7f070032

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 162
    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    const-string v1, "Tab2"

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    .line 166
    const-string v1, "SQE"

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    .line 167
    const v1, 0x7f070036

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 169
    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    const-string v1, "Tab3"

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    .line 173
    const-string v1, "DEV"

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    .line 174
    const v1, 0x7f070038

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 175
    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/chaton/AdminMenu;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 181
    const v0, 0x7f07003a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->c:Landroid/widget/EditText;

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->c:Landroid/widget/EditText;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 183
    const v0, 0x7f07003b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->d:Landroid/widget/EditText;

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->d:Landroid/widget/EditText;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 185
    const v0, 0x7f07003c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->e:Landroid/widget/EditText;

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->e:Landroid/widget/EditText;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 187
    const v0, 0x7f07003d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->f:Landroid/widget/EditText;

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->f:Landroid/widget/EditText;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 189
    const v0, 0x7f07003e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->g:Landroid/widget/Button;

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->g:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    const v0, 0x7f07003f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->h:Landroid/widget/Button;

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    const v0, 0x7f070040

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->i:Landroid/widget/Button;

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->i:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    const v0, 0x7f070041

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->j:Landroid/widget/Button;

    .line 196
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->j:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->k:Landroid/widget/TextView;

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->k:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    const v0, 0x7f070042

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->l:Landroid/widget/Button;

    .line 201
    const v0, 0x7f070043

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->m:Landroid/widget/Button;

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->l:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->m:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    const v0, 0x7f070035

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->x:Landroid/widget/Button;

    .line 207
    const v0, 0x7f070044

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->y:Landroid/widget/Button;

    .line 208
    const v0, 0x7f070045

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->z:Landroid/widget/Button;

    .line 209
    const v0, 0x7f070047

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->A:Landroid/widget/Button;

    .line 210
    const v0, 0x7f070046

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->B:Landroid/widget/Button;

    .line 211
    const v0, 0x7f070048

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->C:Landroid/widget/Button;

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->x:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->y:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->z:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->A:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->B:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->C:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->x:Landroid/widget/Button;

    const-string v1, "A-Resend:ENABLED"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 224
    :goto_0
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->y:Landroid/widget/Button;

    const-string v1, "A-Resend:Noti enabled"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 232
    :goto_1
    const v0, 0x7f070049

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->o:Landroid/widget/Button;

    .line 233
    const v0, 0x7f07004a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->p:Landroid/widget/Button;

    .line 234
    const v0, 0x7f07004b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->q:Landroid/widget/Button;

    .line 237
    const v0, 0x7f07004c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->t:Landroid/widget/Button;

    .line 238
    const v0, 0x7f07004d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->u:Landroid/widget/Button;

    .line 239
    const v0, 0x7f07004e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->v:Landroid/widget/Button;

    .line 241
    const v0, 0x7f07004f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->w:Landroid/widget/Button;

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->o:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->p:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->q:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->t:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->u:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->v:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->w:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 254
    const v0, 0x7f070034

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->r:Landroid/widget/Button;

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->r:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    .line 257
    new-instance v1, Lcom/sec/chaton/a;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v1, p0, v4}, Lcom/sec/chaton/a;-><init>(Lcom/sec/chaton/AdminMenu;Landroid/os/Looper;)V

    .line 283
    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->c(Landroid/os/Handler;)V

    .line 285
    const v0, 0x7f070033

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->n:Landroid/widget/Button;

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->n:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 287
    invoke-static {}, Lcom/sec/chaton/util/y;->b()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/AdminMenu;->K:I

    .line 288
    invoke-static {v5}, Lcom/sec/chaton/util/y;->a(I)V

    .line 290
    iget v0, p0, Lcom/sec/chaton/AdminMenu;->K:I

    if-lez v0, :cond_3

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->n:Landroid/widget/Button;

    const-string v1, "Log Off"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 295
    :goto_2
    iget-boolean v0, p0, Lcom/sec/chaton/AdminMenu;->J:Z

    if-ne v0, v5, :cond_0

    .line 296
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->p:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 297
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->o:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 301
    :cond_0
    const v0, 0x7f070037

    invoke-virtual {p0, v0}, Lcom/sec/chaton/AdminMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/AdminMenu;->s:Landroid/widget/Button;

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->s:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/chaton/AdminMenu;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setVisibility(I)V

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    move v1, v2

    .line 308
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabWidget;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x1020016

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 310
    const-string v4, "#000000"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 308
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->x:Landroid/widget/Button;

    const-string v1, "A-Resend:DISABLED"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->y:Landroid/widget/Button;

    const-string v1, "A-Resend:Noti disabled"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 293
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->n:Landroid/widget/Button;

    const-string v1, "Log On"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 312
    :cond_4
    if-eqz v3, :cond_6

    .line 313
    const-string v0, "mapping_mode"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/AdminMenu;->J:Z

    .line 315
    const-string v0, "general_tab"

    invoke-virtual {v3, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 316
    iget-object v0, p0, Lcom/sec/chaton/AdminMenu;->G:Landroid/widget/TabHost;

    invoke-virtual {v0, v2}, Landroid/widget/TabHost;->setVisibility(I)V

    .line 325
    :goto_4
    return-void

    .line 318
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/AdminMenu;->e()V

    goto :goto_4

    .line 322
    :cond_6
    invoke-direct {p0}, Lcom/sec/chaton/AdminMenu;->e()V

    goto :goto_4
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 730
    return-void
.end method
