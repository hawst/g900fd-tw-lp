.class Lcom/sec/chaton/bm;
.super Landroid/os/Handler;
.source "TabActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/TabActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 2305
    iput-object p1, p0, Lcom/sec/chaton/bm;->a:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2308
    iget-object v0, p0, Lcom/sec/chaton/bm;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->F(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2340
    :goto_0
    return-void

    .line 2311
    :cond_0
    const-string v0, "2"

    .line 2313
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 2314
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/AvaliableApps;

    .line 2316
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_4

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2318
    const-string v0, "spp_update_is"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2319
    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->version:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2320
    const-string v0, "spp_latest_ver"

    iget-object v1, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->version:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2324
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateIsCritical"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2325
    const-string v0, "ChatON updating process is still progressing."

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2329
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/bm;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->E(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2330
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/bm;->a:Lcom/sec/chaton/TabActivity;

    const-class v2, Lcom/sec/chaton/SPPUpgradeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2331
    iget-object v1, p0, Lcom/sec/chaton/bm;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2333
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/bm;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, v3}, Lcom/sec/chaton/TabActivity;->f(Lcom/sec/chaton/TabActivity;Z)Z

    goto :goto_0

    .line 2337
    :cond_4
    const-string v0, "There is no update of SPPPushClient."

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
