.class Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;
.super Ljava/lang/Object;
.source "PrefFragmentSuggestion.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/high16 v5, 0x7f0e0000

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 208
    if-eqz p2, :cond_1

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z
    invoke-static {v0, v3}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$402(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;Z)Z

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$500(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 212
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 213
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100003"

    const-string v2, "00000001"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->control:Lcom/sec/chaton/d/h;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$600(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/chaton/d/h;->g(Z)V

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 230
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$000(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0089

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 232
    return v3

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z
    invoke-static {v0, v4}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$402(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;Z)Z

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$500(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 222
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 223
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100003"

    const-string v2, "00000002"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->control:Lcom/sec/chaton/d/h;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$600(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/chaton/d/h;->g(Z)V

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method
