.class Lcom/sec/chaton/settings2/PrefFragmentHide$1;
.super Ljava/lang/Object;
.source "PrefFragmentHide.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentHide;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentHide;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentHide$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentHide;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 104
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 119
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 106
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentHide$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentHide;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentHide;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/hide/HideListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    const-string v1, "hide_buddy_list"

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentHide$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentHide;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentHide;->access$000(Lcom/sec/chaton/settings2/PrefFragmentHide;)Lcom/sec/chaton/hide/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/hide/g;->e()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 108
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentHide$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentHide;

    const/16 v2, 0xc8

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings2/PrefFragmentHide;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 111
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentHide$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentHide;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentHide;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 112
    const-string v1, "BUDDY_SORT_STYLE"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 113
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 114
    const-string v1, "ACTIVITY_PURPOSE_ARG2"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 115
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentHide$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentHide;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings2/PrefFragmentHide;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x7f0705b0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
