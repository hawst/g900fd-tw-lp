.class public Lcom/sec/chaton/settings2/PrefFragmentBackupList;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentBackupList.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mBackupListView:Lcom/sec/chaton/localbackup/BackupListView;

.field private mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

.field private mListener:Lcom/sec/chaton/settings2/PrefFragmentBackupList$OnBackKeyListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/chaton/localbackup/ActivityBackupList;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 42
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentBackupList;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->handleExternalStorageState()V

    return-void
.end method

.method private handleExternalStorageState()V
    .locals 3

    .prologue
    .line 86
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 91
    :cond_0
    return-void
.end method

.method private startWatchingExternalStorage()V
    .locals 3

    .prologue
    .line 70
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentBackupList$1;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentBackupList$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentBackupList;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    .line 77
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 78
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 79
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 80
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->handleExternalStorageState()V

    .line 83
    return-void
.end method

.method private stopWatchingExternalStorage()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 102
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->mActivity:Landroid/app/Activity;

    .line 36
    if-eqz p1, :cond_0

    .line 40
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/sec/chaton/localbackup/BackupListView;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/BackupListView;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->mBackupListView:Lcom/sec/chaton/localbackup/BackupListView;

    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 96
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPause()V

    .line 97
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->stopWatchingExternalStorage()V

    .line 98
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackupList;->startWatchingExternalStorage()V

    .line 66
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 67
    return-void
.end method
