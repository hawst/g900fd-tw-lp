.class Lcom/sec/chaton/settings2/PrefFragmentAbout$4;
.super Landroid/os/Handler;
.source "PrefFragmentAbout.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentAbout;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10

    .prologue
    const v9, 0x7f07033c

    const v8, 0x7f07033b

    const v7, 0x7f07033a

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 298
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 299
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 391
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 301
    :pswitch_1
    const-string v1, "2"

    .line 302
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/AvaliableApps;

    .line 304
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_3

    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->resultCode:Ljava/lang/String;

    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 305
    const-string v0, "spp_update_is"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 308
    const-string v0, "com.coolots.chaton"

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$000(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    .line 315
    :goto_1
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionCurrent:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v3

    sget v4, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v3, v4

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v3

    sget v4, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v3, v4

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v3

    sget v4, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v3, v4

    .line 319
    iget-object v0, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->version:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 320
    const-string v0, "spp_latest_ver"

    iget-object v2, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->version:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v0

    sget v2, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    aget-object v0, v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v3}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/chaton/io/entry/AvaliableApps;->version:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 326
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAbout;->googleAppsUpdate(I)V
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$400(Lcom/sec/chaton/settings2/PrefFragmentAbout;I)V

    goto/16 :goto_0

    .line 311
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$000(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_1

    .line 323
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    aget-object v0, v0, v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 329
    :cond_3
    const-string v0, "There is no update of SPPPushClient."

    const-string v1, "ActivityAbout"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 338
    :pswitch_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_7

    .line 339
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetVersionNotice;

    .line 340
    if-eqz v0, :cond_0

    .line 341
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 344
    iget-object v1, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 346
    invoke-static {}, Lcom/sec/chaton/util/am;->s()V

    .line 347
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAbout;->dismissVersionUpgradeDialog()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$500(Lcom/sec/chaton/settings2/PrefFragmentAbout;)V

    goto/16 :goto_0

    .line 351
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$000(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 354
    if-eqz v2, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 355
    const v1, 0x7f07014d

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 356
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionCurrent:[Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v3, v5

    .line 357
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v3, v5

    .line 358
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    aput-object v1, v3, v5

    .line 359
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionCurrent:[Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v1

    aget-object v1, v1, v5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v3}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0043

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v3, v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 360
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v1

    aget-object v2, v1, v5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v3}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    aget-object v1, v1, v5

    :goto_3
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v1

    aget-object v1, v1, v5

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 362
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAbout;->googleAppsUpdate(I)V
    invoke-static {v1, v5}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$400(Lcom/sec/chaton/settings2/PrefFragmentAbout;I)V

    .line 367
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "UpdateTargetVersion"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->newversion:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "UpdateIsCritical"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->critical:Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 369
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "UpdateIsNormal"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->uptodate:Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 370
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "UpdateUrl"

    iget-object v3, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->downloadurl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "SamsungappsUrl"

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetVersionNotice;->samsungappsurl:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 360
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    aget-object v1, v1, v5

    goto :goto_3

    .line 381
    :cond_7
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 383
    const-string v0, "network fail to get version"

    const-string v1, "ActivityAbout"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 299
    :pswitch_data_0
    .packed-switch 0x450
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
