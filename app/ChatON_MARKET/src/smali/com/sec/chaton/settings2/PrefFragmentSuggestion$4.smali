.class Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;
.super Landroid/os/Handler;
.source "PrefFragmentSuggestion.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 392
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 394
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$000(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 400
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v1, :cond_3

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$500(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_2

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z
    invoke-static {v0, v4}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$402(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;Z)Z

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$500(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 410
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemSuggestions:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$800(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$400(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 413
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 419
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$000(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0089

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 405
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z
    invoke-static {v0, v3}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$402(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;Z)Z

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$500(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "recomned_receive"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    .line 414
    :catch_0
    move-exception v0

    .line 415
    const-string v0, "Exception occurred while trying to dismiss progress dialog"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 435
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 439
    :goto_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 440
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-eq v1, v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$000(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 436
    :catch_1
    move-exception v0

    .line 437
    const-string v0, "Exception occurred while trying to dismiss progress dialog"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method
