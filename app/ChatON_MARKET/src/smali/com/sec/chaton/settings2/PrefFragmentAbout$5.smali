.class Lcom/sec/chaton/settings2/PrefFragmentAbout$5;
.super Ljava/lang/Object;
.source "PrefFragmentAbout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

.field final synthetic val$service:I


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentAbout;I)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iput p2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 488
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 531
    :cond_0
    :goto_0
    return-void

    .line 492
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/cb;->b()Z

    move-result v0

    .line 493
    invoke-static {}, Lcom/sec/chaton/util/cb;->c()Z

    move-result v1

    .line 495
    iget v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    sget v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    if-ne v2, v3, :cond_4

    .line 496
    if-eqz v1, :cond_2

    .line 497
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(I)V

    goto :goto_0

    .line 498
    :cond_2
    if-eqz v0, :cond_3

    .line 499
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(I)V

    goto :goto_0

    .line 502
    :cond_3
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->e(I)V

    goto :goto_0

    .line 506
    :cond_4
    if-eqz v0, :cond_8

    if-eqz v1, :cond_8

    .line 508
    const/4 v0, 0x0

    .line 509
    iget v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    if-nez v1, :cond_6

    .line 510
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/UpgradeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 516
    :cond_5
    :goto_1
    if-eqz v0, :cond_0

    .line 517
    const-string v1, "isCritical"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 518
    const-string v1, "isFromHome"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 520
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 511
    :cond_6
    iget v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    if-ne v1, v4, :cond_7

    .line 512
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/ChatONVUpgradeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 513
    :cond_7
    iget v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 514
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/NewSPPUpgradeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 523
    :cond_8
    if-eqz v0, :cond_9

    if-nez v1, :cond_9

    .line 524
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(I)V

    goto :goto_0

    .line 525
    :cond_9
    if-nez v0, :cond_a

    if-eqz v1, :cond_a

    .line 526
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(I)V

    goto/16 :goto_0

    .line 528
    :cond_a
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;->val$service:I

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->e(I)V

    goto/16 :goto_0
.end method
