.class public Lcom/sec/chaton/settings2/PrefFragmentBlock;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentBlock.java"


# instance fields
.field private impl:Lcom/sec/chaton/block/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 16
    new-instance v0, Lcom/sec/chaton/block/k;

    invoke-direct {v0, p0}, Lcom/sec/chaton/block/k;-><init>(Lcom/sec/chaton/settings2/PrefFragmentBlock;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBlock;->impl:Lcom/sec/chaton/block/k;

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 20
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 21
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBlock;->impl:Lcom/sec/chaton/block/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/block/k;->a(IILandroid/content/Intent;)V

    .line 22
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 28
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBlock;->impl:Lcom/sec/chaton/block/k;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/block/k;->a(Landroid/app/Activity;)V

    .line 29
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBlock;->setHasOptionsMenu(Z)V

    .line 42
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBlock;->impl:Lcom/sec/chaton/block/k;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/block/k;->a(Landroid/os/Bundle;)V

    .line 44
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBlock;->impl:Lcom/sec/chaton/block/k;

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/block/k;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 56
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBlock;->impl:Lcom/sec/chaton/block/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/block/k;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroy()V

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBlock;->impl:Lcom/sec/chaton/block/k;

    invoke-virtual {v0}, Lcom/sec/chaton/block/k;->c()V

    .line 96
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDetach()V

    .line 34
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBlock;->impl:Lcom/sec/chaton/block/k;

    invoke-virtual {v0}, Lcom/sec/chaton/block/k;->a()V

    .line 35
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBlock;->impl:Lcom/sec/chaton/block/k;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/block/k;->a(Landroid/view/Menu;)V

    .line 49
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 50
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBlock;->impl:Lcom/sec/chaton/block/k;

    invoke-virtual {v0}, Lcom/sec/chaton/block/k;->b()V

    .line 88
    return-void
.end method
