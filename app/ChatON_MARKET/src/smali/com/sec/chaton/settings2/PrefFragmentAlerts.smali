.class public Lcom/sec/chaton/settings2/PrefFragmentAlerts;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentAlerts.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final MUTE_HOUR:I = 0x0

.field public static final MUTE_MANUALLY:I = 0x2

.field public static final MUTE_NONE:I = 0x3

.field public static final MUTE_UNTIL_7:I = 0x1

.field public static final POPUP_INPUT:I = 0x1

.field public static final POPUP_SIMPLE:I = 0x0

.field protected static final SET_RINGTONE:I = 0x2


# instance fields
.field private alertTypes:[Ljava/lang/String;

.field am:Landroid/app/AlarmManager;

.field private ctx:Landroid/content/Context;

.field private isAlertNewGroupChat:Z

.field private isDisplayBlackscreenPopup:Z

.field private isDisplayReceiveMessageChecked:Z

.field private isNotificationReceive:Z

.field private mActivity:Landroid/preference/PreferenceActivity;

.field private mCurrentMuteType:I

.field private mCurrentPopupType:I

.field mMuteItemClickListener:Landroid/content/DialogInterface$OnClickListener;

.field mMuteTimeClickListener:Landroid/content/DialogInterface$OnClickListener;

.field mPopupTypeItemClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mSelectedPopupType:I

.field muteTimeDialog:Lcom/sec/chaton/settings/co;

.field muteTypeDialog:Lcom/sec/common/a/d;

.field private optionText:[Ljava/lang/String;

.field prefItemAbout:Lcom/sec/chaton/settings/AboutNewNotice;

.field prefItemAlertNewGroupChat:Landroid/preference/CheckBoxPreference;

.field prefItemBlackscreenPopup:Landroid/preference/CheckBoxPreference;

.field prefItemNotification:Landroid/preference/CheckBoxPreference;

.field private prefItemPopupType:Landroid/preference/Preference;

.field prefItemReceivedMessage:Landroid/preference/CheckBoxPreference;

.field private prefMuteAlert:Landroid/preference/Preference;

.field private prefSettingType:Landroid/preference/Preference;

.field private sharedPref:Lcom/sec/chaton/util/ab;

.field tag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 54
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 56
    const-string v0, "Settings"

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->tag:Ljava/lang/String;

    .line 69
    iput-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 73
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayReceiveMessageChecked:Z

    .line 74
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayBlackscreenPopup:Z

    .line 75
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isAlertNewGroupChat:Z

    .line 76
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isNotificationReceive:Z

    .line 77
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mCurrentMuteType:I

    .line 98
    iput-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTypeDialog:Lcom/sec/common/a/d;

    .line 99
    iput-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTimeDialog:Lcom/sec/chaton/settings/co;

    .line 447
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mMuteItemClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 493
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mMuteTimeClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 564
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$12;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$12;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mPopupTypeItemClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isNotificationReceive:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayReceiveMessageChecked:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Landroid/preference/PreferenceActivity;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mActivity:Landroid/preference/PreferenceActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setNotiMuteType()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->displayManuallyDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Z)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setNotiSettingType(Z)V

    return-void
.end method

.method static synthetic access$302(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayBlackscreenPopup:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mCurrentPopupType:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/chaton/settings2/PrefFragmentAlerts;I)I
    .locals 0

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mCurrentPopupType:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mSelectedPopupType:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/chaton/settings2/PrefFragmentAlerts;I)I
    .locals 0

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mSelectedPopupType:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemPopupType:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isAlertNewGroupChat:Z

    return p1
.end method

.method private displayManuallyDialog()V
    .locals 4

    .prologue
    .line 487
    new-instance v0, Lcom/sec/chaton/settings/co;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/chaton/settings/co;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTimeDialog:Lcom/sec/chaton/settings/co;

    .line 488
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTimeDialog:Lcom/sec/chaton/settings/co;

    const/4 v1, -0x2

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    const v3, 0x7f0b0037

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mMuteTimeClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/settings/co;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 489
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTimeDialog:Lcom/sec/chaton/settings/co;

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    const v3, 0x7f0b0039

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/chaton/settings/co;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 490
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTimeDialog:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/co;->show()V

    .line 491
    return-void
.end method

.method private getFormatedTime(Ljava/lang/Long;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 528
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 529
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initializeForPreference()V
    .locals 7

    .prologue
    const v6, 0x7f08001b

    const v5, 0x7f08003f

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 156
    const-string v0, "pref_item_push_notification"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemNotification:Landroid/preference/CheckBoxPreference;

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting Notification"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 158
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isNotificationReceive:Z

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting Notification"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 164
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemNotification:Landroid/preference/CheckBoxPreference;

    iget-boolean v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isNotificationReceive:Z

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 165
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0194

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemNotification:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentAlerts$1;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 197
    const-string v0, "pref_item_blackscreen_popup"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemBlackscreenPopup:Landroid/preference/CheckBoxPreference;

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting show blackscreen popup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_3

    .line 200
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayBlackscreenPopup:Z

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting show blackscreen popup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 206
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemBlackscreenPopup:Landroid/preference/CheckBoxPreference;

    iget-boolean v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayBlackscreenPopup:Z

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0174

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemBlackscreenPopup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemBlackscreenPopup:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentAlerts$2;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 227
    const-string v0, "pref_item_popup_type"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemPopupType:Landroid/preference/Preference;

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting is simple popup"

    invoke-virtual {v0, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting is simple popup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 235
    :goto_2
    if-eqz v0, :cond_5

    .line 236
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b032c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemPopupType:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 237
    iput v4, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mCurrentPopupType:I

    .line 242
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemPopupType:Landroid/preference/Preference;

    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 273
    const-string v0, "pref_item_alert_group_chat"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemAlertNewGroupChat:Landroid/preference/CheckBoxPreference;

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting alert_new_groupchat"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_6

    .line 275
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isAlertNewGroupChat:Z

    .line 276
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting alert_new_groupchat"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 281
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemAlertNewGroupChat:Landroid/preference/CheckBoxPreference;

    iget-boolean v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isAlertNewGroupChat:Z

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 282
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b01ef

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemAlertNewGroupChat:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemAlertNewGroupChat:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentAlerts$4;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$4;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 311
    const-string v0, "pref_item_received_message"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemReceivedMessage:Landroid/preference/CheckBoxPreference;

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting show receive message"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v1, :cond_7

    .line 314
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayReceiveMessageChecked:Z

    .line 315
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting show receive message"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 320
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemReceivedMessage:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayReceiveMessageChecked:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 321
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemReceivedMessage:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemReceivedMessage:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentAlerts$5;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$5;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 351
    const-string v0, "pref_item_type"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefSettingType:Landroid/preference/Preference;

    .line 352
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Set Type Text"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    if-ne v0, v1, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Set Type Text"

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isNotificationReceive:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setNotiSettingType(Z)V

    .line 358
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefSettingType:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentAlerts$6;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$6;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 375
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/y;->g:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_1

    .line 376
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 377
    const v1, 0x7f0b0144

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 378
    const-string v1, "pref_item_push_status"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 379
    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setOrder(I)V

    .line 380
    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentAlerts$7;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$7;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 392
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 394
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v1

    .line 395
    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Landroid/os/Looper;Landroid/preference/Preference;)V

    .line 424
    invoke-virtual {v1, v2}, Lcom/sec/chaton/d/a;->c(Landroid/os/Handler;)V

    .line 430
    :cond_1
    const-string v0, "pref_item_mute"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefMuteAlert:Landroid/preference/Preference;

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/chat/notification/a;->g()Lcom/sec/chaton/chat/notification/d;

    .line 432
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setNotiMuteType()V

    .line 433
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefMuteAlert:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentAlerts$9;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts$9;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 444
    return-void

    .line 161
    :cond_2
    iput-boolean v4, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isNotificationReceive:Z

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting Notification"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 203
    :cond_3
    iput-boolean v4, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayBlackscreenPopup:Z

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting show blackscreen popup"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_1

    .line 232
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting is simple popup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    move v0, v1

    .line 233
    goto/16 :goto_2

    .line 239
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b032d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemPopupType:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 240
    iput v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mCurrentPopupType:I

    goto/16 :goto_3

    .line 278
    :cond_6
    iput-boolean v4, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isAlertNewGroupChat:Z

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Setting alert_new_groupchat"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_4

    .line 317
    :cond_7
    iput-boolean v4, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayReceiveMessageChecked:Z

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Setting show receive message"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_5
.end method

.method private setNotiMuteType()V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v6, 0x3

    const-wide/16 v4, 0x0

    .line 534
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mCurrentMuteType:I

    .line 535
    const-string v0, ""

    .line 537
    iget v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mCurrentMuteType:I

    packed-switch v1, :pswitch_data_0

    .line 560
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefMuteAlert:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 562
    return-void

    .line 539
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour start Long"

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getFormatedTime(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ~ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour end Long"

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getFormatedTime(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 543
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour start Long"

    invoke-virtual {v0, v1, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 544
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Setting mute hour end Long"

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 545
    new-instance v4, Lcom/sec/chaton/settings/cn;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/sec/chaton/settings/cn;-><init>(JJ)V

    .line 547
    const-string v0, "%02d:%02d ~ %02d:%02d"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->h()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v4}, Lcom/sec/chaton/settings/cn;->i()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 550
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 553
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 554
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 555
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour start Long"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 556
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour end Long"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 557
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute repeat"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 537
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private setNotiSettingType(Z)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 583
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Set Type Text"

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 584
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->optionText:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 585
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->optionText:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 586
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->alertTypes:[Ljava/lang/String;

    aget-object v0, v1, v0

    .line 590
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefSettingType:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 597
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemBlackscreenPopup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 598
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemReceivedMessage:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 599
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemAlertNewGroupChat:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 601
    return-void

    .line 584
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 604
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 605
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 606
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 607
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 638
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 611
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 613
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mActivity:Landroid/preference/PreferenceActivity;

    .line 106
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cm;->a(Landroid/app/Activity;)V

    .line 107
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 108
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "gotoAlert"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x400000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 118
    :cond_0
    const v0, 0x7f05000e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->addPreferencesFromResource(I)V

    .line 120
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    .line 122
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->am:Landroid/app/AlarmManager;

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->optionText:[Ljava/lang/String;

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->alertTypes:[Ljava/lang/String;

    .line 139
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->initializeForPreference()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :goto_0
    return-void

    .line 140
    :catch_0
    move-exception v0

    .line 141
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 618
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPause()V

    .line 619
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x400000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 620
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 624
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 625
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isNotificationReceive:Z

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setNotiSettingType(Z)V

    .line 626
    return-void
.end method

.method public run()V
    .locals 0

    .prologue
    .line 632
    return-void
.end method
