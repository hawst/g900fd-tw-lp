.class Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$3;
.super Ljava/lang/Object;
.source "PrefFragmentPasswordLock.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 111
    const-string v0, "passwordLock passwordHint"

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$000(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "now password state : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordData:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$100(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->INDEX_LOCKSTATE:I
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$200(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$000(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockHint;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->startActivity(Landroid/content/Intent;)V

    .line 117
    const/4 v0, 0x0

    return v0
.end method
