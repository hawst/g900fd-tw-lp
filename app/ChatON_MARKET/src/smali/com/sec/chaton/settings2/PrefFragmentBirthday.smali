.class public Lcom/sec/chaton/settings2/PrefFragmentBirthday;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentBirthday.java"


# instance fields
.field private impl:Lcom/sec/chaton/userprofile/j;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 15
    new-instance v0, Lcom/sec/chaton/userprofile/j;

    invoke-direct {v0}, Lcom/sec/chaton/userprofile/j;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->impl:Lcom/sec/chaton/userprofile/j;

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->impl:Lcom/sec/chaton/userprofile/j;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/userprofile/j;->a(Landroid/app/Activity;)V

    .line 25
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 26
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->impl:Lcom/sec/chaton/userprofile/j;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/userprofile/j;->a(Landroid/os/Bundle;)V

    .line 19
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 20
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->impl:Lcom/sec/chaton/userprofile/j;

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/userprofile/j;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 46
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->impl:Lcom/sec/chaton/userprofile/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/userprofile/j;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 38
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->setHasOptionsMenu(Z)V

    .line 39
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->impl:Lcom/sec/chaton/userprofile/j;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/j;->f()V

    .line 62
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroy()V

    .line 63
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->impl:Lcom/sec/chaton/userprofile/j;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/j;->a()V

    .line 31
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDetach()V

    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->setHasOptionsMenu(Z)V

    .line 33
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->impl:Lcom/sec/chaton/userprofile/j;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/userprofile/j;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBirthday;->impl:Lcom/sec/chaton/userprofile/j;

    invoke-virtual {v0}, Lcom/sec/chaton/userprofile/j;->e()V

    .line 57
    return-void
.end method
