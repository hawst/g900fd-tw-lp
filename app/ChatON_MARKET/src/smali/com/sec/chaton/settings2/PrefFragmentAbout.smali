.class public Lcom/sec/chaton/settings2/PrefFragmentAbout;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentAbout.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final CHATON:I = 0x0

.field public static CHATON_V:I = 0x0

.field private static final DIALOG_LICENSE:I = 0x1

.field private static final DIALOG_OPENSOURCE:Ljava/lang/String; = "dialogOpensource"

.field public static final IS_HEADER_VIEW_REQUIRED:Ljava/lang/String; = "is_header_view_required"

.field public static final MAX_SERVICE:I = 0x3

.field public static final PACKAGE_NAME_FROM_SERVER:Ljava/lang/String; = "com.sec.chaton"

.field public static SPP:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ActivityAbout"

.field private static final fileName:Ljava/lang/String; = "NOTICE.html"


# instance fields
.field private chatonVUpdate:Z

.field private chatonvAPII:Lcom/coolots/sso/a/a;

.field protected critical:Ljava/lang/Boolean;

.field protected downloadurl:Ljava/lang/String;

.field private listViewHeight:I

.field private mActivity:Landroid/app/Activity;

.field private mButton:[Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field mCurrentVersion:[Ljava/lang/String;

.field private mGetVersionNoticeTask:Lcom/sec/chaton/d/a/ct;

.field public mHandler:Landroid/os/Handler;

.field private mLicences:Landroid/widget/Button;

.field private mLogoImageClickCount:I

.field mNewVersion:[Ljava/lang/String;

.field private mPolicy:Landroid/widget/TextView;

.field private mTerms:Landroid/widget/TextView;

.field private mVersionCurrent:[Landroid/widget/TextView;

.field private mVersionLatest:[Landroid/widget/TextView;

.field private serviceAdapter:Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;

.field private serviceList:Landroid/widget/ListView;

.field private termsLayout:Landroid/widget/LinearLayout;

.field protected update:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    sput v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    .line 99
    const/4 v0, 0x2

    sput v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 79
    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceAdapter:Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->listViewHeight:I

    .line 103
    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonvAPII:Lcom/coolots/sso/a/a;

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonVUpdate:Z

    .line 295
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$4;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAbout;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mHandler:Landroid/os/Handler;

    .line 536
    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionCurrent:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/chaton/settings2/PrefFragmentAbout;I)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->googleAppsUpdate(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefFragmentAbout;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->dismissVersionUpgradeDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonvAPII:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/chaton/settings2/PrefFragmentAbout;Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/a;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonvAPII:Lcom/coolots/sso/a/a;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonVUpdate:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/chaton/settings2/PrefFragmentAbout;Z)Z
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonVUpdate:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/chaton/settings2/PrefFragmentAbout;)I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mLogoImageClickCount:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/chaton/settings2/PrefFragmentAbout;I)I
    .locals 0

    .prologue
    .line 63
    iput p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mLogoImageClickCount:I

    return p1
.end method

.method static synthetic access$908(Lcom/sec/chaton/settings2/PrefFragmentAbout;)I
    .locals 2

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mLogoImageClickCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mLogoImageClickCount:I

    return v0
.end method

.method private checkUpdateService()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 280
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v2, v0, v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "UpdateTargetVersion"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 286
    return-void

    .line 281
    :catch_0
    move-exception v0

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v5

    goto :goto_0
.end method

.method private dismissVersionUpgradeDialog()V
    .locals 2

    .prologue
    .line 290
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.chaton.ACTION_DISMISS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method private googleAppsUpdate(I)V
    .locals 2

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;

    aget-object v0, v0, p1

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/settings2/PrefFragmentAbout$5;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAbout;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 533
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 473
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 474
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mActivity:Landroid/app/Activity;

    .line 475
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 254
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 255
    const-string v0, "onConfigurationChagne"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 238
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 240
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    .line 241
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    .line 243
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionCurrent:[Landroid/widget/TextView;

    .line 244
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;

    .line 245
    new-array v0, v1, [Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;

    .line 247
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;

    .line 249
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->checkUpdateService()V

    .line 250
    return-void
.end method

.method protected onCreateDialog(I)V
    .locals 3

    .prologue
    .line 403
    packed-switch p1, :pswitch_data_0

    .line 409
    :goto_0
    return-void

    .line 405
    :pswitch_0
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentAbout$OpenSourceDialog;

    invoke-direct {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$OpenSourceDialog;-><init>()V

    .line 406
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialogOpensource"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentAbout$OpenSourceDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 403
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 113
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 114
    const-string v0, "onCreateView"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    const v0, 0x7f0300ed

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 120
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 122
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v1, v3, :cond_4

    .line 123
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 129
    :goto_0
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 130
    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 131
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 132
    instance-of v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 133
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move-object v0, v1

    .line 134
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v3, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 135
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_1

    move-object v0, v1

    .line 136
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 137
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 142
    :cond_1
    const v0, 0x7f070416

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 143
    const v0, 0x7f07050b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 144
    const v3, 0x7f0b0391

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 145
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 147
    const v0, 0x7f07050d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 148
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    const v0, 0x7f07050c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 150
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 151
    const v0, 0x7f07050e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 152
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 154
    const v0, 0x102000a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceList:Landroid/widget/ListView;

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceList:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 162
    const v0, 0x7f03007f

    invoke-virtual {p1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 163
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 164
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceList:Landroid/widget/ListView;

    invoke-virtual {v1, v0, v6, v7}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 167
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "UpdateUrl"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->downloadurl:Ljava/lang/String;

    .line 170
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAbout;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceAdapter:Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->serviceAdapter:Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 174
    const v0, 0x7f070208

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mTerms:Landroid/widget/TextView;

    .line 175
    const v0, 0x7f070209

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mPolicy:Landroid/widget/TextView;

    .line 176
    const v0, 0x7f07020a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mLicences:Landroid/widget/Button;

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mTerms:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<u>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0026

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "</u>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mPolicy:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<u>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0302

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "</u>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mTerms:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentAbout$1;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAbout;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mPolicy:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentAbout$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAbout;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mLicences:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentAbout$3;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAbout;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    if-nez p3, :cond_2

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/bj;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/bj;->b()Lcom/sec/chaton/d/a/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mGetVersionNoticeTask:Lcom/sec/chaton/d/a/ct;

    .line 227
    :cond_2
    const-string v0, "com.sec.spp.push"

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "com.sec.spp.push"

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x15

    if-ge v0, v1, :cond_3

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/as;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/as;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/as;->a(Landroid/content/Context;)V

    .line 232
    :cond_3
    return-object v2

    .line 126
    :cond_4
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 264
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroyView()V

    .line 266
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDestroyView, mGetVersionNoticeTask : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mGetVersionNoticeTask:Lcom/sec/chaton/d/a/ct;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonvAPII:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonvAPII:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/d;)V

    .line 273
    :cond_1
    const/4 v0, 0x1

    sput v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    .line 274
    const/4 v0, 0x2

    sput v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    .line 275
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 480
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDetach()V

    .line 481
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mActivity:Landroid/app/Activity;

    .line 482
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 398
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPause()V

    .line 399
    return-void
.end method
