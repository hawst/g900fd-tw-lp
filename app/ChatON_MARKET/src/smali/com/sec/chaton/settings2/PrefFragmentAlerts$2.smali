.class Lcom/sec/chaton/settings2/PrefFragmentAlerts$2;
.super Ljava/lang/Object;
.source "PrefFragmentAlerts.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 215
    if-eqz p2, :cond_0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayBlackscreenPopup:Z
    invoke-static {v0, v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$302(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Z)Z

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting show blackscreen popup"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 222
    :goto_0
    return v3

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->isDisplayBlackscreenPopup:Z
    invoke-static {v0, v2}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$302(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Z)Z

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting show blackscreen popup"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method
