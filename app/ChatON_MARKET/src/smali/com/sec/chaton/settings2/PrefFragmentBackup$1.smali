.class Lcom/sec/chaton/settings2/PrefFragmentBackup$1;
.super Ljava/lang/Object;
.source "PrefFragmentBackup.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 135
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 137
    if-eqz p2, :cond_1

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 139
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 140
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentBackup;->showSDcarDialog()V
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$000(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V

    .line 154
    :goto_0
    return v0

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$100(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "auto_backup_on"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAutoBackup:Z
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$202(Lcom/sec/chaton/settings2/PrefFragmentBackup;Z)Z

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentBackup;->asyncFileEncryptionTask()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$300(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$400(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;Z)V

    :goto_1
    move v0, v1

    .line 154
    goto :goto_0

    .line 150
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$100(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "auto_backup_on"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 151
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$400(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;)V

    .line 152
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAutoBackup:Z
    invoke-static {v2, v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$202(Lcom/sec/chaton/settings2/PrefFragmentBackup;Z)Z

    goto :goto_1
.end method
