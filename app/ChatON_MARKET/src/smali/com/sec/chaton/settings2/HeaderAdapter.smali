.class public Lcom/sec/chaton/settings2/HeaderAdapter;
.super Landroid/widget/ArrayAdapter;
.source "HeaderAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/preference/PreferenceActivity$Header;",
        ">;"
    }
.end annotation


# static fields
.field static final HEADER_TYPE_CATEGORY:I = 0x0

.field private static final HEADER_TYPE_COUNT:I = 0x3

.field static final HEADER_TYPE_DELETE_ACCOUNT_BUTTON:I = 0x2

.field static final HEADER_TYPE_NORMAL:I = 0x1


# instance fields
.field private headers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private selectedPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 31
    iput v0, p0, Lcom/sec/chaton/settings2/HeaderAdapter;->selectedPosition:I

    .line 45
    iput-object p2, p0, Lcom/sec/chaton/settings2/HeaderAdapter;->headers:Ljava/util/List;

    .line 46
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/settings2/HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 47
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/chaton/settings2/HeaderAdapter;->headers:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/HeaderAdapter;->headers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/settings2/HeaderAdapter;->headers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 98
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeaderType(Landroid/preference/PreferenceActivity$Header;)I
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    .line 64
    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings2/HeaderAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 70
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/HeaderAdapter;->getHeaderType(Landroid/preference/PreferenceActivity$Header;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 106
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings2/HeaderAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 107
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/HeaderAdapter;->getHeaderType(Landroid/preference/PreferenceActivity$Header;)I

    move-result v4

    .line 110
    if-nez p2, :cond_1

    .line 112
    new-instance v3, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;

    invoke-direct {v3, v1}, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;-><init>(Lcom/sec/chaton/settings2/HeaderAdapter$1;)V

    .line 114
    packed-switch v4, :pswitch_data_0

    move-object v2, v1

    .line 137
    :goto_0
    invoke-virtual {v2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v3

    .line 146
    :goto_1
    packed-switch v4, :pswitch_data_1

    .line 170
    :goto_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/sec/chaton/settings2/HeaderAdapter;->selectedPosition:I

    if-ne p1, v0, :cond_3

    .line 171
    iget-object v0, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->selector:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->selector:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 173
    iget-object v0, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->unselector:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 174
    const v0, 0x7f080003

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 175
    iget-object v0, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    const-string v1, "#ef6a20"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 184
    :cond_0
    :goto_3
    return-object v2

    .line 118
    :pswitch_0
    new-instance v2, Lcom/sec/chaton/widget/AdaptableTextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x1010208

    invoke-direct {v2, v5, v1, v6}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    move-object v1, v2

    .line 119
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    goto :goto_0

    .line 124
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/settings2/HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03013e

    invoke-virtual {v1, v2, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 125
    const v1, 0x1020016

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    .line 126
    const v1, 0x1020010

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    .line 127
    const v1, 0x7f07014d

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->updateNumber:Landroid/widget/TextView;

    .line 128
    const v1, 0x7f070511

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v3, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->selector:Landroid/view/View;

    .line 129
    const v1, 0x7f070510

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v3, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->unselector:Landroid/view/View;

    goto :goto_0

    .line 133
    :pswitch_2
    iget-object v1, p0, Lcom/sec/chaton/settings2/HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03013c

    invoke-virtual {v1, v2, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto/16 :goto_0

    .line 142
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;

    move-object v2, p2

    goto/16 :goto_1

    .line 150
    :pswitch_3
    iget-object v3, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 155
    :pswitch_4
    iget-object v3, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceActivity$Header;->getSummary(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 158
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 159
    iget-object v3, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    iget-object v3, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 163
    :cond_2
    iget-object v0, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 177
    :cond_3
    iget-object v0, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->selector:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->selector:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 179
    iget-object v0, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->unselector:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 180
    iget-object v0, v1, Lcom/sec/chaton/settings2/HeaderAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 181
    const v0, 0x7f02040d

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 146
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x3

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings2/HeaderAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSelectedPosition(I)V
    .locals 0

    .prologue
    .line 189
    iput p1, p0, Lcom/sec/chaton/settings2/HeaderAdapter;->selectedPosition:I

    .line 190
    return-void
.end method
