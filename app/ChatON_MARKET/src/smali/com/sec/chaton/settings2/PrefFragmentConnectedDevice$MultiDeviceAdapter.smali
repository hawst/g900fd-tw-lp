.class public Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;
.super Landroid/widget/BaseAdapter;
.source "PrefFragmentConnectedDevice.java"


# instance fields
.field private currentDevice:Z

.field private mButton:[Landroid/widget/Button;

.field private mCode:Landroid/widget/TextView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mText:Landroid/widget/TextView;

.field private number:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)V
    .locals 1

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 292
    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$500(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 294
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 308
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const v5, 0x7f0b03df

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 316
    iput-boolean v2, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->currentDevice:Z

    .line 317
    if-nez p2, :cond_0

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030123

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 320
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    invoke-virtual {p2, v2, v0, v2, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 322
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->getCount()I

    move-result v0

    new-array v0, v0, [Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mButton:[Landroid/widget/Button;

    .line 324
    const v0, 0x7f07014c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mText:Landroid/widget/TextView;

    .line 325
    const v0, 0x7f07014d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mCode:Landroid/widget/TextView;

    .line 326
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mButton:[Landroid/widget/Button;

    const v0, 0x7f0702d7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, p1

    .line 327
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mButton:[Landroid/widget/Button;

    aget-object v0, v0, p1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mButton:[Landroid/widget/Button;

    aget-object v0, v0, p1

    const v1, 0x7f0b0304

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->isAsker:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->isAsker:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->currentDevice:Z

    .line 337
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v1, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->model:Ljava/lang/String;

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 345
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->phoneNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->number:Ljava/lang/String;

    .line 355
    if-eqz v1, :cond_5

    const-string v0, "web"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mCode:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->webURL:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$200(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mButton:[Landroid/widget/Button;

    aget-object v0, v0, p1

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 379
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mCode:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mCode:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 381
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mText:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 384
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->chatonid:Ljava/lang/String;

    .line 386
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mButton:[Landroid/widget/Button;

    aget-object v1, v1, p1

    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 410
    :cond_4
    return-object p2

    .line 359
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->number:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 360
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->currentDevice:Z

    if-ne v0, v3, :cond_6

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mCode:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 362
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mCode:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    invoke-virtual {v2}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 364
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mCode:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 367
    :cond_7
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->currentDevice:Z

    if-ne v0, v3, :cond_2

    .line 368
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->mCode:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
