.class Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;
.super Ljava/lang/Object;
.source "PrefFragmentAlerts.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V
    .locals 0

    .prologue
    .line 493
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 498
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTimeDialog:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/co;->a()Lcom/sec/chaton/settings/cn;

    move-result-object v0

    .line 501
    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->a()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    .line 502
    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->c()V

    .line 512
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour start Long"

    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 513
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour end Long"

    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->b()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 514
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute type"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 515
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute repeat"

    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->e()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 518
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTimeDialog:Lcom/sec/chaton/settings/co;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTimeDialog:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/co;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTimeDialog:Lcom/sec/chaton/settings/co;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/co;->setDismissMessage(Landroid/os/Message;)V

    .line 520
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTimeDialog:Lcom/sec/chaton/settings/co;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/co;->dismiss()V

    .line 522
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$800(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b001f

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 523
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$11;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setNotiMuteType()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$1200(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    .line 524
    return-void

    .line 503
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->b()J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    .line 504
    sget-boolean v1, Lcom/sec/chaton/util/y;->c:Z

    if-eqz v1, :cond_3

    .line 505
    const-string v1, "end time is expired. set to next day"

    const-string v2, "Mute Manually"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->d()V

    .line 508
    invoke-virtual {v0}, Lcom/sec/chaton/settings/cn;->c()V

    goto/16 :goto_0
.end method
