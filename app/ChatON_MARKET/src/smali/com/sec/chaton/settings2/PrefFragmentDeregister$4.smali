.class Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;
.super Landroid/os/Handler;
.source "PrefFragmentDeregister.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 237
    :goto_0
    return-void

    .line 213
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 214
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 216
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->dissmissProgress()V
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$500(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V

    .line 217
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mDeRegistrationTask:Lcom/sec/chaton/d/a/ak;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$600(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/sec/chaton/d/a/ak;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mDeRegistrationTask:Lcom/sec/chaton/d/a/ak;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$600(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/sec/chaton/d/a/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/a/ak;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    const-string v0, "ChatON deregistration is success."

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$400()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 223
    const-string v1, "com.sec.chaton.action.USER_DEREGISTRATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b029c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/global/GlobalApplication;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 231
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->g()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->showErrorDialog(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$800(Lcom/sec/chaton/settings2/PrefFragmentDeregister;Ljava/lang/String;)V

    .line 232
    const-string v0, "ChatON deregistration is fail."

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$400()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 214
    nop

    :pswitch_data_0
    .packed-switch 0xca
        :pswitch_0
    .end packed-switch
.end method
