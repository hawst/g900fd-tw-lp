.class public Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentManagedAccounts.java"


# instance fields
.field private final ACCOUNT_VER:I

.field private final FACEBOOK_ACCOUNT_SIGN_IN:I

.field private final SAMSUNG_ACCOUNT_LOGIN:I

.field private final SIGN_IN:I

.field private TAG:Ljava/lang/String;

.field private final TWITTER_ACCOUNT_SIGN_IN:I

.field private final WEIBO_ACCOUNT_SIGN_IN:I

.field private email:Ljava/lang/String;

.field private mActivity:Landroid/app/Activity;

.field private mAlertDialog:Lcom/sec/common/a/d;

.field private mContext:Landroid/content/Context;

.field private mOnFacebookLoginCallback:Lcom/sec/chaton/settings/tellfriends/af;

.field private mOnFacebookLogoutCallback:Lcom/sec/chaton/settings/tellfriends/ag;

.field private mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;

.field private mPrefItemRenren:Lcom/sec/chaton/settings/SnsSignInOutPreference;

.field private mPrefItemTwitter:Lcom/sec/chaton/settings/SnsSignInOutPreference;

.field private mPrefItemWeibo:Lcom/sec/chaton/settings/SnsSignInOutPreference;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mRrSnsHelper:Lcom/sec/chaton/settings/tellfriends/al;

.field private mSnsHelper:Lcom/sec/chaton/settings/tellfriends/aj;

.field private mTwSnsHelper:Lcom/sec/chaton/settings/tellfriends/an;

.field private mWbSnsHelper:Lcom/sec/chaton/settings/tellfriends/ap;

.field private progressBar:Landroid/app/ProgressDialog;

.field private result:Z

.field private samsungAccount:Landroid/preference/Preference;

.field private ssoEmail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 40
    const-class v0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->SAMSUNG_ACCOUNT_LOGIN:I

    .line 43
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->SIGN_IN:I

    .line 44
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TWITTER_ACCOUNT_SIGN_IN:I

    .line 45
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->WEIBO_ACCOUNT_SIGN_IN:I

    .line 46
    const/16 v0, 0x2ee5

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->ACCOUNT_VER:I

    .line 47
    const/16 v0, 0x7f99

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->FACEBOOK_ACCOUNT_SIGN_IN:I

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->progressBar:Landroid/app/ProgressDialog;

    .line 619
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mOnFacebookLogoutCallback:Lcom/sec/chaton/settings/tellfriends/ag;

    .line 652
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$4;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$4;-><init>(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mOnFacebookLoginCallback:Lcom/sec/chaton/settings/tellfriends/af;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Lcom/sec/chaton/settings/SnsSignInOutPreference;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->showAlertDialog()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->checkFacebookLogin()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->dismissAllDialog()V

    return-void
.end method

.method private checkFacebookLogin()V
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mSnsHelper:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mSnsHelper:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->e()Ljava/lang/String;

    move-result-object v0

    .line 419
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 420
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    const v1, 0x7f0b01f2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->setSummary(I)V

    .line 421
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    sget-object v1, Lcom/sec/chaton/settings/fe;->a:Lcom/sec/chaton/settings/fe;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a(Lcom/sec/chaton/settings/fe;)V

    .line 426
    :goto_0
    return-void

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    sget-object v1, Lcom/sec/chaton/settings/fe;->b:Lcom/sec/chaton/settings/fe;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a(Lcom/sec/chaton/settings/fe;)V

    goto :goto_0
.end method

.method private checkRenrenLogin()V
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mRrSnsHelper:Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/al;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mRrSnsHelper:Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/al;->b()Ljava/lang/String;

    move-result-object v0

    .line 379
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemRenren:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemRenren:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    const v1, 0x7f0b01f5

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->setSummary(I)V

    .line 381
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemRenren:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    sget-object v1, Lcom/sec/chaton/settings/fe;->a:Lcom/sec/chaton/settings/fe;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a(Lcom/sec/chaton/settings/fe;)V

    .line 385
    :goto_0
    return-void

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemRenren:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    sget-object v1, Lcom/sec/chaton/settings/fe;->b:Lcom/sec/chaton/settings/fe;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a(Lcom/sec/chaton/settings/fe;)V

    goto :goto_0
.end method

.method private checkTwitterLogin()V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mTwSnsHelper:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/an;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mTwSnsHelper:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/an;->b()Ljava/lang/String;

    move-result-object v0

    .line 406
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemTwitter:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 407
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemTwitter:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    const v1, 0x7f0b01f3

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->setSummary(I)V

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemTwitter:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    sget-object v1, Lcom/sec/chaton/settings/fe;->a:Lcom/sec/chaton/settings/fe;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a(Lcom/sec/chaton/settings/fe;)V

    .line 412
    :goto_0
    return-void

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemTwitter:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    sget-object v1, Lcom/sec/chaton/settings/fe;->b:Lcom/sec/chaton/settings/fe;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a(Lcom/sec/chaton/settings/fe;)V

    goto :goto_0
.end method

.method private checkWeiboLogin()V
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mWbSnsHelper:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mWbSnsHelper:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->b()Ljava/lang/String;

    move-result-object v0

    .line 392
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemWeibo:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 393
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemWeibo:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    const v1, 0x7f0b01f4

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->setSummary(I)V

    .line 394
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemWeibo:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    sget-object v1, Lcom/sec/chaton/settings/fe;->a:Lcom/sec/chaton/settings/fe;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a(Lcom/sec/chaton/settings/fe;)V

    .line 398
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemWeibo:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    sget-object v1, Lcom/sec/chaton/settings/fe;->b:Lcom/sec/chaton/settings/fe;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a(Lcom/sec/chaton/settings/fe;)V

    goto :goto_0
.end method

.method private dismissAlertDialog()V
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mAlertDialog:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mAlertDialog:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mAlertDialog:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 694
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mAlertDialog:Lcom/sec/common/a/d;

    .line 696
    :cond_0
    return-void
.end method

.method private dismissAllDialog()V
    .locals 0

    .prologue
    .line 685
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->dismissProgressDialog()V

    .line 686
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->dismissAlertDialog()V

    .line 687
    return-void
.end method

.method private dismissProgressDialog()V
    .locals 3

    .prologue
    .line 609
    .line 610
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dismissProgressDialog() \t- mProgressDialog : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". isSowing : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 613
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 615
    :cond_0
    return-void
.end method

.method private doFinish(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->samsungAccount:Landroid/preference/Preference;

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 500
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->samsungAccount:Landroid/preference/Preference;

    const v1, 0x7f0b0092

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 502
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->samsungAccount:Landroid/preference/Preference;

    const v1, 0x7f030142

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setWidgetLayoutResource(I)V

    .line 503
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->samsungAccount:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 504
    return-void
.end method

.method private initiallizeSNSPreference()V
    .locals 2

    .prologue
    .line 140
    const-string v0, "pref_item_facebook"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/SnsSignInOutPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    .line 141
    const-string v0, "pref_item_twitter"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/SnsSignInOutPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemTwitter:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    .line 142
    const-string v0, "pref_item_weibo"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/SnsSignInOutPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemWeibo:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    .line 143
    const-string v0, "pref_item_renren"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/SnsSignInOutPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemRenren:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    .line 144
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 145
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemTwitter:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 146
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemWeibo:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 147
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemRenren:Lcom/sec/chaton/settings/SnsSignInOutPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 372
    return-void
.end method

.method private runSSO()V
    .locals 3

    .prologue
    .line 479
    const-string v0, "ActivitySignIn will be run"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 481
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 482
    const-string v1, "client_id"

    const-string v2, "fs24s8z0hh"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 483
    const-string v1, "client_ secret"

    const-string v2, "8F23805C79D7D4EBAAC5CE705A87371D"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 484
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 487
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 494
    :goto_0
    return-void

    .line 489
    :catch_0
    move-exception v0

    .line 490
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private showAlertDialog()V
    .locals 3

    .prologue
    .line 701
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mAlertDialog:Lcom/sec/common/a/d;

    if-nez v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0122

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$5;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$5;-><init>(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mAlertDialog:Lcom/sec/common/a/d;

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mAlertDialog:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 714
    return-void
.end method

.method private showPasswordLockActivity()V
    .locals 3

    .prologue
    .line 509
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 512
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 513
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 514
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 515
    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->startActivity(Landroid/content/Intent;)V

    .line 517
    :cond_0
    return-void
.end method

.method private showProgressDialog()V
    .locals 3

    .prologue
    .line 583
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    const v2, 0x7f0b000d

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 586
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 587
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 604
    :goto_0
    return-void

    .line 601
    :cond_0
    const-string v0, "showProgressDialog() \t- not null. show"

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 432
    sparse-switch p1, :sswitch_data_0

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 435
    :sswitch_0
    const/4 v0, 0x0

    .line 437
    if-eqz p3, :cond_1

    .line 439
    const-string v0, "country_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 442
    :cond_1
    if-ne p2, v1, :cond_2

    .line 444
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "samsung_account_email"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->doFinish(Ljava/lang/String;)V

    .line 447
    if-eqz v0, :cond_0

    .line 449
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/util/cb;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    goto :goto_0

    .line 453
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->progressBar:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->progressBar:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->progressBar:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 460
    :sswitch_1
    if-ne p2, v1, :cond_3

    .line 461
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 462
    invoke-direct {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->doFinish(Ljava/lang/String;)V

    .line 464
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "email : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 466
    :cond_3
    const-string v0, "SIGNING_SSO : result is ERROR"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 470
    :sswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mSnsHelper:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/settings/tellfriends/aj;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 432
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x7f99 -> :sswitch_2
    .end sparse-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    .line 62
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 63
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v3, 0x7f0b0092

    .line 77
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const v0, 0x7f05000d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->addPreferencesFromResource(I)V

    .line 80
    const-string v0, "pref_item_samsung_account"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->samsungAccount:Landroid/preference/Preference;

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->samsungAccount:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setTitle(I)V

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mContext:Landroid/content/Context;

    .line 84
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->email:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/util/am;->d(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->result:Z

    .line 87
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->email:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->samsungAccount:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->samsungAccount:Landroid/preference/Preference;

    const v1, 0x7f030142

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setWidgetLayoutResource(I)V

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->samsungAccount:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(I)V

    .line 110
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->initiallizeSNSPreference()V

    .line 111
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->samsungAccount:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$1;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mSnsHelper:Lcom/sec/chaton/settings/tellfriends/aj;

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mSnsHelper:Lcom/sec/chaton/settings/tellfriends/aj;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/aj;->a()V

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mTwSnsHelper:Lcom/sec/chaton/settings/tellfriends/an;

    if-eqz v0, :cond_1

    .line 561
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mTwSnsHelper:Lcom/sec/chaton/settings/tellfriends/an;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/an;->a()V

    .line 563
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mWbSnsHelper:Lcom/sec/chaton/settings/tellfriends/ap;

    if-eqz v0, :cond_2

    .line 564
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mWbSnsHelper:Lcom/sec/chaton/settings/tellfriends/ap;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/ap;->a()V

    .line 566
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mRrSnsHelper:Lcom/sec/chaton/settings/tellfriends/al;

    if-eqz v0, :cond_3

    .line 567
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mRrSnsHelper:Lcom/sec/chaton/settings/tellfriends/al;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/tellfriends/al;->a()V

    .line 570
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->progressBar:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_4

    .line 571
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->progressBar:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 574
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_5

    .line 575
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 577
    :cond_5
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroy()V

    .line 578
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    .line 70
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDetach()V

    .line 71
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 540
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPause()V

    .line 541
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 531
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 532
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->showPasswordLockActivity()V

    .line 534
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 523
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onStart()V

    .line 524
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStart, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 548
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onStop()V

    .line 549
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStop, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    return-void
.end method
