.class Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;
.super Landroid/os/Handler;
.source "PrefFragmentSuggestion.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 124
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$000(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 134
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 136
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 137
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v2, v3, :cond_4

    .line 138
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPrivacyList;

    .line 139
    if-eqz v0, :cond_0

    .line 142
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPrivacyList;->privacy:Ljava/util/ArrayList;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->privacyArray:Ljava/util/ArrayList;
    invoke-static {v2, v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$202(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 143
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->privacyArray:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$200(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 144
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->privacyArray:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$200(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->privacyArray:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$200(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;->_value:Ljava/lang/String;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->InitPrivacyCheck(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$300(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 146
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 151
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 154
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->access$000(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    invoke-virtual {v2}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x142
        :pswitch_0
    .end packed-switch
.end method
