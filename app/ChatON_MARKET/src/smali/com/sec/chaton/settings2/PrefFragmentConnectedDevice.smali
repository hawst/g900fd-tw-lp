.class public Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentConnectedDevice.java"


# instance fields
.field private final MAPPING_RESULT:I

.field private TAG:Ljava/lang/String;

.field private deviceAdapter:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;

.field private deviceList:Landroid/widget/LinearLayout;

.field private mActivity:Landroid/app/Activity;

.field private mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field private mMappingInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/MappingInfoEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mProfileControl:Lcom/sec/chaton/d/w;

.field private mSAControl:Lcom/sec/chaton/d/at;

.field private myAccountInfo:Landroid/widget/TextView;

.field private progressBar:Landroid/app/ProgressDialog;

.field private webURL:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 45
    const-class v0, Lcom/sec/chaton/settings/ActivityMultiDeviceView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->TAG:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->MAPPING_RESULT:I

    .line 49
    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceAdapter:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;

    .line 53
    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->webURL:Ljava/lang/String;

    .line 178
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mHandler:Landroid/os/Handler;

    .line 279
    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->webURL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->webURL:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceList:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceAdapter:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Lcom/sec/chaton/d/w;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mProfileControl:Lcom/sec/chaton/d/w;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Lcom/sec/chaton/d/at;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mSAControl:Lcom/sec/chaton/d/at;

    return-object v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 161
    packed-switch p1, :pswitch_data_0

    .line 176
    :goto_0
    return-void

    .line 164
    :pswitch_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 165
    const-string v0, "mapping was FAILED"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->myAccountInfo:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/util/am;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mProfileControl:Lcom/sec/chaton/d/w;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 148
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 149
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    .line 150
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f07014c

    const/4 v8, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 62
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 63
    const v0, 0x7f0300ec

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 66
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 68
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v1, v3, :cond_2

    .line 69
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 75
    :goto_0
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 76
    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 77
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 78
    instance-of v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 79
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move-object v0, v1

    .line 80
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 81
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_0

    move-object v0, v1

    .line 82
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 83
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 88
    :cond_0
    const v0, 0x7f070416

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 89
    const v0, 0x7f07050b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 90
    const v3, 0x7f0b0288

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 91
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 93
    const v0, 0x7f07050d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 94
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    const v0, 0x7f07050c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 96
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    const v0, 0x7f07050e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 98
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0092

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    const v0, 0x7f07014d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->myAccountInfo:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f070423

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 105
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0288

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 109
    const v0, 0x7f070424

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceList:Landroid/widget/LinearLayout;

    .line 114
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mContext:Landroid/content/Context;

    .line 115
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;-><init>(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceAdapter:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;

    .line 116
    new-instance v0, Lcom/sec/chaton/d/w;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/w;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mProfileControl:Lcom/sec/chaton/d/w;

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/at;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/at;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mSAControl:Lcom/sec/chaton/d/at;

    .line 119
    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-ne v0, v6, :cond_3

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->myAccountInfo:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/util/am;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    if-nez p3, :cond_1

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b000b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v8, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mProfileControl:Lcom/sec/chaton/d/w;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/w;->f(Ljava/lang/String;)V

    .line 141
    :goto_1
    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->setHasOptionsMenu(Z)V

    .line 142
    return-object v2

    .line 72
    :cond_2
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 135
    :cond_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/chaton/registration/ActivitySyncSignInPopup;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 136
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 137
    const-string v1, "isSyncContacts"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 138
    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 275
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroy()V

    .line 276
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onDestroy, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDetach()V

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    .line 157
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 263
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPause()V

    .line 264
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onPause, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 256
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onResume, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 249
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onStart()V

    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStart, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 269
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onStop()V

    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onStop, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    return-void
.end method
