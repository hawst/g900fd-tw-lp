.class public Lcom/sec/chaton/settings2/PrefFragmentAbout$OpenSourceDialog;
.super Landroid/app/DialogFragment;
.source "PrefFragmentAbout.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 416
    .line 417
    const/4 v0, 0x0

    .line 421
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v2, 0x800

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 422
    const/16 v2, 0x800

    new-array v5, v2, [C

    .line 424
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$OpenSourceDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "NOTICE.html"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 426
    if-nez v3, :cond_2

    .line 446
    if-eqz v3, :cond_0

    .line 447
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    .line 449
    :cond_0
    if-eqz v1, :cond_1

    .line 450
    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStreamReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    .line 466
    :cond_1
    :goto_0
    return-object v1

    .line 430
    :cond_2
    :try_start_3
    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 432
    :goto_1
    :try_start_4
    invoke-virtual {v2, v5}, Ljava/io/InputStreamReader;->read([C)I

    move-result v0

    if-ltz v0, :cond_7

    .line 433
    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    .line 436
    :catch_0
    move-exception v0

    .line 437
    :goto_2
    :try_start_5
    const-string v4, "License file not found"

    const-string v5, "ActivityAbout"

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 446
    if-eqz v3, :cond_3

    .line 447
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 449
    :cond_3
    if-eqz v2, :cond_1

    .line 450
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_0

    .line 452
    :catch_1
    move-exception v0

    .line 453
    :goto_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 440
    :catch_2
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    .line 441
    :goto_4
    :try_start_7
    const-string v4, "Error reading license"

    const-string v5, "ActivityAbout"

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 446
    if-eqz v3, :cond_4

    .line 447
    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8

    .line 449
    :cond_4
    if-eqz v2, :cond_1

    .line 450
    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_0

    .line 452
    :catch_3
    move-exception v0

    goto :goto_3

    .line 445
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    .line 446
    :goto_5
    if-eqz v3, :cond_5

    .line 447
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 449
    :cond_5
    if-eqz v2, :cond_6

    .line 450
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 445
    :cond_6
    :goto_6
    throw v0

    .line 446
    :cond_7
    if-eqz v3, :cond_8

    .line 447
    :try_start_b
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 449
    :cond_8
    if-eqz v2, :cond_9

    .line 450
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    .line 457
    :cond_9
    :goto_7
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 458
    const-string v0, "License file is empty"

    const-string v2, "ActivityAbout"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 462
    :cond_a
    new-instance v0, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$OpenSourceDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 463
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    new-instance v1, Landroid/webkit/WebViewClient;

    invoke-direct {v1}, Landroid/webkit/WebViewClient;-><init>()V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 466
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$OpenSourceDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b00e3

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v1

    goto/16 :goto_0

    .line 452
    :catch_4
    move-exception v1

    .line 453
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 452
    :catch_5
    move-exception v0

    .line 453
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 452
    :catch_6
    move-exception v0

    goto/16 :goto_3

    :catch_7
    move-exception v0

    goto/16 :goto_3

    :catch_8
    move-exception v0

    goto/16 :goto_3

    .line 445
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_5

    :catchall_2
    move-exception v0

    goto :goto_5

    .line 440
    :catch_9
    move-exception v0

    move-object v2, v1

    goto/16 :goto_4

    :catch_a
    move-exception v0

    goto/16 :goto_4

    .line 436
    :catch_b
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto/16 :goto_2

    :catch_c
    move-exception v0

    move-object v2, v1

    goto/16 :goto_2
.end method
