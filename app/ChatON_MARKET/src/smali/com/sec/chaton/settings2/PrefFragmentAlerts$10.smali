.class Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;
.super Ljava/lang/Object;
.source "PrefFragmentAlerts.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V
    .locals 0

    .prologue
    .line 447
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    .prologue
    const v8, 0x7f0b001f

    const/4 v3, 0x5

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 450
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 451
    packed-switch p2, :pswitch_data_0

    .line 482
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->muteTypeDialog:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 483
    return-void

    .line 453
    :pswitch_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 454
    const-wide/32 v2, 0x36ee80

    add-long/2addr v2, v0

    .line 455
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Setting mute hour start Long"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 456
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour end Long"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 457
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute type"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 458
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$800(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v8, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setNotiMuteType()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$1200(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    goto :goto_0

    .line 462
    :pswitch_1
    invoke-virtual {v6, v3, v7}, Ljava/util/Calendar;->add(II)V

    .line 463
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 464
    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v6, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x7

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 465
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour start Long"

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 466
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting mute hour end Long"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 467
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute type"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 468
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$800(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v8, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 469
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setNotiMuteType()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$1200(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    goto/16 :goto_0

    .line 472
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->displayManuallyDialog()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$1300(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    goto/16 :goto_0

    .line 475
    :pswitch_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute type"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 476
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour start Long"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 477
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute hour end Long"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 478
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting mute repeat"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$10;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setNotiMuteType()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$1200(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)V

    goto/16 :goto_0

    .line 451
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
