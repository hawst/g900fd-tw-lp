.class Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;
.super Ljava/lang/Object;
.source "PrefFragmentPasswordLock.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    .line 60
    const-string v0, "passwordLock select ok"

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$000(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "now password state : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordData:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$100(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->INDEX_LOCKSTATE:I
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$200(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$000(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    const-string v2, "GET"

    const-string v3, ""

    const-string v4, ""

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->prePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordData:[Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$102(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;[Ljava/lang/String;)[Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordData:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$100(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->INDEX_LOCKSTATE:I
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$200(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {}, Lcom/sec/chaton/util/p;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LOCK_STATE : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->LOCK_STATE:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$300(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$000(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-static {}, Lcom/sec/chaton/util/p;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->changeSummary(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    const-string v1, "SET"

    const-string v2, "OFF"

    const-string v3, "0000"

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->prePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    .line 84
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-static {}, Lcom/sec/chaton/util/p;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->changeSummary(Ljava/lang/String;)V

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LOCK_STATE : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->LOCK_STATE:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$300(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->access$000(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    const-string v1, "MODE"

    const-string v2, "SET"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
