.class public Lcom/sec/chaton/settings2/PrefFragmentContactSync;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentContactSync.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private autoContactsDescription:Landroid/view/View;

.field private isAutoSync:Z

.field private isContactSynced:Z

.field private isSync:Z

.field private layoutAutoContacts:Landroid/widget/RelativeLayout;

.field private layoutManualSync:Landroid/widget/RelativeLayout;

.field private layoutSimContacts:Landroid/widget/RelativeLayout;

.field private final mHandler:Landroid/os/Handler;

.field private mIsActive:Z

.field private mRunnableSyncStateUpdated:Ljava/lang/Runnable;

.field private mStatusChangeListenerHandle:Ljava/lang/Object;

.field private mSyncStatusObserver:Landroid/content/SyncStatusObserver;

.field private manualSyncDesc:Landroid/widget/TextView;

.field private manualSyncView:Landroid/widget/ImageView;

.field private sharedPref:Lcom/sec/chaton/util/ab;

.field private simContactsDescription:Landroid/view/View;

.field private syncAutoChkBox:Landroid/widget/CheckBox;

.field private syncChkBox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 53
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mIsActive:Z

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 60
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isContactSynced:Z

    .line 238
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mHandler:Landroid/os/Handler;

    .line 240
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentContactSync$4;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync$4;-><init>(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mRunnableSyncStateUpdated:Ljava/lang/Runnable;

    .line 247
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentContactSync$5;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync$5;-><init>(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mSyncStatusObserver:Landroid/content/SyncStatusObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isAutoSync:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/chaton/settings2/PrefFragmentContactSync;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isAutoSync:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->sharedPref:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->syncAutoChkBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/chaton/settings2/PrefFragmentContactSync;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isSync:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->onSyncStateUpdated()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mRunnableSyncStateUpdated:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private isSyncing()Z
    .locals 2

    .prologue
    .line 270
    invoke-static {}, Landroid/content/ContentResolver;->getCurrentSync()Landroid/content/SyncInfo;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    const-string v1, "com.sec.chaton"

    iget-object v0, v0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSyncingOverHoneyComb()Z
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 276
    invoke-static {}, Landroid/content/ContentResolver;->getCurrentSyncs()Ljava/util/List;

    move-result-object v3

    .line 279
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    move v1, v0

    move v2, v0

    .line 280
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 281
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SyncInfo;

    iget-object v0, v0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    const-string v4, "com.sec.chaton"

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SyncInfo;

    iget-object v0, v0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    const/4 v2, 0x1

    .line 284
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSyncing return value = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "SyncStatePreference"

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 287
    :cond_2
    return v0
.end method

.method private onSyncStateUpdated()V
    .locals 3

    .prologue
    .line 291
    .line 293
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 294
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isSyncingOverHoneyComb()Z

    move-result v0

    .line 298
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSyncStateUpdated "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SyncStatePreference"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    invoke-direct {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->setActive(Z)V

    .line 300
    return-void

    .line 296
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isSyncing()Z

    move-result v0

    goto :goto_0
.end method

.method private setActive(Z)V
    .locals 3

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mIsActive:Z

    if-eq v0, p1, :cond_0

    .line 304
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mIsActive:Z

    .line 306
    if-eqz p1, :cond_1

    .line 308
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0202ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 309
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->manualSyncView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 310
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->manualSyncView:Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentContactSync$6;

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync$6;-><init>(Lcom/sec/chaton/settings2/PrefFragmentContactSync;Landroid/graphics/drawable/AnimationDrawable;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->manualSyncView:Landroid/widget/ImageView;

    const v1, 0x7f0202a1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 320
    const-string v0, "support_contact_auto_sync"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 321
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->manualSyncDesc:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->setSummaryLatestSyncTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 323
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->manualSyncDesc:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->setLatestContactSyncTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setLatestContactSyncTime()Ljava/lang/String;
    .locals 6

    .prologue
    const v5, 0x7f0b0193

    .line 346
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting Sync TimeInMillis"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 348
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 349
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 360
    :goto_0
    return-object v0

    .line 350
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 354
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0244

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private setSummaryLatestSyncTime()Ljava/lang/String;
    .locals 6

    .prologue
    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 331
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting Sync TimeInMillis"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 333
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_0

    .line 334
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 336
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0244

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 206
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 234
    :goto_0
    :pswitch_0
    return-void

    .line 209
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isSync:Z

    if-nez v2, :cond_0

    :goto_1
    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isSync:Z

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->syncChkBox:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isSync:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 209
    goto :goto_1

    .line 214
    :pswitch_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v2

    .line 216
    const/4 v3, -0x3

    if-eq v3, v2, :cond_1

    const/4 v3, -0x2

    if-ne v3, v2, :cond_2

    .line 217
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0b015f

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 219
    :cond_2
    iget-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isContactSynced:Z

    if-ne v1, v0, :cond_3

    .line 220
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "buddy_request_sync_in_chaton"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 221
    invoke-static {v0}, Lcom/sec/chaton/account/i;->a(Z)V

    goto :goto_0

    .line 224
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/registration/ActivityRegist;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 225
    const-string v1, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    const-string v2, "ACTIVITY_PURPOSE_CALL_CONTACT_SYNC"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 206
    :pswitch_data_0
    .packed-switch 0x7f07041b
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "contact_sim_sync"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 66
    iput-boolean v4, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isSync:Z

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "contact_sim_sync"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 72
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 73
    return-void

    .line 69
    :cond_0
    iput-boolean v3, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isSync:Z

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "contact_sim_sync"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f0b0277

    const v8, 0x7f07014d

    const v7, 0x7f07014c

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 78
    const v0, 0x7f0300ea

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 81
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 83
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v1, v3, :cond_2

    .line 84
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 90
    :goto_0
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 91
    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 92
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 93
    instance-of v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 94
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move-object v0, v1

    .line 95
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 96
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_0

    move-object v0, v1

    .line 97
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 98
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 103
    :cond_0
    const v0, 0x7f070416

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 104
    const v0, 0x7f07050b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 105
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(I)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 108
    const v0, 0x7f07050d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 109
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    const v0, 0x7f07050c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 111
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 112
    const v0, 0x7f07050e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 113
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    const-string v0, "support_contact_auto_sync"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    const v0, 0x7f070417

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->layoutAutoContacts:Landroid/widget/RelativeLayout;

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->layoutAutoContacts:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 119
    const v0, 0x7f07041a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 120
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 121
    const v0, 0x7f070418

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->syncAutoChkBox:Landroid/widget/CheckBox;

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->syncAutoChkBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentContactSync$1;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->layoutAutoContacts:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentContactSync$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    const v0, 0x7f070419

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->autoContactsDescription:Landroid/view/View;

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->autoContactsDescription:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 141
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->autoContactsDescription:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 142
    const v3, 0x7f0b03a8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 143
    const-string v0, ""

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :cond_1
    const v0, 0x7f07041b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->layoutSimContacts:Landroid/widget/RelativeLayout;

    .line 148
    const v0, 0x7f07041c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->syncChkBox:Landroid/widget/CheckBox;

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->syncChkBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentContactSync$3;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentContactSync;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->layoutSimContacts:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    const v0, 0x7f07041d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->simContactsDescription:Landroid/view/View;

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->simContactsDescription:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 161
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->simContactsDescription:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 162
    const v3, 0x7f0b0281

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 163
    const v0, 0x7f0b0281

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 165
    const v0, 0x7f07041e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->layoutManualSync:Landroid/widget/RelativeLayout;

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->layoutManualSync:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    const v0, 0x7f070420

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 170
    const v1, 0x7f070421

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->manualSyncDesc:Landroid/widget/TextView;

    .line 171
    const-string v1, "support_contact_auto_sync"

    invoke-static {v1}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 172
    const v1, 0x7f0b03a9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->manualSyncDesc:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->setSummaryLatestSyncTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    :goto_1
    const v0, 0x7f07041f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->manualSyncView:Landroid/widget/ImageView;

    .line 181
    return-object v2

    .line 87
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 175
    :cond_3
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->manualSyncDesc:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->setLatestContactSyncTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 199
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPause()V

    .line 200
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->stopSyncObserver()V

    .line 201
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 186
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 188
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isContactSynced:Z

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->syncChkBox:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isSync:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->syncAutoChkBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->syncAutoChkBox:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->isAutoSync:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 194
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->startSyncObserver()V

    .line 195
    return-void

    .line 188
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startSyncObserver()V
    .locals 2

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->stopSyncObserver()V

    .line 256
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mSyncStatusObserver:Landroid/content/SyncStatusObserver;

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mStatusChangeListenerHandle:Ljava/lang/Object;

    .line 257
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->onSyncStateUpdated()V

    .line 258
    return-void
.end method

.method public stopSyncObserver()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mStatusChangeListenerHandle:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mRunnableSyncStateUpdated:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mStatusChangeListenerHandle:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentContactSync;->mStatusChangeListenerHandle:Ljava/lang/Object;

    .line 266
    :cond_0
    return-void
.end method
