.class Lcom/sec/chaton/settings2/PrefBackupListView$2;
.super Ljava/lang/Object;
.source "PrefBackupListView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefBackupListView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefBackupListView;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const v4, 0x7f070592

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 165
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/p;

    .line 166
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mSwitchViewType:Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$000(Lcom/sec/chaton/settings2/PrefBackupListView;)Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;->DELETE:Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;

    if-ne v1, v2, :cond_3

    .line 168
    iget-object v1, v0, Lcom/sec/chaton/localbackup/p;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # invokes: Lcom/sec/chaton/settings2/PrefBackupListView;->setKeyInHashmap(Lcom/sec/chaton/localbackup/p;Z)V
    invoke-static {v1, v0, v3}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$100(Lcom/sec/chaton/settings2/PrefBackupListView;Lcom/sec/chaton/localbackup/p;Z)V

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$200(Lcom/sec/chaton/settings2/PrefBackupListView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$300(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 179
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$200(Lcom/sec/chaton/settings2/PrefBackupListView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$400(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 200
    :goto_1
    return-void

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # invokes: Lcom/sec/chaton/settings2/PrefBackupListView;->setKeyInHashmap(Lcom/sec/chaton/localbackup/p;Z)V
    invoke-static {v1, v0, v5}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$100(Lcom/sec/chaton/settings2/PrefBackupListView;Lcom/sec/chaton/localbackup/p;Z)V

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$300(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$400(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    .line 188
    :cond_3
    iget-object v1, v0, Lcom/sec/chaton/localbackup/p;->f:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "backup_checkkey"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 189
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    invoke-virtual {v2}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/localbackup/ActivitySecretKey;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 190
    const-string v2, "password_mode"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 191
    const-string v2, "target_filepath"

    iget-object v3, v0, Lcom/sec/chaton/localbackup/p;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    sget-object v2, Lcom/sec/chaton/settings2/PrefBackupListView;->BACKUP_FILE_NAME:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/p;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings2/PrefBackupListView;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 195
    :cond_4
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    invoke-virtual {v2}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/localbackup/chatlist/ChatListActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 196
    sget-object v2, Lcom/sec/chaton/settings2/PrefBackupListView;->BACKUP_FILE_NAME:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/p;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$2;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings2/PrefBackupListView;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
