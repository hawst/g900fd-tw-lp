.class public Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentPasswordLock.java"


# instance fields
.field private Hint:Ljava/lang/String;

.field private INDEX_LOCKSTATE:I

.field private LOCK_STATE:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private passwordChange:Landroid/preference/Preference;

.field private passwordData:[Ljava/lang/String;

.field private passwordHint:Landroid/preference/Preference;

.field private passwordLock:Landroid/preference/CheckBoxPreference;

.field private passwordTitle:Lcom/sec/chaton/settings2/BreadCrumbsPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 25
    const-class v0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;

    .line 27
    const-string v0, "OFF"

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->LOCK_STATE:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->INDEX_LOCKSTATE:I

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordData:[Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->Hint:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordData:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordData:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->INDEX_LOCKSTATE:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->LOCK_STATE:Ljava/lang/String;

    return-object v0
.end method

.method private setSummaryWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 227
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 228
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 229
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 230
    return-void
.end method

.method private setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 220
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 221
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 222
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 223
    return-void
.end method


# virtual methods
.method public changeSummary(Ljava/lang/String;)V
    .locals 8

    .prologue
    const v7, 0x7f0b01c6

    const v6, 0x7f08001f

    const v5, 0x7f080015

    const/4 v4, 0x0

    const v3, 0x7f08003f

    .line 134
    const-string v0, "changeSummary"

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "PASSWORD_LOCK"

    invoke-virtual {v0, v1, v4}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 138
    const-string v1, "PASSWORD_HINT"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->Hint:Ljava/lang/String;

    .line 140
    invoke-static {}, Lcom/sec/chaton/util/p;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordLock:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 144
    const v0, 0x7f0b01c3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordLock:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->setSummaryWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 145
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordChange:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 146
    const v0, 0x7f0b01c7

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordHint:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 157
    :goto_0
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->Hint:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    const v0, 0x7f0b01c1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordHint:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->setSummaryWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 165
    :goto_1
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordLock:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 152
    const v0, 0x7f0b01c4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordLock:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->setSummaryWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 153
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordChange:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 154
    const v0, 0x7f0b01c7

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordHint:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_0

    .line 163
    :cond_1
    const v0, 0x7f0b01ca

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordHint:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->setSummaryWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f05000f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->addPreferencesFromResource(I)V

    .line 48
    const-string v0, "pref_item_breadcrumbs"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordTitle:Lcom/sec/chaton/settings2/BreadCrumbsPreference;

    .line 49
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordTitle:Lcom/sec/chaton/settings2/BreadCrumbsPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->setActivity(Landroid/app/Activity;)V

    .line 51
    const-string v0, "pref_item_password_lock"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordLock:Landroid/preference/CheckBoxPreference;

    .line 52
    const-string v0, "pref_item_change_password_lock"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordChange:Landroid/preference/Preference;

    .line 53
    const-string v0, "pref_item_hint_password_lock"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordHint:Landroid/preference/Preference;

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordLock:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordChange:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordHint:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$3;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 120
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 126
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 128
    const-string v0, "GET"

    const-string v1, ""

    const-string v2, ""

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->prePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordData:[Ljava/lang/String;

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->passwordData:[Ljava/lang/String;

    iget v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->INDEX_LOCKSTATE:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->changeSummary(Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method public prePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x0

    .line 169
    const-string v1, "prePassword"

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    .line 175
    new-array v3, v6, [Ljava/lang/String;

    move v1, v0

    .line 177
    :goto_0
    if-ge v1, v6, :cond_0

    .line 178
    const-string v4, ""

    aput-object v4, v3, v1

    .line 177
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 181
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v4, "PASSWORD_LOCK"

    invoke-virtual {v1, v4, v0}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 183
    const-string v4, "GET"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 185
    const-string v2, "GET preference"

    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v2, "LOCK_STATE"

    invoke-static {}, Lcom/sec/chaton/util/p;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v0

    .line 188
    const/4 v2, 0x1

    const-string v4, "PASSWORD"

    const-string v5, "0000"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    .line 190
    :goto_1
    if-ge v0, v6, :cond_2

    .line 191
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "data : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, v3, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 196
    :cond_1
    const-string v0, "SET preference"

    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "state : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pass : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 201
    invoke-static {p2}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-static {p3}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 203
    const-string v5, "LOCK_STATE"

    invoke-interface {v0, v5, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 204
    const-string v1, "PASSWORD"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 205
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 207
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "OFF"

    invoke-virtual {v0, v1, p2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v0, "OFF"

    const-string v1, "default"

    invoke-virtual {v2, v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LOCK_STATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_2
    return-object v3
.end method
