.class Lcom/sec/chaton/settings2/PrefFragmentPrivacy$7;
.super Ljava/lang/Object;
.source "PrefFragmentPrivacy.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$7;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 353
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$7;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->passwordData:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$600(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$7;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->INDEX_LOCKSTATE:I
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$700(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {}, Lcom/sec/chaton/util/p;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$7;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$300(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 355
    const-string v1, "MODE"

    const-string v2, "PRIVACY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 356
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$7;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->startActivityForResult(Landroid/content/Intent;I)V

    .line 361
    :goto_0
    return v6

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$7;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    const-class v1, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0b01c5

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    goto :goto_0
.end method
