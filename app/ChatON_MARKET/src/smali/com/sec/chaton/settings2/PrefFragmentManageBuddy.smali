.class public Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentManageBuddy.java"


# instance fields
.field private isBirthdayCategory:Z

.field prefItemBirthdayCategory:Landroid/preference/CheckBoxPreference;

.field prefItemBlocked:Landroid/preference/Preference;

.field prefItemHided:Landroid/preference/Preference;

.field prefItemSuggestions:Landroid/preference/Preference;

.field private sharedPref:Lcom/sec/chaton/util/ab;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->isBirthdayCategory:Z

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->sharedPref:Lcom/sec/chaton/util/ab;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->isBirthdayCategory:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->sharedPref:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method private initializeForPreference(Landroid/preference/PreferenceScreen;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 81
    const-string v0, "pref_item_birthday"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->prefItemBirthdayCategory:Landroid/preference/CheckBoxPreference;

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Profile Birth Chk"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 83
    iput-boolean v3, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->isBirthdayCategory:Z

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Profile Birth Chk"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 89
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->prefItemBirthdayCategory:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->isBirthdayCategory:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b027e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->prefItemBirthdayCategory:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08003f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->prefItemBirthdayCategory:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy$1;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 110
    const-string v0, "pref_item_hided"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->prefItemHided:Landroid/preference/Preference;

    .line 113
    const-string v0, "pref_item_blocked"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->prefItemBlocked:Landroid/preference/Preference;

    .line 115
    const-string v0, "pref_item_suggestions"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->prefItemSuggestions:Landroid/preference/Preference;

    .line 116
    return-void

    .line 86
    :cond_0
    iput-boolean v4, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->isBirthdayCategory:Z

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Profile Birth Chk"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method private setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 122
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 123
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 124
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 41
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 43
    const v0, 0x7f050004

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->addPreferencesFromResource(I)V

    .line 46
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->initializeForPreference(Landroid/preference/PreferenceScreen;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 57
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    .line 60
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->prefItemHided:Landroid/preference/Preference;

    if-ne p2, v1, :cond_1

    .line 62
    const-class v1, Lcom/sec/chaton/settings2/PrefFragmentHide;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f0b031c

    invoke-virtual {p0, v4}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    move v3, v7

    .line 76
    :cond_0
    :goto_0
    return v3

    .line 65
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->prefItemBlocked:Landroid/preference/Preference;

    if-ne p2, v1, :cond_2

    .line 67
    const-class v1, Lcom/sec/chaton/settings2/PrefFragmentBlock;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f0b02cb

    invoke-virtual {p0, v4}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    move v3, v7

    .line 68
    goto :goto_0

    .line 70
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->prefItemSuggestions:Landroid/preference/Preference;

    if-ne p2, v1, :cond_0

    .line 72
    const-class v1, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f0b01b7

    invoke-virtual {p0, v4}, Lcom/sec/chaton/settings2/PrefFragmentManageBuddy;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    move v3, v7

    .line 73
    goto :goto_0
.end method
