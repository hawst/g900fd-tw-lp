.class Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;
.super Landroid/os/AsyncTask;
.source "PrefFragmentBackup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field encryptionBackupFile:Lcom/sec/chaton/localbackup/q;

.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V
    .locals 1

    .prologue
    .line 334
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 336
    new-instance v0, Lcom/sec/chaton/localbackup/q;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/q;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->encryptionBackupFile:Lcom/sec/chaton/localbackup/q;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentBackup;Lcom/sec/chaton/settings2/PrefFragmentBackup$1;)V
    .locals 0

    .prologue
    .line 334
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;-><init>(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 351
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->encryptionBackupFile:Lcom/sec/chaton/localbackup/q;

    sget-object v1, Lcom/sec/chaton/localbackup/s;->b:Lcom/sec/chaton/localbackup/s;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/localbackup/q;->a(Lcom/sec/chaton/localbackup/s;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->encryptionBackupFile:Lcom/sec/chaton/localbackup/q;

    const-string v2, "local-backup.db.crypt"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/localbackup/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 353
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->encryptionBackupFile:Lcom/sec/chaton/localbackup/q;

    sget-object v3, Lcom/sec/chaton/localbackup/t;->a:Lcom/sec/chaton/localbackup/t;

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/chaton/localbackup/q;->a(Lcom/sec/chaton/localbackup/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 362
    :goto_0
    return-object v0

    .line 356
    :catch_0
    move-exception v0

    .line 358
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 359
    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$700()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 362
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 334
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 368
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 370
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAlive:Z
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$800(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 391
    :goto_0
    return-void

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->mProgressDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$900(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 375
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->mProgressDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$900(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 378
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[File encryption] complete "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setLastmodified()V

    .line 381
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAutoBackup:Z
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$200(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 382
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$400(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b033e

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 383
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAutoBackup:Z
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$202(Lcom/sec/chaton/settings2/PrefFragmentBackup;Z)Z

    .line 390
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->mFileEncryptionTask:Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$1002(Lcom/sec/chaton/settings2/PrefFragmentBackup;Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;)Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;

    goto :goto_0

    .line 385
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$400(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0338

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 388
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$400(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0339

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 334
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 341
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 343
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->this$0:Lcom/sec/chaton/settings2/PrefFragmentBackup;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentBackup;->showProgressDialog()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->access$600(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V

    .line 344
    return-void
.end method
