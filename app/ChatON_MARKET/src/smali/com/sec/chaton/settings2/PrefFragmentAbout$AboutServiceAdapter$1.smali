.class Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter$1;
.super Ljava/lang/Object;
.source "PrefFragmentAbout.java"

# interfaces
.implements Lcom/coolots/sso/a/d;


# instance fields
.field final synthetic this$1:Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;)V
    .locals 0

    .prologue
    .line 624
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceiveUpdateVerion(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 627
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onReceiveUpdateVerion] ChatONV currentVersionName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",serverVersionName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",versionInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ActivityAbout"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    aput-object p1, v0, v1

    .line 631
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    aput-object p2, v0, v1

    .line 633
    if-eqz p3, :cond_0

    .line 634
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    const/4 v1, 0x1

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonVUpdate:Z
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$802(Lcom/sec/chaton/settings2/PrefFragmentAbout;Z)Z

    .line 635
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->notifyDataSetChanged()V

    .line 636
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chatonVUpdateStatus"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 640
    :cond_0
    return-void
.end method
