.class public Lcom/sec/chaton/settings2/PrefFragmentBackup;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentBackup.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final REQ_PASSWORD:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isAlive:Z

.field private isAutoBackup:Z

.field private mActivity:Landroid/preference/PreferenceActivity;

.field private mContext:Landroid/content/Context;

.field private mFileEncryptionTask:Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;

.field private mProgressDialog:Landroid/app/Dialog;

.field private mSDcardDialog:Landroid/app/Dialog;

.field private prefAutoBackup:Landroid/preference/CheckBoxPreference;

.field private prefBackupNow:Landroid/preference/Preference;

.field private prefsettingBackup:Landroid/preference/Preference;

.field private sharedPref:Lcom/sec/chaton/util/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/sec/chaton/settings2/PrefFragmentBackup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 334
    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->showSDcarDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/chaton/settings2/PrefFragmentBackup;Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;)Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mFileEncryptionTask:Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAutoBackup:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/chaton/settings2/PrefFragmentBackup;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAutoBackup:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->asyncFileEncryptionTask()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Landroid/preference/PreferenceActivity;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mActivity:Landroid/preference/PreferenceActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->showProgressDialog()V

    return-void
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAlive:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/chaton/settings2/PrefFragmentBackup;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mProgressDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method private asyncFileEncryptionTask()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 397
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;-><init>(Lcom/sec/chaton/settings2/PrefFragmentBackup;Lcom/sec/chaton/settings2/PrefFragmentBackup$1;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mFileEncryptionTask:Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;

    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[File encryption] start "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mFileEncryptionTask:Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 406
    :goto_0
    return-void

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mFileEncryptionTask:Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private initializeForPreference()V
    .locals 8

    .prologue
    const v7, 0x7f0b0333

    const v6, 0x7f08001f

    const v5, 0x7f08001c

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 127
    const-string v0, "pref_auto_backup"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    .line 129
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 159
    const-string v0, "pref_backup_now"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentBackup$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 178
    const-string v0, "pref_setting_backup"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefsettingBackup:Landroid/preference/Preference;

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefsettingBackup:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentBackup$3;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 193
    const-string v0, "pref_view_backedup"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 194
    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentBackup$4;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup$4;-><init>(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 211
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "auto_backup_on"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 217
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Theme"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 218
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 219
    const v0, 0x7f0b0336

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 226
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "setting_backup_enable"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 227
    invoke-virtual {p0, v4, v3}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->settingCheck(ZZ)V

    .line 233
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0344

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 236
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setLastmodified()V

    .line 237
    return-void

    .line 221
    :cond_1
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 222
    const v0, 0x7f0b0336

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_0

    .line 229
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefsettingBackup:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0341

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 116
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 118
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 120
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 122
    return-void
.end method

.method private showProgressDialog()V
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mProgressDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mProgressDialog:Landroid/app/Dialog;

    .line 326
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mProgressDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mProgressDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 332
    :cond_1
    return-void
.end method

.method private showSDcarDialog()V
    .locals 3

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mSDcardDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01e8

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b033a

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentBackup$5;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup$5;-><init>(Lcom/sec/chaton/settings2/PrefFragmentBackup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mSDcardDialog:Landroid/app/Dialog;

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mSDcardDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mSDcardDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mSDcardDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 291
    :cond_1
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const v7, 0x7f0b0336

    const v6, 0x7f0b0333

    const v5, 0x7f08001f

    const v4, 0x7f08001c

    const/4 v3, 0x1

    .line 296
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 298
    packed-switch p1, :pswitch_data_0

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 300
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "setting_backup_enable"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 304
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Theme"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 305
    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 306
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 311
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefsettingBackup:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0341

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    const v1, 0x7f0b033f

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 308
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 309
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_1

    .line 315
    :cond_2
    if-nez p2, :cond_0

    goto/16 :goto_0

    .line 298
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mActivity:Landroid/preference/PreferenceActivity;

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v0}, Lcom/sec/chaton/util/cm;->a(Landroid/app/Activity;)V

    .line 71
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 72
    const v0, 0x7f050013

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->addPreferencesFromResource(I)V

    .line 74
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mActivity:Landroid/preference/PreferenceActivity;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAlive:Z

    .line 85
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->initializeForPreference()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 93
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 88
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 89
    :catch_1
    move-exception v0

    .line 91
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mFileEncryptionTask:Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mFileEncryptionTask:Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup$FileEncryptionTask;->cancel(Z)Z

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0339

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 108
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAlive:Z

    .line 109
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroy()V

    .line 110
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public setLastmodified()V
    .locals 5

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->isAlive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0334

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "backup_last_modified"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 243
    :cond_0
    return-void
.end method

.method public settingCheck(ZZ)V
    .locals 5

    .prologue
    const v4, 0x7f08003f

    const/4 v3, 0x0

    .line 247
    if-eqz p1, :cond_1

    .line 249
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/localbackup/ActivitySecretKey;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 250
    const-string v1, "password_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 251
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->startActivityForResult(Landroid/content/Intent;I)V

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "setting_backup_enable"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 257
    const v0, 0x7f0b0333

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefAutoBackup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "auto_backup_on"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 260
    const v0, 0x7f0b0336

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->prefBackupNow:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentBackup;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 261
    if-nez p2, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentBackup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/localbackup/noti/a;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
