.class Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;
.super Ljava/lang/Object;
.source "PrefFragmentDeregister.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v3}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b029b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$102(Lcom/sec/chaton/settings2/PrefFragmentDeregister;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$100(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    new-instance v1, Lcom/coolots/sso/a/a;

    invoke-direct {v1}, Lcom/coolots/sso/a/a;-><init>()V

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mChatONV:Lcom/coolots/sso/a/a;
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$202(Lcom/sec/chaton/settings2/PrefFragmentDeregister;Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/a;

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mChatONV:Lcom/coolots/sso/a/a;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$200(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mChatONV:Lcom/coolots/sso/a/a;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$200(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mChatONV:Lcom/coolots/sso/a/a;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$200(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/coolots/sso/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->pushControl:Lcom/sec/chaton/d/a;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$300(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/sec/chaton/d/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mPushHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 149
    const-string v0, "ChatONV was NOT registered"

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$400()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->pushControl:Lcom/sec/chaton/d/a;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$300(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/sec/chaton/d/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mPushHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    goto :goto_0

    .line 154
    :cond_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 155
    const-string v0, "ChatONV was NOT installed"

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$400()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->pushControl:Lcom/sec/chaton/d/a;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$300(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/sec/chaton/d/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mPushHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    goto :goto_0
.end method
