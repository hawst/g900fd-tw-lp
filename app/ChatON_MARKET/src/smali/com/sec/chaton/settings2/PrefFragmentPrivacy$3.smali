.class Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;
.super Ljava/lang/Object;
.source "PrefFragmentPrivacy.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 261
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mNetworkError:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$200(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v5, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$300(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 287
    :goto_0
    return v5

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    const v3, 0x7f0b0282

    invoke-virtual {v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    const v4, 0x7f0b0147

    invoke-virtual {v3, v4}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    const v4, 0x7f0b0256

    invoke-virtual {v3, v4}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->showProfileImageNum:I
    invoke-static {v3}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$500(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)I

    move-result v3

    new-instance v4, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3$1;

    invoke-direct {v4, p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;)V

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v1

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mAlertDialog:Lcom/sec/common/a/d;
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$402(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    goto :goto_0
.end method
