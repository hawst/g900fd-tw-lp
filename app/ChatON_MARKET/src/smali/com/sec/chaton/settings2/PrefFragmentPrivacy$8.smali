.class Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;
.super Landroid/os/Handler;
.source "PrefFragmentPrivacy.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V
    .locals 0

    .prologue
    .line 535
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const v5, 0x7f0b0205

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 538
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 540
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$300(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 595
    :cond_0
    :goto_0
    return-void

    .line 544
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 552
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 555
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 556
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_3

    .line 557
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setPrivacyGuide()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$800(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V

    .line 558
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$300(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0089

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 560
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$900(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "show_phone_number_to_all"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 561
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$1000(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "samsung_account_show"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 562
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$300(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 567
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 568
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v2, v3, :cond_5

    .line 569
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mNetworkError:Ljava/lang/Boolean;
    invoke-static {v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$202(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 570
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPrivacyList;

    .line 571
    if-eqz v0, :cond_0

    .line 575
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPrivacyList;->privacy:Ljava/util/ArrayList;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->privacyArray:Ljava/util/ArrayList;
    invoke-static {v2, v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$1102(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 576
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->privacyArray:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$1100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 577
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->privacyArray:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$1100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->privacyArray:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$1100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PrivacyList;->_value:Ljava/lang/String;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->InitPrivacyCheck(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$1200(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 579
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setPrivacyGuide()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$800(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V

    .line 580
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 585
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mNetworkError:Ljava/lang/Boolean;
    invoke-static {v0, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$202(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 586
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$900(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 587
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$1000(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 588
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 589
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 591
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$300(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 552
    nop

    :sswitch_data_0
    .sparse-switch
        0x130 -> :sswitch_0
        0x142 -> :sswitch_1
    .end sparse-switch
.end method
