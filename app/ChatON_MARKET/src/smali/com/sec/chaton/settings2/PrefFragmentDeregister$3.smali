.class Lcom/sec/chaton/settings2/PrefFragmentDeregister$3;
.super Landroid/os/Handler;
.source "PrefFragmentDeregister.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 197
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 183
    const-string v0, "Push deregistration is success. execute chaton deregistration."

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$400()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :goto_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->dissmissProgress()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$500(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V

    .line 192
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01e2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 186
    :cond_1
    const-string v0, "Push deregistration is fail."

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$400()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentDeregister;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->regControl:Lcom/sec/chaton/d/ap;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$700(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/sec/chaton/d/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/ap;->a()Lcom/sec/chaton/d/a/ak;

    move-result-object v1

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mDeRegistrationTask:Lcom/sec/chaton/d/a/ak;
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->access$602(Lcom/sec/chaton/settings2/PrefFragmentDeregister;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;

    goto :goto_0
.end method
