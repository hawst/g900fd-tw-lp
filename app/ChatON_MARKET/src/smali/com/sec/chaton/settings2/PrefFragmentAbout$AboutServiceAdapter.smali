.class public Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;
.super Landroid/widget/BaseAdapter;
.source "PrefFragmentAbout.java"


# instance fields
.field private chatonVUpgradeListener:Lcom/coolots/sso/a/d;

.field private count:I

.field private isInstallChatONV:Z

.field private isInstallSPP:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private mServiceLogo:Landroid/widget/ImageView;

.field private mServiceName:Landroid/widget/TextView;

.field private sppUpdate:Z

.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentAbout;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 546
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 541
    iput v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->count:I

    .line 542
    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->sppUpdate:Z

    .line 543
    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallChatONV:Z

    .line 544
    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallSPP:Z

    .line 624
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->chatonVUpgradeListener:Lcom/coolots/sso/a/d;

    .line 547
    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 549
    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 550
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallChatONV:Z

    .line 551
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->count:I

    .line 553
    :cond_0
    const-string v0, "com.sec.spp.push"

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallSPP:Z

    .line 555
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->count:I

    .line 557
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallChatONV:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallSPP:Z

    if-ne v0, v1, :cond_2

    .line 558
    sput v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    .line 559
    const/4 v0, -0x1

    sput v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    .line 566
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->checkChatonupgradeStatus()V

    .line 567
    return-void
.end method

.method private checkChatonupgradeStatus()V
    .locals 6

    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallChatONV:Z

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    const-string v2, "com.coolots.chaton"

    invoke-static {v2}, Lcom/sec/chaton/util/cb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 574
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    new-instance v1, Lcom/coolots/sso/a/a;

    invoke-direct {v1}, Lcom/coolots/sso/a/a;-><init>()V

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonvAPII:Lcom/coolots/sso/a/a;
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$702(Lcom/sec/chaton/settings2/PrefFragmentAbout;Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/a;

    .line 575
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonvAPII:Lcom/coolots/sso/a/a;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$700(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 576
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonvAPII:Lcom/coolots/sso/a/a;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$700(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->chatonVUpgradeListener:Lcom/coolots/sso/a/d;

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/d;)V

    .line 577
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonvAPII:Lcom/coolots/sso/a/a;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$700(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Lcom/coolots/sso/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->n(Landroid/content/Context;)I

    .line 586
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallSPP:Z

    if-eqz v0, :cond_4

    .line 587
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    const-string v2, "com.sec.spp.push"

    invoke-static {v2}, Lcom/sec/chaton/util/cb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 590
    :try_start_0
    const-string v0, "content://com.sec.spp.provider/version_info"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 592
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 594
    if-eqz v0, :cond_6

    .line 595
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 596
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    sget v2, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    const-string v3, "current_version"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 597
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    sget v2, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    const-string v3, "latest_version"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 598
    const-string v1, "need_update"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->sppUpdate:Z

    .line 600
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 601
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SPP update status] currentVer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v2, v2, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " latestVer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v2, v2, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " needUpdate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->sppUpdate:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ActivityAbout"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->notifyDataSetChanged()V

    .line 605
    const-string v1, "spp_update_is"

    iget-boolean v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->sppUpdate:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 606
    iget-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->sppUpdate:Z

    if-eqz v1, :cond_2

    .line 609
    :cond_2
    const-string v1, "spp_latest_ver"

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v2, v2, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    aget-object v2, v2, v3

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 622
    :cond_4
    :goto_1
    return-void

    .line 579
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 580
    const-string v0, "chatonv update is NOT available"

    const-string v1, "ActivityAbout"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 614
    :cond_6
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 615
    const-string v0, "cursor is null"

    const-string v1, "ActivityAbout"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 618
    :catch_0
    move-exception v0

    .line 619
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ActivityAbout"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private initializeChatONV()V
    .locals 4

    .prologue
    .line 727
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceLogo:Landroid/widget/ImageView;

    const v1, 0x7f0203d7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 728
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceName:Landroid/widget/TextView;

    const v1, 0x7f0b03d4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 729
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionCurrent:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    aget-object v0, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v2}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v2, v2, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 730
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    aget-object v1, v0, v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v2}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    aget-object v0, v0, v3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    aget-object v0, v0, v3

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 731
    sget v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->setButton(I)V

    .line 732
    return-void

    .line 730
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    aget-object v0, v0, v3

    goto :goto_0
.end method

.method private initializeSPP()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 735
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceLogo:Landroid/widget/ImageView;

    const v1, 0x7f0203de

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 736
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceName:Landroid/widget/TextView;

    const v1, 0x7f0b029a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 737
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionCurrent:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    aget-object v0, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v2}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v2, v2, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 738
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    aget-object v1, v0, v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v2}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "spp_latest_ver"

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "spp_latest_ver"

    invoke-virtual {v0, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 740
    sget v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->setButton(I)V

    .line 741
    return-void

    .line 738
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    sget v3, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    aget-object v0, v0, v3

    goto :goto_0
.end method

.method private setButton(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 745
    if-nez p1, :cond_2

    .line 746
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAbout;->googleAppsUpdate(I)V
    invoke-static {v0, p1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$400(Lcom/sec/chaton/settings2/PrefFragmentAbout;I)V

    .line 747
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->downloadurl:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->downloadurl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 748
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 768
    :cond_0
    :goto_0
    return-void

    .line 750
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 752
    :cond_2
    sget v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->CHATON_V:I

    if-ne p1, v0, :cond_4

    .line 753
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAbout;->googleAppsUpdate(I)V
    invoke-static {v0, p1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$400(Lcom/sec/chaton/settings2/PrefFragmentAbout;I)V

    .line 755
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->chatonVUpdate:Z
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$800(Lcom/sec/chaton/settings2/PrefFragmentAbout;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 756
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 758
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 760
    :cond_4
    sget v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->SPP:I

    if-ne p1, v0, :cond_0

    .line 761
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAbout;->googleAppsUpdate(I)V
    invoke-static {v0, p1}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$400(Lcom/sec/chaton/settings2/PrefFragmentAbout;I)V

    .line 762
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->sppUpdate:Z

    if-nez v0, :cond_5

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "spp_update_is"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 763
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 765
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 647
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->count:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 654
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 660
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 667
    if-nez p2, :cond_0

    .line 668
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0300b6

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 671
    :cond_0
    const v0, 0x7f070338

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceLogo:Landroid/widget/ImageView;

    .line 673
    const v0, 0x7f070339

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceName:Landroid/widget/TextView;

    .line 675
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionCurrent:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v1

    const v0, 0x7f07033a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, p1

    .line 676
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v1

    const v0, 0x7f07033b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, p1

    .line 678
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mButton:[Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$300(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/Button;

    move-result-object v1

    const v0, 0x7f07033c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, p1

    .line 680
    packed-switch p1, :pswitch_data_0

    .line 722
    :cond_1
    :goto_0
    return-object p2

    .line 682
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceLogo:Landroid/widget/ImageView;

    const v1, 0x7f0203d7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 683
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceName:Landroid/widget/TextView;

    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 684
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mLogoImageClickCount:I
    invoke-static {v0, v4}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$902(Lcom/sec/chaton/settings2/PrefFragmentAbout;I)I

    .line 685
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceLogo:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 686
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 687
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->mServiceLogo:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 701
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionCurrent:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v0

    aget-object v0, v0, v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v2}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v2, v2, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 702
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v0

    aget-object v1, v0, v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    invoke-virtual {v2}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mNewVersion:[Ljava/lang/String;

    aget-object v0, v0, v4

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 703
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAbout;->mVersionLatest:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAbout;->access$200(Lcom/sec/chaton/settings2/PrefFragmentAbout;)[Landroid/widget/TextView;

    move-result-object v0

    aget-object v0, v0, v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 704
    invoke-direct {p0, v4}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->setButton(I)V

    goto/16 :goto_0

    .line 702
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAbout;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAbout;->mCurrentVersion:[Ljava/lang/String;

    aget-object v0, v0, v4

    goto :goto_1

    .line 708
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallChatONV:Z

    if-eqz v0, :cond_3

    .line 709
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->initializeChatONV()V

    goto/16 :goto_0

    .line 710
    :cond_3
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallSPP:Z

    if-eqz v0, :cond_1

    .line 711
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->initializeSPP()V

    goto/16 :goto_0

    .line 716
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->isInstallSPP:Z

    if-eqz v0, :cond_1

    .line 717
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAbout$AboutServiceAdapter;->initializeSPP()V

    goto/16 :goto_0

    .line 680
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
