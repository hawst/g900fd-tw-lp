.class public Lcom/sec/chaton/settings2/PrefFragmentAlertType;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentAlertType.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final SILENT_STRING:Ljava/lang/String; = "Silent"


# instance fields
.field private mActivity:Landroid/preference/PreferenceActivity;

.field private mLedPreference:Landroid/preference/CheckBoxPreference;

.field private mPref:Lcom/sec/chaton/util/ab;

.field private mSoundPreference:Landroid/preference/CheckBoxPreference;

.field private mSoundTypePreference:Landroid/preference/Preference;

.field private mVibrationPreference:Landroid/preference/CheckBoxPreference;

.field private prefItemTitle:Lcom/sec/chaton/settings2/BreadCrumbsPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    return-void
.end method

.method private getCurrentRingtoneTitle()Ljava/lang/String;
    .locals 6

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Ringtone title"

    const v2, 0x7f0b01a5

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Ringtone"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 156
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 157
    const-string v0, "content://"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_6

    .line 160
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-virtual {v0, v1}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 186
    :cond_0
    :goto_0
    return-object v0

    .line 162
    :cond_1
    const-string v0, "android.resource://"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    invoke-static {}, Lcom/sec/chaton/settings/downloads/az;->a()Ljava/util/Map;

    move-result-object v0

    .line 164
    const/4 v3, 0x0

    .line 165
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 166
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings/downloads/az;

    iget-object v1, v1, Lcom/sec/chaton/settings/downloads/az;->s:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 167
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings/downloads/az;

    iget-object v2, v0, Lcom/sec/chaton/settings/downloads/az;->o:Ljava/lang/String;

    .line 168
    const/4 v0, 0x1

    move v1, v0

    move-object v0, v2

    .line 172
    :goto_1
    if-nez v1, :cond_0

    .line 173
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->initDefaultRingtoneTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 176
    :cond_3
    const-string v0, "file://"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 177
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x7

    invoke-virtual {v4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 178
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    .line 179
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->initDefaultRingtoneTitle()Ljava/lang/String;

    move-result-object v2

    :cond_4
    move-object v0, v2

    .line 181
    goto :goto_0

    .line 183
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->initDefaultRingtoneTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    move-object v0, v2

    goto :goto_0

    :cond_7
    move v1, v3

    move-object v0, v2

    goto :goto_1
.end method

.method private initDefaultRingtoneTitle()Ljava/lang/String;
    .locals 5

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mActivity:Landroid/preference/PreferenceActivity;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    .line 193
    const/4 v0, 0x0

    .line 195
    if-eqz v1, :cond_0

    .line 196
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v3, "Ringtone"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v2, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v1

    .line 199
    if-eqz v1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-virtual {v1, v0}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Ringtone title"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_0
    return-object v0
.end method

.method private setAlertPreference()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 83
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v2}, Lcom/sec/chaton/chat/notification/a;->a(Landroid/content/Context;)Lcom/sec/chaton/chat/notification/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/chat/notification/a;->i()Ljava/lang/String;

    move-result-object v2

    .line 87
    const-string v3, "ALL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    move v2, v1

    .line 105
    :goto_0
    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 106
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mVibrationPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mLedPreference:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v3, "LED Indicator"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 109
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->getCurrentRingtoneTitle()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundTypePreference:Landroid/preference/Preference;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->setSummaryWithColor(Ljava/lang/String;Landroid/preference/Preference;Z)V

    .line 110
    return-void

    .line 91
    :cond_0
    const-string v3, "MELODY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v1

    .line 94
    goto :goto_0

    .line 95
    :cond_1
    const-string v3, "VIBRATION"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v0

    move v0, v1

    .line 98
    goto :goto_0

    :cond_2
    move v2, v0

    .line 102
    goto :goto_0
.end method

.method private setSummaryWithColor(Ljava/lang/String;Landroid/preference/Preference;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 239
    if-eqz p1, :cond_1

    .line 240
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 241
    if-nez p3, :cond_0

    .line 242
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 244
    :cond_0
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 245
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-interface {v1, v2, v3, v0, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 246
    invoke-virtual {p2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 248
    :cond_1
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mActivity:Landroid/preference/PreferenceActivity;

    .line 51
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v0}, Lcom/sec/chaton/util/cm;->a(Landroid/app/Activity;)V

    .line 52
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v0, 0x7f050003

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->addPreferencesFromResource(I)V

    .line 55
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    .line 58
    const-string v0, "pref_item_breadcrumbs"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->prefItemTitle:Lcom/sec/chaton/settings2/BreadCrumbsPreference;

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->prefItemTitle:Lcom/sec/chaton/settings2/BreadCrumbsPreference;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mActivity:Landroid/preference/PreferenceActivity;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->setActivity(Landroid/app/Activity;)V

    .line 61
    const-string v0, "pref_item_led"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mLedPreference:Landroid/preference/CheckBoxPreference;

    .line 62
    const-string v0, "pref_item_sound"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundPreference:Landroid/preference/CheckBoxPreference;

    .line 63
    const-string v0, "pref_item_vibration"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mVibrationPreference:Landroid/preference/CheckBoxPreference;

    .line 64
    const-string v0, "pref_item_sound_type"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundTypePreference:Landroid/preference/Preference;

    .line 66
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_1

    .line 67
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 68
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mVibrationPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mVibrationPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mLedPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mVibrationPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundTypePreference:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 78
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundTypePreference:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundTypePreference:Landroid/preference/Preference;

    check-cast v0, Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->b()V

    .line 232
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroy()V

    .line 233
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 217
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPause()V

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundTypePreference:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundTypePreference:Landroid/preference/Preference;

    check-cast v0, Lcom/sec/chaton/settings/RingtonePreference2;

    invoke-virtual {v0}, Lcom/sec/chaton/settings/RingtonePreference2;->a()V

    .line 224
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mVibrationPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    .line 119
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundPreference:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_2

    move-object v0, p2

    .line 120
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 121
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->getCurrentRingtoneTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundTypePreference:Landroid/preference/Preference;

    invoke-direct {p0, v2, v3, v0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->setSummaryWithColor(Ljava/lang/String;Landroid/preference/Preference;Z)V

    move v4, v1

    move v1, v0

    move v0, v4

    .line 126
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundPreference:Landroid/preference/CheckBoxPreference;

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mVibrationPreference:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_6

    .line 127
    :cond_0
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Set Type"

    const-string v2, "ALL"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_1
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mVibrationPreference:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_a

    move-object v0, p2

    .line 123
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v1, v2

    goto :goto_0

    .line 129
    :cond_3
    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Set Type"

    const-string v2, "MELODY"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 131
    :cond_4
    if-nez v1, :cond_5

    if-eqz v0, :cond_5

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Set Type"

    const-string v2, "VIBRATION"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 134
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Set Type"

    const-string v2, "OFF"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 136
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundTypePreference:Landroid/preference/Preference;

    if-ne p1, v0, :cond_9

    .line 137
    check-cast p2, Ljava/lang/String;

    .line 138
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_8

    .line 139
    :cond_7
    const-string p2, "Silent"

    .line 141
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v2, "Ringtone"

    invoke-virtual {v0, v2, p2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->getCurrentRingtoneTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 143
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->getCurrentRingtoneTitle()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mSoundTypePreference:Landroid/preference/Preference;

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->setSummaryWithColor(Ljava/lang/String;Landroid/preference/Preference;Z)V

    goto :goto_1

    .line 144
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mLedPreference:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->mPref:Lcom/sec/chaton/util/ab;

    const-string v1, "LED Indicator"

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {v0, v1, p2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    :cond_a
    move v0, v1

    move v1, v2

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 209
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 210
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentAlertType;->setAlertPreference()V

    .line 211
    return-void
.end method
