.class public Lcom/sec/chaton/settings2/PrefFragmentSuggestion;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentSuggestion.java"


# instance fields
.field private control:Lcom/sec/chaton/d/h;

.field private ctx:Landroid/content/Context;

.field private isExcludeMe:Z

.field private isSpecialbuddyReceive:Z

.field private isSuggestionsReceive:Z

.field private mAttachedActivity:Landroid/app/Activity;

.field private mHandler:Landroid/os/Handler;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mUiHandler:Landroid/os/Handler;

.field prefItemAbout:Lcom/sec/chaton/settings/AboutNewNotice;

.field private prefItemExcludeMe:Landroid/preference/CheckBoxPreference;

.field private prefItemSpecialbuddySuggestions:Landroid/preference/CheckBoxPreference;

.field private prefItemSuggestions:Landroid/preference/CheckBoxPreference;

.field private prefItemTitle:Lcom/sec/chaton/settings2/BreadCrumbsPreference;

.field private privacyArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/PrivacyList;",
            ">;"
        }
    .end annotation
.end field

.field private privacyControl:Lcom/sec/chaton/d/h;

.field private sharedPref:Lcom/sec/chaton/util/ab;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 49
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 52
    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z

    .line 53
    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSpecialbuddyReceive:Z

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isExcludeMe:Z

    .line 56
    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 64
    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 121
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mHandler:Landroid/os/Handler;

    .line 389
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$4;-><init>(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method private InitPrivacyCheck(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 161
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 162
    :cond_0
    const-string v0, "have no privacy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_1
    :goto_0
    return-void

    .line 164
    :cond_2
    const-string v0, "recommendationbuddy"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 165
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemSuggestions:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "recomned_receive"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 172
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recommendationbuddy : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemSuggestions:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "recomned_receive"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    .line 173
    :cond_4
    const-string v0, "ignorerecommendation"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemExcludeMe:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "exclude_me"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 181
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ignorerecommendation : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 178
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemExcludeMe:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "exclude_me"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->privacyArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->privacyArray:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->InitPrivacyCheck(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->control:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isExcludeMe:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemSuggestions:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private initializeForPreference()V
    .locals 6

    .prologue
    const v5, 0x7f08003f

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 188
    const-string v0, "pref_item_breadcrumbs"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemTitle:Lcom/sec/chaton/settings2/BreadCrumbsPreference;

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemTitle:Lcom/sec/chaton/settings2/BreadCrumbsPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->setActivity(Landroid/app/Activity;)V

    .line 192
    const-string v0, "pref_item_buddy_sugestions"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemSuggestions:Landroid/preference/CheckBoxPreference;

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "recomned_receive"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 194
    iput-boolean v3, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "recomned_receive"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 200
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemSuggestions:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0218

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemSuggestions:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemSuggestions:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "recomned_special"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 239
    iput-boolean v3, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSpecialbuddyReceive:Z

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "recomned_special"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 287
    :cond_0
    const-string v0, "pref_item_exclude_me"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemExcludeMe:Landroid/preference/CheckBoxPreference;

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "exclude_me"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 291
    iput-boolean v4, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isExcludeMe:Z

    .line 292
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "exclude_me"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 298
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemExcludeMe:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isExcludeMe:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 299
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0217

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemExcludeMe:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemExcludeMe:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$3;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentSuggestion;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->prefItemExcludeMe:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0216

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 341
    return-void

    .line 197
    :cond_1
    iput-boolean v4, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isSuggestionsReceive:Z

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "recomned_receive"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 294
    :cond_2
    iput-boolean v3, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->isExcludeMe:Z

    .line 295
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "exclude_me"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1
.end method

.method private setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 349
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 350
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 351
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 352
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mAttachedActivity:Landroid/app/Activity;

    .line 77
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 78
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 345
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 346
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 88
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mAttachedActivity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->ctx:Landroid/content/Context;

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mAttachedActivity:Landroid/app/Activity;

    invoke-static {v0, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 98
    const v0, 0x7f050005

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->addPreferencesFromResource(I)V

    .line 100
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mUiHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->control:Lcom/sec/chaton/d/h;

    .line 101
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 105
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->initializeForPreference()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_0
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->privacyControl:Lcom/sec/chaton/d/h;

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->privacyControl:Lcom/sec/chaton/d/h;

    const-string v1, "recommendationbuddy|ignorerecommendation"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->g(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 114
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 71
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 369
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroy()V

    .line 371
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 373
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 377
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentSuggestion;->mAttachedActivity:Landroid/app/Activity;

    .line 83
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDetach()V

    .line 84
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 118
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 357
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPause()V

    .line 359
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 363
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 365
    return-void
.end method
