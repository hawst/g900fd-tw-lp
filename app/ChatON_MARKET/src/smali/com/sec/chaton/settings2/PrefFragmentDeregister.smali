.class public Lcom/sec/chaton/settings2/PrefFragmentDeregister;
.super Landroid/app/Fragment;
.source "PrefFragmentDeregister.java"

# interfaces
.implements Lcom/coolots/sso/a/c;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAgreeCheckBox:Landroid/widget/CheckBox;

.field private mChatONV:Lcom/coolots/sso/a/a;

.field private mDeRegistrationTask:Lcom/sec/chaton/d/a/ak;

.field private mDeregisterTextView:Landroid/widget/TextView;

.field private mErrorDialog:Lcom/sec/common/a/d;

.field private mOkButton:Landroid/widget/Button;

.field mPushHandler:Landroid/os/Handler;

.field public mUiHandler:Landroid/os/Handler;

.field private progressBar:Landroid/app/ProgressDialog;

.field private pushControl:Lcom/sec/chaton/d/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/chaton/d/a",
            "<*>;"
        }
    .end annotation
.end field

.field private regControl:Lcom/sec/chaton/d/ap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/sec/chaton/settings2/SettingActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->progressBar:Landroid/app/ProgressDialog;

    .line 174
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$3;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mPushHandler:Landroid/os/Handler;

    .line 206
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister$4;-><init>(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->checkAcceptStatus()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->progressBar:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/chaton/settings2/PrefFragmentDeregister;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->progressBar:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/coolots/sso/a/a;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mChatONV:Lcom/coolots/sso/a/a;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/chaton/settings2/PrefFragmentDeregister;Lcom/coolots/sso/a/a;)Lcom/coolots/sso/a/a;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mChatONV:Lcom/coolots/sso/a/a;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/sec/chaton/d/a;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->pushControl:Lcom/sec/chaton/d/a;

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->dissmissProgress()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/sec/chaton/d/a/ak;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mDeRegistrationTask:Lcom/sec/chaton/d/a/ak;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/chaton/settings2/PrefFragmentDeregister;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mDeRegistrationTask:Lcom/sec/chaton/d/a/ak;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)Lcom/sec/chaton/d/ap;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->regControl:Lcom/sec/chaton/d/ap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/chaton/settings2/PrefFragmentDeregister;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->showErrorDialog(Ljava/lang/String;)V

    return-void
.end method

.method private checkAcceptStatus()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mAgreeCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mOkButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 171
    :goto_0
    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mOkButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private closeDialog()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mErrorDialog:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mErrorDialog:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mErrorDialog:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 281
    :cond_0
    return-void
.end method

.method private dissmissProgress()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->progressBar:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->progressBar:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->progressBar:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 204
    :cond_0
    return-void
.end method

.method private showErrorDialog(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 241
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showErrorDialog : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->closeDialog()V

    .line 245
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b029b

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b016c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p1}, Lcom/sec/chaton/util/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentDeregister$5;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister$5;-><init>(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mErrorDialog:Lcom/sec/common/a/d;

    .line 257
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 67
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mUiHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->regControl:Lcom/sec/chaton/d/ap;

    .line 68
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->pushControl:Lcom/sec/chaton/d/a;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mErrorDialog:Lcom/sec/common/a/d;

    .line 70
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 74
    const v0, 0x7f03009a

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 77
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 79
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v1, v3, :cond_2

    .line 80
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 86
    :goto_0
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 87
    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 88
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 89
    instance-of v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 90
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move-object v0, v1

    .line 91
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v3, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 92
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_0

    move-object v0, v1

    .line 93
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 94
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 99
    :cond_0
    const v0, 0x7f070416

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 100
    const v0, 0x7f07050b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 101
    const v3, 0x7f0b029b

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 104
    const v0, 0x7f07050d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 105
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 106
    const v0, 0x7f07050c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 107
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 108
    const v0, 0x7f07050e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 109
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 111
    const v0, 0x7f0702f6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mDeregisterTextView:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0702f7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mAgreeCheckBox:Landroid/widget/CheckBox;

    .line 113
    const v0, 0x7f0702f8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mOkButton:Landroid/widget/Button;

    .line 115
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    .line 117
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mDeregisterTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b03d9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mAgreeCheckBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentDeregister$1;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mOkButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentDeregister;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    return-object v2

    .line 83
    :cond_2
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 263
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 265
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mChatONV:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mChatONV:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 269
    const-string v0, "onDestroy:mChatonV.setListener(null)"

    sget-object v1, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->closeDialog()V

    .line 274
    return-void
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 287
    return-void
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 2

    .prologue
    .line 292
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveRemoveAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->pushControl:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentDeregister;->mPushHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 306
    return-void
.end method
