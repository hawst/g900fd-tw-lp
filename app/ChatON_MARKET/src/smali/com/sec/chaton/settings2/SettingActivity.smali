.class public Lcom/sec/chaton/settings2/SettingActivity;
.super Lcom/sec/chaton/settings/BasePreferenceActivity;
.source "SettingActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final PREF_SETTING_LEGACY_ABOUT:Ljava/lang/String; = "pref_setting_legacy_about"

.field private static final PREF_SETTING_LEGACY_ALERTS:Ljava/lang/String; = "pref_setting_legacy_alerts"

.field private static final PREF_SETTING_LEGACY_CALL_SETTINGS:Ljava/lang/String; = "pref_setting_legacy_call_settings"

.field private static final PREF_SETTING_LEGACY_CHAT_DISPLAY:Ljava/lang/String; = "pref_setting_legacy_chat_display"

.field private static final PREF_SETTING_LEGACY_CONNECTED_DEVICES:Ljava/lang/String; = "pref_setting_legacy_connected_devices"

.field private static final PREF_SETTING_LEGACY_CONTACT_SYNC:Ljava/lang/String; = "pref_setting_legacy_contact_sync"

.field private static final PREF_SETTING_LEGACY_DELETE_ACCOUNT:Ljava/lang/String; = "pref_setting_legacy_delete_account"

.field private static final PREF_SETTING_LEGACY_LOG_COLLECTOR:Ljava/lang/String; = "pref_setting_legacy_log_collector"

.field private static final PREF_SETTING_LEGACY_MANAGE_ACCOUNTS:Ljava/lang/String; = "pref_setting_legacy_manage_accounts"

.field private static final PREF_SETTING_LEGACY_MANAGE_BUDDIES:Ljava/lang/String; = "pref_setting_legacy_manage_buddies"

.field private static final PREF_SETTING_LEGACY_PREF_SCREEN:Ljava/lang/String; = "pref_setting_legacy_pref_screen"

.field private static final PREF_SETTING_LEGACY_PRIVACY:Ljava/lang/String; = "pref_setting_legacy_privacy"

.field private static final PREF_SETTING_LEGACY_SEND_LOG:Ljava/lang/String; = "pref_setting_legacy_send_log"

.field private static final PREF_SETTING_LOCAL_BACKUP:Ljava/lang/String; = "pref_setting_backup"

.field private static _headers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private deleteAccountlayout:Landroid/view/View;

.field private headerAdapter:Lcom/sec/chaton/settings2/HeaderAdapter;

.field private isDestroy:Z

.field private mChatonV:Lcom/coolots/sso/a/a;

.field private mSettingNewBadgeReceiver:Landroid/content/BroadcastReceiver;

.field private prefAbout:Lcom/sec/chaton/settings/AboutNewNotice;

.field private progressBar:Landroid/app/ProgressDialog;

.field private pushControl:Lcom/sec/chaton/d/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/chaton/d/a",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;-><init>()V

    .line 84
    iput-object v1, p0, Lcom/sec/chaton/settings2/SettingActivity;->deleteAccountlayout:Landroid/view/View;

    .line 87
    iput-object v1, p0, Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->isDestroy:Z

    .line 90
    iput-object v1, p0, Lcom/sec/chaton/settings2/SettingActivity;->headerAdapter:Lcom/sec/chaton/settings2/HeaderAdapter;

    .line 603
    new-instance v0, Lcom/sec/chaton/settings2/SettingActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/SettingActivity$5;-><init>(Lcom/sec/chaton/settings2/SettingActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->mSettingNewBadgeReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/SettingActivity;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->isDestroy:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/SettingActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/SettingActivity;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/chaton/settings2/SettingActivity;->updateSettingTabBadge()V

    return-void
.end method

.method private initializeLegacyPreferences()V
    .locals 5

    .prologue
    .line 318
    const-string v0, "pref_setting_legacy_pref_screen"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 319
    new-instance v1, Lcom/coolots/sso/a/a;

    invoke-direct {v1}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/settings2/SettingActivity;->mChatonV:Lcom/coolots/sso/a/a;

    .line 320
    const-string v1, "pref_setting_legacy_manage_buddies"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 321
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 323
    const-string v1, "pref_setting_legacy_privacy"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 324
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 326
    const-string v1, "pref_setting_legacy_alerts"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 327
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 329
    const-string v1, "pref_setting_legacy_chat_display"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 330
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 333
    const-string v1, "pref_setting_legacy_call_settings"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 334
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v2, p0, v3}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/chaton/settings2/SettingActivity;->mChatonV:Lcom/coolots/sso/a/a;

    invoke-virtual {v2, p0}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 336
    :cond_0
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 342
    :goto_0
    const-string v1, "pref_setting_legacy_connected_devices"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 343
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 348
    const-string v1, "pref_setting_backup"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 349
    const-string v2, "local_backup_feature"

    invoke-static {v2}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 350
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 355
    :goto_1
    const-string v1, "pref_setting_legacy_contact_sync"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 356
    const-string v2, "for_wifi_only_device"

    invoke-static {v2}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/sec/chaton/util/am;->w()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 357
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "msisdn"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 358
    const-string v2, "support_contact_auto_sync"

    invoke-static {v2}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 359
    invoke-direct {p0}, Lcom/sec/chaton/settings2/SettingActivity;->setSummaryContactSync()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 361
    :cond_2
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 372
    :goto_2
    const-string v1, "pref_setting_legacy_about"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/settings/AboutNewNotice;

    iput-object v1, p0, Lcom/sec/chaton/settings2/SettingActivity;->prefAbout:Lcom/sec/chaton/settings/AboutNewNotice;

    .line 373
    iget-object v1, p0, Lcom/sec/chaton/settings2/SettingActivity;->prefAbout:Lcom/sec/chaton/settings/AboutNewNotice;

    invoke-virtual {v1, p0}, Lcom/sec/chaton/settings/AboutNewNotice;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 375
    const-string v1, "pref_setting_legacy_delete_account"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 376
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 379
    const-string v1, "pref_setting_legacy_send_log"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 380
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 383
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/util/y;->g:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_8

    .line 390
    :goto_3
    const-string v1, "pref_setting_legacy_log_collector"

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 391
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 393
    invoke-static {}, Lcom/sec/chaton/util/y;->b()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    .line 398
    :goto_4
    return-void

    .line 339
    :cond_3
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 352
    :cond_4
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    .line 363
    :cond_5
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2

    .line 366
    :cond_6
    const-string v2, "support_contact_auto_sync"

    invoke-static {v2}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 367
    invoke-direct {p0}, Lcom/sec/chaton/settings2/SettingActivity;->setSummaryContactSync()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 369
    :cond_7
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_2

    .line 386
    :cond_8
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3

    .line 396
    :cond_9
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_4
.end method

.method private setSummaryContactSync()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 571
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 573
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 574
    const-string v0, ""

    .line 600
    :goto_0
    return-object v0

    .line 577
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "auto_contact_sync"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 581
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "auto_contact_sync"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 582
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 587
    :goto_1
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 589
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting Sync TimeInMillis"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 591
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_2

    .line 592
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600
    :goto_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 584
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01c4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 594
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0244

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method private updateSettingTabBadge()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 542
    .line 544
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Update contain is "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "UpdateIsCritical"

    invoke-virtual {v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v3, "UpdateIsCritical"

    invoke-virtual {v0, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    .line 548
    :goto_0
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "chatonVUpdateStatus"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_0

    .line 550
    add-int/lit8 v0, v0, 0x1

    .line 552
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "spp_update_is"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 553
    add-int/lit8 v0, v0, 0x1

    .line 555
    :cond_1
    iget-object v3, p0, Lcom/sec/chaton/settings2/SettingActivity;->prefAbout:Lcom/sec/chaton/settings/AboutNewNotice;

    if-eqz v3, :cond_2

    .line 556
    iget-object v3, p0, Lcom/sec/chaton/settings2/SettingActivity;->prefAbout:Lcom/sec/chaton/settings/AboutNewNotice;

    if-lez v0, :cond_4

    :goto_1
    invoke-virtual {v3, v1, v0}, Lcom/sec/chaton/settings/AboutNewNotice;->a(ZI)V

    .line 568
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 546
    goto :goto_0

    :cond_4
    move v1, v2

    .line 556
    goto :goto_1
.end method


# virtual methods
.method public onBuildHeaders(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 219
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onBuildHeaders(Ljava/util/List;)V

    .line 221
    sput-object p1, Lcom/sec/chaton/settings2/SettingActivity;->_headers:Ljava/util/List;

    .line 223
    const-string v0, "tablet_enable_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 224
    const-string v0, "for_wifi_only_device"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 225
    const v0, 0x7f05000b

    invoke-virtual {p0, v0, p1}, Lcom/sec/chaton/settings2/SettingActivity;->loadHeadersFromResource(ILjava/util/List;)V

    .line 231
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/util/y;->g:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_1

    .line 232
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 233
    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f07055a

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 234
    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 241
    :cond_1
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->mChatonV:Lcom/coolots/sso/a/a;

    .line 242
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, p0, v1}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->mChatonV:Lcom/coolots/sso/a/a;

    invoke-virtual {v0, p0}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/chaton/util/am;->r()Z

    move-result v0

    if-nez v0, :cond_4

    .line 244
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 245
    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f070559

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 246
    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 253
    :cond_4
    return-void

    .line 227
    :cond_5
    const v0, 0x7f05000a

    invoke-virtual {p0, v0, p1}, Lcom/sec/chaton/settings2/SettingActivity;->loadHeadersFromResource(ILjava/util/List;)V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 111
    const-string v0, "tablet_enable_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 113
    const v0, 0x7f05000c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/SettingActivity;->addPreferencesFromResource(I)V

    .line 115
    invoke-direct {p0}, Lcom/sec/chaton/settings2/SettingActivity;->initializeLegacyPreferences()V

    .line 122
    :goto_0
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->pushControl:Lcom/sec/chaton/d/a;

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->pushControl:Lcom/sec/chaton/d/a;

    new-instance v1, Lcom/sec/chaton/settings2/SettingActivity$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/chaton/settings2/SettingActivity$1;-><init>(Lcom/sec/chaton/settings2/SettingActivity;Landroid/os/Looper;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->c(Landroid/os/Handler;)V

    .line 143
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "gotoAlert"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/settings/ActivityNoti;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    const-string v1, "gotoAlert"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 146
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 149
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fragment_choose"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fragment_choose"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 151
    const-class v1, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 152
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->switchToHeader(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 160
    :cond_1
    :goto_1
    return-void

    .line 117
    :cond_2
    const v0, 0x7f0b00da

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/SettingActivity;->setTitle(I)V

    goto :goto_0

    .line 153
    :cond_3
    const-class v1, Lcom/sec/chaton/settings2/PrefFragmentBirthday;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 155
    const-string v2, "fragment_choose"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-class v0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->switchToHeader(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 197
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onDestroy()V

    .line 198
    const-string v0, "[LIFE] onDestroy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->isDestroy:Z

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 207
    :cond_0
    return-void
.end method

.method public onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V
    .locals 6

    .prologue
    const-wide/32 v4, 0x7f070559

    .line 258
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V

    .line 259
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f07055a

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 260
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;

    .line 261
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v0

    .line 262
    new-instance v1, Lcom/sec/chaton/settings2/SettingActivity$2;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/chaton/settings2/SettingActivity$2;-><init>(Lcom/sec/chaton/settings2/SettingActivity;Landroid/os/Looper;)V

    .line 282
    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->c(Landroid/os/Handler;)V

    .line 294
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->headerAdapter:Lcom/sec/chaton/settings2/HeaderAdapter;

    if-eqz v0, :cond_1

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 295
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->headerAdapter:Lcom/sec/chaton/settings2/HeaderAdapter;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/settings2/HeaderAdapter;->setSelectedPosition(I)V

    .line 296
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 300
    :cond_1
    return-void

    .line 283
    :cond_2
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->mChatonV:Lcom/coolots/sso/a/a;

    invoke-virtual {v0, p0}, Lcom/coolots/sso/a/a;->e(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 285
    if-eqz v0, :cond_3

    .line 286
    invoke-static {v0}, Lcom/sec/chaton/util/am;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 287
    const-string v0, "success : call log intent"

    const-string v1, "SettingActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 289
    :cond_3
    const-string v0, "fail : call log intent"

    const-string v1, "SettingActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 211
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onPause()V

    .line 212
    const-string v0, "[LIFE] onPause"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/SettingActivity;->mSettingNewBadgeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 215
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f0b0041

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 402
    const-string v2, "pref_setting_legacy_manage_buddies"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 403
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityManageBuddy;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 537
    :goto_0
    return v0

    .line 408
    :cond_0
    const-string v2, "pref_setting_legacy_privacy"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 409
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 414
    :cond_1
    const-string v2, "pref_setting_legacy_alerts"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 415
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityNoti;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 420
    :cond_2
    const-string v2, "pref_setting_legacy_chat_display"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 421
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityChat;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 427
    :cond_3
    const-string v2, "pref_setting_legacy_call_settings"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 428
    iget-object v1, p0, Lcom/sec/chaton/settings2/SettingActivity;->mChatonV:Lcom/coolots/sso/a/a;

    invoke-virtual {v1, p0}, Lcom/coolots/sso/a/a;->e(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 430
    if-eqz v1, :cond_4

    .line 431
    invoke-static {v1}, Lcom/sec/chaton/util/am;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 432
    const-string v1, "success : call log intent"

    const-string v2, "SettingActivity"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 434
    :cond_4
    const-string v1, "fail : call log intent"

    const-string v2, "SettingActivity"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 441
    :cond_5
    const-string v2, "pref_setting_legacy_connected_devices"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 442
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityMultiDeviceView;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 453
    :cond_6
    const-string v2, "pref_setting_backup"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 454
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/localbackup/ActivityLocalBackup;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 458
    :cond_7
    const-string v2, "pref_setting_legacy_contact_sync"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 459
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "msisdn"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 460
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/registration/ActivityRegist;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 462
    :cond_8
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityContactSync;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 468
    :cond_9
    const-string v2, "pref_setting_legacy_about"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 469
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityAboutService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 474
    :cond_a
    const-string v2, "pref_setting_legacy_delete_account"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 475
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityDeregister;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/SettingActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 479
    :cond_b
    const-string v2, "pref_setting_legacy_send_log"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 482
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v5, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;

    .line 483
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v1

    .line 484
    new-instance v2, Lcom/sec/chaton/settings2/SettingActivity$3;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/sec/chaton/settings2/SettingActivity$3;-><init>(Lcom/sec/chaton/settings2/SettingActivity;Landroid/os/Looper;)V

    .line 509
    invoke-virtual {v1, v2}, Lcom/sec/chaton/d/a;->c(Landroid/os/Handler;)V

    goto/16 :goto_0

    .line 514
    :cond_c
    const-string v2, "pref_setting_legacy_log_collector"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 515
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v5, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;

    .line 517
    new-instance v2, Lcom/sec/chaton/settings2/SettingActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/SettingActivity$4;-><init>(Lcom/sec/chaton/settings2/SettingActivity;)V

    .line 527
    invoke-static {v2}, Lcom/sec/chaton/util/logcollector/LogCollectService;->a(Landroid/os/Handler;)V

    .line 529
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v0, v1}, Lcom/sec/chaton/util/logcollector/LogCollectService;->a(Landroid/content/Context;ZZZ)V

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 537
    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 174
    invoke-super {p0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onResume()V

    .line 175
    const-string v0, "[LIFE] onResume"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 178
    if-eqz v0, :cond_0

    .line 180
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 182
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    const v1, 0x7f020414

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 188
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/settings2/SettingActivity;->updateSettingTabBadge()V

    .line 189
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 190
    const-string v1, "more_tab_badge_update"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/SettingActivity;->mSettingNewBadgeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 192
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 164
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/SettingActivity;->finish()V

    .line 166
    const/4 v0, 0x1

    .line 168
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/chaton/settings/BasePreferenceActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 306
    if-nez p1, :cond_0

    .line 307
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 315
    :goto_0
    return-void

    .line 310
    :cond_0
    new-instance v0, Lcom/sec/chaton/settings2/HeaderAdapter;

    sget-object v1, Lcom/sec/chaton/settings2/SettingActivity;->_headers:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings2/HeaderAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->headerAdapter:Lcom/sec/chaton/settings2/HeaderAdapter;

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity;->headerAdapter:Lcom/sec/chaton/settings2/HeaderAdapter;

    invoke-super {p0, v0}, Lcom/sec/chaton/settings/BasePreferenceActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method
