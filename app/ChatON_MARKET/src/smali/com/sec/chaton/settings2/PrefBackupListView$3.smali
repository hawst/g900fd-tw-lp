.class Lcom/sec/chaton/settings2/PrefBackupListView$3;
.super Ljava/lang/Object;
.source "PrefBackupListView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefBackupListView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefBackupListView;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f070592

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$400(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$400(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$200(Lcom/sec/chaton/settings2/PrefBackupListView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$300(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->clearChoices()V

    .line 223
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefBackupListView;->mAdapter:Lcom/sec/chaton/localbackup/o;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/o;->notifyDataSetChanged()V

    .line 224
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$400(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    # invokes: Lcom/sec/chaton/settings2/PrefBackupListView;->setAlldata(Ljava/util/ArrayList;)Ljava/util/HashMap;
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$500(Lcom/sec/chaton/settings2/PrefBackupListView;Ljava/util/ArrayList;)Ljava/util/HashMap;

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # invokes: Lcom/sec/chaton/settings2/PrefBackupListView;->allcheck()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$600(Lcom/sec/chaton/settings2/PrefBackupListView;)V

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView$3;->this$0:Lcom/sec/chaton/settings2/PrefBackupListView;

    # getter for: Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->access$300(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
