.class Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;
.super Landroid/os/Handler;
.source "PrefFragmentConnectedDevice.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v4, 0x7f0b002a

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 181
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 183
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 186
    :sswitch_0
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$000(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$000(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 187
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$000(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 190
    :cond_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v2, v3, :cond_0

    .line 191
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;

    .line 193
    if-eqz v0, :cond_2

    iget-object v2, v0, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;->mapping:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-ne v2, v5, :cond_2

    .line 194
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    iget-object v3, v0, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;->MappingInfo:Ljava/util/ArrayList;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$102(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 195
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/BuddyMappingInfo;->url:Ljava/lang/String;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->webURL:Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$202(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;Ljava/lang/String;)Ljava/lang/String;

    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "webURL : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->webURL:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$200(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " mMappingInfo : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mMappingInfo:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$100(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceList:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$300(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceList:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$300(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v0, v1

    .line 201
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceAdapter:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$400(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 202
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceAdapter:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$400(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceList:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$300(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v1, v0, v6, v3}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$MultiDeviceAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 203
    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceList:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$300(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 205
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v1, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 207
    new-instance v3, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceList:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$300(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 208
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 209
    const v1, 0x7f030083

    invoke-virtual {v2, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 210
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->deviceList:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$300(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$500(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 222
    :sswitch_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_3

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mProfileControl:Lcom/sec/chaton/d/w;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$600(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Lcom/sec/chaton/d/w;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/w;->f(Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$500(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b029c

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 226
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$000(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$000(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$000(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 229
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;->access$500(Lcom/sec/chaton/settings2/PrefFragmentConnectedDevice;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 183
    :sswitch_data_0
    .sparse-switch
        0x19d -> :sswitch_0
        0x7d7 -> :sswitch_1
    .end sparse-switch
.end method
