.class public Lcom/sec/chaton/settings2/PrefBackupListView;
.super Landroid/app/Fragment;
.source "PrefBackupListView.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Lcom/sec/chaton/localbackup/b;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static BACKUP_FILE_NAME:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field mAdapter:Lcom/sec/chaton/localbackup/o;

.field mBackuplistData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/localbackup/m;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckedatas:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mEmptylist:Landroid/widget/TextView;

.field private mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

.field mListView:Landroid/widget/ListView;

.field private mSelectAll:Landroid/widget/CheckedTextView;

.field private mSwitchViewType:Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;

.field private mText:Landroid/widget/TextView;

.field private mbackuplistMenu:Landroid/view/Menu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-string v0, "backup_file_name"

    sput-object v0, Lcom/sec/chaton/settings2/PrefBackupListView;->BACKUP_FILE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefBackupListView;)Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mSwitchViewType:Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefBackupListView;Lcom/sec/chaton/localbackup/p;Z)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings2/PrefBackupListView;->setKeyInHashmap(Lcom/sec/chaton/localbackup/p;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefBackupListView;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/chaton/settings2/PrefBackupListView;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefBackupListView;Ljava/util/ArrayList;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings2/PrefBackupListView;->setAlldata(Ljava/util/ArrayList;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefBackupListView;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->allcheck()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/chaton/settings2/PrefBackupListView;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->handleExternalStorageState()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/chaton/settings2/PrefBackupListView;Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings2/PrefBackupListView;->switchView(Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;)V

    return-void
.end method

.method private allcheck()V
    .locals 3

    .prologue
    .line 293
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 294
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 293
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 296
    :cond_0
    return-void
.end method

.method private filterBackupFiles()V
    .locals 11

    .prologue
    const/16 v3, 0x8

    const/4 v0, 0x0

    .line 299
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/sec/chaton/localbackup/q;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 301
    if-eqz v1, :cond_2

    .line 302
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 303
    if-eqz v1, :cond_1

    .line 305
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 306
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mEmptylist:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 307
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 308
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".crypt"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 309
    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/chaton/localbackup/m;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/chaton/settings2/PrefBackupListView;->makeTimeFormat(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v6, v7, v8, v3}, Lcom/sec/chaton/localbackup/m;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 314
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 315
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mEmptylist:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 319
    :cond_2
    return-void
.end method

.method private handleExternalStorageState()V
    .locals 3

    .prologue
    .line 274
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 279
    :cond_0
    return-void
.end method

.method private makeTimeFormat(Ljava/lang/Long;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 322
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 323
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 324
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd hh:mm:ss a"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 325
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private refreshBackupList()V
    .locals 3

    .prologue
    .line 247
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    .line 248
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 250
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->filterBackupFiles()V

    .line 251
    new-instance v0, Lcom/sec/chaton/localbackup/o;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/localbackup/o;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mAdapter:Lcom/sec/chaton/localbackup/o;

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mAdapter:Lcom/sec/chaton/localbackup/o;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 254
    return-void
.end method

.method private setAlldata(Ljava/util/ArrayList;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/localbackup/m;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 511
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 512
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/m;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/m;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 513
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/m;

    iget-object v3, v0, Lcom/sec/chaton/localbackup/m;->a:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/m;

    iget-object v0, v0, Lcom/sec/chaton/localbackup/m;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 517
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    return-object v0
.end method

.method private setKeyInHashmap(Lcom/sec/chaton/localbackup/p;Z)V
    .locals 3

    .prologue
    .line 536
    if-eqz p2, :cond_1

    .line 537
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/localbackup/p;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/chaton/localbackup/p;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 539
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/localbackup/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/localbackup/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private startWatchingExternalStorage()V
    .locals 3

    .prologue
    .line 258
    new-instance v0, Lcom/sec/chaton/settings2/PrefBackupListView$4;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefBackupListView$4;-><init>(Lcom/sec/chaton/settings2/PrefBackupListView;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    .line 265
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 266
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 267
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 268
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 269
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 270
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->handleExternalStorageState()V

    .line 271
    return-void
.end method

.method private stopWatchingExternalStorage()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 290
    return-void
.end method

.method private switchView(Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;)V
    .locals 7

    .prologue
    const v6, 0x7f070590

    const v5, 0x7f07058e

    const v4, 0x7f070592

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 456
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mSwitchViewType:Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;

    .line 458
    sget-object v0, Lcom/sec/chaton/settings2/PrefBackupListView$6;->$SwitchMap$com$sec$chaton$settings2$PrefBackupListView$SwitchViewType:[I

    invoke-virtual {p1}, Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 507
    :goto_0
    return-void

    .line 460
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 461
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    invoke-interface {v0, v5, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    invoke-interface {v0, v6, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 464
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 465
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 472
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->clearChoices()V

    .line 476
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_2

    .line 477
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 480
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    if-eqz v0, :cond_3

    .line 481
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 484
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 486
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mAdapter:Lcom/sec/chaton/localbackup/o;

    iput-boolean v2, v0, Lcom/sec/chaton/localbackup/o;->a:Z

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_0

    .line 467
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 492
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 493
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    if-eqz v0, :cond_5

    .line 494
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    invoke-interface {v0, v5, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 495
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    invoke-interface {v0, v6, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 498
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 500
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/settings2/SettingActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/SettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 502
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 503
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mAdapter:Lcom/sec/chaton/localbackup/o;

    iput-boolean v3, v0, Lcom/sec/chaton/localbackup/o;->a:Z

    .line 504
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    goto/16 :goto_0

    .line 458
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public DeleteFromPreview(Ljava/util/HashMap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const v7, 0x7f0b0167

    const/4 v6, 0x0

    .line 408
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 411
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 414
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/localbackup/m;

    .line 415
    iget-object v4, v1, Lcom/sec/chaton/localbackup/m;->a:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 416
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 419
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, v1, Lcom/sec/chaton/localbackup/m;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 421
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v7, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 431
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/ActivityBackupList;

    :goto_1
    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->finish()V

    goto :goto_0

    .line 423
    :catch_0
    move-exception v0

    .line 425
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 430
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v7, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 431
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/ActivityBackupList;

    goto :goto_1

    .line 430
    :catchall_0
    move-exception v0

    move-object v1, v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v7, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 431
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/ActivityBackupList;

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/ActivityBackupList;->finish()V

    .line 430
    throw v1

    .line 440
    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mActivity:Landroid/app/Activity;

    .line 85
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->setHasOptionsMenu(Z)V

    .line 86
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 87
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const v2, 0x7f07058f

    .line 331
    const v0, 0x7f0f0011

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 333
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    .line 335
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    const v1, 0x7f07058e

    invoke-interface {v0, v1, v4}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    const v1, 0x7f070590

    invoke-interface {v0, v1, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 344
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 347
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 348
    return-void

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 93
    const v0, 0x7f030067

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 96
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 98
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v1, v3, :cond_1

    .line 99
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 105
    :goto_0
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 106
    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 107
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 108
    instance-of v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 109
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move-object v0, v1

    .line 110
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 111
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_0

    move-object v0, v1

    .line 112
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 113
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 118
    :cond_0
    const v0, 0x7f070416

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 119
    const v0, 0x7f07050b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 120
    const v3, 0x7f0b01e8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 121
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setClickable(Z)V

    .line 122
    new-instance v3, Lcom/sec/chaton/settings2/PrefBackupListView$1;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings2/PrefBackupListView$1;-><init>(Lcom/sec/chaton/settings2/PrefBackupListView;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    const v0, 0x7f07050d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 129
    const v3, 0x7f0b0332

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 130
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 131
    const v0, 0x7f07050e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 132
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 134
    const v0, 0x7f070296

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    .line 136
    const v0, 0x7f070294

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;

    .line 138
    const v0, 0x7f070297

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mEmptylist:Landroid/widget/TextView;

    .line 140
    const v0, 0x7f070295

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 141
    const v0, 0x7f07014c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 142
    sget-object v3, Lcom/sec/chaton/localbackup/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    invoke-virtual {v1, v5}, Landroid/view/View;->setClickable(Z)V

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    .line 147
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 153
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->filterBackupFiles()V

    .line 155
    new-instance v0, Lcom/sec/chaton/localbackup/o;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/localbackup/o;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mAdapter:Lcom/sec/chaton/localbackup/o;

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mAdapter:Lcom/sec/chaton/localbackup/o;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/chaton/settings2/PrefBackupListView$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefBackupListView$2;-><init>(Lcom/sec/chaton/settings2/PrefBackupListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mSelectAll:Landroid/widget/CheckedTextView;

    new-instance v1, Lcom/sec/chaton/settings2/PrefBackupListView$3;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefBackupListView$3;-><init>(Lcom/sec/chaton/settings2/PrefBackupListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    invoke-virtual {p0, v6}, Lcom/sec/chaton/settings2/PrefBackupListView;->setHasOptionsMenu(Z)V

    .line 228
    sget-object v0, Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;->NORMAL:Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->switchView(Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;)V

    .line 230
    return-object v2

    .line 102
    :cond_1
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mBackuplistData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 529
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mCheckedatas:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 532
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 533
    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings2/PrefBackupListView;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 370
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 402
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 375
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/localbackup/ActivityBackupList;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 376
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 381
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;->NORMAL:Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->switchView(Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;)V

    goto :goto_0

    .line 385
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0335

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/settings2/PrefBackupListView$5;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefBackupListView$5;-><init>(Lcom/sec/chaton/settings2/PrefBackupListView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 370
    :pswitch_data_0
    .packed-switch 0x7f07058f
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 284
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 285
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->stopWatchingExternalStorage()V

    .line 286
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 352
    invoke-super {p0, p1}, Landroid/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 354
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    const v1, 0x7f07058f

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 355
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    const v1, 0x7f070591

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mbackuplistMenu:Landroid/view/Menu;

    const v1, 0x7f070592

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 358
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->startWatchingExternalStorage()V

    .line 238
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefBackupListView;->refreshBackupList()V

    .line 239
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 240
    return-void
.end method

.method public switchViewMode()Z
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefBackupListView;->mSwitchViewType:Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;

    sget-object v1, Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;->DELETE:Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;

    if-ne v0, v1, :cond_0

    .line 549
    sget-object v0, Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;->NORMAL:Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;

    invoke-direct {p0, v0}, Lcom/sec/chaton/settings2/PrefBackupListView;->switchView(Lcom/sec/chaton/settings2/PrefBackupListView$SwitchViewType;)V

    .line 550
    const/4 v0, 0x1

    .line 552
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
