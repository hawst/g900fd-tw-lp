.class public Lcom/sec/chaton/settings2/PrefFragmentPrivacy;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentPrivacy.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final PROFILE_IMAGE_BUDDIES:I = 0x0

.field public static final PROFILE_IMAGE_PUBLIC:I = 0x1

.field private static final REQUEST_LOCK_SET:I = 0x1

.field protected static final SET_RINGTONE:I = 0x2


# instance fields
.field private INDEX_LOCKSTATE:I

.field private TAG:Ljava/lang/String;

.field private control:Lcom/sec/chaton/d/h;

.field private ctx:Landroid/content/Context;

.field private mAlertDialog:Lcom/sec/common/a/d;

.field private mGuide:Ljava/lang/String;

.field private mGuideImageAll1:Ljava/lang/String;

.field private mGuideImageAll2:Ljava/lang/String;

.field private mGuideImageBuddy1:Ljava/lang/String;

.field private mGuideImageBuddy2:Ljava/lang/String;

.field private mGuideOff:Ljava/lang/String;

.field private mGuidePhoneNumber:Ljava/lang/String;

.field private mGuideSamsungAccountOff:Ljava/lang/String;

.field private mGuideSamsungAccountOn:Ljava/lang/String;

.field private mGuideSamsungAccountTitle:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mMSISDN:Ljava/lang/String;

.field private mNetworkError:Ljava/lang/Boolean;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSamsungAccount:Ljava/lang/String;

.field private passwordData:[Ljava/lang/String;

.field private phone_number_privacy:Landroid/preference/CheckBoxPreference;

.field private privacyArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/PrivacyList;",
            ">;"
        }
    .end annotation
.end field

.field private samsung_account_privacy:Landroid/preference/CheckBoxPreference;

.field private settings_birthday:Landroid/preference/Preference;

.field private settings_password_lock:Landroid/preference/Preference;

.field private sharedPref:Lcom/sec/chaton/util/ab;

.field private showProfileImageNum:I

.field private show_profileimage:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 58
    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 65
    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 80
    const-class v0, Lcom/sec/chaton/settings/ActivityPrivacy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->TAG:Ljava/lang/String;

    .line 82
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->passwordData:[Ljava/lang/String;

    .line 83
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->INDEX_LOCKSTATE:I

    .line 87
    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mAlertDialog:Lcom/sec/common/a/d;

    .line 535
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$8;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private InitPrivacyCheck(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const v6, 0x7f08003f

    const v5, 0x7f08001b

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 469
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 503
    :cond_0
    :goto_0
    return-void

    .line 472
    :cond_1
    const-string v0, "phonenumber"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isEnabled()Z

    move-result v0

    if-ne v0, v3, :cond_3

    .line 473
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 474
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "show_phone_number_to_all"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 475
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 480
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitPrivacyCheck/ phonenumber : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 477
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "show_phone_number_to_all"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 478
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    .line 481
    :cond_3
    const-string v0, "showprofileimage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 482
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 483
    iput v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->showProfileImageNum:I

    .line 484
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_profile_image_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 485
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageAll1:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageAll2:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->show_profileimage:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    .line 491
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitPrivacyCheck/ showprofileimage : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 487
    :cond_4
    iput v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->showProfileImageNum:I

    .line 488
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_profile_image_show"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 489
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageBuddy1:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageBuddy2:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->show_profileimage:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto :goto_2

    .line 492
    :cond_5
    const-string v0, "emailsamsung"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isEnabled()Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 493
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 494
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 495
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_show"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 500
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitPrivacyCheck/ emailsamsung : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 497
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 498
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_show"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_3
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Lcom/sec/chaton/d/h;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->control:Lcom/sec/chaton/d/h;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->privacyArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->privacyArray:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->InitPrivacyCheck(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mNetworkError:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mNetworkError:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mAlertDialog:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mAlertDialog:Lcom/sec/common/a/d;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->showProfileImageNum:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->passwordData:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->INDEX_LOCKSTATE:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setPrivacyGuide()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private initializeForPreference()V
    .locals 11

    .prologue
    const v10, 0x7f080015

    const v9, 0x7f08001b

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v6, 0x7f08003f

    .line 201
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 203
    const-string v0, "pref_item_phonenumber"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 206
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "show_phone_number_to_all"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 213
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "show_phone_number_to_all"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-ne v0, v8, :cond_5

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    const-string v1, ""

    if-ne v0, v1, :cond_4

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuide:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 230
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 250
    const-string v0, "pref_item_show_profileimage"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->show_profileimage:Landroid/preference/Preference;

    .line 252
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_profile_image_show"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v8, :cond_8

    .line 253
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageAll1:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageAll2:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->show_profileimage:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    .line 258
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->show_profileimage:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 291
    const-string v0, "pref_item_samsung_account"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    .line 293
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mSamsungAccount:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mSamsungAccount:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    .line 294
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_show"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 295
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 296
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 301
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "samsung_account_show"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-ne v0, v8, :cond_a

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountOn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountOn:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 312
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$4;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$4;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 327
    const-string v0, "pref_item_birthday"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_birthday:Landroid/preference/Preference;

    .line 328
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setBirthday()V

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_birthday:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$5;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$5;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 339
    const-string v0, "pref_item_poston"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 340
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0183

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 341
    const v1, 0x7f0b0183

    invoke-virtual {p0, v1}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 342
    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$6;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$6;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_password_lock:Landroid/preference/Preference;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$7;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$7;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 364
    return-void

    .line 210
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v8}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_0

    .line 219
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuide:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto/16 :goto_1

    .line 222
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    const-string v1, ""

    if-ne v0, v1, :cond_7

    .line 223
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuidePhoneNumber:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideOff:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_1

    .line 226
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideOff:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto/16 :goto_1

    .line 255
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageBuddy1:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageBuddy2:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->show_profileimage:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto/16 :goto_2

    .line 298
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v8}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_3

    .line 307
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountTitle:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 308
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountOff:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountOff:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_4
.end method

.method private setBirthday()V
    .locals 9

    .prologue
    const v8, 0x7f0b008f

    const v7, 0x7f08003f

    const/4 v6, 0x2

    const/4 v5, 0x1

    const v4, 0x7f08001b

    .line 507
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "birthday"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 508
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "birthday_type"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 509
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 510
    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 511
    const-string v0, "SHORT_HIDE"

    .line 513
    :cond_0
    const-string v2, "FULL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 514
    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_birthday:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 532
    :goto_0
    return-void

    .line 515
    :cond_1
    const-string v2, "SHORT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 516
    const-string v0, "-"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 517
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, v0, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v0, v0, v6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 518
    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_birthday:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_0

    .line 519
    :cond_2
    const-string v2, "FULL_HIDE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 520
    invoke-static {v1}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_birthday:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_0

    .line 521
    :cond_3
    const-string v2, "SHORT_HIDE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 522
    const-string v0, "-"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 523
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, v0, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v0, v0, v6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 524
    invoke-static {v0}, Lcom/sec/chaton/userprofile/MyInfoFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_birthday:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_0

    .line 526
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_birthday:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_0

    .line 530
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_birthday:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_0
.end method

.method private setPrivacyGuide()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const v7, 0x7f08001b

    const v6, 0x7f08003f

    .line 436
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 437
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    const-string v1, ""

    if-ne v0, v1, :cond_1

    .line 438
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "show_phone_number_to_all"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 439
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuide:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 450
    :goto_0
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_profile_image_show"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v9, :cond_5

    .line 451
    iput v9, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->showProfileImageNum:I

    .line 452
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageAll1:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageAll2:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->show_profileimage:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    .line 458
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountOn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 460
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountOn:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 466
    :goto_2
    return-void

    .line 441
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuide:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto :goto_0

    .line 444
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    const-string v1, ""

    if-ne v0, v1, :cond_4

    .line 445
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideOff:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_0

    .line 447
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideOff:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->phone_number_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto/16 :goto_0

    .line 455
    :cond_5
    iput v8, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->showProfileImageNum:I

    .line 456
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageBuddy1:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageBuddy2:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->show_profileimage:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V

    goto/16 :goto_1

    .line 462
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountOff:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 463
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountOff:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->samsung_account_privacy:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto/16 :goto_2
.end method

.method private setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 367
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 368
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 369
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 370
    return-void
.end method

.method private setTextWitTwoColor(Ljava/lang/String;Ljava/lang/String;Landroid/preference/Preference;II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 373
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 374
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 375
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, p4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-interface {v0, v2, v4, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 376
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, p5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    invoke-interface {v1, v2, v4, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 377
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v4

    const/4 v0, 0x1

    const-string v3, "\n"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    aput-object v1, v2, v0

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 378
    return-void
.end method

.method private setTitleTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 382
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 383
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 384
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 385
    return-void
.end method


# virtual methods
.method public changeSummary(Ljava/lang/String;)V
    .locals 5

    .prologue
    const v4, 0x7f0b01c4

    const v2, 0x7f0b01c3

    const v3, 0x7f08001b

    .line 424
    const-string v0, "changeSummary"

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    invoke-static {}, Lcom/sec/chaton/util/p;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_password_lock:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSummary(I)V

    .line 427
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_password_lock:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 432
    :goto_0
    return-void

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_password_lock:Landroid/preference/Preference;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(I)V

    .line 430
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_password_lock:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 178
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 179
    packed-switch p1, :pswitch_data_0

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 181
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    const-class v1, Lcom/sec/chaton/settings2/PrefFragmentPasswordLock;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0b01c5

    const/4 v6, 0x0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    goto :goto_0

    .line 179
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 97
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 99
    const v0, 0x7f050010

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->addPreferencesFromResource(I)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Z)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b00b6

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$1;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 121
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mNetworkError:Ljava/lang/Boolean;

    .line 122
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->control:Lcom/sec/chaton/d/h;

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->control:Lcom/sec/chaton/d/h;

    const-string v1, "phonenumber|showprofileimage|emailsamsung"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->g(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->privacyArray:Ljava/util/ArrayList;

    .line 132
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 134
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mSamsungAccount:Ljava/lang/String;

    .line 136
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mMSISDN:Ljava/lang/String;

    .line 137
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b018f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuide:Ljava/lang/String;

    .line 138
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0190

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideOff:Ljava/lang/String;

    .line 139
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0147

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageBuddy1:Ljava/lang/String;

    .line 140
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0284

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageBuddy2:Ljava/lang/String;

    .line 141
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0256

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageAll1:Ljava/lang/String;

    .line 142
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0283

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideImageAll2:Ljava/lang/String;

    .line 143
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0275

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountOn:Ljava/lang/String;

    .line 144
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0285

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountOff:Ljava/lang/String;

    .line 145
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b018d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuidePhoneNumber:Ljava/lang/String;

    .line 146
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0274

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mGuideSamsungAccountTitle:Ljava/lang/String;

    .line 148
    const-string v0, "pref_item_password_lock"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->settings_password_lock:Landroid/preference/Preference;

    .line 151
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->initializeForPreference()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    const-string v1, "fragment_choose"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    const-string v1, "fragment_choose"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    const-class v1, Lcom/sec/chaton/settings2/PrefFragmentBirthday;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    const-class v1, Lcom/sec/chaton/settings2/PrefFragmentBirthday;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0b0017

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    .line 165
    :cond_0
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 153
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 193
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroy()V

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mAlertDialog:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mAlertDialog:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 197
    :cond_0
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 169
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 170
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->setBirthday()V

    .line 171
    const-string v0, "GET"

    const-string v1, ""

    const-string v2, ""

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->prePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->passwordData:[Ljava/lang/String;

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->passwordData:[Ljava/lang/String;

    iget v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->INDEX_LOCKSTATE:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->changeSummary(Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method public prePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x0

    .line 388
    const-string v1, "prePassword"

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->TAG:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->TAG:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    new-array v2, v6, [Ljava/lang/String;

    move v1, v0

    .line 392
    :goto_0
    if-ge v1, v6, :cond_0

    .line 393
    const-string v3, ""

    aput-object v3, v2, v1

    .line 392
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 396
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;

    const-string v3, "PASSWORD_LOCK"

    invoke-virtual {v1, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 398
    const-string v3, "GET"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 399
    const-string v3, "GET preference"

    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->TAG:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const-string v3, "LOCK_STATE"

    invoke-static {}, Lcom/sec/chaton/util/p;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 402
    const/4 v3, 0x1

    const-string v4, "PASSWORD"

    const-string v5, "0000"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    .line 404
    :goto_1
    if-ge v0, v6, :cond_2

    .line 405
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "data : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->TAG:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 409
    :cond_1
    const-string v0, "SET preference"

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->TAG:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "state : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->TAG:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pass : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->TAG:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 414
    invoke-static {p2}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 415
    invoke-static {p3}, Lcom/sec/chaton/util/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 416
    const-string v4, "LOCK_STATE"

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 417
    const-string v1, "PASSWORD"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 418
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 420
    :cond_2
    return-object v2
.end method
