.class Lcom/sec/chaton/settings2/PrefFragmentChats$4;
.super Ljava/lang/Object;
.source "PrefFragmentChats.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentChats;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    .prologue
    const v3, 0x7f0b0318

    const/4 v4, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 297
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$4;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->access$400(Lcom/sec/chaton/settings2/PrefFragmentChats;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    .line 298
    const v0, 0x7f0b000e

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    .line 300
    const-string v0, ""

    .line 301
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ar"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v0, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 303
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 304
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteOldChatroom country["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 312
    invoke-virtual {v1, v5}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/settings2/PrefFragmentChats$4$2;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings2/PrefFragmentChats$4$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentChats$4;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b0039

    new-instance v3, Lcom/sec/chaton/settings2/PrefFragmentChats$4$1;

    invoke-direct {v3, p0}, Lcom/sec/chaton/settings2/PrefFragmentChats$4$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentChats$4;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 324
    invoke-virtual {v1}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 325
    return v5

    .line 307
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
