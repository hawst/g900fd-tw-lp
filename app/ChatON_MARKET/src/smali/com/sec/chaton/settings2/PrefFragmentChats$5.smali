.class Lcom/sec/chaton/settings2/PrefFragmentChats$5;
.super Ljava/lang/Object;
.source "PrefFragmentChats.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentChats;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 363
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentChats;->mDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->access$600(Lcom/sec/chaton/settings2/PrefFragmentChats;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 366
    packed-switch p3, :pswitch_data_0

    .line 397
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 398
    const-string v0, "Delete old failed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_0
    :goto_0
    return-void

    .line 369
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->access$400(Lcom/sec/chaton/settings2/PrefFragmentChats;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0319

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 370
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 371
    const-string v0, "Delete old Chat Rooms"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 379
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->access$400(Lcom/sec/chaton/settings2/PrefFragmentChats;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b031b

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 380
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 381
    const-string v0, "No old Chatroom"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 388
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$5;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->access$400(Lcom/sec/chaton/settings2/PrefFragmentChats;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b031a

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 389
    sget-boolean v0, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v0, :cond_0

    .line 390
    const-string v0, "No old Chatroom"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 366
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 352
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 346
    return-void
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 358
    return-void
.end method
