.class public Lcom/sec/chaton/settings2/BreadCrumbsPreference;
.super Landroid/preference/Preference;
.source "BreadCrumbsPreference.java"


# instance fields
.field private breadcrumb1:Ljava/lang/String;

.field private breadcrumb2:Ljava/lang/String;

.field private breadcrumb3:Ljava/lang/String;

.field mAttachedActivity:Landroid/app/Activity;

.field private mBreadCrumb1:Landroid/widget/TextView;

.field private mBreadCrumb2:Landroid/widget/TextView;

.field private mBreadCrumb3:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private final mListener:Lcom/sec/chaton/settings2/BreadCrumbsPreference$Listener;

.field private mSeparator1:Landroid/widget/ImageView;

.field private mSeparator2:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/settings2/BreadCrumbsPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/chaton/settings2/BreadCrumbsPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    new-instance v0, Lcom/sec/chaton/settings2/BreadCrumbsPreference$Listener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/settings2/BreadCrumbsPreference$Listener;-><init>(Lcom/sec/chaton/settings2/BreadCrumbsPreference;Lcom/sec/chaton/settings2/BreadCrumbsPreference$1;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mListener:Lcom/sec/chaton/settings2/BreadCrumbsPreference$Listener;

    .line 35
    iput-object p1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mContext:Landroid/content/Context;

    .line 36
    sget-object v0, Lcom/sec/chaton/am;->BreadCrumbsPreference:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 37
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->breadcrumb1:Ljava/lang/String;

    .line 38
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->breadcrumb2:Ljava/lang/String;

    .line 39
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->breadcrumb3:Ljava/lang/String;

    .line 40
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 41
    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f08001b

    const/16 v2, 0x8

    .line 66
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 68
    const v0, 0x7f07050b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb1:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f07050d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb2:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f07050f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb3:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f07050c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mSeparator1:Landroid/widget/ImageView;

    .line 73
    const v0, 0x7f07050e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mSeparator2:Landroid/widget/ImageView;

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->breadcrumb3:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mSeparator2:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb3:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->breadcrumb2:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->breadcrumb1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mSeparator1:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb2:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    :cond_0
    :goto_1
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb3:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb3:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->breadcrumb3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 89
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->breadcrumb1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isClickable()Z

    move-result v0

    if-nez v0, :cond_3

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb1:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 93
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mListener:Lcom/sec/chaton/settings2/BreadCrumbsPreference$Listener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->breadcrumb2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->breadcrumb3:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mBreadCrumb2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 58
    const v1, 0x7f03013d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/chaton/settings2/BreadCrumbsPreference;->mAttachedActivity:Landroid/app/Activity;

    .line 53
    return-void
.end method
