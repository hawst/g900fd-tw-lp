.class public Lcom/sec/chaton/settings2/PrefFragmentChats;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentChats.java"


# static fields
.field public static final ALL_CHAT:Ljava/lang/String; = "all_chat"

.field public static final BACKGROUND_COLOURS_BLACK:I = 0x1

.field public static final BACKGROUND_COLOURS_DEFAULT:I = 0x0

.field public static final CHANGE_BACKGROUND:Ljava/lang/String; = "change_background"

.field public static final CHANGE_BUBBLE:Ljava/lang/String; = "change_bubble"

.field public static final CHANGE_MODE:Ljava/lang/String; = "change_mode"

.field private static final CONTROL_CODE:I = 0x1b58

.field private static final DELETE_OLD_MSGBOX:I = 0x1

.field public static final DELETE_OLD_MSGBOX_FAILED:I = -0x1

.field public static final DELETE_OLD_MSGBOX_NO_CHATROOM:I = 0x2

.field public static final DELETE_OLD_MSGBOX_NO_OLD_CHATROOM:I = 0x1

.field public static final DELETE_OLD_MSGBOX_SUCCESS:I = 0x0

.field public static final METHOD_SET_TYPING_STATUS:I = 0x1b59

.field public static final ONE_CHAT:Ljava/lang/String; = "one_chat"

.field protected static final SET_RINGTONE:I = 0x2

.field public static final TYPE:Ljava/lang/String; = "type"


# instance fields
.field private ctx:Landroid/content/Context;

.field private mActivity:Landroid/app/Activity;

.field private mBackKeyIgnore:Z

.field private mDialog:Landroid/app/ProgressDialog;

.field private mIsEnterKey:Z

.field private mPrefItemAutoResend:Landroid/preference/CheckBoxPreference;

.field private mQueryHandler:Lcom/sec/chaton/e/a/u;

.field mQueryListener:Lcom/sec/chaton/e/a/v;

.field private mTypingStatus:Z

.field prefItemAbout:Lcom/sec/chaton/settings/AboutNewNotice;

.field private prefItemChangeBubbleBackground:Landroid/preference/Preference;

.field private prefItemFontSize:Landroid/preference/Preference;

.field private prefItemFontStyle:Landroid/preference/Preference;

.field private prefItemTypingStatus:Landroid/preference/CheckBoxPreference;

.field private sharedPref:Lcom/sec/chaton/util/ab;

.field tag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 52
    const-string v0, "Settings"

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->tag:Ljava/lang/String;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mBackKeyIgnore:Z

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mTypingStatus:Z

    .line 340
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentChats$5;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentChats$5;-><init>(Lcom/sec/chaton/settings2/PrefFragmentChats;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mQueryListener:Lcom/sec/chaton/e/a/v;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/chaton/settings2/PrefFragmentChats;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mIsEnterKey:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentChats;)Lcom/sec/chaton/util/ab;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentChats;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mPrefItemAutoResend:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/chaton/settings2/PrefFragmentChats;Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/settings2/PrefFragmentChats;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/chaton/settings2/PrefFragmentChats;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/chaton/settings2/PrefFragmentChats;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->deleteOldChatRooms()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/chaton/settings2/PrefFragmentChats;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private deleteOldChatRooms()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 331
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mDialog:Landroid/app/ProgressDialog;

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mDialog:Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;

    const v4, 0x7f0b0313

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mDialog:Landroid/app/ProgressDialog;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 334
    iput-boolean v1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mBackKeyIgnore:Z

    .line 335
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mQueryHandler:Lcom/sec/chaton/e/a/u;

    invoke-static {}, Lcom/sec/chaton/e/g;->a()Landroid/net/Uri;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/e/a/u;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 338
    return-void
.end method

.method private getFontName()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 410
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 412
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Default Font Typeface"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 414
    if-eq v1, v4, :cond_0

    .line 416
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Default Font Name"

    invoke-virtual {v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 417
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Default Font Name"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 426
    :cond_0
    :goto_0
    return-object v0

    .line 419
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/chaton/e/a/k;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 420
    if-eqz v1, :cond_2

    .line 421
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 423
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Default Font Name"

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getFontSize()Ljava/lang/String;
    .locals 4

    .prologue
    const v3, 0x7f0b017c

    .line 430
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Default Font Size"

    sget-object v2, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v2}, Lcom/sec/chaton/settings/dk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 432
    invoke-static {v0}, Lcom/sec/chaton/settings/dk;->a(Ljava/lang/String;)Lcom/sec/chaton/settings/dk;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v0

    .line 435
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 437
    sget-object v1, Lcom/sec/chaton/settings/dk;->a:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 438
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 450
    :goto_0
    return-object v0

    .line 439
    :cond_0
    sget-object v1, Lcom/sec/chaton/settings/dk;->b:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 440
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 441
    :cond_1
    sget-object v1, Lcom/sec/chaton/settings/dk;->c:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 442
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 443
    :cond_2
    sget-object v1, Lcom/sec/chaton/settings/dk;->d:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 444
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 445
    :cond_3
    sget-object v1, Lcom/sec/chaton/settings/dk;->e:Lcom/sec/chaton/settings/dk;

    invoke-virtual {v1}, Lcom/sec/chaton/settings/dk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 446
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 448
    :cond_4
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private initializeForPreference()V
    .locals 9

    .prologue
    const v8, 0x7f08001b

    const/4 v7, 0x0

    const v6, 0x7f08003f

    const/4 v5, 0x1

    .line 179
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 181
    const-string v0, "pref_item_change_bubble_background_style"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->prefItemChangeBubbleBackground:Landroid/preference/Preference;

    .line 185
    const-string v0, "pref_item_font_style"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->prefItemFontStyle:Landroid/preference/Preference;

    .line 187
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getFontName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->prefItemFontStyle:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentChats;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 193
    const-string v0, "pref_item_font_size"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->prefItemFontSize:Landroid/preference/Preference;

    .line 195
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getFontSize()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->prefItemFontSize:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentChats;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 199
    const-string v0, "pref_item_enter_key"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 201
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v3, "Setting enter key"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-ne v2, v5, :cond_0

    .line 202
    iput-boolean v5, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mIsEnterKey:Z

    .line 203
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v3, "Setting enter key"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 208
    :goto_0
    iget-boolean v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mIsEnterKey:Z

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 209
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01e1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v2, v0, v3}, Lcom/sec/chaton/settings2/PrefFragmentChats;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 211
    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentChats$1;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentChats$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentChats;)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 235
    const-string v0, "pref_item_push_to_talk_key"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 236
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v3, "Setting push to talk"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 237
    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v4, "Setting push to talk"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 238
    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 239
    const v2, 0x7f0b0212

    invoke-virtual {p0, v2}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v2, v0, v3}, Lcom/sec/chaton/settings2/PrefFragmentChats;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 241
    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentChats$2;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentChats$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentChats;)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 264
    const-string v0, "pref_item_auto_resend"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mPrefItemAutoResend:Landroid/preference/CheckBoxPreference;

    .line 265
    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    .line 266
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mPrefItemAutoResend:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 267
    if-eqz v0, :cond_1

    .line 268
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b03c0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mPrefItemAutoResend:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentChats;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 273
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mPrefItemAutoResend:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentChats$3;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentChats$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentChats;)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 293
    const-string v0, "pref_item_delete_old_chat_rooms"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 294
    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentChats$4;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentChats$4;-><init>(Lcom/sec/chaton/settings2/PrefFragmentChats;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 328
    return-void

    .line 205
    :cond_0
    iput-boolean v7, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mIsEnterKey:Z

    .line 206
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v3, "Setting enter key"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 270
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b03c1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mPrefItemAutoResend:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentChats;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_1
.end method

.method private setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 173
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 174
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 175
    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 176
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mActivity:Landroid/app/Activity;

    .line 91
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 92
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 102
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LIFE] onCreate, isTaskRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Task ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Memory Address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mActivity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;

    .line 109
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mQueryListener:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mQueryHandler:Lcom/sec/chaton/e/a/u;

    .line 117
    const v0, 0x7f050006

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->addPreferencesFromResource(I)V

    .line 119
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;

    const-string v1, "Lock Check"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 129
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->initializeForPreference()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 131
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->mActivity:Landroid/app/Activity;

    .line 97
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDetach()V

    .line 98
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 149
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->prefItemChangeBubbleBackground:Landroid/preference/Preference;

    if-ne p2, v0, :cond_0

    .line 153
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/settings/ActivityBgBubbleChange;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 154
    const-string v1, "type"

    const-string v2, "all_chat"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->startActivity(Landroid/content/Intent;)V

    .line 156
    const/4 v0, 0x0

    .line 169
    :goto_0
    return v0

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->prefItemFontStyle:Landroid/preference/Preference;

    if-ne p2, v0, :cond_1

    .line 159
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;

    const-class v3, Lcom/sec/chaton/settings/ActivityFontChange;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 160
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 161
    goto :goto_0

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->prefItemFontSize:Landroid/preference/Preference;

    if-ne p2, v0, :cond_2

    .line 164
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->ctx:Landroid/content/Context;

    const-class v3, Lcom/sec/chaton/settings/ActivityChatView;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165
    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 167
    goto :goto_0

    :cond_2
    move v0, v1

    .line 169
    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const v3, 0x7f08003f

    .line 140
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 142
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getFontName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->prefItemFontStyle:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentChats;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 143
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getFontSize()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats;->prefItemFontSize:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentChats;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 144
    return-void
.end method
