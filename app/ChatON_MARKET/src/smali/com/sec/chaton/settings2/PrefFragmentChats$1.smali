.class Lcom/sec/chaton/settings2/PrefFragmentChats$1;
.super Ljava/lang/Object;
.source "PrefFragmentChats.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentChats;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/high16 v4, 0x7f0e0000

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 217
    if-eqz p2, :cond_1

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentChats;->mIsEnterKey:Z
    invoke-static {v0, v3}, Lcom/sec/chaton/settings2/PrefFragmentChats;->access$002(Lcom/sec/chaton/settings2/PrefFragmentChats;Z)Z

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->access$100(Lcom/sec/chaton/settings2/PrefFragmentChats;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting enter key"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 220
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 221
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100013"

    const-string v2, "00000001"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_0
    :goto_0
    return v3

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentChats;->mIsEnterKey:Z
    invoke-static {v0, v2}, Lcom/sec/chaton/settings2/PrefFragmentChats;->access$002(Lcom/sec/chaton/settings2/PrefFragmentChats;Z)Z

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentChats$1;->this$0:Lcom/sec/chaton/settings2/PrefFragmentChats;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentChats;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentChats;->access$100(Lcom/sec/chaton/settings2/PrefFragmentChats;)Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Setting enter key"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 226
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 227
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100013"

    const-string v2, "00000002"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
