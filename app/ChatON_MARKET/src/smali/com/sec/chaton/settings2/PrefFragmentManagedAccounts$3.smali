.class Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;
.super Ljava/lang/Object;
.source "PrefFragmentManagedAccounts.java"

# interfaces
.implements Lcom/sec/chaton/settings/tellfriends/ag;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V
    .locals 0

    .prologue
    .line 619
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelled()V
    .locals 2

    .prologue
    .line 635
    const-string v0, "onCancelled() \t- Logout"

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$200(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->dismissProgressDialog()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$300(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V

    .line 637
    return-void
.end method

.method public onComplete()V
    .locals 3

    .prologue
    .line 622
    const-string v0, "onComplete() \t- Logout"

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$200(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 624
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 625
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->dismissProgressDialog()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$300(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V

    .line 627
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$400(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/settings/tellfriends/y;->a(Landroid/content/Context;)Lcom/sec/chaton/settings/tellfriends/y;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/tellfriends/y;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/settings/tellfriends/y;->a(Ljava/lang/String;Z)V

    .line 628
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$500(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Lcom/sec/chaton/settings/SnsSignInOutPreference;

    move-result-object v0

    const v1, 0x7f0b01f2

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->setTitle(I)V

    .line 629
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$500(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Lcom/sec/chaton/settings/SnsSignInOutPreference;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 630
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->mPrefItemFacebook:Lcom/sec/chaton/settings/SnsSignInOutPreference;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$500(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Lcom/sec/chaton/settings/SnsSignInOutPreference;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/settings/fe;->b:Lcom/sec/chaton/settings/fe;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/settings/SnsSignInOutPreference;->a(Lcom/sec/chaton/settings/fe;)V

    .line 631
    return-void
.end method

.method public onError()V
    .locals 2

    .prologue
    .line 641
    const-string v0, "onError() \t- Logout"

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$200(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->dismissProgressDialog()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$300(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V

    .line 643
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->showAlertDialog()V
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;->access$600(Lcom/sec/chaton/settings2/PrefFragmentManagedAccounts;)V

    .line 644
    return-void
.end method
