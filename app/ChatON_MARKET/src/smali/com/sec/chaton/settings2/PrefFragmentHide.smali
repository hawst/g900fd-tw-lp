.class public Lcom/sec/chaton/settings2/PrefFragmentHide;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentHide.java"


# instance fields
.field private impl:Lcom/sec/chaton/hide/g;

.field private menuListener:Landroid/view/MenuItem$OnMenuItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 26
    new-instance v0, Lcom/sec/chaton/hide/g;

    invoke-direct {v0}, Lcom/sec/chaton/hide/g;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    .line 100
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentHide$1;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentHide$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentHide;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->menuListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentHide;)Lcom/sec/chaton/hide/g;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    return-object v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/hide/g;->a(IILandroid/content/Intent;)V

    .line 97
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 31
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/hide/g;->a(Landroid/app/Activity;)V

    .line 32
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 43
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/hide/g;->a(Landroid/os/Bundle;)V

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/settings2/PrefFragmentHide;->setHasOptionsMenu(Z)V

    .line 47
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 53
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->menuListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/hide/g;->a(Landroid/view/MenuItem$OnMenuItemClickListener;)V

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/hide/g;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 56
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/hide/g;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroy()V

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/g;->d()V

    .line 89
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDetach()V

    .line 37
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/g;->b()V

    .line 38
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 61
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/hide/g;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentHide;->impl:Lcom/sec/chaton/hide/g;

    invoke-virtual {v0}, Lcom/sec/chaton/hide/g;->c()V

    .line 81
    return-void
.end method
