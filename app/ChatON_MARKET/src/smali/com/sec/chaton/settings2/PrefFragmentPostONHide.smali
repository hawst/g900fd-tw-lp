.class public Lcom/sec/chaton/settings2/PrefFragmentPostONHide;
.super Lcom/sec/chaton/settings2/BasePreferenceFragment;
.source "PrefFragmentPostONHide.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Lcom/sec/chaton/settings/en;


# static fields
.field public static final BLOCK_BUDDY_LIST:Ljava/lang/String; = "block_buddy_list"

.field public static final BLOCK_BUDDY_RESULT:Ljava/lang/String; = "block_buddy_result"

.field public static final BLOCK_BUDDY_TYPE:Ljava/lang/String; = "block_buddy_type"

.field private static final KEY_POSTON_BLIND_LIST:Ljava/lang/String; = "blindlist"

.field private static final POSTON_BLIND_ADD:I = 0x0

.field private static final POSTON_BLIND_EDIT:I = 0x1

.field static mProgressDialog:Landroid/app/ProgressDialog;


# instance fields
.field private adapter:Lcom/sec/chaton/settings/ek;

.field private breadCrumb2:Landroid/widget/TextView;

.field private mAddItem:Landroid/view/MenuItem;

.field private mAttachedActivity:Landroid/app/Activity;

.field private mBlindFlag:Ljava/lang/Boolean;

.field private mBlindListNew:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBlindListOld:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mControl:Lcom/sec/chaton/d/v;

.field private mCount:I

.field private mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/block/aa;",
            ">;"
        }
    .end annotation
.end field

.field private mEditItem:Landroid/view/MenuItem;

.field private final mHandler:Landroid/os/Handler;

.field private mIsBlindListAlreadyUpdated:Z

.field private mListView:Landroid/widget/ListView;

.field private mNetworkErrorLayout:Landroid/widget/LinearLayout;

.field private mNetworkErrorView:Landroid/view/View;

.field private mNoBuddyLayout:Landroid/widget/LinearLayout;

.field private mNoBuddyView:Landroid/view/View;

.field mPostONHideList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    .line 360
    new-instance v0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide$3;

    invoke-direct {v0, p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide$3;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPostONHide;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private InitAdapter()V
    .locals 3

    .prologue
    .line 467
    new-instance v0, Lcom/sec/chaton/settings/ek;

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/settings/ek;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->adapter:Lcom/sec/chaton/settings/ek;

    .line 468
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->adapter:Lcom/sec/chaton/settings/ek;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/settings/ek;->a(Lcom/sec/chaton/settings/en;)V

    .line 469
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->adapter:Lcom/sec/chaton/settings/ek;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 470
    return-void
.end method

.method static synthetic access$000(Lcom/sec/chaton/settings2/PrefFragmentPostONHide;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAttachedActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/chaton/settings2/PrefFragmentPostONHide;)Lcom/sec/chaton/d/v;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mControl:Lcom/sec/chaton/d/v;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/chaton/settings2/PrefFragmentPostONHide;Lcom/sec/chaton/a/a/f;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->showPostONHideList(Lcom/sec/chaton/a/a/f;)V

    return-void
.end method

.method private showPostONHideList(Lcom/sec/chaton/a/a/f;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 431
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->c:Lcom/sec/chaton/j/o;

    if-eq v0, v2, :cond_3

    .line 432
    invoke-virtual {p1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/GetPostONBlindList;

    .line 433
    iget-object v0, v0, Lcom/sec/chaton/io/entry/GetPostONBlindList;->blind:Ljava/util/ArrayList;

    .line 434
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 435
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 437
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;

    .line 439
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;->value:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/chaton/e/a/d;->h(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 440
    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;->value:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Add blindedBuddies: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/PostONBlind;->value:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 444
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 445
    new-array v3, v2, [Ljava/lang/String;

    .line 446
    :goto_1
    if-ge v1, v2, :cond_2

    .line 447
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v1

    .line 446
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 449
    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->showPostONHideList_inner([Ljava/lang/String;)V

    .line 457
    :goto_2
    return-void

    .line 451
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 452
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNoBuddyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNetworkErrorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 454
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 455
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0b0205

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method

.method private showPostONHideList_inner([Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v1, 0x0

    .line 393
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 395
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 396
    new-instance v3, Lcom/sec/chaton/block/aa;

    aget-object v4, p1, v0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    aget-object v6, p1, v0

    invoke-static {v5, v6}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/chaton/block/aa;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mCount:I

    .line 403
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->breadCrumb2:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAttachedActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0182

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mData:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 406
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 407
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 409
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNoBuddyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNetworkErrorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 420
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    .line 421
    invoke-direct {p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->InitAdapter()V

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindFlag:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v8, :cond_1

    .line 424
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindFlag:Ljava/lang/Boolean;

    .line 425
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0b0020

    invoke-static {v0, v2, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 427
    :cond_1
    return-void

    .line 413
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 416
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNoBuddyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNetworkErrorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public getBlindListToSet()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - getBlindListToSet()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 296
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 297
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[mBlindOldList] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 301
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[mBlindNewList] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move v1, v2

    .line 304
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 306
    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v4

    .line 307
    if-ne v4, v6, :cond_2

    .line 308
    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 304
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 311
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 312
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[1st mBlindOldList] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 314
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 315
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[1st mBlindNewList] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 318
    :cond_5
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 320
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 321
    if-ne v1, v6, :cond_6

    .line 322
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 318
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 326
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 327
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[2nd mBlindOldList] "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 329
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 330
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[2nd mBlindNewList] "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 332
    :cond_9
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v1, -0x1

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 248
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 249
    packed-switch p1, :pswitch_data_0

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 251
    :pswitch_0
    if-ne p2, v1, :cond_0

    .line 252
    const-string v1, "Get blind list from user"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const-string v1, "blindlist"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 255
    array-length v1, v2

    iput v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mCount:I

    .line 256
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->breadCrumb2:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAttachedActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0182

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 258
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    invoke-static {v1}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 260
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 261
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 262
    iget-object v5, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mControl:Lcom/sec/chaton/d/v;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListNew:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/d/v;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 265
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindFlag:Ljava/lang/Boolean;

    .line 266
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0041

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v6}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mProgressDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 272
    :pswitch_1
    if-ne p2, v1, :cond_0

    .line 273
    const-string v1, "Remove multiple blinded buddy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 277
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindListOld:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 279
    const-string v1, "block_buddy_result"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 280
    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 281
    iget-object v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 283
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->showPostONHideList_inner([Ljava/lang/String;)V

    .line 285
    iput-boolean v6, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mIsBlindListAlreadyUpdated:Z

    goto/16 :goto_0

    .line 249
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 98
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAttachedActivity:Landroid/app/Activity;

    .line 99
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 85
    new-instance v0, Lcom/sec/chaton/d/v;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/v;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mControl:Lcom/sec/chaton/d/v;

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mControl:Lcom/sec/chaton/d/v;

    invoke-virtual {v0}, Lcom/sec/chaton/d/v;->b()V

    .line 87
    const-string v0, "Start getInteractionBlindList Sync"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindFlag:Ljava/lang/Boolean;

    .line 89
    iput-boolean v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mIsBlindListAlreadyUpdated:Z

    .line 92
    iput v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mCount:I

    .line 93
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 189
    const v0, 0x7f0f0006

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 190
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 191
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f07014c

    const v8, 0x7f07014b

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 116
    const v0, 0x7f0300dc

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 122
    const v0, 0x7f070416

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 123
    const v0, 0x7f07050b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 124
    const v2, 0x7f0b0011

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 125
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setClickable(Z)V

    .line 126
    new-instance v2, Lcom/sec/chaton/settings2/PrefFragmentPostONHide$1;

    invoke-direct {v2, p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide$1;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPostONHide;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    const v0, 0x7f07050d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->breadCrumb2:Landroid/widget/TextView;

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->breadCrumb2:Landroid/widget/TextView;

    const v2, 0x7f0b0182

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->breadCrumb2:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAttachedActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f08001b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 135
    const v0, 0x7f07050e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 136
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    const v0, 0x7f0703da

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mListView:Landroid/widget/ListView;

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mData:Ljava/util/ArrayList;

    .line 143
    const v0, 0x7f0703db

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNoBuddyLayout:Landroid/widget/LinearLayout;

    .line 144
    const v0, 0x7f0703dc

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNoBuddyView:Landroid/view/View;

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNoBuddyView:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 146
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNoBuddyView:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 147
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNoBuddyView:Landroid/view/View;

    const v4, 0x7f07014d

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 148
    const v4, 0x7f02034b

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 149
    const v0, 0x7f0b0155

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 150
    const v0, 0x7f0b0185

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 153
    const v0, 0x7f0703dd

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNetworkErrorLayout:Landroid/widget/LinearLayout;

    .line 154
    const v0, 0x7f0703de

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNetworkErrorView:Landroid/view/View;

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNetworkErrorView:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 156
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNetworkErrorView:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 157
    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNetworkErrorView:Landroid/view/View;

    const v4, 0x7f07014d

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 158
    const v4, 0x7f02034e

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 159
    const v0, 0x7f0b002a

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 160
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNoBuddyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mNetworkErrorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 166
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    sput-object v0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 167
    sget-object v0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/chaton/settings2/PrefFragmentPostONHide$2;

    invoke-direct {v1, p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide$2;-><init>(Lcom/sec/chaton/settings2/PrefFragmentPostONHide;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 182
    sget-object v0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 183
    invoke-virtual {p0, v7}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->setHasOptionsMenu(Z)V

    .line 184
    return-object v3
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 353
    sget-object v0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 354
    sget-object v0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 356
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDestroyView()V

    .line 357
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onDetach()V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAttachedActivity:Landroid/app/Activity;

    .line 105
    return-void
.end method

.method public onItemClicked(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 474
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 475
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 476
    sget-object v1, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 477
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindFlag:Ljava/lang/Boolean;

    .line 478
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mControl:Lcom/sec/chaton/d/v;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/d/v;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PostON Buddy Show: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 484
    invoke-virtual {p0, p1}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 222
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f070577

    if-ne v0, v1, :cond_0

    .line 223
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAttachedActivity:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/buddy/BuddyActivity2;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 224
    const-string v1, "BUDDY_SORT_STYLE"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 225
    const-string v1, "ACTIVITY_PURPOSE"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 226
    const-string v1, "ACTIVITY_PURPOSE_ARG"

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mPostONHideList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 227
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->startActivityForResult(Landroid/content/Intent;I)V

    .line 229
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f070578

    if-ne v0, v1, :cond_1

    .line 230
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAttachedActivity:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/poston/PostONHideListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 231
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 232
    const-string v2, "block_buddy_list"

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 233
    const-string v2, "block_buddy_type"

    const-string v3, "post_on"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 236
    invoke-virtual {p0, v0, v4}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->startActivityForResult(Landroid/content/Intent;I)V

    .line 239
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_2

    .line 240
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 243
    :cond_2
    return v4
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 463
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPause()V

    .line 464
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 196
    invoke-super {p0, p1}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 197
    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    .line 198
    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAddItem:Landroid/view/MenuItem;

    .line 199
    const v0, 0x7f070578

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 201
    const v0, 0x7f070577

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAddItem:Landroid/view/MenuItem;

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAddItem:Landroid/view/MenuItem;

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAddItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {p0}, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/e/a/d;->b(Landroid/content/ContentResolver;)I

    move-result v0

    .line 207
    if-gtz v0, :cond_2

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAddItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 212
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAddItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mEditItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 219
    :cond_1
    return-void

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAddItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 337
    invoke-super {p0}, Lcom/sec/chaton/settings2/BasePreferenceFragment;->onResume()V

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->breadCrumb2:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mAttachedActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0182

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 340
    iget-boolean v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mIsBlindListAlreadyUpdated:Z

    if-eqz v0, :cond_0

    .line 341
    iput-boolean v4, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mIsBlindListAlreadyUpdated:Z

    .line 349
    :goto_0
    return-void

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mControl:Lcom/sec/chaton/d/v;

    invoke-virtual {v0}, Lcom/sec/chaton/d/v;->b()V

    .line 344
    sget-object v0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 345
    const-string v0, "Start getInteractionBlindList Sync"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPostONHide;->mBlindFlag:Ljava/lang/Boolean;

    goto :goto_0
.end method
