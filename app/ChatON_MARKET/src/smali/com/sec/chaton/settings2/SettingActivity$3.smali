.class Lcom/sec/chaton/settings2/SettingActivity$3;
.super Landroid/os/Handler;
.source "SettingActivity.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/SettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/SettingActivity;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, Lcom/sec/chaton/settings2/SettingActivity$3;->this$0:Lcom/sec/chaton/settings2/SettingActivity;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 487
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity$3;->this$0:Lcom/sec/chaton/settings2/SettingActivity;

    # getter for: Lcom/sec/chaton/settings2/SettingActivity;->isDestroy:Z
    invoke-static {v0}, Lcom/sec/chaton/settings2/SettingActivity;->access$000(Lcom/sec/chaton/settings2/SettingActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 490
    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 491
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3eb

    if-ne v0, v1, :cond_0

    .line 492
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 496
    sget-object v1, Lcom/sec/chaton/global/GlobalApplication;->a:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 498
    const/4 v0, 0x0

    .line 502
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/ey;->a(Landroid/content/Context;Z)V

    .line 503
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity$3;->this$0:Lcom/sec/chaton/settings2/SettingActivity;

    # getter for: Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/SettingActivity;->access$100(Lcom/sec/chaton/settings2/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity$3;->this$0:Lcom/sec/chaton/settings2/SettingActivity;

    # getter for: Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/SettingActivity;->access$100(Lcom/sec/chaton/settings2/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/chaton/settings2/SettingActivity$3;->this$0:Lcom/sec/chaton/settings2/SettingActivity;

    # getter for: Lcom/sec/chaton/settings2/SettingActivity;->progressBar:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/SettingActivity;->access$100(Lcom/sec/chaton/settings2/SettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method
