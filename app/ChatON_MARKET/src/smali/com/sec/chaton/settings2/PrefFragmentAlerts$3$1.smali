.class Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;
.super Ljava/lang/Object;
.source "PrefFragmentAlerts.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mSelectedPopupType:I
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$500(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)I

    move-result v1

    # setter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mCurrentPopupType:I
    invoke-static {v0, v1}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$402(Lcom/sec/chaton/settings2/PrefFragmentAlerts;I)I

    .line 252
    const-string v0, ""

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->mCurrentPopupType:I
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$400(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)I

    move-result v0

    if-nez v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b032c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 256
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting is simple popup"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 262
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    iget-object v2, v2, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->prefItemPopupType:Landroid/preference/Preference;
    invoke-static {v2}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$600(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Landroid/preference/Preference;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    iget-object v3, v3, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    invoke-virtual {v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    invoke-static {v1, v0, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$700(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 263
    return-void

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    iget-object v0, v0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    invoke-virtual {v0}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b032d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 260
    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3$1;->this$1:Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;

    iget-object v1, v1, Lcom/sec/chaton/settings2/PrefFragmentAlerts$3;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->sharedPref:Lcom/sec/chaton/util/ab;
    invoke-static {v1}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$100(Lcom/sec/chaton/settings2/PrefFragmentAlerts;)Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "Setting is simple popup"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method
