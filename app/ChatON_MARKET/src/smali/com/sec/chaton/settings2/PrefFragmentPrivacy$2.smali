.class Lcom/sec/chaton/settings2/PrefFragmentPrivacy$2;
.super Ljava/lang/Object;
.source "PrefFragmentPrivacy.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mNetworkError:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$200(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$300(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 246
    :goto_0
    return v2

    .line 236
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237
    if-eqz p2, :cond_1

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->control:Lcom/sec/chaton/d/h;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$000(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/chaton/d/h;->c(Z)V

    .line 243
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$100(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentPrivacy$2;->this$0:Lcom/sec/chaton/settings2/PrefFragmentPrivacy;

    # getter for: Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->control:Lcom/sec/chaton/d/h;
    invoke-static {v0}, Lcom/sec/chaton/settings2/PrefFragmentPrivacy;->access$000(Lcom/sec/chaton/settings2/PrefFragmentPrivacy;)Lcom/sec/chaton/d/h;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/chaton/d/h;->c(Z)V

    goto :goto_1
.end method
