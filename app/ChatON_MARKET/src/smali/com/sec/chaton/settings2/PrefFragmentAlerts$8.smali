.class Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;
.super Landroid/os/Handler;
.source "PrefFragmentAlerts.java"


# instance fields
.field final synthetic this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

.field final synthetic val$prefItemPushStatus:Landroid/preference/Preference;


# direct methods
.method constructor <init>(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Landroid/os/Looper;Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iput-object p3, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;->val$prefItemPushStatus:Landroid/preference/Preference;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const v4, 0x7f08001b

    .line 399
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 400
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3eb

    if-ne v0, v1, :cond_1

    .line 401
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 405
    sget-object v1, Lcom/sec/chaton/global/GlobalApplication;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 407
    const/4 v0, 0x0

    .line 413
    :cond_0
    if-eqz v0, :cond_2

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    const v2, 0x7f0b0145

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;->val$prefItemPushStatus:Landroid/preference/Preference;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    invoke-virtual {v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$700(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Ljava/lang/String;Landroid/preference/Preference;I)V

    .line 421
    :cond_1
    :goto_0
    return-void

    .line 416
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    iget-object v1, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    const v2, 0x7f0b0146

    invoke-virtual {v1, v2}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;->val$prefItemPushStatus:Landroid/preference/Preference;

    iget-object v3, p0, Lcom/sec/chaton/settings2/PrefFragmentAlerts$8;->this$0:Lcom/sec/chaton/settings2/PrefFragmentAlerts;

    invoke-virtual {v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    # invokes: Lcom/sec/chaton/settings2/PrefFragmentAlerts;->setTextWitColor(Ljava/lang/String;Landroid/preference/Preference;I)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/settings2/PrefFragmentAlerts;->access$700(Lcom/sec/chaton/settings2/PrefFragmentAlerts;Ljava/lang/String;Landroid/preference/Preference;I)V

    goto :goto_0
.end method
