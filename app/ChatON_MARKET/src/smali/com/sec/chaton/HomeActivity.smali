.class public Lcom/sec/chaton/HomeActivity;
.super Lcom/sec/chaton/base/BaseActivity;
.source "HomeActivity.java"

# interfaces
.implements Lcom/sec/chaton/n;
.implements Lcom/sec/chaton/util/cn;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/coolots/sso/a/a;

.field private c:Lcom/sec/chaton/registration/fu;

.field private d:Z

.field private final e:Landroid/os/Handler;

.field private f:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/sec/chaton/HomeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/HomeActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/HomeActivity;->b:Lcom/coolots/sso/a/a;

    .line 66
    new-instance v0, Lcom/sec/chaton/x;

    invoke-direct {v0, p0}, Lcom/sec/chaton/x;-><init>(Lcom/sec/chaton/HomeActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/HomeActivity;->e:Landroid/os/Handler;

    .line 394
    new-instance v0, Lcom/sec/chaton/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/z;-><init>(Lcom/sec/chaton/HomeActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/HomeActivity;->f:Landroid/accounts/AccountManagerCallback;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/HomeActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Z)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 100
    const-class v0, Lcom/sec/chaton/HomeActivity;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 101
    const/high16 v0, 0x10000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 102
    const-string v0, "skipSplash"

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 104
    return-object p1
.end method

.method public static a(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/chaton/HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 89
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 90
    const-string v1, "skipSplash"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 92
    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/HomeActivity;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/chaton/HomeActivity;->d(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/HomeActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/chaton/HomeActivity;->e:Landroid/os/Handler;

    return-object v0
.end method

.method private a(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 307
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkScreenMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/HomeActivity;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_0
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 313
    invoke-virtual {p0}, Lcom/sec/chaton/HomeActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 315
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 316
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "densityDpi : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Config : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/HomeActivity;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :cond_1
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/HomeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 320
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 323
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v4, :cond_4

    .line 324
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    if-ne v3, v1, :cond_3

    .line 325
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/HomeActivity;->setRequestedOrientation(I)V

    .line 349
    :cond_2
    :goto_0
    return-void

    .line 326
    :cond_3
    const/4 v1, 0x3

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    if-ne v1, v0, :cond_2

    .line 327
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/HomeActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 330
    :cond_4
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    if-nez v1, :cond_5

    .line 331
    invoke-virtual {p0, v3}, Lcom/sec/chaton/HomeActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 332
    :cond_5
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    if-ne v4, v0, :cond_2

    .line 333
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/HomeActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/HomeActivity;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/chaton/HomeActivity;->d:Z

    return p1
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/chaton/HomeActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    sget-object v0, Lcom/sec/chaton/util/ak;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 112
    sget-object v1, Lcom/sec/chaton/util/ak;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 114
    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/chaton/HomeActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    :cond_0
    invoke-static {}, Lcom/sec/chaton/util/ak;->a()Lcom/sec/chaton/util/ak;

    move-result-object v0

    sget v2, Lcom/sec/chaton/util/ak;->a:I

    invoke-virtual {v0, p0, v2}, Lcom/sec/chaton/util/ak;->a(Landroid/content/Context;I)V

    .line 117
    :cond_1
    if-eqz v1, :cond_2

    .line 118
    invoke-static {}, Lcom/sec/chaton/util/ak;->a()Lcom/sec/chaton/util/ak;

    move-result-object v0

    sget v1, Lcom/sec/chaton/util/ak;->b:I

    invoke-virtual {v0, p0, v1}, Lcom/sec/chaton/util/ak;->a(Landroid/content/Context;I)V

    .line 120
    :cond_2
    return-void
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 295
    invoke-virtual {p0}, Lcom/sec/chaton/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 297
    const-string v2, "finish"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298
    const/4 v0, 0x1

    .line 300
    :cond_0
    return v0
.end method

.method private d(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 387
    invoke-static {p0, p1}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 391
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 435
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "back_deregi_fail"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/HomeActivity;->e:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/chaton/HomeActivity;->d:Z

    if-nez v0, :cond_0

    .line 276
    invoke-virtual {p0}, Lcom/sec/chaton/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 278
    iget-object v1, p0, Lcom/sec/chaton/HomeActivity;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/HomeActivity;->e:Landroid/os/Handler;

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 280
    :cond_0
    return-void
.end method

.method public getBlackTheme()I
    .locals 1

    .prologue
    .line 474
    const v0, 0x7f0c00fd

    return v0
.end method

.method public getDefaultTheme()I
    .locals 1

    .prologue
    .line 469
    const v0, 0x7f0c00fd

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 285
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 287
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 288
    const-string v0, "onConfigurationChanged"

    sget-object v1, Lcom/sec/chaton/HomeActivity;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 127
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 130
    invoke-virtual {p0}, Lcom/sec/chaton/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 132
    invoke-direct {p0, v3}, Lcom/sec/chaton/HomeActivity;->c(Landroid/content/Intent;)V

    .line 136
    invoke-direct {p0}, Lcom/sec/chaton/HomeActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    const-string v0, "AuthenticatorActivity is requested finish."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/chaton/HomeActivity;->finish()V

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/HomeActivity;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 145
    if-nez p1, :cond_2

    .line 158
    invoke-virtual {p0, v1, v1}, Lcom/sec/chaton/HomeActivity;->overridePendingTransition(II)V

    .line 159
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.chaton"

    iget-object v6, p0, Lcom/sec/chaton/HomeActivity;->f:Landroid/accounts/AccountManagerCallback;

    move-object v3, v2

    move-object v4, v2

    move-object v5, p0

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0

    .line 161
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/HomeActivity;->finish()V

    goto :goto_0

    .line 166
    :cond_3
    const-string v0, "skipSplash"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 167
    const-string v4, "skipSplash"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 170
    sget-object v4, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    if-eqz v4, :cond_4

    sget-object v4, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    invoke-virtual {v4}, Lcom/sec/chaton/m;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v4

    sget-object v5, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v4, v5, :cond_5

    :cond_4
    move v0, v1

    .line 174
    :cond_5
    if-eqz v0, :cond_6

    .line 176
    invoke-direct {p0, v3}, Lcom/sec/chaton/HomeActivity;->d(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/chaton/HomeActivity;->finish()V

    goto :goto_0

    .line 179
    :cond_6
    const v0, 0x7f0300f0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/HomeActivity;->setContentView(I)V

    .line 181
    invoke-virtual {p0}, Lcom/sec/chaton/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/HomeActivity;->a(Landroid/content/res/Configuration;)V

    .line 184
    invoke-direct {p0}, Lcom/sec/chaton/HomeActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 188
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/sec/chaton/service/BackGroundRegiService;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 189
    const-string v4, "request_type"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 190
    const-string v4, "request_on_chaton"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 191
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 201
    :cond_7
    const-string v0, "460"

    invoke-static {}, Lcom/sec/chaton/util/am;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 202
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_8

    .line 203
    const-string v0, "Sim is china, so show first launcher"

    sget-object v3, Lcom/sec/chaton/HomeActivity;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_8
    new-instance v0, Lcom/sec/chaton/y;

    invoke-direct {v0, p0}, Lcom/sec/chaton/y;-><init>(Lcom/sec/chaton/HomeActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/HomeActivity;->c:Lcom/sec/chaton/registration/fu;

    .line 220
    new-instance v0, Lcom/sec/chaton/registration/es;

    iget-object v3, p0, Lcom/sec/chaton/HomeActivity;->c:Lcom/sec/chaton/registration/fu;

    invoke-direct {v0, p0, v2, v3}, Lcom/sec/chaton/registration/es;-><init>(Landroid/content/Context;Landroid/os/Bundle;Lcom/sec/chaton/registration/fu;)V

    invoke-virtual {v0}, Lcom/sec/chaton/registration/es;->a()V

    .line 221
    iput-boolean v6, p0, Lcom/sec/chaton/HomeActivity;->d:Z

    .line 227
    :goto_1
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    if-nez v0, :cond_b

    .line 228
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_9

    .line 229
    const-string v0, "Application initialize logic isn\'t execute. execute it now."

    sget-object v2, Lcom/sec/chaton/HomeActivity;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_9
    new-instance v0, Lcom/sec/chaton/m;

    invoke-direct {v0}, Lcom/sec/chaton/m;-><init>()V

    sput-object v0, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    .line 233
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/m;->a(Lcom/sec/chaton/n;)V

    .line 234
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/m;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 223
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/HomeActivity;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/chaton/HomeActivity;->e:Landroid/os/Handler;

    invoke-static {v2, v1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v3, 0x1f4

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 237
    :cond_b
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    invoke-virtual {v0}, Lcom/sec/chaton/m;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 238
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_c

    .line 239
    const-string v0, "Application initialize logic alrady running."

    sget-object v1, Lcom/sec/chaton/HomeActivity;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_c
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/m;->a(Lcom/sec/chaton/n;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 255
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onDestroy()V

    .line 258
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    if-eqz v0, :cond_0

    .line 259
    sget-object v0, Lcom/sec/chaton/global/GlobalApplication;->e:Lcom/sec/chaton/m;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/m;->b(Lcom/sec/chaton/n;)V

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/HomeActivity;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 265
    const-string v0, "chatonv_feature"

    invoke-static {v0}, Lcom/sec/chaton/global/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/HomeActivity;->b:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_1

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/HomeActivity;->b:Lcom/coolots/sso/a/a;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 270
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 250
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onResume()V

    .line 251
    return-void
.end method
