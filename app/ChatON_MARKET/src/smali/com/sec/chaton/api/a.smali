.class public Lcom/sec/chaton/api/a;
.super Ljava/lang/Object;
.source "BuddyBackgroundHandler.java"

# interfaces
.implements Lcom/sec/chaton/api/b;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/chaton/api/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 133
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.chaton.api.background.ADD_BUDDY_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 135
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 137
    const-string v1, "request_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    if-ne p3, v2, :cond_0

    .line 140
    const-string v1, "result"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 144
    :goto_0
    const-string v1, "result_code"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 146
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 147
    return-void

    .line 142
    :cond_0
    const-string v1, "result"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Intent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 42
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 44
    const-string v3, "com.sec.chaton.api.background.ADD_BUDDY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 129
    :goto_0
    return v0

    .line 47
    :cond_0
    const-string v2, "request_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 48
    const-string v2, "packageName"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 49
    const-string v2, "password"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 50
    const-string v5, "id"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 51
    const-string v6, "isChatONId"

    invoke-virtual {p1, v6, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 54
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v2}, Lcom/sec/chaton/api/access_token/AccessTokenProvider;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 55
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_1

    .line 56
    const-string v0, "Access token is invalid."

    sget-object v2, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_1
    invoke-direct {p0, v4, v3, v8}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    move v0, v1

    .line 60
    goto :goto_0

    .line 64
    :cond_2
    invoke-static {}, Lcom/sec/common/util/i;->k()Z

    move-result v2

    if-nez v2, :cond_4

    .line 65
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_3

    .line 66
    const-string v0, "Network isn\'t available."

    sget-object v2, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_3
    invoke-direct {p0, v4, v3, v8}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    move v0, v1

    .line 70
    goto :goto_0

    .line 73
    :cond_4
    new-instance v2, Lcom/sec/chaton/d/h;

    const/4 v7, 0x0

    invoke-direct {v2, v7}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 74
    invoke-virtual {v2, v5, v6}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;Z)Lcom/sec/chaton/d/a/h;

    move-result-object v2

    .line 77
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/chaton/d/a/h;->b()Lcom/sec/chaton/a/a/f;

    move-result-object v2

    .line 79
    invoke-virtual {v2}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v5

    sget-object v6, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v5, v6, :cond_6

    .line 80
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_5

    .line 81
    const-string v2, "AddBuddy is success."

    sget-object v5, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_5
    const/4 v2, 0x1

    invoke-direct {p0, v4, v3, v2}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :goto_1
    move v0, v1

    .line 129
    goto :goto_0

    .line 84
    :cond_6
    invoke-virtual {v2}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v5

    sget-object v6, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v5, v6, :cond_9

    .line 85
    sget-boolean v2, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v2, :cond_7

    .line 86
    const-string v2, "Unknown buddy."

    sget-object v5, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_7
    const/16 v2, 0x64

    invoke-direct {p0, v4, v3, v2}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 117
    :catch_0
    move-exception v2

    .line 118
    sget-boolean v5, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v5, :cond_8

    .line 119
    sget-object v5, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 121
    :cond_8
    invoke-direct {p0, v4, v3, v0}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 90
    :cond_9
    :try_start_1
    invoke-virtual {v2}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v5

    const/16 v6, 0x3e83

    if-ne v5, v6, :cond_c

    .line 91
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_a

    .line 92
    const-string v2, "Can\'t add myself."

    sget-object v5, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_a
    const/16 v2, 0x65

    invoke-direct {p0, v4, v3, v2}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 122
    :catch_1
    move-exception v2

    .line 123
    sget-boolean v5, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v5, :cond_b

    .line 124
    sget-object v5, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 126
    :cond_b
    invoke-direct {p0, v4, v3, v0}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 95
    :cond_c
    :try_start_2
    invoke-virtual {v2}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v5

    const/16 v6, 0x3e84

    if-ne v5, v6, :cond_e

    .line 96
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_d

    .line 97
    const-string v2, "Already your buddy."

    sget-object v5, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_d
    const/16 v2, 0x66

    invoke-direct {p0, v4, v3, v2}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 100
    :cond_e
    invoke-virtual {v2}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v5

    const/16 v6, 0x3e85

    if-ne v5, v6, :cond_10

    .line 101
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_f

    .line 102
    const-string v2, "Blocked buddy."

    sget-object v5, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_f
    const/16 v2, 0x67

    invoke-direct {p0, v4, v3, v2}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 105
    :cond_10
    invoke-virtual {v2}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    const/16 v5, 0x3e8a

    if-ne v2, v5, :cond_12

    .line 106
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_11

    .line 107
    const-string v2, "Invalid phone number."

    sget-object v5, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_11
    const/16 v2, 0x68

    invoke-direct {p0, v4, v3, v2}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 111
    :cond_12
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_13

    .line 112
    const-string v2, "Network error."

    sget-object v5, Lcom/sec/chaton/api/a;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_13
    const/4 v2, 0x0

    invoke-direct {p0, v4, v3, v2}, Lcom/sec/chaton/api/a;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1
.end method
