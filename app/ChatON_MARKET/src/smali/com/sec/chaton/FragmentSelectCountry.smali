.class public Lcom/sec/chaton/FragmentSelectCountry;
.super Landroid/support/v4/app/Fragment;
.source "FragmentSelectCountry.java"


# instance fields
.field final a:I

.field final b:I

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private d:[Ljava/lang/CharSequence;

.field private e:[Ljava/lang/CharSequence;

.field private f:Ljava/lang/String;

.field private g:Landroid/app/ProgressDialog;

.field private h:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 26
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->a:I

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->b:I

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->g:Landroid/app/ProgressDialog;

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, -0x1

    .line 99
    packed-switch p1, :pswitch_data_0

    .line 145
    :goto_0
    return-void

    .line 101
    :pswitch_0
    if-ne p2, v3, :cond_1

    .line 102
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 105
    const-string v1, "PARAMS_COUNTRY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->f:Ljava/lang/String;

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->c:Ljava/util/Map;

    iget-object v1, p0, Lcom/sec/chaton/FragmentSelectCountry;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 112
    const-string v1, "country_letter"

    iget-object v2, p0, Lcom/sec/chaton/FragmentSelectCountry;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v1, "country_name"

    invoke-static {v1, v0}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ACS] mCountryLetter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/FragmentSelectCountry;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " selectedCountryName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 126
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 124
    :cond_0
    const-string v0, "selectedCountryName is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 129
    :cond_1
    if-nez p2, :cond_2

    .line 130
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 131
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 132
    :cond_2
    if-ne p2, v1, :cond_3

    .line 135
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 136
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 140
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->h:Landroid/content/Context;

    .line 46
    const-string v0, "onCreate.............."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->g:Landroid/app/ProgressDialog;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->c:Ljava/util/Map;

    .line 49
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f0d0000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->d:[Ljava/lang/CharSequence;

    .line 51
    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0002

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/FragmentSelectCountry;->e:[Ljava/lang/CharSequence;

    move v0, v1

    .line 52
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/FragmentSelectCountry;->d:[Ljava/lang/CharSequence;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 54
    iget-object v2, p0, Lcom/sec/chaton/FragmentSelectCountry;->c:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/chaton/FragmentSelectCountry;->e:[Ljava/lang/CharSequence;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/sec/chaton/FragmentSelectCountry;->d:[Ljava/lang/CharSequence;

    aget-object v4, v4, v0

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/FragmentSelectCountry;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/registration/CountryActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    const-string v2, "mode"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 60
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 61
    const/16 v1, 0x10

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/FragmentSelectCountry;->startActivityForResult(Landroid/content/Intent;I)V

    .line 63
    return-void
.end method
