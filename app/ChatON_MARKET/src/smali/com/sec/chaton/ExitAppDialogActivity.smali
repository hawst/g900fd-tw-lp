.class public Lcom/sec/chaton/ExitAppDialogActivity;
.super Landroid/app/Activity;
.source "ExitAppDialogActivity.java"


# instance fields
.field private a:I

.field private b:Lcom/coolots/sso/a/a;

.field private c:Lcom/sec/common/a/d;

.field private d:Landroid/content/Context;

.field private e:Landroid/app/ProgressDialog;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->e:Landroid/app/ProgressDialog;

    .line 47
    const-class v0, Lcom/sec/chaton/ExitAppDialogActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->f:Ljava/lang/String;

    .line 200
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/ExitAppDialogActivity;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->a:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/ExitAppDialogActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/chaton/ExitAppDialogActivity;->e:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method public static a(I)V
    .locals 2

    .prologue
    .line 192
    const-string v0, "exit_app_done"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/aa;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 193
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.chaton.ACTION_EXIT_APP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 194
    const-string v1, "reason"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 195
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 197
    return-void
.end method

.method public static a()Z
    .locals 3

    .prologue
    .line 174
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "exit_app_done"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/ExitAppDialogActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->d:Landroid/content/Context;

    return-object v0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 178
    const-string v0, "Exit App. Unauthorized."

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/ExitAppDialogActivity;->a(I)V

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 184
    const-string v0, "Currently, there is NO UID"

    const-string v1, "ExitAppDialogActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/ExitAppDialogActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->e:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 85
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 86
    const-string v0, "showAlertDailog"

    iget-object v1, p0, Lcom/sec/chaton/ExitAppDialogActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/r;

    invoke-direct {v2, p0}, Lcom/sec/chaton/r;-><init>(Lcom/sec/chaton/ExitAppDialogActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->c:Lcom/sec/common/a/d;

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->c:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->c:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 122
    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 125
    iget v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->a:I

    if-ne v3, v0, :cond_0

    .line 127
    invoke-virtual {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 140
    :goto_0
    return-object v0

    .line 129
    :cond_0
    const/4 v0, 0x2

    iget v1, p0, Lcom/sec/chaton/ExitAppDialogActivity;->a:I

    if-ne v0, v1, :cond_1

    .line 131
    invoke-virtual {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 132
    :cond_1
    const/4 v0, 0x3

    iget v1, p0, Lcom/sec/chaton/ExitAppDialogActivity;->a:I

    if-ne v0, v1, :cond_2

    .line 134
    invoke-virtual {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 135
    :cond_2
    const/4 v0, 0x4

    iget v1, p0, Lcom/sec/chaton/ExitAppDialogActivity;->a:I

    if-ne v0, v1, :cond_3

    .line 136
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "samsung_account_email"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137
    invoke-virtual {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b03fb

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 140
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0195

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 58
    invoke-virtual {p0, v2}, Lcom/sec/chaton/ExitAppDialogActivity;->setFinishOnTouchOutside(Z)V

    .line 62
    :cond_0
    iput-object p0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->d:Landroid/content/Context;

    .line 64
    invoke-virtual {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "reason"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->a:I

    .line 65
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mExitReason : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/ExitAppDialogActivity;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/ExitAppDialogActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_1
    invoke-static {p0}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->b:Lcom/coolots/sso/a/a;

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->b:Lcom/coolots/sso/a/a;

    invoke-virtual {v0, p0}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->b:Lcom/coolots/sso/a/a;

    invoke-static {p0, v0}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    .line 80
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->c()V

    .line 81
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 170
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 148
    invoke-virtual {p0, p1}, Lcom/sec/chaton/ExitAppDialogActivity;->setIntent(Landroid/content/Intent;)V

    .line 150
    if-eqz p1, :cond_0

    .line 151
    const-string v0, "reason"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/ExitAppDialogActivity;->a:I

    .line 155
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/ExitAppDialogActivity;->c()V

    .line 156
    return-void

    .line 153
    :cond_0
    iput v1, p0, Lcom/sec/chaton/ExitAppDialogActivity;->a:I

    goto :goto_0
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 256
    const-string v0, "onUserLeaveHint"

    iget-object v1, p0, Lcom/sec/chaton/ExitAppDialogActivity;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    return-void
.end method
