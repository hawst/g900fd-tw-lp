.class Lcom/sec/chaton/calllog/view/aa;
.super Landroid/os/Handler;
.source "DeleteCallLogFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V
    .locals 1

    .prologue
    .line 595
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 597
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/aa;->b:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;Lcom/sec/chaton/calllog/view/w;)V
    .locals 0

    .prologue
    .line 595
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/aa;-><init>(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V

    return-void
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 631
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "moveToPreActivity() isAllDeleted:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;Ljava/lang/String;)V

    .line 632
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 633
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 635
    if-eqz p1, :cond_0

    .line 645
    const-string v0, "DELETE_DETAIL_CALLLOG_ALL"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 646
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v5, v2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 648
    invoke-static {v6}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 650
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/ab;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/ab;->c()V

    .line 670
    :goto_0
    return-void

    .line 655
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 656
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    .line 657
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->d(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-nez v0, :cond_1

    .line 658
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->d(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->_id:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 656
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 662
    :cond_2
    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 663
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 665
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v5, v2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 666
    invoke-static {v6}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 668
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/ab;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/ab;->c()V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 602
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->isDetached()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    iget-boolean v1, p0, Lcom/sec/chaton/calllog/view/aa;->b:Z

    if-nez v1, :cond_0

    .line 610
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 612
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    const-string v2, "CallLog Updated!!"

    invoke-static {v1, v2}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;Ljava/lang/String;)V

    .line 613
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V

    .line 615
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v0, :cond_2

    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/aa;->a(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 621
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/aa;->a:Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V

    goto :goto_0

    .line 610
    :pswitch_data_0
    .packed-switch 0x1f40
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
