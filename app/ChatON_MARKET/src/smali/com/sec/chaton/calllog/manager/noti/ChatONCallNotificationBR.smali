.class public Lcom/sec/chaton/calllog/manager/noti/ChatONCallNotificationBR;
.super Landroid/content/BroadcastReceiver;
.source "ChatONCallNotificationBR.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/chaton/calllog/manager/noti/ChatONCallNotificationBR;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/calllog/manager/noti/ChatONCallNotificationBR;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 23
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/calllog/manager/noti/ChatONCallNotificationBR;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const-string v1, "com.sec.chaton.MISSEDCALL_CREATE_NOTIFICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 25
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/calllog/manager/noti/a;->a()Lcom/sec/chaton/calllog/manager/noti/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/noti/a;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 36
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    const-string v1, "com.sec.chaton.MISSEDCALL_DELETE_NOTIFICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 28
    invoke-static {}, Lcom/sec/chaton/calllog/manager/noti/a;->a()Lcom/sec/chaton/calllog/manager/noti/a;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/manager/noti/a;->a(I)V

    goto :goto_0

    .line 30
    :cond_2
    const-string v1, "com.sec.chaton.REJECT_MSG_CREATE_NOTIFICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 31
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/calllog/manager/noti/a;->a()Lcom/sec/chaton/calllog/manager/noti/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/noti/a;->c()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 33
    :cond_3
    const-string v1, "com.sec.chaton.REJECT_MSG_DELETE_NOTIFICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-static {}, Lcom/sec/chaton/calllog/manager/noti/a;->a()Lcom/sec/chaton/calllog/manager/noti/a;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/manager/noti/a;->a(I)V

    goto :goto_0
.end method
