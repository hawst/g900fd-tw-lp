.class Lcom/sec/chaton/calllog/view/s;
.super Landroid/os/Handler;
.source "CallLogFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/calllog/view/CallLogFragment;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/sec/chaton/calllog/view/CallLogFragment;)V
    .locals 1

    .prologue
    .line 1235
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/s;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1236
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/s;->b:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/calllog/view/CallLogFragment;Lcom/sec/chaton/calllog/view/m;)V
    .locals 0

    .prologue
    .line 1235
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/s;-><init>(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 1241
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/s;->b:Z

    if-eqz v0, :cond_0

    .line 1282
    :goto_0
    return-void

    .line 1245
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1265
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/s;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    goto :goto_0

    .line 1248
    :sswitch_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/s;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    const-string v1, "CallLog Updated!!"

    invoke-static {v0, v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Lcom/sec/chaton/calllog/view/CallLogFragment;Ljava/lang/String;)V

    .line 1250
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/s;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    goto :goto_0

    .line 1253
    :sswitch_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/s;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    const-string v1, "CallLog Added!!"

    invoke-static {v0, v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Lcom/sec/chaton/calllog/view/CallLogFragment;Ljava/lang/String;)V

    .line 1254
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/s;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    goto :goto_0

    .line 1259
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 1260
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/s;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-static {v1, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Lcom/sec/chaton/calllog/view/CallLogFragment;Lcom/sec/chaton/calllog/manager/model/CallLogData;)V

    goto :goto_0

    .line 1270
    :sswitch_4
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/s;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    goto :goto_0

    .line 1245
    nop

    :sswitch_data_0
    .sparse-switch
        0x7d1 -> :sswitch_0
        0x82d -> :sswitch_4
        0x1f40 -> :sswitch_1
        0x1f42 -> :sswitch_3
        0x1f43 -> :sswitch_2
    .end sparse-switch
.end method
