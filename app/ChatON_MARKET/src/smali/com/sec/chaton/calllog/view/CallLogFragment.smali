.class public Lcom/sec/chaton/calllog/view/CallLogFragment;
.super Landroid/support/v4/app/Fragment;
.source "CallLogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static B:Ljava/lang/String;

.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Lcom/sec/chaton/calllog/view/a;

.field public static d:I

.field public static e:I

.field public static final f:Lcom/sec/chaton/calllog/manager/a;

.field private static i:I


# instance fields
.field private A:Landroid/app/ProgressDialog;

.field public g:Lcom/coolots/sso/a/a;

.field public h:Landroid/os/Handler;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/content/Context;

.field private o:Lcom/sec/chaton/calllog/view/s;

.field private p:Landroid/view/View;

.field private q:Landroid/widget/ListView;

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private u:I

.field private v:Landroid/support/v4/app/DialogFragment;

.field private final w:Lcom/sec/chaton/calllog/manager/b/a;

.field private final x:Lcom/sec/chaton/calllog/view/v;

.field private y:Landroid/app/Dialog;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 104
    sput-object v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->a:Ljava/lang/String;

    .line 105
    sput-object v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->b:Ljava/lang/String;

    .line 107
    sput v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->i:I

    .line 108
    sput-object v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->c:Lcom/sec/chaton/calllog/view/a;

    .line 132
    sput v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->d:I

    sput v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->e:I

    .line 153
    new-instance v0, Lcom/sec/chaton/calllog/manager/a;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/manager/a;-><init>()V

    sput-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    .line 167
    sput-object v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->B:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 134
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    .line 135
    new-instance v0, Lcom/sec/chaton/calllog/view/s;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/calllog/view/s;-><init>(Lcom/sec/chaton/calllog/view/CallLogFragment;Lcom/sec/chaton/calllog/view/m;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->o:Lcom/sec/chaton/calllog/view/s;

    .line 136
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->p:Landroid/view/View;

    .line 140
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->q:Landroid/widget/ListView;

    .line 147
    iput v2, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->u:I

    .line 154
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->w:Lcom/sec/chaton/calllog/manager/b/a;

    .line 156
    new-instance v0, Lcom/sec/chaton/calllog/view/v;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/view/v;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->x:Lcom/sec/chaton/calllog/view/v;

    .line 158
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->y:Landroid/app/Dialog;

    .line 159
    iput v2, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->z:I

    .line 165
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->A:Landroid/app/ProgressDialog;

    .line 1372
    new-instance v0, Lcom/sec/chaton/calllog/view/r;

    invoke-direct {v0, p0}, Lcom/sec/chaton/calllog/view/r;-><init>(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->h:Landroid/os/Handler;

    return-void
.end method

.method public static a(Lcom/sec/chaton/calllog/manager/model/c;)I
    .locals 3

    .prologue
    .line 440
    const/4 v0, 0x0

    .line 442
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 444
    const/4 v0, 0x4

    .line 446
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBuddyType ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/calllog/view/CallLogActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/CallLogFragment;)Lcom/sec/chaton/calllog/view/s;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->o:Lcom/sec/chaton/calllog/view/s;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 223
    const-string v0, "24"

    .line 224
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 225
    const-string v0, "12"

    .line 228
    :cond_0
    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 79
    sput-object p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->B:Ljava/lang/String;

    return-object p0
.end method

.method private a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V
    .locals 2

    .prologue
    .line 1203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1204
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1205
    sget-object v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/manager/a;->b(Ljava/util/ArrayList;)I

    .line 1206
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/CallLogFragment;Lcom/sec/chaton/calllog/manager/model/CallLogData;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/CallLogFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1362
    sput-object p2, Lcom/sec/chaton/calllog/view/CallLogFragment;->B:Ljava/lang/String;

    .line 1365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "addBuddy() buddyNo["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/calllog/view/CallLogActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1367
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->h()V

    .line 1368
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->h:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1369
    invoke-virtual {v0, p1}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;)Lcom/sec/chaton/d/a/h;

    .line 1370
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 1220
    if-eqz p2, :cond_1

    .line 1221
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->g:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "Push Name"

    const-string v6, ""

    invoke-virtual {v3, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1226
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Return value from ChatonV= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    if-eqz v0, :cond_0

    .line 1228
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b008a

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1230
    :cond_0
    return-void

    .line 1224
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->g:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "Push Name"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move v2, v7

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1178
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1180
    if-eqz p1, :cond_0

    .line 1181
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->q:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1182
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1188
    :goto_0
    return-void

    .line 1185
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->q:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1186
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public static b()I
    .locals 1

    .prologue
    .line 1358
    sget v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->i:I

    return v0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 232
    .line 233
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 234
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v2

    .line 235
    if-eqz v2, :cond_4

    array-length v0, v2

    if-lez v0, :cond_4

    .line 236
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_3

    .line 237
    aget-char v3, v2, v0

    const/16 v4, 0x79

    if-ne v3, v4, :cond_0

    .line 238
    const-string v3, "yyyy-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 240
    :cond_0
    aget-char v3, v2, v0

    const/16 v4, 0x4d

    if-ne v3, v4, :cond_1

    .line 241
    const-string v3, "MM-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 243
    :cond_1
    aget-char v3, v2, v0

    const/16 v4, 0x64

    if-ne v3, v4, :cond_2

    .line 244
    const-string v3, "dd-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 236
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 250
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/calllog/view/CallLogFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 124
    const-string v0, "[CallLogFragment]"

    invoke-static {p1, v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-void
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/calllog/view/CallLogFragment;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->g()V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 129
    const-string v0, "[CallLogFragment]"

    invoke-static {p1, v0}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->p:Landroid/view/View;

    const v1, 0x7f0700ef

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->j:Landroid/widget/LinearLayout;

    .line 344
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->k:Landroid/widget/ImageView;

    .line 345
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->k:Landroid/widget/ImageView;

    const v1, 0x7f02034c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->l:Landroid/widget/TextView;

    .line 347
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0351

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->m:Landroid/widget/TextView;

    .line 349
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->m:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0352

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 350
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->j:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 352
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/calllog/view/CallLogFragment;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->i()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/calllog/view/CallLogFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    return-object v0
.end method

.method private e()V
    .locals 8

    .prologue
    const/high16 v5, -0x40800000    # -1.0f

    .line 558
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 559
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 561
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 562
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 787
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->q:Landroid/widget/ListView;

    .line 788
    new-instance v1, Lcom/sec/chaton/calllog/view/o;

    invoke-direct {v1, p0}, Lcom/sec/chaton/calllog/view/o;-><init>(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 805
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 852
    const-string v0, "handleEventContactUpdateUI()"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Ljava/lang/String;)V

    .line 853
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a()V

    .line 854
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->c:Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/a;->notifyDataSetChanged()V

    .line 855
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 1429
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->A:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 1430
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->A:Landroid/app/ProgressDialog;

    .line 1434
    :goto_0
    return-void

    .line 1432
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 1437
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->A:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 1438
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1440
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ContextMenu$ContextMenuInfo;)I
    .locals 1

    .prologue
    .line 778
    check-cast p1, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 780
    iget v0, p1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    return v0
.end method

.method public a()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 813
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    sget v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->i:I

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/calllog/manager/a;->a(IZ)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    .line 815
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/a;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->t:Ljava/util/HashMap;

    .line 818
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<<RDJ>> calllog count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/a;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->b(Ljava/lang/String;)V

    .line 820
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Z)V

    .line 822
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->r:Ljava/util/List;

    move v0, v2

    .line 826
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    if-gt v0, v1, :cond_0

    .line 828
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->r:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 826
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 836
    :cond_0
    new-instance v0, Lcom/sec/chaton/calllog/view/a;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->t:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->o:Lcom/sec/chaton/calllog/view/s;

    iget-object v6, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->x:Lcom/sec/chaton/calllog/view/v;

    iget-object v7, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->r:Ljava/util/List;

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/calllog/view/a;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/HashMap;Landroid/os/Handler;Lcom/sec/chaton/calllog/view/u;Ljava/util/List;Lcom/sec/chaton/calllog/common/view/swipe/e;)V

    sput-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->c:Lcom/sec/chaton/calllog/view/a;

    .line 838
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->q:Landroid/widget/ListView;

    sget-object v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->c:Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 843
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->p:Landroid/view/View;

    const v1, 0x7f0700ee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 844
    sget v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->d:I

    sget v2, Lcom/sec/chaton/calllog/view/CallLogFragment;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 845
    return-void
.end method

.method public a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 753
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 755
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 756
    instance-of v2, v0, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-eqz v2, :cond_0

    .line 769
    :goto_0
    return-void

    .line 759
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/calllog/view/CallLogDetailActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 760
    const-string v2, "normal"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 761
    const-string v2, "calllogdata"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 762
    const-string v2, "searchtype"

    sget v3, Lcom/sec/chaton/calllog/view/CallLogFragment;->i:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 763
    const-string v2, "order"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 765
    if-ne p1, v4, :cond_1

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-nez v0, :cond_1

    .line 766
    const-string v0, "updateUIAfterCall"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 768
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 863
    packed-switch p1, :pswitch_data_0

    .line 880
    :goto_0
    return-void

    .line 865
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->g:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 866
    if-le v1, v0, :cond_0

    .line 867
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0368

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/sec/chaton/calllog/view/CallLogFragment;->i:I

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a(Ljava/lang/String;IZ)Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;

    move-result-object v0

    .line 868
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "viewbydialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 866
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 873
    :pswitch_1
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0353

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b037f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->v:Landroid/support/v4/app/DialogFragment;

    .line 875
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->v:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "groupdeletedialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0

    .line 863
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c(I)V
    .locals 9

    .prologue
    .line 1153
    sput p1, Lcom/sec/chaton/calllog/view/CallLogFragment;->i:I

    .line 1158
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    sget v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->i:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/calllog/manager/a;->a(IZ)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    .line 1161
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Z)V

    .line 1165
    new-instance v0, Lcom/sec/chaton/calllog/view/a;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->t:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->o:Lcom/sec/chaton/calllog/view/s;

    iget-object v6, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->x:Lcom/sec/chaton/calllog/view/v;

    iget-object v7, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->r:Ljava/util/List;

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/calllog/view/a;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/HashMap;Landroid/os/Handler;Lcom/sec/chaton/calllog/view/u;Ljava/util/List;Lcom/sec/chaton/calllog/common/view/swipe/e;)V

    sput-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->c:Lcom/sec/chaton/calllog/view/a;

    .line 1167
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->q:Landroid/widget/ListView;

    sget-object v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->c:Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1168
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->c:Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/a;->notifyDataSetChanged()V

    .line 1169
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 218
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 220
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1320
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1321
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1336
    :cond_0
    :goto_0
    return-void

    .line 1326
    :cond_1
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6

    .prologue
    .line 1296
    if-eqz p2, :cond_0

    .line 1297
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0357

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b037e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0359

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/calllog/view/q;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/view/q;-><init>(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b035a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/calllog/view/p;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/view/p;-><init>(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 1315
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1288
    const-string v0, "onConfigurationChanged()"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Ljava/lang/String;)V

    .line 1289
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1291
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->g()V

    .line 1292
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    const/high16 v9, 0x20000000

    const/high16 v8, 0x20000

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 568
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Landroid/view/ContextMenu$ContextMenuInfo;)I

    move-result v3

    .line 569
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 570
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    .line 571
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    .line 574
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 575
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 742
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 579
    :pswitch_0
    iget-object v4, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v7, :cond_0

    .line 580
    sget-object v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/a;->a()Ljava/util/HashMap;

    move-result-object v1

    iget-object v4, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-static {v1, v4, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Ljava/util/HashMap;Ljava/util/List;Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v1

    .line 582
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-class v6, Lcom/sec/chaton/calllog/view/ShowConferenceMemberList;

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "calllogdata"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v4, "searchtype"

    sget v5, Lcom/sec/chaton/calllog/view/CallLogFragment;->i:I

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v4, "order"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "makecall"

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "groupName"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from_intent"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 593
    :cond_0
    const-string v0, "single voice call start!!"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Ljava/lang/String;)V

    .line 594
    invoke-direct {p0, v1, v2}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 600
    :pswitch_1
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v7, :cond_1

    .line 601
    sget-object v1, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/a;->a()Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-static {v1, v2, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Ljava/util/HashMap;Ljava/util/List;Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v1

    .line 603
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/sec/chaton/calllog/view/ShowConferenceMemberList;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    const-string v4, "calllogdata"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "searchtype"

    sget v4, Lcom/sec/chaton/calllog/view/CallLogFragment;->i:I

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "order"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "makecall"

    invoke-virtual {v0, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "groupName"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from_intent"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 614
    :cond_1
    const-string v0, "single video call start!!"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Ljava/lang/String;)V

    .line 615
    invoke-direct {p0, v1, v7}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 621
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0356

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0378

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b035a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/calllog/view/n;

    invoke-direct {v2, p0, v3}, Lcom/sec/chaton/calllog/view/n;-><init>(Lcom/sec/chaton/calllog/view/CallLogFragment;I)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b036a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/calllog/view/m;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/view/m;-><init>(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 653
    :pswitch_3
    new-instance v3, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    const-class v4, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 654
    const-string v4, "PROFILE_BUDDY_NO"

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 655
    const-string v1, "PROFILE_BUDDY_NAME"

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->w:Lcom/sec/chaton/calllog/manager/b/a;

    invoke-virtual {v2, v0}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 657
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 662
    :pswitch_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 664
    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/util/ArrayList;

    move-result-object v1

    .line 669
    sget-object v3, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v3}, Lcom/sec/chaton/calllog/manager/a;->a()Ljava/util/HashMap;

    move-result-object v3

    .line 671
    iget-object v4, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->w:Lcom/sec/chaton/calllog/manager/b/a;

    iget-object v5, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-virtual {v4, v0, v3, v5}, Lcom/sec/chaton/calllog/manager/b/a;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/util/HashMap;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 672
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 674
    invoke-static {v0, v3}, Lcom/sec/chaton/e/a/f;->d(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v4

    .line 675
    new-instance v5, Lcom/sec/chaton/buddy/a/b;

    if-nez v1, :cond_2

    move v0, v2

    :goto_1
    const/4 v6, 0x2

    invoke-direct {v5, v4, v3, v0, v6}, Lcom/sec/chaton/buddy/a/b;-><init>(ILjava/lang/String;II)V

    .line 681
    new-instance v4, Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    const-class v6, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;

    invoke-direct {v4, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 682
    const-string v0, "ACTIVITY_PURPOSE"

    const/16 v6, 0x12

    invoke-virtual {v4, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 683
    const-string v0, "groupInfo"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 684
    const-string v5, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    new-array v0, v2, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 685
    const-string v0, "GROUP_PROFILE_NAME"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 687
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    invoke-virtual {v0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 675
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_1

    .line 711
    :pswitch_5
    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 718
    :pswitch_6
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 719
    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/util/ArrayList;

    move-result-object v4

    .line 721
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v3, v2

    .line 722
    :goto_2
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_3

    .line 723
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 722
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 726
    :cond_3
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    const-class v3, Lcom/sec/chaton/chat/ChatInfoMoreActivity;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 727
    const-string v0, "ACTIVITY_PURPOSE"

    const/4 v3, 0x6

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 728
    sget-object v3, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    new-array v0, v2, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 729
    const-string v3, "member_name"

    new-array v0, v2, [Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 730
    const-string v0, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 731
    const-string v0, "ACTIVITY_PURPOSE_CALLLOG_GROUP"

    invoke-virtual {v1, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 733
    invoke-virtual {p0, v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 575
    :pswitch_data_0
    .packed-switch 0x7f070599
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 181
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 183
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->g:Lcom/coolots/sso/a/a;

    .line 184
    const/4 v0, 0x0

    sput v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->i:I

    .line 186
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 5

    .prologue
    const v4, 0x7f0f0018

    .line 486
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 487
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    invoke-virtual {p0, p3}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Landroid/view/ContextMenu$ContextMenuInfo;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 490
    instance-of v1, v0, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-eqz v1, :cond_0

    .line 555
    :goto_0
    return-void

    .line 495
    :cond_0
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-nez v1, :cond_1

    .line 498
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Lcom/sec/chaton/calllog/manager/model/c;)I

    move-result v1

    .line 500
    packed-switch v1, :pswitch_data_0

    .line 553
    :goto_1
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 554
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->e()V

    goto :goto_0

    .line 502
    :pswitch_0
    const v1, 0x7f0f0015

    invoke-virtual {v2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 503
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->w:Lcom/sec/chaton/calllog/manager/b/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_1

    .line 506
    :pswitch_1
    const v1, 0x7f0f0017

    invoke-virtual {v2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 507
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->w:Lcom/sec/chaton/calllog/manager/b/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_1

    .line 510
    :pswitch_2
    invoke-virtual {v2, v4, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 511
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->w:Lcom/sec/chaton/calllog/manager/b/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_1

    .line 518
    :pswitch_3
    invoke-virtual {v2, v4, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 519
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->w:Lcom/sec/chaton/calllog/manager/b/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_1

    .line 539
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->w:Lcom/sec/chaton/calllog/manager/b/a;

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->t:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-virtual {v1, v0, v3, v4}, Lcom/sec/chaton/calllog/manager/b/a;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/util/HashMap;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 541
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 543
    :cond_2
    const v1, 0x7f0f0019

    invoke-virtual {v2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 544
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->w:Lcom/sec/chaton/calllog/manager/b/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_1

    .line 548
    :cond_3
    const v0, 0x7f0f0016

    invoke-virtual {v2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 549
    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto :goto_1

    .line 500
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    const v0, 0x7f0f0020

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 372
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 373
    return-void

    .line 368
    :cond_0
    const v0, 0x7f0f001f

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 257
    const-string v0, "onCreateView()"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Ljava/lang/String;)V

    .line 258
    const v0, 0x7f030026

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->p:Landroid/view/View;

    .line 260
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    .line 261
    invoke-static {v2}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->a:Ljava/lang/String;

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->b:Ljava/lang/String;

    .line 267
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->a:Ljava/lang/String;

    const-string v1, "24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 268
    :cond_0
    invoke-static {v2}, Lcom/sec/chaton/calllog/view/a;->b(Z)V

    .line 275
    :goto_0
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 276
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->b:Ljava/lang/String;

    const-string v1, "MM-dd-yyyy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 277
    invoke-static {v2}, Lcom/sec/chaton/calllog/view/a;->a(I)V

    .line 296
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->p:Landroid/view/View;

    const v1, 0x7f0700ee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->q:Landroid/widget/ListView;

    .line 300
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->f()V

    .line 302
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->d()V

    .line 304
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a()V

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->q:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 334
    invoke-virtual {p0, v3}, Lcom/sec/chaton/calllog/view/CallLogFragment;->setHasOptionsMenu(Z)V

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->q:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->p:Landroid/view/View;

    return-object v0

    .line 271
    :cond_2
    invoke-static {v3}, Lcom/sec/chaton/calllog/view/a;->b(Z)V

    goto :goto_0

    .line 279
    :cond_3
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->b:Ljava/lang/String;

    const-string v1, "dd-MM-yyyy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 280
    invoke-static {v3}, Lcom/sec/chaton/calllog/view/a;->a(I)V

    goto :goto_1

    .line 282
    :cond_4
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->b:Ljava/lang/String;

    const-string v1, "yyyy-MM-dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 283
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/a;->a(I)V

    goto :goto_1

    .line 288
    :cond_5
    invoke-static {v2}, Lcom/sec/chaton/calllog/view/a;->a(I)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 213
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 214
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onItemClick+position: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->b(Ljava/lang/String;)V

    .line 358
    invoke-virtual {p0, p3}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(I)V

    .line 359
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 379
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 433
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 383
    :sswitch_0
    invoke-virtual {p0, v3}, Lcom/sec/chaton/calllog/view/CallLogFragment;->b(I)V

    .line 384
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 388
    :sswitch_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 389
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0351

    invoke-virtual {p0, v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 398
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 392
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/calllog/view/DeleteCallLogActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 394
    const-string v1, "from_intent"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 396
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/calllog/view/CallLogFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 406
    :sswitch_2
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    .line 407
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->f(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 408
    if-eqz v0, :cond_0

    .line 411
    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->startActivity(Landroid/content/Intent;)V

    .line 412
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 416
    :sswitch_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/ActivityWebView;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 417
    const-string v1, "PARAM_MENU"

    const-string v2, "Help"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 418
    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 425
    :sswitch_4
    invoke-static {v2}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 426
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/t;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/t;->c()V

    goto/16 :goto_0

    .line 379
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_4
        0x7f0705a0 -> :sswitch_1
        0x7f0705a2 -> :sswitch_0
        0x7f0705a3 -> :sswitch_2
        0x7f0705a4 -> :sswitch_3
    .end sparse-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 207
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 209
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 190
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 191
    const-string v0, "<<YHT99>> onResume() called!!"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Ljava/lang/String;)V

    .line 197
    invoke-static {}, Lcom/sec/chaton/calllog/manager/noti/a;->a()Lcom/sec/chaton/calllog/manager/noti/a;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/manager/noti/a;->a(I)V

    .line 201
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogFragment;->o:Lcom/sec/chaton/calllog/view/s;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/manager/a;->a(Landroid/os/Handler;)V

    .line 202
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->g()V

    .line 203
    return-void
.end method
