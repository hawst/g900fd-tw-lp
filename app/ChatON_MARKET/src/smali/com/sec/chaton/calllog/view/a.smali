.class public Lcom/sec/chaton/calllog/view/a;
.super Landroid/widget/ArrayAdapter;
.source "CallLogArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Z

.field public static b:Z

.field public static c:Ljava/lang/String;

.field public static d:Landroid/content/Context;


# instance fields
.field public e:Landroid/os/Handler;

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/sec/chaton/calllog/view/u;

.field public j:Lcom/sec/chaton/calllog/common/view/swipe/e;

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/view/layout/List_Common;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 39
    sput-boolean v0, Lcom/sec/chaton/calllog/view/a;->a:Z

    .line 41
    sput-boolean v0, Lcom/sec/chaton/calllog/view/a;->b:Z

    .line 43
    sput-object v1, Lcom/sec/chaton/calllog/view/a;->c:Ljava/lang/String;

    .line 45
    sput-object v1, Lcom/sec/chaton/calllog/view/a;->d:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/HashMap;Landroid/os/Handler;Lcom/sec/chaton/calllog/view/u;Ljava/util/List;Lcom/sec/chaton/calllog/common/view/swipe/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Handler;",
            "Lcom/sec/chaton/calllog/view/u;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/sec/chaton/calllog/common/view/swipe/e;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 77
    sput-object p1, Lcom/sec/chaton/calllog/view/a;->d:Landroid/content/Context;

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 47
    iput-object v0, p0, Lcom/sec/chaton/calllog/view/a;->e:Landroid/os/Handler;

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/calllog/view/a;->g:Ljava/util/List;

    .line 53
    iput-object v0, p0, Lcom/sec/chaton/calllog/view/a;->h:Ljava/util/HashMap;

    .line 55
    iput-object v0, p0, Lcom/sec/chaton/calllog/view/a;->i:Lcom/sec/chaton/calllog/view/u;

    .line 57
    iput-object v0, p0, Lcom/sec/chaton/calllog/view/a;->j:Lcom/sec/chaton/calllog/common/view/swipe/e;

    .line 79
    iput-object p5, p0, Lcom/sec/chaton/calllog/view/a;->e:Landroid/os/Handler;

    .line 81
    iput-object p3, p0, Lcom/sec/chaton/calllog/view/a;->f:Ljava/util/ArrayList;

    .line 83
    iput-object p6, p0, Lcom/sec/chaton/calllog/view/a;->i:Lcom/sec/chaton/calllog/view/u;

    .line 85
    iput-object p7, p0, Lcom/sec/chaton/calllog/view/a;->g:Ljava/util/List;

    .line 87
    iput-object p4, p0, Lcom/sec/chaton/calllog/view/a;->h:Ljava/util/HashMap;

    .line 89
    iput-object p8, p0, Lcom/sec/chaton/calllog/view/a;->j:Lcom/sec/chaton/calllog/common/view/swipe/e;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/a;->k:Ljava/util/ArrayList;

    .line 93
    return-void
.end method

.method public static a(I)V
    .locals 1

    .prologue
    .line 169
    packed-switch p0, :pswitch_data_0

    .line 195
    :goto_0
    return-void

    .line 173
    :pswitch_0
    const-string v0, "MM/dd/yyyy"

    sput-object v0, Lcom/sec/chaton/calllog/view/a;->c:Ljava/lang/String;

    goto :goto_0

    .line 179
    :pswitch_1
    const-string v0, "dd/MM/yyyy"

    sput-object v0, Lcom/sec/chaton/calllog/view/a;->c:Ljava/lang/String;

    goto :goto_0

    .line 185
    :pswitch_2
    const-string v0, "yyyy/MM/dd"

    sput-object v0, Lcom/sec/chaton/calllog/view/a;->c:Ljava/lang/String;

    goto :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Z)V
    .locals 0

    .prologue
    .line 157
    sput-boolean p0, Lcom/sec/chaton/calllog/view/a;->a:Z

    .line 159
    return-void
.end method

.method public static b(Z)V
    .locals 0

    .prologue
    .line 163
    sput-boolean p0, Lcom/sec/chaton/calllog/view/a;->b:Z

    .line 165
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/a;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/a;->g:Ljava/util/List;

    .line 147
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 200
    check-cast p2, Lcom/sec/chaton/calllog/view/layout/List_Common;

    .line 204
    if-nez p2, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/a;->i:Lcom/sec/chaton/calllog/view/u;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/a;->i:Lcom/sec/chaton/calllog/view/u;

    sget-object v1, Lcom/sec/chaton/calllog/view/a;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/a;->e:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/a;->j:Lcom/sec/chaton/calllog/common/view/swipe/e;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/calllog/view/u;->a(Landroid/content/Context;Landroid/os/Handler;Lcom/sec/chaton/calllog/common/view/swipe/e;)Lcom/sec/chaton/calllog/view/layout/List_Common;

    move-result-object p2

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    move-object v0, p2

    .line 216
    invoke-virtual {p0, p1}, Lcom/sec/chaton/calllog/view/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 220
    if-eqz v1, :cond_2

    .line 222
    const/4 v6, 0x0

    .line 224
    iget-object v2, p0, Lcom/sec/chaton/calllog/view/a;->g:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/a;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, p1, :cond_1

    .line 226
    iget-object v2, p0, Lcom/sec/chaton/calllog/view/a;->g:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 230
    :cond_1
    sget-boolean v2, Lcom/sec/chaton/calllog/view/a;->a:Z

    sget-boolean v3, Lcom/sec/chaton/calllog/view/a;->b:Z

    sget-object v4, Lcom/sec/chaton/calllog/view/a;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/chaton/calllog/view/a;->h:Ljava/util/HashMap;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/calllog/view/layout/List_Common;->setView(Lcom/sec/chaton/calllog/manager/model/CallLogData;ZZLjava/lang/String;Ljava/util/HashMap;Z)V

    .line 234
    :cond_2
    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_Common;->setTag(Ljava/lang/Object;)V

    .line 238
    return-object v0
.end method
