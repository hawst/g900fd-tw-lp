.class public Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;
.super Landroid/widget/RelativeLayout;
.source "List_ProfileCallLogDetail.java"


# instance fields
.field private a:Landroid/widget/RelativeLayout;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/CheckBox;

.field private h:Lcom/sec/chaton/calllog/manager/model/CallLogData;

.field private i:I

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->h:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->i:I

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->j:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->k:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->l:Ljava/lang/String;

    .line 34
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->a(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->h:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->i:I

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->j:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->k:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->l:Ljava/lang/String;

    .line 49
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->a(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method private final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    const v0, 0x7f03012e

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 61
    const v0, 0x7f0704c3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->a:Landroid/widget/RelativeLayout;

    .line 62
    const v0, 0x7f0704c4

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->b:Landroid/widget/ImageView;

    .line 63
    const v0, 0x7f0704c6

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->c:Landroid/widget/ImageView;

    .line 64
    const v0, 0x7f0704c8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->d:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0704c7

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->f:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0704c9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->e:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0704aa

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->g:Landroid/widget/CheckBox;

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setDuration(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setTime(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->h:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setCallType(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V

    .line 72
    iget v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->i:I

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setCallMethod(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setDate(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 76
    return-void
.end method


# virtual methods
.method public final setCallMethod(I)V
    .locals 2

    .prologue
    const v0, 0x7f02037d

    .line 91
    iput p1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->i:I

    .line 95
    packed-switch p1, :pswitch_data_0

    .line 114
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 115
    return-void

    .line 103
    :pswitch_1
    const v0, 0x7f0200e9

    .line 104
    goto :goto_0

    .line 107
    :pswitch_2
    const v0, 0x7f0200e8

    .line 108
    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setCallType(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V
    .locals 2

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->h:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->b:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/sec/chaton/calllog/manager/b/a;->c(Lcom/sec/chaton/calllog/manager/model/CallLogData;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 87
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 139
    return-void
.end method

.method public setCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 148
    return-void
.end method

.method public final setDate(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->l:Ljava/lang/String;

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :cond_0
    return-void
.end method

.method public setDeleteMode(Z)V
    .locals 2

    .prologue
    .line 124
    if-eqz p1, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->g:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->g:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method public final setDuration(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->k:Ljava/lang/String;

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    :cond_0
    return-void
.end method

.method public final setTime(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->j:Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    :cond_0
    return-void
.end method
