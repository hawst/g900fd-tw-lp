.class public Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "RadioButtonListDialog.java"


# static fields
.field private static a:I

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput v0, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a:I

    .line 23
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a:I

    return v0
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 18
    sput p0, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a:I

    return p0
.end method

.method public static a(Ljava/lang/String;IZ)Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;
    .locals 3

    .prologue
    .line 27
    new-instance v0, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;-><init>()V

    .line 28
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 29
    const-string v2, "title"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->setArguments(Landroid/os/Bundle;)V

    .line 31
    sput p1, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a:I

    .line 32
    sput-boolean p2, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->b:Z

    .line 34
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0368

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 42
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 43
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b037d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0361

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0360

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    sget-boolean v0, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->b:Z

    if-eqz v0, :cond_0

    .line 47
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0358

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b035f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b036f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0373

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0374

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0372

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    sget-boolean v0, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->b:Z

    if-nez v0, :cond_1

    .line 56
    sget v0, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a:I

    const/4 v3, 0x4

    if-le v0, v3, :cond_1

    .line 57
    sget v0, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a:I

    add-int/lit8 v0, v0, -0x2

    sput v0, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a:I

    .line 79
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    .line 80
    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    .line 82
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v1

    sget v3, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a:I

    new-instance v4, Lcom/sec/chaton/calllog/common/view/layout/d;

    invoke-direct {v4, p0, v2}, Lcom/sec/chaton/calllog/common/view/layout/d;-><init>(Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;Ljava/util/List;)V

    invoke-virtual {v1, v0, v3, v4}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b003a

    new-instance v2, Lcom/sec/chaton/calllog/common/view/layout/c;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/common/view/layout/c;-><init>(Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 161
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 163
    return-void
.end method
