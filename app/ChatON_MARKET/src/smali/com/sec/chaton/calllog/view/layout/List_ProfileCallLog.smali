.class public Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;
.super Lcom/sec/chaton/calllog/view/layout/List_Common;
.source "List_ProfileCallLog.java"


# instance fields
.field public final a:Landroid/content/Context;

.field private b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;


# direct methods
.method private a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    const-string v0, ""

    .line 87
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b()Z

    move-result v0

    .line 89
    if-eqz p2, :cond_1

    .line 90
    if-eqz v0, :cond_0

    .line 91
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v2, "HH:mm"

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    .line 93
    :cond_0
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v2, "HH:mm"

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 97
    :cond_1
    if-eqz v0, :cond_2

    .line 98
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v2, "a h:mm"

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 100
    :cond_2
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v2, "h:mm a"

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 158
    const-string v0, "kor"

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a()V

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setVisibility(I)V

    .line 111
    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/graphics/Color;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    invoke-direct {v0, p1}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    .line 46
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->addView(Landroid/view/View;)V

    .line 48
    return-void
.end method

.method protected a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;ZLjava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 137
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setDeleteMode(Z)V

    .line 140
    iget v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    iget v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setTime(Ljava/lang/String;)V

    .line 148
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->k:I

    invoke-virtual {v2, v0}, Lcom/sec/chaton/calllog/manager/b/a;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setDuration(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setCallType(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    iget v1, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setCallMethod(I)V

    .line 152
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    invoke-virtual {v2, v0, p4}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setDate(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    invoke-virtual {v0, p5}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setChecked(Z)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setVisibility(I)V

    .line 155
    return-void
.end method

.method public setView(Lcom/sec/chaton/calllog/manager/model/CallLogData;ZZLjava/lang/String;Ljava/util/HashMap;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 68
    instance-of v0, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->b:Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLogDetail;->setVisibility(I)V

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    instance-of v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    if-eqz v0, :cond_0

    .line 72
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p4

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/calllog/view/layout/List_ProfileCallLog;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;ZLjava/lang/String;Z)V

    goto :goto_0
.end method
