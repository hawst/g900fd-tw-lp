.class public Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;
.super Landroid/widget/RelativeLayout;
.source "List_TypeDetailCallLog.java"


# instance fields
.field private a:Landroid/widget/RelativeLayout;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->a(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->a(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method private final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    const v0, 0x7f03011b

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 45
    const v0, 0x7f0700ee

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->a:Landroid/widget/RelativeLayout;

    .line 46
    const v0, 0x7f0704ae

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->b:Landroid/widget/ImageView;

    .line 47
    const v0, 0x7f0704ad

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->c:Landroid/widget/ImageView;

    .line 48
    const v0, 0x7f0704af

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->d:Landroid/widget/ImageView;

    .line 49
    const v0, 0x7f0704ac

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->e:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f0704b0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->f:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f0704aa

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->g:Landroid/widget/CheckBox;

    .line 53
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 55
    return-void
.end method


# virtual methods
.method public final setCallMethod(I)V
    .locals 3

    .prologue
    const v0, 0x7f0200ee

    const/16 v2, 0x8

    .line 70
    .line 72
    packed-switch p1, :pswitch_data_0

    .line 93
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 94
    return-void

    .line 76
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 81
    :pswitch_2
    const v0, 0x7f0200ed

    .line 82
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 86
    :pswitch_3
    const v0, 0x7f0200e8

    .line 87
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->d:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setCallType(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->b:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/sec/chaton/calllog/manager/b/a;->c(Lcom/sec/chaton/calllog/manager/model/CallLogData;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 65
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 132
    return-void
.end method

.method public setCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 153
    return-void
.end method

.method public setDeleteMode(Z)V
    .locals 2

    .prologue
    .line 117
    if-eqz p1, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->g:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->g:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method public final setDuration(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->f:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 108
    :cond_0
    return-void
.end method

.method public final setTime(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    :cond_0
    return-void
.end method
