.class public Lcom/sec/chaton/calllog/manager/model/d;
.super Ljava/lang/Object;
.source "CallLogList.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/calllog/manager/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/calllog/manager/model/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    .line 53
    new-instance v0, Lcom/sec/chaton/calllog/manager/a/a;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/manager/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    .line 54
    return-void
.end method

.method private a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 712
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    .line 714
    new-instance v1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    invoke-direct {v1}, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;-><init>()V

    .line 715
    if-eqz v0, :cond_0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 717
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0365

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 721
    iput-object v0, v1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    .line 722
    iput v3, v1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->displaytype:I

    .line 737
    :goto_0
    return-object v1

    .line 723
    :cond_1
    if-ne v0, v4, :cond_2

    .line 725
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0366

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 729
    iput-object v0, v1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    .line 730
    iput v4, v1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->displaytype:I

    goto :goto_0

    .line 731
    :cond_2
    const/4 v2, 0x2

    if-lt v0, v2, :cond_3

    .line 732
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/manager/model/d;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    goto :goto_0

    .line 734
    :cond_3
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/manager/model/d;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;IZ)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "IZ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 608
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 609
    if-eqz p1, :cond_3

    .line 611
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 612
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 614
    new-instance v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-direct {v0, p1}, Lcom/sec/chaton/calllog/manager/model/CallLogData;-><init>(Landroid/database/Cursor;)V

    .line 615
    :cond_0
    :goto_0
    :pswitch_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 617
    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/calllog/manager/model/CallLogData;->a(Landroid/database/Cursor;I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 648
    :pswitch_1
    invoke-direct {p0, v1, v0, p3}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/ArrayList;Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)V

    .line 649
    new-instance v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-direct {v0, p1}, Lcom/sec/chaton/calllog/manager/model/CallLogData;-><init>(Landroid/database/Cursor;)V

    .line 650
    if-ne p2, v3, :cond_0

    .line 651
    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/List;)Z

    goto :goto_0

    .line 621
    :pswitch_2
    invoke-direct {p0, v1, v0, p3}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/ArrayList;Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)V

    .line 622
    new-instance v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-direct {v0, p1}, Lcom/sec/chaton/calllog/manager/model/CallLogData;-><init>(Landroid/database/Cursor;)V

    .line 623
    if-ne p2, v3, :cond_0

    .line 624
    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/List;)Z

    goto :goto_0

    .line 629
    :pswitch_3
    if-nez p2, :cond_0

    .line 631
    invoke-direct {p0, v1, v0, p3}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/ArrayList;Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)V

    .line 632
    new-instance v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-direct {v0, p1}, Lcom/sec/chaton/calllog/manager/model/CallLogData;-><init>(Landroid/database/Cursor;)V

    goto :goto_0

    .line 640
    :pswitch_4
    invoke-direct {p0, v1, v0, p3}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/ArrayList;Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)V

    .line 641
    new-instance v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-direct {v0, p1}, Lcom/sec/chaton/calllog/manager/model/CallLogData;-><init>(Landroid/database/Cursor;)V

    .line 642
    if-ne p2, v3, :cond_0

    .line 643
    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/List;)Z

    goto :goto_0

    .line 661
    :cond_1
    invoke-direct {p0, v1, v0, p3}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/ArrayList;Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)V

    .line 662
    if-ne p2, v3, :cond_2

    .line 663
    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/List;)Z

    .line 666
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 669
    :cond_3
    return-object v1

    .line 617
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private a(Ljava/util/ArrayList;Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 674
    if-eqz p3, :cond_0

    .line 676
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_3

    .line 678
    invoke-direct {p0, p2}, Lcom/sec/chaton/calllog/manager/model/d;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    move-result-object v0

    .line 679
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 689
    :cond_0
    :goto_0
    iget-object v0, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 690
    iget-object v0, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    .line 691
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v4, "chaton_id"

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 692
    iget v1, v0, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    iput v1, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myCalllogtype:I

    .line 693
    iget v1, v0, Lcom/sec/chaton/calllog/manager/model/c;->k:I

    iput v1, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myDuration:I

    .line 694
    iget-object v1, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    iget-object v2, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 695
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    iget-object v0, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v4

    move v2, v3

    .line 697
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 698
    iget-object v0, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 699
    iget-object v0, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 697
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 681
    :cond_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    iget-object v0, p2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/calllog/manager/b/a;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 683
    invoke-direct {p0, p2}, Lcom/sec/chaton/calllog/manager/model/d;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    move-result-object v0

    .line 684
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 707
    :cond_4
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 708
    return-void
.end method

.method private a(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 742
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 744
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/manager/model/CallLogData;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 747
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 748
    const/4 v0, 0x1

    .line 752
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogDBData;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    const-string v0, "createCallLogDBList()"

    sget-object v3, Lcom/sec/chaton/calllog/manager/model/d;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 82
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;

    .line 83
    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->b()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 84
    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->a()Landroid/content/ContentValues;

    move-result-object v0

    .line 85
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 87
    :cond_0
    const-string v0, "NO CALLDATE!!!!!!!!!"

    sget-object v5, Lcom/sec/chaton/calllog/manager/model/d;->a:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/calllog/manager/a/a;->a(Ljava/util/ArrayList;)I

    move-result v3

    .line 93
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;

    iget v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->groupcallkey:I

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/calllog/manager/model/d;->a(ZZ)I

    .line 94
    if-gez v3, :cond_2

    .line 95
    const/4 v2, -0x1

    .line 97
    :cond_2
    return v2

    :cond_3
    move v0, v2

    .line 93
    goto :goto_1
.end method

.method public a(ZZ)I
    .locals 6

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 190
    const-string v0, "autoDeleteOldestCallLog()"

    sget-object v3, Lcom/sec/chaton/calllog/manager/model/d;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/manager/model/d;->c()I

    move-result v4

    .line 193
    add-int/lit16 v0, v4, -0x1f4

    .line 196
    if-lez v0, :cond_5

    .line 198
    if-nez p1, :cond_0

    const/4 v3, 0x1

    if-le v0, v3, :cond_3

    :cond_0
    move v3, v2

    move v0, v2

    .line 200
    :goto_0
    add-int/lit16 v5, v4, -0x1f4

    if-ge v3, v5, :cond_1

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/calllog/manager/a/a;->a(Z)I

    move-result v0

    .line 202
    if-gez v0, :cond_2

    .line 214
    :cond_1
    :goto_1
    if-gez v0, :cond_4

    move v0, v1

    .line 217
    :goto_2
    return v0

    .line 200
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 207
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/calllog/manager/a/a;->a(Z)I

    move-result v0

    .line 208
    if-gez v0, :cond_1

    move v0, v1

    .line 209
    goto :goto_2

    :cond_4
    move v0, v2

    .line 217
    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public a(IZ)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467
    const/4 v0, 0x0

    .line 468
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1, p1}, Lcom/sec/chaton/calllog/manager/a/a;->a(I)Landroid/database/Cursor;

    move-result-object v1

    .line 470
    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0, v1, v2, p2}, Lcom/sec/chaton/calllog/manager/model/d;->a(Landroid/database/Cursor;IZ)Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 474
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 476
    return-object v0

    .line 471
    :catch_0
    move-exception v2

    .line 472
    :try_start_1
    invoke-virtual {v2}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 474
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(JZ)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 316
    const/4 v0, 0x0

    .line 317
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1, p1, p2}, Lcom/sec/chaton/calllog/manager/a/a;->a(J)Landroid/database/Cursor;

    move-result-object v1

    .line 319
    const/4 v2, 0x2

    :try_start_0
    invoke-direct {p0, v1, v2, p3}, Lcom/sec/chaton/calllog/manager/model/d;->a(Landroid/database/Cursor;IZ)Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 323
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 325
    return-object v0

    .line 320
    :catch_0
    move-exception v2

    .line 321
    :try_start_1
    invoke-virtual {v2}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 323
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(Lcom/sec/chaton/calllog/manager/model/CallLogDBData;Z)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/calllog/manager/model/CallLogDBData;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    const/4 v0, 0x0

    .line 230
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {p1}, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->a()Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/calllog/manager/a/a;->b(Landroid/content/ContentValues;)Landroid/database/Cursor;

    move-result-object v1

    .line 232
    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0, v1, v2, p2}, Lcom/sec/chaton/calllog/manager/model/d;->a(Landroid/database/Cursor;IZ)Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 236
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 238
    return-object v0

    .line 233
    :catch_0
    move-exception v2

    .line 234
    :try_start_1
    invoke-virtual {v2}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 236
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 363
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 364
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1, p1, p2}, Lcom/sec/chaton/calllog/manager/a/a;->a(Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v2

    .line 365
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-object v0

    .line 370
    :cond_1
    const/4 v1, 0x0

    .line 371
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_3

    .line 383
    :cond_2
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 376
    :cond_3
    :try_start_1
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_3

    goto :goto_1

    .line 378
    :catch_0
    move-exception v1

    .line 379
    :try_start_2
    sget-boolean v3, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v3, :cond_2

    .line 380
    sget-object v3, Lcom/sec/chaton/calllog/manager/model/d;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 383
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247
    const/4 v0, 0x0

    .line 248
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1, p1}, Lcom/sec/chaton/calllog/manager/a/a;->b(Ljava/util/ArrayList;)Landroid/database/Cursor;

    move-result-object v1

    .line 250
    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, v1, v2, p2}, Lcom/sec/chaton/calllog/manager/model/d;->a(Landroid/database/Cursor;IZ)Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 254
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 256
    return-object v0

    .line 251
    :catch_0
    move-exception v2

    .line 252
    :try_start_1
    invoke-virtual {v2}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 254
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(Z)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 481
    const/4 v0, 0x0

    .line 482
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/a/a;->g()Landroid/database/Cursor;

    move-result-object v1

    .line 484
    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, v1, v2, p1}, Lcom/sec/chaton/calllog/manager/model/d;->a(Landroid/database/Cursor;IZ)Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 488
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 490
    return-object v0

    .line 485
    :catch_0
    move-exception v2

    .line 486
    :try_start_1
    invoke-virtual {v2}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 488
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a()Ljava/util/HashMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 262
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 263
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 264
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/a/a;->c()Landroid/database/Cursor;

    move-result-object v6

    .line 270
    if-eqz v6, :cond_5

    .line 271
    :goto_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 272
    const-string v1, "buddy_no"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 273
    const-string v1, "group_name"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 274
    const-string v7, "_id"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 277
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 278
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    :cond_0
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 283
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 284
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 304
    :catch_0
    move-exception v1

    .line 305
    :try_start_1
    invoke-virtual {v1}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 307
    if-eqz v6, :cond_1

    .line 308
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 311
    :cond_1
    return-object v3

    .line 287
    :cond_2
    :try_start_2
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 288
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 289
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 307
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 308
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 307
    :cond_3
    throw v1

    .line 294
    :cond_4
    :try_start_3
    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 295
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 297
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 298
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 299
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Set;

    move-object v2, v0

    .line 300
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 301
    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 307
    :cond_5
    if-eqz v6, :cond_1

    goto :goto_1
.end method

.method public a(I)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 509
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/manager/a/a;->b(I)Landroid/database/Cursor;

    move-result-object v2

    .line 513
    if-eqz v2, :cond_2

    .line 515
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 516
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 519
    :cond_0
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 520
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 522
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 528
    :cond_2
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 531
    return-object v1

    .line 525
    :catch_0
    move-exception v0

    .line 526
    :try_start_1
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 528
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public b()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 344
    .line 345
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/a/a;->d()Landroid/database/Cursor;

    move-result-object v2

    .line 347
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 349
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 355
    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 357
    return v0

    .line 352
    :catch_0
    move-exception v1

    .line 353
    :try_start_1
    invoke-virtual {v1}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 355
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public b(Ljava/util/ArrayList;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 137
    const-string v0, "deleteCallLog()"

    sget-object v1, Lcom/sec/chaton/calllog/manager/model/d;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 138
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 140
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 142
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 143
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/chaton/calllog/manager/model/d;->a(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 144
    iget-object v6, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/manager/model/CallLogData;->a(I)Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/sec/chaton/calllog/manager/a/a;->a(Landroid/content/ContentValues;)I

    move-result v1

    .line 145
    if-gez v1, :cond_0

    move v3, v4

    .line 163
    :cond_1
    :goto_1
    return v3

    .line 150
    :cond_2
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 152
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 155
    iget-object v7, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/manager/model/CallLogData;->a(I)Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/chaton/calllog/manager/a/a;->a(Landroid/content/ContentValues;)I

    move-result v1

    .line 156
    if-gez v1, :cond_4

    move v3, v4

    .line 157
    goto :goto_1

    .line 138
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0
.end method

.method public b(Z)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 495
    const/4 v0, 0x0

    .line 496
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/a/a;->h()Landroid/database/Cursor;

    move-result-object v1

    .line 498
    const/4 v2, 0x2

    :try_start_0
    invoke-direct {p0, v1, v2, p1}, Lcom/sec/chaton/calllog/manager/model/d;->a(Landroid/database/Cursor;IZ)Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 502
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 504
    return-object v0

    .line 499
    :catch_0
    move-exception v2

    .line 500
    :try_start_1
    invoke-virtual {v2}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 502
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public c()I
    .locals 2

    .prologue
    .line 424
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/manager/model/d;->d()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/manager/model/d;->e()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public d()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 429
    .line 430
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/a/a;->e()Landroid/database/Cursor;

    move-result-object v2

    .line 432
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 434
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 441
    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 443
    return v0

    .line 438
    :catch_0
    move-exception v1

    .line 439
    :try_start_1
    invoke-virtual {v1}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 441
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public e()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 448
    .line 449
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/a/a;->f()Landroid/database/Cursor;

    move-result-object v2

    .line 451
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 453
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 460
    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 462
    return v0

    .line 457
    :catch_0
    move-exception v1

    .line 458
    :try_start_1
    invoke-virtual {v1}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 460
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 758
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "date_format"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 759
    if-nez v0, :cond_1

    .line 761
    const-string v0, "yyyy/MM/dd"

    .line 772
    :cond_0
    :goto_0
    return-object v0

    .line 763
    :cond_1
    const-string v1, "MM-dd-yyyy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 764
    const-string v0, "MM/dd/yyyy"

    goto :goto_0

    .line 766
    :cond_2
    const-string v1, "dd-MM-yyyy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 767
    const-string v0, "dd/MM/yyyy"

    goto :goto_0

    .line 769
    :cond_3
    const-string v1, "yyyy-MM-dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 770
    const-string v0, "yyyy/MM/dd"

    goto :goto_0
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/d;->b:Lcom/sec/chaton/calllog/manager/a/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/a/a;->b()V

    .line 594
    return-void
.end method
