.class Lcom/sec/chaton/calllog/view/e;
.super Ljava/lang/Object;
.source "CallLogDetailFragment.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1610
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/e;->c:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    iput-object p2, p0, Lcom/sec/chaton/calllog/view/e;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/sec/chaton/calllog/view/e;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const v2, 0x7f02016a

    .line 1613
    invoke-virtual {p1}, Landroid/view/View;->isHovered()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1614
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/e;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1615
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/e;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1616
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/e;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/e;->c:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1624
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 1625
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/e;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/e;->c:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1627
    :cond_1
    const/4 v0, 0x0

    return v0

    .line 1618
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/e;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/e;->c:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020169

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1621
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/e;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/e;->c:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
