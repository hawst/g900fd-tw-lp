.class public final enum Lcom/sec/chaton/calllog/view/layout/a;
.super Ljava/lang/Enum;
.source "CallLogTitleBar.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/calllog/view/layout/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/calllog/view/layout/a;

.field public static final enum b:Lcom/sec/chaton/calllog/view/layout/a;

.field public static final enum c:Lcom/sec/chaton/calllog/view/layout/a;

.field public static final enum d:Lcom/sec/chaton/calllog/view/layout/a;

.field public static final enum e:Lcom/sec/chaton/calllog/view/layout/a;

.field private static final synthetic f:[Lcom/sec/chaton/calllog/view/layout/a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/sec/chaton/calllog/view/layout/a;

    const-string v1, "Green"

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/calllog/view/layout/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/calllog/view/layout/a;->a:Lcom/sec/chaton/calllog/view/layout/a;

    .line 31
    new-instance v0, Lcom/sec/chaton/calllog/view/layout/a;

    const-string v1, "Orange"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/calllog/view/layout/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/calllog/view/layout/a;->b:Lcom/sec/chaton/calllog/view/layout/a;

    .line 32
    new-instance v0, Lcom/sec/chaton/calllog/view/layout/a;

    const-string v1, "Blue"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/calllog/view/layout/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/calllog/view/layout/a;->c:Lcom/sec/chaton/calllog/view/layout/a;

    .line 33
    new-instance v0, Lcom/sec/chaton/calllog/view/layout/a;

    const-string v1, "Gray"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/calllog/view/layout/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/calllog/view/layout/a;->d:Lcom/sec/chaton/calllog/view/layout/a;

    .line 34
    new-instance v0, Lcom/sec/chaton/calllog/view/layout/a;

    const-string v1, "Violet"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/calllog/view/layout/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/calllog/view/layout/a;->e:Lcom/sec/chaton/calllog/view/layout/a;

    .line 29
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/chaton/calllog/view/layout/a;

    sget-object v1, Lcom/sec/chaton/calllog/view/layout/a;->a:Lcom/sec/chaton/calllog/view/layout/a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/chaton/calllog/view/layout/a;->b:Lcom/sec/chaton/calllog/view/layout/a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/calllog/view/layout/a;->c:Lcom/sec/chaton/calllog/view/layout/a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/calllog/view/layout/a;->d:Lcom/sec/chaton/calllog/view/layout/a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/calllog/view/layout/a;->e:Lcom/sec/chaton/calllog/view/layout/a;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/calllog/view/layout/a;->f:[Lcom/sec/chaton/calllog/view/layout/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/calllog/view/layout/a;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/chaton/calllog/view/layout/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/layout/a;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/calllog/view/layout/a;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/chaton/calllog/view/layout/a;->f:[Lcom/sec/chaton/calllog/view/layout/a;

    invoke-virtual {v0}, [Lcom/sec/chaton/calllog/view/layout/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/calllog/view/layout/a;

    return-object v0
.end method
