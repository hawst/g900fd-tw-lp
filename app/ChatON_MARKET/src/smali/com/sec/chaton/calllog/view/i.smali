.class public Lcom/sec/chaton/calllog/view/i;
.super Landroid/os/Handler;
.source "CallLogDetailFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V
    .locals 1

    .prologue
    .line 912
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/i;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 914
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/i;->b:Z

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 920
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/i;->b:Z

    if-eqz v0, :cond_1

    .line 949
    :cond_0
    :goto_0
    return-void

    .line 925
    :cond_1
    invoke-static {}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b()Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/i;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/i;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v2}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Lcom/sec/chaton/calllog/manager/model/CallLogData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->setParent(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Lcom/sec/chaton/calllog/manager/model/CallLogData;)V

    .line 926
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/i;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->c(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    .line 927
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/i;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    .line 928
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/i;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->e(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    .line 932
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x1f42

    if-eq v0, v1, :cond_0

    .line 936
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_2

    .line 938
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/i;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    const-string v1, "EVENT_UPDATE_GROUP_INFO"

    invoke-static {v0, v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 942
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x1f44

    if-ne v0, v1, :cond_0

    .line 944
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/i;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    const-string v1, "CallLogDetailActivity--handleMessage()?EVENT_CALLLOG_ADD"

    invoke-static {v0, v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Ljava/lang/String;)V

    .line 946
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    goto :goto_0
.end method
