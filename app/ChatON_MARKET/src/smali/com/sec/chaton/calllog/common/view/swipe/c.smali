.class Lcom/sec/chaton/calllog/common/view/swipe/c;
.super Ljava/lang/Object;
.source "SwipeLayout.java"

# interfaces
.implements Lcom/sec/chaton/calllog/common/view/swipe/b;


# instance fields
.field final synthetic a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)V
    .locals 1

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/chaton/calllog/common/view/swipe/c;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/c;->b:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;Lcom/sec/chaton/calllog/common/view/swipe/a;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/c;-><init>(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    .line 121
    iget-boolean v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/c;->b:Z

    if-eqz v1, :cond_1

    .line 143
    :cond_0
    :goto_0
    return v0

    .line 127
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/c;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-static {v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)I

    move-result v1

    if-eq v1, v4, :cond_2

    .line 129
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/c;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-static {v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;I)I

    .line 131
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/c;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-static {v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->b(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)Lcom/sec/chaton/calllog/common/view/swipe/d;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v4, v2, v3}, Lcom/sec/chaton/calllog/common/view/swipe/d;->sendEmptyMessageDelayed(IJ)Z

    .line 133
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/c;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-static {v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->c(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)Lcom/sec/chaton/calllog/common/view/swipe/e;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/c;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-static {v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->c(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)Lcom/sec/chaton/calllog/common/view/swipe/e;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/sec/chaton/calllog/common/view/swipe/e;->a(Lcom/sec/chaton/calllog/common/view/swipe/b;)Z

    goto :goto_0

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/c;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
