.class public Lcom/sec/chaton/calllog/manager/model/CallLogData;
.super Ljava/lang/Object;
.source "CallLogData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static final CLASSNAME:Ljava/lang/String; = "[CallLogData]"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final DONT_CARE:I = -0x1

.field public static final METHOD_CONFERENCE_VIDEO:I = 0xb

.field public static final METHOD_CONFERENCE_VOICE:I = 0xa

.field public static final METHOD_P2P_VIDEO:I = 0x7

.field public static final METHOD_P2P_VOICE:I = 0x6

.field public static final METHOD_SIP_VIDEO:I = 0x9

.field public static final METHOD_SIP_VOICE:I = 0x8

.field public static final REJECT_MESSAGE:I = 0xc

.field public static final TAG:Ljava/lang/String;

.field public static final TYPE_BLOCKED:I = 0x4

.field public static final TYPE_INCOMING:I = 0x0

.field public static final TYPE_MISSED:I = 0x2

.field public static final TYPE_OUTGOING:I = 0x1

.field public static final TYPE_PARTICIPANT:I = 0x5

.field public static final TYPE_REJECTED:I = 0x3

.field public static final UPDATE_DATA_DIFERENCE_CALL:I = 0x1

.field public static final UPDATE_DATA_DUP_SINGLE_CALL:I = 0x2

.field public static final UPDATE_DATA_DUP_SINGLE_CALL_OTHER_DAY:I = 0x6

.field public static final UPDATE_DATA_NEW_CONF_CALL:I = 0x4

.field public static final UPDATE_DATA_SAME_CONF_CALL:I = 0x3

.field public static final UPDATE_DATA_SKIP:I = 0x5


# instance fields
.field public _id:I

.field public callmethod:I

.field public groupcallkey:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public groupid:J

.field public myCalllogtype:I

.field public myDuration:I

.field public rejectmsg:Ljava/lang/String;

.field public totalIDList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public userInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->TAG:Ljava/lang/String;

    .line 806
    new-instance v0, Lcom/sec/chaton/calllog/manager/model/b;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/manager/model/b;-><init>()V

    sput-object v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->_id:I

    .line 47
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    .line 48
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myCalllogtype:I

    .line 49
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myDuration:I

    .line 50
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    .line 51
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    .line 52
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    .line 53
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    .line 54
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupid:J

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->_id:I

    .line 47
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    .line 48
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myCalllogtype:I

    .line 49
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myDuration:I

    .line 50
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    .line 51
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    .line 52
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    .line 53
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    .line 54
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupid:J

    .line 113
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/manager/model/CallLogData;->a(Landroid/database/Cursor;)V

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->_id:I

    .line 47
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    .line 48
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myCalllogtype:I

    .line 49
    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myDuration:I

    .line 50
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    .line 51
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    .line 52
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    .line 53
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    .line 54
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupid:J

    .line 821
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/manager/model/CallLogData;->a(Landroid/os/Parcel;)V

    .line 822
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 16

    .prologue
    .line 121
    if-eqz p1, :cond_1

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    if-gez v1, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    if-eqz p1, :cond_0

    .line 128
    const-string v1, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_2

    .line 129
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->_id:I

    .line 130
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    .line 131
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 132
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->_id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    :cond_2
    const-string v1, "callmethod"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_3

    .line 136
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    .line 138
    :cond_3
    const-string v1, "calllogtype"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_4

    .line 139
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myCalllogtype:I

    .line 143
    :cond_4
    const/4 v3, 0x0

    .line 144
    const-string v1, "userid"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_5

    .line 145
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 148
    :cond_5
    const/4 v4, 0x0

    .line 149
    const-string v1, "username"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_6

    .line 150
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 153
    :cond_6
    const/4 v5, 0x0

    .line 154
    const-string v1, "buddy_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_7

    .line 155
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 158
    :cond_7
    const/4 v6, 0x0

    .line 159
    const-string v1, "buddy_status_message"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_8

    .line 160
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 163
    :cond_8
    const/4 v7, 0x0

    .line 164
    const-string v1, "buddy_orginal_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_9

    .line 165
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 168
    :cond_9
    const/4 v8, -0x1

    .line 169
    const-string v1, "buddy_raw_contact_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_a

    .line 170
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 173
    :cond_a
    const/4 v9, -0x1

    .line 174
    const-string v1, "buddy_show_phone_number"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_b

    .line 175
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 178
    :cond_b
    const/4 v10, 0x0

    .line 179
    const-string v1, "buddy_extra_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_c

    .line 180
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 183
    :cond_c
    const/4 v11, 0x0

    .line 184
    const-string v1, "buddy_msisdns"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_d

    .line 185
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 188
    :cond_d
    const/4 v12, 0x0

    .line 189
    const-string v1, "calldate"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_e

    .line 190
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 193
    :cond_e
    const/4 v13, -0x1

    .line 194
    const-string v1, "duration"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_f

    .line 195
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 196
    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myDuration:I

    .line 199
    :cond_f
    const/4 v14, -0x1

    .line 200
    const-string v1, "calllogtype"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_10

    .line 201
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 204
    :cond_10
    if-eqz v3, :cond_11

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    .line 205
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    .line 206
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    new-instance v1, Lcom/sec/chaton/calllog/manager/model/c;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/sec/chaton/calllog/manager/model/c;-><init>(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    :cond_11
    const-string v1, "groupcallkey"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_12

    .line 213
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_14

    .line 215
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    :cond_12
    :goto_1
    const-string v1, "rejectmsg"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_13

    .line 225
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    .line 227
    :cond_13
    const-string v1, "groupid"

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 228
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupid:J

    goto/16 :goto_0

    .line 220
    :cond_14
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    goto :goto_1
.end method

.method private a(Landroid/os/Parcel;)V
    .locals 17

    .prologue
    .line 663
    .line 665
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->_id:I

    .line 666
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    .line 667
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myCalllogtype:I

    .line 668
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupid:J

    .line 670
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 671
    if-nez v1, :cond_3

    .line 672
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    .line 678
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 679
    if-nez v1, :cond_4

    .line 680
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    .line 769
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 770
    if-nez v1, :cond_d

    .line 771
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    .line 782
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 783
    if-nez v1, :cond_e

    .line 784
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    .line 803
    :cond_2
    return-void

    .line 675
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    goto :goto_0

    .line 684
    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    move v15, v1

    .line 685
    :goto_1
    if-lez v15, :cond_0

    .line 693
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 694
    if-nez v1, :cond_5

    .line 695
    const/4 v3, 0x0

    .line 701
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 702
    if-nez v1, :cond_6

    .line 703
    const/4 v4, 0x0

    .line 709
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 710
    if-nez v1, :cond_7

    .line 711
    const/4 v5, 0x0

    .line 717
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 718
    if-nez v1, :cond_8

    .line 719
    const/4 v6, 0x0

    .line 725
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 726
    if-nez v1, :cond_9

    .line 727
    const/4 v7, 0x0

    .line 733
    :goto_6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 734
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 736
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 737
    if-nez v1, :cond_a

    .line 738
    const/4 v10, 0x0

    .line 744
    :goto_7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 745
    if-nez v1, :cond_b

    .line 746
    const/4 v11, 0x0

    .line 752
    :goto_8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 753
    if-nez v1, :cond_c

    .line 754
    const/4 v12, 0x0

    .line 760
    :goto_9
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v13

    .line 761
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    .line 763
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v1, Lcom/sec/chaton/calllog/manager/model/c;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/sec/chaton/calllog/manager/model/c;-><init>(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 685
    add-int/lit8 v1, v15, -0x1

    move v15, v1

    goto :goto_1

    .line 698
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 706
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 714
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 722
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    goto :goto_5

    .line 730
    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    goto :goto_6

    .line 741
    :cond_a
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    goto :goto_7

    .line 749
    :cond_b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    goto :goto_8

    .line 757
    :cond_c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    goto :goto_9

    .line 775
    :cond_d
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    .line 776
    :goto_a
    if-lez v1, :cond_1

    .line 778
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 776
    add-int/lit8 v1, v1, -0x1

    goto :goto_a

    .line 788
    :cond_e
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    move v2, v1

    .line 789
    :goto_b
    if-lez v2, :cond_2

    .line 791
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 792
    if-eqz v1, :cond_10

    .line 794
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 795
    :goto_c
    if-lez v1, :cond_f

    .line 797
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 795
    add-int/lit8 v1, v1, -0x1

    goto :goto_c

    .line 799
    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 789
    :cond_10
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_b
.end method


# virtual methods
.method public a(Landroid/database/Cursor;I)I
    .locals 17

    .prologue
    .line 261
    new-instance v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Lcom/sec/chaton/calllog/manager/model/CallLogData;-><init>(Landroid/database/Cursor;)V

    .line 266
    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 267
    :cond_0
    const/4 v1, 0x5

    .line 347
    :goto_0
    return v1

    .line 269
    :cond_1
    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 270
    :cond_2
    const/4 v1, 0x5

    goto :goto_0

    .line 274
    :cond_3
    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    if-nez v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    if-eqz v1, :cond_5

    :cond_4
    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-nez v1, :cond_5

    .line 275
    const/4 v1, 0x1

    goto :goto_0

    .line 278
    :cond_5
    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-nez v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-nez v1, :cond_b

    .line 282
    const/4 v1, 0x2

    move/from16 v0, p2

    if-ne v0, v1, :cond_6

    .line 283
    const/4 v1, 0x1

    goto :goto_0

    .line 285
    :cond_6
    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v2, v1, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v2, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    if-eq v2, v1, :cond_8

    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    if-nez v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_8

    :cond_7
    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    if-nez v1, :cond_e

    .line 291
    :cond_8
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v3, v1, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/sec/chaton/calllog/manager/b/a;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 295
    const/4 v1, 0x6

    goto/16 :goto_0

    .line 297
    :cond_9
    const/4 v1, 0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_a

    .line 299
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 300
    iget v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->_id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    :cond_a
    const/4 v1, 0x2

    goto/16 :goto_0

    .line 307
    :cond_b
    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-eqz v1, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-eqz v1, :cond_e

    .line 310
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 311
    iget-object v1, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 312
    if-ne v2, v1, :cond_d

    .line 317
    if-nez p2, :cond_c

    .line 319
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 322
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v3, v2, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v4, v2, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v5, v2, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v6, v2, Lcom/sec/chaton/calllog/manager/model/c;->d:Ljava/lang/String;

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v7, v2, Lcom/sec/chaton/calllog/manager/model/c;->e:Ljava/lang/String;

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget v8, v2, Lcom/sec/chaton/calllog/manager/model/c;->f:I

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v9, 0x0

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget v9, v2, Lcom/sec/chaton/calllog/manager/model/c;->g:I

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v10, 0x0

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v10, v2, Lcom/sec/chaton/calllog/manager/model/c;->h:Ljava/lang/String;

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v11, 0x0

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v11, v2, Lcom/sec/chaton/calllog/manager/model/c;->i:Ljava/lang/String;

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v12, 0x0

    invoke-interface {v2, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v12, v2, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v13, 0x0

    invoke-interface {v2, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget v13, v2, Lcom/sec/chaton/calllog/manager/model/c;->k:I

    iget-object v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v14, 0x0

    invoke-interface {v2, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget v14, v2, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/sec/chaton/calllog/manager/model/c;-><init>(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget v2, v15, Lcom/sec/chaton/calllog/manager/model/CallLogData;->_id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    const/4 v1, 0x3

    goto/16 :goto_0

    .line 343
    :cond_d
    const/4 v1, 0x4

    goto/16 :goto_0

    .line 347
    :cond_e
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public a(I)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 238
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 241
    const-string v1, "_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 242
    return-object v0
.end method

.method public a()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 425
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 428
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 430
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 434
    :cond_1
    return-object v1
.end method

.method public a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Z
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 528
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 534
    iget v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->_id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 535
    iget v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 536
    iget v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myCalllogtype:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 537
    iget-wide v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupid:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 539
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 540
    :cond_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 547
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 548
    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 627
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 628
    :cond_3
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 638
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 639
    :cond_5
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 659
    :cond_6
    return-void

    .line 543
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 544
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 552
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 553
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    .line 555
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 556
    :cond_9
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 563
    :goto_2
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 564
    :cond_a
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 571
    :goto_3
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 572
    :cond_b
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 579
    :goto_4
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->d:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->d:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 580
    :cond_c
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 587
    :goto_5
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->e:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 588
    :cond_d
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 595
    :goto_6
    iget v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->f:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 596
    iget v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->g:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 598
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->h:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->h:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 599
    :cond_e
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 606
    :goto_7
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->i:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 607
    :cond_f
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 614
    :goto_8
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 615
    :cond_10
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 622
    :goto_9
    iget v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->k:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 623
    iget v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_1

    .line 559
    :cond_11
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 560
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 567
    :cond_12
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 568
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 575
    :cond_13
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 576
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 583
    :cond_14
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->d:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 584
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 591
    :cond_15
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 592
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->e:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 602
    :cond_16
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->h:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 603
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->h:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 610
    :cond_17
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 611
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->i:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 618
    :cond_18
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 619
    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_9

    .line 632
    :cond_19
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 633
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 634
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_a

    .line 643
    :cond_1a
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 644
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 646
    if-eqz v0, :cond_1c

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 647
    :cond_1c
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_b

    .line 651
    :cond_1d
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 652
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 654
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_c
.end method
