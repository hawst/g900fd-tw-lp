.class public Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ButtonTwoDialog.java"


# static fields
.field private static a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 16
    sget v0, Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;->a:I

    return v0
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;-><init>()V

    .line 35
    sput p0, Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;->a:I

    .line 37
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 39
    const-string v2, "title"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v2, "msg"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;->setArguments(Landroid/os/Bundle;)V

    .line 45
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "msg"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b035a

    new-instance v2, Lcom/sec/chaton/calllog/common/view/layout/b;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/common/view/layout/b;-><init>(Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b003a

    new-instance v2, Lcom/sec/chaton/calllog/common/view/layout/a;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/common/view/layout/a;-><init>(Lcom/sec/chaton/calllog/common/view/layout/ButtonTwoDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 135
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 137
    return-void
.end method
