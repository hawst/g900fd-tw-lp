.class public Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;
.super Landroid/widget/RelativeLayout;
.source "CallLogTitleBar.java"


# instance fields
.field a:Landroid/widget/TextView;

.field b:Landroid/widget/TextView;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->a(Landroid/content/Context;Lcom/sec/chaton/calllog/view/layout/a;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    sget-object v0, Lcom/sec/chaton/calllog/view/layout/a;->a:Lcom/sec/chaton/calllog/view/layout/a;

    invoke-direct {p0, p1, v0}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->a(Landroid/content/Context;Lcom/sec/chaton/calllog/view/layout/a;)V

    .line 69
    return-void
.end method

.method private final a(Landroid/content/Context;Lcom/sec/chaton/calllog/view/layout/a;)V
    .locals 2

    .prologue
    .line 80
    const v0, 0x7f030088

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 82
    const v0, 0x7f07014c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->a:Landroid/widget/TextView;

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0365

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->c:Ljava/lang/String;

    .line 86
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0366

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->d:Ljava/lang/String;

    .line 88
    return-void
.end method


# virtual methods
.method public setTag(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :cond_0
    return-void
.end method
