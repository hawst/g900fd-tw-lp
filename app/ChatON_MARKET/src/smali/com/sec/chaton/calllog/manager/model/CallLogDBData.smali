.class public Lcom/sec/chaton/calllog/manager/model/CallLogDBData;
.super Ljava/lang/Object;
.source "CallLogDBData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public _id:I

.field public calldate:Ljava/lang/String;

.field public calllogtype:I

.field public callmethod:I

.field public duration:I

.field public groupcallkey:I

.field public rejectmsg:Ljava/lang/String;

.field public userid:Ljava/lang/String;

.field public username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->TAG:Ljava/lang/String;

    .line 284
    new-instance v0, Lcom/sec/chaton/calllog/manager/model/a;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/manager/model/a;-><init>()V

    sput-object v0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->_id:I

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->duration:I

    .line 36
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    .line 37
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->callmethod:I

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    .line 43
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->groupcallkey:I

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->_id:I

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->duration:I

    .line 36
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    .line 37
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->callmethod:I

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    .line 43
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->groupcallkey:I

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    .line 299
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->a(Landroid/os/Parcel;)V

    .line 300
    return-void
.end method

.method public constructor <init>(Lcom/coolots/sso/calllog/ChatONCallLogData;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->_id:I

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->duration:I

    .line 36
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    .line 37
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->callmethod:I

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    .line 43
    iput v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->groupcallkey:I

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Lcom/coolots/sso/calllog/ChatONCallLogData;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    .line 58
    invoke-virtual {p1}, Lcom/coolots/sso/calllog/ChatONCallLogData;->b()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->duration:I

    .line 59
    invoke-virtual {p1}, Lcom/coolots/sso/calllog/ChatONCallLogData;->c()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    .line 60
    invoke-virtual {p1}, Lcom/coolots/sso/calllog/ChatONCallLogData;->d()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->callmethod:I

    .line 61
    invoke-virtual {p1}, Lcom/coolots/sso/calllog/ChatONCallLogData;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    .line 62
    invoke-virtual {p1}, Lcom/coolots/sso/calllog/ChatONCallLogData;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Lcom/coolots/sso/calllog/ChatONCallLogData;->g()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->groupcallkey:I

    .line 64
    invoke-virtual {p1}, Lcom/coolots/sso/calllog/ChatONCallLogData;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    .line 65
    return-void
.end method

.method private a(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 242
    .line 244
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 245
    if-nez v0, :cond_0

    .line 246
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    .line 252
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->duration:I

    .line 253
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    .line 254
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->callmethod:I

    .line 256
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 257
    if-nez v0, :cond_1

    .line 258
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    .line 264
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 265
    if-nez v0, :cond_2

    .line 266
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    .line 272
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->groupcallkey:I

    .line 274
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 275
    if-nez v0, :cond_3

    .line 276
    iput-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    .line 281
    :goto_3
    return-void

    .line 249
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    goto :goto_0

    .line 261
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    goto :goto_1

    .line 269
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    goto :goto_2

    .line 279
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    goto :goto_3
.end method


# virtual methods
.method public a()Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 125
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 128
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 129
    const-string v1, "calldate"

    iget-object v2, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_0
    iget v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->duration:I

    if-lez v1, :cond_1

    .line 132
    const-string v1, "duration"

    iget v2, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->duration:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 134
    :cond_1
    iget v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    if-ltz v1, :cond_2

    .line 135
    const-string v1, "calllogtype"

    iget v2, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 137
    :cond_2
    iget v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->callmethod:I

    if-ltz v1, :cond_3

    .line 138
    const-string v1, "callmethod"

    iget v2, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->callmethod:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 141
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 142
    const-string v1, "username"

    iget-object v2, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 145
    const-string v1, "userid"

    iget-object v2, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_5
    iget v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->groupcallkey:I

    if-ltz v1, :cond_6

    .line 150
    const-string v1, "groupcallkey"

    iget v2, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->groupcallkey:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 153
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 154
    const-string v1, "rejectmsg"

    iget-object v2, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_7
    return-object v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    :cond_0
    const/4 v0, 0x0

    .line 185
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 202
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 209
    :goto_0
    iget v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->duration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 210
    iget v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calllogtype:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 211
    iget v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->callmethod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 214
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 221
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 222
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 229
    :goto_2
    iget v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->groupcallkey:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 232
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 238
    :goto_3
    return-void

    .line 205
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 206
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->calldate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 217
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->username:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 225
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->userid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 235
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->rejectmsg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3
.end method
