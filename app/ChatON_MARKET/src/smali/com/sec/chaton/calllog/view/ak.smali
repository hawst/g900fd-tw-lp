.class public Lcom/sec/chaton/calllog/view/ak;
.super Landroid/widget/ArrayAdapter;
.source "ShowConferenceMemberListFragment.java"


# instance fields
.field public a:Lcom/sec/chaton/calllog/manager/model/CallLogData;

.field private b:Landroid/content/Context;

.field private c:I

.field private d:Lcom/sec/chaton/calllog/view/ah;

.field private e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/chaton/calllog/manager/model/CallLogData;Lcom/sec/chaton/calllog/view/ah;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 450
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 440
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ak;->b:Landroid/content/Context;

    .line 446
    iput-boolean v1, p0, Lcom/sec/chaton/calllog/view/ak;->e:Z

    .line 451
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/ak;->b:Landroid/content/Context;

    .line 452
    iput p2, p0, Lcom/sec/chaton/calllog/view/ak;->c:I

    .line 453
    iput-object p3, p0, Lcom/sec/chaton/calllog/view/ak;->a:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 454
    iput-object p4, p0, Lcom/sec/chaton/calllog/view/ak;->d:Lcom/sec/chaton/calllog/view/ah;

    .line 455
    iput-boolean v1, p0, Lcom/sec/chaton/calllog/view/ak;->e:Z

    .line 456
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 459
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 440
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ak;->b:Landroid/content/Context;

    .line 446
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/ak;->e:Z

    .line 460
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/ak;->b:Landroid/content/Context;

    .line 461
    iput p2, p0, Lcom/sec/chaton/calllog/view/ak;->c:I

    .line 462
    iput-object p3, p0, Lcom/sec/chaton/calllog/view/ak;->a:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 464
    iput-object p4, p0, Lcom/sec/chaton/calllog/view/ak;->f:Ljava/util/List;

    .line 465
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/ak;->e:Z

    .line 466
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/ak;)Lcom/sec/chaton/calllog/view/ah;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ak;->d:Lcom/sec/chaton/calllog/view/ah;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ak;->a:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 481
    .line 483
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ak;->a:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    .line 484
    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Lcom/sec/chaton/calllog/manager/model/c;)I

    move-result v4

    .line 486
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MembersAdapter.getView() position["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], buddyType["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/calllog/view/ShowConferenceMemberList;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    if-eqz p2, :cond_1

    .line 490
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/view/aj;

    move-object v2, v1

    .line 498
    :goto_0
    iget-boolean v1, p0, Lcom/sec/chaton/calllog/view/ak;->e:Z

    if-eqz v1, :cond_4

    .line 500
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_2

    .line 501
    :cond_0
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->b:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 506
    :goto_1
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 507
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 508
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 510
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 511
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ak;->f:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 512
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->e:Landroid/widget/CheckBox;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 551
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ak;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v3, v2, Lcom/sec/chaton/calllog/view/aj;->a:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 568
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 569
    return-object p2

    .line 493
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ak;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/calllog/view/ak;->c:I

    invoke-virtual {v1, v2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 495
    new-instance v3, Lcom/sec/chaton/calllog/view/aj;

    move-object v1, v2

    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {v3, v1}, Lcom/sec/chaton/calllog/view/aj;-><init>(Landroid/view/ViewGroup;)V

    move-object p2, v2

    move-object v2, v3

    goto :goto_0

    .line 503
    :cond_2
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->b:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 514
    :cond_3
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2

    .line 527
    :cond_4
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 529
    const/4 v1, 0x4

    if-eq v4, v1, :cond_5

    const/4 v1, 0x2

    if-ne v4, v1, :cond_6

    .line 531
    :cond_5
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->b:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 533
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 534
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->d:Landroid/widget/ImageView;

    new-instance v3, Lcom/sec/chaton/calllog/view/al;

    invoke-direct {v3, p0, v0}, Lcom/sec/chaton/calllog/view/al;-><init>(Lcom/sec/chaton/calllog/view/ak;Lcom/sec/chaton/calllog/manager/model/c;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 545
    :cond_6
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->b:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 546
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 547
    iget-object v1, v2, Lcom/sec/chaton/calllog/view/aj;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 470
    const/4 v0, 0x1

    return v0
.end method
