.class public Lcom/sec/chaton/calllog/view/layout/List_CallLog;
.super Lcom/sec/chaton/calllog/view/layout/List_Common;
.source "List_CallLog.java"


# instance fields
.field public a:Landroid/os/Handler;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field private final g:I

.field private h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

.field private i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

.field private final j:Landroid/content/Context;

.field private final k:Lcom/sec/chaton/calllog/manager/b/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/sec/chaton/calllog/common/view/swipe/e;)V
    .locals 2

    .prologue
    .line 52
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p3}, Lcom/sec/chaton/calllog/view/layout/List_Common;-><init>(Landroid/content/Context;ZLcom/sec/chaton/calllog/common/view/swipe/e;)V

    .line 46
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->k:Lcom/sec/chaton/calllog/manager/b/a;

    .line 53
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->j:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a:Landroid/os/Handler;

    .line 56
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 57
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 58
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 59
    iget v0, v1, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->g:I

    .line 60
    return-void
.end method

.method private a(FFFI)Landroid/view/animation/TranslateAnimation;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 358
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    move v2, p1

    move v3, v1

    move v4, p2

    move v5, v1

    move v7, v1

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 359
    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 360
    new-instance v1, Lcom/sec/chaton/calllog/view/layout/b;

    invoke-direct {v1, p0, p3, p4}, Lcom/sec/chaton/calllog/view/layout/b;-><init>(Lcom/sec/chaton/calllog/view/layout/List_CallLog;FI)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 380
    return-object v0
.end method

.method private a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 125
    if-eqz p2, :cond_0

    .line 126
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v2, "HH:mm"

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 127
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v3, "HH:mm"

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return-object v1

    .line 130
    :cond_0
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v2, "a h:mm"

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v3, "h:mm a"

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 133
    goto :goto_1
.end method

.method private a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 340
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 341
    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 342
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 343
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/layout/List_CallLog;I)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->b(I)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/calllog/view/layout/List_CallLog;I)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->b(I)V

    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 314
    const-string v0, "kor"

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 273
    packed-switch p1, :pswitch_data_0

    .line 288
    :goto_0
    return-void

    .line 278
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->setVideoEventUI(Z)V

    goto :goto_0

    .line 283
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->setVideoEventUI(Z)V

    goto :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a()V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setVisibility(I)V

    .line 141
    return-void
.end method

.method protected a(F)V
    .locals 4

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->setVisibility(I)V

    .line 393
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-virtual {v2}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b()I

    move-result v2

    int-to-float v2, v2

    sub-float v2, p1, v2

    iget v3, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->g:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    const/4 v3, 0x1

    invoke-direct {p0, v1, v2, p1, v3}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a(FFFI)Landroid/view/animation/TranslateAnimation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->startAnimation(Landroid/view/animation/Animation;)V

    .line 394
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(F)V

    .line 395
    return-void
.end method

.method protected a(I)V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a(Landroid/view/View;I)V

    .line 328
    invoke-super {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a(I)V

    .line 329
    return-void
.end method

.method protected a(II)V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-direct {p0, v0, p2}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a(Landroid/view/View;I)V

    .line 387
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(I)V

    .line 388
    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/graphics/Color;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a(Landroid/content/Context;Landroid/graphics/Color;Landroid/util/AttributeSet;)V

    .line 74
    new-instance v0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-direct {v0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->addView(Landroid/view/View;)V

    .line 77
    new-instance v0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-direct {v0, p1, p3}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->addView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->setVisibility(I)V

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0365

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->b:Ljava/lang/String;

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0366

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->c:Ljava/lang/String;

    .line 83
    return-void
.end method

.method protected a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;ZLjava/util/HashMap;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 179
    iget v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->g:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 180
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->k:Lcom/sec/chaton/calllog/manager/b/a;

    invoke-virtual {v1, p1}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setName(Ljava/lang/String;)V

    .line 187
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setProfileImage(ZLjava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-virtual {v0, p5}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setChecked(Z)V

    .line 244
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/chaton/calllog/manager/b/a;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setMsg(Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setDeleteMode(Z)V

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    iget-object v1, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setCallCount(I)V

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setTime(Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setCallType(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    iget v1, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setCallMethod(I)V

    .line 253
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setVisibility(I)V

    .line 255
    iget v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->c(I)V

    .line 256
    return-void

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->k:Lcom/sec/chaton/calllog/manager/b/a;

    iget-object v1, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-virtual {v0, p1, p4, v1}, Lcom/sec/chaton/calllog/manager/b/a;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/util/HashMap;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 233
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->k:Lcom/sec/chaton/calllog/manager/b/a;

    invoke-virtual {v2, p1}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setName(Ljava/lang/String;)V

    .line 241
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-virtual {v1, p5}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setChecked(Z)V

    .line 242
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setProfileImage(ZLjava/lang/String;)V

    goto :goto_0

    .line 237
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->setName(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected a(Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 156
    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    iget-object v1, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setTitle(Ljava/lang/String;)V

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    iget-object v1, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->tagname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setTag(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setVisibility(I)V

    .line 165
    return-void

    .line 160
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    const-string v2, "/"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "000000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 161
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    invoke-virtual {v2, v0, p2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected b(F)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-virtual {v2}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b()I

    move-result v2

    iget v3, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->g:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    sub-float/2addr v2, p1

    invoke-direct {p0, v1, v2, p1, v4}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a(FFFI)Landroid/view/animation/TranslateAnimation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->startAnimation(Landroid/view/animation/Animation;)V

    .line 401
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b(F)V

    .line 402
    return-void
.end method

.method protected c(F)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 407
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-direct {p0, v0, v3}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a(Landroid/view/View;I)V

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p1, v1, p1, v2}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a(FFFI)Landroid/view/animation/TranslateAnimation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->startAnimation(Landroid/view/animation/Animation;)V

    .line 409
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->setVisibility(I)V

    .line 410
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c(F)V

    .line 411
    return-void
.end method

.method public setView(Lcom/sec/chaton/calllog/manager/model/CallLogData;ZZLjava/lang/String;Ljava/util/HashMap;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 99
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a()V

    .line 101
    instance-of v0, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-eqz v0, :cond_1

    .line 102
    check-cast p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    invoke-virtual {p0, p1, p4}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a(Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0, v6}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->setEnabledSwipe(Z)V

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    instance-of v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    if-eqz v0, :cond_0

    .line 105
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->setEnabledSwipe(Z)V

    .line 106
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;ZLjava/util/HashMap;Z)V

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->i:Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;

    invoke-direct {p0, v0, v6}, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->a(Landroid/view/View;I)V

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_CallLog;->h:Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a()V

    goto :goto_0
.end method
