.class public Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;
.super Landroid/widget/RelativeLayout;
.source "List_TypeDetailGroupCallLog.java"


# instance fields
.field private a:Landroid/widget/RelativeLayout;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->a(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->a(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method private final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    const v0, 0x7f03011c

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    const v0, 0x7f0700ee

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->a:Landroid/widget/RelativeLayout;

    .line 47
    const v0, 0x7f0704ae

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->b:Landroid/widget/ImageView;

    .line 48
    const v0, 0x7f0704ad

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->c:Landroid/widget/ImageView;

    .line 49
    const v0, 0x7f0704af

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->d:Landroid/widget/ImageView;

    .line 50
    const v0, 0x7f0704b1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->e:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f0704ac

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->f:Landroid/widget/TextView;

    .line 52
    const v0, 0x7f0704b0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->g:Landroid/widget/TextView;

    .line 53
    const v0, 0x7f0704aa

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->h:Landroid/widget/CheckBox;

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 57
    return-void
.end method


# virtual methods
.method public final setCallMethod(I)V
    .locals 3

    .prologue
    const v0, 0x7f0200ee

    const/16 v2, 0x8

    .line 72
    .line 74
    packed-switch p1, :pswitch_data_0

    .line 96
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 97
    return-void

    .line 78
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 83
    :pswitch_2
    const v0, 0x7f0200ed

    .line 84
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 88
    :pswitch_3
    const v0, 0x7f0200e8

    .line 89
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->d:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setCallType(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->b:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/sec/chaton/calllog/manager/b/a;->c(Lcom/sec/chaton/calllog/manager/model/CallLogData;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 67
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 160
    return-void
.end method

.method public setCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 181
    return-void
.end method

.method public setDeleteMode(Z)V
    .locals 2

    .prologue
    .line 145
    if-eqz p1, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->h:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 150
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->h:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method public final setDuration(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->g:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 136
    :cond_0
    return-void
.end method

.method public final setName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    :cond_0
    return-void
.end method

.method public final setTime(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, -0x2

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 111
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 112
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 113
    const/16 v1, 0xb

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 114
    const/16 v1, 0x19

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 115
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
