.class public Lcom/sec/chaton/calllog/manager/a;
.super Landroid/os/Handler;
.source "CallLogManager.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/os/Handler;

.field private c:Lcom/sec/chaton/calllog/manager/model/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/calllog/manager/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->b:Landroid/os/Handler;

    .line 22
    new-instance v0, Lcom/sec/chaton/calllog/manager/model/d;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/manager/model/d;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    .line 27
    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogDBData;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 118
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 119
    :cond_0
    const/16 v0, -0x1f42

    .line 131
    :cond_1
    :goto_0
    return v0

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/ArrayList;)I

    move-result v0

    .line 124
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/a;->b:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 126
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 127
    const/16 v2, 0x1b58

    iput v2, v1, Landroid/os/Message;->what:I

    .line 128
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 129
    iget-object v2, p0, Lcom/sec/chaton/calllog/manager/a;->b:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(IZ)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/calllog/manager/model/d;->a(IZ)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a(JZ)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/chaton/calllog/manager/model/d;->a(JZ)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/sec/chaton/calllog/manager/model/CallLogDBData;Z)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/calllog/manager/model/CallLogDBData;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/calllog/manager/model/d;->a(Lcom/sec/chaton/calllog/manager/model/CallLogDBData;Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0, p1, p2}, Lcom/sec/chaton/calllog/manager/model/d;->a(Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 722
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/manager/model/d;->a(Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 696
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/model/d;->a()Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/sec/chaton/calllog/manager/a;->b:Landroid/os/Handler;

    .line 197
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/model/d;->b()I

    move-result v0

    return v0
.end method

.method public b(Ljava/util/ArrayList;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 159
    if-nez p1, :cond_1

    .line 160
    const/16 v0, -0x1f42

    .line 169
    :cond_0
    :goto_0
    return v0

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/manager/model/d;->b(Ljava/util/ArrayList;)I

    move-result v0

    .line 165
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/a;->b:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/a;->b:Landroid/os/Handler;

    const/16 v2, 0x1f40

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public b(Z)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 730
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/manager/model/d;->b(Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/a;->c:Lcom/sec/chaton/calllog/manager/model/d;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/model/d;->c()I

    move-result v0

    return v0
.end method
