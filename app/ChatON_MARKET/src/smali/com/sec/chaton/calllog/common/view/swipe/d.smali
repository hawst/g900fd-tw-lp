.class Lcom/sec/chaton/calllog/common/view/swipe/d;
.super Landroid/os/Handler;
.source "SwipeLayout.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)V
    .locals 1

    .prologue
    .line 384
    iput-object p1, p0, Lcom/sec/chaton/calllog/common/view/swipe/d;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 386
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/d;->b:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;Lcom/sec/chaton/calllog/common/view/swipe/a;)V
    .locals 0

    .prologue
    .line 384
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/d;-><init>(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 391
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/d;->b:Z

    if-eqz v0, :cond_0

    .line 423
    :goto_0
    return-void

    .line 397
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_2

    .line 399
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/d;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->setPressed(Z)V

    .line 421
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 403
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/d;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->setPressed(Z)V

    .line 407
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/d;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->setBackgroundColor(I)V

    goto :goto_1

    .line 409
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/d;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 415
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/d;->a:Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->d(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)V

    goto :goto_1
.end method
