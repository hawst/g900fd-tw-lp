.class public Lcom/sec/chaton/calllog/view/CallLogDetailFragment;
.super Landroid/support/v4/app/Fragment;
.source "CallLogDetailFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;


# instance fields
.field private A:Landroid/view/View;

.field private final B:Lcom/sec/chaton/calllog/manager/a;

.field private final C:Lcom/sec/chaton/calllog/manager/b/a;

.field private D:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation
.end field

.field private E:Lcom/sec/chaton/calllog/view/l;

.field private F:Landroid/view/View;

.field private G:Lcom/coolots/sso/a/a;

.field private H:I

.field private I:Ljava/lang/String;

.field private J:Landroid/app/ProgressDialog;

.field private K:Landroid/widget/Toast;

.field private L:Lcom/sec/chaton/buddy/a/c;

.field private M:Ljava/lang/String;

.field private N:I

.field public a:Lcom/sec/chaton/calllog/view/i;

.field b:Landroid/os/Handler;

.field protected c:Landroid/os/Handler;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageButton;

.field private g:Landroid/widget/ImageButton;

.field private h:Landroid/widget/ImageButton;

.field private i:Landroid/widget/ListView;

.field private j:Landroid/widget/LinearLayout;

.field private l:Landroid/widget/TextView;

.field private m:Ljava/lang/String;

.field private n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

.field private o:Z

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:I

.field private v:I

.field private w:Landroid/widget/ImageButton;

.field private x:Landroid/widget/ImageButton;

.field private y:Landroid/widget/ImageView;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 91
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    .line 106
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m:Ljava/lang/String;

    .line 111
    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->o:Z

    .line 112
    iput v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->p:I

    .line 122
    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->q:Z

    .line 123
    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->r:Z

    .line 124
    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->s:Z

    .line 125
    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->t:Z

    .line 136
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    .line 137
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->C:Lcom/sec/chaton/calllog/manager/b/a;

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->D:Ljava/util/ArrayList;

    .line 140
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->E:Lcom/sec/chaton/calllog/view/l;

    .line 142
    new-instance v0, Lcom/sec/chaton/calllog/view/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/calllog/view/i;-><init>(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a:Lcom/sec/chaton/calllog/view/i;

    .line 149
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->F:Landroid/view/View;

    .line 155
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->G:Lcom/coolots/sso/a/a;

    .line 159
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->J:Landroid/app/ProgressDialog;

    .line 162
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->M:Ljava/lang/String;

    .line 164
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->N:I

    .line 1792
    new-instance v0, Lcom/sec/chaton/calllog/view/f;

    invoke-direct {v0, p0}, Lcom/sec/chaton/calllog/view/f;-><init>(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b:Landroid/os/Handler;

    .line 1872
    new-instance v0, Lcom/sec/chaton/calllog/view/h;

    invoke-direct {v0, p0}, Lcom/sec/chaton/calllog/view/h;-><init>(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->c:Landroid/os/Handler;

    return-void
.end method

.method public static a(IILcom/sec/chaton/calllog/manager/a;)Lcom/sec/chaton/calllog/manager/model/CallLogData;
    .locals 3

    .prologue
    .line 1490
    const/4 v0, 0x0

    .line 1492
    const/4 v1, 0x5

    if-ne p1, v1, :cond_1

    .line 1494
    invoke-static {p2}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Lcom/sec/chaton/calllog/manager/a;)Lcom/sec/chaton/calllog/manager/model/CallLogData;

    move-result-object v0

    .line 1496
    const-string v1, "updateBuddyInfo() with missed call"

    const-class v2, Lcom/sec/chaton/calllog/view/CallLogDetailActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1510
    :cond_0
    :goto_0
    return-object v0

    .line 1500
    :cond_1
    if-ltz p0, :cond_0

    .line 1502
    const/4 v0, 0x1

    invoke-virtual {p2, p1, v0}, Lcom/sec/chaton/calllog/manager/a;->a(IZ)Ljava/util/ArrayList;

    move-result-object v0

    .line 1503
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 1505
    const-string v1, "updateBuddyInfo() normal case"

    const-class v2, Lcom/sec/chaton/calllog/view/CallLogDetailActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Lcom/sec/chaton/calllog/manager/a;)Lcom/sec/chaton/calllog/manager/model/CallLogData;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1137
    invoke-virtual {p0, v3}, Lcom/sec/chaton/calllog/manager/a;->b(Z)Ljava/util/ArrayList;

    move-result-object v1

    .line 1138
    const/4 v0, 0x0

    .line 1140
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1142
    const-string v1, "getMissedCallLog missed_call:CallLogList is Empty"

    const-class v2, Lcom/sec/chaton/calllog/view/CallLogDetailActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    :cond_0
    :goto_0
    return-object v0

    .line 1147
    :cond_1
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 1149
    if-nez v0, :cond_0

    .line 1151
    const-string v1, "getMissedCallLog callLog is NULL"

    const-class v2, Lcom/sec/chaton/calllog/view/CallLogDetailActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/HashMap;Ljava/util/List;Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/c;",
            ">;",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 633
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v0

    invoke-virtual {v0, p2, p0, p1}, Lcom/sec/chaton/calllog/manager/b/a;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/util/HashMap;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 635
    return-object v0
.end method

.method public static a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1449
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1451
    invoke-static {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Lcom/sec/chaton/calllog/manager/model/CallLogData;

    move-result-object v3

    .line 1453
    if-nez v3, :cond_0

    move-object v0, v2

    .line 1460
    :goto_0
    return-object v0

    .line 1457
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, v3, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1458
    iget-object v0, v3, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1457
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 1460
    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1607
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 1632
    :goto_0
    return-void

    .line 1610
    :cond_0
    new-instance v0, Lcom/sec/chaton/calllog/view/e;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/chaton/calllog/view/e;-><init>(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Landroid/view/View;Landroid/view/View;)V

    .line 1631
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->s()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 1783
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->K:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 1785
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->K:Landroid/widget/Toast;

    .line 1788
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->K:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 1789
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->K:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1790
    return-void
.end method

.method private a(Z)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 1522
    if-eqz p1, :cond_1

    .line 1523
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->G:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v3, v3, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v3, v3, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Push Name"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1528
    :goto_0
    if-eqz v0, :cond_0

    .line 1529
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b008a

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1532
    :cond_0
    return-void

    .line 1526
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->G:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v2, v2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v3, v2, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v4, "Push Name"

    const-string v6, ""

    invoke-virtual {v2, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move v2, v8

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1355
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 1356
    const-string v0, "callChatList"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1357
    if-nez p1, :cond_0

    .line 1359
    const-string v2, "receivers"

    new-array v3, v3, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1370
    :goto_0
    if-eqz p2, :cond_1

    .line 1371
    const-string v0, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1379
    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->startActivity(Landroid/content/Intent;)V

    .line 1381
    return-void

    .line 1362
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1366
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1368
    const-string v2, "receivers"

    new-array v3, v4, [Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 1373
    :cond_1
    if-nez p1, :cond_2

    .line 1374
    const-string v0, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 1377
    :cond_2
    const-string v0, "chatType"

    sget-object v2, Lcom/sec/chaton/e/r;->c:Lcom/sec/chaton/e/r;

    invoke-virtual {v2}, Lcom/sec/chaton/e/r;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1
.end method

.method private a(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1090
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 1091
    const-string v0, "updateUIAfterCall"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1092
    iput-boolean v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->o:Z

    .line 1094
    :cond_0
    const-string v0, "searchtype"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->p:I

    .line 1095
    const-string v0, "normal"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1097
    const-string v0, "normal"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    .line 1098
    const-string v0, "calllogdata"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 1099
    const-string v0, "order"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->N:I

    .line 1101
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    if-nez v0, :cond_4

    .line 1102
    const-string v0, "mCallLog is NULL"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    move v0, v1

    .line 1132
    :goto_0
    return v0

    .line 1107
    :cond_1
    const-string v0, "rejected_call"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1109
    const-string v0, "rejected call"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    .line 1110
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/calllog/manager/a;->a(Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 1111
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    if-nez v0, :cond_4

    .line 1113
    const-string v0, "mCallLog is NULL"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    move v0, v1

    .line 1114
    goto :goto_0

    .line 1122
    :cond_2
    const-string v0, "missed_call"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1124
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Lcom/sec/chaton/calllog/manager/a;)Lcom/sec/chaton/calllog/manager/model/CallLogData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 1125
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    if-nez v0, :cond_3

    move v0, v1

    .line 1126
    goto :goto_0

    .line 1128
    :cond_3
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->p:I

    :cond_4
    move v0, v2

    .line 1132
    goto :goto_0
.end method

.method public static b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Lcom/sec/chaton/calllog/manager/model/CallLogData;
    .locals 4

    .prologue
    .line 1465
    const/4 v0, 0x0

    .line 1467
    if-eqz p0, :cond_4

    iget-object v1, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1

    move-object p0, v0

    .line 1485
    :cond_0
    :goto_0
    return-object p0

    .line 1470
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    .line 1471
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_0

    .line 1474
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-le v2, v1, :cond_3

    .line 1476
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v3, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1477
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1474
    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    .line 1471
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    move-object p0, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Lcom/sec/chaton/calllog/manager/model/CallLogData;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    return-object v0
.end method

.method static synthetic b()Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->M:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 88
    const-string v0, "[CallLogDetailActivity]"

    invoke-static {p1, v0}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method private c(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1635
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1636
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1637
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, "|"

    invoke-direct {v1, p1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1639
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1641
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 1642
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 1644
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x13

    if-lt v3, v4, :cond_0

    const-string v3, "10"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1645
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chaton id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CallLogDetailActivity"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655
    :goto_1
    const-string v2, ""

    goto :goto_0

    .line 1647
    :cond_0
    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1648
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pin number : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CallLogDetailActivity"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1651
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "msisdn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CallLogDetailActivity"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1652
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1659
    :cond_2
    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    sput-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    .line 211
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d()V

    .line 212
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->o()V

    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    .line 216
    if-eqz v0, :cond_0

    if-eqz v0, :cond_3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 220
    iput v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->v:I

    .line 221
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->a(I)V

    .line 232
    :cond_1
    :goto_0
    return-void

    .line 224
    :cond_2
    iput v3, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->v:I

    .line 225
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->a(I)V

    goto :goto_0

    .line 229
    :cond_3
    if-eqz v0, :cond_1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->s:Z

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->j()V

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 7

    .prologue
    const-wide/16 v0, 0x0

    const v6, 0x7f0b00d8

    const/4 v5, 0x0

    .line 1697
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 1699
    invoke-virtual {p0, p1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1701
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1702
    const-string v0, "phone"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1703
    const-string v3, "name"

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "phoneNum: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "CallLogDetailActivity"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1706
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NAME: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "CallLogDetailActivity"

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1708
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.INSERT"

    invoke-direct {v0, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1709
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1710
    const-string v1, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1713
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1748
    :goto_0
    return-void

    .line 1714
    :catch_0
    move-exception v0

    .line 1715
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1716
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v6, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1732
    :cond_0
    cmp-long v3, v0, v0

    if-nez v3, :cond_1

    .line 1733
    iget v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->H:I

    int-to-long v0, v0

    .line 1736
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CallLogDetailActivity"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1738
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1739
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1742
    :try_start_1
    invoke-virtual {p0, v3}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1743
    :catch_1
    move-exception v0

    .line 1744
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1745
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v6, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 236
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700ec

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->i:Landroid/widget/ListView;

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->l:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Landroid/view/View;Landroid/view/View;)V

    .line 240
    new-instance v0, Lcom/sec/chaton/calllog/view/l;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-direct {v0, v1}, Lcom/sec/chaton/calllog/view/l;-><init>(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->E:Lcom/sec/chaton/calllog/view/l;

    .line 242
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->f()V

    .line 244
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->i()V

    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 6

    .prologue
    const v5, 0x7f0700de

    const/16 v4, 0xb

    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f070076

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->e:Landroid/widget/ImageView;

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    sput-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700e2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->j:Landroid/widget/LinearLayout;

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700e7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->f:Landroid/widget/ImageButton;

    .line 261
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700e9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->g:Landroid/widget/ImageButton;

    .line 263
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->f:Landroid/widget/ImageButton;

    sget-object v1, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    invoke-virtual {v1, v5}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusUpId(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->g:Landroid/widget/ImageButton;

    sget-object v1, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    invoke-virtual {v1, v5}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusUpId(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700e3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->w:Landroid/widget/ImageButton;

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700eb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->h:Landroid/widget/ImageButton;

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700e5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->x:Landroid/widget/ImageButton;

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700e4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->z:Landroid/view/View;

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700ea

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->A:Landroid/view/View;

    .line 274
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k()V

    .line 275
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->s:Z

    if-eqz v0, :cond_0

    .line 276
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->r:Z

    if-eqz v0, :cond_1

    .line 278
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->a(I)V

    .line 279
    iput v3, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->v:I

    .line 286
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 290
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->j()V

    .line 291
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->g()V

    .line 298
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 342
    return-void

    .line 283
    :cond_1
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->a(I)V

    .line 284
    iput v4, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->v:I

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 353
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->i:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 354
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->s:Z

    if-nez v1, :cond_0

    .line 355
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->I:Ljava/lang/String;

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->A:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 363
    :goto_0
    return-void

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->t()V

    return-void
.end method

.method static synthetic h(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->M:Ljava/lang/String;

    return-object v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->w:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->y:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 508
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->q:Z

    if-eqz v0, :cond_0

    .line 533
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Lcom/sec/chaton/buddy/a/c;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->L:Lcom/sec/chaton/buddy/a/c;

    return-object v0
.end method

.method private i()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 543
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->p()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/calllog/manager/a;->a(Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->D:Ljava/util/ArrayList;

    .line 545
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->D:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/j;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/j;->c()V

    .line 551
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/a;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 552
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v0, v2

    .line 554
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->D:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt v0, v1, :cond_2

    .line 555
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 558
    :cond_2
    new-instance v0, Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->D:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a:Lcom/sec/chaton/calllog/view/i;

    iget-object v6, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->E:Lcom/sec/chaton/calllog/view/l;

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/calllog/view/a;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/HashMap;Landroid/os/Handler;Lcom/sec/chaton/calllog/view/u;Ljava/util/List;Lcom/sec/chaton/calllog/common/view/swipe/e;)V

    .line 560
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->i:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 561
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->i:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 562
    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/a;->notifyDataSetChanged()V

    .line 564
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Lcom/sec/chaton/calllog/manager/a;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 568
    const-string v0, "setDisplayUpdate()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k()V

    .line 570
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->l()V

    .line 571
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m()V

    .line 572
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->h()V

    .line 573
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 580
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 583
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/a;->a()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Ljava/util/HashMap;Ljava/util/List;Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m:Ljava/lang/String;

    .line 586
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 589
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->C:Lcom/sec/chaton/calllog/manager/b/a;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 599
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->r()V

    .line 620
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 621
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->r:Z

    .line 624
    :cond_1
    return-void

    .line 594
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v2, v2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 603
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->C:Lcom/sec/chaton/calllog/manager/b/a;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    if-ne v1, v0, :cond_4

    .line 606
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 615
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->C:Lcom/sec/chaton/calllog/manager/b/a;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private l()V
    .locals 0

    .prologue
    .line 672
    return-void
.end method

.method private m()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 675
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setBuddyDisplayUpdate()mBuddyType"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->v:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    iget v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->v:I

    packed-switch v0, :pswitch_data_0

    .line 715
    :goto_0
    :pswitch_0
    return-void

    .line 680
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 681
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 691
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->j:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 702
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 703
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->e:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 708
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->q:Z

    goto :goto_0

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->e:Landroid/widget/ImageView;

    const v1, 0x7f0201bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 677
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private n()Landroid/content/Context;
    .locals 1

    .prologue
    .line 908
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 954
    const-string v0, "addDuplicatedCallLog()"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    .line 955
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->t:Z

    if-eqz v0, :cond_5

    .line 958
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/a;->b()I

    move-result v2

    .line 960
    iget v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->u:I

    if-eq v0, v2, :cond_5

    .line 962
    iput-boolean v4, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->t:Z

    .line 963
    new-instance v0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;-><init>()V

    .line 964
    iput v2, v0, Lcom/sec/chaton/calllog/manager/model/CallLogDBData;->_id:I

    .line 965
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v1, v0, v4}, Lcom/sec/chaton/calllog/manager/a;->a(Lcom/sec/chaton/calllog/manager/model/CallLogDBData;Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 967
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v3, v1, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-eqz v1, :cond_5

    .line 969
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v3, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    if-eq v3, v1, :cond_2

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    if-eq v1, v6, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    if-ne v1, v6, :cond_5

    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    if-nez v1, :cond_5

    .line 973
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    iget v3, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    if-eq v1, v3, :cond_4

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    if-le v1, v5, :cond_3

    iget v1, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    if-gt v1, v5, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget v1, v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    if-ge v1, v5, :cond_5

    iget v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    if-ge v0, v5, :cond_5

    .line 977
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 978
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 979
    const-string v1, "add calllog!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    .line 980
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v1, v1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 988
    :cond_5
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->i()V

    .line 989
    return-void
.end method

.method private p()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1000
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1001
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/a;->b()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->u:I

    .line 1003
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->totalIDList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1005
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1007
    iget v4, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->u:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 1009
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->t:Z

    .line 1012
    :cond_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1016
    :cond_2
    return-object v1
.end method

.method private q()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1053
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1054
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1056
    if-eqz v0, :cond_0

    const-string v2, "24"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1057
    :cond_0
    invoke-static {v3}, Lcom/sec/chaton/calllog/view/a;->b(Z)V

    .line 1063
    :goto_0
    if-eqz v1, :cond_5

    .line 1064
    const-string v0, "MM-dd-yyyy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1065
    invoke-static {v3}, Lcom/sec/chaton/calllog/view/a;->a(I)V

    .line 1078
    :cond_1
    :goto_1
    return-void

    .line 1060
    :cond_2
    invoke-static {v4}, Lcom/sec/chaton/calllog/view/a;->b(Z)V

    goto :goto_0

    .line 1067
    :cond_3
    const-string v0, "dd-MM-yyyy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1068
    invoke-static {v4}, Lcom/sec/chaton/calllog/view/a;->a(I)V

    goto :goto_1

    .line 1070
    :cond_4
    const-string v0, "yyyy-MM-dd"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1071
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/a;->a(I)V

    goto :goto_1

    .line 1075
    :cond_5
    invoke-static {v3}, Lcom/sec/chaton/calllog/view/a;->a(I)V

    goto :goto_1
.end method

.method private r()V
    .locals 0

    .prologue
    .line 1408
    return-void
.end method

.method private s()V
    .locals 4

    .prologue
    .line 1769
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->J:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 1770
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->J:Landroid/app/ProgressDialog;

    .line 1774
    :goto_0
    return-void

    .line 1772
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->J:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method private t()V
    .locals 1

    .prologue
    .line 1777
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->J:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 1778
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->J:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1780
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 1751
    new-instance v1, Lcom/sec/chaton/buddy/a/c;

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->C:Lcom/sec/chaton/calllog/manager/b/a;

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1752
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->L:Lcom/sec/chaton/buddy/a/c;

    .line 1753
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->s()V

    .line 1754
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 1755
    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->M:Ljava/lang/String;

    .line 1756
    invoke-virtual {v1}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;)Lcom/sec/chaton/d/a/h;

    .line 1757
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 1664
    .line 1668
    :try_start_0
    const-string v0, "[contactSavedCheck] Contact Saved Find Start"

    const-string v1, "CallLogDetailActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1671
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "number"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1672
    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 1673
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_4

    .line 1675
    :try_start_2
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->H:I

    .line 1676
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[contactSavedCheck] Found in Contact Number : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "CallLogDetailActivity"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[contactSavedCheck] ContactSaved id : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->H:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "CallLogDetailActivity"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1683
    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1684
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1688
    :cond_0
    :goto_1
    if-nez v7, :cond_1

    .line 1689
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[contactSavedCheck] No PhoneNumber in Contact : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CallLogDetailActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    iput v8, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->H:I

    .line 1692
    :cond_1
    return v7

    .line 1680
    :catch_0
    move-exception v0

    move-object v1, v6

    move v2, v8

    .line 1681
    :goto_2
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1683
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1684
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move v7, v2

    .line 1686
    goto :goto_1

    .line 1683
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1684
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1683
    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1680
    :catch_1
    move-exception v0

    move v2, v8

    goto :goto_2

    :catch_2
    move-exception v0

    move v2, v7

    goto :goto_2

    :cond_4
    move v7, v8

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1176
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1177
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1178
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 1223
    :cond_0
    :goto_0
    return-void

    .line 1182
    :cond_1
    const-string v0, "onActivityResult()"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    .line 1183
    if-ne p1, v2, :cond_3

    .line 1185
    const-string v0, "DELETE_RESULT"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    .line 1189
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DELETE_DETAIL_CALLLOG_ALL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-ne v2, v0, :cond_2

    .line 1190
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/j;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/j;->c()V

    goto :goto_0

    .line 1192
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->j()V

    goto :goto_0

    .line 1195
    :cond_3
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    .line 1197
    const-string v0, "SELECT_GROUP_LIST"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    .line 1199
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "MEMBERS_LIST_CHANGE_BUDDY_INFO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-ne v2, v0, :cond_0

    .line 1201
    iget v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->N:I

    iget v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->p:I

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(IILcom/sec/chaton/calllog/manager/a;)Lcom/sec/chaton/calllog/manager/model/CallLogData;

    move-result-object v0

    .line 1202
    if-eqz v0, :cond_0

    .line 1205
    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 1206
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/high16 v7, 0x20000000

    const/high16 v6, 0x20000

    const/16 v5, 0xc8

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1251
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1350
    :goto_0
    :pswitch_0
    return-void

    .line 1254
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->q:Z

    if-eqz v0, :cond_0

    .line 1260
    const-string v0, "onClick() profile_v_voice group"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1268
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/calllog/view/ShowConferenceMemberList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "calllogdata"

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "searchtype"

    iget v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->p:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "order"

    iget v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->N:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "makecall"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "groupName"

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from_intent"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1280
    :cond_0
    const-string v0, "onClick() profile_v_voice"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1281
    invoke-direct {p0, v4}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Z)V

    goto :goto_0

    .line 1287
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->q:Z

    if-eqz v0, :cond_1

    .line 1292
    const-string v0, "onClick() profile_v_video group"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1299
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/calllog/view/ShowConferenceMemberList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "calllogdata"

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "searchtype"

    iget v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->p:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "order"

    iget v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->N:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "makecall"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "groupName"

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from_intent"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1311
    :cond_1
    invoke-direct {p0, v3}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Z)V

    goto/16 :goto_0

    .line 1332
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->q:Z

    invoke-direct {p0, v0, v4}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(ZZ)V

    goto/16 :goto_0

    .line 1336
    :pswitch_4
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->I:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1339
    :pswitch_5
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->q:Z

    invoke-direct {p0, v0, v3}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(ZZ)V

    goto/16 :goto_0

    .line 1251
    :pswitch_data_0
    .packed-switch 0x7f0700e3
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1568
    const-string v0, "onConfigurationChanged()"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    .line 1570
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    if-eqz v0, :cond_0

    .line 1571
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->setBtnTextUpdate()V

    .line 1574
    :cond_0
    const-string v0, "YKYU onConfigurationChanged()"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1575
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1577
    const-string v0, "YKYU onConfigurationChanged()  ORIENTATION_PORTRAIT is called!!!!!!!!"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1578
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    const v1, 0x7f0700e7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 1589
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1590
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 723
    const/4 v0, 0x0

    .line 724
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 726
    iget v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->v:I

    packed-switch v1, :pswitch_data_0

    .line 754
    :goto_0
    :pswitch_0
    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 756
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 757
    return-void

    .line 728
    :pswitch_1
    const v0, 0x7f0f001a

    .line 729
    goto :goto_0

    .line 732
    :pswitch_2
    const v0, 0x7f0f001d

    .line 733
    goto :goto_0

    .line 739
    :pswitch_3
    const v0, 0x7f0f001e

    .line 740
    goto :goto_0

    .line 743
    :pswitch_4
    const v0, 0x7f0f001b

    .line 744
    goto :goto_0

    .line 747
    :pswitch_5
    const v0, 0x7f0f001c

    .line 748
    goto :goto_0

    .line 726
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 187
    const v0, 0x7f030024

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    .line 189
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/j;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/j;->c()V

    .line 192
    const/4 v0, 0x0

    .line 201
    :goto_0
    return-object v0

    .line 194
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->c()V

    .line 195
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->e()V

    .line 199
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->setHasOptionsMenu(Z)V

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d:Landroid/view/View;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 1602
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 1603
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 802
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 804
    sparse-switch v0, :sswitch_data_0

    .line 904
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 806
    :sswitch_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-nez v0, :cond_0

    .line 807
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/calllog/view/DeleteCallLogActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 808
    const-string v1, "calllogdata"

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 809
    const-string v1, "from_intent"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 810
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 811
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 812
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 813
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 816
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 817
    const v1, 0x7f0b0356

    invoke-virtual {p0, v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 818
    const v2, 0x7f0b037e

    invoke-virtual {p0, v2}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 819
    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 820
    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 821
    const v1, 0x7f0b035a

    new-instance v2, Lcom/sec/chaton/calllog/view/b;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/view/b;-><init>(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 832
    const v1, 0x7f0b003a

    new-instance v2, Lcom/sec/chaton/calllog/view/c;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/view/c;-><init>(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 839
    invoke-virtual {v0, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    .line 842
    new-instance v1, Lcom/sec/chaton/calllog/view/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/calllog/view/d;-><init>(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/sec/common/a/a;

    .line 849
    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0

    .line 884
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 885
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/calllog/view/ShowConferenceMemberList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 886
    const-string v1, "calllogdata"

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 887
    const-string v1, "searchtype"

    iget v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->p:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 888
    const-string v1, "order"

    iget v2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->N:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 889
    const-string v1, "makecall"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 890
    const/16 v1, 0xc8

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 893
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto/16 :goto_0

    .line 897
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/j;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/j;->c()V

    goto/16 :goto_0

    .line 804
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_2
        0x7f0705a0 -> :sswitch_0
        0x7f0705a1 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 1594
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    if-eqz v0, :cond_0

    .line 1595
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->k:Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->a()V

    .line 1597
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 1598
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1021
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 1022
    const-string v0, "onResume()"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Ljava/lang/String;)V

    .line 1024
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->o:Z

    if-eqz v0, :cond_0

    .line 1026
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->B:Lcom/sec/chaton/calllog/manager/a;

    iget v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->p:I

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/calllog/manager/a;->a(IZ)Ljava/util/ArrayList;

    move-result-object v1

    .line 1027
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1029
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->n:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 1030
    invoke-static {}, Lcom/sec/chaton/calllog/manager/noti/a;->a()Lcom/sec/chaton/calllog/manager/noti/a;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/manager/noti/a;->a(I)V

    .line 1042
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->q()V

    .line 1043
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->i()V

    .line 1044
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a:Lcom/sec/chaton/calllog/view/i;

    const/16 v1, 0x1f40

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/i;->sendEmptyMessage(I)Z

    .line 1049
    return-void
.end method
