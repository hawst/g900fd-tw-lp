.class public Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;
.super Lcom/sec/chaton/calllog/view/layout/List_Common;
.source "List_DetailCallLog.java"


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Landroid/content/Context;

.field protected c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/util/List;Lcom/sec/chaton/calllog/common/view/swipe/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/sec/chaton/calllog/common/view/swipe/e;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p4}, Lcom/sec/chaton/calllog/view/layout/List_Common;-><init>(Landroid/content/Context;ZLcom/sec/chaton/calllog/common/view/swipe/e;)V

    .line 26
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->b:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->a:Landroid/os/Handler;

    .line 28
    iput-object p3, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->c:Ljava/util/List;

    .line 29
    return-void
.end method

.method private a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 84
    if-eqz p2, :cond_0

    .line 85
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v2, "HH:mm"

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v3, "HH:mm"

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return-object v1

    .line 89
    :cond_0
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v2, "a h:mm"

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v3, "h:mm a"

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 92
    goto :goto_1
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 154
    const-string v0, "kor"

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a()V

    .line 99
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->setVisibility(I)V

    .line 100
    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/graphics/Color;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a(Landroid/content/Context;Landroid/graphics/Color;Landroid/util/AttributeSet;)V

    .line 43
    new-instance v0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    invoke-direct {v0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    .line 44
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->addView(Landroid/view/View;)V

    .line 46
    return-void
.end method

.method protected a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 127
    .line 129
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->setDeleteMode(Z)V

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->setTime(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/chaton/calllog/manager/b/a;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->setDuration(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->setCallType(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    iget v1, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->setCallMethod(I)V

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    invoke-virtual {v0, p4}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->setChecked(Z)V

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->g:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailCallLog;->setVisibility(I)V

    .line 151
    return-void
.end method

.method protected a(Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;)V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    iget-object v1, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setTitle(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    iget-object v1, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->tagname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setTag(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setVisibility(I)V

    .line 117
    return-void
.end method

.method public setView(Lcom/sec/chaton/calllog/manager/model/CallLogData;ZZLjava/lang/String;Ljava/util/HashMap;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 61
    iput-object p4, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->f:Ljava/lang/String;

    .line 62
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->a()V

    .line 64
    instance-of v0, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-eqz v0, :cond_1

    .line 65
    check-cast p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->a(Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;)V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    instance-of v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    if-eqz v0, :cond_0

    .line 67
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2, p6}, Lcom/sec/chaton/calllog/view/layout/List_DetailCallLog;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;ZZ)V

    goto :goto_0
.end method
