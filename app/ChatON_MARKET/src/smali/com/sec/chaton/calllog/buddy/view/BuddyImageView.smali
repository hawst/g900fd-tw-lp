.class public Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;
.super Landroid/widget/ImageView;
.source "BuddyImageView.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lcom/sec/chaton/calllog/buddy/view/c;

.field private d:J

.field private e:Lcom/sec/chaton/calllog/buddy/view/b;

.field private f:Landroid/os/Handler;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 54
    iput-object v2, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->a:Ljava/lang/String;

    .line 56
    iput-object v2, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->b:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/sec/chaton/calllog/buddy/view/c;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/calllog/buddy/view/c;-><init>(Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;Lcom/sec/chaton/calllog/buddy/view/a;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->c:Lcom/sec/chaton/calllog/buddy/view/c;

    .line 62
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->d:J

    .line 66
    new-instance v0, Lcom/sec/chaton/calllog/buddy/view/b;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/calllog/buddy/view/b;-><init>(Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;Lcom/sec/chaton/calllog/buddy/view/a;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->e:Lcom/sec/chaton/calllog/buddy/view/b;

    .line 68
    iput-object v2, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->f:Landroid/os/Handler;

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->g:I

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    iput-object v2, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->a:Ljava/lang/String;

    .line 56
    iput-object v2, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->b:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/sec/chaton/calllog/buddy/view/c;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/calllog/buddy/view/c;-><init>(Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;Lcom/sec/chaton/calllog/buddy/view/a;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->c:Lcom/sec/chaton/calllog/buddy/view/c;

    .line 62
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->d:J

    .line 66
    new-instance v0, Lcom/sec/chaton/calllog/buddy/view/b;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/calllog/buddy/view/b;-><init>(Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;Lcom/sec/chaton/calllog/buddy/view/a;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->e:Lcom/sec/chaton/calllog/buddy/view/b;

    .line 68
    iput-object v2, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->f:Landroid/os/Handler;

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->g:I

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    iput-object v2, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->a:Ljava/lang/String;

    .line 56
    iput-object v2, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->b:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/sec/chaton/calllog/buddy/view/c;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/calllog/buddy/view/c;-><init>(Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;Lcom/sec/chaton/calllog/buddy/view/a;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->c:Lcom/sec/chaton/calllog/buddy/view/c;

    .line 62
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->d:J

    .line 66
    new-instance v0, Lcom/sec/chaton/calllog/buddy/view/b;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/calllog/buddy/view/b;-><init>(Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;Lcom/sec/chaton/calllog/buddy/view/a;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->e:Lcom/sec/chaton/calllog/buddy/view/b;

    .line 68
    iput-object v2, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->f:Landroid/os/Handler;

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->g:I

    .line 101
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/chaton/util/bt;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 120
    iget v0, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->g:I

    .line 134
    return-void
.end method

.method public setBuddyGroupDefaultImage()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public setImageViewMode(I)V
    .locals 0

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->g:I

    .line 107
    return-void
.end method
