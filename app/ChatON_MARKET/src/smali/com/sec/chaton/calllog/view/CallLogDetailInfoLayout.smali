.class public Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;
.super Landroid/widget/LinearLayout;
.source "CallLogDetailInfoLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/sec/chaton/calllog/manager/a;

.field private final b:Lcom/sec/chaton/calllog/manager/b/a;

.field private c:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/Button;

.field private m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

.field private n:I

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->a:Lcom/sec/chaton/calllog/manager/a;

    .line 50
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->b:Lcom/sec/chaton/calllog/manager/b/a;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->o:Ljava/util/ArrayList;

    .line 87
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 88
    const v1, 0x7f030023

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 89
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    const-string v0, "[CallLogDetailInfoLayout]"

    invoke-static {p1, v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateUIComponent()-------buddy type???"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->a(Ljava/lang/String;)V

    .line 121
    iput p1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->n:I

    .line 122
    packed-switch p1, :pswitch_data_0

    .line 186
    :goto_0
    :pswitch_0
    return-void

    .line 125
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 131
    :pswitch_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 141
    :pswitch_3
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->g:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 150
    :pswitch_4
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 159
    :pswitch_5
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->i:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 168
    :pswitch_6
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->l:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 176
    :pswitch_7
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->j:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 196
    const-string v0, "onClick()"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->a(Ljava/lang/String;)V

    .line 198
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->g:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->j:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 199
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->g:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->c:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a()V

    .line 334
    :cond_1
    :goto_0
    return-void

    .line 203
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/util/ArrayList;

    move-result-object v3

    .line 206
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 207
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 211
    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v5, Lcom/sec/chaton/chat/ChatInfoMoreActivity;

    invoke-direct {v1, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 212
    const-string v0, "ACTIVITY_PURPOSE"

    const/4 v5, 0x6

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 213
    sget-object v5, Lcom/sec/chaton/chat/ChatFragment;->f:Ljava/lang/String;

    new-array v0, v2, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const-string v3, "member_name"

    new-array v0, v2, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    const-string v0, "ACTIVITY_PURPOSE_ARG"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 216
    const-string v0, "ACTIVITY_PURPOSE_CALLLOG_GROUP"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 218
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 252
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->h:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 267
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->i:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 280
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->k:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 282
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 283
    const-string v3, "PROFILE_BUDDY_NO"

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    const-string v3, "PROFILE_BUDDY_NAME"

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 286
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 298
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->l:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 300
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 301
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 304
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->a:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/a;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 306
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->b:Lcom/sec/chaton/calllog/manager/b/a;

    iget-object v4, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v5, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v5, v5, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-virtual {v1, v4, v0, v5}, Lcom/sec/chaton/calllog/manager/b/a;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/util/HashMap;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 307
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 309
    invoke-static {v0, v1}, Lcom/sec/chaton/e/a/f;->d(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v4

    .line 310
    new-instance v5, Lcom/sec/chaton/buddy/a/b;

    if-nez v3, :cond_7

    move v0, v2

    :goto_3
    const/4 v6, 0x2

    invoke-direct {v5, v4, v1, v0, v6}, Lcom/sec/chaton/buddy/a/b;-><init>(ILjava/lang/String;II)V

    .line 317
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v6, Lcom/sec/chaton/buddy/BuddyGroupProfileActivity;

    invoke-direct {v4, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 318
    const-string v0, "ACTIVITY_PURPOSE"

    const/16 v6, 0x12

    invoke-virtual {v4, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 319
    const-string v0, "groupInfo"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 320
    const-string v5, "ACTIVITY_ARG_GROUP_MEMBER_NO_LIST"

    new-array v0, v2, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 321
    const-string v0, "GROUP_PROFILE_NAME"

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 324
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 310
    :cond_7
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_3
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 93
    const v0, 0x7f0700db

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->e:Landroid/widget/LinearLayout;

    .line 94
    const v0, 0x7f0700d7

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->d:Landroid/widget/LinearLayout;

    .line 95
    const v0, 0x7f0700dd

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->f:Landroid/widget/LinearLayout;

    .line 96
    const v0, 0x7f0700d8

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->g:Landroid/widget/Button;

    .line 97
    const v0, 0x7f0700d9

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->h:Landroid/widget/Button;

    .line 98
    const v0, 0x7f0700da

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->i:Landroid/widget/Button;

    .line 99
    const v0, 0x7f0700de

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->j:Landroid/widget/Button;

    .line 101
    const v0, 0x7f0700dc

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->k:Landroid/widget/Button;

    .line 102
    const v0, 0x7f0700df

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->l:Landroid/widget/Button;

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->k:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->l:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 112
    return-void
.end method

.method public setBtnTextUpdate()V
    .locals 3

    .prologue
    const v2, 0x7f0b035e

    .line 356
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 357
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->h:Landroid/widget/Button;

    const v1, 0x7f0b036b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 358
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 359
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->j:Landroid/widget/Button;

    const v1, 0x7f0b0369

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 360
    return-void
.end method

.method public setParent(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Lcom/sec/chaton/calllog/manager/model/CallLogData;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->c:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    .line 116
    iput-object p2, p0, Lcom/sec/chaton/calllog/view/CallLogDetailInfoLayout;->m:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 117
    return-void
.end method
