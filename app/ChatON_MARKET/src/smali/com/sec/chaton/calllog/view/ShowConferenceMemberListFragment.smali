.class public Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;
.super Landroid/support/v4/app/Fragment;
.source "ShowConferenceMemberListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field protected a:Lcom/sec/chaton/calllog/manager/model/c;

.field public b:Landroid/os/Handler;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ListView;

.field private e:Lcom/sec/chaton/calllog/view/ak;

.field private f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

.field private final g:Lcom/sec/chaton/calllog/manager/a;

.field private h:Lcom/sec/chaton/calllog/view/ah;

.field private i:Lcom/sec/chaton/buddy/a/c;

.field private j:Landroid/app/ProgressDialog;

.field private k:Landroid/widget/Toast;

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Landroid/widget/RelativeLayout;

.field private q:Landroid/widget/CheckBox;

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcom/coolots/sso/a/a;

.field private t:Ljava/lang/String;

.field private u:Landroid/view/Menu;

.field private v:Z

.field private w:[I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c:Landroid/view/View;

    .line 77
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->g:Lcom/sec/chaton/calllog/manager/a;

    .line 86
    iput v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->l:I

    .line 87
    iput v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->m:I

    .line 88
    iput v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->n:I

    .line 91
    iput-boolean v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->o:Z

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    .line 95
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->s:Lcom/coolots/sso/a/a;

    .line 102
    iput-boolean v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->v:Z

    .line 607
    new-instance v0, Lcom/sec/chaton/calllog/view/ae;

    invoke-direct {v0, p0}, Lcom/sec/chaton/calllog/view/ae;-><init>(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->b:Landroid/os/Handler;

    return-void
.end method

.method private a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->k:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->k:Landroid/widget/Toast;

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->k:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 308
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->k:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 309
    return-void
.end method

.method private a(I[Ljava/lang/String;)Z
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 776
    const/4 v0, -0x1

    .line 779
    array-length v1, p2

    if-ne v1, v8, :cond_3

    .line 781
    const-string v1, "makeCall() with single buddy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->g()Z

    move-result v1

    if-nez v1, :cond_1

    .line 785
    if-nez p1, :cond_2

    .line 786
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->s:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    aget-object v3, p2, v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v6, "Push Name"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 813
    :cond_0
    :goto_0
    if-eqz v0, :cond_5

    .line 815
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b008a

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 821
    :cond_1
    :goto_1
    return v2

    .line 788
    :cond_2
    if-ne p1, v8, :cond_0

    .line 789
    iget-object v6, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->s:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    aget-object v9, p2, v2

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Push Name"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 798
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->g()Z

    move-result v1

    if-nez v1, :cond_1

    .line 800
    if-nez p1, :cond_4

    .line 801
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->s:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "Push Name"

    const-string v6, ""

    invoke-virtual {v3, v4, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->t:Ljava/lang/String;

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 803
    :cond_4
    if-ne p1, v8, :cond_0

    .line 804
    iget-object v6, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->s:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "Push Name"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v12, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->t:Ljava/lang/String;

    move-object v9, p2

    move-object v11, v5

    invoke-virtual/range {v6 .. v12}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_5
    move v2, v8

    .line 818
    goto :goto_1
.end method

.method private a(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 346
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 347
    const-string v0, "calllogdata"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 349
    const-string v3, "from_intent"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->n:I

    .line 350
    const-string v3, "order"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->m:I

    .line 351
    const-string v3, "searchtype"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->l:I

    .line 352
    const-string v3, "makecall"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->o:Z

    .line 353
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "groupName"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->t:Ljava/lang/String;

    .line 355
    if-nez v0, :cond_0

    .line 370
    :goto_0
    return v1

    .line 359
    :cond_0
    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Lcom/sec/chaton/calllog/manager/model/CallLogData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadCallLogData(3) userInfo.size["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v2, v2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] -1"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v2, Lcom/sec/chaton/calllog/view/ShowConferenceMemberList;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    move v0, v1

    .line 365
    :goto_1
    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v2, v2, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 366
    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 370
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 163
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 164
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 170
    :cond_1
    iget v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 172
    iget v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->n:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->n:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 174
    :cond_2
    iget v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->n:I

    new-array v0, v2, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a(I[Ljava/lang/String;)Z

    move-result v0

    .line 176
    if-eqz v0, :cond_3

    .line 177
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/ai;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/ai;->c()V

    .line 187
    :cond_3
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->d()V

    return-void
.end method

.method private b(Z)Z
    .locals 8

    .prologue
    const v7, 0x7f0b0385

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 685
    .line 687
    const/4 v0, -0x1

    .line 689
    iget v3, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->n:I

    if-nez v3, :cond_0

    .line 690
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->s:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    .line 695
    :goto_0
    if-eqz p1, :cond_1

    .line 697
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_5

    .line 699
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v2

    invoke-virtual {v3, v7, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 703
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "overMax() maxCount["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], mCheckedList.size()["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/calllog/view/ShowConferenceMemberList;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    :goto_2
    return v0

    .line 691
    :cond_0
    iget v3, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->n:I

    if-ne v3, v1, :cond_6

    .line 692
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->s:Lcom/coolots/sso/a/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Lcom/coolots/sso/a/a;->b(Landroid/content/Context;Z)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    goto :goto_0

    :cond_1
    move v3, v2

    move v4, v2

    .line 708
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 709
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 710
    add-int/lit8 v4, v4, 0x1

    .line 708
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 713
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "overMax() maxCount["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "], checkedCallLogAmount["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v3, Lcom/sec/chaton/calllog/view/ShowConferenceMemberList;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    if-lt v4, v5, :cond_4

    .line 717
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v3, v7, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 718
    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto/16 :goto_1

    :cond_6
    move v5, v0

    goto/16 :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->j:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 289
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->j:Landroid/app/ProgressDialog;

    .line 293
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e()V

    return-void
.end method

.method private c(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0705a6

    .line 757
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->u:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->u:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 758
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->u:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 760
    :cond_0
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->j:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 299
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->h()V

    return-void
.end method

.method private d(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0705a6

    .line 763
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->u:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->u:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 765
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->u:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 767
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)Lcom/sec/chaton/buddy/a/c;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->i:Lcom/sec/chaton/buddy/a/c;

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    const v4, 0x7f030132

    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateList() mOrder["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], mSearchType"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/calllog/view/ShowConferenceMemberList;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iget v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->m:I

    iget v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->l:I

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->g:Lcom/sec/chaton/calllog/manager/a;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(IILcom/sec/chaton/calllog/manager/a;)Lcom/sec/chaton/calllog/manager/model/CallLogData;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Lcom/sec/chaton/calllog/manager/model/CallLogData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 334
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->o:Z

    if-eqz v0, :cond_0

    .line 335
    new-instance v0, Lcom/sec/chaton/calllog/view/ak;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/chaton/calllog/view/ak;-><init>(Landroid/content/Context;ILcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e:Lcom/sec/chaton/calllog/view/ak;

    .line 340
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e:Lcom/sec/chaton/calllog/view/ak;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e:Lcom/sec/chaton/calllog/view/ak;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/ak;->notifyDataSetChanged()V

    .line 342
    return-void

    .line 337
    :cond_0
    new-instance v0, Lcom/sec/chaton/calllog/view/ak;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->h:Lcom/sec/chaton/calllog/view/ah;

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/chaton/calllog/view/ak;-><init>(Landroid/content/Context;ILcom/sec/chaton/calllog/manager/model/CallLogData;Lcom/sec/chaton/calllog/view/ah;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e:Lcom/sec/chaton/calllog/view/ak;

    goto :goto_0
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 726
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    move v3, v2

    .line 728
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 729
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 730
    add-int/lit8 v3, v3, 0x1

    .line 728
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 734
    :cond_1
    if-gt v4, v3, :cond_2

    .line 735
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 740
    :goto_1
    if-nez v3, :cond_3

    .line 741
    invoke-direct {p0, v2}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c(Z)V

    .line 745
    :goto_2
    return-void

    .line 737
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 743
    :cond_3
    invoke-direct {p0, v5}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c(Z)V

    goto :goto_2
.end method

.method static synthetic f(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)[I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->w:[I

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)Lcom/sec/chaton/calllog/manager/model/CallLogData;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    return-object v0
.end method

.method private g()Z
    .locals 11

    .prologue
    const v10, 0x7f0b0039

    const v9, 0x7f0b0037

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 828
    .line 831
    new-array v0, v7, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->w:[I

    move v1, v2

    move v3, v2

    move v4, v2

    .line 833
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 834
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 835
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 838
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->w:[I

    aput v1, v0, v3

    .line 839
    add-int/lit8 v3, v3, 0x1

    move v4, v5

    .line 833
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 844
    :cond_1
    if-ne v5, v4, :cond_2

    .line 846
    if-ne v3, v5, :cond_3

    .line 848
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0065

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0b0386

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    iget-object v7, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->w:[I

    aget v7, v7, v2

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    aput-object v0, v5, v2

    invoke-virtual {v3, v6, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/calllog/view/af;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/view/af;-><init>(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 878
    :cond_2
    :goto_1
    return v4

    .line 863
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0065

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0b0387

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    iget-object v8, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->w:[I

    aget v8, v8, v2

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    aput-object v0, v7, v2

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->w:[I

    aget v2, v2, v5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    aput-object v0, v7, v5

    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/calllog/view/ag;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/view/ag;-><init>(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_1

    .line 831
    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method private h()V
    .locals 3

    .prologue
    .line 883
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 884
    const-string v1, "MEMBERS_LIST_CHANGE_BUDDY_INFO"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 885
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 886
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 262
    new-instance v0, Lcom/sec/chaton/buddy/a/c;

    invoke-direct {v0, p1, p2}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->i:Lcom/sec/chaton/buddy/a/c;

    .line 264
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c()V

    .line 265
    new-instance v1, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->b:Landroid/os/Handler;

    invoke-direct {v1, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 266
    invoke-virtual {v0}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/d/h;->b(Ljava/lang/String;)Lcom/sec/chaton/d/a/h;

    .line 267
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 748
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e:Lcom/sec/chaton/calllog/view/ak;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/ak;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 749
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 748
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 751
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e:Lcom/sec/chaton/calllog/view/ak;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/ak;->notifyDataSetChanged()V

    .line 754
    return-void
.end method

.method public a([Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 271
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 273
    new-instance v2, Lcom/sec/chaton/buddy/a/c;

    aget-object v3, p1, v0

    invoke-direct {v2, v3, p2}, Lcom/sec/chaton/buddy/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iput-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->i:Lcom/sec/chaton/buddy/a/c;

    .line 276
    array-length v2, p1

    :goto_0
    if-ge v0, v2, :cond_0

    .line 278
    aget-object v3, p1, v0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 281
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c()V

    .line 282
    new-instance v0, Lcom/sec/chaton/d/h;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->b:Landroid/os/Handler;

    invoke-direct {v0, v2}, Lcom/sec/chaton/d/h;-><init>(Landroid/os/Handler;)V

    .line 283
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/d/h;->a(Ljava/util/ArrayList;Z)V

    .line 285
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 106
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 110
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->o:Z

    if-eqz v0, :cond_0

    .line 112
    invoke-direct {p0, v2}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 119
    invoke-direct {p0, v2}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c(Z)V

    .line 125
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a(Z)V

    goto :goto_0

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 123
    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c(Z)V

    goto :goto_1

    .line 132
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->o:Z

    if-eqz v0, :cond_0

    .line 134
    invoke-direct {p0, v2}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 138
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 141
    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c(Z)V

    .line 147
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a(Z)V

    goto :goto_0

    .line 143
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 145
    invoke-direct {p0, v2}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c(Z)V

    goto :goto_2

    .line 106
    :pswitch_data_0
    .packed-switch 0x7f070140
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 403
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 405
    const v0, 0x7f0f0023

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 406
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->u:Landroid/view/Menu;

    .line 408
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->o:Z

    if-eqz v0, :cond_0

    .line 409
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->d(Z)V

    .line 414
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 415
    return-void

    .line 411
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->d(Z)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f030132

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 193
    const v0, 0x7f0300f2

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c:Landroid/view/View;

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c:Landroid/view/View;

    const v1, 0x7f070436

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->d:Landroid/widget/ListView;

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c:Landroid/view/View;

    const v1, 0x7f070140

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->p:Landroid/widget/RelativeLayout;

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c:Landroid/view/View;

    const v1, 0x7f070141

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    .line 200
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a(Landroid/content/Intent;)Z

    .line 202
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->o:Z

    if-eqz v0, :cond_0

    .line 213
    new-instance v0, Lcom/sec/chaton/calllog/view/ak;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/chaton/calllog/view/ak;-><init>(Landroid/content/Context;ILcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e:Lcom/sec/chaton/calllog/view/ak;

    .line 215
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0177

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 232
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e:Lcom/sec/chaton/calllog/view/ak;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->d:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 235
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->o:Z

    if-eqz v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->p:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->p:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    :goto_1
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c:Landroid/view/View;

    return-object v0

    .line 219
    :cond_0
    new-instance v0, Lcom/sec/chaton/calllog/view/ad;

    invoke-direct {v0, p0}, Lcom/sec/chaton/calllog/view/ad;-><init>(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->h:Lcom/sec/chaton/calllog/view/ah;

    .line 229
    new-instance v0, Lcom/sec/chaton/calllog/view/ak;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->h:Lcom/sec/chaton/calllog/view/ah;

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/chaton/calllog/view/ak;-><init>(Landroid/content/Context;ILcom/sec/chaton/calllog/manager/model/CallLogData;Lcom/sec/chaton/calllog/view/ah;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e:Lcom/sec/chaton/calllog/view/ak;

    goto :goto_0

    .line 244
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->p:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 246
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->p:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 377
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->o:Z

    if-eqz v0, :cond_3

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->b(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->r:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v2, p3, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 382
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e:Lcom/sec/chaton/calllog/view/ak;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/ak;->notifyDataSetChanged()V

    .line 383
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f()V

    .line 398
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 381
    goto :goto_0

    .line 388
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f:Lcom/sec/chaton/calllog/manager/model/CallLogData;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    .line 389
    iget-object v1, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 392
    new-instance v1, Landroid/content/Intent;

    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/buddy/BuddyProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 393
    const-string v2, "PROFILE_BUDDY_NO"

    iget-object v3, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 394
    const-string v2, "PROFILE_BUDDY_NAME"

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 425
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 436
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 427
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/ai;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/ai;->c()V

    goto :goto_0

    .line 431
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->b()V

    goto :goto_0

    .line 425
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0705a6 -> :sswitch_1
    .end sparse-switch
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 257
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 258
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->f()V

    .line 259
    return-void
.end method
