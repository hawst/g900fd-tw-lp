.class Lcom/sec/chaton/calllog/view/ae;
.super Landroid/os/Handler;
.source "ShowConferenceMemberListFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V
    .locals 0

    .prologue
    .line 607
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f0b01fb

    const v5, 0x7f0b0037

    const v4, 0x7f0b00be

    const/4 v3, 0x0

    .line 611
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 612
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_0

    .line 677
    :goto_0
    return-void

    .line 616
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 635
    :sswitch_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->b(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V

    .line 637
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 638
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_2

    .line 640
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;Ljava/lang/CharSequence;)V

    .line 643
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V

    .line 644
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->d(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V

    goto :goto_0

    .line 620
    :sswitch_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->b(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V

    .line 622
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v1, :cond_1

    .line 623
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 625
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->c(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V

    .line 626
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->d(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)V

    goto :goto_0

    .line 628
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 646
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_3

    .line 648
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-static {v2}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->e(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 652
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 654
    :cond_3
    const-string v1, ""

    .line 656
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADD BUDDY RESULT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ShowConferenceMemberList"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e83

    if-ne v1, v2, :cond_4

    .line 658
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 667
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->a(Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 659
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e84

    if-ne v1, v2, :cond_5

    .line 660
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 661
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v1, 0x3e85

    if-ne v0, v1, :cond_6

    .line 662
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 664
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/ae;->a:Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/ShowConferenceMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 671
    :cond_7
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 616
    nop

    :sswitch_data_0
    .sparse-switch
        0x12f -> :sswitch_0
        0x137 -> :sswitch_1
    .end sparse-switch
.end method
