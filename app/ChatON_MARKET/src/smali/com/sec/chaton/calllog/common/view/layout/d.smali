.class Lcom/sec/chaton/calllog/common/view/layout/d;
.super Ljava/lang/Object;
.source "RadioButtonListDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;


# direct methods
.method constructor <init>(Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->b:Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;

    iput-object p2, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->b:Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f070013

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 96
    instance-of v0, v1, Lcom/sec/chaton/calllog/view/CallLogFragment;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b037d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a(I)I

    :cond_0
    :goto_0
    move-object v0, v1

    .line 120
    check-cast v0, Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-static {}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(I)V

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->b:Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->dismiss()V

    .line 125
    :cond_1
    return-void

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0361

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 101
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a(I)I

    goto :goto_0

    .line 102
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0360

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 103
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a(I)I

    goto :goto_0

    .line 105
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0358

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 106
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a(I)I

    goto :goto_0

    .line 107
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b035f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 108
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a(I)I

    goto/16 :goto_0

    .line 110
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b036f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 111
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a(I)I

    goto/16 :goto_0

    .line 112
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0373

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 113
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a(I)I

    goto/16 :goto_0

    .line 114
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0374

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 115
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a(I)I

    goto/16 :goto_0

    .line 116
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0372

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/sec/chaton/calllog/common/view/layout/RadioButtonListDialog;->a(I)I

    goto/16 :goto_0
.end method
