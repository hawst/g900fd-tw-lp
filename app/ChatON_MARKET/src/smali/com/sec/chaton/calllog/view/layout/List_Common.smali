.class public Lcom/sec/chaton/calllog/view/layout/List_Common;
.super Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;
.source "List_Common.java"


# instance fields
.field protected d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

.field protected e:Z

.field protected f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLcom/sec/chaton/calllog/common/view/swipe/e;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;-><init>(Landroid/content/Context;Lcom/sec/chaton/calllog/common/view/swipe/e;)V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->e:Z

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->f:Ljava/lang/String;

    .line 37
    invoke-virtual {p0, p1, v1, v1}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a(Landroid/content/Context;Landroid/graphics/Color;Landroid/util/AttributeSet;)V

    .line 39
    iput-boolean p2, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->e:Z

    .line 41
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setVisibility(I)V

    .line 122
    :cond_0
    return-void
.end method

.method protected a(F)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method protected a(II)V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/graphics/Color;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 52
    const v0, 0x7f03012f

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 54
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->e:Z

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    invoke-direct {v0, p1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    .line 64
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_Common;->addView(Landroid/view/View;)V

    .line 68
    :cond_0
    return-void
.end method

.method protected a(Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    iget-object v1, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setTitle(Ljava/lang/String;)V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setVisibility(I)V

    .line 173
    :cond_1
    return-void
.end method

.method protected b(F)V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method protected c(F)V
    .locals 0

    .prologue
    .line 205
    return-void
.end method

.method public setView(Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)V
    .locals 0

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a()V

    .line 83
    return-void
.end method

.method public setView(Lcom/sec/chaton/calllog/manager/model/CallLogData;ZZLjava/lang/String;Ljava/util/HashMap;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 106
    iput-object p4, p0, Lcom/sec/chaton/calllog/view/layout/List_Common;->f:Ljava/lang/String;

    .line 108
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a()V

    .line 110
    return-void
.end method
