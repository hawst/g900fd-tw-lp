.class public abstract Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;
.super Landroid/widget/FrameLayout;
.source "SwipeLayout.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field private a:Lcom/sec/chaton/calllog/common/view/swipe/e;

.field private b:Landroid/view/GestureDetector;

.field private c:Z

.field private d:Z

.field private e:I

.field private final f:Lcom/sec/chaton/calllog/common/view/swipe/d;

.field private final g:Lcom/sec/chaton/calllog/common/view/swipe/c;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/chaton/calllog/common/view/swipe/e;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 156
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 46
    iput-boolean v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->c:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->d:Z

    .line 58
    iput v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    .line 70
    new-instance v0, Lcom/sec/chaton/calllog/common/view/swipe/d;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/calllog/common/view/swipe/d;-><init>(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;Lcom/sec/chaton/calllog/common/view/swipe/a;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->f:Lcom/sec/chaton/calllog/common/view/swipe/d;

    .line 110
    new-instance v0, Lcom/sec/chaton/calllog/common/view/swipe/c;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/calllog/common/view/swipe/c;-><init>(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;Lcom/sec/chaton/calllog/common/view/swipe/a;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->g:Lcom/sec/chaton/calllog/common/view/swipe/c;

    .line 149
    iput v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->h:I

    .line 158
    iput-object p2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    .line 160
    new-instance v0, Landroid/view/GestureDetector;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->b:Landroid/view/GestureDetector;

    .line 162
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;I)I
    .locals 0

    .prologue
    .line 18
    iput p1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    return p1
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 370
    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->setPressed(Z)V

    .line 372
    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->setBackgroundColor(I)V

    .line 374
    iput v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    .line 376
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    invoke-interface {v0, p0}, Lcom/sec/chaton/calllog/common/view/swipe/e;->a(Landroid/view/View;)V

    .line 382
    :cond_0
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 226
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->h:I

    .line 228
    iput v3, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->f:Lcom/sec/chaton/calllog/common/view/swipe/d;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/chaton/calllog/common/view/swipe/d;->sendEmptyMessageDelayed(IJ)Z

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->f:Lcom/sec/chaton/calllog/common/view/swipe/d;

    const/4 v1, 0x3

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/calllog/common/view/swipe/d;->sendEmptyMessageDelayed(IJ)Z

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->g:Lcom/sec/chaton/calllog/common/view/swipe/c;

    invoke-interface {v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/e;->a(Lcom/sec/chaton/calllog/common/view/swipe/b;)Z

    .line 242
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 24
    const-string v0, "[SwipeLayout]"

    invoke-static {p1, v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)Lcom/sec/chaton/calllog/common/view/swipe/d;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->f:Lcom/sec/chaton/calllog/common/view/swipe/d;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->f:Lcom/sec/chaton/calllog/common/view/swipe/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/d;->removeMessages(I)V

    .line 433
    invoke-virtual {p0, v2}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->setPressed(Z)V

    .line 435
    invoke-virtual {p0, v2}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->setBackgroundColor(I)V

    .line 437
    return-void
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 298
    iget v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    packed-switch v0, :pswitch_data_0

    .line 326
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->h:I

    sub-int/2addr v0, v1

    if-gez v0, :cond_0

    .line 328
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->d(I)V

    .line 336
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    .line 338
    return-void

    .line 304
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    if-eqz v0, :cond_1

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    invoke-interface {v0, p0}, Lcom/sec/chaton/calllog/common/view/swipe/e;->onClick(Landroid/view/View;)V

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->f:Lcom/sec/chaton/calllog/common/view/swipe/d;

    const/4 v1, 0x2

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/calllog/common/view/swipe/d;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 318
    :pswitch_1
    const-string v0, "<<YHT>>DETECT ERROR CASE !!!!!!"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a(Ljava/lang/String;)V

    .line 320
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->d(I)V

    goto :goto_0

    .line 298
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic c(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)Lcom/sec/chaton/calllog/common/view/swipe/e;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    return-object v0
.end method

.method private c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 250
    iget v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    if-ne v0, v2, :cond_2

    .line 252
    iget v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->h:I

    if-ge v0, p1, :cond_1

    .line 254
    const/4 v0, 0x0

    iget v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->h:I

    sub-int v1, p1, v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a(II)V

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->d:Z

    if-eqz v0, :cond_0

    .line 260
    const/4 v0, 0x1

    iget v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->h:I

    sub-int v1, p1, v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a(II)V

    goto :goto_0

    .line 268
    :cond_2
    iget v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->h:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_0

    .line 276
    iget v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->h:I

    if-ge v0, p1, :cond_0

    .line 278
    invoke-direct {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->b()V

    .line 280
    iput v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    goto :goto_0
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    .line 346
    invoke-direct {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->b()V

    .line 348
    iget v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->h:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->c(F)V

    .line 350
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a()V

    return-void
.end method


# virtual methods
.method protected abstract a(F)V
.end method

.method protected a(I)V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/e;->a(Lcom/sec/chaton/calllog/common/view/swipe/b;)Z

    .line 364
    :cond_0
    return-void
.end method

.method protected abstract a(II)V
.end method

.method protected abstract b(F)V
.end method

.method protected b(I)V
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    invoke-interface {v0, p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/e;->a(Landroid/view/View;I)V

    .line 459
    :cond_0
    return-void
.end method

.method protected abstract c(F)V
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 468
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .prologue
    const/high16 v6, 0x43160000    # 150.0f

    const/high16 v5, 0x41200000    # 10.0f

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 475
    const-string v0, "<<YHT>>onFling !!!!!!"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a(Ljava/lang/String;)V

    .line 477
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    .line 481
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Distance:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Velocity:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a(Ljava/lang/String;)V

    .line 483
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    cmpl-float v0, v0, v6

    if-lez v0, :cond_1

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    .line 485
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    .line 487
    invoke-direct {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->b()V

    .line 489
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->d:Z

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    if-eqz v0, :cond_0

    .line 495
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a(F)V

    .line 535
    :cond_0
    :goto_0
    return v3

    .line 507
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    cmpl-float v0, v0, v6

    if-lez v0, :cond_2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_2

    .line 509
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    .line 511
    invoke-direct {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->b()V

    .line 513
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    if-eqz v0, :cond_2

    .line 517
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->b(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 525
    :catch_0
    move-exception v0

    .line 529
    :cond_2
    iput v4, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->e:I

    .line 531
    invoke-direct {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->b()V

    .line 533
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->c(F)V

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b038b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 544
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 549
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b038a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 551
    return v3
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 558
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0389

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 560
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 565
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0388

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 567
    return v3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->c:Z

    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 184
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 186
    iget-object v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->b:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 188
    packed-switch v0, :pswitch_data_0

    .line 216
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 178
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    .line 194
    :pswitch_0
    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->c(I)V

    goto :goto_0

    .line 200
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->b(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 206
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 188
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setEnabledSwipe(Z)V
    .locals 1

    .prologue
    .line 100
    iput-boolean p1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->c:Z

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->a:Lcom/sec/chaton/calllog/common/view/swipe/e;

    if-nez v0, :cond_0

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeLayout;->c:Z

    .line 108
    :cond_0
    return-void
.end method
