.class Lcom/sec/chaton/calllog/view/r;
.super Landroid/os/Handler;
.source "CallLogFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/calllog/view/CallLogFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/calllog/view/CallLogFragment;)V
    .locals 0

    .prologue
    .line 1372
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const v6, 0x7f0b00be

    const v5, 0x7f0b0037

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1376
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1377
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 1425
    :goto_0
    return-void

    .line 1381
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->d(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    .line 1383
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1384
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_0

    .line 1386
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b01fb

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1388
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c(Lcom/sec/chaton/calllog/view/CallLogFragment;)V

    .line 1418
    :goto_1
    invoke-static {v4}, Lcom/sec/chaton/calllog/view/CallLogFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 1390
    :cond_0
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_1

    .line 1392
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {}, Lcom/sec/chaton/calllog/view/CallLogFragment;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1396
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->e(Lcom/sec/chaton/calllog/view/CallLogFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_1

    .line 1398
    :cond_1
    const-string v1, ""

    .line 1400
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADD BUDDY RESULT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e83

    if-ne v1, v2, :cond_2

    .line 1402
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1411
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->e(Lcom/sec/chaton/calllog/view/CallLogFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_1

    .line 1403
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e84

    if-ne v1, v2, :cond_3

    .line 1404
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1405
    :cond_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v1, 0x3e85

    if-ne v0, v1, :cond_4

    .line 1406
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1408
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/r;->a:Lcom/sec/chaton/calllog/view/CallLogFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1415
    :cond_5
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 1377
    :pswitch_data_0
    .packed-switch 0x12f
        :pswitch_0
    .end packed-switch
.end method
