.class public Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;
.super Lcom/sec/chaton/calllog/view/layout/List_Common;
.source "List_DetailGroupCallLog.java"


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Landroid/content/Context;

.field protected c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/sec/chaton/calllog/manager/b/a;

.field private h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/util/List;Lcom/sec/chaton/calllog/common/view/swipe/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/sec/chaton/calllog/common/view/swipe/e;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p4}, Lcom/sec/chaton/calllog/view/layout/List_Common;-><init>(Landroid/content/Context;ZLcom/sec/chaton/calllog/common/view/swipe/e;)V

    .line 19
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->g:Lcom/sec/chaton/calllog/manager/b/a;

    .line 28
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->b:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->a:Landroid/os/Handler;

    .line 30
    iput-object p3, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->c:Ljava/util/List;

    .line 31
    return-void
.end method

.method private a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 86
    if-eqz p2, :cond_0

    .line 87
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v2, "HH:mm"

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v3, "HH:mm"

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return-object v1

    .line 91
    :cond_0
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v2, "a h:mm"

    invoke-virtual {v1, v0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->j:Ljava/lang/String;

    const-string v3, "h:mm a"

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 94
    goto :goto_1
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 153
    const-string v0, "kor"

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a()V

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->setVisibility(I)V

    .line 102
    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/graphics/Color;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/calllog/view/layout/List_Common;->a(Landroid/content/Context;Landroid/graphics/Color;Landroid/util/AttributeSet;)V

    .line 45
    new-instance v0, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    invoke-direct {v0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    .line 46
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->addView(Landroid/view/View;)V

    .line 48
    return-void
.end method

.method protected a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 129
    .line 131
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->setDeleteMode(Z)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->g:Lcom/sec/chaton/calllog/manager/b/a;

    invoke-virtual {v1, p1}, Lcom/sec/chaton/calllog/manager/b/a;->b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->setName(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->setTime(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    invoke-static {}, Lcom/sec/chaton/calllog/manager/b/a;->a()Lcom/sec/chaton/calllog/manager/b/a;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/chaton/calllog/manager/b/a;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->setDuration(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->setCallType(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    iget v1, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->setCallMethod(I)V

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    invoke-virtual {v0, p4}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->setChecked(Z)V

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->h:Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/List_TypeDetailGroupCallLog;->setVisibility(I)V

    .line 150
    return-void
.end method

.method protected a(Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    iget-object v1, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->seperatorname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setTitle(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    iget-object v1, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;->tagname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setTag(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->d:Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/layout/CallLogTitleBar;->setVisibility(I)V

    .line 119
    return-void
.end method

.method public setView(Lcom/sec/chaton/calllog/manager/model/CallLogData;ZZLjava/lang/String;Ljava/util/HashMap;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 63
    iput-object p4, p0, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->f:Ljava/lang/String;

    .line 64
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->a()V

    .line 66
    instance-of v0, p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-eqz v0, :cond_1

    .line 67
    check-cast p1, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->a(Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    instance-of v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    if-eqz v0, :cond_0

    .line 69
    invoke-direct {p0, p1, p3}, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2, p6}, Lcom/sec/chaton/calllog/view/layout/List_DetailGroupCallLog;->a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/lang/String;ZZ)V

    goto :goto_0
.end method
