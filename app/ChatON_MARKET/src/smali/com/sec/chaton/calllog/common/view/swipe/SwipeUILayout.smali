.class public Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;
.super Landroid/widget/FrameLayout;
.source "SwipeUILayout.java"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/TextView;

.field private i:Z

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->i:Z

    .line 57
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/content/Context;)V

    .line 59
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a()V

    .line 61
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 65
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 67
    const v1, 0x7f03014c

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 69
    const v0, 0x7f070006

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a:Landroid/view/View;

    .line 71
    const v0, 0x7f070543

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    .line 73
    const v0, 0x7f070544

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c:Landroid/view/View;

    .line 75
    const v0, 0x7f070545

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->d:Landroid/view/View;

    .line 77
    const v0, 0x7f070007

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->e:Landroid/view/View;

    .line 79
    const v0, 0x7f070547

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->f:Landroid/widget/ImageView;

    .line 81
    const v0, 0x7f070546

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->g:Landroid/widget/ImageView;

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 85
    const v0, 0x7f070548

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->h:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 97
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 125
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v4, p2

    move v3, v1

    move v5, v1

    move v6, v2

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 129
    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 131
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 135
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 219
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    sub-int v1, p1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 221
    iget v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->j:I

    sub-int/2addr v1, p1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 223
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 225
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 227
    iget v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->j:I

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    add-int/2addr v2, v3

    sub-int/2addr v2, p1

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 229
    iget v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->j:I

    sub-int v2, p1, v2

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 231
    iget-object v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 233
    iget-boolean v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->i:Z

    if-eqz v1, :cond_0

    .line 235
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b037c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    :goto_0
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    mul-int/lit8 v1, v1, 0x2

    if-le p1, v1, :cond_1

    .line 245
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->h:Landroid/widget/TextView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 255
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->f:Landroid/widget/ImageView;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 257
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-le p1, v1, :cond_2

    .line 269
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->e:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 279
    return-void

    .line 239
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0379

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 249
    :cond_1
    int-to-float v1, p1

    const/high16 v2, 0x40000000    # 2.0f

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    .line 251
    iget-object v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_1

    .line 263
    :cond_2
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    sub-int/2addr v1, p1

    mul-int/lit16 v1, v1, 0x100

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    div-int v0, v1, v0

    goto :goto_2
.end method

.method private c(I)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 285
    iget v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->j:I

    sub-int/2addr v1, p1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 287
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    sub-int v1, p1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 289
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 291
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 293
    iget v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->j:I

    sub-int v2, p1, v2

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 295
    iget v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->j:I

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    add-int/2addr v2, v3

    sub-int/2addr v2, p1

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 297
    iget-object v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->e:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 299
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b035c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    mul-int/lit8 v1, v1, 0x2

    if-le p1, v1, :cond_0

    .line 303
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->h:Landroid/widget/TextView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 315
    :goto_0
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-le p1, v1, :cond_1

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 327
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 329
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 337
    return-void

    .line 307
    :cond_0
    int-to-float v1, p1

    const/high16 v2, 0x40000000    # 2.0f

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    .line 309
    iget-object v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0

    .line 321
    :cond_1
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    sub-int/2addr v1, p1

    mul-int/lit16 v1, v1, 0x100

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    div-int v0, v1, v0

    .line 323
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 109
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 111
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 113
    iget v0, v1, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->j:I

    .line 115
    return-void
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 139
    float-to-int v0, p1

    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->j:I

    sub-int/2addr v0, v1

    .line 141
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 143
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 145
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 147
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->d:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 149
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->e:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 151
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 187
    if-lez p1, :cond_0

    .line 189
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 191
    neg-int v1, p1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 193
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b(I)V

    .line 205
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207
    return-void

    .line 197
    :cond_0
    neg-int v1, p1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 199
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 201
    neg-int v1, p1

    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c(I)V

    goto :goto_0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->j:I

    add-int/2addr v0, v1

    return v0
.end method

.method public b(F)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->j:I

    add-int/2addr v0, v1

    float-to-int v1, p1

    sub-int/2addr v0, v1

    .line 157
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 159
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 161
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 163
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->d:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 165
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->e:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 167
    return-void
.end method

.method public c(F)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->c:Landroid/view/View;

    neg-float v1, p1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->b:Landroid/view/View;

    neg-float v1, p1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a:Landroid/view/View;

    neg-float v1, p1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->d:Landroid/view/View;

    neg-float v1, p1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->e:Landroid/view/View;

    neg-float v1, p1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->a(Landroid/view/View;I)V

    .line 181
    return-void
.end method

.method public setCenterMessage(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    return-void
.end method

.method public setVideoEventUI(Z)V
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeUILayout;->i:Z

    .line 103
    return-void
.end method
