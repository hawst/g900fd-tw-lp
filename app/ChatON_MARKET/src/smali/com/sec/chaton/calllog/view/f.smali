.class Lcom/sec/chaton/calllog/view/f;
.super Landroid/os/Handler;
.source "CallLogDetailFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V
    .locals 0

    .prologue
    .line 1792
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const v7, 0x7f0b00be

    const v6, 0x7f0b0037

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 1796
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/chaton/a/a/f;

    .line 1798
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->f(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1851
    :goto_0
    return-void

    .line 1802
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->g(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    .line 1804
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1805
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_2

    .line 1806
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->h(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1807
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1809
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b027d

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v4}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->h(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->a(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Ljava/lang/CharSequence;)V

    .line 1810
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->b(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1811
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->d(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)V

    .line 1812
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/j;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/j;->c()V

    goto :goto_0

    .line 1814
    :cond_2
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/j/o;->b:Lcom/sec/chaton/j/o;

    if-ne v1, v2, :cond_3

    .line 1815
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v2}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->i(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Lcom/sec/chaton/buddy/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/buddy/a/c;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1819
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->f(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1821
    :cond_3
    const-string v1, ""

    .line 1823
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ADD BUDDY RESULT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CallLogDetailActivity"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e83

    if-ne v1, v2, :cond_4

    .line 1825
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1834
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->f(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0

    .line 1826
    :cond_4
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v1

    const/16 v2, 0x3e84

    if-ne v1, v2, :cond_5

    .line 1827
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1828
    :cond_5
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->f()I

    move-result v0

    const/16 v1, 0x3e85

    if-ne v0, v1, :cond_6

    .line 1829
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1831
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1838
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->f(Lcom/sec/chaton/calllog/view/CallLogDetailFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0065

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/calllog/view/g;

    invoke-direct {v2, p0}, Lcom/sec/chaton/calllog/view/g;-><init>(Lcom/sec/chaton/calllog/view/f;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/f;->a:Lcom/sec/chaton/calllog/view/CallLogDetailFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/CallLogDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto/16 :goto_0
.end method
