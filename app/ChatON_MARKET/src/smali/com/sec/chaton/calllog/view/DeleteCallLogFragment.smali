.class public Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;
.super Landroid/support/v4/app/Fragment;
.source "DeleteCallLogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/app/Dialog;

.field private c:Landroid/app/ProgressDialog;

.field private d:Landroid/widget/RelativeLayout;

.field private e:Landroid/widget/CheckBox;

.field private f:Landroid/widget/ListView;

.field private g:Lcom/sec/chaton/calllog/view/a;

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/sec/chaton/calllog/view/u;

.field private final k:Lcom/sec/chaton/calllog/manager/a;

.field private final l:Lcom/sec/chaton/calllog/view/aa;

.field private m:Landroid/view/Menu;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 49
    iput-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a:Landroid/view/View;

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    .line 163
    sget-object v0, Lcom/sec/chaton/calllog/view/CallLogFragment;->f:Lcom/sec/chaton/calllog/manager/a;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->k:Lcom/sec/chaton/calllog/manager/a;

    .line 166
    new-instance v0, Lcom/sec/chaton/calllog/view/aa;

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/calllog/view/aa;-><init>(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;Lcom/sec/chaton/calllog/view/w;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->l:Lcom/sec/chaton/calllog/view/aa;

    .line 595
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->d()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 198
    const-string v0, "[DeleteCallLogActivity]"

    invoke-static {p1, v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 203
    const-string v0, "[DeleteCallLogActivity]"

    invoke-static {p1, v0}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)Lcom/sec/chaton/calllog/view/aa;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->l:Lcom/sec/chaton/calllog/view/aa;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const v5, 0x7f0b0357

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 86
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 87
    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    .line 93
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    new-array v0, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v5, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 95
    const v0, 0x7f0b037e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 106
    :goto_0
    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 107
    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    .line 110
    const v0, 0x7f0b035a

    new-instance v1, Lcom/sec/chaton/calllog/view/w;

    invoke-direct {v1, p0}, Lcom/sec/chaton/calllog/view/w;-><init>(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 121
    const v0, 0x7f0b003a

    new-instance v1, Lcom/sec/chaton/calllog/view/x;

    invoke-direct {v1, p0}, Lcom/sec/chaton/calllog/view/x;-><init>(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    .line 129
    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->b(Z)Lcom/sec/common/a/a;

    .line 132
    new-instance v0, Lcom/sec/chaton/calllog/view/y;

    invoke-direct {v0, p0}, Lcom/sec/chaton/calllog/view/y;-><init>(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V

    invoke-virtual {v2, v0}, Lcom/sec/common/a/a;->a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/sec/common/a/a;

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 144
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b:Landroid/app/Dialog;

    .line 146
    :cond_1
    invoke-virtual {v2}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b:Landroid/app/Dialog;

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 149
    return-void

    .line 97
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 98
    const v0, 0x7f0b0356

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 99
    const v0, 0x7f0b0378

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_3
    new-array v0, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v5, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 102
    const v0, 0x7f0b0377

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 176
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 177
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 183
    instance-of v3, v0, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-nez v3, :cond_0

    .line 184
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 179
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 190
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<CIH> : \'mCheckedList.size()\' = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->k:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/calllog/manager/a;->b(Ljava/util/ArrayList;)I

    .line 194
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 215
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a:Landroid/view/View;

    const v1, 0x7f070140

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->d:Landroid/widget/RelativeLayout;

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a:Landroid/view/View;

    const v1, 0x7f070141

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a:Landroid/view/View;

    const v1, 0x7f0700ee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->f:Landroid/widget/ListView;

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->k:Lcom/sec/chaton/calllog/manager/a;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->l:Lcom/sec/chaton/calllog/view/aa;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/manager/a;->a(Landroid/os/Handler;)V

    .line 224
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Z)V

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->d:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/chaton/calllog/view/z;

    invoke-direct {v1, p0}, Lcom/sec/chaton/calllog/view/z;-><init>(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 256
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)Lcom/sec/chaton/calllog/view/a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g:Lcom/sec/chaton/calllog/view/a;

    return-object v0
.end method

.method private f()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 261
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 262
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<CIH> : \'intent\' = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 263
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "from_intent"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 264
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->k:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/manager/a;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<CIH> : \'where\' = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 267
    if-eqz v9, :cond_3

    .line 268
    invoke-static {v5}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 274
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 300
    const-string v0, "<CIH> : Here is a access restriction area!"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Ljava/lang/String;)V

    .line 304
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 305
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/ab;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/ab;->c()V

    .line 332
    :goto_1
    return-void

    .line 276
    :pswitch_0
    const-string v1, "<CIH> : \'where\' = FROM_BUDDY_PROFILE_ACTIVITY"

    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 277
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "profile_userno"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 278
    new-instance v3, Lcom/sec/chaton/calllog/view/v;

    invoke-direct {v3}, Lcom/sec/chaton/calllog/view/v;-><init>()V

    iput-object v3, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->j:Lcom/sec/chaton/calllog/view/u;

    .line 279
    iget-object v3, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->k:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v3, v0, v1, v5}, Lcom/sec/chaton/calllog/manager/a;->a(JZ)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    .line 280
    new-instance v0, Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->l:Lcom/sec/chaton/calllog/view/aa;

    iget-object v6, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->j:Lcom/sec/chaton/calllog/view/u;

    iget-object v7, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/calllog/view/a;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/HashMap;Landroid/os/Handler;Lcom/sec/chaton/calllog/view/u;Ljava/util/List;Lcom/sec/chaton/calllog/common/view/swipe/e;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g:Lcom/sec/chaton/calllog/view/a;

    goto :goto_0

    .line 284
    :pswitch_1
    const-string v0, "<CIH> : \'where\' = FROM_CALLLOG_ACTIVITY"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 285
    new-instance v0, Lcom/sec/chaton/calllog/view/v;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/view/v;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->j:Lcom/sec/chaton/calllog/view/u;

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->k:Lcom/sec/chaton/calllog/manager/a;

    invoke-static {}, Lcom/sec/chaton/calllog/view/CallLogFragment;->b()I

    move-result v1

    invoke-virtual {v0, v1, v5}, Lcom/sec/chaton/calllog/manager/a;->a(IZ)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    .line 287
    new-instance v0, Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->l:Lcom/sec/chaton/calllog/view/aa;

    iget-object v6, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->j:Lcom/sec/chaton/calllog/view/u;

    iget-object v7, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/calllog/view/a;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/HashMap;Landroid/os/Handler;Lcom/sec/chaton/calllog/view/u;Ljava/util/List;Lcom/sec/chaton/calllog/common/view/swipe/e;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g:Lcom/sec/chaton/calllog/view/a;

    goto :goto_0

    .line 291
    :pswitch_2
    const-string v1, "<CIH> : \'where\' = FROM_CALLLOG_DETAIL_ACTIVITY"

    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 292
    const-string v1, "calllogdata"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 293
    invoke-virtual {v0}, Lcom/sec/chaton/calllog/manager/model/CallLogData;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 294
    iget-object v3, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->k:Lcom/sec/chaton/calllog/manager/a;

    invoke-virtual {v3, v1, v5}, Lcom/sec/chaton/calllog/manager/a;->a(Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    .line 295
    new-instance v1, Lcom/sec/chaton/calllog/view/l;

    invoke-direct {v1, v0}, Lcom/sec/chaton/calllog/view/l;-><init>(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V

    iput-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->j:Lcom/sec/chaton/calllog/view/u;

    .line 296
    new-instance v0, Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->l:Lcom/sec/chaton/calllog/view/aa;

    iget-object v6, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->j:Lcom/sec/chaton/calllog/view/u;

    iget-object v7, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-direct/range {v0 .. v8}, Lcom/sec/chaton/calllog/view/a;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/HashMap;Landroid/os/Handler;Lcom/sec/chaton/calllog/view/u;Ljava/util/List;Lcom/sec/chaton/calllog/common/view/swipe/e;)V

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g:Lcom/sec/chaton/calllog/view/a;

    goto/16 :goto_0

    .line 308
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<CIH> : \'mCallLogList.size()\' = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    move v0, v2

    .line 312
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 313
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 323
    :cond_1
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g:Lcom/sec/chaton/calllog/view/a;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/view/a;->a(Ljava/util/List;)V

    .line 326
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g:Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_1

    .line 329
    :cond_3
    const-string v0, "<CIH> : <<<<<< intent \'where\' is Emptry >>>>>>"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Ljava/lang/String;)V

    .line 330
    const-string v0, "<CIH> : Here is a access restriction area!"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 274
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private g()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 335
    move v1, v2

    move v3, v2

    move v4, v2

    .line 336
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 337
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 338
    instance-of v5, v0, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-nez v5, :cond_0

    .line 339
    add-int/lit8 v4, v4, 0x1

    .line 342
    :cond_0
    instance-of v0, v0, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 343
    add-int/lit8 v3, v3, 0x1

    .line 336
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 347
    :cond_2
    if-gt v4, v3, :cond_3

    .line 348
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 353
    :goto_1
    if-nez v3, :cond_4

    .line 354
    invoke-virtual {p0, v2}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Z)V

    .line 358
    :goto_2
    return-void

    .line 350
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 356
    :cond_4
    invoke-virtual {p0, v6}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Z)V

    goto :goto_2
.end method

.method static synthetic g(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g()V

    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 580
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->c:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 581
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b035b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->c:Landroid/app/ProgressDialog;

    .line 585
    :goto_0
    return-void

    .line 583
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i()V

    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 591
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->f()V

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g:Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/a;->notifyDataSetChanged()V

    .line 173
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0705a6

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->m:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->m:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->m:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 211
    :cond_0
    return-void
.end method

.method public b()I
    .locals 3

    .prologue
    .line 383
    const/4 v1, 0x0

    .line 384
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 386
    add-int/lit8 v0, v1, 0x1

    .line 384
    :goto_1
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 389
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public b(Z)V
    .locals 3

    .prologue
    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<CIH> \'mCheckedList.size()\' = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g:Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {v1}, Lcom/sec/chaton/calllog/view/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 368
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g:Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/a;->a()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/CallLogData;

    .line 370
    instance-of v0, v0, Lcom/sec/chaton/calllog/manager/model/SeperatorCallLog;

    if-nez v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->i:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 368
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 374
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g:Lcom/sec/chaton/calllog/view/a;

    invoke-virtual {v0}, Lcom/sec/chaton/calllog/view/a;->notifyDataSetChanged()V

    .line 375
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 435
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 463
    :goto_0
    return-void

    .line 437
    :pswitch_0
    const-string v0, "<CIH> Select all button is pressed"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 440
    invoke-virtual {p0, v2}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Z)V

    .line 445
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Z)V

    goto :goto_0

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 443
    invoke-virtual {p0, v1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Z)V

    goto :goto_1

    .line 449
    :pswitch_1
    const-string v0, "<CIH> Select all layout is pressed"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 451
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 452
    invoke-virtual {p0, v1}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Z)V

    .line 457
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Z)V

    goto :goto_0

    .line 454
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 455
    invoke-virtual {p0, v2}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a(Z)V

    goto :goto_2

    .line 435
    :pswitch_data_0
    .packed-switch 0x7f070140
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 425
    const-string v0, "<CIH> onConfigurationChanged()"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 426
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 428
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a()V

    .line 429
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 479
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 480
    const v0, 0x7f0f0023

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 481
    iput-object p1, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->m:Landroid/view/Menu;

    .line 482
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 483
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 396
    const v0, 0x7f030035

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a:Landroid/view/View;

    .line 398
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->e()V

    .line 399
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->f()V

    .line 400
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 402
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->a:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 413
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 414
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 415
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 419
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 420
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 421
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 495
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 510
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 497
    :sswitch_0
    const-string v0, "<CIH> Back button is pressed"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 498
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/chaton/calllog/view/a;->a(Z)V

    .line 499
    invoke-virtual {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/view/ab;

    invoke-interface {v0}, Lcom/sec/chaton/calllog/view/ab;->c()V

    goto :goto_0

    .line 503
    :sswitch_1
    const-string v0, "<CIH> Done button is pressed"

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->b(Ljava/lang/String;)V

    .line 504
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->c()V

    goto :goto_0

    .line 495
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0705a6 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 488
    const v0, 0x7f0705a6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 490
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 407
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 408
    invoke-direct {p0}, Lcom/sec/chaton/calllog/view/DeleteCallLogFragment;->g()V

    .line 409
    return-void
.end method
