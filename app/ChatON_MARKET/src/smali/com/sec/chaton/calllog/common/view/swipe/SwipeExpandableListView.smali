.class public Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;
.super Landroid/widget/ExpandableListView;
.source "SwipeExpandableListView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/sec/chaton/calllog/common/view/swipe/e;


# instance fields
.field private a:Lcom/sec/chaton/calllog/common/view/swipe/b;

.field private b:Lcom/sec/chaton/calllog/common/view/swipe/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {p1}, Landroid/view/View;->performLongClick()Z

    .line 199
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 165
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    invoke-interface {v1, p1, v0, p2}, Lcom/sec/chaton/calllog/common/view/swipe/f;->a(Landroid/view/View;II)V

    .line 169
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/chaton/calllog/common/view/swipe/b;)Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    if-nez v0, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 152
    :goto_0
    return v0

    .line 150
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    .line 152
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 180
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 184
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 125
    invoke-virtual {p0, p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 127
    invoke-super {p0}, Landroid/widget/ExpandableListView;->onFinishInflate()V

    .line 129
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 58
    iget-object v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    if-nez v2, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v0

    .line 64
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 66
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 108
    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->setFastScrollAlwaysVisible(Z)V

    goto :goto_0

    .line 72
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    if-eqz v1, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    invoke-interface {v0, p2}, Lcom/sec/chaton/calllog/common/view/swipe/b;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 86
    :pswitch_2
    iget-object v2, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    if-eqz v2, :cond_2

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    invoke-interface {v0, p2}, Lcom/sec/chaton/calllog/common/view/swipe/b;->a(Landroid/view/MotionEvent;)Z

    move v0, v1

    .line 92
    goto :goto_0

    .line 96
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    .line 100
    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->setFastScrollAlwaysVisible(Z)V

    goto :goto_0

    .line 66
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setOnSwipeListener(Lcom/sec/chaton/calllog/common/view/swipe/f;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeExpandableListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    .line 47
    return-void
.end method
