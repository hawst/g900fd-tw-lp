.class public Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;
.super Landroid/widget/ListView;
.source "SwipeListView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/sec/chaton/calllog/common/view/swipe/e;


# instance fields
.field private a:Lcom/sec/chaton/calllog/common/view/swipe/b;

.field private b:Lcom/sec/chaton/calllog/common/view/swipe/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 192
    invoke-virtual {p1}, Landroid/view/View;->performLongClick()Z

    .line 196
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 158
    invoke-virtual {p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 162
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    invoke-interface {v1, p1, v0, p2}, Lcom/sec/chaton/calllog/common/view/swipe/f;->a(Landroid/view/View;II)V

    .line 166
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/chaton/calllog/common/view/swipe/b;)Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    if-nez v0, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 149
    :goto_0
    return v0

    .line 147
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    .line 149
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 173
    invoke-virtual {p0, p1}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 177
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 181
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 122
    invoke-virtual {p0, p0}, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 124
    invoke-super {p0}, Landroid/widget/ListView;->onFinishInflate()V

    .line 126
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 75
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 95
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    if-eqz v1, :cond_2

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    invoke-interface {v0, p2}, Lcom/sec/chaton/calllog/common/view/swipe/b;->a(Landroid/view/MotionEvent;)Z

    .line 101
    const/4 v0, 0x1

    goto :goto_0

    .line 81
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    if-eqz v1, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    invoke-interface {v0, p2}, Lcom/sec/chaton/calllog/common/view/swipe/b;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 105
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->a:Lcom/sec/chaton/calllog/common/view/swipe/b;

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOnSwipeListener(Lcom/sec/chaton/calllog/common/view/swipe/f;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/chaton/calllog/common/view/swipe/SwipeListView;->b:Lcom/sec/chaton/calllog/common/view/swipe/f;

    .line 48
    return-void
.end method
