.class public Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;
.super Landroid/widget/RelativeLayout;
.source "SearchLayout.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ImageButton;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    iput-object p1, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->e:Landroid/content/Context;

    .line 65
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a(Landroid/content/Context;)V

    .line 67
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 115
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 71
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 73
    const v1, 0x7f030144

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 75
    const v0, 0x7f070516

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    .line 77
    const v0, 0x7f070517

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->b:Landroid/view/View;

    .line 79
    const v0, 0x7f070518

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->c:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f070149

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->d:Landroid/widget/ImageButton;

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 87
    invoke-direct {p0}, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a()V

    .line 89
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 146
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    return v0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 199
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 213
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public setDefaultLanguageEnglish()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    const-string v1, "defaultInputmode=english;"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method public setEditText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 221
    return-void
.end method

.method public setHintText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    return-void
.end method

.method public setInputEmailArrdessType()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 184
    return-void
.end method

.method public setInputNumberType(Z)V
    .locals 2

    .prologue
    .line 160
    if-eqz p1, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 176
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    goto :goto_0
.end method

.method public setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/chaton/calllog/common/view/layout/SearchLayout;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 133
    return-void
.end method
