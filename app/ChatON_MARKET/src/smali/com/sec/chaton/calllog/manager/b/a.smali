.class public final Lcom/sec/chaton/calllog/manager/b/a;
.super Ljava/lang/Object;
.source "CallLogStringUtil.java"


# static fields
.field private static a:Lcom/sec/chaton/calllog/manager/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/sec/chaton/calllog/manager/b/a;

    invoke-direct {v0}, Lcom/sec/chaton/calllog/manager/b/a;-><init>()V

    sput-object v0, Lcom/sec/chaton/calllog/manager/b/a;->a:Lcom/sec/chaton/calllog/manager/b/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/util/Date;Ljava/util/Date;)J
    .locals 8

    .prologue
    const-wide/32 v6, 0xa8c0

    const-wide/16 v4, 0x3e8

    .line 97
    .line 100
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    div-long/2addr v0, v4

    .line 101
    invoke-direct {p0, p2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    div-long/2addr v2, v4

    .line 103
    sub-long v0, v2, v0

    .line 105
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 106
    add-long/2addr v0, v6

    .line 111
    :goto_0
    const-wide/32 v2, 0x15180

    div-long/2addr v0, v2

    .line 113
    return-wide v0

    .line 108
    :cond_0
    sub-long/2addr v0, v6

    goto :goto_0
.end method

.method public static a()Lcom/sec/chaton/calllog/manager/b/a;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/chaton/calllog/manager/b/a;->a:Lcom/sec/chaton/calllog/manager/b/a;

    return-object v0
.end method

.method private a(Ljava/util/Date;)Ljava/util/Date;
    .locals 5

    .prologue
    const-wide/16 v3, 0x3e8

    const/4 v1, 0x0

    .line 118
    instance-of v0, p1, Ljava/sql/Date;

    if-eqz v0, :cond_0

    .line 126
    :goto_0
    return-object p1

    .line 121
    :cond_0
    invoke-virtual {p1}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 122
    invoke-virtual {v0, v1}, Ljava/util/Date;->setHours(I)V

    .line 123
    invoke-virtual {v0, v1}, Ljava/util/Date;->setMinutes(I)V

    .line 124
    invoke-virtual {v0, v1}, Ljava/util/Date;->setSeconds(I)V

    .line 125
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    div-long/2addr v1, v3

    mul-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    move-object p1, v0

    .line 126
    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 168
    :cond_0
    return-object p1
.end method

.method public static c(Lcom/sec/chaton/calllog/manager/model/CallLogData;)I
    .locals 4

    .prologue
    const v1, 0x7f0201b2

    const/4 v3, 0x0

    .line 444
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    iget v2, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myCalllogtype:I

    .line 446
    iget v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myDuration:I

    .line 453
    :goto_0
    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 477
    :goto_1
    return v0

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget v2, v0, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->k:I

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 455
    goto :goto_1

    .line 458
    :pswitch_1
    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_1

    .line 459
    const v0, 0x7f0201b5

    goto :goto_1

    .line 461
    :cond_1
    const v0, 0x7f0201b3

    goto :goto_1

    .line 465
    :pswitch_2
    const v0, 0x7f0201b6

    goto :goto_1

    .line 468
    :pswitch_3
    const v0, 0x7f0201b7

    goto :goto_1

    .line 471
    :pswitch_4
    const v0, 0x7f0201b4

    goto :goto_1

    .line 474
    :pswitch_5
    const v0, 0x7f0200eb

    goto :goto_1

    .line 453
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 71
    const-string v0, "yyyyMMddHHmmss"

    .line 74
    :try_start_0
    const-string v0, "yyyyMMddHHmmss"

    invoke-virtual {p0, p1, v0}, Lcom/sec/chaton/calllog/manager/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 75
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 76
    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/util/Date;Ljava/util/Date;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 81
    :goto_0
    return-wide v0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 81
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 136
    const-string v1, "00"

    .line 137
    const-string v0, "00"

    .line 138
    const-string v0, "00"

    .line 140
    div-int/lit8 v0, p1, 0x3c

    .line 142
    const/16 v2, 0x3c

    if-lt v0, v2, :cond_0

    .line 143
    div-int/lit8 v1, v0, 0x3c

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 144
    rem-int/lit8 v0, v0, 0x3c

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 148
    :goto_0
    rem-int/lit8 v2, p1, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 151
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v1}, Lcom/sec/chaton/calllog/manager/b/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/sec/chaton/calllog/manager/b/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, v2}, Lcom/sec/chaton/calllog/manager/b/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    return-object v0

    .line 146
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 178
    iget v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->callmethod:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    .line 179
    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->rejectmsg:Ljava/lang/String;

    .line 214
    :goto_0
    return-object v0

    .line 182
    :cond_0
    const-string v0, "Reject message is empty"

    goto :goto_0

    .line 186
    :cond_1
    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupcallkey:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_2

    .line 187
    iget v1, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myCalllogtype:I

    .line 188
    iget v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->myDuration:I

    .line 195
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 214
    const-string v0, "Unknown"

    goto :goto_0

    .line 191
    :cond_2
    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget v1, v0, Lcom/sec/chaton/calllog/manager/model/c;->l:I

    .line 192
    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    iget v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->k:I

    goto :goto_1

    .line 199
    :pswitch_0
    if-eqz v0, :cond_3

    .line 200
    invoke-static {v0}, Lcom/sec/chaton/util/co;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 203
    :cond_3
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0362

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 207
    :pswitch_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0370

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 211
    :pswitch_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0371

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/sec/chaton/calllog/manager/model/CallLogData;Ljava/util/HashMap;Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/calllog/manager/model/CallLogData;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/calllog/manager/model/c;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 415
    const/4 v1, 0x0

    .line 418
    iget-wide v2, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->groupid:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    .line 423
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 424
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    .line 427
    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 429
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 431
    invoke-virtual {p2, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 439
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    if-eqz p2, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    :cond_0
    const-string v0, ""

    .line 46
    :goto_0
    return-object v0

    .line 39
    :cond_1
    const-string v0, "yyyyMMddHHmmss"

    .line 42
    :try_start_0
    const-string v0, "yyyyMMddHHmmss"

    invoke-virtual {p0, p1, v0}, Lcom/sec/chaton/calllog/manager/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 43
    invoke-virtual {p0, v0, p2}, Lcom/sec/chaton/calllog/manager/b/a;->a(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 44
    :catch_0
    move-exception v0

    .line 45
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 46
    const-string v0, ""

    goto :goto_0
.end method

.method public a(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {v0, p2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/sec/chaton/calllog/manager/model/CallLogData;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 268
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 270
    iget-object v0, p1, Lcom/sec/chaton/calllog/manager/model/CallLogData;->userInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/manager/model/c;

    .line 271
    iget-object v4, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 275
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "chaton_id"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 279
    iget-object v4, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 293
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 294
    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    :cond_1
    iget-object v4, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 299
    :cond_2
    iget-object v4, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 300
    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 303
    :cond_3
    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 308
    :cond_4
    iget-object v0, v0, Lcom/sec/chaton/calllog/manager/model/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 316
    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {v0, p2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 226
    const-string v0, "yyyyMMddHHmmss"

    .line 229
    :try_start_0
    const-string v0, "yyyyMMddHHmmss"

    invoke-virtual {p0, p1, v0}, Lcom/sec/chaton/calllog/manager/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 230
    const-string v1, "yyyyMMddHHmmss"

    invoke-virtual {p0, p2, v1}, Lcom/sec/chaton/calllog/manager/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 231
    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v2

    invoke-virtual {v1}, Ljava/util/Date;->getYear()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Ljava/util/Date;->getMonth()I

    move-result v2

    invoke-virtual {v1}, Ljava/util/Date;->getMonth()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Ljava/util/Date;->getDate()I

    move-result v0

    invoke-virtual {v1}, Ljava/util/Date;->getDate()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ne v0, v1, :cond_0

    .line 234
    const/4 v0, 0x1

    .line 239
    :goto_0
    return v0

    .line 236
    :catch_0
    move-exception v0

    .line 237
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 239
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
