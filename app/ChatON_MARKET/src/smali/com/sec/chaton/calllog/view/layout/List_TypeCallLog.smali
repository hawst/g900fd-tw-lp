.class public Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;
.super Landroid/widget/RelativeLayout;
.source "List_TypeCallLog.java"


# instance fields
.field private a:Landroid/widget/RelativeLayout;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/ImageView;

.field private e:Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/CheckBox;

.field private k:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->a(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->a(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method private final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    const v0, 0x7f030130

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 83
    const v0, 0x7f0704ca

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->a:Landroid/widget/RelativeLayout;

    .line 84
    const v0, 0x7f0704d0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->b:Landroid/widget/ImageView;

    .line 85
    const v0, 0x7f0704cd

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->c:Landroid/widget/ImageView;

    .line 86
    const v0, 0x7f0704af

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->d:Landroid/widget/ImageView;

    .line 87
    const v0, 0x7f0704cb

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->e:Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;

    .line 88
    const v0, 0x7f0704cf

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->f:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0704ce

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->g:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0704d1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->h:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0704d2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->i:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0704cc

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->j:Landroid/widget/CheckBox;

    .line 93
    const v0, 0x7f0704d3

    invoke-virtual {p0, v0}, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->k:Landroid/widget/LinearLayout;

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 105
    return-void
.end method


# virtual methods
.method public final setCallCount(I)V
    .locals 3

    .prologue
    .line 169
    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final setCallMethod(I)V
    .locals 3

    .prologue
    const v0, 0x7f0200ee

    const/16 v2, 0x8

    .line 120
    .line 122
    packed-switch p1, :pswitch_data_0

    .line 144
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 146
    return-void

    .line 126
    :pswitch_1
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 131
    :pswitch_2
    const v0, 0x7f0200ed

    .line 132
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 136
    :pswitch_3
    const v0, 0x7f0200e8

    .line 137
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->d:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setCallType(Lcom/sec/chaton/calllog/manager/model/CallLogData;)V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->b:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/sec/chaton/calllog/manager/b/a;->c(Lcom/sec/chaton/calllog/manager/model/CallLogData;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 116
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 203
    return-void
.end method

.method public setCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 257
    return-void
.end method

.method public setDeleteMode(Z)V
    .locals 2

    .prologue
    .line 186
    if-eqz p1, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->j:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 192
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->j:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method public final setMsg(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    return-void
.end method

.method public final setName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    :cond_0
    return-void
.end method

.method public setProfileImage(ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 271
    if-eqz p1, :cond_1

    .line 272
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 273
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/bt;->a(Landroid/content/Context;)Lcom/sec/chaton/util/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->e:Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;

    invoke-virtual {v0, v1, p2}, Lcom/sec/chaton/util/bt;->b(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 304
    :goto_0
    return-void

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->e:Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;

    const v1, 0x7f0201bc

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->setImageResource(I)V

    goto :goto_0

    .line 284
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->e:Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;

    invoke-virtual {v0, p2}, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 285
    :catch_0
    move-exception v0

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->e:Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;

    const v1, 0x7f020387

    invoke-virtual {v0, v1}, Lcom/sec/chaton/calllog/buddy/view/BuddyImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public final setTime(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, -0x2

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 217
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 218
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 219
    const/16 v1, 0xb

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 220
    const/16 v1, 0x19

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 221
    iget-object v1, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 222
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/calllog/view/layout/List_TypeCallLog;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
