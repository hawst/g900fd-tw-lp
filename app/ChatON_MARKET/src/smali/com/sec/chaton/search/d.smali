.class public Lcom/sec/chaton/search/d;
.super Ljava/lang/Object;
.source "SearchRequest.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/net/Uri;

.field private c:Lcom/sec/chaton/search/f;

.field private d:[Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:[Ljava/lang/String;

.field private g:Z

.field private h:Lcom/sec/chaton/search/g;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/search/e;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/sec/chaton/search/e;->a(Lcom/sec/chaton/search/e;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->a:Landroid/content/Context;

    .line 38
    invoke-static {p1}, Lcom/sec/chaton/search/e;->b(Lcom/sec/chaton/search/e;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->b:Landroid/net/Uri;

    .line 39
    invoke-static {p1}, Lcom/sec/chaton/search/e;->c(Lcom/sec/chaton/search/e;)Lcom/sec/chaton/search/f;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->c:Lcom/sec/chaton/search/f;

    .line 40
    invoke-static {p1}, Lcom/sec/chaton/search/e;->d(Lcom/sec/chaton/search/e;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->d:[Ljava/lang/String;

    .line 41
    invoke-static {p1}, Lcom/sec/chaton/search/e;->e(Lcom/sec/chaton/search/e;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->e:Ljava/lang/String;

    .line 42
    invoke-static {p1}, Lcom/sec/chaton/search/e;->f(Lcom/sec/chaton/search/e;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->f:[Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcom/sec/chaton/search/e;->g(Lcom/sec/chaton/search/e;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/search/d;->g:Z

    .line 44
    invoke-static {p1}, Lcom/sec/chaton/search/e;->h(Lcom/sec/chaton/search/e;)Lcom/sec/chaton/search/g;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->h:Lcom/sec/chaton/search/g;

    .line 46
    invoke-virtual {p0}, Lcom/sec/chaton/search/d;->a()V

    .line 47
    invoke-virtual {p0}, Lcom/sec/chaton/search/d;->b()V

    .line 48
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/search/d;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/chaton/search/d;->b:Landroid/net/Uri;

    const-string v1, "limit"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->k:Ljava/lang/String;

    .line 53
    iget-object v0, p0, Lcom/sec/chaton/search/d;->b:Landroid/net/Uri;

    const-string v1, "stime"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->i:Ljava/lang/String;

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/search/d;->b:Landroid/net/Uri;

    const-string v1, "etime"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->j:Ljava/lang/String;

    .line 56
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/chaton/search/d;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Lcom/sec/chaton/search/a;

    invoke-direct {v0}, Lcom/sec/chaton/search/a;-><init>()V

    .line 61
    iget-object v1, p0, Lcom/sec/chaton/search/d;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/search/a;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/search/d;->l:[Ljava/lang/String;

    .line 63
    :cond_0
    return-void
.end method

.method public c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/search/d;->l:[Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/sec/chaton/search/f;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/chaton/search/d;->c:Lcom/sec/chaton/search/f;

    return-object v0
.end method

.method public e()Lcom/sec/chaton/search/g;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/chaton/search/d;->h:Lcom/sec/chaton/search/g;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/chaton/search/d;->i:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/chaton/search/d;->j:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/chaton/search/d;->k:Ljava/lang/String;

    return-object v0
.end method
