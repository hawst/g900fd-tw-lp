.class public Lcom/sec/chaton/search/e;
.super Ljava/lang/Object;
.source "SearchRequest.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/net/Uri;

.field private c:Lcom/sec/chaton/search/f;

.field private d:[Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:[Ljava/lang/String;

.field private g:Z

.field private h:Lcom/sec/chaton/search/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/sec/chaton/search/e;->a:Landroid/content/Context;

    .line 137
    iput-object p2, p0, Lcom/sec/chaton/search/e;->b:Landroid/net/Uri;

    .line 138
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/search/e;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/chaton/search/e;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/search/e;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/chaton/search/e;->b:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/search/e;)Lcom/sec/chaton/search/f;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/chaton/search/e;->c:Lcom/sec/chaton/search/f;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/search/e;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/chaton/search/e;->d:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/search/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/chaton/search/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/search/e;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/chaton/search/e;->f:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/search/e;)Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/sec/chaton/search/e;->g:Z

    return v0
.end method

.method static synthetic h(Lcom/sec/chaton/search/e;)Lcom/sec/chaton/search/g;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/chaton/search/e;->h:Lcom/sec/chaton/search/g;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/chaton/search/d;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/sec/chaton/search/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/search/d;-><init>(Lcom/sec/chaton/search/e;)V

    return-object v0
.end method

.method public a(Lcom/sec/chaton/search/f;)Lcom/sec/chaton/search/e;
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/chaton/search/e;->c:Lcom/sec/chaton/search/f;

    .line 142
    return-object p0
.end method

.method public a(Lcom/sec/chaton/search/g;)Lcom/sec/chaton/search/e;
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/chaton/search/e;->h:Lcom/sec/chaton/search/g;

    .line 167
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/chaton/search/e;
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/chaton/search/e;->e:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method public a(Z)Lcom/sec/chaton/search/e;
    .locals 0

    .prologue
    .line 161
    iput-boolean p1, p0, Lcom/sec/chaton/search/e;->g:Z

    .line 162
    return-object p0
.end method

.method public a([Ljava/lang/String;)Lcom/sec/chaton/search/e;
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/chaton/search/e;->d:[Ljava/lang/String;

    .line 147
    return-object p0
.end method

.method public b([Ljava/lang/String;)Lcom/sec/chaton/search/e;
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/chaton/search/e;->f:[Ljava/lang/String;

    .line 157
    return-object p0
.end method
