.class public Lcom/sec/chaton/account/i;
.super Ljava/lang/Object;
.source "ContactManager.java"


# static fields
.field private static a:Lcom/coolots/sso/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    sput-object v0, Lcom/sec/chaton/account/i;->a:Lcom/coolots/sso/a/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583
    return-void
.end method

.method public static a()I
    .locals 7

    .prologue
    .line 658
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 659
    const/4 v0, -0x1

    .line 660
    if-nez v1, :cond_0

    .line 670
    :goto_0
    return v0

    .line 663
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RawContacts Upgrade ACCOUNT_NAME "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ContactManager"

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 665
    const-string v2, "account_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "account_type=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "com.sec.chaton"

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 667
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RawContacts Upgrade result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ContactManager"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(J)J
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 59
    const-wide/16 v6, 0x0

    .line 60
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 62
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 63
    if-eqz v2, :cond_1

    .line 64
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "contact_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 67
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 69
    :goto_1
    return-wide v0

    :cond_0
    move-wide v0, v6

    goto :goto_0

    :cond_1
    move-wide v0, v6

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 548
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, "|"

    invoke-direct {v1, p0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    const-string v2, ""

    .line 550
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 551
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 552
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 554
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Buddy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    new-instance v0, Lcom/sec/chaton/account/h;

    invoke-direct {v0, p0, p1}, Lcom/sec/chaton/account/h;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 80
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 81
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 82
    return-void
.end method

.method private static a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 764
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CompareCursor_SyncStatusMessage() orgNum ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ContactManager"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    const/4 v1, 0x0

    .line 767
    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 769
    const/4 v0, -0x1

    :try_start_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 770
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 771
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 772
    const/4 v2, 0x1

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 773
    const/4 v2, 0x2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 774
    const/4 v2, 0x3

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 776
    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 782
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 783
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 784
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 786
    const-string v3, "presence_data_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    const-string v0, "status"

    invoke-virtual {v2, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    const-string v0, "status_res_package"

    const-string v3, "com.sec.chaton"

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    const-string v0, "status_label"

    const v3, 0x7f0b0007

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 790
    const-string v0, "status_icon"

    const v3, 0x7f020168

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 791
    const-string v0, "status_ts"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 793
    sget-object v0, Landroid/provider/ContactsContract$StatusUpdates;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 805
    :cond_1
    :goto_1
    return-void

    .line 795
    :catch_0
    move-exception v0

    .line 796
    :try_start_1
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 797
    :catchall_0
    move-exception v0

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Z)V
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/account/i;->a(ZLjava/lang/String;)V

    .line 601
    return-void
.end method

.method private static a(ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 604
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 605
    const-string v1, "com.sec.chaton"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 608
    array-length v1, v0

    if-lez v1, :cond_2

    .line 609
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 611
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.sec.chaton"

    invoke-direct {v1, v0, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 613
    if-eqz p0, :cond_0

    .line 615
    const-string v2, "force"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 617
    :cond_0
    if-eqz p1, :cond_1

    .line 618
    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 620
    :cond_1
    const-string v2, "com.android.contacts"

    invoke-static {v1, v2, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 628
    :goto_0
    return-void

    .line 622
    :cond_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 623
    const-string v0, "requestSync. ksy is empty. ignore sync operation"

    const-string v1, "ContactManager"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    :cond_3
    invoke-static {}, Lcom/sec/chaton/account/i;->b()V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 953
    const/4 v0, 0x0

    .line 956
    :try_start_0
    sget-object v1, Lcom/sec/chaton/account/i;->a:Lcom/coolots/sso/a/a;

    invoke-virtual {v1, p0}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 960
    :goto_0
    return v0

    .line 957
    :catch_0
    move-exception v1

    .line 958
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 470
    const/4 v0, 0x0

    .line 472
    if-eqz p0, :cond_1

    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    .line 473
    const/4 v1, -0x1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 474
    :cond_0
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 475
    const-string v1, "data4"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 476
    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 477
    const/4 v0, 0x1

    .line 481
    goto :goto_0

    .line 483
    :catch_0
    move-exception v1

    .line 485
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 487
    :cond_1
    return v0
.end method

.method public static a(Lcom/sec/chaton/io/entry/inner/Buddy;)Z
    .locals 6

    .prologue
    const/16 v5, 0x13

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 442
    .line 444
    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v5, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    const-string v1, "10"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/Buddy;->msisdns:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 446
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 448
    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 449
    iget-object v0, p0, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/account/i;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 450
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 451
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v5, :cond_4

    .line 452
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    .line 455
    :cond_2
    if-le v1, v3, :cond_3

    move v2, v3

    .line 464
    :cond_3
    return v2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 512
    .line 514
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isAlreadyExistChatONV chatonid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sainfo = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ContactManager"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    :cond_0
    if-eqz p1, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v8, :cond_3

    :cond_1
    move v0, v6

    .line 543
    :cond_2
    :goto_0
    return v0

    .line 526
    :cond_3
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "data4"

    aput-object v4, v2, v3

    const-string v3, "data1=? AND data2=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p0, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 529
    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-lez v0, :cond_4

    move v0, v8

    .line 539
    :goto_1
    if-eqz v1, :cond_2

    .line 540
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_4
    move v0, v6

    .line 532
    goto :goto_1

    .line 535
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 537
    :goto_2
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 539
    if-eqz v1, :cond_5

    .line 540
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    move v0, v6

    .line 542
    goto :goto_0

    .line 539
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v7, :cond_6

    .line 540
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 539
    :cond_6
    throw v0

    :catchall_1
    move-exception v0

    move-object v7, v1

    goto :goto_3

    .line 535
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static b(Ljava/lang/String;)J
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 564
    const-wide/16 v7, 0x0

    .line 567
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/chaton/account/j;->a:[Ljava/lang/String;

    const-string v3, "account_type=\'com.sec.chaton\' AND sync1=? AND deleted=0"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 568
    if-eqz v1, :cond_1

    .line 569
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 570
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    .line 576
    :goto_0
    if-eqz v1, :cond_0

    .line 577
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 580
    :cond_0
    return-wide v2

    .line 573
    :cond_1
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 576
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_2

    .line 577
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 576
    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_3
    move-wide v2, v7

    goto :goto_0
.end method

.method private static b()V
    .locals 4

    .prologue
    .line 631
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    .line 632
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "msisdn"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 634
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 635
    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 636
    invoke-static {v0, v1}, Lcom/sec/chaton/account/i;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/chaton/account/i;->b(Z)V

    .line 644
    :cond_0
    :goto_0
    return-void

    .line 639
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 640
    const-string v0, "runRegiAccount. mMSISDN or mUid is empty. ignore sync operation"

    const-string v1, "ContactManager"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static declared-synchronized b(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/io/entry/inner/Buddy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    const-class v9, Lcom/sec/chaton/account/i;

    monitor-enter v9

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 87
    new-instance v10, Lcom/sec/chaton/account/a;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v2}, Lcom/sec/chaton/account/a;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;)V

    .line 90
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 91
    const-string v3, "com.sec.chaton"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v11

    .line 94
    const/4 v8, 0x0

    .line 95
    invoke-static/range {p0 .. p0}, Lcom/sec/chaton/account/i;->a(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v12

    .line 98
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "raw_contact_id"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "data1"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "data4"

    aput-object v6, v4, v5

    const-string v5, "data2=\'ChatON\' "

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v4

    .line 103
    :try_start_2
    array-length v2, v11

    if-lez v2, :cond_21

    .line 104
    const/4 v2, 0x0

    aget-object v2, v11, v2

    iget-object v7, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 105
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SyncKey name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ContactManager"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/io/entry/inner/Buddy;

    .line 109
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->deleted:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->blocked:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 111
    :cond_1
    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/chaton/account/k;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 383
    :catchall_0
    move-exception v2

    move-object v3, v4

    :goto_1
    if-eqz v3, :cond_2

    .line 384
    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 383
    :cond_2
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 86
    :catchall_1
    move-exception v2

    monitor-exit v9

    throw v2

    .line 131
    :cond_3
    :try_start_4
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/account/i;->b(Ljava/lang/String;)J

    move-result-wide v13

    .line 133
    invoke-static/range {p0 .. p0}, Lcom/sec/chaton/plugin/a;->a(Landroid/content/Context;)Z

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_9

    const/4 v3, 0x1

    if-ne v12, v3, :cond_9

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->sainfo:Ljava/lang/String;

    if-eqz v3, :cond_9

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->sainfo:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->einfo:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->einfo:Ljava/lang/String;

    const-string v5, "voip=1"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 136
    const/4 v3, 0x1

    .line 137
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "syncContacts() ChatON V available handset. isChatonV = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ContactManager"

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v3

    .line 141
    :goto_2
    const-wide/16 v15, 0x0

    cmp-long v3, v13, v15

    if-lez v3, :cond_6

    .line 175
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 176
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "orgnums is : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " orgnum is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v5, Lcom/sec/chaton/account/i;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 179
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "orgnums is null : buddy.value : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " deleted!!"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v5, Lcom/sec/chaton/account/i;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/account/k;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 201
    :cond_6
    :goto_3
    const-wide/16 v15, 0x0

    cmp-long v3, v13, v15

    if-lez v3, :cond_7

    invoke-static {v2}, Lcom/sec/chaton/account/i;->a(Lcom/sec/chaton/io/entry/inner/Buddy;)Z

    move-result v3

    if-nez v3, :cond_7

    if-eqz v6, :cond_8

    :cond_7
    const-wide/16 v15, -0x1

    cmp-long v3, v13, v15

    if-nez v3, :cond_c

    .line 336
    :cond_8
    :goto_4
    invoke-virtual {v10}, Lcom/sec/chaton/account/a;->a()I

    move-result v2

    const/16 v3, 0x12c

    if-le v2, v3, :cond_0

    .line 337
    invoke-virtual {v10}, Lcom/sec/chaton/account/a;->b()V

    goto/16 :goto_0

    .line 139
    :cond_9
    const/4 v3, 0x0

    move v6, v3

    goto/16 :goto_2

    .line 182
    :cond_a
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    iget-object v5, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    const-string v11, "+"

    const-string v15, ""

    invoke-virtual {v5, v11, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 184
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    const-string v5, "0"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 185
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "orgnums is null : buddy.value : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " deleted!!"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v5, Lcom/sec/chaton/account/i;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/account/k;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 190
    :cond_b
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    const-string v5, "@"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 191
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "orgnum is email address : buddy.orgnum : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " deleted!!"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v5, Lcom/sec/chaton/account/i;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/account/k;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    goto/16 :goto_3

    .line 213
    :cond_c
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    if-eqz v3, :cond_16

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_16

    .line 215
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 216
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/account/i;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 218
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 220
    const/4 v3, 0x0

    move v5, v3

    :goto_5
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v3

    if-ge v5, v3, :cond_8

    .line 221
    invoke-interface {v11, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v14, 0x13

    if-lt v3, v14, :cond_e

    .line 220
    :cond_d
    :goto_6
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_5

    .line 226
    :cond_e
    invoke-interface {v11, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/e/a/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 229
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v15, 0x1

    if-eq v3, v15, :cond_d

    .line 233
    invoke-static {v4, v14}, Lcom/sec/chaton/account/i;->a(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v15

    .line 234
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    iget-object v0, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->sainfo:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Lcom/sec/chaton/account/i;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v16

    .line 236
    invoke-interface {v11, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/e/a/g;->c(Ljava/lang/String;)J

    move-result-wide v17

    .line 238
    const/4 v3, 0x1

    if-ne v15, v3, :cond_f

    .line 239
    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_f

    .line 241
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "originalPhoneNumber = "

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v19, " is already exist with rawContactId : "

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v19, "ContactManager"

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_f
    if-nez v6, :cond_10

    const/4 v3, 0x1

    if-eq v15, v3, :cond_d

    .line 247
    :cond_10
    const/4 v3, 0x1

    if-ne v6, v3, :cond_11

    const/4 v3, 0x1

    move/from16 v0, v16

    if-eq v0, v3, :cond_d

    .line 249
    :cond_11
    const/4 v3, 0x1

    if-ne v15, v3, :cond_12

    const/4 v3, 0x1

    if-ne v6, v3, :cond_12

    if-nez v16, :cond_12

    .line 251
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/account/k;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 256
    :cond_12
    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/sec/chaton/account/i;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 257
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_d

    .line 258
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "isAlreadyExistByRawId rawContactId : "

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v14, "ContactManager"

    invoke-static {v3, v14}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 263
    :cond_13
    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v13, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 264
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_d

    .line 265
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "prevRawContactId.contains rawContactId : "

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v14, "ContactManager"

    invoke-static {v3, v14}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 271
    :cond_14
    const-wide/16 v15, 0x0

    cmp-long v3, v17, v15

    if-lez v3, :cond_d

    .line 272
    invoke-interface {v11, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/e/a/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 274
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v15

    const-string v16, "msisdn"

    const-string v19, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 275
    if-eqz v3, :cond_d

    .line 276
    invoke-virtual {v10}, Lcom/sec/chaton/account/a;->a()I

    move-result v16

    .line 277
    if-eqz v15, :cond_d

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_d

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    const-string v19, ""

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_d

    .line 279
    invoke-static {v2, v7}, Lcom/sec/chaton/account/k;->a(Lcom/sec/chaton/io/entry/inner/Buddy;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v15

    invoke-virtual {v10, v15}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 280
    move/from16 v0, v16

    invoke-static {v0, v3}, Lcom/sec/chaton/account/k;->a(ILjava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 281
    move/from16 v0, v16

    invoke-static {v2, v0, v14}, Lcom/sec/chaton/account/k;->a(Lcom/sec/chaton/io/entry/inner/Buddy;ILjava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 283
    const/4 v3, 0x1

    if-ne v6, v3, :cond_15

    .line 284
    move/from16 v0, v16

    invoke-static {v2, v0, v14}, Lcom/sec/chaton/account/k;->b(Lcom/sec/chaton/io/entry/inner/Buddy;ILjava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 287
    :cond_15
    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 292
    :cond_16
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    if-eqz v3, :cond_8

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 294
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v5, 0x13

    if-ge v3, v5, :cond_0

    .line 299
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/e/a/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 300
    invoke-static {v4, v3}, Lcom/sec/chaton/account/i;->a(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v5

    .line 301
    iget-object v11, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    iget-object v13, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->sainfo:Ljava/lang/String;

    invoke-static {v11, v13}, Lcom/sec/chaton/account/i;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 303
    const/4 v13, 0x1

    if-ne v5, v13, :cond_19

    if-eqz v6, :cond_19

    if-nez v11, :cond_19

    .line 305
    iget-object v11, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    invoke-static {v11}, Lcom/sec/chaton/account/k;->a(Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 310
    :cond_17
    iget-object v11, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-static {v11}, Lcom/sec/chaton/e/a/g;->c(Ljava/lang/String;)J

    move-result-wide v13

    .line 313
    const-wide/16 v15, 0x0

    cmp-long v11, v13, v15

    if-lez v11, :cond_8

    .line 314
    iget-object v11, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-static {v11}, Lcom/sec/chaton/e/a/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 316
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v13

    const-string v14, "msisdn"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 318
    if-eqz v11, :cond_8

    .line 319
    invoke-virtual {v10}, Lcom/sec/chaton/account/a;->a()I

    move-result v14

    .line 320
    if-eqz v13, :cond_8

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_8

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    const-string v15, ""

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_8

    .line 322
    if-nez v5, :cond_18

    .line 324
    invoke-static {v2, v7}, Lcom/sec/chaton/account/k;->a(Lcom/sec/chaton/io/entry/inner/Buddy;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v10, v5}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 325
    invoke-static {v14, v11}, Lcom/sec/chaton/account/k;->a(ILjava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v10, v5}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 326
    invoke-static {v2, v14, v3}, Lcom/sec/chaton/account/k;->a(Lcom/sec/chaton/io/entry/inner/Buddy;ILjava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v10, v5}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    .line 328
    :cond_18
    const/4 v5, 0x1

    if-ne v6, v5, :cond_8

    .line 329
    invoke-static {v2, v14, v3}, Lcom/sec/chaton/account/k;->b(Lcom/sec/chaton/io/entry/inner/Buddy;ILjava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/sec/chaton/account/a;->a(Landroid/content/ContentProviderOperation;)V

    goto/16 :goto_4

    .line 306
    :cond_19
    const/4 v11, 0x1

    if-ne v5, v11, :cond_17

    goto/16 :goto_0

    .line 340
    :cond_1a
    invoke-virtual {v10}, Lcom/sec/chaton/account/a;->a()I

    move-result v2

    if-lez v2, :cond_1b

    .line 341
    invoke-virtual {v10}, Lcom/sec/chaton/account/a;->b()V

    .line 345
    :cond_1b
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1c
    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_22

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/io/entry/inner/Buddy;

    .line 348
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->blocked:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1d

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->deleted:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 349
    :cond_1d
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    if-eqz v3, :cond_1e

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1e

    .line 351
    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/chaton/e/a/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 353
    invoke-static {v4, v2}, Lcom/sec/chaton/account/i;->c(Landroid/database/Cursor;Ljava/lang/String;)V

    goto :goto_7

    .line 354
    :cond_1e
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    if-eqz v3, :cond_1c

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1c

    .line 355
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 356
    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/chaton/account/i;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 357
    const/4 v2, 0x0

    move v3, v2

    :goto_8
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_1c

    .line 358
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/chaton/e/a/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 359
    invoke-static {v4, v2}, Lcom/sec/chaton/account/i;->c(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 357
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_8

    .line 363
    :cond_1f
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->status:Ljava/lang/String;

    if-eqz v3, :cond_1c

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    if-eqz v3, :cond_1c

    .line 364
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    if-eqz v3, :cond_20

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_20

    .line 365
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 366
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/account/i;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 367
    const/4 v3, 0x0

    move v5, v3

    :goto_9
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    if-ge v5, v3, :cond_1c

    .line 368
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/e/a/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 369
    iget-object v8, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->status:Ljava/lang/String;

    invoke-static {v4, v3, v8}, Lcom/sec/chaton/account/i;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_9

    .line 371
    :cond_20
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    if-eqz v3, :cond_1c

    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1c

    .line 372
    iget-object v3, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/chaton/e/a/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 373
    iget-object v2, v2, Lcom/sec/chaton/io/entry/inner/Buddy;->status:Ljava/lang/String;

    invoke-static {v4, v3, v2}, Lcom/sec/chaton/account/i;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 380
    :cond_21
    const-string v2, "samsungAccount is not exist "

    const-string v3, "ContactManager"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 383
    :cond_22
    if-eqz v4, :cond_23

    .line 384
    :try_start_5
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 387
    :cond_23
    monitor-exit v9

    return-void

    .line 383
    :catchall_2
    move-exception v2

    move-object v3, v8

    goto/16 :goto_1
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 647
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "ChatON"

    const-string v2, "com.sec.chaton"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 649
    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 650
    const-string v1, "com.android.contacts"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 652
    return-void
.end method

.method public static b(Z)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 912
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 913
    sget-object v1, Lcom/sec/chaton/e/i;->a:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 914
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 915
    if-eqz p0, :cond_0

    .line 916
    const-string v3, "Resync contact to Buddy (delete all contact)"

    const-string v4, "ContactManager"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "account_type=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "com.sec.chaton"

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 921
    :cond_0
    if-eqz v1, :cond_1

    .line 923
    const/4 v0, -0x1

    :try_start_0
    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 924
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 925
    new-instance v0, Lcom/sec/chaton/io/entry/inner/Buddy;

    invoke-direct {v0}, Lcom/sec/chaton/io/entry/inner/Buddy;-><init>()V

    .line 926
    const-string v3, "buddy_no"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->value:Ljava/lang/String;

    .line 927
    const-string v3, "buddy_orginal_number"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnum:Ljava/lang/String;

    .line 928
    const-string v3, "buddy_orginal_numbers"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->orgnums:Ljava/lang/String;

    .line 929
    const-string v3, "buddy_extra_info"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->einfo:Ljava/lang/String;

    .line 930
    const-string v3, "buddy_status_message"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->status:Ljava/lang/String;

    .line 931
    const-string v3, "buddy_sainfo"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/chaton/io/entry/inner/Buddy;->sainfo:Ljava/lang/String;

    .line 934
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 936
    :catch_0
    move-exception v0

    .line 937
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 940
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 946
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 948
    :cond_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/chaton/account/i;->a(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 949
    return-void

    .line 939
    :catchall_0
    move-exception v0

    .line 940
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 939
    :goto_2
    throw v0

    .line 941
    :catch_1
    move-exception v1

    .line 942
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 941
    :catch_2
    move-exception v0

    .line 942
    :goto_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 940
    :cond_2
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    .line 941
    :catch_3
    move-exception v0

    goto :goto_3
.end method

.method public static b(Landroid/database/Cursor;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 491
    const/4 v0, 0x0

    .line 493
    if-eqz p0, :cond_1

    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    .line 494
    const/4 v1, -0x1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 495
    :cond_0
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 496
    const-string v1, "raw_contact_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 497
    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 498
    const/4 v0, 0x1

    .line 502
    goto :goto_0

    .line 504
    :catch_0
    move-exception v1

    .line 506
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 508
    :cond_1
    return v0
.end method

.method private static c(Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 703
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CompareCursor_DeleteStatusMessage() orgNum ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ContactManager"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    if-eqz p0, :cond_0

    .line 706
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 708
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 710
    const-string v0, "data4"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 711
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 712
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 713
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 714
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 716
    const-string v3, "presence_data_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    const-string v0, "status"

    const-string v3, ""

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    sget-object v0, Landroid/provider/ContactsContract$StatusUpdates;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 720
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 728
    :cond_0
    :goto_0
    return-void

    .line 724
    :catch_0
    move-exception v0

    .line 725
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 595
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/sec/chaton/account/i;->a(ZLjava/lang/String;)V

    .line 596
    return-void
.end method
