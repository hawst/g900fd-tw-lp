.class Lcom/sec/chaton/account/d;
.super Lcom/sec/chaton/util/ar;
.source "ChatONAccountService.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/account/c;


# direct methods
.method constructor <init>(Lcom/sec/chaton/account/c;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/chaton/account/d;->a:Lcom/sec/chaton/account/c;

    invoke-direct {p0}, Lcom/sec/chaton/util/ar;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 326
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 328
    const-string v0, "push deregister : success"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/account/d;->a:Lcom/sec/chaton/account/c;

    invoke-static {v0}, Lcom/sec/chaton/account/c;->b(Lcom/sec/chaton/account/c;)Lcom/sec/chaton/d/ap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/account/d;->a:Lcom/sec/chaton/account/c;

    iget-object v1, p0, Lcom/sec/chaton/account/d;->a:Lcom/sec/chaton/account/c;

    invoke-static {v1}, Lcom/sec/chaton/account/c;->b(Lcom/sec/chaton/account/c;)Lcom/sec/chaton/d/ap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/account/d;->a:Lcom/sec/chaton/account/c;

    invoke-static {v2}, Lcom/sec/chaton/account/c;->a(Lcom/sec/chaton/account/c;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/chaton/d/ap;->a(Landroid/content/Context;)Lcom/sec/chaton/d/a/ak;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/account/c;->a(Lcom/sec/chaton/account/c;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;

    .line 346
    :goto_1
    return-void

    .line 331
    :cond_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 332
    const-string v0, "push deregister : fail"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 340
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/account/d;->a:Lcom/sec/chaton/account/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/account/c;->a(Lcom/sec/chaton/account/c;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/account/d;->a:Lcom/sec/chaton/account/c;

    iget-object v0, v0, Lcom/sec/chaton/account/c;->b:Landroid/os/Handler;

    const/16 v1, 0xca

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 342
    const-string v0, "onReceiveRemoveAccount : regControl is null."

    const-class v1, Lcom/sec/chaton/account/ChatONAccountService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
