.class Lcom/sec/chaton/account/c;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "ChatONAccountService.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Lcom/coolots/sso/a/c;


# instance fields
.field public a:Lcom/sec/chaton/util/ar;

.field public b:Landroid/os/Handler;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/chaton/d/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/chaton/d/a",
            "<*>;"
        }
    .end annotation
.end field

.field private e:Lcom/sec/chaton/d/ap;

.field private f:Lcom/sec/chaton/d/a/ak;

.field private g:Lcom/coolots/sso/a/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    .line 321
    new-instance v0, Lcom/sec/chaton/account/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/account/d;-><init>(Lcom/sec/chaton/account/c;)V

    iput-object v0, p0, Lcom/sec/chaton/account/c;->a:Lcom/sec/chaton/util/ar;

    .line 349
    new-instance v0, Lcom/sec/chaton/account/e;

    invoke-direct {v0, p0}, Lcom/sec/chaton/account/e;-><init>(Lcom/sec/chaton/account/c;)V

    iput-object v0, p0, Lcom/sec/chaton/account/c;->b:Landroid/os/Handler;

    .line 94
    iput-object p1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    .line 95
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/account/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/account/c;Lcom/sec/chaton/d/a/ak;)Lcom/sec/chaton/d/a/ak;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/chaton/account/c;->f:Lcom/sec/chaton/d/a/ak;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/account/c;)Lcom/sec/chaton/d/ap;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/chaton/account/c;->e:Lcom/sec/chaton/d/ap;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/account/c;)Lcom/sec/chaton/d/a/ak;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/chaton/account/c;->f:Lcom/sec/chaton/d/a/ak;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 197
    const-string v0, "destroyVAccount"

    const-class v1, Lcom/sec/chaton/account/ChatONAccountService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/account/c;->g:Lcom/coolots/sso/a/a;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/account/c;->g:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    invoke-virtual {v0, v1, v3}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 203
    iput-object v3, p0, Lcom/sec/chaton/account/c;->g:Lcom/coolots/sso/a/a;

    .line 204
    const-string v0, "destroyVAccount:mChatonV.setListener(null)"

    const-class v1, Lcom/sec/chaton/account/ChatONAccountService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_0
    return-void
.end method

.method public addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 106
    const-string v0, "Add Account"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 109
    const-string v1, "accountAuthenticatorResponse"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 110
    const-string v1, "authtokenType"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 113
    const-string v2, "intent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 115
    return-object v1
.end method

.method public b()V
    .locals 3

    .prologue
    .line 210
    const-string v0, "initVAccount"

    const-class v1, Lcom/sec/chaton/account/ChatONAccountService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-static {}, Lcom/sec/chaton/plugin/g;->a()Lcom/sec/chaton/plugin/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    sget-object v2, Lcom/sec/chaton/plugin/h;->a:Lcom/sec/chaton/plugin/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/plugin/g;->a(Landroid/content/Context;Lcom/sec/chaton/plugin/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/account/c;->g:Lcom/coolots/sso/a/a;

    if-nez v0, :cond_0

    .line 215
    new-instance v0, Lcom/coolots/sso/a/a;

    invoke-direct {v0}, Lcom/coolots/sso/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/account/c;->g:Lcom/coolots/sso/a/a;

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/account/c;->g:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/account/c;->g:Lcom/coolots/sso/a/a;

    iget-object v1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    invoke-virtual {v0, v1, p0}, Lcom/coolots/sso/a/a;->a(Landroid/content/Context;Lcom/coolots/sso/a/c;)V

    .line 219
    const-string v0, "initVAccount:mChatonV.setListener()"

    const-class v1, Lcom/sec/chaton/account/ChatONAccountService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_0
    return-void
.end method

.method public confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 120
    const-string v0, "Confirm Credentials"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const/4 v0, 0x0

    return-object v0
.end method

.method public editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 99
    const-string v0, "Edit Account Properties"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAccountRemovalAllowed(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 229
    invoke-super {p0, p1, p2}, Landroid/accounts/AbstractAccountAuthenticator;->getAccountRemovalAllowed(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;)Landroid/os/Bundle;

    move-result-object v0

    .line 232
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v1

    .line 234
    const/4 v2, -0x3

    if-eq v2, v1, :cond_0

    const/4 v2, -0x2

    if-ne v2, v1, :cond_3

    .line 235
    :cond_0
    const-string v1, "errorCode"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 236
    const-string v1, "errorMessage"

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0205

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v1, "booleanResult"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 238
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    .line 240
    new-instance v1, Lcom/sec/chaton/account/f;

    invoke-direct {v1, p0, v5}, Lcom/sec/chaton/account/f;-><init>(Lcom/sec/chaton/account/c;Lcom/sec/chaton/account/b;)V

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/account/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 288
    :cond_1
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAccountRemovalAllowed(): result = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_2
    return-object v0

    .line 246
    :cond_3
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ab;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 252
    iget-object v1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    .line 255
    if-eqz v0, :cond_1

    const-string v1, "booleanResult"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 256
    const-string v1, "booleanResult"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 258
    if-eqz v1, :cond_1

    .line 259
    iget-object v1, p0, Lcom/sec/chaton/account/c;->b:Landroid/os/Handler;

    invoke-static {v1}, Lcom/sec/chaton/d/ap;->a(Landroid/os/Handler;)Lcom/sec/chaton/d/ap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/account/c;->e:Lcom/sec/chaton/d/ap;

    .line 261
    invoke-static {}, Lcom/sec/chaton/d/ao;->a()Lcom/sec/chaton/d/a;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/account/c;->d:Lcom/sec/chaton/d/a;

    .line 265
    invoke-virtual {p0}, Lcom/sec/chaton/account/c;->b()V

    .line 268
    iget-object v1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/chaton/util/cb;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/chaton/account/c;->g:Lcom/coolots/sso/a/a;

    iget-object v2, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/coolots/sso/a/a;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 270
    iget-object v1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/chaton/account/c;->g:Lcom/coolots/sso/a/a;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/cb;->b(Landroid/content/Context;Lcom/coolots/sso/a/a;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 271
    iget-object v1, p0, Lcom/sec/chaton/account/c;->d:Lcom/sec/chaton/d/a;

    iget-object v2, p0, Lcom/sec/chaton/account/c;->a:Lcom/sec/chaton/util/ar;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 272
    iget-object v1, p0, Lcom/sec/chaton/account/c;->a:Lcom/sec/chaton/util/ar;

    const/16 v2, 0x7530

    invoke-virtual {v1, v2}, Lcom/sec/chaton/util/ar;->a(I)V

    goto/16 :goto_0

    .line 275
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/account/c;->e:Lcom/sec/chaton/d/ap;

    if-eqz v1, :cond_5

    .line 276
    iget-object v1, p0, Lcom/sec/chaton/account/c;->e:Lcom/sec/chaton/d/ap;

    iget-object v2, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/d/ap;->a(Landroid/content/Context;)Lcom/sec/chaton/d/a/ak;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/account/c;->f:Lcom/sec/chaton/d/a/ak;

    goto/16 :goto_0

    .line 278
    :cond_5
    iput-object v5, p0, Lcom/sec/chaton/account/c;->f:Lcom/sec/chaton/d/a/ak;

    .line 279
    iget-object v1, p0, Lcom/sec/chaton/account/c;->b:Landroid/os/Handler;

    const/16 v2, 0xca

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 280
    const-string v1, "onReceiveRemoveAccount : regControl is null."

    const-class v2, Lcom/sec/chaton/account/ChatONAccountService;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 126
    const-string v0, "get Auth Token"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v0, "com.sec.chaton"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 129
    const-string v1, "errorMessage"

    const-string v2, "invalid authTokenType"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :goto_0
    return-object v0

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 133
    invoke-virtual {v0, p2}, Landroid/accounts/AccountManager;->getPassword(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    .line 135
    if-eqz v1, :cond_1

    .line 136
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 138
    const-string v2, "authAccount"

    iget-object v3, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v2, "accountType"

    const-string v3, "com.sec.chaton"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v2, "authtoken"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 145
    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    const-string v0, "username"

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    const-string v0, "authtokenType"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    const-string v0, "accountAuthenticatorResponse"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 150
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 151
    const-string v2, "intent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    const-string v0, "get Auth Token label"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v0, "com.sec.chaton"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 179
    const-string v0, "has features"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 181
    const-string v1, "booleanResult"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 182
    return-object v0
.end method

.method public onReceiveCreateAccount(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 387
    return-void
.end method

.method public onReceiveRemoveAccount(Z)V
    .locals 2

    .prologue
    .line 392
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceiveRemoveAccount"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/account/ChatONAccountService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget-object v0, p0, Lcom/sec/chaton/account/c;->d:Lcom/sec/chaton/d/a;

    iget-object v1, p0, Lcom/sec/chaton/account/c;->a:Lcom/sec/chaton/util/ar;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/d/a;->b(Landroid/os/Handler;)V

    .line 396
    iget-object v0, p0, Lcom/sec/chaton/account/c;->a:Lcom/sec/chaton/util/ar;

    const/16 v1, 0x7530

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ar;->a(I)V

    .line 412
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 187
    const-string v0, "onServiceConnected"

    const-class v1, Lcom/sec/chaton/account/ChatONAccountService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 193
    const-string v0, "onServiceDisconnected"

    const-class v1, Lcom/sec/chaton/account/ChatONAccountService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    return-void
.end method

.method public updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 166
    const-string v0, "update Credentials"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/account/c;->c:Landroid/content/Context;

    const-class v2, Lcom/sec/chaton/registration/AuthenticatorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 168
    const-string v1, "username"

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 169
    const-string v1, "authtokenType"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    const-string v1, "confirmCredentials"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 172
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 173
    const-string v2, "intent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 174
    return-object v1
.end method
