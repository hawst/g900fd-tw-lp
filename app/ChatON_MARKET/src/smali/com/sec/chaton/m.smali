.class public Lcom/sec/chaton/m;
.super Landroid/os/AsyncTask;
.source "ApplicationInitializeTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 42
    const-class v0, Lcom/sec/chaton/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/m;->a:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/m;->b:Ljava/util/List;

    .line 48
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 66
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v7

    .line 67
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 69
    invoke-static {}, Lcom/sec/common/util/log/collector/c;->d()V

    .line 71
    invoke-static {v7}, Lcom/sec/chaton/e/aw;->a(Landroid/content/Context;)Lcom/sec/chaton/e/aw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/e/aw;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 74
    invoke-static {v0}, Lcom/sec/chaton/e/a/q;->a(Landroid/content/ContentResolver;)I

    .line 77
    const-string v1, "com.sec.chaton.provider2"

    invoke-static {}, Lcom/sec/chaton/e/a/j;->a()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-static {v7, v1, v2}, Lcom/sec/chaton/util/al;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/ContentProviderOperation;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :try_start_1
    sget-object v1, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "install"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "item_id"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "data1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, " IS NULL OR "

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "data2"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, " IS NULL OR "

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "data3"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, " IS NULL"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 92
    if-eqz v1, :cond_1

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 94
    const-string v0, "Retrieve anicon package list."

    iget-object v2, p0, Lcom/sec/chaton/m;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    const/4 v0, 0x0

    invoke-static {v7, v0}, Lcom/sec/chaton/d/j;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/j;

    move-result-object v0

    .line 97
    sget-object v2, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    const-string v3, "0"

    const-string v4, "zip"

    const/16 v5, 0xf0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/chaton/d/j;->a(Lcom/sec/chaton/d/a/b;Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/chaton/d/a/cd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/cd;->b()Lcom/sec/chaton/a/a/f;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v0, v2, :cond_4

    .line 100
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 101
    const-string v0, "Success retrieve anicon package list."

    iget-object v2, p0, Lcom/sec/chaton/m;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 110
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    .line 111
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 116
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "primary_translation_address"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V

    .line 117
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "secondary_translation_address"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    .line 124
    :cond_3
    :goto_1
    return-object v6

    .line 104
    :cond_4
    :try_start_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v0, :cond_1

    .line 105
    const-string v0, "Fail retrieve anicon package list."

    iget-object v2, p0, Lcom/sec/chaton/m;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 110
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_5

    .line 111
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    .line 118
    :catch_0
    move-exception v0

    .line 119
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 120
    iget-object v1, p0, Lcom/sec/chaton/m;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 110
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method public a(Lcom/sec/chaton/n;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/m;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    return-void
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 131
    iget-object v0, p0, Lcom/sec/chaton/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/n;

    .line 132
    invoke-interface {v0}, Lcom/sec/chaton/n;->a()V

    goto :goto_0

    .line 134
    :cond_0
    return-void
.end method

.method public b(Lcom/sec/chaton/n;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/m;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/m;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 41
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/m;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 61
    return-void
.end method
