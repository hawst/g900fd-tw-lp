.class public final Lcom/sec/chaton/c/g;
.super Ljava/lang/Object;
.source "NetworkConstant.java"


# static fields
.field private static a:Ljava/lang/String;


# direct methods
.method public static a()V
    .locals 1

    .prologue
    .line 341
    const-string v0, ""

    sput-object v0, Lcom/sec/chaton/c/g;->a:Ljava/lang/String;

    .line 342
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 333
    sput-object p0, Lcom/sec/chaton/c/g;->a:Ljava/lang/String;

    .line 334
    sget-object v0, Lcom/sec/chaton/c/b;->c:Ljava/util/HashMap;

    const-string v1, "XXX"

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    sget-object v0, Lcom/sec/chaton/c/b;->d:Ljava/util/HashMap;

    const-string v1, "XXX"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    sget-object v0, Lcom/sec/chaton/c/b;->e:Ljava/util/HashMap;

    const-string v1, "XXX"

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    sget-object v0, Lcom/sec/chaton/c/b;->f:Ljava/util/HashMap;

    const-string v1, "XXX"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    return-void
.end method

.method public static b()I
    .locals 2

    .prologue
    .line 345
    sget-object v0, Lcom/sec/chaton/c/g;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    sget-object v0, Lcom/sec/chaton/c/b;->c:Ljava/util/HashMap;

    const-string v1, "XXX"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 351
    :goto_0
    return v0

    .line 348
    :cond_0
    sget-object v0, Lcom/sec/chaton/c/b;->c:Ljava/util/HashMap;

    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349
    sget-object v0, Lcom/sec/chaton/c/b;->c:Ljava/util/HashMap;

    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 351
    :cond_1
    const/16 v0, 0x4e20

    goto :goto_0
.end method

.method public static c()I
    .locals 2

    .prologue
    .line 357
    sget-object v0, Lcom/sec/chaton/c/g;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 358
    sget-object v0, Lcom/sec/chaton/c/b;->d:Ljava/util/HashMap;

    const-string v1, "XXX"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 363
    :goto_0
    return v0

    .line 360
    :cond_0
    sget-object v0, Lcom/sec/chaton/c/b;->d:Ljava/util/HashMap;

    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 361
    sget-object v0, Lcom/sec/chaton/c/b;->d:Ljava/util/HashMap;

    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 363
    :cond_1
    const/16 v0, 0x7530

    goto :goto_0
.end method

.method public static d()I
    .locals 2

    .prologue
    .line 369
    sget-object v0, Lcom/sec/chaton/c/g;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 370
    sget-object v0, Lcom/sec/chaton/c/b;->e:Ljava/util/HashMap;

    const-string v1, "XXX"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 375
    :goto_0
    return v0

    .line 372
    :cond_0
    sget-object v0, Lcom/sec/chaton/c/b;->e:Ljava/util/HashMap;

    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 373
    sget-object v0, Lcom/sec/chaton/c/b;->e:Ljava/util/HashMap;

    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 375
    :cond_1
    const/16 v0, 0x4e20

    goto :goto_0
.end method

.method public static e()I
    .locals 2

    .prologue
    .line 381
    sget-object v0, Lcom/sec/chaton/c/g;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    sget-object v0, Lcom/sec/chaton/c/b;->f:Ljava/util/HashMap;

    const-string v1, "XXX"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 387
    :goto_0
    return v0

    .line 384
    :cond_0
    sget-object v0, Lcom/sec/chaton/c/b;->f:Ljava/util/HashMap;

    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 385
    sget-object v0, Lcom/sec/chaton/c/b;->f:Ljava/util/HashMap;

    invoke-static {}, Lcom/sec/chaton/util/am;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 387
    :cond_1
    const/16 v0, 0x7530

    goto :goto_0
.end method
