.class public Lcom/sec/chaton/c/a;
.super Ljava/lang/Object;
.source "ChatONConst.java"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Z

.field public static e:Z

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:I

.field public static i:I

.field public static final j:Landroid/net/Uri;

.field public static k:I

.field public static l:I

.field public static m:I

.field private static n:Z

.field private static o:Z

.field private static p:Z

.field private static q:[Ljava/lang/CharSequence;

.field private static r:[Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x320

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 85
    const-string v0, "failed"

    sput-object v0, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    .line 89
    const-string v0, "com.sec.chaton.service.DELETE_DIR_ACTION"

    sput-object v0, Lcom/sec/chaton/c/a;->c:Ljava/lang/String;

    .line 109
    sput-boolean v5, Lcom/sec/chaton/c/a;->n:Z

    .line 111
    sput-boolean v4, Lcom/sec/chaton/c/a;->d:Z

    .line 112
    sput-boolean v4, Lcom/sec/chaton/c/a;->e:Z

    .line 121
    :try_start_0
    const-string v0, "com.google.android.maps.MapActivity"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    sput-boolean v4, Lcom/sec/chaton/c/a;->o:Z

    .line 141
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 143
    const-string v1, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.android.app.myfiles"

    const-string v3, "com.sec.android.app.myfiles.fileselector.SingleSelectorActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_0

    .line 149
    sput-boolean v5, Lcom/sec/chaton/c/a;->o:Z

    .line 170
    :cond_0
    sput-boolean v5, Lcom/sec/chaton/c/a;->p:Z

    .line 223
    :try_start_1
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 224
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 225
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v0, Lcom/sec/chaton/c/a;->a:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 248
    :goto_1
    const-string v0, "IMAGE_PREVIEW_SAVE"

    sput-object v0, Lcom/sec/chaton/c/a;->f:Ljava/lang/String;

    .line 249
    const-string v0, "IMAGE_PREVIEW_SEND"

    sput-object v0, Lcom/sec/chaton/c/a;->g:Ljava/lang/String;

    .line 251
    const/4 v0, 0x5

    sput v0, Lcom/sec/chaton/c/a;->h:I

    .line 252
    sput v5, Lcom/sec/chaton/c/a;->i:I

    .line 261
    const-string v0, "content://com.sec.badge/apps"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/c/a;->j:Landroid/net/Uri;

    .line 278
    sput v6, Lcom/sec/chaton/c/a;->k:I

    .line 279
    const/16 v0, 0x258

    sput v0, Lcom/sec/chaton/c/a;->l:I

    .line 280
    const v0, 0x7d000

    sput v0, Lcom/sec/chaton/c/a;->m:I

    .line 284
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_2

    .line 289
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    if-le v1, v2, :cond_3

    .line 290
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    .line 291
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    .line 296
    :goto_2
    if-gt v1, v6, :cond_1

    const/16 v1, 0x1e0

    if-le v0, v1, :cond_2

    .line 297
    :cond_1
    const/16 v0, 0x400

    sput v0, Lcom/sec/chaton/c/a;->k:I

    .line 298
    const/16 v0, 0x300

    sput v0, Lcom/sec/chaton/c/a;->l:I

    .line 302
    :cond_2
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 124
    const-string v0, "ChatON"

    const-string v1, "Google MAP API NOT available"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    sput-boolean v4, Lcom/sec/chaton/c/a;->n:Z

    goto/16 :goto_0

    .line 293
    :cond_3
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 294
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    goto :goto_2

    .line 226
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 131
    sget-boolean v0, Lcom/sec/chaton/c/a;->n:Z

    return v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 155
    sget-boolean v0, Lcom/sec/chaton/c/a;->o:Z

    return v0
.end method

.method public static c()Z
    .locals 2

    .prologue
    .line 162
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SNOTE_PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public static d()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 178
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/chaton/c/a;->p:Z

    .line 179
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/c/a;->q:[Ljava/lang/CharSequence;

    .line 180
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/c/a;->r:[Ljava/lang/CharSequence;

    .line 182
    invoke-static {}, Lcom/sec/chaton/util/am;->t()Ljava/lang/String;

    move-result-object v3

    .line 183
    const/4 v0, 0x0

    .line 184
    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 185
    invoke-static {}, Lcom/sec/chaton/util/am;->d()Ljava/lang/String;

    move-result-object v0

    .line 188
    :cond_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    .line 189
    :goto_0
    sget-object v4, Lcom/sec/chaton/c/a;->q:[Ljava/lang/CharSequence;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 190
    sget-object v4, Lcom/sec/chaton/c/a;->q:[Ljava/lang/CharSequence;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 191
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Facebook is not available : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/c/a;->q:[Ljava/lang/CharSequence;

    aget-object v1, v4, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ChatONConst"

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    sput-boolean v2, Lcom/sec/chaton/c/a;->p:Z

    .line 200
    :cond_1
    sget-boolean v1, Lcom/sec/chaton/c/a;->p:Z

    if-eqz v1, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    .line 201
    :goto_1
    sget-object v3, Lcom/sec/chaton/c/a;->q:[Ljava/lang/CharSequence;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 202
    sget-object v3, Lcom/sec/chaton/c/a;->q:[Ljava/lang/CharSequence;

    aget-object v3, v3, v1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Facebook is not available : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/c/a;->q:[Ljava/lang/CharSequence;

    aget-object v1, v3, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatONConst"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    sput-boolean v2, Lcom/sec/chaton/c/a;->p:Z

    .line 212
    :cond_2
    return-void

    .line 195
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Facebook is not available : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/c/a;->q:[Ljava/lang/CharSequence;

    aget-object v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ChatONConst"

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FacebookAvailableCheck() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ChatONConst"

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 207
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Facebook is not available2 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/c/a;->q:[Ljava/lang/CharSequence;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ChatONConst"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FacebookAvailableCheck() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ChatONConst"

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 216
    sget-boolean v0, Lcom/sec/chaton/c/a;->p:Z

    return v0
.end method
