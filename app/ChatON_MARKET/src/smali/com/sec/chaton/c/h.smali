.class public Lcom/sec/chaton/c/h;
.super Ljava/lang/Object;
.source "SkinMessageControl.java"


# instance fields
.field private a:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lcom/sec/chaton/c/h;->a:Landroid/os/Handler;

    .line 43
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/d/a/b;Ljava/lang/String;IILjava/lang/String;)Lcom/sec/chaton/d/a/av;
    .locals 6

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 78
    const-string v1, "pa"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    const/4 v0, 0x1

    .line 84
    :cond_0
    :goto_0
    new-instance v1, Lcom/sec/chaton/j/j;

    sget-object v2, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    const-string v3, "/skin/download"

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v2, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    invoke-virtual {v1, v2}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v3

    const-string v4, "uid"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "platform"

    const-string v3, "android"

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "skinid"

    invoke-virtual {v1, v2, p2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "stwidth"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "stheight"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "rwidth"

    invoke-static {}, Lcom/sec/common/util/i;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "rheight"

    invoke-static {}, Lcom/sec/common/util/i;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v1

    const-string v2, "bgtype"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x44d

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/io/a/b;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->b(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/io/entry/DownloadSkinEntry;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 90
    new-instance v1, Lcom/sec/chaton/d/a/av;

    iget-object v2, p0, Lcom/sec/chaton/c/h;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v1, v2, v0, p1}, Lcom/sec/chaton/d/a/av;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/d/a/b;)V

    .line 91
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/w;->c()Lcom/sec/chaton/j/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 92
    return-object v1

    .line 80
    :cond_1
    const-string v1, "ma"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    const/4 v0, 0x2

    goto/16 :goto_0
.end method

.method public a(Lcom/sec/chaton/d/a/b;IIII)Lcom/sec/chaton/d/a/ca;
    .locals 7

    .prologue
    .line 46
    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->a()I

    move-result v0

    .line 47
    invoke-static {}, Lcom/sec/chaton/settings/downloads/cd;->a()I

    move-result v1

    .line 49
    new-instance v2, Lcom/sec/chaton/j/j;

    sget-object v3, Lcom/sec/chaton/util/cg;->b:Lcom/sec/chaton/util/cg;

    const-string v4, "/skin"

    invoke-direct {v2, v3, v4}, Lcom/sec/chaton/j/j;-><init>(Lcom/sec/chaton/util/cg;Ljava/lang/String;)V

    sget-object v3, Lcom/sec/chaton/j/k;->a:Lcom/sec/chaton/j/k;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/j/j;->a(Lcom/sec/chaton/j/k;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "uid"

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v4

    const-string v5, "uid"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "imei"

    invoke-static {}, Lcom/sec/chaton/util/am;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "platform"

    const-string v4, "android"

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "osversion"

    sget-object v4, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "model"

    invoke-static {}, Lcom/sec/chaton/util/am;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "lang"

    invoke-static {}, Lcom/sec/chaton/util/am;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "pvwidth"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "pvheight"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "dvwidth"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "dvheight"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v2

    const-string v3, "stwidth"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v2, "stheight"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "rwidth"

    invoke-static {}, Lcom/sec/common/util/i;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "rheight"

    invoke-static {}, Lcom/sec/common/util/i;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-string v1, "bgtype"

    const-string v2, "pa_ma"

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const/16 v1, 0x44c

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(I)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/io/a/b;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->b(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    const-class v1, Lcom/sec/chaton/io/entry/GetListSkinEntry;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/j;->a(Ljava/lang/Class;)Lcom/sec/chaton/j/j;

    move-result-object v0

    .line 69
    new-instance v1, Lcom/sec/chaton/d/a/ca;

    iget-object v2, p0, Lcom/sec/chaton/c/h;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/sec/chaton/j/j;->a()Lcom/sec/chaton/j/h;

    move-result-object v0

    invoke-direct {v1, v2, v0, p1}, Lcom/sec/chaton/d/a/ca;-><init>(Landroid/os/Handler;Lcom/sec/chaton/j/h;Lcom/sec/chaton/d/a/b;)V

    .line 70
    invoke-static {}, Lcom/sec/chaton/j/w;->a()Lcom/sec/chaton/j/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/j/w;->c()Lcom/sec/chaton/j/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/l;->a(Lcom/sec/chaton/d/a/a;)V

    .line 72
    return-object v1
.end method
