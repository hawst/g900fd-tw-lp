.class public Lcom/sec/chaton/UpgradeDialog;
.super Lcom/sec/chaton/registration/AbstractUpgradeDialog;
.source "UpgradeDialog.java"


# instance fields
.field private final b:Ljava/lang/String;

.field private c:Landroid/support/v4/content/LocalBroadcastManager;

.field private d:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;-><init>()V

    .line 23
    const-class v0, Lcom/sec/chaton/UpgradeDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/UpgradeDialog;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 73
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 74
    const-string v0, "initializeView"

    iget-object v1, p0, Lcom/sec/chaton/UpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/chaton/UpgradeDialog;->b(I)V

    .line 77
    invoke-virtual {p0, p0, v2}, Lcom/sec/chaton/UpgradeDialog;->a(Landroid/content/Context;I)V

    .line 78
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 31
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 32
    const-string v0, "onCreate"

    iget-object v1, p0, Lcom/sec/chaton/UpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/UpgradeDialog;->c:Landroid/support/v4/content/LocalBroadcastManager;

    .line 36
    new-instance v0, Lcom/sec/chaton/bz;

    invoke-direct {v0, p0}, Lcom/sec/chaton/bz;-><init>(Lcom/sec/chaton/UpgradeDialog;)V

    iput-object v0, p0, Lcom/sec/chaton/UpgradeDialog;->d:Landroid/content/BroadcastReceiver;

    .line 44
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 45
    const-string v1, "com.sec.chaton.ACTION_DISMISS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 46
    iget-object v1, p0, Lcom/sec/chaton/UpgradeDialog;->c:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v2, p0, Lcom/sec/chaton/UpgradeDialog;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 48
    invoke-super {p0, p1}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->onCreate(Landroid/os/Bundle;)V

    .line 49
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->onDestroy()V

    .line 55
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "onDestroy"

    iget-object v1, p0, Lcom/sec/chaton/UpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/UpgradeDialog;->c:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v1, p0, Lcom/sec/chaton/UpgradeDialog;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 59
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/sec/chaton/registration/AbstractUpgradeDialog;->onResume()V

    .line 65
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "onResume"

    iget-object v1, p0, Lcom/sec/chaton/UpgradeDialog;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    return-void
.end method
