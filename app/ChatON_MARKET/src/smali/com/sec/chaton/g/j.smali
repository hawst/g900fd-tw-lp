.class Lcom/sec/chaton/g/j;
.super Ljava/lang/Object;
.source "FacebookManager.java"

# interfaces
.implements Lcom/facebook/a/c;


# instance fields
.field final synthetic a:Lcom/sec/chaton/g/a;

.field private b:I

.field private c:Lcom/sec/chaton/g/h;


# direct methods
.method private constructor <init>(Lcom/sec/chaton/g/a;ILcom/sec/chaton/g/h;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/sec/chaton/g/j;->a:Lcom/sec/chaton/g/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340
    iput p2, p0, Lcom/sec/chaton/g/j;->b:I

    .line 341
    iput-object p3, p0, Lcom/sec/chaton/g/j;->c:Lcom/sec/chaton/g/h;

    .line 342
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/g/a;ILcom/sec/chaton/g/h;Lcom/sec/chaton/g/b;)V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/g/j;-><init>(Lcom/sec/chaton/g/a;ILcom/sec/chaton/g/h;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 353
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Login Request:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/g/j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Canceled."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/g/a;->a(Ljava/lang/String;)V

    .line 354
    iget-object v0, p0, Lcom/sec/chaton/g/j;->a:Lcom/sec/chaton/g/a;

    invoke-static {v0}, Lcom/sec/chaton/g/a;->b(Lcom/sec/chaton/g/a;)Lcom/sec/chaton/settings/tellfriends/common/c;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/g/j;->b:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 355
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Login Request:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/g/j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Done."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/g/a;->a(Ljava/lang/String;)V

    .line 348
    iget-object v0, p0, Lcom/sec/chaton/g/j;->a:Lcom/sec/chaton/g/a;

    invoke-static {v0}, Lcom/sec/chaton/g/a;->a(Lcom/sec/chaton/g/a;)V

    .line 349
    return-void
.end method

.method public a(Lcom/facebook/a/a;)V
    .locals 4

    .prologue
    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Login Request:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/g/j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Webview Error. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/a/a;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/g/a;->b(Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/sec/chaton/g/j;->a:Lcom/sec/chaton/g/a;

    invoke-static {v0}, Lcom/sec/chaton/g/a;->b(Lcom/sec/chaton/g/a;)Lcom/sec/chaton/settings/tellfriends/common/c;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/g/j;->b:I

    const/16 v2, -0x3ea

    invoke-virtual {p1}, Lcom/facebook/a/a;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 367
    return-void
.end method

.method public a(Lcom/facebook/a/d;)V
    .locals 4

    .prologue
    .line 359
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Login Request:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/g/j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Facebook Error. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/a/d;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/g/a;->b(Ljava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/sec/chaton/g/j;->a:Lcom/sec/chaton/g/a;

    invoke-static {v0}, Lcom/sec/chaton/g/a;->b(Lcom/sec/chaton/g/a;)Lcom/sec/chaton/settings/tellfriends/common/c;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/g/j;->b:I

    const/16 v2, -0x3e9

    invoke-virtual {p1}, Lcom/facebook/a/d;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/chaton/settings/tellfriends/common/c;->a(IILjava/lang/Object;)V

    .line 361
    return-void
.end method
