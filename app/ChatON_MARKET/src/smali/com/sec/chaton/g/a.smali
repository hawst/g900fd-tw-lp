.class public Lcom/sec/chaton/g/a;
.super Ljava/lang/Object;
.source "FacebookManager.java"


# instance fields
.field a:Landroid/os/Handler;

.field private final b:Ljava/lang/Object;

.field private c:Landroid/app/Activity;

.field private d:Lcom/sec/chaton/settings/tellfriends/common/c;

.field private e:Lcom/facebook/b/o;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/am;",
            "Lcom/facebook/aw;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/facebook/FacebookManager$BlockingTasksExecutor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/sec/chaton/settings/tellfriends/common/c;)V
    .locals 4

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/g/a;->b:Ljava/lang/Object;

    .line 115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    .line 740
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/g/a;->g:Ljava/util/ArrayList;

    .line 988
    new-instance v0, Lcom/sec/chaton/g/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/g/g;-><init>(Lcom/sec/chaton/g/a;)V

    iput-object v0, p0, Lcom/sec/chaton/g/a;->a:Landroid/os/Handler;

    .line 126
    iput-object p1, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    .line 127
    iput-object p2, p0, Lcom/sec/chaton/g/a;->d:Lcom/sec/chaton/settings/tellfriends/common/c;

    .line 129
    new-instance v0, Lcom/facebook/b/o;

    iget-object v1, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    new-instance v2, Lcom/sec/chaton/g/i;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/chaton/g/i;-><init>(Lcom/sec/chaton/g/a;Lcom/sec/chaton/g/b;)V

    invoke-direct {v0, v1, v2}, Lcom/facebook/b/o;-><init>(Landroid/content/Context;Lcom/facebook/bn;)V

    iput-object v0, p0, Lcom/sec/chaton/g/a;->e:Lcom/facebook/b/o;

    .line 130
    invoke-direct {p0}, Lcom/sec/chaton/g/a;->h()Lcom/facebook/ba;

    .line 131
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 432
    invoke-static {p0}, Lcom/facebook/a/e;->a(Landroid/content/Context;)V

    .line 434
    return-void
.end method

.method private a(Lcom/facebook/ba;Lcom/facebook/bs;Ljava/lang/Exception;Lcom/facebook/a/c;)V
    .locals 5

    .prologue
    .line 209
    invoke-virtual {p1}, Lcom/facebook/ba;->a()Landroid/os/Bundle;

    move-result-object v2

    .line 211
    sget-object v0, Lcom/facebook/bs;->d:Lcom/facebook/bs;

    if-ne p2, v0, :cond_2

    .line 212
    const/4 v1, 0x0

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/g/a;->e:Lcom/facebook/b/o;

    invoke-virtual {v0}, Lcom/facebook/b/o;->a()Lcom/facebook/ba;

    move-result-object v0

    .line 215
    iget-object v3, p0, Lcom/sec/chaton/g/a;->b:Ljava/lang/Object;

    monitor-enter v3

    .line 216
    if-eq p1, v0, :cond_5

    .line 218
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/g/a;->e:Lcom/facebook/b/o;

    invoke-virtual {v1, p1}, Lcom/facebook/b/o;->a(Lcom/facebook/ba;)V

    .line 220
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {v0}, Lcom/facebook/ba;->h()V

    .line 226
    :cond_0
    invoke-interface {p4, v2}, Lcom/facebook/a/c;->a(Landroid/os/Bundle;)V

    .line 244
    :cond_1
    :goto_1
    return-void

    .line 220
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 227
    :cond_2
    if-eqz p3, :cond_1

    .line 228
    instance-of v0, p3, Lcom/facebook/aa;

    if-eqz v0, :cond_3

    .line 229
    invoke-interface {p4}, Lcom/facebook/a/c;->a()V

    goto :goto_1

    .line 230
    :cond_3
    instance-of v0, p3, Lcom/facebook/w;

    if-eqz v0, :cond_4

    if-eqz v2, :cond_4

    const-string v0, "com.facebook.sdk.WebViewErrorCode"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "com.facebook.sdk.FailingUrl"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 234
    new-instance v0, Lcom/facebook/a/a;

    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v3, "com.facebook.sdk.WebViewErrorCode"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v4, "com.facebook.sdk.FailingUrl"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Lcom/facebook/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 237
    invoke-interface {p4, v0}, Lcom/facebook/a/c;->a(Lcom/facebook/a/a;)V

    goto :goto_1

    .line 240
    :cond_4
    new-instance v0, Lcom/facebook/a/d;

    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/a/d;-><init>(Ljava/lang/String;)V

    .line 241
    invoke-interface {p4, v0}, Lcom/facebook/a/c;->a(Lcom/facebook/a/d;)V

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Lcom/facebook/model/GraphCustomUser;)V
    .locals 3

    .prologue
    .line 508
    iget-object v0, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    invoke-interface {p1}, Lcom/facebook/model/GraphCustomUser;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/a/e;->b(Landroid/content/Context;Ljava/lang/String;)Z

    .line 509
    iget-object v0, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    invoke-interface {p1}, Lcom/facebook/model/GraphCustomUser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/a/e;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    invoke-interface {p1}, Lcom/facebook/model/GraphCustomUser;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/a/e;->c(Landroid/content/Context;Ljava/lang/String;)Z

    .line 511
    iget-object v0, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/chaton/c/b;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/facebook/model/GraphCustomUser;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/picture"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/a/e;->d(Landroid/content/Context;Ljava/lang/String;)Z

    .line 512
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/g/a;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/chaton/g/a;->j()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/g/a;Lcom/facebook/ba;Lcom/facebook/bs;Ljava/lang/Exception;Lcom/facebook/a/c;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/chaton/g/a;->a(Lcom/facebook/ba;Lcom/facebook/bs;Ljava/lang/Exception;Lcom/facebook/a/c;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/g/a;Lcom/facebook/model/GraphCustomUser;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/sec/chaton/g/a;->a(Lcom/facebook/model/GraphCustomUser;)V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 81
    invoke-static {p0}, Lcom/sec/chaton/g/a;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/g/a;)Lcom/sec/chaton/settings/tellfriends/common/c;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/chaton/g/a;->d:Lcom/sec/chaton/settings/tellfriends/common/c;

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 81
    invoke-static {p0}, Lcom/sec/chaton/g/a;->e(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/g/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/chaton/g/a;->e:Lcom/facebook/b/o;

    invoke-virtual {v0}, Lcom/facebook/b/o;->b()Lcom/facebook/ba;

    move-result-object v0

    .line 520
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 521
    const-string v2, "picture"

    iget-object v3, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    const v4, 0x7f0b0004

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    const-string v2, "message"

    const-string v3, "asdasdasdasd"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    const-string v2, "description"

    iget-object v3, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    const v4, 0x7f0b0173

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    const-string v2, "link"

    const-string v3, "www.chaton.com/invite.html"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const-string v2, "name"

    const-string v3, "ChatON"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    const-string v2, "caption"

    const-string v3, "www.chaton.com/invite.html"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    const-string v2, "to"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    new-instance v2, Lcom/facebook/c/v;

    iget-object v3, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    invoke-direct {v2, v3, v0, v1}, Lcom/facebook/c/v;-><init>(Landroid/content/Context;Lcom/facebook/ba;Landroid/os/Bundle;)V

    new-instance v0, Lcom/sec/chaton/g/f;

    invoke-direct {v0, p0}, Lcom/sec/chaton/g/f;-><init>(Lcom/sec/chaton/g/a;)V

    invoke-virtual {v2, v0}, Lcom/facebook/c/v;->a(Lcom/facebook/c/w;)Lcom/facebook/c/t;

    move-result-object v0

    check-cast v0, Lcom/facebook/c/v;

    invoke-virtual {v0}, Lcom/facebook/c/v;->a()Lcom/facebook/c/o;

    move-result-object v0

    .line 552
    invoke-virtual {v0}, Lcom/facebook/c/o;->show()V

    .line 553
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/g/a;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    return-object v0
.end method

.method private static d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 575
    const-string v0, "FacebookManager"

    invoke-static {p0, v0}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    return-void
.end method

.method private static e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 579
    const-string v0, "FacebookManager"

    invoke-static {p0, v0}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    return-void
.end method

.method private h()Lcom/facebook/ba;
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/chaton/g/a;->e:Lcom/facebook/b/o;

    invoke-virtual {v0}, Lcom/facebook/b/o;->a()Lcom/facebook/ba;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/ba;->c()Lcom/facebook/bs;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/bs;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/g/a;->e:Lcom/facebook/b/o;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/b/o;->a(Lcom/facebook/ba;)V

    .line 143
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ba;->a(Landroid/content/Context;)Lcom/facebook/ba;

    move-result-object v1

    .line 144
    if-nez v1, :cond_1

    .line 145
    new-instance v0, Lcom/facebook/bk;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/bk;-><init>(Landroid/content/Context;)V

    const-string v1, "254066384616989"

    invoke-virtual {v0, v1}, Lcom/facebook/bk;->a(Ljava/lang/String;)Lcom/facebook/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/bk;->a()Lcom/facebook/ba;

    move-result-object v0

    .line 146
    invoke-static {v0}, Lcom/facebook/ba;->a(Lcom/facebook/ba;)V

    .line 151
    :cond_1
    return-object v0
.end method

.method private i()Lcom/facebook/bl;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 186
    new-instance v0, Lcom/sec/chaton/g/j;

    invoke-direct {v0, p0, v5, v1, v1}, Lcom/sec/chaton/g/j;-><init>(Lcom/sec/chaton/g/a;ILcom/sec/chaton/g/h;Lcom/sec/chaton/g/b;)V

    .line 187
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "user_photos"

    aput-object v3, v1, v2

    const-string v2, "publish_stream"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "read_stream"

    aput-object v3, v1, v2

    const-string v2, "email"

    aput-object v2, v1, v5

    const/4 v2, 0x4

    const-string v3, "offline_access"

    aput-object v3, v1, v2

    .line 189
    new-instance v2, Lcom/sec/chaton/g/b;

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/g/b;-><init>(Lcom/sec/chaton/g/a;Lcom/sec/chaton/g/j;)V

    .line 197
    new-instance v0, Lcom/facebook/bl;

    iget-object v3, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    invoke-direct {v0, v3}, Lcom/facebook/bl;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v2}, Lcom/facebook/bl;->b(Lcom/facebook/bn;)Lcom/facebook/bl;

    move-result-object v0

    sget-object v2, Lcom/facebook/br;->a:Lcom/facebook/br;

    invoke-virtual {v0, v2}, Lcom/facebook/bl;->b(Lcom/facebook/br;)Lcom/facebook/bl;

    move-result-object v0

    const/16 v2, 0x7f99

    invoke-virtual {v0, v2}, Lcom/facebook/bl;->b(I)Lcom/facebook/bl;

    move-result-object v0

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/bl;->b(Ljava/util/List;)Lcom/facebook/bl;

    move-result-object v0

    .line 202
    invoke-virtual {v0, v4}, Lcom/facebook/bl;->a(Z)V

    .line 204
    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/chaton/g/a;->e:Lcom/facebook/b/o;

    invoke-virtual {v0}, Lcom/facebook/b/o;->b()Lcom/facebook/ba;

    move-result-object v0

    .line 399
    if-eqz v0, :cond_0

    .line 400
    new-instance v1, Lcom/sec/chaton/g/c;

    invoke-direct {v1, p0}, Lcom/sec/chaton/g/c;-><init>(Lcom/sec/chaton/g/a;)V

    invoke-static {v0, v1}, Lcom/facebook/am;->a(Lcom/facebook/ba;Lcom/facebook/as;)Lcom/facebook/am;

    move-result-object v0

    .line 415
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 416
    const-string v2, "fields"

    const-string v3, "name,email"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    invoke-virtual {v0, v1}, Lcom/facebook/am;->a(Landroid/os/Bundle;)V

    .line 418
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/am;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/facebook/am;->b([Lcom/facebook/am;)Lcom/facebook/aw;

    move-result-object v1

    .line 419
    iget-object v2, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/facebook/a/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/sec/chaton/g/a;->h()Lcom/facebook/ba;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Lcom/facebook/ba;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 176
    invoke-direct {p0}, Lcom/sec/chaton/g/a;->i()Lcom/facebook/bl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ba;->b(Lcom/facebook/bl;)V

    .line 178
    :cond_0
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Login onActivityResult requstCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resultCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/g/a;->d(Ljava/lang/String;)V

    .line 333
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 464
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 465
    invoke-direct {p0, p2}, Lcom/sec/chaton/g/a;->c(Ljava/lang/String;)V

    .line 467
    :cond_0
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/chaton/g/a;->e:Lcom/facebook/b/o;

    invoke-virtual {v0}, Lcom/facebook/b/o;->b()Lcom/facebook/ba;

    move-result-object v1

    .line 471
    if-eqz v1, :cond_1

    .line 472
    invoke-static {}, Lcom/facebook/model/GraphObject$Factory;->create()Lcom/facebook/model/GraphObject;

    move-result-object v2

    .line 473
    const-string v0, "picture"

    iget-object v3, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    const v4, 0x7f0b0004

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/model/GraphObject;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474
    const-string v0, "message"

    invoke-interface {v2, v0, p3}, Lcom/facebook/model/GraphObject;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 475
    const-string v0, "description"

    iget-object v3, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    const v4, 0x7f0b0173

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/model/GraphObject;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 476
    const-string v0, "link"

    const-string v3, "www.chaton.com/invite.html"

    invoke-interface {v2, v0, v3}, Lcom/facebook/model/GraphObject;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 477
    const-string v0, "name"

    const-string v3, "ChatON"

    invoke-interface {v2, v0, v3}, Lcom/facebook/model/GraphObject;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 478
    const-string v0, "caption"

    const-string v3, "www.chaton.com/invite.html"

    invoke-interface {v2, v0, v3}, Lcom/facebook/model/GraphObject;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 480
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string v0, "me/feed"

    .line 482
    :goto_0
    new-instance v3, Lcom/sec/chaton/g/e;

    invoke-direct {v3, p0}, Lcom/sec/chaton/g/e;-><init>(Lcom/sec/chaton/g/a;)V

    invoke-static {v1, v0, v2, v3}, Lcom/facebook/am;->a(Lcom/facebook/ba;Ljava/lang/String;Lcom/facebook/model/GraphObject;Lcom/facebook/ar;)Lcom/facebook/am;

    move-result-object v0

    .line 499
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/am;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/facebook/am;->b([Lcom/facebook/am;)Lcom/facebook/aw;

    move-result-object v1

    .line 500
    iget-object v2, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    :cond_1
    return-void

    .line 480
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/feed"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/facebook/bs;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 572
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/facebook/a/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/facebook/a/e;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/chaton/g/a;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/facebook/a/e;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/chaton/g/a;->e:Lcom/facebook/b/o;

    invoke-virtual {v0}, Lcom/facebook/b/o;->b()Lcom/facebook/ba;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 4

    .prologue
    .line 437
    iget-object v0, p0, Lcom/sec/chaton/g/a;->e:Lcom/facebook/b/o;

    invoke-virtual {v0}, Lcom/facebook/b/o;->b()Lcom/facebook/ba;

    move-result-object v0

    .line 438
    if-eqz v0, :cond_0

    .line 439
    new-instance v1, Lcom/sec/chaton/g/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/g/d;-><init>(Lcom/sec/chaton/g/a;)V

    invoke-static {v0, v1}, Lcom/facebook/am;->a(Lcom/facebook/ba;Lcom/facebook/at;)Lcom/facebook/am;

    move-result-object v0

    .line 455
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 456
    const-string v2, "fields"

    const-string v3, "name,email,installed"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    invoke-virtual {v0, v1}, Lcom/facebook/am;->a(Landroid/os/Bundle;)V

    .line 458
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/am;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/facebook/am;->b([Lcom/facebook/am;)Lcom/facebook/aw;

    move-result-object v1

    .line 459
    iget-object v2, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    :cond_0
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 610
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "killAllTask() \t- Requests count : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\tRequests : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/g/a;->d(Ljava/lang/String;)V

    .line 611
    iget-object v1, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    monitor-enter v1

    .line 612
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 613
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 614
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/am;

    .line 615
    iget-object v3, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/aw;

    .line 616
    if-eqz v0, :cond_0

    .line 617
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/facebook/aw;->cancel(Z)Z

    goto :goto_0

    .line 621
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 620
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/g/a;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 621
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 622
    return-void
.end method
