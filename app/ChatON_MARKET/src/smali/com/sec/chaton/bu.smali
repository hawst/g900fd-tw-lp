.class Lcom/sec/chaton/bu;
.super Ljava/lang/Object;
.source "TabActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/TabActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 930
    iput-object p1, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v8, -0x1

    .line 934
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->i(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    iget-object v3, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v3}, Lcom/sec/chaton/TabActivity;->j(Lcom/sec/chaton/TabActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->k(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    iget-object v3, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v3}, Lcom/sec/chaton/TabActivity;->j(Lcom/sec/chaton/TabActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    if-eq v0, v3, :cond_5

    .line 935
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v3, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v3}, Lcom/sec/chaton/TabActivity;->j(Lcom/sec/chaton/TabActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/chaton/TabActivity;->e(Lcom/sec/chaton/TabActivity;I)I

    .line 936
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v3, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v3}, Lcom/sec/chaton/TabActivity;->j(Lcom/sec/chaton/TabActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/chaton/TabActivity;->f(Lcom/sec/chaton/TabActivity;I)I

    .line 943
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    const v3, 0x7f0704a2

    invoke-virtual {v0, v3}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 946
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->f()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 947
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v4, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v4}, Lcom/sec/chaton/TabActivity;->j(Lcom/sec/chaton/TabActivity;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-static {v0, v4}, Lcom/sec/chaton/TabActivity;->g(Lcom/sec/chaton/TabActivity;I)I

    .line 952
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v4, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v4}, Lcom/sec/chaton/TabActivity;->m(Lcom/sec/chaton/TabActivity;)I

    move-result v4

    invoke-static {v0, v4}, Lcom/sec/chaton/TabActivity;->h(Lcom/sec/chaton/TabActivity;I)I

    .line 953
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v4, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    const v6, 0x7f07049e

    invoke-virtual {v4, v6}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Landroid/view/View;)Landroid/view/View;

    .line 954
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v4, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    const v6, 0x7f07049f

    invoke-virtual {v4, v6}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/TabActivity;->b(Lcom/sec/chaton/TabActivity;Landroid/view/View;)Landroid/view/View;

    .line 955
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v4, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    const v6, 0x7f07049c

    invoke-virtual {v4, v6}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/TabActivity;->c(Lcom/sec/chaton/TabActivity;Landroid/view/View;)Landroid/view/View;

    .line 956
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v4, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    const v6, 0x7f0704a0

    invoke-virtual {v4, v6}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/TabActivity;->d(Lcom/sec/chaton/TabActivity;Landroid/view/View;)Landroid/view/View;

    .line 957
    iget-object v4, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->n(Lcom/sec/chaton/TabActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-static {v4, v0}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;Landroid/widget/LinearLayout$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    .line 958
    iget-object v4, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->o(Lcom/sec/chaton/TabActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-static {v4, v0}, Lcom/sec/chaton/TabActivity;->b(Lcom/sec/chaton/TabActivity;Landroid/widget/LinearLayout$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    .line 960
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/TabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v4, v0, Landroid/content/res/Configuration;->orientation:I

    .line 961
    if-eqz v3, :cond_5

    .line 962
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->p(Lcom/sec/chaton/TabActivity;)Landroid/view/View;

    move-result-object v0

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 964
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    const v3, 0x7f0704a0

    invoke-virtual {v0, v3}, Lcom/sec/chaton/TabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 967
    const-string v0, ""

    .line 970
    iget-object v6, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v6}, Lcom/sec/chaton/TabActivity;->q(Lcom/sec/chaton/TabActivity;)I

    move-result v6

    const v7, 0x7f070008

    if-eq v6, v7, :cond_2

    iget-object v6, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v6}, Lcom/sec/chaton/TabActivity;->q(Lcom/sec/chaton/TabActivity;)I

    move-result v6

    const v7, 0x7f070009

    if-ne v6, v7, :cond_9

    .line 971
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v2}, Lcom/sec/chaton/TabActivity;->q(Lcom/sec/chaton/TabActivity;)I

    move-result v2

    const v3, 0x7f070008

    if-ne v2, v3, :cond_7

    .line 972
    const-string v0, "splitview_position_buddy"

    .line 977
    :cond_3
    :goto_1
    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 978
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    .line 979
    cmpl-float v1, v0, v1

    if-nez v1, :cond_8

    .line 980
    const/high16 v0, 0x3e800000    # 0.25f

    move v1, v0

    .line 986
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->r(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 987
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    float-to-double v1, v1

    iget-object v4, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v4}, Lcom/sec/chaton/TabActivity;->r(Lcom/sec/chaton/TabActivity;)I

    move-result v4

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/TabActivity;->a(Lcom/sec/chaton/TabActivity;DIIZ)V

    .line 1012
    :cond_5
    :goto_3
    return-void

    .line 949
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    iget-object v4, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v4}, Lcom/sec/chaton/TabActivity;->l(Lcom/sec/chaton/TabActivity;)I

    move-result v4

    invoke-static {v0, v4}, Lcom/sec/chaton/TabActivity;->g(Lcom/sec/chaton/TabActivity;I)I

    goto/16 :goto_0

    .line 973
    :cond_7
    iget-object v2, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v2}, Lcom/sec/chaton/TabActivity;->q(Lcom/sec/chaton/TabActivity;)I

    move-result v2

    const v3, 0x7f070009

    if-ne v2, v3, :cond_3

    .line 974
    const-string v0, "splitview_position_chat"

    goto :goto_1

    .line 982
    :cond_8
    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    move v1, v0

    goto :goto_2

    .line 989
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->q(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    const v6, 0x7f07000a

    if-ne v0, v6, :cond_c

    .line 990
    const/4 v0, 0x1

    if-ne v4, v0, :cond_a

    .line 993
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->s(Lcom/sec/chaton/TabActivity;)Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v8, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 994
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v5, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 998
    :cond_a
    const v1, 0x3f1f7cee    # 0.623f

    .line 1004
    :cond_b
    :goto_4
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->r(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1005
    iget-object v1, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v1}, Lcom/sec/chaton/TabActivity;->s(Lcom/sec/chaton/TabActivity;)Landroid/view/ViewGroup;

    move-result-object v1

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v0, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1006
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v5, v8, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 999
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/bu;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->q(Lcom/sec/chaton/TabActivity;)I

    move-result v0

    const v4, 0x7f07000b

    if-ne v0, v4, :cond_b

    move v1, v2

    .line 1000
    goto :goto_4
.end method
