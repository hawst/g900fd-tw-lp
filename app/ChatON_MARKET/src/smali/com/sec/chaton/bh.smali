.class Lcom/sec/chaton/bh;
.super Landroid/content/BroadcastReceiver;
.source "TabActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/TabActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/TabActivity;)V
    .locals 0

    .prologue
    .line 1927
    iput-object p1, p0, Lcom/sec/chaton/bh;->a:Lcom/sec/chaton/TabActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1930
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 1931
    const-string v0, "received Update Push Version"

    invoke-static {}, Lcom/sec/chaton/TabActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1934
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/bh;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0}, Lcom/sec/chaton/TabActivity;->E(Lcom/sec/chaton/TabActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1935
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/bh;->a:Lcom/sec/chaton/TabActivity;

    const-class v2, Lcom/sec/chaton/NewSPPUpgradeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1936
    const-string v1, "isCritical"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1937
    const-string v1, "isFromHome"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1940
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1942
    iget-object v1, p0, Lcom/sec/chaton/bh;->a:Lcom/sec/chaton/TabActivity;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/TabActivity;->startActivity(Landroid/content/Intent;)V

    .line 1947
    :goto_0
    return-void

    .line 1944
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/bh;->a:Lcom/sec/chaton/TabActivity;

    invoke-static {v0, v3}, Lcom/sec/chaton/TabActivity;->f(Lcom/sec/chaton/TabActivity;Z)Z

    goto :goto_0
.end method
