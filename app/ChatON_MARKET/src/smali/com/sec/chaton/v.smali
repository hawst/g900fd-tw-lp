.class public final enum Lcom/sec/chaton/v;
.super Ljava/lang/Enum;
.source "HandleIntent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/v;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/v;

.field public static final enum b:Lcom/sec/chaton/v;

.field public static final enum c:Lcom/sec/chaton/v;

.field public static final enum d:Lcom/sec/chaton/v;

.field public static final enum e:Lcom/sec/chaton/v;

.field public static final enum f:Lcom/sec/chaton/v;

.field public static final enum g:Lcom/sec/chaton/v;

.field public static final enum h:Lcom/sec/chaton/v;

.field public static final enum i:Lcom/sec/chaton/v;

.field public static final enum j:Lcom/sec/chaton/v;

.field public static final enum k:Lcom/sec/chaton/v;

.field public static final enum l:Lcom/sec/chaton/v;

.field public static final enum m:Lcom/sec/chaton/v;

.field public static final enum n:Lcom/sec/chaton/v;

.field private static final synthetic o:[Lcom/sec/chaton/v;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 44
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "CHATROOM"

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    .line 45
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "FORWARD"

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->b:Lcom/sec/chaton/v;

    .line 46
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v5}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    .line 47
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "CHATLIST"

    invoke-direct {v0, v1, v6}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->d:Lcom/sec/chaton/v;

    .line 48
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "MYPAGE"

    invoke-direct {v0, v1, v7}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->e:Lcom/sec/chaton/v;

    .line 49
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "CHATONV"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->f:Lcom/sec/chaton/v;

    .line 50
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "ADD_BUDDY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->g:Lcom/sec/chaton/v;

    .line 51
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "VIEW_BUDDY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->h:Lcom/sec/chaton/v;

    .line 52
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "VIEW_PLUS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->i:Lcom/sec/chaton/v;

    .line 53
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "VOICE_CHAT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->j:Lcom/sec/chaton/v;

    .line 54
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "VIDEO_CHAT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->k:Lcom/sec/chaton/v;

    .line 55
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "VOICE_GROUP_CHAT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->l:Lcom/sec/chaton/v;

    .line 56
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "VIDEO_GROUP_CHAT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->m:Lcom/sec/chaton/v;

    .line 57
    new-instance v0, Lcom/sec/chaton/v;

    const-string v1, "AUTO_BACKUP"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/chaton/v;->n:Lcom/sec/chaton/v;

    .line 43
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/sec/chaton/v;

    sget-object v1, Lcom/sec/chaton/v;->a:Lcom/sec/chaton/v;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/chaton/v;->b:Lcom/sec/chaton/v;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/v;->c:Lcom/sec/chaton/v;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/v;->d:Lcom/sec/chaton/v;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/v;->e:Lcom/sec/chaton/v;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/v;->f:Lcom/sec/chaton/v;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/v;->g:Lcom/sec/chaton/v;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/v;->h:Lcom/sec/chaton/v;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/v;->i:Lcom/sec/chaton/v;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/chaton/v;->j:Lcom/sec/chaton/v;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/chaton/v;->k:Lcom/sec/chaton/v;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/chaton/v;->l:Lcom/sec/chaton/v;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/chaton/v;->m:Lcom/sec/chaton/v;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/chaton/v;->n:Lcom/sec/chaton/v;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/v;->o:[Lcom/sec/chaton/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/v;
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/sec/chaton/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/v;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/v;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/chaton/v;->o:[Lcom/sec/chaton/v;

    invoke-virtual {v0}, [Lcom/sec/chaton/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/v;

    return-object v0
.end method
