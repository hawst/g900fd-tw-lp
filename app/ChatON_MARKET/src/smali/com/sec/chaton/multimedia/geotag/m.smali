.class Lcom/sec/chaton/multimedia/geotag/m;
.super Landroid/os/AsyncTask;
.source "GeotagActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

.field private b:Z

.field private c:D

.field private d:D


# direct methods
.method public constructor <init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;ZDD)V
    .locals 0

    .prologue
    .line 1051
    iput-object p1, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1052
    iput-boolean p2, p0, Lcom/sec/chaton/multimedia/geotag/m;->b:Z

    .line 1053
    iput-wide p3, p0, Lcom/sec/chaton/multimedia/geotag/m;->c:D

    .line 1054
    iput-wide p5, p0, Lcom/sec/chaton/multimedia/geotag/m;->d:D

    .line 1055
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1060
    :try_start_0
    new-instance v0, Landroid/location/Geocoder;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->c(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 1062
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/geotag/m;->b:Z

    if-eqz v1, :cond_0

    .line 1063
    const/4 v1, 0x0

    aget-object v1, p1, v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 1075
    :goto_0
    return-object v0

    .line 1066
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1067
    iget-wide v1, p0, Lcom/sec/chaton/multimedia/geotag/m;->c:D

    iget-wide v3, p0, Lcom/sec/chaton/multimedia/geotag/m;->d:D

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1070
    :catch_0
    move-exception v0

    .line 1071
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 1072
    invoke-static {}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1075
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide v5, 0x412e848000000000L    # 1000000.0

    const/4 v4, 0x0

    .line 1092
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1094
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149
    :goto_0
    return-void

    .line 1098
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1099
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->b:Z

    if-eqz v0, :cond_2

    .line 1100
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1104
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1107
    new-instance v1, Lcom/google/android/maps/GeoPoint;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    invoke-virtual {v0}, Landroid/location/Address;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v5

    double-to-int v2, v2

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    invoke-virtual {v0}, Landroid/location/Address;->getLongitude()D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v0, v3

    invoke-direct {v1, v2, v0}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 1108
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Lcom/google/android/maps/GeoPoint;)Lcom/google/android/maps/GeoPoint;

    .line 1110
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->n(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->b(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Lcom/google/android/maps/GeoPoint;)Lcom/google/android/maps/GeoPoint;

    .line 1111
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/google/android/maps/MapController;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 1113
    new-instance v0, Lcom/google/android/maps/OverlayItem;

    const-string v2, "test"

    const-string v3, "Searched location"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 1115
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->o(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/sec/chaton/multimedia/geotag/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/geotag/a;->b(Lcom/google/android/maps/OverlayItem;)V

    .line 1116
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->o(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/sec/chaton/multimedia/geotag/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/geotag/a;->a(Lcom/google/android/maps/OverlayItem;)V

    .line 1118
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->p(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1120
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->p(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->o(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/sec/chaton/multimedia/geotag/a;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1126
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->q(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 1130
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->c(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003e

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1133
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1134
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 1135
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 1137
    if-eqz v0, :cond_3

    .line 1138
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-virtual {v0, v4}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1143
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1144
    const-string v1, "GEOPOINT"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lcom/sec/chaton/multimedia/geotag/m;->c:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/chaton/multimedia/geotag/m;->d:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1145
    const-string v1, "GEOADDRESS"

    iget-object v2, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->r(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1146
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->setResult(ILandroid/content/Intent;)V

    .line 1147
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->finish()V

    goto/16 :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1045
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/geotag/m;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 1080
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 1082
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1087
    :goto_0
    return-void

    .line 1086
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/m;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1045
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/geotag/m;->a(Ljava/util/List;)V

    return-void
.end method
