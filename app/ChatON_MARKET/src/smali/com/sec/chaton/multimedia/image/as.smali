.class public Lcom/sec/chaton/multimedia/image/as;
.super Ljava/lang/Object;
.source "ZoomListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field b:I

.field c:Landroid/graphics/Matrix;

.field d:Landroid/graphics/Matrix;

.field e:Landroid/graphics/PointF;

.field f:Landroid/graphics/PointF;

.field g:F

.field final h:F

.field i:Z

.field j:I

.field k:I

.field l:I

.field m:I

.field n:Z

.field private o:Landroid/view/GestureDetector;

.field private p:Landroid/widget/ImageView;

.field private q:Lcom/sec/chaton/multimedia/image/at;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/chaton/multimedia/image/as;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/image/as;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v1, p0, Lcom/sec/chaton/multimedia/image/as;->b:I

    .line 31
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    .line 32
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->d:Landroid/graphics/Matrix;

    .line 33
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->e:Landroid/graphics/PointF;

    .line 34
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->f:Landroid/graphics/PointF;

    .line 35
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/as;->g:F

    .line 36
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/as;->h:F

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/as;->i:Z

    .line 39
    iput v1, p0, Lcom/sec/chaton/multimedia/image/as;->j:I

    .line 40
    iput v1, p0, Lcom/sec/chaton/multimedia/image/as;->k:I

    .line 42
    iput v1, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    .line 43
    iput v1, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    .line 84
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/sec/chaton/multimedia/image/a;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/image/a;-><init>(Lcom/sec/chaton/multimedia/image/as;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->o:Landroid/view/GestureDetector;

    .line 85
    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/multimedia/image/at;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v1, p0, Lcom/sec/chaton/multimedia/image/as;->b:I

    .line 31
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    .line 32
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->d:Landroid/graphics/Matrix;

    .line 33
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->e:Landroid/graphics/PointF;

    .line 34
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->f:Landroid/graphics/PointF;

    .line 35
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/as;->g:F

    .line 36
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/as;->h:F

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/as;->i:Z

    .line 39
    iput v1, p0, Lcom/sec/chaton/multimedia/image/as;->j:I

    .line 40
    iput v1, p0, Lcom/sec/chaton/multimedia/image/as;->k:I

    .line 42
    iput v1, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    .line 43
    iput v1, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    .line 90
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/sec/chaton/multimedia/image/a;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/image/a;-><init>(Lcom/sec/chaton/multimedia/image/as;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->o:Landroid/view/GestureDetector;

    .line 91
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/as;->q:Lcom/sec/chaton/multimedia/image/at;

    .line 92
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)F
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 199
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 200
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    .line 201
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v1, v2

    .line 202
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    .line 205
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 211
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    .line 212
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    add-float/2addr v1, v2

    .line 213
    div-float/2addr v0, v3

    div-float/2addr v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 214
    return-void
.end method

.method private b(Landroid/widget/ImageView;)V
    .locals 10

    .prologue
    const/high16 v4, 0x40800000    # 4.0f

    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v8, 0x0

    const/4 v7, 0x5

    const/4 v6, 0x2

    .line 284
    const/16 v0, 0x9

    new-array v2, v0, [F

    .line 285
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 287
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 288
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->d:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 301
    iget v0, p0, Lcom/sec/chaton/multimedia/image/as;->j:I

    int-to-float v0, v0

    const/4 v1, 0x0

    aget v1, v2, v1

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 302
    iget v0, p0, Lcom/sec/chaton/multimedia/image/as;->k:I

    int-to-float v0, v0

    const/4 v3, 0x4

    aget v3, v2, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 306
    const/4 v3, 0x0

    aget v3, v2, v3

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    const/4 v3, 0x4

    aget v3, v2, v3

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    aget v1, v2, v1

    div-float v1, v4, v1

    const/4 v3, 0x4

    aget v3, v2, v3

    div-float v3, v4, v3

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/as;->f:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/sec/chaton/multimedia/image/as;->f:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 311
    iget v0, p0, Lcom/sec/chaton/multimedia/image/as;->j:I

    int-to-float v0, v0

    const/4 v1, 0x0

    aget v1, v2, v1

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 312
    iget v0, p0, Lcom/sec/chaton/multimedia/image/as;->k:I

    int-to-float v0, v0

    const/4 v3, 0x4

    aget v3, v2, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 315
    :cond_1
    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    if-gt v1, v3, :cond_2

    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    if-gt v0, v3, :cond_2

    .line 316
    invoke-static {p1}, Lcom/sec/chaton/util/ad;->a(Landroid/widget/ImageView;)V

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->d:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 400
    :goto_0
    return-void

    .line 324
    :cond_2
    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    if-gt v1, v3, :cond_3

    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    if-le v0, v3, :cond_5

    .line 327
    :cond_3
    aget v3, v2, v6

    cmpl-float v3, v3, v8

    if-lez v3, :cond_7

    .line 328
    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    if-ge v1, v3, :cond_6

    .line 329
    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    mul-float/2addr v1, v9

    aput v1, v2, v6

    .line 343
    :cond_4
    :goto_1
    aget v1, v2, v7

    cmpl-float v1, v1, v8

    if-lez v1, :cond_a

    .line 344
    iget v1, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    if-ge v0, v1, :cond_9

    .line 345
    iget v1, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    sub-int v0, v1, v0

    int-to-float v0, v0

    mul-float/2addr v0, v9

    aput v0, v2, v7

    .line 397
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->setValues([F)V

    .line 398
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->d:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 331
    :cond_6
    aput v8, v2, v6

    goto :goto_1

    .line 334
    :cond_7
    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    if-ge v1, v3, :cond_8

    .line 335
    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    mul-float/2addr v1, v9

    aput v1, v2, v6

    goto :goto_1

    .line 337
    :cond_8
    aget v3, v2, v6

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    sub-int/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    .line 338
    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    aput v1, v2, v6

    goto :goto_1

    .line 347
    :cond_9
    aput v8, v2, v7

    goto :goto_2

    .line 351
    :cond_a
    iget v1, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    if-ge v0, v1, :cond_b

    .line 352
    iget v1, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    sub-int v0, v1, v0

    int-to-float v0, v0

    mul-float/2addr v0, v9

    aput v0, v2, v7

    goto :goto_2

    .line 354
    :cond_b
    aget v1, v2, v7

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    sub-int/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    .line 355
    iget v1, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    sub-int v0, v1, v0

    int-to-float v0, v0

    aput v0, v2, v7

    goto :goto_2
.end method


# virtual methods
.method public a(FF)V
    .locals 4

    .prologue
    const/high16 v3, 0x40800000    # 4.0f

    const/high16 v2, 0x40000000    # 2.0f

    .line 243
    .line 250
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/as;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 253
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 254
    const/4 v1, 0x0

    aget v1, v0, v1

    cmpl-float v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v1, 0x4

    aget v0, v0, v1

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2, v2, p1, p2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->p:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/image/as;->b(Landroid/widget/ImageView;)V

    .line 279
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->p:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 277
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->p:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/as;->a(Landroid/widget/ImageView;)V

    goto :goto_1
.end method

.method public a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 61
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/as;->j:I

    .line 64
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/as;->k:I

    .line 66
    invoke-virtual {p1}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    .line 67
    invoke-virtual {p1}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    .line 69
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 70
    invoke-static {p1}, Lcom/sec/chaton/util/ad;->a(Landroid/widget/ImageView;)V

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->d:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 76
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 217
    const/16 v1, 0x9

    new-array v1, v1, [F

    .line 218
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 220
    iget v2, p0, Lcom/sec/chaton/multimedia/image/as;->j:I

    int-to-float v2, v2

    aget v3, v1, v0

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 221
    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->k:I

    int-to-float v3, v3

    const/4 v4, 0x4

    aget v1, v1, v4

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 223
    iget v3, p0, Lcom/sec/chaton/multimedia/image/as;->l:I

    if-gt v2, v3, :cond_0

    iget v2, p0, Lcom/sec/chaton/multimedia/image/as;->m:I

    if-gt v1, v2, :cond_0

    .line 224
    const/4 v0, 0x1

    .line 226
    :cond_0
    return v0
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 231
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/as;->a()Z

    move-result v1

    if-ne v1, v0, :cond_0

    iget v1, p0, Lcom/sec/chaton/multimedia/image/as;->b:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 235
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->q:Lcom/sec/chaton/multimedia/image/at;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->q:Lcom/sec/chaton/multimedia/image/at;

    invoke-interface {v0}, Lcom/sec/chaton/multimedia/image/at;->f()V

    .line 406
    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/high16 v5, 0x41200000    # 10.0f

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 105
    check-cast p1, Landroid/widget/ImageView;

    .line 107
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/as;->p:Landroid/widget/ImageView;

    .line 110
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/as;->i:Z

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/image/as;->a(Landroid/widget/ImageView;)V

    .line 119
    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/image/as;->i:Z

    .line 122
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 189
    :cond_1
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->o:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 192
    return v4

    .line 124
    :pswitch_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 125
    const-string v0, "ACTION_DOWN"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->d:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->e:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 129
    const-string v0, "Mode = DRAG"

    sget-object v1, Lcom/sec/chaton/multimedia/image/as;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iput v4, p0, Lcom/sec/chaton/multimedia/image/as;->b:I

    goto :goto_0

    .line 133
    :pswitch_2
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 134
    const-string v0, "ACTION_POINTER_DOWN"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_3
    invoke-direct {p0, p2}, Lcom/sec/chaton/multimedia/image/as;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/as;->g:F

    .line 137
    iget v0, p0, Lcom/sec/chaton/multimedia/image/as;->g:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->d:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->f:Landroid/graphics/PointF;

    invoke-direct {p0, v0, p2}, Lcom/sec/chaton/multimedia/image/as;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 140
    iput v3, p0, Lcom/sec/chaton/multimedia/image/as;->b:I

    goto :goto_0

    .line 145
    :pswitch_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 146
    const-string v0, "ACTION_POINTER_UP/ACTION_UP"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_4
    iget v0, p0, Lcom/sec/chaton/multimedia/image/as;->b:I

    if-ne v0, v3, :cond_5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v4, :cond_5

    .line 149
    iput-boolean v4, p0, Lcom/sec/chaton/multimedia/image/as;->n:Z

    .line 150
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "POINTS: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 154
    :cond_5
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_6

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "POINTS: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_6
    iput v2, p0, Lcom/sec/chaton/multimedia/image/as;->b:I

    .line 158
    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/image/as;->n:Z

    .line 159
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/image/as;->b(Landroid/widget/ImageView;)V

    goto/16 :goto_0

    .line 163
    :pswitch_4
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_7

    .line 164
    const-string v0, "ACTION_MOVE"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_7
    iget v0, p0, Lcom/sec/chaton/multimedia/image/as;->b:I

    if-ne v0, v4, :cond_8

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 168
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/as;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/as;->e:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/as;->e:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_0

    .line 172
    :cond_8
    iget v0, p0, Lcom/sec/chaton/multimedia/image/as;->b:I

    if-ne v0, v3, :cond_1

    .line 174
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/as;->n:Z

    if-nez v0, :cond_1

    .line 175
    invoke-direct {p0, p2}, Lcom/sec/chaton/multimedia/image/as;->a(Landroid/view/MotionEvent;)F

    move-result v0

    .line 176
    cmpl-float v1, v0, v5

    if-lez v1, :cond_1

    .line 177
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/as;->d:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 178
    iget v1, p0, Lcom/sec/chaton/multimedia/image/as;->g:F

    div-float/2addr v0, v1

    .line 179
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/as;->c:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/as;->f:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/as;->f:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto/16 :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
