.class Lcom/sec/chaton/multimedia/image/p;
.super Landroid/os/Handler;
.source "ImagePagerActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V
    .locals 0

    .prologue
    .line 764
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/p;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 768
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 769
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0149

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 770
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/p;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->finish()V

    .line 787
    :cond_0
    :goto_0
    return-void

    .line 774
    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "download_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 775
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 777
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 778
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "messageId : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", uri: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->h()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/p;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v0, v2, v3, v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;JLjava/lang/String;)V

    .line 783
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/p;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->n(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/p;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->n(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    .line 785
    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
