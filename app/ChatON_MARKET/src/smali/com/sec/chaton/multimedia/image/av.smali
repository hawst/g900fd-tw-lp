.class Lcom/sec/chaton/multimedia/image/av;
.super Ljava/lang/Object;
.source "ZoomableImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 853
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 858
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v0, v1

    .line 862
    sub-float v1, v0, v5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v1, v1

    const-wide v3, 0x3fa999999999999aL    # 0.05

    cmpl-double v1, v1, v3

    if-lez v1, :cond_3

    .line 864
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    .line 866
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 868
    sub-float/2addr v0, v5

    .line 870
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    const v2, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v2

    add-float/2addr v0, v5

    iput v0, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    .line 872
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 874
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 876
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 878
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iput v5, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    .line 902
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_2

    .line 904
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v3, v3, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->m:F

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v4, v4, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->n:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 906
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 908
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->invalidate()V

    .line 950
    :goto_1
    return-void

    .line 886
    :cond_1
    sub-float v0, v5, v0

    .line 888
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    sub-float v0, v5, v0

    iput v0, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    .line 890
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 892
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 894
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 896
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iput v5, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    goto :goto_0

    .line 914
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iput-boolean v6, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    .line 916
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iput v5, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    .line 918
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v3, v3, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v3, v3, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->m:F

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v4, v4, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->n:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 920
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 922
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 924
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->invalidate()V

    .line 926
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->d(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V

    goto :goto_1

    .line 934
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iput-boolean v6, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    .line 936
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iput v5, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    .line 938
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v3, v3, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v3, v3, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->m:F

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v4, v4, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->n:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 940
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 942
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 944
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->invalidate()V

    .line 946
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/av;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->d(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V

    goto/16 :goto_1
.end method
