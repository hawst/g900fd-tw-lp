.class Lcom/sec/chaton/multimedia/geotag/j;
.super Landroid/os/Handler;
.source "GeotagActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V
    .locals 0

    .prologue
    .line 873
    iput-object p1, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 881
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->j(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 883
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 884
    const-string v0, "mark with networkLocation "

    invoke-static {}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->j(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Landroid/location/Location;)V

    .line 934
    :goto_0
    return-void

    .line 892
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    if-nez v0, :cond_2

    .line 894
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->c(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00df

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0037

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/multimedia/geotag/l;

    invoke-direct {v3, p0}, Lcom/sec/chaton/multimedia/geotag/l;-><init>(Lcom/sec/chaton/multimedia/geotag/j;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/multimedia/geotag/k;

    invoke-direct {v3, p0}, Lcom/sec/chaton/multimedia/geotag/k;-><init>(Lcom/sec/chaton/multimedia/geotag/j;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;

    .line 928
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 929
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 932
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 933
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/j;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Z)Z

    goto/16 :goto_0
.end method
