.class public Lcom/sec/chaton/multimedia/emoticon/anicon/m;
.super Lcom/sec/common/f/a;
.source "DynamicAniconDispatcherTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:I

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 59
    iput p2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->b:I

    .line 60
    iput p3, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->c:I

    .line 61
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 3

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->e()Landroid/widget/ImageView;

    move-result-object v1

    .line 83
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0203cb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 84
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 86
    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    .line 87
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 90
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 188
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->e()Landroid/widget/ImageView;

    move-result-object v1

    .line 197
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 198
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 200
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 203
    if-eqz p1, :cond_3

    .line 204
    check-cast p1, Ljava/util/List;

    .line 205
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->k()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;Ljava/util/List;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v0

    .line 210
    :goto_0
    if-eqz p2, :cond_4

    .line 211
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 220
    :goto_1
    instance-of v1, v0, Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v1, :cond_0

    .line 221
    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 225
    :cond_0
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->g:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/chaton/settings/downloads/u;->i(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/z;

    move-result-object v0

    .line 227
    sget-object v1, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    if-eq v0, v1, :cond_1

    .line 228
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a()Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->g:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Ljava/lang/String;)V

    .line 230
    :cond_1
    sget-object v0, Lcom/sec/chaton/settings/downloads/u;->a:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    :cond_2
    return-void

    .line 207
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0201ca

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 213
    :cond_4
    new-instance v3, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v0, v4, v2

    invoke-direct {v3, v4}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 215
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 217
    const/16 v1, 0x64

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->e()Landroid/widget/ImageView;

    move-result-object v0

    .line 97
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 99
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 100
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 102
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p0, v0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->a(Ljava/util/concurrent/Callable;J)V

    .line 103
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 108
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->k()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->g:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->b:I

    iget v3, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->c:I

    invoke-static {v1, v0, v2, v3}, Lcom/sec/chaton/settings/downloads/u;->b(Landroid/content/Context;Ljava/lang/String;II)Ljava/util/List;

    move-result-object v0

    .line 110
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    :try_start_0
    sget-object v0, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->g:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 118
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 120
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 121
    const-string v0, "zip_url"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v0

    .line 147
    :goto_0
    if-eqz v1, :cond_0

    .line 148
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 152
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    move-object v0, v6

    .line 182
    :cond_1
    :goto_1
    return-object v0

    .line 125
    :cond_2
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->k()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/chaton/d/j;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/j;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->g:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string v4, "png"

    const/16 v5, 0xf0

    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/sec/chaton/d/j;->c(Lcom/sec/chaton/d/a/b;Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/chaton/d/a/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/d/a/au;->b()Lcom/sec/chaton/a/a/f;
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v0

    .line 136
    :goto_2
    if-eqz v0, :cond_5

    :try_start_3
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->n()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v2, v3, :cond_5

    .line 138
    invoke-virtual {v0}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/io/entry/DownloadEmoticonEntry;

    .line 140
    iget-object v0, v0, Lcom/sec/chaton/io/entry/DownloadEmoticonEntry;->anicon:Lcom/sec/chaton/io/entry/inner/Anicon;

    iget-object v0, v0, Lcom/sec/chaton/io/entry/inner/Anicon;->aniconzipurl:Ljava/lang/String;

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_3

    .line 128
    sget-object v2, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_3
    move-object v0, v6

    .line 134
    goto :goto_2

    .line 130
    :catch_1
    move-exception v0

    .line 131
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_4

    .line 132
    sget-object v2, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_4
    move-object v0, v6

    goto :goto_2

    .line 147
    :cond_5
    if-eqz v1, :cond_6

    .line 148
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    move-object v0, v6

    .line 143
    goto :goto_1

    .line 147
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_7

    .line 148
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 147
    :cond_7
    throw v0

    .line 156
    :cond_8
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->k()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/util/a/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 161
    :try_start_4
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/util/a/a;->a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 164
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->k()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->g:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lcom/sec/chaton/settings/downloads/u;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 167
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->k()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->g:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v3, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->b:I

    iget v4, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->c:I

    invoke-static {v2, v0, v3, v4}, Lcom/sec/chaton/settings/downloads/u;->b(Landroid/content/Context;Ljava/lang/String;II)Ljava/util/List;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 175
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 176
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 168
    :catch_2
    move-exception v0

    .line 169
    :try_start_5
    sget-boolean v2, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v2, :cond_9

    .line 170
    sget-object v2, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 175
    :cond_9
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 176
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_a
    move-object v0, v6

    .line 173
    goto/16 :goto_1

    .line 175
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 176
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 175
    :cond_b
    throw v0

    .line 147
    :catchall_2
    move-exception v0

    move-object v6, v1

    goto :goto_3
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->e()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 238
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->a(Landroid/view/View;)V

    .line 241
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->f()Ljava/util/List;

    move-result-object v1

    .line 243
    if-eqz v1, :cond_2

    .line 244
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/util/d;

    .line 245
    invoke-virtual {v0}, Lcom/sec/chaton/util/d;->b()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/util/j;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 248
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 250
    :cond_2
    return-void
.end method

.method public e()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 254
    invoke-super {p0}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/util/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260
    invoke-super {p0}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->f()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/m;->e()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x1

    return v0
.end method
