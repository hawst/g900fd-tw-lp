.class Lcom/sec/chaton/multimedia/a/b;
.super Ljava/lang/Object;
.source "SaveFileTask.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/a/a;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/a/a;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/chaton/multimedia/a/b;->a:Lcom/sec/chaton/multimedia/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMediaScannerConnected()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/multimedia/a/b;->a:Lcom/sec/chaton/multimedia/a/a;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/a/a;->b(Lcom/sec/chaton/multimedia/a/a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/a/b;->a:Lcom/sec/chaton/multimedia/a/a;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/a/a;->b(Lcom/sec/chaton/multimedia/a/a;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/chaton/multimedia/a/b;->a:Lcom/sec/chaton/multimedia/a/a;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/a/a;->a(Lcom/sec/chaton/multimedia/a/a;)Landroid/media/MediaScannerConnection;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/multimedia/a/b;->a:Lcom/sec/chaton/multimedia/a/a;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/a/a;->b(Lcom/sec/chaton/multimedia/a/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/a/b;->a:Lcom/sec/chaton/multimedia/a/a;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/a/a;->c(Lcom/sec/chaton/multimedia/a/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/a/b;->a:Lcom/sec/chaton/multimedia/a/a;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/a/a;->a(Lcom/sec/chaton/multimedia/a/a;)Landroid/media/MediaScannerConnection;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/multimedia/a/b;->a:Lcom/sec/chaton/multimedia/a/a;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/a/a;->b(Lcom/sec/chaton/multimedia/a/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/a/b;->a:Lcom/sec/chaton/multimedia/a/a;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/a/a;->c(Lcom/sec/chaton/multimedia/a/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/chaton/multimedia/a/b;->a:Lcom/sec/chaton/multimedia/a/a;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/a/a;->a(Lcom/sec/chaton/multimedia/a/a;)Landroid/media/MediaScannerConnection;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->disconnect()V

    .line 64
    return-void
.end method
