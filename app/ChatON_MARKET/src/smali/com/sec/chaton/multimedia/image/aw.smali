.class Lcom/sec/chaton/multimedia/image/aw;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ZoomableImageView.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 954
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 959
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-boolean v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    if-ne v0, v4, :cond_0

    .line 991
    :goto_0
    return v4

    .line 965
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->o:F

    .line 967
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iput-boolean v4, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    .line 969
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->m:F

    .line 971
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->n:F

    .line 973
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->w:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3fb999999999999aL    # 0.1

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 975
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->w:F

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    .line 985
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->p:F

    .line 987
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 989
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 981
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/aw;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->v:F

    iput v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->l:F

    goto :goto_1
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1005
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 998
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method
