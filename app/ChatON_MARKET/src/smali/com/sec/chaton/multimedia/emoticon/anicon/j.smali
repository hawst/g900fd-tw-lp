.class public Lcom/sec/chaton/multimedia/emoticon/anicon/j;
.super Ljava/lang/Object;
.source "AniconPlusTask.java"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final e:Lcom/sec/chaton/multimedia/emoticon/anicon/j;


# instance fields
.field a:Lcom/sec/chaton/multimedia/emoticon/anicon/l;

.field private c:Ljava/lang/Object;

.field private d:Landroid/media/MediaPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b:Ljava/lang/String;

    .line 30
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;-><init>()V

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->e:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c:Ljava/lang/Object;

    .line 24
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/l;

    .line 34
    return-void
.end method

.method public static a()Lcom/sec/chaton/multimedia/emoticon/anicon/j;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->e:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/emoticon/anicon/j;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/emoticon/anicon/j;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 171
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 174
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-nez v1, :cond_0

    .line 175
    const-string v0, "[AniconId, Silent mode] "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :goto_0
    return-void

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b()V

    .line 182
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sec/chaton/settings/downloads/u;->e(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 184
    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/chaton/settings/downloads/u;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 185
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/l;

    if-nez v1, :cond_1

    .line 186
    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/anicon/l;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/chaton/multimedia/emoticon/anicon/l;-><init>(Lcom/sec/chaton/multimedia/emoticon/anicon/j;Lcom/sec/chaton/multimedia/emoticon/anicon/k;)V

    iput-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/l;

    .line 187
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/l;

    invoke-virtual {v1, v3}, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a(I)V

    .line 188
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/l;

    invoke-virtual {v1, p1}, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a(Ljava/lang/String;)V

    .line 189
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/l;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->start()V

    .line 194
    :cond_1
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 195
    const-string v0, "[AniconId, Vibrate mode] "

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->d:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/l;

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/l;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a()V

    .line 216
    const-string v0, "[mVibratorThread is not null] "

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_1
    return-void

    .line 208
    :catch_0
    move-exception v0

    .line 209
    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 222
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sec/chaton/settings/downloads/u;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 224
    if-eqz v1, :cond_2

    .line 225
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->d:Landroid/media/MediaPlayer;

    if-nez v2, :cond_0

    .line 226
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->d:Landroid/media/MediaPlayer;

    .line 229
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->d:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    .line 230
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->reset()V

    .line 232
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 233
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 234
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 254
    :cond_1
    const/4 v0, 0x1

    :cond_2
    :goto_0
    return v0

    .line 237
    :catch_0
    move-exception v1

    .line 238
    sget-object v2, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :catch_1
    move-exception v1

    .line 241
    sget-object v2, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 243
    :catch_2
    move-exception v1

    .line 244
    sget-object v2, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 246
    :catch_3
    move-exception v1

    .line 247
    sget-object v2, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
