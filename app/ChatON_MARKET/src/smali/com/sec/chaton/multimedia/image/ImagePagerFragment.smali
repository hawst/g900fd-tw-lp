.class public Lcom/sec/chaton/multimedia/image/ImagePagerFragment;
.super Landroid/support/v4/app/Fragment;
.source "ImagePagerFragment.java"

# interfaces
.implements Lcom/sec/chaton/multimedia/image/at;


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private A:Landroid/view/View$OnClickListener;

.field private B:Landroid/os/Handler;

.field protected a:Landroid/widget/ProgressBar;

.field protected b:J

.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;

.field e:Landroid/view/View$OnClickListener;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private l:I

.field private m:Lcom/sec/chaton/multimedia/image/aa;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/ImageButton;

.field private p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private r:Ljava/lang/String;

.field private s:Lcom/sec/chaton/e/w;

.field private t:Lcom/sec/chaton/widget/c;

.field private u:Z

.field private v:Landroid/app/Activity;

.field private w:Landroid/widget/FrameLayout;

.field private x:Landroid/widget/ImageView;

.field private y:Lcom/sec/chaton/multimedia/image/b;

.field private z:Lcom/sec/chaton/multimedia/image/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 51
    const-string v0, "messageId"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->g:Ljava/lang/String;

    .line 52
    const-string v0, "downloadUri"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->h:Ljava/lang/String;

    .line 53
    const-string v0, "msgType"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->i:Ljava/lang/String;

    .line 54
    const-string v0, "content"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->j:Ljava/lang/String;

    .line 55
    const-string v0, "sender"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->k:Ljava/lang/String;

    .line 176
    new-instance v0, Lcom/sec/chaton/multimedia/image/u;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/u;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e:Landroid/view/View$OnClickListener;

    .line 508
    new-instance v0, Lcom/sec/chaton/multimedia/image/y;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/y;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->A:Landroid/view/View$OnClickListener;

    .line 592
    new-instance v0, Lcom/sec/chaton/multimedia/image/z;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/z;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->B:Landroid/os/Handler;

    .line 80
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Lcom/sec/chaton/e/w;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 519
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    .line 520
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 521
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 523
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->w:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 525
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 526
    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 498
    invoke-static {}, Lcom/sec/common/util/i;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499
    if-eqz p1, :cond_0

    const-string v0, "thumbnail"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 501
    const/4 v0, 0x1

    .line 505
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->u:Z

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Lcom/sec/chaton/multimedia/image/aa;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->m:Lcom/sec/chaton/multimedia/image/aa;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Lcom/sec/chaton/multimedia/image/ab;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->z:Lcom/sec/chaton/multimedia/image/ab;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 164
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/chaton/j/c/a;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 167
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "downloadImage : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->f:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_0
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f()Landroid/os/Handler;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->d()Ljava/lang/String;

    move-result-object v7

    iget-wide v8, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    iget-object v10, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v11, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    iget-object v12, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d:Ljava/lang/String;

    move-object v2, v1

    move v6, v4

    invoke-virtual/range {v0 .. v12}, Lcom/sec/chaton/j/c/a;->a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 174
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v8, 0x0

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 225
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 227
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b()V

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    const-string v1, "thumbnail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 233
    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    .line 236
    :goto_0
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    const-string v1, "file:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    .line 241
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 242
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->o:Landroid/widget/ImageButton;

    const v1, 0x7f02012b

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->o:Landroid/widget/ImageButton;

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 244
    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->u:Z

    .line 246
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 247
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->l:I

    invoke-virtual {v0, v1, v8}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b(IZ)V

    .line 256
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 257
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->l:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c(IZ)V

    .line 260
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 261
    new-instance v0, Lcom/sec/chaton/multimedia/image/ab;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f()Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/multimedia/image/ab;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Landroid/os/Handler;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->z:Lcom/sec/chaton/multimedia/image/ab;

    .line 264
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->y:Lcom/sec/chaton/multimedia/image/b;

    if-nez v0, :cond_b

    .line 265
    new-instance v0, Lcom/sec/chaton/multimedia/image/b;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->w:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->B:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/chaton/multimedia/image/b;-><init>(Ljava/lang/String;Landroid/view/ViewGroup;Landroid/view/View;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->y:Lcom/sec/chaton/multimedia/image/b;

    .line 266
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_a

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->y:Lcom/sec/chaton/multimedia/image/b;

    new-array v1, v8, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 276
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->o:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->bringToFront()V

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_7

    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->u:Z

    if-eqz v0, :cond_c

    .line 281
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 286
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 287
    new-instance v1, Lcom/sec/chaton/multimedia/image/v;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/image/v;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)V

    iput-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 298
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 301
    :cond_8
    return-void

    .line 251
    :cond_9
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 252
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->l:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(IZ)V

    goto/16 :goto_1

    .line 269
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->y:Lcom/sec/chaton/multimedia/image/b;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v8, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/b;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2

    .line 271
    :cond_b
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 273
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e()Lcom/sec/common/f/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->z:Lcom/sec/chaton/multimedia/image/ab;

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_2

    .line 283
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->m:Lcom/sec/chaton/multimedia/image/aa;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_3

    :cond_d
    move-object v4, v7

    goto/16 :goto_0
.end method

.method protected c()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 304
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/j/c/a;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    invoke-static {}, Lcom/sec/chaton/util/ck;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 309
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b01bc

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b01e2

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b0037

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->t:Lcom/sec/chaton/widget/c;

    if-nez v0, :cond_2

    .line 317
    new-instance v0, Lcom/sec/chaton/widget/c;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/chaton/widget/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->t:Lcom/sec/chaton/widget/c;

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->t:Lcom/sec/chaton/widget/c;

    const v2, 0x7f0b0226

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/c;->setTitle(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->t:Lcom/sec/chaton/widget/c;

    const v2, 0x7f0b01ce

    invoke-virtual {p0, v2}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/c;->setMessage(Ljava/lang/CharSequence;)V

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->t:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/c;->setCancelable(Z)V

    .line 323
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->t:Lcom/sec/chaton/widget/c;

    const/4 v2, -0x2

    const v3, 0x7f0b0039

    invoke-virtual {p0, v3}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/sec/chaton/multimedia/image/w;

    invoke-direct {v5, p0}, Lcom/sec/chaton/multimedia/image/w;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)V

    invoke-virtual {v0, v2, v3, v5}, Lcom/sec/chaton/widget/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 361
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 362
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f()Landroid/os/Handler;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->d()Ljava/lang/String;

    move-result-object v7

    iget-wide v8, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    iget-object v10, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v11, Lcom/sec/chaton/e/r;->b:Lcom/sec/chaton/e/r;

    iget-object v12, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d:Ljava/lang/String;

    move-object v2, v1

    move v6, v4

    invoke-virtual/range {v0 .. v12}, Lcom/sec/chaton/j/c/a;->a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    .line 364
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->t:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->show()V

    .line 365
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->t:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 367
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/c/a;->b(J)Lcom/sec/chaton/j/c/c;

    move-result-object v0

    .line 368
    if-eqz v0, :cond_0

    .line 369
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->t:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/c;->a(Lcom/sec/chaton/widget/c;)V

    .line 370
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/c;->a(Landroid/os/Handler;)V

    goto/16 :goto_0
.end method

.method public d()Lcom/sec/chaton/multimedia/image/aa;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->m:Lcom/sec/chaton/multimedia/image/aa;

    return-object v0
.end method

.method protected e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->v:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    .line 547
    return-object v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 586
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d()Lcom/sec/chaton/multimedia/image/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->u:Z

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(Z)V

    .line 590
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->v:Landroid/app/Activity;

    .line 553
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 554
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->l:I

    .line 379
    new-instance v0, Lcom/sec/chaton/multimedia/image/aa;

    invoke-direct {v0, p0, p0}, Lcom/sec/chaton/multimedia/image/aa;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;Lcom/sec/chaton/multimedia/image/at;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->m:Lcom/sec/chaton/multimedia/image/aa;

    .line 381
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 382
    return-void

    .line 378
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 84
    const v0, 0x7f0300b3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 85
    const v0, 0x7f070331

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->w:Landroid/widget/FrameLayout;

    .line 86
    const v0, 0x7f070332

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    .line 87
    const v0, 0x7f070334

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a:Landroid/widget/ProgressBar;

    .line 88
    const v0, 0x7f070333

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->o:Landroid/widget/ImageButton;

    .line 92
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c()Ljava/util/HashMap;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 93
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c()Ljava/util/HashMap;

    move-result-object v2

    iget v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/t;

    .line 95
    if-eqz v0, :cond_1

    .line 97
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 98
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Item]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/t;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_0
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    .line 102
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    iput-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    .line 103
    iget-wide v2, v0, Lcom/sec/chaton/multimedia/image/t;->b:J

    iput-wide v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    .line 104
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/t;->d:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->c:Ljava/lang/String;

    .line 105
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/t;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d:Ljava/lang/String;

    .line 131
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 132
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(JLcom/sec/chaton/multimedia/image/ImagePagerFragment;)V

    .line 135
    :cond_2
    return-object v1

    .line 108
    :cond_3
    if-eqz p3, :cond_1

    .line 110
    const-string v0, "downloadUri"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    .line 111
    const-string v0, "msgType"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    .line 112
    const-string v0, "messageId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    .line 113
    const-string v0, "content"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->c:Ljava/lang/String;

    .line 114
    const-string v0, "sender"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d:Ljava/lang/String;

    .line 116
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "messageId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", mediaType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", sender: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", downloadUri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[restore from onSaveInstanceState]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->f:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 564
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 566
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 567
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e()Lcom/sec/common/f/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 568
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(J)V

    .line 570
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 572
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e()Lcom/sec/common/f/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->y:Lcom/sec/chaton/multimedia/image/b;

    if-eqz v0, :cond_1

    .line 577
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->y:Lcom/sec/chaton/multimedia/image/b;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/b;->a()V

    .line 580
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 581
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 558
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->v:Landroid/app/Activity;

    .line 559
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 560
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 392
    if-eqz v0, :cond_0

    .line 393
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 398
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 399
    if-eqz v0, :cond_1

    .line 400
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 403
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 404
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 409
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 411
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 412
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->l:I

    invoke-virtual {v0, v1, v7}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b(IZ)V

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    const-string v1, "thumbnail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 416
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a()V

    .line 419
    :cond_3
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/j/c/a;->b(J)Lcom/sec/chaton/j/c/c;

    move-result-object v0

    .line 421
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/sec/chaton/j/c/c;->a()Z

    move-result v1

    if-nez v1, :cond_7

    .line 423
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_4

    .line 424
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setImageHander : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 428
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->l:I

    invoke-virtual {v1, v2, v7}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(IZ)V

    .line 429
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->l:I

    invoke-virtual {v1, v2, v7}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c(IZ)V

    .line 430
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/j/c/c;->a(Landroid/os/Handler;)V

    .line 433
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_7

    .line 434
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 438
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->o:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 440
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    const-string v1, "file:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 441
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    .line 444
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_a

    .line 445
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->u:Z

    .line 448
    :cond_a
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 449
    new-instance v0, Lcom/sec/chaton/multimedia/image/ab;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f()Landroid/os/Handler;

    move-result-object v5

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/multimedia/image/ab;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Landroid/os/Handler;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->z:Lcom/sec/chaton/multimedia/image/ab;

    .line 451
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->y:Lcom/sec/chaton/multimedia/image/b;

    if-nez v0, :cond_11

    .line 452
    new-instance v0, Lcom/sec/chaton/multimedia/image/b;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->w:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->B:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/sec/chaton/multimedia/image/b;-><init>(Ljava/lang/String;Landroid/view/ViewGroup;Landroid/view/View;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->y:Lcom/sec/chaton/multimedia/image/b;

    .line 453
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_10

    .line 454
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->y:Lcom/sec/chaton/multimedia/image/b;

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 463
    :cond_b
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_c

    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->u:Z

    if-eqz v0, :cond_12

    .line 464
    :cond_c
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->u:Z

    if-eqz v0, :cond_d

    .line 465
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->o:Landroid/widget/ImageButton;

    const v1, 0x7f02012b

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 467
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->o:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 468
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 473
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    if-eqz v0, :cond_e

    .line 474
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 475
    new-instance v1, Lcom/sec/chaton/multimedia/image/x;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/image/x;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)V

    iput-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 486
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 489
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    if-eqz v0, :cond_f

    .line 490
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 494
    :cond_f
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 495
    return-void

    .line 456
    :cond_10
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->y:Lcom/sec/chaton/multimedia/image/b;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v7, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/b;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 459
    :cond_11
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e()Lcom/sec/common/f/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->z:Lcom/sec/chaton/multimedia/image/ab;

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_0

    .line 470
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->n:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->m:Lcom/sec/chaton/multimedia/image/aa;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 141
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "messageId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", mediaType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", sender: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", content: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", downloadUri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onSaveInstanceState]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_0
    const-string v0, "downloadUri"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v0, "msgType"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->s:Lcom/sec/chaton/e/w;

    invoke-virtual {v1}, Lcom/sec/chaton/e/w;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 156
    const-string v0, "messageId"

    iget-wide v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 157
    const-string v0, "content"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v0, "sender"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 161
    return-void
.end method
