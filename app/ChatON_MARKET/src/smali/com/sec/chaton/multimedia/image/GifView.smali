.class public Lcom/sec/chaton/multimedia/image/GifView;
.super Landroid/view/View;
.source "GifView.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/graphics/Movie;

.field private c:J

.field private d:F

.field private e:F

.field private f:Z

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/chaton/multimedia/image/GifView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/image/GifView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->f:Z

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->f:Z

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 30
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 26
    iput-boolean v3, p0, Lcom/sec/chaton/multimedia/image/GifView;->f:Z

    .line 34
    :try_start_0
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v2, 0x4000

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/image/GifView;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 44
    if-nez v0, :cond_1

    .line 45
    iput-boolean v3, p0, Lcom/sec/chaton/multimedia/image/GifView;->g:Z

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v0

    .line 36
    sget-object v1, Lcom/sec/chaton/multimedia/image/GifView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 37
    iput-boolean v3, p0, Lcom/sec/chaton/multimedia/image/GifView;->g:Z

    goto :goto_0

    .line 49
    :cond_1
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/Movie;->decodeByteArray([BII)Landroid/graphics/Movie;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->b:Landroid/graphics/Movie;

    .line 51
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->b:Landroid/graphics/Movie;

    if-nez v0, :cond_2

    .line 52
    iput-boolean v3, p0, Lcom/sec/chaton/multimedia/image/GifView;->g:Z

    goto :goto_0

    .line 56
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->b:Landroid/graphics/Movie;

    invoke-virtual {v0}, Landroid/graphics/Movie;->width()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->d:F

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->b:Landroid/graphics/Movie;

    invoke-virtual {v0}, Landroid/graphics/Movie;->height()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->e:F

    .line 59
    iget v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->d:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->e:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_0

    .line 60
    :cond_3
    iput-boolean v3, p0, Lcom/sec/chaton/multimedia/image/GifView;->g:Z

    goto :goto_0
.end method

.method private a(Ljava/io/InputStream;)[B
    .locals 4

    .prologue
    const/16 v0, 0x400

    .line 66
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 67
    new-array v0, v0, [B

    .line 70
    :goto_0
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-ltz v2, :cond_3

    .line 71
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    :try_start_1
    sget-object v2, Lcom/sec/chaton/multimedia/image/GifView;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    if-eqz p1, :cond_0

    .line 80
    :try_start_2
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 82
    :cond_0
    if-eqz v1, :cond_1

    .line 83
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 89
    :cond_1
    :goto_1
    const/4 v0, 0x0

    :cond_2
    :goto_2
    return-object v0

    .line 73
    :cond_3
    :try_start_3
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 79
    if-eqz p1, :cond_4

    .line 80
    :try_start_4
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 82
    :cond_4
    if-eqz v1, :cond_2

    .line 83
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 85
    :catch_1
    move-exception v1

    .line 86
    sget-object v2, Lcom/sec/chaton/multimedia/image/GifView;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 78
    :catchall_0
    move-exception v0

    .line 79
    if-eqz p1, :cond_5

    .line 80
    :try_start_5
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 82
    :cond_5
    if-eqz v1, :cond_6

    .line 83
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 78
    :cond_6
    :goto_3
    throw v0

    .line 85
    :catch_2
    move-exception v1

    .line 86
    sget-object v2, Lcom/sec/chaton/multimedia/image/GifView;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_3

    .line 85
    :catch_3
    move-exception v0

    .line 86
    sget-object v1, Lcom/sec/chaton/multimedia/image/GifView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->f:Z

    .line 128
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->g:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->b:Landroid/graphics/Movie;

    if-eqz v0, :cond_1

    .line 104
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 105
    iget-wide v2, p0, Lcom/sec/chaton/multimedia/image/GifView;->c:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 106
    iput-wide v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->c:J

    .line 108
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/GifView;->b:Landroid/graphics/Movie;

    invoke-virtual {v2}, Landroid/graphics/Movie;->duration()I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 109
    iget-wide v3, p0, Lcom/sec/chaton/multimedia/image/GifView;->c:J

    sub-long/2addr v0, v3

    int-to-long v2, v2

    rem-long/2addr v0, v2

    long-to-int v0, v0

    .line 110
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/GifView;->b:Landroid/graphics/Movie;

    invoke-virtual {v1, v0}, Landroid/graphics/Movie;->setTime(I)Z

    .line 112
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/chaton/multimedia/image/GifView;->d:F

    div-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/chaton/multimedia/image/GifView;->e:F

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 114
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/chaton/multimedia/image/GifView;->d:F

    mul-float/2addr v2, v0

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v6

    div-float/2addr v1, v0

    .line 115
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/chaton/multimedia/image/GifView;->e:F

    mul-float/2addr v3, v0

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, v6

    div-float/2addr v2, v0

    .line 117
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->b:Landroid/graphics/Movie;

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Movie;->draw(Landroid/graphics/Canvas;FF)V

    .line 120
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/GifView;->f:Z

    if-eqz v0, :cond_1

    .line 121
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/GifView;->invalidate()V

    .line 124
    :cond_1
    return-void
.end method
