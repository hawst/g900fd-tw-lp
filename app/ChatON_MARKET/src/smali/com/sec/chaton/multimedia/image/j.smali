.class public Lcom/sec/chaton/multimedia/image/j;
.super Landroid/os/AsyncTask;
.source "ImageEffectFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

.field private b:Landroid/view/View;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/j;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 443
    iput-object p2, p0, Lcom/sec/chaton/multimedia/image/j;->b:Landroid/view/View;

    .line 444
    iput-object p3, p0, Lcom/sec/chaton/multimedia/image/j;->c:Ljava/lang/String;

    .line 445
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 450
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/j;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->h(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/j;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/j;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-static {v1, v2, v3, v4, v4}, Lcom/sec/chaton/util/r;->a(Landroid/content/Context;Ljava/io/File;ZZZ)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->b(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/j;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/j;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/i;->a(I)Lcom/sec/chaton/multimedia/image/i;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/j;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->h(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Landroid/graphics/Bitmap;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 438
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/image/j;->a([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 464
    :try_start_0
    instance-of v1, p1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    move-object v0, p1

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 466
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    .line 467
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/j;->b:Landroid/view/View;

    const v2, 0x7f070327

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/j;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-direct {v2, v3, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 472
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/j;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->i(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/j;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/i;->a(I)Lcom/sec/chaton/multimedia/image/i;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 473
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/j;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->i(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/j;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/i;->a(I)Lcom/sec/chaton/multimedia/image/i;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    :cond_0
    :goto_1
    return-void

    .line 469
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/j;->b:Landroid/view/View;

    const v2, 0x7f070327

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/j;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-virtual {v3}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-direct {v2, v3, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 486
    :catch_0
    move-exception v1

    .line 487
    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 478
    :cond_2
    :try_start_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 479
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed PreviewFilterTask : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/j;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/i;->a(I)Lcom/sec/chaton/multimedia/image/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/image/i;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/j;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->i(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/j;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/i;->a(I)Lcom/sec/chaton/multimedia/image/i;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/j;->b:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 488
    :catch_1
    move-exception v1

    .line 489
    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 458
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 459
    return-void
.end method
