.class public Lcom/sec/chaton/multimedia/audio/a;
.super Ljava/lang/Object;
.source "AudioEffectPool.java"


# instance fields
.field private a:Landroid/media/SoundPool;

.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/multimedia/audio/a;->c:I

    .line 33
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    .line 34
    iput p1, p0, Lcom/sec/chaton/multimedia/audio/a;->c:I

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/media/SoundPool$OnLoadCompleteListener;)V
    .locals 4

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/chaton/multimedia/audio/a;->c:I

    .line 54
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/chaton/multimedia/audio/a;->c:I

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    .line 55
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    invoke-virtual {v0, p1}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    .line 59
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 101
    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/media/SoundPool;->unload(I)Z

    goto :goto_0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 116
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 119
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 121
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    if-lez v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    invoke-virtual {v1}, Landroid/media/SoundPool;->autoPause()V

    .line 124
    iget v1, p0, Lcom/sec/chaton/multimedia/audio/a;->c:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v3

    .line 125
    iget v1, p0, Lcom/sec/chaton/multimedia/audio/a;->c:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v5

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v2, v5

    int-to-float v6, v3

    div-float/2addr v2, v6

    int-to-float v5, v5

    int-to-float v3, v3

    div-float v3, v5, v3

    const/high16 v6, 0x3f800000    # 1.0f

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 131
    :cond_0
    return-void
.end method

.method public a(ILjava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 63
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 65
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, p1, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :goto_0
    return v0

    .line 67
    :catch_0
    move-exception v1

    .line 68
    const-class v2, Lcom/sec/chaton/multimedia/audio/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 81
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/a;->b:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/a;->a:Landroid/media/SoundPool;

    const/4 v3, 0x1

    invoke-virtual {v2, p1, v3}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :goto_0
    return v0

    .line 85
    :catch_0
    move-exception v1

    .line 86
    const-class v2, Lcom/sec/chaton/multimedia/audio/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
