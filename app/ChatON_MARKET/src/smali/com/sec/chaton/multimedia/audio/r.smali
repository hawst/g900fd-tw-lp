.class Lcom/sec/chaton/multimedia/audio/r;
.super Ljava/lang/Object;
.source "VoiceListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/audio/q;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/audio/q;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 140
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 142
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/audio/q;->a(Lcom/sec/chaton/multimedia/audio/q;)Lcom/sec/chaton/multimedia/audio/w;

    move-result-object v1

    if-nez v1, :cond_1

    .line 147
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    new-instance v2, Lcom/sec/chaton/multimedia/audio/w;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    invoke-static {v3}, Lcom/sec/chaton/multimedia/audio/q;->b(Lcom/sec/chaton/multimedia/audio/q;)Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/chaton/multimedia/audio/w;-><init>(Landroid/os/Handler;)V

    invoke-static {v1, v2}, Lcom/sec/chaton/multimedia/audio/q;->a(Lcom/sec/chaton/multimedia/audio/q;Lcom/sec/chaton/multimedia/audio/w;)Lcom/sec/chaton/multimedia/audio/w;

    .line 150
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/audio/q;->c(Lcom/sec/chaton/multimedia/audio/q;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/q;->a(Lcom/sec/chaton/multimedia/audio/q;)Lcom/sec/chaton/multimedia/audio/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/w;->a()V

    goto :goto_0

    .line 153
    :cond_2
    invoke-static {}, Lcom/sec/chaton/util/am;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/sec/chaton/util/am;->k()Z

    move-result v1

    if-nez v1, :cond_4

    .line 154
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00dd

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 158
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/audio/q;->d(Lcom/sec/chaton/multimedia/audio/q;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 159
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/audio/q;->d(Lcom/sec/chaton/multimedia/audio/q;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020125

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 161
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/audio/q;->a(Lcom/sec/chaton/multimedia/audio/q;)Lcom/sec/chaton/multimedia/audio/w;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/audio/w;->a(Ljava/lang/String;)V

    .line 162
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    invoke-static {v1, p1}, Lcom/sec/chaton/multimedia/audio/q;->a(Lcom/sec/chaton/multimedia/audio/q;Landroid/view/View;)Landroid/view/View;

    .line 163
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/r;->a:Lcom/sec/chaton/multimedia/audio/q;

    invoke-static {v1, v0}, Lcom/sec/chaton/multimedia/audio/q;->a(Lcom/sec/chaton/multimedia/audio/q;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
