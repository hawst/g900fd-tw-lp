.class Lcom/sec/chaton/multimedia/image/au;
.super Ljava/lang/Object;
.source "ZoomableImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 788
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x2

    const/high16 v4, 0x40a00000    # 5.0f

    const v3, 0x3e99999a    # 0.3f

    const/4 v2, 0x0

    .line 795
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->j:F

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->k:F

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    .line 797
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iput-boolean v2, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    .line 799
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->a(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 801
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 803
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 805
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    aget v2, v0, v2

    iput v2, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 807
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    aget v2, v0, v5

    iput v2, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    .line 809
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    aget v0, v0, v6

    iput v0, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    .line 813
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->j:F

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    sub-float/2addr v0, v1

    .line 815
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->k:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    sub-float/2addr v1, v2

    .line 817
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 847
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->invalidate()V

    .line 849
    return-void

    .line 823
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    .line 825
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 827
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 829
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    aget v2, v0, v2

    iput v2, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 831
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    aget v2, v0, v5

    iput v2, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    .line 833
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    aget v0, v0, v6

    iput v0, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    .line 837
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v0, v0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->j:F

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    .line 839
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->k:F

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v3

    .line 841
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 843
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/au;->a:Lcom/sec/chaton/multimedia/image/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x19

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
