.class public final enum Lcom/sec/chaton/multimedia/audio/f;
.super Ljava/lang/Enum;
.source "PlaySoundTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/multimedia/audio/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/multimedia/audio/f;

.field public static final enum b:Lcom/sec/chaton/multimedia/audio/f;

.field public static final enum c:Lcom/sec/chaton/multimedia/audio/f;

.field public static final enum d:Lcom/sec/chaton/multimedia/audio/f;

.field public static final enum e:Lcom/sec/chaton/multimedia/audio/f;

.field public static final enum f:Lcom/sec/chaton/multimedia/audio/f;

.field public static final enum g:Lcom/sec/chaton/multimedia/audio/f;

.field public static final enum h:Lcom/sec/chaton/multimedia/audio/f;

.field private static final synthetic j:[Lcom/sec/chaton/multimedia/audio/f;


# instance fields
.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 51
    new-instance v0, Lcom/sec/chaton/multimedia/audio/f;

    const-string v1, "PLAY_STATUS_INVALID"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/multimedia/audio/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/audio/f;->a:Lcom/sec/chaton/multimedia/audio/f;

    .line 52
    new-instance v0, Lcom/sec/chaton/multimedia/audio/f;

    const-string v1, "PLAY_STATUS_DOWNLOADING"

    const/16 v2, 0x384

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/chaton/multimedia/audio/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/audio/f;->b:Lcom/sec/chaton/multimedia/audio/f;

    .line 53
    new-instance v0, Lcom/sec/chaton/multimedia/audio/f;

    const-string v1, "PLAY_STATUS_DOWNLOAD_FAILED"

    const/16 v2, 0x385

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/chaton/multimedia/audio/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/audio/f;->c:Lcom/sec/chaton/multimedia/audio/f;

    .line 54
    new-instance v0, Lcom/sec/chaton/multimedia/audio/f;

    const-string v1, "PLAY_STATUS_DOWNLOAD_INTERRUPTED"

    const/16 v2, 0x386

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/chaton/multimedia/audio/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/audio/f;->d:Lcom/sec/chaton/multimedia/audio/f;

    .line 55
    new-instance v0, Lcom/sec/chaton/multimedia/audio/f;

    const-string v1, "PLAY_STATUS_STARTED"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/chaton/multimedia/audio/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/audio/f;->e:Lcom/sec/chaton/multimedia/audio/f;

    .line 56
    new-instance v0, Lcom/sec/chaton/multimedia/audio/f;

    const-string v1, "PLAY_STATUS_ONPLAYING"

    const/4 v2, 0x5

    const/16 v3, 0x3e9

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/audio/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/audio/f;->f:Lcom/sec/chaton/multimedia/audio/f;

    .line 57
    new-instance v0, Lcom/sec/chaton/multimedia/audio/f;

    const-string v1, "PLAY_STATUS_FINISHED"

    const/4 v2, 0x6

    const/16 v3, 0x3ea

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/audio/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/audio/f;->g:Lcom/sec/chaton/multimedia/audio/f;

    .line 58
    new-instance v0, Lcom/sec/chaton/multimedia/audio/f;

    const-string v1, "PLAY_STATUS_CANCELED"

    const/4 v2, 0x7

    const/16 v3, 0x3eb

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/audio/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/audio/f;->h:Lcom/sec/chaton/multimedia/audio/f;

    .line 50
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/chaton/multimedia/audio/f;

    sget-object v1, Lcom/sec/chaton/multimedia/audio/f;->a:Lcom/sec/chaton/multimedia/audio/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/multimedia/audio/f;->b:Lcom/sec/chaton/multimedia/audio/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/multimedia/audio/f;->c:Lcom/sec/chaton/multimedia/audio/f;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/multimedia/audio/f;->d:Lcom/sec/chaton/multimedia/audio/f;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/multimedia/audio/f;->e:Lcom/sec/chaton/multimedia/audio/f;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/multimedia/audio/f;->f:Lcom/sec/chaton/multimedia/audio/f;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/multimedia/audio/f;->g:Lcom/sec/chaton/multimedia/audio/f;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/multimedia/audio/f;->h:Lcom/sec/chaton/multimedia/audio/f;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/multimedia/audio/f;->j:[Lcom/sec/chaton/multimedia/audio/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput p3, p0, Lcom/sec/chaton/multimedia/audio/f;->i:I

    .line 64
    return-void
.end method

.method public static a(I)Lcom/sec/chaton/multimedia/audio/f;
    .locals 1

    .prologue
    .line 71
    sparse-switch p0, :sswitch_data_0

    .line 87
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->a:Lcom/sec/chaton/multimedia/audio/f;

    :goto_0
    return-object v0

    .line 73
    :sswitch_0
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->b:Lcom/sec/chaton/multimedia/audio/f;

    goto :goto_0

    .line 75
    :sswitch_1
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->c:Lcom/sec/chaton/multimedia/audio/f;

    goto :goto_0

    .line 77
    :sswitch_2
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->d:Lcom/sec/chaton/multimedia/audio/f;

    goto :goto_0

    .line 79
    :sswitch_3
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->e:Lcom/sec/chaton/multimedia/audio/f;

    goto :goto_0

    .line 81
    :sswitch_4
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->f:Lcom/sec/chaton/multimedia/audio/f;

    goto :goto_0

    .line 83
    :sswitch_5
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->g:Lcom/sec/chaton/multimedia/audio/f;

    goto :goto_0

    .line 85
    :sswitch_6
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->h:Lcom/sec/chaton/multimedia/audio/f;

    goto :goto_0

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x384 -> :sswitch_0
        0x385 -> :sswitch_1
        0x386 -> :sswitch_2
        0x3e8 -> :sswitch_3
        0x3e9 -> :sswitch_4
        0x3ea -> :sswitch_5
        0x3eb -> :sswitch_6
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/multimedia/audio/f;
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/sec/chaton/multimedia/audio/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/f;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/multimedia/audio/f;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->j:[Lcom/sec/chaton/multimedia/audio/f;

    invoke-virtual {v0}, [Lcom/sec/chaton/multimedia/audio/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/multimedia/audio/f;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/chaton/multimedia/audio/f;->i:I

    return v0
.end method
