.class public Lcom/sec/chaton/multimedia/multisend/PreviewPageView;
.super Landroid/support/v4/app/Fragment;
.source "PreviewPageView.java"

# interfaces
.implements Lcom/sec/chaton/multimedia/multisend/n;
.implements Lcom/sec/chaton/multimedia/multisend/p;


# instance fields
.field private a:Landroid/widget/GridView;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/sec/chaton/multimedia/multisend/o;

.field private d:Lcom/sec/common/f/c;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Landroid/widget/TextView;

.field private h:Ljava/lang/String;

.field private i:Landroid/view/Menu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/multisend/PreviewPageView;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->f:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/multisend/PreviewPageView;I)I
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->f:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/multisend/PreviewPageView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/multisend/PreviewPageView;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 201
    const-string v0, "[SendingMedia] Start - Picture"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v2, Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 203
    if-eqz p2, :cond_0

    .line 204
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 205
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 211
    const-string v0, "return-data"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 212
    const-string v0, "randomFName"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 214
    invoke-virtual {p0, v1, v3}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 215
    return-void

    .line 207
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/multisend/PreviewPageView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/multisend/PreviewPageView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->e:Ljava/lang/String;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->f:I

    if-le v0, v1, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->f:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0, p1}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a(Ljava/lang/String;)V

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->a:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 239
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 266
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->h:Ljava/lang/String;

    .line 280
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b(Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->a:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 282
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b()V

    .line 283
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    .line 244
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 245
    const-string v0, "send_list"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 246
    const-string v0, "preview_data"

    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 247
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->c(Landroid/content/Intent;)V

    .line 248
    const/4 v0, 0x1

    return v0
.end method

.method b()V
    .locals 4

    .prologue
    const v3, 0x7f0705b6

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->i:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->i:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->i:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    .line 192
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 193
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 197
    :cond_1
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 219
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 221
    packed-switch p1, :pswitch_data_0

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 224
    :pswitch_0
    const-string v0, "Get the item modified by effect function"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    if-eqz p3, :cond_0

    .line 226
    const-string v0, "temp_file_path"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 227
    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 221
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    if-eqz p1, :cond_0

    .line 53
    const-string v0, "position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->f:I

    .line 54
    const-string v0, "preview_data_tmp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    .line 64
    :goto_0
    return-void

    .line 58
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 59
    const-string v1, "preview_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 119
    const v0, 0x7f0f0034

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 121
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->i:Landroid/view/Menu;

    .line 123
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 124
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    .line 68
    const v0, 0x7f0300f9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 70
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->d:Lcom/sec/common/f/c;

    .line 72
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/o;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0300fa

    iget-object v3, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->d:Lcom/sec/common/f/c;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/multisend/o;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;Lcom/sec/chaton/multimedia/multisend/PreviewPageView;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->c:Lcom/sec/chaton/multimedia/multisend/o;

    .line 74
    const v0, 0x7f07044b

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->a:Landroid/widget/GridView;

    .line 76
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->a:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->c:Lcom/sec/chaton/multimedia/multisend/o;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->a:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/chaton/multimedia/multisend/r;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/multisend/r;-><init>(Lcom/sec/chaton/multimedia/multisend/PreviewPageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 95
    const v0, 0x7f070441

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->g:Landroid/widget/TextView;

    .line 97
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->setHasOptionsMenu(Z)V

    .line 99
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b()V

    .line 101
    return-object v6
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 178
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 179
    const-string v0, "PreviewPageView.onDestroy()"

    const-class v1, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 186
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 187
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->a:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->a:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 167
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->a:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->d:Lcom/sec/common/f/c;

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->d:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 173
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 132
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 157
    :cond_0
    :goto_0
    return v2

    .line 135
    :pswitch_0
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 140
    const/4 v4, -0x3

    if-eq v4, v0, :cond_1

    const/4 v4, -0x2

    if-ne v4, v0, :cond_3

    :cond_1
    move v0, v2

    .line 144
    :goto_1
    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/chaton/msgsend/q;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 145
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 147
    const v1, 0x7f0b0205

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 148
    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    .line 149
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 151
    :cond_2
    const-string v0, "send_list"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 152
    const-string v0, "preview_data"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 153
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;

    invoke-virtual {v0, v3}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->c(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 132
    :pswitch_data_0
    .packed-switch 0x7f0705b6
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 254
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 256
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 258
    const-string v1, "position"

    iget v2, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;->f:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 259
    const-string v1, "preview_data_tmp"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 260
    return-void
.end method
