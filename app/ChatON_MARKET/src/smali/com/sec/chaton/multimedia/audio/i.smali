.class Lcom/sec/chaton/multimedia/audio/i;
.super Ljava/util/TimerTask;
.source "PlayVoiceTask.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/audio/h;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/audio/h;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/i;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/i;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/h;->a(Lcom/sec/chaton/multimedia/audio/h;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/i;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/h;->b(Lcom/sec/chaton/multimedia/audio/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/i;->a:Lcom/sec/chaton/multimedia/audio/h;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/multimedia/audio/i;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-static {v3}, Lcom/sec/chaton/multimedia/audio/h;->a(Lcom/sec/chaton/multimedia/audio/h;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/audio/h;->a(Lcom/sec/chaton/multimedia/audio/h;[Ljava/lang/Object;)V

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CurrentPosition : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/i;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/audio/h;->a(Lcom/sec/chaton/multimedia/audio/h;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/multimedia/audio/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
