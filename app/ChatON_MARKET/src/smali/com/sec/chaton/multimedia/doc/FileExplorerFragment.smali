.class public Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;
.super Landroid/support/v4/app/Fragment;
.source "FileExplorerFragment.java"

# interfaces
.implements Lcom/sec/chaton/multimedia/doc/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/doc/g;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/sec/chaton/multimedia/doc/h;

.field private d:Ljava/lang/String;

.field private e:Landroid/widget/TextView;

.field private f:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/app/ProgressDialog;

.field private h:Lcom/sec/common/f/c;

.field private i:Landroid/widget/ListView;

.field private j:Landroid/app/Activity;

.field private k:Landroid/view/MenuItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 53
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f:Ljava/util/Stack;

    .line 155
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Ljava/io/File;)Lcom/sec/chaton/multimedia/doc/b;
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a(Ljava/io/File;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/File;)Lcom/sec/chaton/multimedia/doc/b;
    .locals 3

    .prologue
    .line 347
    invoke-virtual {p1}, Ljava/io/File;->isHidden()Z

    move-result v0

    if-nez v0, :cond_2

    .line 348
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349
    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    .line 365
    :cond_0
    :goto_0
    return-object v0

    .line 354
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 358
    :goto_1
    if-eqz v0, :cond_2

    .line 359
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->b(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    .line 360
    if-nez v0, :cond_0

    .line 365
    :cond_2
    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    goto :goto_0

    .line 355
    :catch_0
    move-exception v0

    .line 356
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/util/Stack;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f:Ljava/util/Stack;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 133
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mCurrrentPath: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_3

    .line 141
    new-instance v0, Lcom/sec/chaton/multimedia/doc/e;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/multimedia/doc/e;-><init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Lcom/sec/chaton/multimedia/doc/d;)V

    new-array v1, v5, [Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/doc/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 147
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-ge v0, v5, :cond_2

    .line 148
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->k:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->k:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 151
    :cond_2
    return-void

    .line 143
    :cond_3
    new-instance v0, Lcom/sec/chaton/multimedia/doc/e;

    invoke-direct {v0, p0, v2}, Lcom/sec/chaton/multimedia/doc/e;-><init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Lcom/sec/chaton/multimedia/doc/d;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v5, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/e;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->k:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->j:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->g:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Lcom/sec/chaton/multimedia/doc/h;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->c:Lcom/sec/chaton/multimedia/doc/h;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->i:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 395
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->c()V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->j:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 380
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 382
    iput-object p1, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->j:Landroid/app/Activity;

    .line 383
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 124
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->h:Lcom/sec/common/f/c;

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->b:Ljava/util/ArrayList;

    .line 127
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->setHasOptionsMenu(Z)V

    .line 129
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 419
    const v0, 0x7f0f0010

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 421
    const v0, 0x7f07058d

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->k:Landroid/view/MenuItem;

    .line 422
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->k:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->k:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 424
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 425
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 64
    const v0, 0x7f0300c3

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 66
    const v0, 0x7f07036b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 67
    const v2, 0x7f07014c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->e:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f07036c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->i:Landroid/widget/ListView;

    .line 69
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 73
    new-instance v0, Lcom/sec/chaton/multimedia/doc/h;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->j:Landroid/app/Activity;

    const v3, 0x7f030125

    iget-object v4, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->b:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->h:Lcom/sec/common/f/c;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/sec/chaton/multimedia/doc/h;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->c:Lcom/sec/chaton/multimedia/doc/h;

    .line 74
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->i:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->c:Lcom/sec/chaton/multimedia/doc/h;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->i:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/multimedia/doc/d;

    invoke-direct {v2, p0}, Lcom/sec/chaton/multimedia/doc/d;-><init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->j:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b00b8

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->g:Landroid/app/ProgressDialog;

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 111
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v0, v2, :cond_0

    .line 112
    new-instance v0, Lcom/sec/chaton/multimedia/doc/e;

    invoke-direct {v0, p0, v8}, Lcom/sec/chaton/multimedia/doc/e;-><init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Lcom/sec/chaton/multimedia/doc/d;)V

    new-array v2, v6, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/doc/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 117
    :goto_0
    return-object v1

    .line 114
    :cond_0
    new-instance v0, Lcom/sec/chaton/multimedia/doc/e;

    invoke-direct {v0, p0, v8}, Lcom/sec/chaton/multimedia/doc/e;-><init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Lcom/sec/chaton/multimedia/doc/d;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-virtual {v0, v2, v3}, Lcom/sec/chaton/multimedia/doc/e;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->h:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 372
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->g:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 375
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 376
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 387
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 389
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->j:Landroid/app/Activity;

    .line 390
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 405
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 406
    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->j:Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 407
    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->j:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 414
    :goto_0
    return v0

    .line 410
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f07058d

    if-ne v1, v2, :cond_1

    .line 411
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->c()V

    goto :goto_0

    .line 414
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
