.class public Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "PreviewPageActivity.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/BroadcastReceiver;

.field private c:Lcom/sec/chaton/multimedia/multisend/PreviewPageView;

.field private d:Lcom/sec/chaton/multimedia/multisend/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 27
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->e()V

    return-void
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/multisend/m;-><init>(Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->b:Landroid/content/BroadcastReceiver;

    .line 86
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 87
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 88
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 89
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 90
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 91
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->e()V

    .line 92
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 95
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 97
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->finish()V

    .line 100
    :cond_0
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 111
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageView;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->c:Lcom/sec/chaton/multimedia/multisend/PreviewPageView;

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->c:Lcom/sec/chaton/multimedia/multisend/PreviewPageView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->d:Lcom/sec/chaton/multimedia/multisend/n;

    .line 49
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->c:Lcom/sec/chaton/multimedia/multisend/PreviewPageView;

    return-object v0
.end method

.method public c(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 63
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->setResult(ILandroid/content/Intent;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->finish()V

    .line 65
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->d:Lcom/sec/chaton/multimedia/multisend/n;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->d:Lcom/sec/chaton/multimedia/multisend/n;

    invoke-interface {v0}, Lcom/sec/chaton/multimedia/multisend/n;->a()Z

    .line 60
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 135
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 136
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 137
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    if-eqz p1, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/n;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->d:Lcom/sec/chaton/multimedia/multisend/n;

    .line 42
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 105
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onPause()V

    .line 106
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->f()V

    .line 107
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 72
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 74
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->d()V

    .line 75
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 76
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 119
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 130
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 122
    :pswitch_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->d:Lcom/sec/chaton/multimedia/multisend/n;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->d:Lcom/sec/chaton/multimedia/multisend/n;

    invoke-interface {v0}, Lcom/sec/chaton/multimedia/multisend/n;->a()Z

    goto :goto_0

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;->finish()V

    goto :goto_0

    .line 119
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
