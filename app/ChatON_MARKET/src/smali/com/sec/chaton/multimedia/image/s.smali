.class public Lcom/sec/chaton/multimedia/image/s;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "ImagePagerActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 599
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/s;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    .line 600
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 601
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/s;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 606
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/s;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->l(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 607
    new-instance v0, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;

    invoke-direct {v0}, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;-><init>()V

    .line 609
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 610
    const-string v2, "position"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 611
    invoke-virtual {v0, v1}, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 613
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 614
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getItem : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    :cond_0
    :goto_0
    return-object v0

    .line 619
    :cond_1
    new-instance v0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;-><init>()V

    .line 621
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 622
    const-string v2, "position"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 623
    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 625
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 626
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getItem : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 638
    const/4 v0, -0x2

    return v0
.end method
