.class Lcom/sec/chaton/multimedia/image/z;
.super Landroid/os/Handler;
.source "ImagePagerFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)V
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 596
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 598
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 612
    :cond_0
    :goto_0
    return-void

    .line 602
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->g(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Lcom/sec/chaton/multimedia/image/ab;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 605
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 606
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e()Lcom/sec/common/f/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->d(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->g(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Lcom/sec/chaton/multimedia/image/ab;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_0

    .line 608
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->e()Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e()Lcom/sec/common/f/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->f(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/z;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->g(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Lcom/sec/chaton/multimedia/image/ab;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_0
.end method
