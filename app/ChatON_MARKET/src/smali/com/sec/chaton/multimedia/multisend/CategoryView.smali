.class public Lcom/sec/chaton/multimedia/multisend/CategoryView;
.super Landroid/support/v4/app/Fragment;
.source "CategoryView.java"


# instance fields
.field private a:Landroid/widget/GridView;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/e;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/sec/chaton/multimedia/multisend/b;

.field private f:Lcom/sec/common/f/c;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Landroid/widget/TextView;

.field private j:I

.field private k:Landroid/widget/Toast;

.field private l:Landroid/view/Menu;

.field private m:Z

.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 211
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/multisend/CategoryView;I)I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/multisend/CategoryView;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->k:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/multisend/CategoryView;)Lcom/sec/chaton/multimedia/multisend/b;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->e:Lcom/sec/chaton/multimedia/multisend/b;

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;I)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;I)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p2, :cond_1

    .line 198
    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->g:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/multisend/CategoryView;)I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/multisend/CategoryView;I)I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    return v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 209
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/multisend/CategoryView;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->k:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/multisend/CategoryView;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 4

    .prologue
    const v3, 0x7f0705b5

    .line 185
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->i:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b01d0

    invoke-virtual {p0, v2}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->l:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 187
    iget v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    if-nez v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->l:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->l:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 261
    .line 265
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 269
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/Map$Entry;

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    .line 273
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v6

    .line 278
    :goto_1
    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move-object v5, v4

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/e;

    .line 280
    iget-object v3, v0, Lcom/sec/chaton/multimedia/multisend/e;->a:Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 281
    iget-object v5, v0, Lcom/sec/chaton/multimedia/multisend/e;->c:Ljava/lang/String;

    goto :goto_2

    .line 284
    :cond_3
    iget-object v9, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->g:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/multisend/PreviewData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    move v1, v6

    .line 289
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_5
    move v0, v6

    .line 294
    :goto_4
    if-eqz v0, :cond_6

    .line 295
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 296
    const/4 v1, -0x1

    .line 289
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 301
    :cond_7
    return-void

    :cond_8
    move v0, v7

    goto :goto_4

    :cond_9
    move v0, v7

    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 306
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 308
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 312
    packed-switch p1, :pswitch_data_0

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 314
    :pswitch_0
    if-eqz p3, :cond_0

    .line 315
    const-string v0, "send_list"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 316
    const-string v1, "preview_data"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    .line 317
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    .line 319
    if-eqz v0, :cond_1

    .line 320
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 321
    const-string v2, "preview_data"

    iget-object v3, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 322
    const-string v2, "send_list"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 323
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/CategoryActivity;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/multisend/CategoryActivity;->c(Landroid/content/Intent;)V

    .line 325
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a()V

    .line 326
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 327
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a(Ljava/util/ArrayList;I)Ljava/util/HashMap;

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    goto :goto_0

    .line 312
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 52
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 56
    const-string v1, "category"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->g:Ljava/lang/String;

    .line 57
    const-string v1, "categoryName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->h:Ljava/lang/String;

    .line 58
    const-string v1, "preview_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    .line 61
    const-string v1, "POSTON"

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "caller"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->m:Z

    .line 63
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "attachedimagecount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->n:I

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    iput-boolean v4, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->m:Z

    .line 66
    iput v4, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->n:I

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 150
    const v0, 0x7f0f0033

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 152
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->l:Landroid/view/Menu;

    .line 154
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a()V

    .line 156
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 157
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    .line 72
    const v0, 0x7f0300f5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->b:Ljava/util/ArrayList;

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    .line 78
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->f:Lcom/sec/common/f/c;

    .line 80
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/b;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0300f6

    iget-object v3, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->b:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->f:Lcom/sec/common/f/c;

    iget-object v5, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/multisend/b;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->e:Lcom/sec/chaton/multimedia/multisend/b;

    .line 82
    const v0, 0x7f070442

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a:Landroid/widget/GridView;

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/chaton/multimedia/multisend/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/multisend/d;-><init>(Lcom/sec/chaton/multimedia/multisend/CategoryView;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 118
    const v0, 0x7f070441

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->i:Landroid/widget/TextView;

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 121
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/CategoryActivity;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->b:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/multisend/CategoryActivity;->a(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    .line 125
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    invoke-direct {p0, v0, v1}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a(Ljava/util/ArrayList;I)Ljava/util/HashMap;

    .line 128
    :cond_0
    iget v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    iget v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->n:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->j:I

    .line 130
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->e:Lcom/sec/chaton/multimedia/multisend/b;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 131
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->setHasOptionsMenu(Z)V

    .line 133
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a()V

    .line 135
    return-object v6
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 241
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 242
    const-string v0, "CategoryView.onDestroy()"

    const-class v1, Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->b()V

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 248
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 253
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 257
    :cond_3
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 258
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 226
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->f:Lcom/sec/common/f/c;

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->f:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 236
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 163
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 181
    :goto_0
    return v3

    .line 166
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->m:Z

    if-eqz v0, :cond_0

    .line 167
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 168
    const-string v0, "send_list"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 169
    const-string v0, "preview_data"

    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 170
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/CategoryActivity;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/multisend/CategoryActivity;->c(Landroid/content/Intent;)V

    goto :goto_0

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a(Ljava/util/HashMap;)V

    .line 173
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/multimedia/multisend/PreviewPageActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 175
    const-string v1, "preview_data"

    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 177
    invoke-virtual {p0, v0, v3}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 163
    :pswitch_data_0
    .packed-switch 0x7f0705b5
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 143
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/CategoryActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/CategoryActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/CategoryView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 144
    return-void
.end method
