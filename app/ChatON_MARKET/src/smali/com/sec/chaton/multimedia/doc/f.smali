.class public Lcom/sec/chaton/multimedia/doc/f;
.super Lcom/sec/chaton/multimedia/image/c;
.source "FileIconDispatcherTask.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/multimedia/doc/g;

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/chaton/multimedia/doc/b;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/chaton/multimedia/doc/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/doc/f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/g;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-virtual {p2}, Lcom/sec/chaton/multimedia/doc/g;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/sec/chaton/multimedia/image/c;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    .line 26
    iput-object p2, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    .line 28
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    const v2, 0x7f020185

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->c:Lcom/sec/chaton/multimedia/doc/b;

    const v2, 0x7f020188

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->d:Lcom/sec/chaton/multimedia/doc/b;

    const v2, 0x7f020186

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->e:Lcom/sec/chaton/multimedia/doc/b;

    const v2, 0x7f020187

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->f:Lcom/sec/chaton/multimedia/doc/b;

    const v2, 0x7f020189

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->g:Lcom/sec/chaton/multimedia/doc/b;

    const v2, 0x7f02018c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->h:Lcom/sec/chaton/multimedia/doc/b;

    const v2, 0x7f020184

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->i:Lcom/sec/chaton/multimedia/doc/b;

    const v2, 0x7f02018b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->j:Lcom/sec/chaton/multimedia/doc/b;

    const v2, 0x7f020198

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->m:Lcom/sec/chaton/multimedia/doc/b;

    const v2, 0x7f02018a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void

    :cond_0
    move v0, v1

    .line 25
    goto/16 :goto_0
.end method

.method private a(Lcom/sec/chaton/multimedia/doc/b;)I
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 114
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->c:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->i:Lcom/sec/chaton/multimedia/doc/b;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->l:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_1

    .line 105
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/chaton/multimedia/image/c;->a(Ljava/lang/Object;Z)V

    .line 107
    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/doc/f;->a()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0202d7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 55
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->l:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/doc/f;->a()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0202d8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    .line 51
    sget-object v1, Lcom/sec/chaton/multimedia/doc/f;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/doc/f;->a()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/chaton/multimedia/doc/f;->a(Lcom/sec/chaton/multimedia/doc/b;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 52
    :catch_1
    move-exception v0

    .line 53
    sget-object v1, Lcom/sec/chaton/multimedia/doc/f;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->f()Lcom/sec/chaton/multimedia/doc/j;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 62
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->f()Lcom/sec/chaton/multimedia/doc/j;

    move-result-object v1

    .line 63
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v3, :cond_2

    .line 64
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/doc/j;->a()J

    move-result-wide v3

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static {v0, v3, v4, v5, v6}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_3

    .line 68
    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/doc/j;->b()I

    move-result v3

    if-gtz v3, :cond_0

    .line 99
    :goto_0
    return-object v0

    .line 72
    :cond_0
    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/doc/j;->b()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/ad;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 73
    if-eqz v1, :cond_3

    .line 74
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 75
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    move-object v0, v1

    .line 77
    goto :goto_0

    .line 80
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/multimedia/doc/b;->l:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v3, :cond_3

    .line 81
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/doc/j;->a()J

    move-result-wide v3

    const/4 v1, 0x3

    const/4 v5, 0x0

    invoke-static {v0, v3, v4, v1, v5}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_3

    .line 84
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 85
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f020138

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 86
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v3, v4, v1

    invoke-direct {v0, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    sget-object v1, Lcom/sec/chaton/multimedia/doc/f;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_3
    :goto_1
    move-object v0, v2

    .line 99
    goto :goto_0

    .line 91
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/f;->b:Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->l:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_3

    .line 92
    :cond_5
    invoke-super {p0}, Lcom/sec/chaton/multimedia/image/c;->c()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 96
    :catch_1
    move-exception v0

    .line 97
    sget-object v1, Lcom/sec/chaton/multimedia/doc/f;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method
