.class Lcom/sec/chaton/multimedia/image/ad;
.super Ljava/lang/Object;
.source "PostONImagePagerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 255
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/aj;

    .line 261
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/aj;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 263
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 264
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDetailView : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :cond_2
    new-instance v2, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->c(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v3, Lcom/sec/chaton/poston/PostONDetailActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 280
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v3}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->b:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/aj;->k:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->c:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/aj;->f:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->f:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/aj;->e:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->d:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/aj;->h:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->e:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/aj;->h:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/chaton/poston/d;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 286
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->g:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/aj;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 287
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->i:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/aj;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 288
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->j:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/aj;->d:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->k:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/aj;->c:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->l:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/aj;->k:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->n:Ljava/lang/String;

    sget-object v3, Lcom/sec/chaton/poston/PostONDetailFragment;->p:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 293
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 294
    sget-object v3, Lcom/sec/chaton/poston/PostONDetailFragment;->m:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v4}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    :cond_3
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_4

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Total Read Comment : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/aj;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 301
    sget-object v0, Lcom/sec/chaton/poston/PostONDetailFragment;->o:Ljava/lang/String;

    const-string v1, "MY_PAGE"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 304
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ad;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method
