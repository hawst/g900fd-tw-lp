.class Lcom/sec/chaton/multimedia/audio/y;
.super Ljava/lang/Object;
.source "VoiceListPlayer.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/audio/w;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/audio/w;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3

    .prologue
    .line 132
    packed-switch p1, :pswitch_data_0

    .line 170
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 134
    :pswitch_1
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 135
    const-string v0, "[AudioFocus] : AUDIOFOCUS_GAIN"

    invoke-static {}, Lcom/sec/chaton/multimedia/audio/w;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/w;->c(Lcom/sec/chaton/multimedia/audio/w;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/w;->d(Lcom/sec/chaton/multimedia/audio/w;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/audio/w;->a(Lcom/sec/chaton/multimedia/audio/w;Z)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 167
    :catch_0
    move-exception v0

    .line 168
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/w;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 144
    :pswitch_2
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 145
    const-string v0, "[AudioFocus] : AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK"

    invoke-static {}, Lcom/sec/chaton/multimedia/audio/w;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/w;->d(Lcom/sec/chaton/multimedia/audio/w;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/w;->d(Lcom/sec/chaton/multimedia/audio/w;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/audio/w;->a(Lcom/sec/chaton/multimedia/audio/w;Z)Z

    goto :goto_0

    .line 155
    :pswitch_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 156
    const-string v0, "[AudioFocus] : AUDIOFOCUS_LOSS"

    invoke-static {}, Lcom/sec/chaton/multimedia/audio/w;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/w;->d(Lcom/sec/chaton/multimedia/audio/w;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/w;->d(Lcom/sec/chaton/multimedia/audio/w;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/w;->a(Lcom/sec/chaton/multimedia/audio/w;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/w;->a(Lcom/sec/chaton/multimedia/audio/w;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/y;->a:Lcom/sec/chaton/multimedia/audio/w;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/audio/w;->b(Lcom/sec/chaton/multimedia/audio/w;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
