.class public Lcom/sec/chaton/multimedia/image/ab;
.super Lcom/sec/common/f/a;
.source "ImageViewPagerDispatcherTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lcom/sec/chaton/e/w;

.field private d:Ljava/lang/String;

.field private e:Landroid/os/Handler;

.field private i:Z

.field private j:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/chaton/multimedia/image/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/image/ab;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;Ljava/lang/String;Landroid/os/Handler;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 38
    iput-object p2, p0, Lcom/sec/chaton/multimedia/image/ab;->b:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lcom/sec/chaton/multimedia/image/ab;->c:Lcom/sec/chaton/e/w;

    .line 40
    iput-object p4, p0, Lcom/sec/chaton/multimedia/image/ab;->d:Ljava/lang/String;

    .line 41
    iput-object p5, p0, Lcom/sec/chaton/multimedia/image/ab;->e:Landroid/os/Handler;

    .line 42
    iput-object p6, p0, Lcom/sec/chaton/multimedia/image/ab;->j:Landroid/widget/ImageView;

    .line 44
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[init] key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDownloadUri: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMsgContentType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->c:Lcom/sec/chaton/e/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/image/ab;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 86
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    :cond_0
    const/4 v0, 0x0

    .line 90
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x1

    invoke-static {p1, v0}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 145
    invoke-super {p0}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 97
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ab;->i:Z

    if-eqz v1, :cond_1

    .line 98
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->e:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 99
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 100
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ab;->e:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    if-eqz p1, :cond_2

    move-object v0, p1

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 105
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ab;->a()Landroid/widget/ImageView;

    move-result-object v1

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 107
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->j:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->j:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 127
    :catch_0
    move-exception v1

    .line 128
    sget-object v2, Lcom/sec/chaton/multimedia/image/ab;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 129
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->e:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 130
    iput v3, v1, Landroid/os/Message;->what:I

    .line 131
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ab;->e:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 112
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ab;->a()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 113
    const/4 v2, -0x2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 114
    const/4 v2, -0x2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 115
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ab;->a()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 117
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->c:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->c:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_5

    .line 118
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->d:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 119
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ab;->a()Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020443

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 120
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->j:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ab;->a()Landroid/widget/ImageView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 124
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ab;->a()Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020444

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 58
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-object v0

    .line 62
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->b:Ljava/lang/String;

    const-string v2, "file:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 63
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->b:Ljava/lang/String;

    .line 66
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->c:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->c:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_4

    .line 68
    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/ad;->b(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v1

    .line 70
    sget-object v2, Lcom/sec/chaton/multimedia/image/ab;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 79
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ab;->c:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ab;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/image/ab;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 71
    :catch_1
    move-exception v1

    .line 72
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/image/ab;->i:Z

    .line 73
    sget-object v2, Lcom/sec/chaton/multimedia/image/ab;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 74
    :catch_2
    move-exception v1

    .line 75
    sget-object v2, Lcom/sec/chaton/multimedia/image/ab;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public d()V
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ab;->g()Ljava/lang/Object;

    move-result-object v1

    .line 138
    if-eqz v1, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 141
    :cond_0
    return-void
.end method

.method public synthetic h()Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ab;->a()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method
