.class public Lcom/sec/chaton/multimedia/vcalendar/f;
.super Ljava/lang/Object;
.source "VCalendarListChildItem.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(Lcom/sec/chaton/multimedia/vcalendar/f;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/vcalendar/f;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->a:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/vcalendar/f;->b()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->b:Ljava/lang/Long;

    .line 26
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/vcalendar/f;->c()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->c:Ljava/lang/Long;

    .line 27
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/vcalendar/f;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->d:Ljava/lang/String;

    .line 28
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/vcalendar/f;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->e:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->a:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->b:Ljava/lang/Long;

    .line 18
    iput-object p3, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->c:Ljava/lang/Long;

    .line 19
    iput-object p4, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->d:Ljava/lang/String;

    .line 20
    iput-object p5, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->e:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->b:Ljava/lang/Long;

    return-object v0
.end method

.method public c()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->c:Ljava/lang/Long;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/f;->e:Ljava/lang/String;

    return-object v0
.end method
