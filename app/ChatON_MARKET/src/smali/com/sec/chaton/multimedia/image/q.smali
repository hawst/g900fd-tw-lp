.class Lcom/sec/chaton/multimedia/image/q;
.super Landroid/os/Handler;
.source "ImagePagerActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V
    .locals 0

    .prologue
    .line 791
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/q;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 796
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 824
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 825
    return-void

    .line 798
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/common/util/a/g;

    .line 799
    invoke-virtual {v0}, Lcom/sec/common/util/a/g;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/localbackup/a/d;

    .line 801
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/q;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-virtual {v1}, Lcom/sec/chaton/localbackup/a/d;->a()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/sec/common/util/a/g;->b()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;JLjava/lang/String;)V

    .line 803
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/q;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->n(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/chaton/localbackup/a/d;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 804
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/q;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->n(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/chaton/localbackup/a/d;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    .line 805
    invoke-virtual {v0}, Lcom/sec/common/util/a/g;->b()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 812
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/common/util/a/h;

    .line 813
    invoke-virtual {v0}, Lcom/sec/common/util/a/h;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/localbackup/a/d;

    .line 815
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/q;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->n(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/a/d;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 816
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/q;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->n(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/localbackup/a/d;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    .line 817
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 796
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
