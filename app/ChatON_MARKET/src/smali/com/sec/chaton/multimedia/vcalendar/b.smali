.class public Lcom/sec/chaton/multimedia/vcalendar/b;
.super Ljava/lang/Object;
.source "VCalParser.java"


# instance fields
.field private a:Lcom/sec/chaton/multimedia/vcalendar/j;

.field private b:Lcom/sec/chaton/multimedia/vcalendar/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/sec/chaton/multimedia/vcalendar/j;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/vcalendar/j;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->a:Lcom/sec/chaton/multimedia/vcalendar/j;

    .line 17
    new-instance v0, Lcom/sec/chaton/multimedia/vcalendar/k;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/vcalendar/k;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    .line 18
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 82
    const-string v1, ""

    .line 83
    const-string v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v0

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    move v6, v0

    move-object v0, v1

    move v1, v6

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 84
    const-string v5, "CHARSET"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 85
    const/16 v0, 0x8

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 83
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 88
    :cond_1
    return-object v0
.end method

.method private c(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 93
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 95
    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    aput-object v1, v0, v3

    .line 97
    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-le v1, v4, :cond_0

    .line 98
    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 103
    :goto_0
    return-object v0

    .line 100
    :cond_0
    const-string v1, ""

    aput-object v1, v0, v4

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/sec/chaton/multimedia/vcalendar/j;
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 22
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 23
    const-string v0, ""

    .line 25
    array-length v7, v6

    move v4, v5

    :goto_0
    if-ge v4, v7, :cond_d

    aget-object v3, v6, v4

    .line 27
    invoke-direct {p0, v3}, Lcom/sec/chaton/multimedia/vcalendar/b;->c(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 28
    if-eqz v1, :cond_2

    aget-object v1, v1, v10

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 29
    if-eqz v0, :cond_0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 32
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 37
    :goto_1
    const-string v1, ""

    .line 38
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 39
    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/vcalendar/b;->c(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 40
    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/vcalendar/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v11, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v11

    .line 47
    :goto_2
    aget-object v8, v1, v5

    .line 48
    aget-object v1, v1, v10

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/multimedia/vcalendar/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Raw Line : "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 53
    const-string v1, "SUMMARY"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 54
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcalendar/k;->i:Ljava/lang/String;

    .line 25
    :cond_1
    :goto_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move-object v0, v2

    goto/16 :goto_0

    .line 34
    :cond_2
    const-string v0, ""

    goto :goto_1

    .line 42
    :cond_3
    invoke-direct {p0, v3}, Lcom/sec/chaton/multimedia/vcalendar/b;->c(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 43
    invoke-direct {p0, v3}, Lcom/sec/chaton/multimedia/vcalendar/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v3

    .line 44
    goto :goto_2

    .line 55
    :cond_4
    const-string v1, "DTSTART"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 56
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcalendar/k;->c:Ljava/lang/String;

    goto :goto_3

    .line 57
    :cond_5
    const-string v1, "DTEND"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 58
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcalendar/k;->b:Ljava/lang/String;

    goto :goto_3

    .line 59
    :cond_6
    const-string v1, "DUE"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 60
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcalendar/k;->d:Ljava/lang/String;

    goto :goto_3

    .line 61
    :cond_7
    const-string v1, "DESCRIPTION"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 62
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcalendar/k;->a:Ljava/lang/String;

    goto :goto_3

    .line 63
    :cond_8
    const-string v1, "LOCATION"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 64
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcalendar/k;->j:Ljava/lang/String;

    goto :goto_3

    .line 65
    :cond_9
    const-string v1, "COMPLETED"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 66
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcalendar/k;->f:Ljava/lang/String;

    goto :goto_3

    .line 67
    :cond_a
    const-string v1, "RRULE"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 68
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcalendar/k;->g:Ljava/lang/String;

    goto :goto_3

    .line 69
    :cond_b
    const-string v1, "STATUS"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 70
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcalendar/k;->h:Ljava/lang/String;

    goto :goto_3

    .line 71
    :cond_c
    const-string v1, "END"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    const-string v1, "V"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcalendar/k;->b:Ljava/lang/String;

    goto/16 :goto_3

    .line 77
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->a:Lcom/sec/chaton/multimedia/vcalendar/j;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->b:Lcom/sec/chaton/multimedia/vcalendar/k;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/vcalendar/j;->a(Lcom/sec/chaton/multimedia/vcalendar/k;)V

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/b;->a:Lcom/sec/chaton/multimedia/vcalendar/j;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 13

    .prologue
    const/16 v12, 0x10

    const/4 v11, 0x2

    const/4 v1, 0x0

    .line 108
    const-string v0, "="

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 109
    const-string v0, ";"

    const-string v2, ""

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 110
    const-string v0, "="

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 111
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 117
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_8

    .line 118
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x3d

    if-ne v2, v3, :cond_1

    .line 124
    :goto_1
    array-length v7, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v7, :cond_7

    aget-object v8, v4, v2

    .line 126
    add-int/lit8 v3, v3, 0x1

    .line 128
    if-eqz v0, :cond_2

    .line 129
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 124
    :cond_0
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 117
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_2
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v9, v11, :cond_5

    .line 135
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_3

    .line 136
    new-instance v9, Ljava/math/BigInteger;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v12}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v9}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v9

    .line 138
    :try_start_0
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v9, p2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 144
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    invoke-virtual {v6, v1, v9}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 146
    :cond_3
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    :goto_4
    array-length v8, v4

    if-ne v3, v8, :cond_0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 168
    new-instance v8, Ljava/math/BigInteger;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v12}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v8}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v8

    .line 170
    :try_start_1
    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v8, p2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 171
    invoke-static {v9}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 172
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 173
    :catch_0
    move-exception v0

    .line 174
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 182
    :cond_4
    :goto_5
    return-object p1

    .line 139
    :catch_1
    move-exception v0

    .line 140
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_5

    .line 147
    :cond_5
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-le v9, v11, :cond_6

    .line 148
    const-string v9, ""

    .line 149
    invoke-virtual {v8, v1, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    new-instance v9, Ljava/math/BigInteger;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v12}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v9}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v9

    .line 152
    :try_start_2
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v9, p2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 153
    invoke-static {v10}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 154
    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    .line 160
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    invoke-virtual {v6, v1, v9}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 161
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 162
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 155
    :catch_2
    move-exception v0

    .line 156
    const-string v0, "Cannot Encode"

    const-string v1, "VCardParser"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 164
    :cond_6
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 179
    :cond_7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_5

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method
