.class Lcom/sec/chaton/multimedia/image/u;
.super Ljava/lang/Object;
.source "ImagePagerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 185
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 189
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Lcom/sec/chaton/e/w;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Lcom/sec/chaton/e/w;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_4

    .line 190
    :cond_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/amschaton/AMSPlayerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 191
    const-string v1, "AMS_FILE_PATH"

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    const-string v1, "VIEWER_MODE"

    const/16 v2, 0x3ea

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 193
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 195
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->a(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Lcom/sec/chaton/e/w;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->c()V

    goto :goto_0

    .line 201
    :cond_5
    invoke-static {}, Lcom/sec/chaton/util/am;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/sec/chaton/util/am;->k()Z

    move-result v0

    if-nez v0, :cond_7

    .line 202
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00dd

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 206
    :cond_7
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->b(Lcom/sec/chaton/multimedia/image/ImagePagerFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "video/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    const-string v1, "android.intent.extra.finishOnCompletion"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 210
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/u;->a:Lcom/sec/chaton/multimedia/image/ImagePagerFragment;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 213
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 214
    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImagePagerFragment;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
