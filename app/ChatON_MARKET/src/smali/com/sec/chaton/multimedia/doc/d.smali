.class Lcom/sec/chaton/multimedia/doc/d;
.super Ljava/lang/Object;
.source "FileExplorerFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_2

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->c(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/util/Stack;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->b(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->d(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->b(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->e(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 84
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->e(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 86
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 87
    new-instance v0, Lcom/sec/chaton/multimedia/doc/e;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/multimedia/doc/e;-><init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Lcom/sec/chaton/multimedia/doc/d;)V

    new-array v1, v2, [Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->b(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/doc/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    new-instance v0, Lcom/sec/chaton/multimedia/doc/e;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/multimedia/doc/e;-><init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Lcom/sec/chaton/multimedia/doc/d;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v3}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->b(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/e;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 93
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->e()Ljava/lang/String;

    move-result-object v0

    .line 94
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_3

    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 99
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 100
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/Activity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/d;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
