.class Lcom/sec/chaton/multimedia/multisend/b;
.super Landroid/widget/ArrayAdapter;
.source "CategoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/multimedia/multisend/e;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Lcom/sec/common/f/c;

.field private e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/e;",
            ">;",
            "Lcom/sec/common/f/c;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 33
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/b;->b:Landroid/content/Context;

    .line 35
    iput-object p4, p0, Lcom/sec/chaton/multimedia/multisend/b;->d:Lcom/sec/common/f/c;

    .line 37
    iput p2, p0, Lcom/sec/chaton/multimedia/multisend/b;->a:I

    .line 39
    iput-object p5, p0, Lcom/sec/chaton/multimedia/multisend/b;->e:Ljava/util/HashMap;

    .line 41
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/b;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/b;->c:Landroid/view/LayoutInflater;

    .line 43
    return-void
.end method

.method private a(Lcom/sec/chaton/multimedia/multisend/c;)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p1, Lcom/sec/chaton/multimedia/multisend/c;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/b;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/b;->e:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/multimedia/multisend/c;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p1, Lcom/sec/chaton/multimedia/multisend/c;->c:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p1, Lcom/sec/chaton/multimedia/multisend/c;->c:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/multimedia/multisend/c;Z)V
    .locals 3

    .prologue
    .line 108
    if-eqz p2, :cond_1

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/b;->e:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/multimedia/multisend/c;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/chaton/multimedia/multisend/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/b;->e:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/multimedia/multisend/c;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/b;->e:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/chaton/multimedia/multisend/c;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 49
    if-nez p2, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/b;->c:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/multimedia/multisend/b;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 51
    new-instance v1, Lcom/sec/chaton/multimedia/multisend/c;

    invoke-direct {v1}, Lcom/sec/chaton/multimedia/multisend/c;-><init>()V

    .line 52
    const v0, 0x7f07033e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/multisend/c;->a:Landroid/widget/ImageView;

    .line 53
    const v0, 0x7f070443

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/multisend/c;->b:Landroid/widget/ImageView;

    .line 54
    const v0, 0x7f070444

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/multisend/c;->c:Landroid/widget/CheckBox;

    .line 55
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v8, v1

    .line 60
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/multisend/b;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/sec/chaton/multimedia/multisend/e;

    .line 62
    iget-object v0, v2, Lcom/sec/chaton/multimedia/multisend/e;->a:Ljava/lang/String;

    iput-object v0, v8, Lcom/sec/chaton/multimedia/multisend/c;->d:Ljava/lang/String;

    .line 63
    iget-object v0, v2, Lcom/sec/chaton/multimedia/multisend/e;->b:Ljava/lang/String;

    iput-object v0, v8, Lcom/sec/chaton/multimedia/multisend/c;->e:Ljava/lang/String;

    .line 64
    iget-object v0, v2, Lcom/sec/chaton/multimedia/multisend/e;->c:Ljava/lang/String;

    iput-object v0, v8, Lcom/sec/chaton/multimedia/multisend/c;->f:Ljava/lang/String;

    .line 66
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/i;

    iget-object v1, v2, Lcom/sec/chaton/multimedia/multisend/e;->a:Ljava/lang/String;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/multisend/e;->b:Ljava/lang/String;

    iget-object v5, v8, Lcom/sec/chaton/multimedia/multisend/c;->f:Ljava/lang/String;

    iget-object v6, v8, Lcom/sec/chaton/multimedia/multisend/c;->c:Landroid/widget/CheckBox;

    iget-object v7, v8, Lcom/sec/chaton/multimedia/multisend/c;->a:Landroid/widget/ImageView;

    move v4, v3

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/multimedia/multisend/i;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Landroid/widget/CheckBox;Landroid/widget/ImageView;)V

    .line 68
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/b;->d:Lcom/sec/common/f/c;

    iget-object v2, v8, Lcom/sec/chaton/multimedia/multisend/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 70
    invoke-direct {p0, v8}, Lcom/sec/chaton/multimedia/multisend/b;->a(Lcom/sec/chaton/multimedia/multisend/c;)V

    .line 72
    return-object p2

    .line 57
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/c;

    move-object v8, v0

    goto :goto_0
.end method
