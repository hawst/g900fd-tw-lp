.class public Lcom/sec/chaton/multimedia/audio/k;
.super Landroid/os/AsyncTask;
.source "RecordVoiceTask.java"

# interfaces
.implements Landroid/media/MediaRecorder$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;",
        "Landroid/media/MediaRecorder$OnInfoListener;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/media/MediaRecorder;

.field private c:Landroid/media/AudioManager;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ljava/util/Timer;

.field private h:I

.field private i:Landroid/widget/LinearLayout;

.field private j:Landroid/widget/ImageView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/ProgressBar;

.field private m:Landroid/os/Handler;

.field private n:Z

.field private o:Landroid/content/Context;

.field private p:Landroid/content/BroadcastReceiver;

.field private q:Z

.field private r:Landroid/view/View;

.field private s:Landroid/media/AudioManager$OnAudioFocusChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/chaton/multimedia/audio/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/audio/k;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/widget/LinearLayout;Landroid/os/Handler;Landroid/app/Activity;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/k;->q:Z

    .line 390
    new-instance v0, Lcom/sec/chaton/multimedia/audio/p;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/audio/p;-><init>(Lcom/sec/chaton/multimedia/audio/k;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->s:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 67
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/k;->d:Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lcom/sec/chaton/multimedia/audio/k;->i:Landroid/widget/LinearLayout;

    .line 69
    iput-object p3, p0, Lcom/sec/chaton/multimedia/audio/k;->m:Landroid/os/Handler;

    .line 70
    iput-object p4, p0, Lcom/sec/chaton/multimedia/audio/k;->o:Landroid/content/Context;

    .line 71
    iput-object p5, p0, Lcom/sec/chaton/multimedia/audio/k;->r:Landroid/view/View;

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/k;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/chaton/multimedia/audio/k;->h:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/k;I)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/chaton/multimedia/audio/k;->h:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/chaton/multimedia/audio/k;->h:I

    return v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/chaton/multimedia/audio/k;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/k;->f()V

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->m:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 332
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/k;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/audio/k;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/k;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/audio/k;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/k;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/k;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/k;Z)Z
    .locals 0

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/chaton/multimedia/audio/k;->e:Z

    return p1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    .line 111
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 119
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->g:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->c:Landroid/media/AudioManager;

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->c:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/k;->s:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 138
    :cond_1
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/b;->a(Z)V

    .line 145
    :cond_2
    :goto_1
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 115
    sget-object v1, Lcom/sec/chaton/multimedia/audio/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 140
    :catch_1
    move-exception v0

    .line 141
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 142
    sget-object v1, Lcom/sec/chaton/multimedia/audio/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/audio/k;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/k;->d()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 314
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deleteFile : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/audio/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 319
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 320
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 321
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 324
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/audio/k;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->f:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 226
    new-instance v0, Lcom/sec/chaton/multimedia/audio/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/audio/m;-><init>(Lcom/sec/chaton/multimedia/audio/k;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->p:Landroid/content/BroadcastReceiver;

    .line 235
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 236
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 237
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 238
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 239
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/k;->o:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/k;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 240
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/k;->d()V

    .line 241
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/audio/k;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->m:Landroid/os/Handler;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 244
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/k;->e:Z

    .line 247
    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/audio/k;->q:Z

    .line 250
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->p:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->o:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/k;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 256
    :cond_0
    return-void
.end method

.method private f()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 335
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->l:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 337
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->k:Landroid/widget/TextView;

    const-string v1, "%d:%02d/%d:%02d"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    iput v5, p0, Lcom/sec/chaton/multimedia/audio/k;->h:I

    .line 339
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 407
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 408
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 409
    const-string v1, "ChatON"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 410
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 411
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const-wide/16 v2, 0x3e8

    .line 78
    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->g:Ljava/util/Timer;

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->g:Ljava/util/Timer;

    new-instance v1, Lcom/sec/chaton/multimedia/audio/l;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/audio/l;-><init>(Lcom/sec/chaton/multimedia/audio/k;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 91
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/k;->e:Z

    if-nez v0, :cond_1

    .line 95
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 98
    sget-object v1, Lcom/sec/chaton/multimedia/audio/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->f:Ljava/lang/String;

    return-object v0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    .line 262
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 263
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/k;->b()V

    .line 264
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/k;->e()V

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->m:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 267
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 269
    iget v1, p0, Lcom/sec/chaton/multimedia/audio/k;->h:I

    const/16 v2, 0xd

    if-lt v1, v2, :cond_0

    .line 270
    iget v1, p0, Lcom/sec/chaton/multimedia/audio/k;->h:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 277
    :goto_0
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/audio/k;->n:Z

    if-eqz v1, :cond_1

    .line 278
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/k;->o:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0211

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b027f

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b0037

    new-instance v3, Lcom/sec/chaton/multimedia/audio/o;

    invoke-direct {v3, p0, v0}, Lcom/sec/chaton/multimedia/audio/o;-><init>(Lcom/sec/chaton/multimedia/audio/k;Landroid/os/Message;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/chaton/multimedia/audio/n;

    invoke-direct {v2, p0, p1}, Lcom/sec/chaton/multimedia/audio/n;-><init>(Lcom/sec/chaton/multimedia/audio/k;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 310
    :goto_1
    return-void

    .line 272
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/k;->f:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/chaton/multimedia/audio/k;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 298
    :cond_1
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/audio/k;->q:Z

    if-eqz v1, :cond_2

    .line 299
    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/audio/k;->a(Landroid/os/Message;)V

    goto :goto_1

    .line 301
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/audio/k;->b(Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->m:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 304
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 305
    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/audio/k;->a(Landroid/os/Message;)V

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 387
    iput-boolean p1, p0, Lcom/sec/chaton/multimedia/audio/k;->e:Z

    .line 388
    return-void
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 343
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 346
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->r:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 347
    iput-boolean v5, p0, Lcom/sec/chaton/multimedia/audio/k;->e:Z

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->l:Landroid/widget/ProgressBar;

    aget-object v1, p1, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 353
    aget-object v0, p1, v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    .line 355
    aget-object v0, p1, v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit16 v0, v0, 0x258

    .line 356
    aget-object v1, p1, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    div-int/lit8 v1, v1, 0xa

    rem-int/lit8 v1, v1, 0x3c

    .line 358
    const-string v2, "%d:%02d/%d:%02d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    const/4 v0, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 360
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/k;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0

    .line 364
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 371
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 372
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/k;->b()V

    .line 373
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/k;->e()V

    .line 374
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/audio/k;->b(Ljava/lang/String;)V

    .line 375
    return-void
.end method

.method public onInfo(Landroid/media/MediaRecorder;II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 379
    const/16 v0, 0x320

    if-ne p2, v0, :cond_0

    .line 381
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/audio/k;->e:Z

    .line 382
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/audio/k;->n:Z

    .line 384
    :cond_0
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/k;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 149
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 150
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/k;->c()V

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->i:Landroid/widget/LinearLayout;

    const v1, 0x7f070106

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->j:Landroid/widget/ImageView;

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->i:Landroid/widget/LinearLayout;

    const v1, 0x7f070108

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->k:Landroid/widget/TextView;

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->i:Landroid/widget/LinearLayout;

    const v1, 0x7f070107

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->l:Landroid/widget/ProgressBar;

    .line 156
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/k;->f()V

    .line 159
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/k;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 164
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 165
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 168
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".3gp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->f:Ljava/lang/String;

    .line 170
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->c:Landroid/media/AudioManager;

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->c:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/k;->s:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 185
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/k;->g()V

    .line 187
    new-instance v0, Landroid/media/MediaRecorder;

    invoke-direct {v0}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    invoke-virtual {v0, p0}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v4}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v4}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v4}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/k;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    const-wide/32 v1, 0x100000

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRecorder;->setMaxFileSize(J)V

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    const v1, 0x1d6b4

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setMaxDuration(I)V

    .line 203
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->prepare()V

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/k;->b:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->start()V

    .line 205
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/b;->a(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 222
    :goto_1
    return-void

    .line 162
    :cond_1
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/k;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 206
    :catch_0
    move-exception v0

    .line 207
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 208
    sget-object v1, Lcom/sec/chaton/multimedia/audio/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 210
    :cond_2
    iput-boolean v4, p0, Lcom/sec/chaton/multimedia/audio/k;->e:Z

    goto :goto_1

    .line 211
    :catch_1
    move-exception v0

    .line 212
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 213
    sget-object v1, Lcom/sec/chaton/multimedia/audio/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 215
    :cond_3
    iput-boolean v4, p0, Lcom/sec/chaton/multimedia/audio/k;->e:Z

    goto :goto_1

    .line 216
    :catch_2
    move-exception v0

    .line 217
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_4

    .line 218
    sget-object v1, Lcom/sec/chaton/multimedia/audio/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 220
    :cond_4
    iput-boolean v4, p0, Lcom/sec/chaton/multimedia/audio/k;->e:Z

    goto :goto_1
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/k;->a([Ljava/lang/Integer;)V

    return-void
.end method
