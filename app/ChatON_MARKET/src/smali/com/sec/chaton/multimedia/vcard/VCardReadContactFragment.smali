.class public Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;
.super Landroid/support/v4/app/ListFragment;
.source "VCardReadContactFragment.java"

# interfaces
.implements Lcom/sec/chaton/multimedia/vcard/n;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/multimedia/vcard/a;

.field private c:Lcom/sec/chaton/multimedia/vcard/o;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/sec/chaton/multimedia/vcard/b;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Landroid/app/ProgressDialog;

.field private i:Ljava/lang/String;

.field private j:Landroid/content/Context;

.field private k:Ljava/lang/String;

.field private l:Landroid/widget/LinearLayout;

.field private m:Ljava/lang/String;

.field private n:Landroid/view/MenuItem;

.field private o:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 45
    iput-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->c:Lcom/sec/chaton/multimedia/vcard/o;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->f:I

    .line 53
    iput-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->i:Ljava/lang/String;

    .line 400
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/r;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/vcard/r;-><init>(Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->o:Ljava/lang/Runnable;

    return-void
.end method

.method private a(I)I
    .locals 4

    .prologue
    .line 185
    const/4 v0, 0x0

    .line 187
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    .line 188
    iget v3, v0, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    if-ne v3, p1, :cond_1

    iget-boolean v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->f:Z

    if-eqz v0, :cond_1

    .line 189
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    .line 192
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;)Lcom/sec/chaton/multimedia/vcard/a;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->b:Lcom/sec/chaton/multimedia/vcard/a;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 226
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/q;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/vcard/q;-><init>(Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;)V

    .line 238
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 240
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 101
    move v1, v0

    move v2, v0

    .line 102
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_3

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    const/16 v3, 0x9

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-boolean v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->f:Z

    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iput-object v7, v0, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-boolean v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->f:Z

    if-eqz v0, :cond_1

    .line 108
    add-int/lit8 v2, v2, 0x1

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    const/4 v3, 0x5

    if-ne v0, v3, :cond_2

    .line 110
    iget-object v3, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v4, v0, Lcom/sec/chaton/multimedia/vcard/c;->a:I

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v5, v0, Lcom/sec/chaton/multimedia/vcard/c;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v0, v6}, Lcom/sec/chaton/multimedia/vcard/b;->a(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 102
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    if-ne v0, v6, :cond_1

    .line 112
    iget-object v3, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v4, v0, Lcom/sec/chaton/multimedia/vcard/c;->a:I

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v5, v0, Lcom/sec/chaton/multimedia/vcard/c;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v0, v6}, Lcom/sec/chaton/multimedia/vcard/b;->b(ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 116
    :cond_3
    if-gtz v2, :cond_4

    .line 117
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 123
    :goto_2
    return-void

    .line 119
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 120
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->o:Ljava/lang/Runnable;

    invoke-direct {v0, v7, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;)V

    .line 121
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_2
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 332
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 334
    :cond_0
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->b:Lcom/sec/chaton/multimedia/vcard/a;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcard/a;->a()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 131
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->j:Landroid/content/Context;

    const v1, 0x7f0b00d8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 133
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 408
    .line 409
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/k;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/vcard/k;-><init>()V

    .line 412
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/vcard/k;->a(Lcom/sec/chaton/multimedia/vcard/b;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->i:Ljava/lang/String;

    .line 413
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "*** VCARD CONTENTS : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a(Ljava/lang/String;)V

    .line 423
    :goto_1
    return-void

    .line 414
    :catch_0
    move-exception v0

    .line 415
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 421
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 450
    iput v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->f:I

    .line 451
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->n:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 452
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->c()V

    .line 453
    return-void
.end method

.method protected a(Lcom/sec/chaton/multimedia/vcard/b;)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 246
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 247
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/b;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/vcard/b;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    .line 248
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    .line 251
    :cond_0
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    .line 254
    :cond_1
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    .line 258
    :cond_2
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 259
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    .line 263
    :cond_3
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    if-eqz v0, :cond_7

    move v6, v7

    .line 264
    :goto_0
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v6, :cond_7

    .line 266
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/vcard/f;->c:Ljava/lang/String;

    .line 269
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->a:I

    if-eq v0, v8, :cond_4

    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->a:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->a:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 273
    :cond_4
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget-object v2, v0, Lcom/sec/chaton/multimedia/vcard/f;->b:Ljava/lang/String;

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ORIGINAL PHONE NUMBER : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EDITED PHONE NUMBER : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/multimedia/vcard/f;

    iget v1, v1, Lcom/sec/chaton/multimedia/vcard/f;->a:I

    const/4 v4, 0x5

    if-nez v6, :cond_6

    move v5, v8

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/vcard/c;-><init>(ILjava/lang/String;Ljava/lang/String;IZ)V

    .line 279
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    :cond_5
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    :cond_6
    move v5, v7

    .line 277
    goto :goto_1

    .line 283
    :cond_7
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 285
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/d;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/vcard/d;->c:Ljava/lang/String;

    .line 286
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/multimedia/vcard/d;

    iget v1, v1, Lcom/sec/chaton/multimedia/vcard/d;->a:I

    iget-object v2, p1, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/multimedia/vcard/d;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/d;->b:Ljava/lang/String;

    move v4, v8

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/vcard/c;-><init>(ILjava/lang/String;Ljava/lang/String;IZ)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    :cond_8
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 291
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v2, p1, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    const/4 v3, 0x0

    const/16 v4, 0x9

    move v1, v7

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/vcard/c;-><init>(ILjava/lang/String;Ljava/lang/String;IZ)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    :cond_9
    const-string v0, ""

    .line 296
    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 297
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 314
    :cond_a
    :goto_2
    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->b(Ljava/lang/String;)V

    .line 316
    iput-object v9, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    .line 318
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_10

    .line 319
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/o;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/multimedia/vcard/o;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->c:Lcom/sec/chaton/multimedia/vcard/o;

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->c:Lcom/sec/chaton/multimedia/vcard/o;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 321
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 328
    :goto_3
    return-void

    .line 300
    :cond_b
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    goto :goto_2

    .line 303
    :cond_c
    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 304
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    goto :goto_2

    .line 306
    :cond_d
    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 307
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    goto :goto_2

    .line 308
    :cond_e
    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    if-eqz v1, :cond_f

    .line 309
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->b:Ljava/lang/String;

    goto :goto_2

    .line 310
    :cond_f
    iget-object v1, p1, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    if-eqz v1, :cond_a

    .line 311
    iget-object v0, p1, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/d;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/d;->b:Ljava/lang/String;

    goto :goto_2

    .line 324
    :cond_10
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/widget/ListView;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 337
    .line 338
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 339
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 346
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 347
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 348
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 350
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/co;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".VCF"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->g:Ljava/lang/String;

    .line 354
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->g:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 362
    if-eqz v1, :cond_1

    .line 363
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 369
    :cond_1
    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 371
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->g:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 372
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 373
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->i:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a(Ljava/lang/String;)V

    .line 376
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 377
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 381
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 382
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 383
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 384
    sget-object v2, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 394
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 395
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 396
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 398
    :goto_2
    return-void

    .line 341
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 342
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_2

    .line 356
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 357
    :goto_3
    :try_start_3
    sget-object v2, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 362
    if-eqz v1, :cond_1

    .line 363
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 365
    :catch_1
    move-exception v0

    .line 366
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a:Ljava/lang/String;

    :goto_4
    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 358
    :catch_2
    move-exception v0

    .line 359
    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 362
    if-eqz v2, :cond_1

    .line 363
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 365
    :catch_3
    move-exception v0

    .line 366
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a:Ljava/lang/String;

    goto :goto_4

    .line 361
    :catchall_0
    move-exception v0

    .line 362
    :goto_6
    if-eqz v2, :cond_5

    .line 363
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 361
    :cond_5
    :goto_7
    throw v0

    .line 386
    :cond_6
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 388
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 389
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->e:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 391
    :cond_8
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 365
    :catch_4
    move-exception v1

    .line 366
    sget-object v2, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_7

    .line 365
    :catch_5
    move-exception v0

    .line 366
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a:Ljava/lang/String;

    goto :goto_4

    .line 361
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_6

    .line 358
    :catch_6
    move-exception v0

    move-object v2, v1

    goto :goto_5

    .line 356
    :catch_7
    move-exception v0

    goto :goto_3
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 205
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a(Landroid/net/Uri;)V

    .line 206
    invoke-virtual {p3}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->m:Ljava/lang/String;

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "inbox_NO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->k:Ljava/lang/String;

    .line 69
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/multimedia/vcard/a;->a(Landroid/content/Context;)Lcom/sec/chaton/multimedia/vcard/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->b:Lcom/sec/chaton/multimedia/vcard/a;

    .line 70
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->j:Landroid/content/Context;

    .line 71
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->setHasOptionsMenu(Z)V

    .line 73
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 441
    const v0, 0x7f0f0023

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 443
    const v0, 0x7f0705a6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->n:Landroid/view/MenuItem;

    .line 444
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->n:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 445
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 446
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    const v0, 0x7f0300c6

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 79
    const v0, 0x7f070375

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->l:Landroid/widget/LinearLayout;

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->j:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->h:Landroid/app/ProgressDialog;

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->h:Landroid/app/ProgressDialog;

    const v2, 0x7f0b0041

    invoke-virtual {p0, v2}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 86
    if-nez p3, :cond_1

    .line 87
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->c()V

    .line 97
    :cond_0
    :goto_0
    return-object v1

    .line 88
    :cond_1
    const-string v0, "dataUriString"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const-string v0, "dataUriString"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->m:Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 146
    invoke-super/range {p0 .. p5}, Landroid/support/v4/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "POSITION : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VCardReadContactActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CONTACT KIND : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VCardReadContactActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CONTACT TYPE : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->a:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VCardReadContactActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CONTACT DATA : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VCardReadContactActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/multimedia/vcard/c;

    iget-boolean v1, v1, Lcom/sec/chaton/multimedia/vcard/c;->f:Z

    if-nez v1, :cond_3

    move v1, v2

    :goto_0
    iput-boolean v1, v0, Lcom/sec/chaton/multimedia/vcard/c;->f:Z

    .line 156
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-boolean v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->f:Z

    if-eqz v0, :cond_5

    .line 157
    iget v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->f:I

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->a(I)I

    move-result v0

    if-le v0, v5, :cond_1

    .line 160
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->j:Landroid/content/Context;

    const v1, 0x7f0b013f

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-virtual {p0, v1, v4}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 167
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iput-boolean v3, v0, Lcom/sec/chaton/multimedia/vcard/c;->f:Z

    .line 168
    iget v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->f:I

    .line 174
    :cond_1
    :goto_2
    iget v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->f:I

    if-lez v0, :cond_6

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->n:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 179
    :goto_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/p;

    .line 180
    iget-object v1, v0, Lcom/sec/chaton/multimedia/vcard/p;->c:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-boolean v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->f:Z

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 182
    :cond_2
    return-void

    :cond_3
    move v1, v3

    .line 154
    goto :goto_0

    .line 163
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    if-ne v0, v2, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->j:Landroid/content/Context;

    const v1, 0x7f0b0140

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-virtual {p0, v1, v4}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 171
    :cond_5
    iget v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->f:I

    goto :goto_2

    .line 177
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->n:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 427
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 428
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 429
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 436
    :goto_0
    return v0

    .line 432
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0705a6

    if-ne v1, v2, :cond_1

    .line 433
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->b()V

    goto :goto_0

    .line 436
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 140
    const-string v0, "dataUriString"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 142
    return-void
.end method
