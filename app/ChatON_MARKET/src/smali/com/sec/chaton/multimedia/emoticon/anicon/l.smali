.class Lcom/sec/chaton/multimedia/emoticon/anicon/l;
.super Ljava/lang/Thread;
.source "AniconPlusTask.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method private constructor <init>(Lcom/sec/chaton/multimedia/emoticon/anicon/j;)V
    .locals 1

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->b:Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->c:Ljava/lang/String;

    .line 43
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->d:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/multimedia/emoticon/anicon/j;Lcom/sec/chaton/multimedia/emoticon/anicon/k;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/emoticon/anicon/l;-><init>(Lcom/sec/chaton/multimedia/emoticon/anicon/j;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Lcom/sec/chaton/multimedia/emoticon/anicon/j;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    new-array v0, v4, [Ljava/lang/Class;

    .line 62
    :try_start_0
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Lcom/sec/chaton/multimedia/emoticon/anicon/j;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "cancel"

    invoke-virtual {v2, v3, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 67
    :goto_0
    new-array v2, v4, [Ljava/lang/Object;

    .line 68
    if-eqz v0, :cond_0

    .line 70
    :try_start_1
    iget-object v3, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-static {v3}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Lcom/sec/chaton/multimedia/emoticon/anicon/j;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    .line 80
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Lcom/sec/chaton/multimedia/emoticon/anicon/j;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/l;

    .line 82
    iput-boolean v4, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->b:Z

    .line 83
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 71
    :catch_1
    move-exception v0

    .line 72
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 73
    :catch_2
    move-exception v0

    .line 74
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 75
    :catch_3
    move-exception v0

    .line 76
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->d:I

    .line 52
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->c:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public run()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->d:I

    if-lez v0, :cond_1

    .line 89
    const-string v0, "[AniconId, RepeatCount does not set.] "

    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/settings/downloads/u;->e(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 95
    if-nez v0, :cond_2

    .line 96
    const-string v0, "[pattern files does not exist.] "

    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 100
    :cond_2
    iget-boolean v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->b:Z

    if-ne v2, v6, :cond_3

    .line 101
    const-string v2, "[vibrating.. ]"

    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a()V

    .line 106
    :cond_3
    new-array v4, v6, [Ljava/lang/Class;

    const-class v2, [B

    aput-object v2, v4, v7

    .line 112
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v0

    new-array v0, v0, [B
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 114
    :try_start_2
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_c
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 121
    if-eqz v2, :cond_4

    .line 123
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9

    .line 130
    :cond_4
    :goto_1
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Lcom/sec/chaton/multimedia/emoticon/anicon/j;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_5

    .line 131
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v3

    const-string v5, "vibrator"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Lcom/sec/chaton/multimedia/emoticon/anicon/j;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :cond_5
    :try_start_4
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Lcom/sec/chaton/multimedia/emoticon/anicon/j;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "vibrateImmVibe"

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v1

    .line 140
    :goto_2
    iput-boolean v6, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->b:Z

    .line 142
    if-eqz v1, :cond_0

    .line 143
    :goto_3
    iget-boolean v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->b:Z

    if-eqz v2, :cond_0

    .line 146
    :try_start_5
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->a:Lcom/sec/chaton/multimedia/emoticon/anicon/j;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->a(Lcom/sec/chaton/multimedia/emoticon/anicon/j;)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_6

    .line 156
    :goto_4
    iget v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->d:I

    if-nez v2, :cond_6

    .line 157
    iput-boolean v7, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/l;->b:Z

    .line 159
    :cond_6
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_3

    .line 115
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 117
    :goto_5
    :try_start_6
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 121
    if-eqz v2, :cond_7

    .line 123
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_8

    :cond_7
    :goto_6
    move-object v0, v1

    .line 128
    goto :goto_1

    .line 118
    :catch_1
    move-exception v0

    move-object v2, v0

    move-object v3, v1

    move-object v0, v1

    .line 119
    :goto_7
    :try_start_8
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 121
    if-eqz v3, :cond_4

    .line 123
    :try_start_9
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    goto :goto_1

    .line 124
    :catch_2
    move-exception v2

    .line 125
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v3

    :goto_8
    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 121
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_9
    if-eqz v2, :cond_8

    .line 123
    :try_start_a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 121
    :cond_8
    :goto_a
    throw v0

    .line 136
    :catch_3
    move-exception v2

    .line 137
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_2

    .line 148
    :catch_4
    move-exception v2

    .line 149
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_4

    .line 150
    :catch_5
    move-exception v2

    .line 151
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_4

    .line 152
    :catch_6
    move-exception v2

    .line 153
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_4

    .line 124
    :catch_7
    move-exception v1

    .line 125
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_a

    .line 124
    :catch_8
    move-exception v0

    .line 125
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_6

    .line 124
    :catch_9
    move-exception v2

    .line 125
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/anicon/j;->c()Ljava/lang/String;

    move-result-object v3

    goto :goto_8

    .line 121
    :catchall_1
    move-exception v0

    goto :goto_9

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_9

    .line 118
    :catch_a
    move-exception v0

    move-object v3, v2

    move-object v2, v0

    move-object v0, v1

    goto :goto_7

    :catch_b
    move-exception v3

    move-object v8, v3

    move-object v3, v2

    move-object v2, v8

    goto :goto_7

    .line 115
    :catch_c
    move-exception v0

    goto :goto_5
.end method
