.class public Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;
.super Landroid/widget/FrameLayout;
.source "EmoticonSelectionView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/chaton/multimedia/emoticon/ams/c;
.implements Lcom/sec/chaton/multimedia/emoticon/anicon/h;
.implements Lcom/sec/chaton/multimedia/emoticon/f;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Landroid/support/v4/app/Fragment;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/support/v4/app/Fragment;",
            "Landroid/support/v4/app/Fragment$SavedState;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/widget/RelativeLayout;

.field private g:Landroid/widget/ImageButton;

.field private h:Landroid/widget/ImageButton;

.field private i:Landroid/widget/ImageButton;

.field private j:Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;

.field private k:Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;

.field private l:Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;

.field private m:Lcom/sec/chaton/multimedia/emoticon/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->c:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    .line 72
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->b:Landroid/content/Context;

    .line 74
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a()V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->c:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    .line 80
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->b:Landroid/content/Context;

    .line 82
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a()V

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->c:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    .line 88
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->b:Landroid/content/Context;

    .line 90
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a()V

    .line 91
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 102
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "EmoticonSelectionView.initialize()"

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->e:Ljava/util/Map;

    .line 109
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 112
    const v1, 0x7f0300a5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 114
    const v0, 0x7f07030f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->f:Landroid/widget/RelativeLayout;

    .line 115
    const v0, 0x7f070023

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->g:Landroid/widget/ImageButton;

    .line 116
    const v0, 0x7f070024

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->h:Landroid/widget/ImageButton;

    .line 117
    const v0, 0x7f070025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->i:Landroid/widget/ImageButton;

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    const-string v0, "anicon"

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a(Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 156
    invoke-static {}, Lcom/sec/chaton/util/ck;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "animessage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01e2

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 173
    :try_start_0
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_3

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->e:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentManager;->saveFragmentInstanceState(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    :cond_3
    const-string v0, "emoticon"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 184
    const-string v0, "emoticon"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->c:Ljava/lang/String;

    .line 186
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 187
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 188
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 190
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->j:Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;

    if-nez v0, :cond_4

    .line 191
    const v0, 0x7f0300a4

    invoke-static {v0, v4}, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->a(IZ)Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->j:Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->j:Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->a(Lcom/sec/chaton/multimedia/emoticon/f;)V

    .line 195
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->j:Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    .line 225
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->e:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 227
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->e:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    invoke-virtual {v2, v0}, Landroid/support/v4/app/Fragment;->setInitialSavedState(Landroid/support/v4/app/Fragment$SavedState;)V

    .line 231
    :cond_6
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f07000d

    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    .line 196
    :cond_7
    const-string v0, "anicon"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 197
    const-string v0, "anicon"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->c:Ljava/lang/String;

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->k:Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;

    if-nez v0, :cond_8

    .line 204
    const v0, 0x7f030065

    invoke-static {v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->a(I)Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->k:Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->k:Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->a(Lcom/sec/chaton/multimedia/emoticon/anicon/h;)V

    .line 208
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->k:Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    goto :goto_2

    .line 209
    :cond_9
    const-string v0, "animessage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 210
    const-string v0, "animessage"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->c:Ljava/lang/String;

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->l:Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;

    if-nez v0, :cond_a

    .line 217
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->a()Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->l:Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->l:Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;

    invoke-virtual {v0, p0}, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->a(Lcom/sec/chaton/multimedia/emoticon/ams/c;)V

    .line 221
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->l:Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->d:Landroid/support/v4/app/Fragment;

    goto/16 :goto_2

    .line 174
    :catch_0
    move-exception v0

    goto/16 :goto_1
.end method

.method private b()Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 130
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 131
    const-string v0, "EmoticonSelectionView.initializeForPhone()"

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->f:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 137
    const v0, 0x7f070023

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->g:Landroid/widget/ImageButton;

    .line 138
    const v0, 0x7f070024

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->h:Landroid/widget/ImageButton;

    .line 139
    const v0, 0x7f070025

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->i:Landroid/widget/ImageButton;

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->h:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 152
    return-void
.end method

.method public a(Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 272
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 273
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "onEmoticonClicked() "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->m:Lcom/sec/chaton/multimedia/emoticon/i;

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->m:Lcom/sec/chaton/multimedia/emoticon/i;

    invoke-interface {v0, p2}, Lcom/sec/chaton/multimedia/emoticon/i;->a(Ljava/lang/String;)V

    .line 279
    :cond_1
    return-void
.end method

.method public a(Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;)V
    .locals 3

    .prologue
    .line 294
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 295
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "onCreateNewAmsItem() "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->m:Lcom/sec/chaton/multimedia/emoticon/i;

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->m:Lcom/sec/chaton/multimedia/emoticon/i;

    invoke-interface {v0}, Lcom/sec/chaton/multimedia/emoticon/i;->l()V

    .line 301
    :cond_1
    return-void
.end method

.method public a(Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;Lcom/sec/chaton/multimedia/emoticon/ams/d;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 305
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 306
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "onAmsItemClicked() "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, ", "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->m:Lcom/sec/chaton/multimedia/emoticon/i;

    if-eqz v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->m:Lcom/sec/chaton/multimedia/emoticon/i;

    invoke-interface {v0, p2, p3}, Lcom/sec/chaton/multimedia/emoticon/i;->a(Lcom/sec/chaton/multimedia/emoticon/ams/d;Ljava/lang/String;)V

    .line 312
    :cond_1
    return-void
.end method

.method public a(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 283
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 284
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "onAniconClicked() "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, ", "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->m:Lcom/sec/chaton/multimedia/emoticon/i;

    if-eqz v0, :cond_1

    .line 288
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->m:Lcom/sec/chaton/multimedia/emoticon/i;

    invoke-interface {v0, p2}, Lcom/sec/chaton/multimedia/emoticon/i;->b(Ljava/lang/String;)V

    .line 290
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->g:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    const-string v0, "emoticon"

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a(Ljava/lang/String;)V

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->h:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260
    const-string v0, "anicon"

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 261
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->i:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    const-string v0, "animessage"

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setEmoticonSelectedListener(Lcom/sec/chaton/multimedia/emoticon/i;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonSelectionView;->m:Lcom/sec/chaton/multimedia/emoticon/i;

    .line 99
    return-void
.end method
