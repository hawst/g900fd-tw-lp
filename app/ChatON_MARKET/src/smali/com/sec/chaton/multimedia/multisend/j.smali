.class Lcom/sec/chaton/multimedia/multisend/j;
.super Ljava/lang/Object;
.source "GalleryView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/multisend/GalleryView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/multisend/GalleryView;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/j;->a:Lcom/sec/chaton/multimedia/multisend/GalleryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/j;->a:Lcom/sec/chaton/multimedia/multisend/GalleryView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->a(Lcom/sec/chaton/multimedia/multisend/GalleryView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/k;

    iget-object v1, v0, Lcom/sec/chaton/multimedia/multisend/k;->e:Ljava/lang/String;

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/j;->a:Lcom/sec/chaton/multimedia/multisend/GalleryView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->a(Lcom/sec/chaton/multimedia/multisend/GalleryView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/k;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/multisend/k;->a:Ljava/lang/String;

    .line 87
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/multisend/j;->a:Lcom/sec/chaton/multimedia/multisend/GalleryView;

    invoke-virtual {v3}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/chaton/multimedia/multisend/CategoryActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    const-string v3, "category"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    const-string v1, "categoryName"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/j;->a:Lcom/sec/chaton/multimedia/multisend/GalleryView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->b(Lcom/sec/chaton/multimedia/multisend/GalleryView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const-string v0, "caller"

    const-string v1, "POSTON"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v0, "attachedimagecount"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/j;->a:Lcom/sec/chaton/multimedia/multisend/GalleryView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c(Lcom/sec/chaton/multimedia/multisend/GalleryView;)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 95
    :cond_0
    const-string v0, "preview_data"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/j;->a:Lcom/sec/chaton/multimedia/multisend/GalleryView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->d(Lcom/sec/chaton/multimedia/multisend/GalleryView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/j;->a:Lcom/sec/chaton/multimedia/multisend/GalleryView;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 97
    return-void
.end method
