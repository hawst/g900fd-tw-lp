.class public final enum Lcom/sec/chaton/multimedia/emoticon/ams/d;
.super Ljava/lang/Enum;
.source "AmsContainerItemType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/multimedia/emoticon/ams/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/multimedia/emoticon/ams/d;

.field public static final enum b:Lcom/sec/chaton/multimedia/emoticon/ams/d;

.field public static final enum c:Lcom/sec/chaton/multimedia/emoticon/ams/d;

.field private static final synthetic e:[Lcom/sec/chaton/multimedia/emoticon/ams/d;


# instance fields
.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 13
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;

    const-string v1, "Basic"

    const-wide/16 v2, 0x2710

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/chaton/multimedia/emoticon/ams/d;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    .line 14
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;

    const-string v1, "Download"

    const-wide/16 v2, 0x4e20

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/chaton/multimedia/emoticon/ams/d;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->b:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    .line 15
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;

    const-string v1, "User"

    const-wide/16 v2, 0x7530

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/chaton/multimedia/emoticon/ams/d;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->c:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    .line 12
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/chaton/multimedia/emoticon/ams/d;

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/ams/d;->b:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/ams/d;->c:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->e:[Lcom/sec/chaton/multimedia/emoticon/ams/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput-wide p3, p0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->d:J

    .line 21
    return-void
.end method

.method public static a(J)Lcom/sec/chaton/multimedia/emoticon/ams/d;
    .locals 3

    .prologue
    .line 28
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->c:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a()J

    move-result-wide v0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    .line 29
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->c:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    .line 33
    :goto_0
    return-object v0

    .line 30
    :cond_0
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->b:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a()J

    move-result-wide v0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_1

    .line 31
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->b:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    goto :goto_0

    .line 32
    :cond_1
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a()J

    move-result-wide v0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_2

    .line 33
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    goto :goto_0

    .line 35
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown item type("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/multimedia/emoticon/ams/d;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/multimedia/emoticon/ams/d;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->e:[Lcom/sec/chaton/multimedia/emoticon/ams/d;

    invoke-virtual {v0}, [Lcom/sec/chaton/multimedia/emoticon/ams/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/multimedia/emoticon/ams/d;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->d:J

    return-wide v0
.end method
