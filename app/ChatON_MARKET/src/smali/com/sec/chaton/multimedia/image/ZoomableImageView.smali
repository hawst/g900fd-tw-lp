.class public Lcom/sec/chaton/multimedia/image/ZoomableImageView;
.super Landroid/widget/ImageView;
.source "ZoomableImageView.java"


# instance fields
.field private A:Landroid/graphics/Bitmap;

.field private B:I

.field private C:I

.field private D:Landroid/os/Handler;

.field private E:Landroid/view/GestureDetector;

.field private F:I

.field private G:Ljava/lang/Runnable;

.field private H:Ljava/lang/Runnable;

.field a:Landroid/graphics/Paint;

.field b:Landroid/graphics/Matrix;

.field c:Landroid/graphics/Matrix;

.field d:Landroid/graphics/PointF;

.field e:F

.field f:F

.field g:F

.field h:F

.field i:I

.field j:F

.field k:F

.field l:F

.field m:F

.field n:F

.field o:F

.field p:F

.field q:F

.field r:Z

.field s:F

.field t:F

.field u:Landroid/graphics/PointF;

.field v:F

.field w:F

.field x:F

.field y:F

.field z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 158
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    .line 70
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    .line 72
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c:Landroid/graphics/Matrix;

    .line 74
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->d:Landroid/graphics/PointF;

    .line 92
    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->i:I

    .line 112
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->q:F

    .line 114
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    .line 116
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->s:F

    .line 120
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->t:F

    .line 122
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->u:Landroid/graphics/PointF;

    .line 124
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->D:Landroid/os/Handler;

    .line 128
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->w:F

    .line 130
    const/high16 v0, 0x41c80000    # 25.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->x:F

    .line 132
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->y:F

    .line 788
    new-instance v0, Lcom/sec/chaton/multimedia/image/au;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/au;-><init>(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->G:Ljava/lang/Runnable;

    .line 853
    new-instance v0, Lcom/sec/chaton/multimedia/image/av;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/av;-><init>(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->H:Ljava/lang/Runnable;

    .line 160
    invoke-virtual {p0, v2}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setFocusable(Z)V

    .line 162
    invoke-virtual {p0, v2}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setFocusableInTouchMode(Z)V

    .line 164
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->z:F

    .line 166
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b()V

    .line 168
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/sec/chaton/multimedia/image/aw;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/image/aw;-><init>(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->E:Landroid/view/GestureDetector;

    .line 170
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 174
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    .line 70
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    .line 72
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c:Landroid/graphics/Matrix;

    .line 74
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->d:Landroid/graphics/PointF;

    .line 92
    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->i:I

    .line 112
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->q:F

    .line 114
    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    .line 116
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->s:F

    .line 120
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->t:F

    .line 122
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->u:Landroid/graphics/PointF;

    .line 124
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->D:Landroid/os/Handler;

    .line 128
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->w:F

    .line 130
    const/high16 v0, 0x41c80000    # 25.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->x:F

    .line 132
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->y:F

    .line 788
    new-instance v0, Lcom/sec/chaton/multimedia/image/au;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/au;-><init>(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->G:Ljava/lang/Runnable;

    .line 853
    new-instance v0, Lcom/sec/chaton/multimedia/image/av;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/av;-><init>(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->H:Ljava/lang/Runnable;

    .line 176
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->z:F

    .line 178
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b()V

    .line 180
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/sec/chaton/multimedia/image/aw;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/image/aw;-><init>(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->E:Landroid/view/GestureDetector;

    .line 182
    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->F:I

    .line 184
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)F
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 636
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    .line 638
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v1, v2

    .line 640
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->G:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 646
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    .line 648
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    add-float/2addr v1, v2

    .line 650
    div-float/2addr v0, v3

    div-float/2addr v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 652
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->D:Landroid/os/Handler;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 188
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->a:Landroid/graphics/Paint;

    .line 190
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->H:Ljava/lang/Runnable;

    return-object v0
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 306
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    .line 430
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    const/16 v2, 0x9

    new-array v2, v2, [F

    .line 314
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 316
    aget v3, v2, v0

    iput v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 318
    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->v:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 320
    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->v:F

    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v3, v4

    .line 322
    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    .line 324
    iget v5, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    .line 326
    iget-object v6, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v6, v3, v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 328
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->invalidate()V

    .line 332
    :cond_2
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 334
    aget v3, v2, v0

    iput v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 336
    const/4 v3, 0x2

    aget v3, v2, v3

    iput v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    .line 338
    const/4 v3, 0x5

    aget v2, v2, v3

    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    .line 340
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 342
    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    sub-int/2addr v3, v4

    .line 348
    if-gez v2, :cond_8

    .line 350
    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    cmpl-float v4, v4, v7

    if-lez v4, :cond_7

    .line 352
    iput v7, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->j:F

    move v2, v1

    .line 376
    :goto_1
    if-gez v3, :cond_a

    .line 378
    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    cmpl-float v4, v4, v7

    if-lez v4, :cond_9

    .line 380
    iput v7, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->k:F

    move v0, v1

    .line 404
    :cond_3
    :goto_2
    if-eq v2, v1, :cond_4

    if-ne v0, v1, :cond_0

    .line 406
    :cond_4
    if-nez v0, :cond_5

    .line 408
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->k:F

    .line 412
    :cond_5
    if-nez v2, :cond_6

    .line 414
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->j:F

    .line 420
    :cond_6
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    .line 424
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->G:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 426
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->G:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 358
    :cond_7
    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    int-to-float v5, v2

    cmpg-float v4, v4, v5

    if-gez v4, :cond_b

    .line 360
    int-to-float v2, v2

    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->j:F

    move v2, v1

    .line 362
    goto :goto_1

    .line 370
    :cond_8
    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->j:F

    move v2, v1

    .line 372
    goto :goto_1

    .line 386
    :cond_9
    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    int-to-float v5, v3

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 388
    int-to-float v0, v3

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->k:F

    move v0, v1

    .line 390
    goto :goto_2

    .line 398
    :cond_a
    div-int/lit8 v0, v3, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->k:F

    move v0, v1

    .line 400
    goto :goto_2

    :cond_b
    move v2, v0

    goto :goto_1
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/image/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c()V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 608
    const/16 v1, 0x9

    new-array v1, v1, [F

    .line 610
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 612
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 614
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 616
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getWidth()I

    move-result v4

    .line 618
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getHeight()I

    move-result v5

    .line 620
    int-to-float v2, v2

    aget v6, v1, v0

    mul-float/2addr v2, v6

    float-to-int v2, v2

    .line 622
    int-to-float v3, v3

    const/4 v6, 0x4

    aget v1, v1, v6

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 624
    if-le v2, v4, :cond_0

    if-gt v1, v5, :cond_1

    .line 627
    :cond_0
    const/4 v0, 0x1

    .line 630
    :cond_1
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 296
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 300
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 195
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 199
    iput p1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    .line 201
    iput p2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 207
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 215
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->F:I

    if-nez v2, :cond_2

    .line 217
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    if-le v3, v2, :cond_1

    .line 219
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    int-to-float v2, v2

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 221
    int-to-float v0, v0

    mul-float/2addr v0, v2

    .line 223
    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    float-to-int v0, v0

    sub-int v0, v3, v0

    div-int/lit8 v0, v0, 0x2

    .line 225
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 227
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    int-to-float v4, v0

    invoke-virtual {v3, v5, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 245
    :goto_0
    int-to-float v1, v1

    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    .line 247
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    .line 249
    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 251
    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->v:F

    .line 283
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->invalidate()V

    .line 287
    :cond_0
    return-void

    .line 233
    :cond_1
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float/2addr v2, v0

    .line 235
    int-to-float v0, v3

    mul-float/2addr v0, v2

    .line 237
    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    float-to-int v0, v0

    sub-int v0, v3, v0

    div-int/lit8 v0, v0, 0x2

    .line 239
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 241
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    int-to-float v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_0

    .line 257
    :cond_2
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    if-le v3, v2, :cond_3

    .line 259
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    .line 261
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    int-to-float v3, v0

    invoke-virtual {v2, v5, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v6, v0

    move v0, v1

    move v1, v6

    .line 273
    :goto_2
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    .line 275
    int-to-float v0, v1

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    .line 277
    iput v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 279
    iput v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->v:F

    goto :goto_1

    .line 267
    :cond_3
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    .line 269
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    int-to-float v3, v0

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v11, 0x5

    const/high16 v10, 0x41200000    # 10.0f

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->E:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 600
    :cond_0
    :goto_0
    return v7

    .line 441
    :cond_1
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    if-eq v0, v7, :cond_0

    .line 449
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 451
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_0

    .line 598
    :cond_2
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->invalidate()V

    goto :goto_0

    .line 457
    :pswitch_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->d:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 461
    iput v7, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->i:I

    goto :goto_1

    .line 469
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->t:F

    .line 471
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->t:F

    cmpl-float v0, v0, v10

    if-lez v0, :cond_2

    .line 473
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 475
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->u:Landroid/graphics/PointF;

    invoke-direct {p0, v0, p1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 477
    iput v8, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->i:I

    goto :goto_1

    .line 487
    :pswitch_3
    iput v9, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->i:I

    .line 489
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 491
    aget v1, v0, v8

    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    .line 493
    aget v1, v0, v11

    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    .line 495
    aget v0, v0, v9

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 499
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c()V

    goto :goto_1

    .line 507
    :pswitch_4
    iget v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->i:I

    if-ne v1, v7, :cond_4

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    if-nez v1, :cond_4

    .line 509
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 511
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->d:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    .line 513
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->d:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    .line 515
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 517
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 522
    :cond_3
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 524
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 526
    aget v1, v0, v8

    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    .line 528
    aget v1, v0, v11

    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    .line 530
    aget v0, v0, v9

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    goto/16 :goto_1

    .line 534
    :cond_4
    iget v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->i:I

    if-ne v1, v8, :cond_2

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->r:Z

    if-nez v1, :cond_2

    .line 536
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->a(Landroid/view/MotionEvent;)F

    move-result v1

    .line 538
    cmpl-float v2, v1, v10

    if-lez v2, :cond_2

    .line 540
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 542
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->t:F

    div-float/2addr v1, v2

    .line 544
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 546
    aget v2, v0, v9

    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 548
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    mul-float/2addr v2, v1

    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->v:F

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_6

    .line 550
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->v:F

    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v3, v4

    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->v:F

    iget v5, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->u:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->u:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 578
    :cond_5
    :goto_2
    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->h:F

    .line 580
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 582
    aget v1, v0, v8

    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    .line 584
    aget v1, v0, v11

    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    .line 586
    aget v0, v0, v9

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    goto/16 :goto_1

    .line 554
    :cond_6
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    mul-float/2addr v2, v1

    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->w:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_7

    .line 556
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->w:F

    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v3, v4

    iget v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->w:F

    iget v5, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    div-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->u:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->u:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 558
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->a(Landroid/view/MotionEvent;)F

    move-result v2

    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->t:F

    .line 560
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->t:F

    cmpl-float v2, v2, v10

    if-lez v2, :cond_5

    .line 562
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->c:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 564
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->u:Landroid/graphics/PointF;

    invoke-direct {p0, v2, p1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 566
    iput v8, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->i:I

    goto :goto_2

    .line 574
    :cond_7
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->u:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->u:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_2

    .line 451
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 7

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 656
    if-eqz p1, :cond_5

    .line 658
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    .line 660
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    .line 662
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    .line 664
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 666
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 674
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 676
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->F:I

    if-nez v2, :cond_1

    .line 678
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    if-le v0, v2, :cond_0

    .line 680
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float/2addr v2, v0

    .line 682
    int-to-float v0, v3

    mul-float/2addr v0, v2

    .line 684
    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    float-to-int v0, v0

    sub-int v0, v3, v0

    div-int/lit8 v0, v0, 0x2

    .line 686
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 688
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    int-to-float v4, v0

    invoke-virtual {v3, v5, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 706
    :goto_0
    int-to-float v1, v1

    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    .line 708
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    .line 710
    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 712
    iput v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->v:F

    .line 768
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->invalidate()V

    .line 780
    :goto_2
    return-void

    .line 694
    :cond_0
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    int-to-float v2, v2

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 696
    int-to-float v0, v0

    mul-float/2addr v0, v2

    .line 698
    iget v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    float-to-int v0, v0

    sub-int v0, v3, v0

    div-int/lit8 v0, v0, 0x2

    .line 700
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 702
    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    int-to-float v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_0

    .line 718
    :cond_1
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    if-le v0, v2, :cond_3

    .line 722
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    if-le v3, v0, :cond_2

    move v0, v1

    .line 734
    :goto_3
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    int-to-float v3, v0

    invoke-virtual {v2, v5, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 758
    :goto_4
    int-to-float v1, v1

    iput v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->f:F

    .line 760
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->g:F

    .line 762
    iput v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->e:F

    .line 764
    iput v4, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->v:F

    goto :goto_1

    .line 730
    :cond_2
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    goto :goto_3

    .line 740
    :cond_3
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->B:I

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    .line 742
    iget v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    if-le v3, v2, :cond_4

    .line 754
    :goto_5
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->b:Landroid/graphics/Matrix;

    int-to-float v3, v0

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_4

    .line 750
    :cond_4
    iget v1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->C:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    goto :goto_5

    .line 774
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->A:Landroid/graphics/Bitmap;

    .line 776
    const-string v0, "ZoomableImageView"

    const-string v1, "bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public setDefaultScale(I)V
    .locals 0

    .prologue
    .line 152
    iput p1, p0, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->F:I

    .line 154
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1016
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1018
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1020
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1038
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1040
    if-eqz p1, :cond_0

    .line 1041
    instance-of v0, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    .line 1045
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1047
    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1065
    :cond_0
    :goto_0
    return-void

    .line 1052
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1054
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1056
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {p1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1058
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1060
    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setImageResource(I)V
    .locals 0

    .prologue
    .line 1027
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1029
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/image/ZoomableImageView;->setBackgroundResource(I)V

    .line 1031
    return-void
.end method
