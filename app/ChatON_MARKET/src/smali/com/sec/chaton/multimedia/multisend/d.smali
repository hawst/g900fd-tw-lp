.class Lcom/sec/chaton/multimedia/multisend/d;
.super Ljava/lang/Object;
.source "CategoryView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/multisend/CategoryView;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/multisend/CategoryView;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/16 v5, 0xa

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 90
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/c;

    .line 92
    iget-object v1, v0, Lcom/sec/chaton/multimedia/multisend/c;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, v0, Lcom/sec/chaton/multimedia/multisend/c;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    iget-object v1, v0, Lcom/sec/chaton/multimedia/multisend/c;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 96
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a(Lcom/sec/chaton/multimedia/multisend/CategoryView;)Lcom/sec/chaton/multimedia/multisend/b;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Lcom/sec/chaton/multimedia/multisend/b;->a(Lcom/sec/chaton/multimedia/multisend/c;Z)V

    .line 97
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-static {v0, v3}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a(Lcom/sec/chaton/multimedia/multisend/CategoryView;I)I

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a()V

    .line 112
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->d(Lcom/sec/chaton/multimedia/multisend/CategoryView;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a(Ljava/util/HashMap;)V

    .line 114
    :cond_0
    return-void

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->b(Lcom/sec/chaton/multimedia/multisend/CategoryView;)I

    move-result v1

    if-lt v1, v5, :cond_2

    .line 101
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a(Lcom/sec/chaton/multimedia/multisend/CategoryView;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c(Lcom/sec/chaton/multimedia/multisend/CategoryView;)Landroid/widget/Toast;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0b01d1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c(Lcom/sec/chaton/multimedia/multisend/CategoryView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Toast;->setDuration(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->c(Lcom/sec/chaton/multimedia/multisend/CategoryView;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 106
    :cond_2
    iget-object v1, v0, Lcom/sec/chaton/multimedia/multisend/c;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 107
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a(Lcom/sec/chaton/multimedia/multisend/CategoryView;)Lcom/sec/chaton/multimedia/multisend/b;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lcom/sec/chaton/multimedia/multisend/b;->a(Lcom/sec/chaton/multimedia/multisend/c;Z)V

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-static {v0, v3}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->b(Lcom/sec/chaton/multimedia/multisend/CategoryView;I)I

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/d;->a:Lcom/sec/chaton/multimedia/multisend/CategoryView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/CategoryView;->a()V

    goto :goto_0
.end method
