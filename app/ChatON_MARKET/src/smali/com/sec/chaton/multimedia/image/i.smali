.class final enum Lcom/sec/chaton/multimedia/image/i;
.super Ljava/lang/Enum;
.source "ImageEffectFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/multimedia/image/i;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum b:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum c:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum d:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum e:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum f:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum g:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum h:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum i:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum j:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum k:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum l:Lcom/sec/chaton/multimedia/image/i;

.field public static final enum m:Lcom/sec/chaton/multimedia/image/i;

.field private static final synthetic o:[Lcom/sec/chaton/multimedia/image/i;


# instance fields
.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 72
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "ORIGINAL"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->a:Lcom/sec/chaton/multimedia/image/i;

    .line 73
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "BLUE_WASH"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->b:Lcom/sec/chaton/multimedia/image/i;

    .line 74
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "VINTAGE"

    invoke-direct {v0, v1, v6, v6}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->c:Lcom/sec/chaton/multimedia/image/i;

    .line 75
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "SOFT_GLOW"

    invoke-direct {v0, v1, v7, v7}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->d:Lcom/sec/chaton/multimedia/image/i;

    .line 76
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "VIVID"

    invoke-direct {v0, v1, v8, v8}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->e:Lcom/sec/chaton/multimedia/image/i;

    .line 77
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "VIGNETTE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->f:Lcom/sec/chaton/multimedia/image/i;

    .line 78
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "FADED_COLOR"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->g:Lcom/sec/chaton/multimedia/image/i;

    .line 79
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "BRIGHT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->h:Lcom/sec/chaton/multimedia/image/i;

    .line 80
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "RETRO"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->i:Lcom/sec/chaton/multimedia/image/i;

    .line 81
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "CLASSIC"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->j:Lcom/sec/chaton/multimedia/image/i;

    .line 82
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "SEPIA"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->k:Lcom/sec/chaton/multimedia/image/i;

    .line 83
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "GRAY"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->l:Lcom/sec/chaton/multimedia/image/i;

    .line 84
    new-instance v0, Lcom/sec/chaton/multimedia/image/i;

    const-string v1, "FUSAIN"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/image/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->m:Lcom/sec/chaton/multimedia/image/i;

    .line 71
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/sec/chaton/multimedia/image/i;

    sget-object v1, Lcom/sec/chaton/multimedia/image/i;->a:Lcom/sec/chaton/multimedia/image/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/multimedia/image/i;->b:Lcom/sec/chaton/multimedia/image/i;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/multimedia/image/i;->c:Lcom/sec/chaton/multimedia/image/i;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/multimedia/image/i;->d:Lcom/sec/chaton/multimedia/image/i;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/multimedia/image/i;->e:Lcom/sec/chaton/multimedia/image/i;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/multimedia/image/i;->f:Lcom/sec/chaton/multimedia/image/i;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/multimedia/image/i;->g:Lcom/sec/chaton/multimedia/image/i;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/multimedia/image/i;->h:Lcom/sec/chaton/multimedia/image/i;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/multimedia/image/i;->i:Lcom/sec/chaton/multimedia/image/i;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/chaton/multimedia/image/i;->j:Lcom/sec/chaton/multimedia/image/i;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/chaton/multimedia/image/i;->k:Lcom/sec/chaton/multimedia/image/i;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/chaton/multimedia/image/i;->l:Lcom/sec/chaton/multimedia/image/i;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/chaton/multimedia/image/i;->m:Lcom/sec/chaton/multimedia/image/i;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/multimedia/image/i;->o:[Lcom/sec/chaton/multimedia/image/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 89
    iput p3, p0, Lcom/sec/chaton/multimedia/image/i;->n:I

    .line 90
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/i;)I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/chaton/multimedia/image/i;->n:I

    return v0
.end method

.method static a(I)Lcom/sec/chaton/multimedia/image/i;
    .locals 1

    .prologue
    .line 93
    packed-switch p0, :pswitch_data_0

    .line 122
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->a:Lcom/sec/chaton/multimedia/image/i;

    :goto_0
    return-object v0

    .line 95
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->a:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 97
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->b:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 99
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->c:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 101
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->d:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 103
    :pswitch_4
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->e:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 105
    :pswitch_5
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->f:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 107
    :pswitch_6
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->g:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 109
    :pswitch_7
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->h:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 111
    :pswitch_8
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->i:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 113
    :pswitch_9
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->j:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 115
    :pswitch_a
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->k:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 117
    :pswitch_b
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->l:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 119
    :pswitch_c
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->m:Lcom/sec/chaton/multimedia/image/i;

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/multimedia/image/i;
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/sec/chaton/multimedia/image/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/i;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/multimedia/image/i;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/sec/chaton/multimedia/image/i;->o:[Lcom/sec/chaton/multimedia/image/i;

    invoke-virtual {v0}, [Lcom/sec/chaton/multimedia/image/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/multimedia/image/i;

    return-object v0
.end method
