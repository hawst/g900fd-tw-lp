.class public Lcom/sec/chaton/multimedia/emoticon/ams/g;
.super Landroid/widget/BaseAdapter;
.source "AmsTemplateListAdapter.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Lcom/sec/common/f/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/common/f/c;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/g;->a:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/sec/chaton/multimedia/emoticon/ams/g;->c:Lcom/sec/common/f/c;

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/g;->b:I

    .line 36
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 40
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/q;->e()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/q;->c()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/q;->d()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 46
    if-nez p1, :cond_0

    .line 47
    const-string v0, ""

    .line 68
    :goto_0
    return-object v0

    .line 50
    :cond_0
    add-int/lit8 v0, p1, -0x1

    .line 52
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/q;->e()I

    move-result v1

    .line 53
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/q;->c()I

    move-result v2

    .line 55
    if-ge v0, v1, :cond_1

    .line 56
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/q;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 59
    :cond_1
    sub-int/2addr v0, v1

    .line 61
    if-ge v0, v2, :cond_2

    .line 62
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/ams/g;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 65
    :cond_2
    sub-int/2addr v0, v2

    .line 68
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/q;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 5

    .prologue
    .line 73
    if-nez p1, :cond_0

    .line 74
    const-wide/16 v0, 0x0

    .line 95
    :goto_0
    return-wide v0

    .line 77
    :cond_0
    add-int/lit8 v0, p1, -0x1

    .line 79
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/q;->e()I

    move-result v1

    .line 80
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/q;->c()I

    move-result v2

    .line 82
    if-ge v0, v1, :cond_1

    .line 83
    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/ams/d;->c:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a()J

    move-result-wide v1

    int-to-long v3, v0

    add-long v0, v1, v3

    goto :goto_0

    .line 86
    :cond_1
    sub-int/2addr v0, v1

    .line 88
    if-ge v0, v2, :cond_2

    .line 89
    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/ams/d;->b:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a()J

    move-result-wide v1

    int-to-long v3, v0

    add-long v0, v1, v3

    goto :goto_0

    .line 92
    :cond_2
    sub-int/2addr v0, v2

    .line 95
    sget-object v1, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a()J

    move-result-wide v1

    int-to-long v3, v0

    add-long v0, v1, v3

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const v3, 0x7f020079

    .line 100
    move-object v0, p2

    check-cast v0, Landroid/widget/ImageView;

    .line 102
    if-nez p2, :cond_1

    .line 103
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    iget v2, p0, Lcom/sec/chaton/multimedia/emoticon/ams/g;->b:I

    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 105
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/ams/g;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 106
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 112
    :goto_0
    if-nez p1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/g;->c:Lcom/sec/common/f/c;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 117
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 120
    const v0, 0x7f0203d6

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 121
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    move-object v0, v1

    .line 136
    :goto_1
    return-object v0

    .line 127
    :cond_0
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 130
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 132
    new-instance v2, Lcom/sec/chaton/multimedia/emoticon/ams/e;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/emoticon/ams/g;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Lcom/sec/chaton/multimedia/emoticon/ams/e;-><init>(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/g;->c:Lcom/sec/common/f/c;

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    move-object v0, v1

    .line 136
    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
