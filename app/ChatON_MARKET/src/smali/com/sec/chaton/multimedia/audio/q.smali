.class public Lcom/sec/chaton/multimedia/audio/q;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "VoiceListAdapter.java"


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field a:Landroid/view/View$OnClickListener;

.field private c:Landroid/content/Context;

.field private d:Landroid/view/LayoutInflater;

.field private e:I

.field private f:Landroid/view/View;

.field private g:Ljava/lang/String;

.field private h:Lcom/sec/chaton/multimedia/audio/w;

.field private i:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/multimedia/audio/q;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .locals 7

    .prologue
    .line 47
    sget-object v4, Lcom/sec/chaton/multimedia/audio/q;->b:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 135
    new-instance v0, Lcom/sec/chaton/multimedia/audio/r;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/audio/r;-><init>(Lcom/sec/chaton/multimedia/audio/q;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->a:Landroid/view/View$OnClickListener;

    .line 177
    new-instance v0, Lcom/sec/chaton/multimedia/audio/s;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/audio/s;-><init>(Lcom/sec/chaton/multimedia/audio/q;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->i:Landroid/os/Handler;

    .line 48
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/q;->c:Landroid/content/Context;

    .line 49
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->d:Landroid/view/LayoutInflater;

    .line 50
    iput p2, p0, Lcom/sec/chaton/multimedia/audio/q;->e:I

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/q;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/q;->f:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/q;)Lcom/sec/chaton/multimedia/audio/w;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->h:Lcom/sec/chaton/multimedia/audio/w;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/q;Lcom/sec/chaton/multimedia/audio/w;)Lcom/sec/chaton/multimedia/audio/w;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/q;->h:Lcom/sec/chaton/multimedia/audio/w;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/q;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/q;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/audio/q;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->i:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/audio/q;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/audio/q;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->f:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->h:Lcom/sec/chaton/multimedia/audio/w;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->h:Lcom/sec/chaton/multimedia/audio/w;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/w;->b()V

    .line 174
    :cond_0
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x3e8

    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/t;

    .line 57
    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/t;->b:Landroid/widget/TextView;

    const-string v2, "_display_name"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    const-string v1, "date_modified"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 66
    const-string v3, "duration"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 68
    new-instance v5, Ljava/util/Date;

    mul-long/2addr v1, v6

    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 70
    const-wide/32 v1, 0xea60

    div-long v1, v3, v1

    .line 71
    div-long/2addr v3, v6

    const-wide/16 v6, 0x3c

    rem-long/2addr v3, v6

    .line 73
    const-string v6, "%02d:%02d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v7, v8

    const/4 v1, 0x1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v1

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 74
    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/t;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/t;->d:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/q;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/q;->mCursor:Landroid/database/Cursor;

    const-string v3, "_data"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 79
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/q;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80
    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/t;->a:Landroid/widget/ImageView;

    const v3, 0x7f020124

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 85
    :goto_0
    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/t;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 86
    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/t;->a:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/q;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 89
    return-void

    .line 82
    :cond_0
    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/t;->a:Landroid/widget/ImageView;

    const v3, 0x7f020125

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/q;->d:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/multimedia/audio/q;->e:I

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 95
    new-instance v2, Lcom/sec/chaton/multimedia/audio/t;

    invoke-direct {v2}, Lcom/sec/chaton/multimedia/audio/t;-><init>()V

    .line 97
    const v0, 0x7f07014b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sec/chaton/multimedia/audio/t;->a:Landroid/widget/ImageView;

    .line 98
    const v0, 0x7f07014c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/multimedia/audio/t;->b:Landroid/widget/TextView;

    .line 99
    iget-object v0, v2, Lcom/sec/chaton/multimedia/audio/t;->b:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 100
    const v0, 0x7f07014d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sec/chaton/multimedia/audio/t;->d:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0704b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 103
    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 104
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v4, v3, v4, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 106
    new-instance v3, Lcom/sec/chaton/widget/AdaptableTextView;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/audio/q;->c:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/chaton/widget/AdaptableTextView;-><init>(Landroid/content/Context;)V

    .line 107
    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 108
    iget-object v4, p0, Lcom/sec/chaton/multimedia/audio/q;->c:Landroid/content/Context;

    const v5, 0x7f0c0060

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 109
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 112
    iput-object v3, v2, Lcom/sec/chaton/multimedia/audio/t;->c:Landroid/widget/TextView;

    .line 114
    iget-object v0, v2, Lcom/sec/chaton/multimedia/audio/t;->a:Landroid/widget/ImageView;

    const v3, 0x7f02011a

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 115
    iget-object v0, v2, Lcom/sec/chaton/multimedia/audio/t;->a:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 117
    iget-object v0, v2, Lcom/sec/chaton/multimedia/audio/t;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 118
    const/high16 v3, 0x42140000    # 37.0f

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 119
    const/high16 v3, 0x42180000    # 38.0f

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 120
    const/high16 v3, 0x40c00000    # 6.0f

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 121
    iget-object v3, v2, Lcom/sec/chaton/multimedia/audio/t;->a:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 123
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 124
    return-object v1
.end method
