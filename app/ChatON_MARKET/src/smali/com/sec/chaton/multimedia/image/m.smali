.class Lcom/sec/chaton/multimedia/image/m;
.super Ljava/lang/Object;
.source "ImagePagerActivity.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 391
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 383
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 16

    .prologue
    .line 294
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_3

    .line 295
    if-eqz p3, :cond_2

    .line 297
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;I)I

    .line 299
    const/4 v13, 0x0

    .line 300
    const/4 v1, 0x0

    .line 302
    :goto_0
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 303
    const-string v2, "message_sender"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 304
    const-string v2, "message_sever_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 305
    const-string v2, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 306
    const-string v2, "message_content"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 307
    const-string v2, "message_download_uri"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 308
    const-string v2, "message_content_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v10

    .line 309
    const-string v2, "message_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 311
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->h(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)J

    move-result-wide v14

    cmp-long v2, v14, v4

    if-nez v2, :cond_6

    move v12, v13

    .line 319
    :goto_1
    new-instance v1, Lcom/sec/chaton/multimedia/image/t;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-direct/range {v1 .. v11}, Lcom/sec/chaton/multimedia/image/t;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;I)V

    .line 321
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 322
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add item : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    add-int/lit8 v13, v13, 0x1

    move v1, v12

    .line 330
    goto/16 :goto_0

    .line 331
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->i(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2, v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;I)I

    .line 333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2, v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 379
    :cond_2
    :goto_2
    return-void

    .line 335
    :catchall_0
    move-exception v1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    throw v1

    .line 338
    :cond_3
    const/4 v1, 0x2

    move/from16 v0, p1

    if-ne v0, v1, :cond_2

    .line 339
    if-eqz p3, :cond_2

    .line 341
    :try_start_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)I

    move-result v2

    if-le v1, v2, :cond_5

    .line 342
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;I)I

    .line 344
    const/4 v1, 0x0

    move v12, v1

    .line 346
    :goto_3
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 347
    const-string v1, "message_sender"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 348
    const-string v1, "message_sever_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 349
    const-string v1, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 350
    const-string v1, "message_content"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 351
    const-string v1, "message_download_uri"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 353
    const-string v1, "message_content_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v10

    .line 355
    const-string v1, "message_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 361
    new-instance v1, Lcom/sec/chaton/multimedia/image/t;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-direct/range {v1 .. v11}, Lcom/sec/chaton/multimedia/image/t;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Lcom/sec/chaton/e/w;I)V

    .line 363
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_4

    .line 364
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add item : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/m;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 370
    add-int/lit8 v1, v12, 0x1

    move v12, v1

    .line 371
    goto/16 :goto_3

    .line 375
    :catchall_1
    move-exception v1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_5
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :cond_6
    move v12, v1

    goto/16 :goto_1
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 387
    return-void
.end method
