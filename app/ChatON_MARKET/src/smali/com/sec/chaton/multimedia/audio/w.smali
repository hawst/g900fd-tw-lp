.class public Lcom/sec/chaton/multimedia/audio/w;
.super Ljava/lang/Object;
.source "VoiceListPlayer.java"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Landroid/media/MediaPlayer$OnCompletionListener;

.field private c:Landroid/media/MediaPlayer;

.field private d:Landroid/media/AudioManager;

.field private e:Landroid/os/Handler;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Landroid/media/AudioManager$OnAudioFocusChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/chaton/multimedia/audio/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/audio/w;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    new-instance v0, Lcom/sec/chaton/multimedia/audio/x;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/audio/x;-><init>(Lcom/sec/chaton/multimedia/audio/w;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->a:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 127
    new-instance v0, Lcom/sec/chaton/multimedia/audio/y;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/audio/y;-><init>(Lcom/sec/chaton/multimedia/audio/w;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->h:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 34
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/w;->e:Landroid/os/Handler;

    .line 35
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->d:Landroid/media/AudioManager;

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/w;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/w;Z)Z
    .locals 0

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/sec/chaton/multimedia/audio/w;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/audio/w;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/chaton/multimedia/audio/w;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/audio/w;)Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/w;->g:Z

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/audio/w;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 110
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 111
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v1, "ChatON"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 113
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 114
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->e:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->e:Landroid/os/Handler;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/w;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :cond_1
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    sget-object v1, Lcom/sec/chaton/multimedia/audio/w;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const v5, 0x7f0b03ce

    const/4 v4, 0x0

    .line 39
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    .line 43
    :cond_0
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/w;->f:Ljava/lang/String;

    .line 46
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 47
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/w;->d()V

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->d:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/w;->h:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 50
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 51
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 53
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/w;->a:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->e:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->e:Landroid/os/Handler;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/w;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 73
    :cond_1
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    sget-object v1, Lcom/sec/chaton/multimedia/audio/w;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 63
    :catch_1
    move-exception v0

    .line 64
    sget-object v1, Lcom/sec/chaton/multimedia/audio/w;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 66
    :catch_2
    move-exception v0

    .line 67
    sget-object v1, Lcom/sec/chaton/multimedia/audio/w;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 68
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 69
    :catch_3
    move-exception v0

    .line 70
    sget-object v1, Lcom/sec/chaton/multimedia/audio/w;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 71
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->c:Landroid/media/MediaPlayer;

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->d:Landroid/media/AudioManager;

    if-eqz v0, :cond_2

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/w;->d:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/w;->h:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :cond_2
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    sget-object v1, Lcom/sec/chaton/multimedia/audio/w;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
