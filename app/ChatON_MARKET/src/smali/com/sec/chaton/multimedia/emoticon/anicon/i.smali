.class public Lcom/sec/chaton/multimedia/emoticon/anicon/i;
.super Lcom/sec/common/f/a;
.source "AniconPanelDispatcherTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field static a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/d/j;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/chaton/multimedia/emoticon/anicon/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/d/j;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 49
    iput-object p2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->c:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->b:Lcom/sec/chaton/d/j;

    .line 51
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 150
    invoke-super {p0}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 121
    .line 123
    if-eqz p1, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->e()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 130
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->a()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 131
    return-void

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->a()Landroid/widget/ImageView;

    move-result-object v0

    .line 57
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 58
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 62
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 64
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->b:Lcom/sec/chaton/d/j;

    sget-object v3, Lcom/sec/chaton/d/a/b;->b:Lcom/sec/chaton/d/a/b;

    const/16 v4, 0xf0

    invoke-virtual {v1, v3, v0, v4}, Lcom/sec/chaton/d/j;->a(Lcom/sec/chaton/d/a/b;Ljava/lang/String;I)Lcom/sec/chaton/d/a/ep;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/d/a/ep;->b()Lcom/sec/chaton/a/a/f;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 78
    :goto_0
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/sec/chaton/a/a/f;->b()Lcom/sec/chaton/j/o;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/j/o;->a:Lcom/sec/chaton/j/o;

    if-ne v3, v4, :cond_5

    .line 79
    invoke-virtual {v1}, Lcom/sec/chaton/a/a/f;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/io/entry/ViewPackageEntry;

    .line 81
    if-eqz v1, :cond_4

    .line 82
    iget-object v1, v1, Lcom/sec/chaton/io/entry/ViewPackageEntry;->_package:Lcom/sec/chaton/io/entry/inner/Package;

    iget-object v1, v1, Lcom/sec/chaton/io/entry/inner/Package;->panelurl:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->c:Ljava/lang/String;

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->k()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v3

    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v4

    invoke-static {v1, v0, v3, v4}, Lcom/sec/chaton/settings/downloads/u;->d(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    .line 94
    if-nez v1, :cond_6

    .line 96
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->k()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/util/a/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 97
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->c:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 105
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->c:Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Lcom/sec/common/util/a/a;->a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 108
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->k()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v3}, Lcom/sec/chaton/settings/downloads/u;->b(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 110
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->k()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v3

    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v4

    invoke-static {v1, v0, v3, v4}, Lcom/sec/chaton/settings/downloads/u;->d(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/drawable/BitmapDrawable;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 116
    :goto_1
    return-object v0

    .line 68
    :catch_0
    move-exception v1

    .line 69
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_2

    .line 70
    sget-object v3, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_2
    move-object v1, v2

    .line 76
    goto :goto_0

    .line 72
    :catch_1
    move-exception v1

    .line 73
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_3

    .line 74
    sget-object v3, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_3
    move-object v1, v2

    goto/16 :goto_0

    :cond_4
    move-object v0, v2

    .line 84
    goto :goto_1

    :cond_5
    move-object v0, v2

    .line 88
    goto :goto_1

    .line 111
    :catch_2
    move-exception v0

    move-object v0, v2

    .line 112
    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public d()V
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->e()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 137
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->a(Landroid/view/View;)V

    .line 139
    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 142
    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 146
    :cond_0
    return-void
.end method

.method public e()Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    .prologue
    .line 155
    invoke-super {p0}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method public synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->e()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Landroid/view/View;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;->a()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method
