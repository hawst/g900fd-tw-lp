.class public Lcom/sec/chaton/multimedia/vcard/l;
.super Ljava/lang/Object;
.source "VCardParser.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/sec/chaton/multimedia/vcard/b;

.field c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/sec/chaton/multimedia/vcard/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/vcard/l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/vcard/l;->c:Z

    .line 15
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/b;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/vcard/b;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/l;->b:Lcom/sec/chaton/multimedia/vcard/b;

    .line 16
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/sec/chaton/multimedia/vcard/b;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 20
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 22
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_5

    aget-object v4, v2, v0

    .line 23
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RAW LINE : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 24
    const-string v5, "NOTE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 25
    iget-object v5, p0, Lcom/sec/chaton/multimedia/vcard/l;->b:Lcom/sec/chaton/multimedia/vcard/b;

    invoke-virtual {p0, v4}, Lcom/sec/chaton/multimedia/vcard/l;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    .line 22
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 26
    :cond_1
    const-string v5, "N"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "FN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 27
    :cond_2
    const-string v5, "NICK"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "NOTE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 28
    iget-object v5, p0, Lcom/sec/chaton/multimedia/vcard/l;->b:Lcom/sec/chaton/multimedia/vcard/b;

    invoke-virtual {p0, v4}, Lcom/sec/chaton/multimedia/vcard/l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    goto :goto_1

    .line 30
    :cond_3
    const-string v5, "TEL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 31
    invoke-virtual {p0, v4}, Lcom/sec/chaton/multimedia/vcard/l;->c(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 32
    if-eqz v4, :cond_0

    .line 33
    iget-object v5, p0, Lcom/sec/chaton/multimedia/vcard/l;->b:Lcom/sec/chaton/multimedia/vcard/b;

    aget-object v6, v4, v10

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aget-object v7, v4, v1

    aget-object v4, v4, v9

    iget-boolean v8, p0, Lcom/sec/chaton/multimedia/vcard/l;->c:Z

    invoke-virtual {v5, v6, v7, v4, v8}, Lcom/sec/chaton/multimedia/vcard/b;->a(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 34
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/vcard/l;->c:Z

    goto :goto_1

    .line 36
    :cond_4
    const-string v5, "EMAIL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 37
    invoke-virtual {p0, v4}, Lcom/sec/chaton/multimedia/vcard/l;->d(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 38
    if-eqz v4, :cond_0

    .line 39
    iget-object v5, p0, Lcom/sec/chaton/multimedia/vcard/l;->b:Lcom/sec/chaton/multimedia/vcard/b;

    aget-object v6, v4, v10

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aget-object v7, v4, v1

    aget-object v4, v4, v9

    iget-boolean v8, p0, Lcom/sec/chaton/multimedia/vcard/l;->c:Z

    invoke-virtual {v5, v6, v7, v4, v8}, Lcom/sec/chaton/multimedia/vcard/b;->b(ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 43
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/l;->b:Lcom/sec/chaton/multimedia/vcard/b;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v1, 0x0

    .line 217
    const-string v0, ";"

    const-string v2, ""

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 219
    const-string v0, "="

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 220
    const-string v0, "="

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 221
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 227
    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_8

    .line 228
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x3d

    if-ne v3, v4, :cond_1

    .line 234
    :goto_1
    array-length v8, v5

    move v3, v1

    move v2, v1

    :goto_2
    if-ge v3, v8, :cond_6

    aget-object v9, v5, v3

    .line 236
    add-int/lit8 v4, v2, 0x1

    .line 238
    if-eqz v0, :cond_2

    .line 239
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 234
    :cond_0
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_2

    .line 227
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_2
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v12, :cond_4

    .line 245
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v10, 0x1

    if-le v2, v10, :cond_3

    .line 250
    :try_start_0
    new-instance v2, Ljava/math/BigInteger;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x10

    invoke-direct {v2, v10, v11}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v2

    .line 251
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v2, p2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    .line 259
    :goto_4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 261
    :cond_3
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    :goto_5
    array-length v2, v5

    if-ne v4, v2, :cond_0

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 291
    :try_start_1
    new-instance v2, Ljava/math/BigInteger;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x10

    invoke-direct {v2, v9, v10}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v2

    .line 292
    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v2, p2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 293
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_3

    .line 294
    :catch_0
    move-exception v2

    .line 296
    const-string v2, "NumberFormatException: Invalid BigInteger"

    const-string v9, "VCardParser"

    invoke-static {v2, v9}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 252
    :catch_1
    move-exception v2

    .line 254
    const-string v2, "NumberFormatException: Invalid BigInteger"

    const-string v10, "VCardParser"

    invoke-static {v2, v10}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 255
    :catch_2
    move-exception v2

    .line 256
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_4

    .line 262
    :cond_4
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v12, :cond_5

    .line 263
    const-string v2, ""

    .line 264
    invoke-virtual {v9, v1, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    :try_start_2
    new-instance v2, Ljava/math/BigInteger;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x10

    invoke-direct {v2, v10, v11}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v2

    .line 270
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v2, p2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 271
    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_4

    .line 279
    :goto_6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 280
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v9, v12, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 281
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 272
    :catch_3
    move-exception v2

    .line 274
    const-string v2, "NumberFormatException: Invalid BigInteger"

    const-string v10, "VCardParser"

    invoke-static {v2, v10}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 275
    :catch_4
    move-exception v2

    .line 276
    const-string v2, "Cannot Decode"

    const-string v10, "VCardParser"

    invoke-static {v2, v10}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 283
    :cond_5
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 297
    :catch_5
    move-exception v2

    .line 298
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_3

    .line 302
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Decode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    .line 303
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 307
    :goto_7
    return-object v0

    .line 306
    :cond_7
    invoke-static {v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;)V

    move-object v0, v2

    .line 307
    goto :goto_7

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 47
    const-string v1, ""

    .line 48
    const-string v2, ""

    .line 51
    :try_start_0
    const-string v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    const-string v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v0

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 59
    array-length v4, v3

    move v7, v0

    move-object v0, v1

    move v1, v7

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 60
    const-string v6, "CHARSET"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 61
    const/16 v0, 0x8

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 59
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 53
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 54
    const/4 v0, 0x0

    .line 64
    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {p0, v2, v0}, Lcom/sec/chaton/multimedia/vcard/l;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public c(Ljava/lang/String;)[Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 68
    new-array v4, v3, [Ljava/lang/String;

    .line 70
    const/4 v5, 0x0

    :try_start_0
    const-string v6, ":"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v6, v6, v7

    aput-object v6, v4, v5
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    const-string v5, ":"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v0

    .line 79
    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v5, v0

    :goto_0
    if-ge v5, v7, :cond_14

    aget-object v8, v6, v5

    .line 80
    const-string v9, "HOME"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 82
    aput-object v8, v4, v1

    move v0, v1

    .line 142
    :cond_0
    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v2

    .line 79
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 73
    const/4 v0, 0x0

    .line 144
    :goto_2
    return-object v0

    .line 83
    :cond_1
    const-string v9, "CELL"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 85
    aput-object v8, v4, v1

    move v0, v2

    goto :goto_1

    .line 86
    :cond_2
    const-string v9, "WORK"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 88
    aput-object v8, v4, v1

    move v0, v3

    goto :goto_1

    .line 89
    :cond_3
    const-string v9, "WORK-FAX"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 90
    const/4 v0, 0x4

    .line 91
    aput-object v8, v4, v1

    goto :goto_1

    .line 92
    :cond_4
    const-string v9, "HOME-FAX"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 93
    const/4 v0, 0x5

    .line 94
    aput-object v8, v4, v1

    goto :goto_1

    .line 95
    :cond_5
    const-string v9, "PAGER"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 96
    const/4 v0, 0x6

    .line 97
    aput-object v8, v4, v1

    goto :goto_1

    .line 98
    :cond_6
    const-string v9, "OTHER"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 99
    const/4 v0, 0x7

    .line 100
    aput-object v8, v4, v1

    goto :goto_1

    .line 101
    :cond_7
    const-string v9, "CALLBACK"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 102
    const/16 v0, 0x8

    .line 103
    aput-object v8, v4, v1

    goto :goto_1

    .line 104
    :cond_8
    const-string v9, "CAR"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 105
    const/16 v0, 0x9

    .line 106
    aput-object v8, v4, v1

    goto :goto_1

    .line 107
    :cond_9
    const-string v9, "COMPANY_MAIN"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 108
    const/16 v0, 0xa

    .line 109
    aput-object v8, v4, v1

    goto :goto_1

    .line 110
    :cond_a
    const-string v9, "ISDN"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 111
    const/16 v0, 0xb

    .line 112
    aput-object v8, v4, v1

    goto/16 :goto_1

    .line 113
    :cond_b
    const-string v9, "MAIN"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 114
    const/16 v0, 0xc

    .line 115
    aput-object v8, v4, v1

    goto/16 :goto_1

    .line 116
    :cond_c
    const-string v9, "OTHER_FAX"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 117
    const/16 v0, 0xd

    .line 118
    aput-object v8, v4, v1

    goto/16 :goto_1

    .line 119
    :cond_d
    const-string v9, "RADIO"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 120
    const/16 v0, 0xe

    .line 121
    aput-object v8, v4, v1

    goto/16 :goto_1

    .line 122
    :cond_e
    const-string v9, "TELEX"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 123
    const/16 v0, 0xf

    .line 124
    aput-object v8, v4, v1

    goto/16 :goto_1

    .line 125
    :cond_f
    const-string v9, "TTY_TDD"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 126
    const/16 v0, 0x10

    .line 127
    aput-object v8, v4, v1

    goto/16 :goto_1

    .line 128
    :cond_10
    const-string v9, "WORK_MOBILE"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 129
    const/16 v0, 0x11

    .line 130
    aput-object v8, v4, v1

    goto/16 :goto_1

    .line 131
    :cond_11
    const-string v9, "WORK_PAGER"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 132
    const/16 v0, 0x12

    .line 133
    aput-object v8, v4, v1

    goto/16 :goto_1

    .line 134
    :cond_12
    const-string v9, "ASSISTANT"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 135
    const/16 v0, 0x13

    .line 136
    aput-object v8, v4, v1

    goto/16 :goto_1

    .line 137
    :cond_13
    const-string v9, "MMS"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 138
    const/16 v0, 0x14

    .line 139
    aput-object v8, v4, v1

    goto/16 :goto_1

    :cond_14
    move-object v0, v4

    .line 144
    goto/16 :goto_2
.end method

.method public d(Ljava/lang/String;)[Ljava/lang/String;
    .locals 10

    .prologue
    .line 148
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    .line 150
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 152
    array-length v1, v0

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 153
    const/4 v0, 0x0

    .line 192
    :goto_0
    return-object v0

    .line 156
    :cond_0
    const/4 v1, 0x1

    aget-object v6, v0, v1

    .line 159
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 160
    const-string v3, ""

    .line 161
    const-string v2, "OTHER"

    .line 162
    const/4 v1, 0x0

    .line 163
    array-length v8, v7

    const/4 v0, 0x0

    move v5, v0

    move v0, v1

    move-object v1, v2

    :goto_1
    if-ge v5, v8, :cond_6

    aget-object v2, v7, v5

    .line 165
    const-string v9, "CHARSET"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 166
    const/16 v3, 0x8

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v2, v3, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 163
    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v3, v2

    goto :goto_1

    .line 167
    :cond_1
    const-string v9, "HOME"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 168
    const/4 v0, 0x1

    move-object v1, v2

    move-object v2, v3

    .line 169
    goto :goto_2

    .line 170
    :cond_2
    const-string v9, "WORK"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 171
    const/4 v0, 0x2

    move-object v1, v2

    move-object v2, v3

    .line 172
    goto :goto_2

    .line 173
    :cond_3
    const-string v9, "OTHER"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 174
    const/4 v0, 0x3

    move-object v1, v2

    move-object v2, v3

    .line 175
    goto :goto_2

    .line 176
    :cond_4
    const-string v9, "MOBILE"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 177
    const/4 v0, 0x4

    move-object v1, v2

    move-object v2, v3

    .line 178
    goto :goto_2

    .line 181
    :cond_5
    const-string v9, "X-"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 182
    const/4 v0, 0x3

    .line 183
    const/4 v1, 0x0

    move-object v2, v3

    goto :goto_2

    .line 188
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {p0, v6, v3}, Lcom/sec/chaton/multimedia/vcard/l;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    .line 189
    const/4 v2, 0x1

    aput-object v1, v4, v2

    .line 190
    const/4 v1, 0x2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    move-object v0, v4

    .line 192
    goto/16 :goto_0

    :cond_7
    move-object v2, v3

    goto :goto_2
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 196
    const-string v1, ""

    .line 200
    :try_start_0
    const-string v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    const-string v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v0

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v7, v0

    move-object v0, v1

    move v1, v7

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 207
    const-string v6, "CHARSET"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 208
    const/16 v0, 0x8

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 206
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 203
    const/4 v0, 0x0

    .line 212
    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {p0, v2, v0}, Lcom/sec/chaton/multimedia/vcard/l;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
