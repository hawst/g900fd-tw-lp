.class public Lcom/sec/chaton/multimedia/multisend/PreviewData;
.super Ljava/lang/Object;
.source "PreviewData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCategoryId:Ljava/lang/String;

.field private mCropFilePath:Ljava/lang/String;

.field private mFileName:Ljava/lang/String;

.field private mImageData:Ljava/lang/String;

.field private mImageId:Ljava/lang/String;

.field private mOriInfo:Ljava/lang/String;

.field private mUploadSuccessUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/l;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/multisend/l;-><init>()V

    sput-object v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mImageData:Ljava/lang/String;

    .line 26
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mImageId:Ljava/lang/String;

    .line 27
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mCategoryId:Ljava/lang/String;

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mCropFilePath:Ljava/lang/String;

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mOriInfo:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mImageData:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mImageId:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mCategoryId:Ljava/lang/String;

    .line 19
    iput-object p4, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mCropFilePath:Ljava/lang/String;

    .line 20
    iput-object p5, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mOriInfo:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mImageData:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mCropFilePath:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mImageId:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mUploadSuccessUrl:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mFileName:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mCropFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mOriInfo:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mUploadSuccessUrl:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mImageData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mImageId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mCategoryId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mCropFilePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/PreviewData;->mOriInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    return-void
.end method
