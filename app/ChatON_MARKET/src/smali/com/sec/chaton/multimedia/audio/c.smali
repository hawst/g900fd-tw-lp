.class Lcom/sec/chaton/multimedia/audio/c;
.super Ljava/lang/Object;
.source "MediaPlayerManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/database/Cursor;

.field final synthetic b:I

.field final synthetic c:Landroid/os/Handler;

.field final synthetic d:Lcom/sec/chaton/e/r;

.field final synthetic e:Lcom/sec/chaton/multimedia/audio/b;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/audio/b;Landroid/database/Cursor;ILandroid/os/Handler;Lcom/sec/chaton/e/r;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/c;->e:Lcom/sec/chaton/multimedia/audio/b;

    iput-object p2, p0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    iput p3, p0, Lcom/sec/chaton/multimedia/audio/c;->b:I

    iput-object p4, p0, Lcom/sec/chaton/multimedia/audio/c;->c:Landroid/os/Handler;

    iput-object p5, p0, Lcom/sec/chaton/multimedia/audio/c;->d:Lcom/sec/chaton/e/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 19

    .prologue
    .line 83
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 84
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/chaton/multimedia/audio/c;->b:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 85
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    const-string v3, "message_content_type"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/e/w;->a(I)Lcom/sec/chaton/e/w;

    move-result-object v1

    .line 87
    sget-object v2, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_2

    .line 89
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    const-string v3, "message_sever_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 90
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    const-string v3, "message_download_uri"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 91
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    const-string v6, "message_sender"

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 93
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    const-string v6, "message_content"

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 94
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/c;->a:Landroid/database/Cursor;

    const-string v6, "message_inbox_no"

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 96
    const/4 v6, 0x0

    .line 98
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "chaton_id"

    const-string v7, ""

    invoke-virtual {v1, v2, v7}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    const/4 v6, 0x1

    .line 101
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/audio/c;->e:Lcom/sec/chaton/multimedia/audio/b;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/audio/c;->e:Lcom/sec/chaton/multimedia/audio/b;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/audio/b;->a(Lcom/sec/chaton/multimedia/audio/b;)Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual/range {v1 .. v6}, Lcom/sec/chaton/multimedia/audio/b;->a(Landroid/view/View;Ljava/lang/String;JZ)V

    .line 103
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Lcom/sec/chaton/j/c/a;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 105
    invoke-static {}, Lcom/sec/chaton/j/c/a;->a()Lcom/sec/chaton/j/c/a;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/chaton/multimedia/audio/c;->c:Landroid/os/Handler;

    const/4 v10, 0x0

    const/4 v12, 0x0

    sget-object v16, Lcom/sec/chaton/e/w;->f:Lcom/sec/chaton/e/w;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/chaton/multimedia/audio/c;->d:Lcom/sec/chaton/e/r;

    move-object/from16 v17, v0

    move-wide v14, v4

    invoke-virtual/range {v6 .. v18}, Lcom/sec/chaton/j/c/a;->a(Landroid/view/View;Lcom/sec/chaton/chat/ChatFragment;Landroid/os/Handler;ILjava/lang/String;ZLjava/lang/String;JLcom/sec/chaton/e/w;Lcom/sec/chaton/e/r;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 115
    :cond_2
    return-void
.end method
