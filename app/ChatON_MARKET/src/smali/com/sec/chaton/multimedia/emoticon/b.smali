.class public abstract Lcom/sec/chaton/multimedia/emoticon/b;
.super Landroid/widget/BaseAdapter;
.source "AbstractMruAdapter.java"


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lcom/sec/common/f/b;

.field private c:[Ljava/lang/String;

.field private d:Lcom/sec/chaton/multimedia/emoticon/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/common/f/b;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/b;->a:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/sec/chaton/multimedia/emoticon/b;->b:Lcom/sec/common/f/b;

    .line 33
    invoke-virtual {p2}, Lcom/sec/common/f/b;->a()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/b;->c:[Ljava/lang/String;

    .line 36
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/multimedia/emoticon/d;-><init>(Lcom/sec/chaton/multimedia/emoticon/b;Lcom/sec/chaton/multimedia/emoticon/c;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/b;->d:Lcom/sec/chaton/multimedia/emoticon/d;

    .line 37
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/b;->b:Lcom/sec/common/f/b;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/b;->d:Lcom/sec/chaton/multimedia/emoticon/d;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/b;->registerObserver(Ljava/lang/Object;)V

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/emoticon/b;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/b;->c:[Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Landroid/view/View;
.end method

.method public abstract a(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/b;->c:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/b;->c:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 52
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 57
    .line 58
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/b;->c:[Ljava/lang/String;

    aget-object v0, v0, p1

    .line 61
    if-nez p2, :cond_0

    .line 62
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/b;->a:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/emoticon/b;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object p2

    .line 66
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/b;->a:Landroid/content/Context;

    invoke-virtual {p0, v1, v0, p2}, Lcom/sec/chaton/multimedia/emoticon/b;->a(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 68
    return-object p2
.end method
