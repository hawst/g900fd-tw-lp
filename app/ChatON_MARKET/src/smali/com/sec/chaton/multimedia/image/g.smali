.class public Lcom/sec/chaton/multimedia/image/g;
.super Landroid/os/AsyncTask;
.source "ImageEffectFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

.field private b:Landroid/view/View;

.field private c:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/g;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 392
    iput-object p2, p0, Lcom/sec/chaton/multimedia/image/g;->b:Landroid/view/View;

    .line 393
    iput-object p3, p0, Lcom/sec/chaton/multimedia/image/g;->c:Landroid/graphics/Bitmap;

    .line 394
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/g;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/g;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/i;->a(I)Lcom/sec/chaton/multimedia/image/i;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/g;->c:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Landroid/graphics/Bitmap;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 387
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/image/g;->a([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 420
    :try_start_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/g;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->c(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->hide()V

    .line 421
    instance-of v1, p1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 422
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/g;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    move-object v0, p1

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    invoke-static {v2, v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 423
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/g;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->e(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/widget/ImageView;

    move-result-object v1

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 434
    :goto_0
    return-void

    .line 424
    :cond_0
    instance-of v1, p1, Ljava/lang/OutOfMemoryError;

    if-eqz v1, :cond_1

    .line 425
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/g;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->f(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 429
    :catch_0
    move-exception v1

    .line 430
    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 427
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/g;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0b016c

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 431
    :catch_1
    move-exception v1

    .line 432
    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 404
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/g;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->c(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/g;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)V

    .line 407
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/g;->b:Landroid/view/View;

    const v1, 0x7f070328

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/g;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 408
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/g;->b:Landroid/view/View;

    const v1, 0x7f070327

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020298

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 414
    :goto_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 415
    return-void

    .line 409
    :catch_0
    move-exception v0

    .line 410
    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 411
    :catch_1
    move-exception v0

    .line 412
    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
