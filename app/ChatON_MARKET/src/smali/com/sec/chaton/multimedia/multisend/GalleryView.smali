.class public Lcom/sec/chaton/multimedia/multisend/GalleryView;
.super Landroid/support/v4/app/Fragment;
.source "GalleryView.java"


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/k;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/sec/chaton/multimedia/multisend/g;

.field private e:Lcom/sec/common/f/c;

.field private f:Landroid/widget/TextView;

.field private g:Z

.field private h:Z

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 153
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/multisend/GalleryView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/multisend/GalleryView;)Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/multisend/GalleryView;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->i:I

    return v0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/multisend/GalleryView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 122
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 126
    packed-switch p1, :pswitch_data_0

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 128
    :pswitch_0
    const-string v0, "Get the item list selected in Category"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    if-eqz p3, :cond_0

    .line 131
    const-string v0, "send_list"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 132
    const-string v1, "preview_data"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    .line 134
    if-eqz v0, :cond_0

    .line 135
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 136
    const-string v0, "preview_data"

    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 138
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->c(Landroid/content/Intent;)V

    goto :goto_0

    .line 145
    :cond_1
    if-nez p2, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;

    if-eqz v0, :cond_2

    .line 147
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->a:Z

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 126
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 38
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 45
    const-string v1, "preview_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    .line 48
    const-string v1, "POSTON"

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "caller"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    iput-boolean v5, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->h:Z

    .line 50
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "attachedimagecount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->i:I

    .line 57
    :goto_0
    if-eqz p1, :cond_0

    .line 58
    const-string v0, "temp_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    .line 59
    iput-boolean v5, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->g:Z

    .line 61
    :cond_0
    return-void

    .line 52
    :cond_1
    iput-boolean v4, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->h:Z

    .line 53
    iput v4, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->i:I

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 65
    const v0, 0x7f0300f7

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->b:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->e:Lcom/sec/common/f/c;

    .line 70
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/g;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0300f8

    iget-object v4, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->b:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->e:Lcom/sec/common/f/c;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/sec/chaton/multimedia/multisend/g;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->d:Lcom/sec/chaton/multimedia/multisend/g;

    .line 72
    const v0, 0x7f070445

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->a:Landroid/widget/ListView;

    .line 74
    const v0, 0x7f070297

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->f:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->a:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/chaton/multimedia/multisend/j;

    invoke-direct {v2, p0}, Lcom/sec/chaton/multimedia/multisend/j;-><init>(Lcom/sec/chaton/multimedia/multisend/GalleryView;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryView;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 111
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->g:Z

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->d:Lcom/sec/chaton/multimedia/multisend/g;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/multisend/g;->a(Ljava/util/ArrayList;)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->a:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->d:Lcom/sec/chaton/multimedia/multisend/g;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 117
    return-object v1

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 190
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 191
    const-string v0, "GalleryView.onDestroy()"

    const-class v1, Lcom/sec/chaton/multimedia/multisend/GalleryView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->d:Lcom/sec/chaton/multimedia/multisend/g;

    if-eqz v0, :cond_3

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->d:Lcom/sec/chaton/multimedia/multisend/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/g;->a()V

    .line 204
    :cond_3
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 205
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 174
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 176
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->a:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->e:Lcom/sec/common/f/c;

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->e:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 185
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 210
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 211
    const-string v0, "temp_state"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/GalleryView;->c:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 212
    return-void
.end method
