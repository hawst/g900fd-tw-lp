.class Lcom/sec/chaton/multimedia/audio/j;
.super Ljava/lang/Object;
.source "PlayVoiceTask.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/audio/h;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/audio/h;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 2

    .prologue
    .line 280
    packed-switch p1, :pswitch_data_0

    .line 324
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 282
    :pswitch_1
    :try_start_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 283
    const-string v0, "[AudioFocus] : AUDIOFOCUS_GAIN"

    invoke-static {}, Lcom/sec/chaton/multimedia/audio/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/h;->c(Lcom/sec/chaton/multimedia/audio/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/h;->a(Lcom/sec/chaton/multimedia/audio/h;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 289
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 292
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/audio/h;->a(Lcom/sec/chaton/multimedia/audio/h;Z)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 320
    :catch_0
    move-exception v0

    .line 321
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 297
    :pswitch_2
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_3

    .line 298
    const-string v0, "[AudioFocus] : AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK"

    invoke-static {}, Lcom/sec/chaton/multimedia/audio/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/h;->a(Lcom/sec/chaton/multimedia/audio/h;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/h;->a(Lcom/sec/chaton/multimedia/audio/h;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/audio/h;->a(Lcom/sec/chaton/multimedia/audio/h;Z)Z

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    goto/16 :goto_0

    .line 311
    :pswitch_3
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 312
    const-string v0, "[AudioFocus] : AUDIOFOCUS_LOSS"

    invoke-static {}, Lcom/sec/chaton/multimedia/audio/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/h;->a(Lcom/sec/chaton/multimedia/audio/h;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/j;->a:Lcom/sec/chaton/multimedia/audio/h;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/audio/h;->b(Lcom/sec/chaton/multimedia/audio/h;Z)Z
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 280
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
