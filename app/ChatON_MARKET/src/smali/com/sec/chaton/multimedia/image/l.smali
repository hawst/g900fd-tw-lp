.class Lcom/sec/chaton/multimedia/image/l;
.super Ljava/lang/Object;
.source "ImagePagerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/l;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x4

    const/4 v5, 0x0

    .line 257
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/l;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/l;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/l;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/l;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/l;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/t;

    .line 263
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 265
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_2

    .line 266
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDetailView : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :cond_2
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->d:Ljava/lang/String;

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 272
    iget-object v7, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    .line 274
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v1, v2, :cond_3

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v2, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v1, v2, :cond_4

    :cond_3
    move v8, v5

    .line 281
    :goto_1
    array-length v0, v4

    if-le v0, v3, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/l;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->d(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/l;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/l;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/lang/String;

    move-result-object v2

    aget-object v3, v4, v3

    const/4 v6, 0x3

    aget-object v4, v4, v6

    iget-object v6, p0, Lcom/sec/chaton/multimedia/image/l;->a:Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-static {v6}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->g(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Z

    move-result v6

    invoke-static/range {v0 .. v8}, Lcom/sec/chaton/trunk/TrunkDetailActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 276
    :cond_4
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v1, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v0, v1, :cond_5

    .line 277
    const/4 v8, 0x1

    goto :goto_1

    :cond_5
    move v8, v5

    goto :goto_1
.end method
