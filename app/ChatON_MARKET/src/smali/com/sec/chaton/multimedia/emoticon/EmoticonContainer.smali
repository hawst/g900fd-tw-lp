.class public Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;
.super Landroid/support/v4/app/Fragment;
.source "EmoticonContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Landroid/app/Activity;

.field private d:Landroid/widget/GridView;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/ListAdapter;

.field private i:Landroid/widget/ListAdapter;

.field private j:Lcom/sec/chaton/multimedia/emoticon/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 243
    return-void
.end method

.method private a()Landroid/widget/ListAdapter;
    .locals 3

    .prologue
    .line 156
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 158
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/h;->c()[Ljava/lang/String;

    move-result-object v0

    .line 160
    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/e;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->c:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Lcom/sec/chaton/multimedia/emoticon/e;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    return-object v1
.end method

.method public static final a(IZ)Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 41
    const-string v1, "layoutResId"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 44
    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;

    invoke-direct {v1}, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;-><init>()V

    .line 45
    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->setArguments(Landroid/os/Bundle;)V

    .line 47
    return-object v1
.end method

.method private b()Landroid/widget/ListAdapter;
    .locals 3

    .prologue
    .line 169
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/g;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->c:Landroid/app/Activity;

    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/h;->b()Lcom/sec/common/f/b;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/multimedia/emoticon/g;-><init>(Landroid/content/Context;Lcom/sec/common/f/b;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/multimedia/emoticon/f;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->j:Lcom/sec/chaton/multimedia/emoticon/f;

    .line 71
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    const-string v0, "recent"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 206
    const-string v0, "recent"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->b:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->h:Landroid/widget/ListAdapter;

    if-nez v0, :cond_2

    .line 213
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->h:Landroid/widget/ListAdapter;

    .line 216
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->h:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->d:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setVisibility(I)V

    .line 224
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->d:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->h:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 220
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->d:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    goto :goto_1

    .line 225
    :cond_4
    const-string v0, "normal"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const-string v0, "normal"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->b:Ljava/lang/String;

    .line 228
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->i:Landroid/widget/ListAdapter;

    if-nez v0, :cond_5

    .line 233
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->i:Landroid/widget/ListAdapter;

    .line 236
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->d:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->d:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->i:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 77
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->c:Landroid/app/Activity;

    .line 78
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->g:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    const-string v0, "normal"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->a(Ljava/lang/String;)V

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->f:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    const-string v0, "recent"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 91
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 92
    const-string v1, "layoutResId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->a:I

    .line 93
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 100
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->b:Ljava/lang/String;

    .line 105
    if-eqz p3, :cond_0

    .line 106
    const-string v0, "currentTabId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    const-string v0, "normal"

    move-object v1, v0

    .line 113
    :goto_0
    iget v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->a:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 115
    const v0, 0x7f07028a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->d:Landroid/widget/GridView;

    .line 117
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->d:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 119
    const v0, 0x7f07028c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->e:Landroid/view/View;

    .line 122
    const v0, 0x7f07030d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->f:Landroid/view/View;

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->f:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    const v0, 0x7f07030e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->g:Landroid/view/View;

    .line 126
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->g:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->a(Ljava/lang/String;)V

    .line 130
    return-object v2

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 135
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 138
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->h:Landroid/widget/ListAdapter;

    .line 139
    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->i:Landroid/widget/ListAdapter;

    .line 140
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->c:Landroid/app/Activity;

    .line 85
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 174
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 176
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->j:Lcom/sec/chaton/multimedia/emoticon/f;

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->j:Lcom/sec/chaton/multimedia/emoticon/f;

    invoke-interface {v1, p0, v0}, Lcom/sec/chaton/multimedia/emoticon/f;->a(Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 147
    const-string v0, "currentTabId"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/EmoticonContainer;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-void
.end method
