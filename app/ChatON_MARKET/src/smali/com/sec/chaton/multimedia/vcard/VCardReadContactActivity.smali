.class public Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "VCardReadContactActivity.java"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;


# instance fields
.field private c:Landroid/content/BroadcastReceiver;

.field private d:Lcom/sec/chaton/multimedia/vcard/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "extra vard name"

    sput-object v0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->a:Ljava/lang/String;

    .line 28
    const-string v0, "extra fileuri"

    sput-object v0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->e()V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/vcard/m;-><init>(Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->c:Landroid/content/BroadcastReceiver;

    .line 86
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 87
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 88
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 89
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 90
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 91
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->e()V

    .line 92
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 95
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 97
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->finish()V

    .line 100
    :cond_0
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 104
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 121
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 124
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->startActivity(Landroid/content/Intent;)V

    .line 129
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactFragment;-><init>()V

    .line 41
    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->d:Lcom/sec/chaton/multimedia/vcard/n;

    .line 42
    return-object v0
.end method

.method public c()Lcom/sec/common/actionbar/a;
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->d:Lcom/sec/chaton/multimedia/vcard/n;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->d:Lcom/sec/chaton/multimedia/vcard/n;

    invoke-interface {v0}, Lcom/sec/chaton/multimedia/vcard/n;->a()V

    .line 58
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 136
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 137
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 140
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    if-eqz p1, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/n;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->d:Lcom/sec/chaton/multimedia/vcard/n;

    .line 51
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->f()V

    .line 75
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onPause()V

    .line 76
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->d()V

    .line 63
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 64
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->g()V

    .line 66
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 70
    :cond_0
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 108
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/VCardReadContactActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 118
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
