.class public Lcom/sec/chaton/multimedia/vcard/i;
.super Landroid/widget/BaseAdapter;
.source "ReadVCardAdapter.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/chaton/multimedia/vcard/i;->a:Landroid/content/Context;

    .line 24
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/i;->b:Landroid/view/LayoutInflater;

    .line 25
    iput-object p2, p0, Lcom/sec/chaton/multimedia/vcard/i;->c:Ljava/util/List;

    .line 26
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/vcard/i;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/i;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/view/View;Lcom/sec/chaton/multimedia/vcard/c;)V
    .locals 4

    .prologue
    const v0, 0x7f0b00c0

    const v3, 0x7f0b0040

    .line 86
    iget v1, p2, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 88
    iget v1, p2, Lcom/sec/chaton/multimedia/vcard/c;->a:I

    packed-switch v1, :pswitch_data_0

    .line 104
    :goto_0
    :pswitch_0
    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 121
    :goto_1
    return-void

    .line 93
    :pswitch_1
    const v0, 0x7f0b00c1

    .line 94
    goto :goto_0

    .line 96
    :pswitch_2
    const v0, 0x7f0b00c2

    .line 97
    goto :goto_0

    .line 99
    :pswitch_3
    const v0, 0x7f0b00c3

    goto :goto_0

    .line 106
    :cond_0
    iget v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 107
    check-cast p1, Landroid/widget/TextView;

    const v0, 0x7f0b003f

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 109
    :cond_1
    iget v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    .line 110
    check-cast p1, Landroid/widget/TextView;

    const v0, 0x7f0b0152

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 114
    :cond_2
    iget-object v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 115
    check-cast p1, Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/i;->a:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/sec/chaton/multimedia/vcard/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 117
    :cond_3
    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(Landroid/view/View;Lcom/sec/chaton/multimedia/vcard/c;)V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 125
    iget v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 126
    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->b:Ljava/lang/String;

    const-string v1, "-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/multimedia/vcard/c;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/i;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/i;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/vcard/i;->a(I)Lcom/sec/chaton/multimedia/vcard/c;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 76
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/i;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 30
    .line 31
    if-nez p2, :cond_0

    .line 32
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/i;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030123

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 34
    new-instance v2, Lcom/sec/chaton/multimedia/vcard/j;

    move-object v0, v1

    check-cast v0, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/multimedia/vcard/j;-><init>(Lcom/sec/chaton/multimedia/vcard/i;Lcom/sec/chaton/widget/CheckableRelativeLayout;)V

    move-object p2, v1

    move-object v1, v2

    .line 39
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/i;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    .line 41
    iget-object v2, v1, Lcom/sec/chaton/multimedia/vcard/j;->a:Landroid/widget/TextView;

    invoke-direct {p0, v2, v0}, Lcom/sec/chaton/multimedia/vcard/i;->a(Landroid/view/View;Lcom/sec/chaton/multimedia/vcard/c;)V

    .line 42
    iget-object v2, v1, Lcom/sec/chaton/multimedia/vcard/j;->b:Landroid/widget/TextView;

    invoke-direct {p0, v2, v0}, Lcom/sec/chaton/multimedia/vcard/i;->b(Landroid/view/View;Lcom/sec/chaton/multimedia/vcard/c;)V

    .line 43
    iput-object v0, v1, Lcom/sec/chaton/multimedia/vcard/j;->c:Lcom/sec/chaton/multimedia/vcard/c;

    .line 45
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 46
    return-object p2

    .line 36
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/j;

    move-object v1, v0

    goto :goto_0
.end method
