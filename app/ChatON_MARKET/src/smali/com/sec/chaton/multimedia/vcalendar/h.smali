.class Lcom/sec/chaton/multimedia/vcalendar/h;
.super Ljava/lang/Object;
.source "VCalendarListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 286
    const-string v0, "*******************onLoadFinished()"

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "*******************Event count : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v0, p2}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;Landroid/database/Cursor;)V

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->b(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Landroid/widget/ExpandableListView;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v4, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v0, v3}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;Z)V

    .line 312
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->c(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 327
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->d(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->d(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->e(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->e(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Lcom/sec/chaton/widget/ClearableEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;Ljava/lang/String;)V

    .line 342
    :cond_1
    return-void

    .line 316
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->b(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Landroid/widget/ExpandableListView;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    invoke-direct {v1, v4, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 317
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;Z)V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 274
    const-string v0, "onCreateLoader()"

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/h;->a:Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "content://com.android.calendar/events"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string v6, "title"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "eventLocation"

    aput-object v6, v3, v5

    const/4 v5, 0x3

    const-string v6, "description"

    aput-object v6, v3, v5

    const/4 v5, 0x4

    const-string v6, "dtstart"

    aput-object v6, v3, v5

    const/4 v5, 0x5

    const-string v6, "dtend"

    aput-object v6, v3, v5

    const/4 v5, 0x6

    const-string v6, "calendar_id"

    aput-object v6, v3, v5

    const/4 v5, 0x7

    const-string v6, "rrule"

    aput-object v6, v3, v5

    const/16 v5, 0x8

    const-string v6, "deleted"

    aput-object v6, v3, v5

    const-string v6, " dtstart asc "

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 270
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/multimedia/vcalendar/h;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347
    return-void
.end method
