.class Lcom/sec/chaton/multimedia/image/af;
.super Ljava/lang/Object;
.source "PostONImagePagerActivity.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 561
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 549
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 16

    .prologue
    .line 480
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    iget v1, v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a:I

    move/from16 v0, p1

    if-ne v0, v1, :cond_4

    .line 481
    if-eqz p3, :cond_4

    .line 484
    const/4 v15, 0x0

    .line 485
    const/4 v14, 0x0

    move v1, v14

    .line 487
    :goto_0
    :try_start_0
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 489
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v3}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 490
    const-string v2, "poston_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 491
    const-string v2, "poston_metaid"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 492
    const-string v2, "poston_metatype"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 493
    const-string v2, "poston_url"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 494
    const-string v2, "joined_my_poston"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 495
    const-string v2, "joined_buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 496
    const-string v2, "poston_seq"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 497
    const-string v2, "joined_my_poston_time"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 498
    const-string v2, "joined_my_poston_comment_read"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 499
    const-string v2, "joined_my_poston_comment_unread"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 500
    const-string v2, "joined_buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 515
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->i(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 517
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v1, v3}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;Ljava/lang/String;)Ljava/lang/String;

    move v14, v15

    .line 519
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUrl(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    const-string v1, "3"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 522
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v14

    goto/16 :goto_0

    .line 502
    :cond_0
    const-string v2, "poston_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 503
    const-string v2, "poston_metaid"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 504
    const-string v2, "poston_metatype"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 505
    const-string v2, "poston_url"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 506
    const-string v2, "joined_buddy_poston"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 507
    const-string v2, "joined_buddy_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 508
    const-string v2, "poston_seq"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 509
    const-string v2, "joined_buddy_poston_time"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 510
    const-string v2, "joined_buddy_poston_comment_read"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 511
    const-string v2, "joined_buddy_poston_comment_unread"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 512
    const-string v2, "joined_buddy_no"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_1

    .line 524
    :cond_1
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_2

    .line 525
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PostON description : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :cond_2
    new-instance v1, Lcom/sec/chaton/multimedia/image/aj;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-direct/range {v1 .. v13}, Lcom/sec/chaton/multimedia/image/aj;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 529
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    add-int/lit8 v15, v15, 0x1

    move v1, v14

    .line 532
    goto/16 :goto_0

    .line 535
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 536
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/af;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v2, v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 543
    :cond_4
    return-void

    .line 538
    :catchall_0
    move-exception v1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_5
    move v14, v1

    goto/16 :goto_2
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 555
    return-void
.end method
