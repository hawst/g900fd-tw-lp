.class public Lcom/sec/chaton/multimedia/image/ai;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "PostONImagePagerActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 617
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/ai;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    .line 618
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 619
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ai;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 624
    new-instance v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;-><init>()V

    .line 626
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 627
    const-string v2, "position"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 628
    const-string v2, "buddyId"

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ai;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-static {v3}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 631
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 632
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getItem : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    :cond_0
    return-object v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 644
    const/4 v0, -0x2

    return v0
.end method
