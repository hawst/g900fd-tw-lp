.class Lcom/sec/chaton/multimedia/image/ak;
.super Ljava/lang/Object;
.source "PostONImagePagerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 231
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 236
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 240
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->a(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->b(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 242
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v0, v2

    invoke-static {v1, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->a(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-static {v3}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->d(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->a(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;Ljava/io/File;)Ljava/io/File;

    .line 245
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->e(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->e(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 250
    invoke-static {}, Lcom/sec/chaton/util/am;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/chaton/util/am;->k()Z

    move-result v0

    if-nez v0, :cond_4

    .line 251
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00dd

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 254
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/aq;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->e(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/chaton/multimedia/image/aq;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 257
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ak;->a:Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->f(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V

    goto/16 :goto_0
.end method
