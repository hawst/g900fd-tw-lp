.class Lcom/sec/chaton/multimedia/audio/v;
.super Ljava/lang/Object;
.source "VoiceListFragment.java"

# interfaces
.implements Lcom/sec/chaton/e/a/v;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/v;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method public onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 146
    return-void
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/v;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->b(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/v;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->c(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)V

    .line 119
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/v;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0, p3}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 121
    const/4 v1, 0x0

    .line 124
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/v;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/v;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 125
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/v;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->d(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    if-eqz v1, :cond_0

    .line 136
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 128
    :cond_3
    :try_start_1
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "count : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/v;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/v;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->e(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Lcom/sec/chaton/multimedia/audio/q;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/v;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/audio/q;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 135
    if-eqz v0, :cond_0

    .line 136
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 136
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 135
    :cond_5
    throw v0
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 152
    return-void
.end method
