.class public Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;
.super Landroid/support/v4/app/Fragment;
.source "VCalendarDetailFragment.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Lcom/sec/chaton/multimedia/vcalendar/j;

.field private k:Lcom/sec/chaton/multimedia/vcalendar/k;

.field private l:Landroid/app/ProgressDialog;

.field private m:Landroid/view/MenuItem;

.field private n:Landroid/app/Activity;

.field private o:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 306
    new-instance v0, Lcom/sec/chaton/multimedia/vcalendar/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/vcalendar/d;-><init>(Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->o:Ljava/lang/Runnable;

    return-void
.end method

.method private a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 534
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://calendar/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 537
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 539
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 543
    :goto_0
    if-nez v6, :cond_0

    .line 545
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://com.android.calendar/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 547
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 552
    :goto_1
    return-object v0

    .line 548
    :catch_0
    move-exception v0

    move-object v0, v6

    goto :goto_1

    .line 540
    :catch_1
    move-exception v0

    goto :goto_0

    :cond_0
    move-object v0, v6

    goto :goto_1

    :cond_1
    move-object v0, v6

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;)Lcom/sec/chaton/multimedia/vcalendar/j;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/j;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 289
    new-instance v0, Lcom/sec/chaton/multimedia/vcalendar/k;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/vcalendar/k;-><init>()V

    .line 290
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->i:Ljava/lang/String;

    .line 293
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->e:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->c:Ljava/lang/String;

    .line 294
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->f:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->b:Ljava/lang/String;

    .line 295
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->j:Ljava/lang/String;

    .line 296
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->h:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->a:Ljava/lang/String;

    .line 298
    new-instance v1, Lcom/sec/chaton/multimedia/vcalendar/j;

    invoke-direct {v1}, Lcom/sec/chaton/multimedia/vcalendar/j;-><init>()V

    iput-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/j;

    .line 299
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/j;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/vcalendar/j;->a(Lcom/sec/chaton/multimedia/vcalendar/k;)V

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 302
    new-instance v0, Ljava/lang/Thread;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->o:Ljava/lang/Runnable;

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;)V

    .line 303
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 304
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 411
    new-instance v3, Lcom/sec/chaton/multimedia/vcalendar/b;

    invoke-direct {v3}, Lcom/sec/chaton/multimedia/vcalendar/b;-><init>()V

    .line 416
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 417
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->c:Ljava/lang/String;

    const-string v2, "file://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 418
    array-length v2, v0

    if-ne v2, v5, :cond_1

    .line 419
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->c:Ljava/lang/String;

    .line 430
    :goto_0
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 432
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 434
    :goto_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 435
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 440
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_0

    .line 441
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 440
    :cond_0
    throw v0

    .line 420
    :cond_1
    array-length v2, v0

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    .line 421
    aget-object v0, v0, v5

    goto :goto_0

    .line 423
    :cond_2
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Parsing Error"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 438
    :cond_3
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/chaton/multimedia/vcalendar/b;->a(Ljava/lang/String;)Lcom/sec/chaton/multimedia/vcalendar/j;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/j;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 440
    if-eqz v2, :cond_4

    .line 441
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 444
    :cond_4
    return-void

    .line 440
    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method private c()V
    .locals 7

    .prologue
    .line 447
    const-string v0, ""

    .line 448
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_1

    .line 449
    const-string v0, "calendar"

    .line 454
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 455
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 458
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 460
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->d()I

    move-result v3

    .line 462
    if-lez v3, :cond_2

    .line 464
    const-string v4, "calendar_id"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 466
    const-string v3, "title"

    iget-object v4, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v3, "description"

    iget-object v4, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string v3, "eventLocation"

    iget-object v4, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const-string v3, "dtstart"

    iget-object v4, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->e:Ljava/lang/Long;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 470
    const-string v3, "dtend"

    iget-object v4, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->f:Ljava/lang/Long;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 473
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 474
    new-instance v4, Ljava/util/Date;

    iget-object v5, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->e:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 475
    const-string v4, "eventTimezone"

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "******* SAVE EVENTS : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/events"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 480
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 482
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    const v1, 0x7f0b00e6

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 483
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 489
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 491
    :cond_0
    return-void

    .line 451
    :cond_1
    const-string v0, "com.android.calendar"

    goto/16 :goto_0

    .line 486
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    const v1, 0x7f0b0082

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private d()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 494
    .line 495
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const/4 v0, 0x1

    const-string v3, "name"

    aput-object v3, v2, v0

    .line 496
    const-string v0, "selected=1"

    .line 497
    const-string v3, "calendars"

    .line 501
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_0

    .line 503
    const/4 v0, 0x0

    .line 507
    :cond_0
    invoke-direct {p0, v2, v0, v3}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 509
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 511
    const-string v2, "_id"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 515
    :cond_1
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 520
    if-eqz v3, :cond_2

    .line 521
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 530
    :goto_0
    return v0

    .line 525
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 333
    .line 334
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 335
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 344
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 345
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 346
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 348
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/co;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".VCS"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 354
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 371
    if-eqz v0, :cond_1

    .line 372
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 383
    :cond_1
    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 394
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 395
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 396
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->a(Ljava/lang/String;)V

    .line 399
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    if-eqz v1, :cond_3

    .line 400
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 401
    sget-object v1, Lcom/sec/chaton/chat/ChatFragment;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 402
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 404
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 405
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 407
    :cond_3
    :goto_1
    return-void

    .line 337
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    if-eqz v0, :cond_3

    .line 338
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 339
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 370
    :catchall_0
    move-exception v0

    .line 371
    :goto_2
    if-eqz v1, :cond_5

    .line 372
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 370
    :cond_5
    :goto_3
    throw v0

    .line 363
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 371
    :goto_4
    if-eqz v0, :cond_1

    .line 372
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    goto :goto_0

    .line 375
    :catch_1
    move-exception v0

    goto :goto_0

    .line 365
    :catch_2
    move-exception v0

    move-object v0, v1

    .line 371
    :goto_5
    if-eqz v0, :cond_1

    .line 372
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 375
    :catch_3
    move-exception v1

    goto :goto_3

    .line 370
    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_2

    .line 365
    :catch_4
    move-exception v1

    goto :goto_5

    .line 363
    :catch_5
    move-exception v1

    goto :goto_4
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 557
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 558
    iput-object p1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    .line 559
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->setHasOptionsMenu(Z)V

    .line 75
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 589
    const v0, 0x7f0f0023

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 591
    const v0, 0x7f0705a6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->m:Landroid/view/MenuItem;

    .line 592
    iget v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 593
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->m:Landroid/view/MenuItem;

    const v1, 0x7f0b003c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 595
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 596
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const-wide/16 v8, 0x0

    const v7, 0x7f0b0136

    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 79
    const v0, 0x7f0300c4

    invoke-virtual {p1, v0, p2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 81
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_0

    .line 83
    const-string v3, "ACTIVITY_PURPOSE"

    invoke-virtual {v0, v3, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->i:I

    .line 86
    :cond_0
    iget v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->i:I

    if-ne v3, v6, :cond_7

    .line 87
    if-eqz v0, :cond_1

    .line 88
    const-string v3, "inbox_NO"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->b:Ljava/lang/String;

    .line 90
    const-string v3, "extra summary"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->d:Ljava/lang/String;

    .line 91
    const-string v3, "extra dtstart"

    invoke-virtual {v0, v3, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->e:Ljava/lang/Long;

    .line 92
    const-string v3, "extra dtend"

    invoke-virtual {v0, v3, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->f:Ljava/lang/Long;

    .line 93
    const-string v3, "extra location"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->g:Ljava/lang/String;

    .line 94
    const-string v3, "extra desc"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->h:Ljava/lang/String;

    .line 200
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 202
    :cond_2
    const v0, 0x7f0b019b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->d:Ljava/lang/String;

    .line 204
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->g:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 205
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->g:Ljava/lang/String;

    .line 207
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->h:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 208
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->h:Ljava/lang/String;

    .line 211
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->e:Ljava/lang/Long;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->f:Ljava/lang/Long;

    if-nez v0, :cond_10

    .line 212
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    if-eqz v0, :cond_10

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {p0, v7}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    move-object v0, v1

    .line 285
    :goto_1
    return-object v0

    .line 97
    :cond_7
    iget v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->i:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 98
    if-eqz v0, :cond_8

    .line 99
    const-string v3, "URI"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->c:Ljava/lang/String;

    .line 104
    :cond_8
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/j;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcalendar/j;->a:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcalendar/k;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    .line 119
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    if-nez v0, :cond_b

    .line 120
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    if-eqz v0, :cond_9

    .line 121
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_9
    move-object v0, v1

    .line 123
    goto :goto_1

    .line 105
    :catch_0
    move-exception v0

    .line 106
    sget-object v2, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    if-eqz v0, :cond_a

    .line 108
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {p0, v7}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_a
    move-object v0, v1

    .line 112
    goto :goto_1

    .line 126
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->d:Ljava/lang/String;

    .line 128
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyyMMdd\'T\'HHmmss"

    invoke-direct {v3, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 130
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 132
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->c:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 136
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->c:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v5, v5, Lcom/sec/chaton/multimedia/vcalendar/k;->c:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 151
    :goto_2
    if-eqz v0, :cond_e

    .line 152
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->e:Ljava/lang/Long;

    .line 161
    :cond_c
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->b:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 165
    :try_start_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->b:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v5, v5, Lcom/sec/chaton/multimedia/vcalendar/k;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v0

    .line 181
    :goto_4
    if-eqz v0, :cond_f

    .line 182
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->f:Ljava/lang/Long;

    .line 191
    :cond_d
    :goto_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->g:Ljava/lang/String;

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcalendar/k;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->h:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->m:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->m:Landroid/view/MenuItem;

    const v3, 0x7f0b003c

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 137
    :catch_1
    move-exception v0

    .line 139
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyyMMdd"

    invoke-direct {v0, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 141
    const-string v4, "GMT"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 143
    :try_start_3
    iget-object v4, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v4, v4, Lcom/sec/chaton/multimedia/vcalendar/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v0

    goto :goto_2

    .line 144
    :catch_2
    move-exception v0

    .line 145
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    move-object v0, v1

    goto :goto_2

    .line 155
    :cond_e
    iput-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->e:Ljava/lang/Long;

    goto :goto_3

    .line 167
    :catch_3
    move-exception v0

    .line 169
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd"

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 171
    const-string v3, "GMT"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 173
    :try_start_4
    iget-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->k:Lcom/sec/chaton/multimedia/vcalendar/k;

    iget-object v3, v3, Lcom/sec/chaton/multimedia/vcalendar/k;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_4
    .catch Ljava/text/ParseException; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v0

    goto :goto_4

    .line 174
    :catch_4
    move-exception v0

    .line 175
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    move-object v0, v1

    goto :goto_4

    .line 185
    :cond_f
    iput-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->f:Ljava/lang/Long;

    goto :goto_5

    .line 219
    :cond_10
    const v0, 0x7f07036d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 220
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    new-instance v3, Ljava/util/Date;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 224
    new-instance v4, Ljava/util/Date;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 225
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v0, "EEEE "

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {v5, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 226
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v6

    .line 227
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, " hh:mma"

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {v0, v1, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 229
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 230
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, " HH:mm"

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {v0, v1, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move-object v1, v0

    .line 234
    :goto_6
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 235
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 236
    new-instance v0, Ljava/util/Date;

    iget-object v8, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->e:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 238
    const v0, 0x7f07036e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 242
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    new-instance v0, Ljava/util/Date;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->f:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 251
    const v0, 0x7f07036f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 255
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "- "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v5, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v6, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    const v0, 0x7f070370

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 266
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->g:Ljava/lang/String;

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_13

    .line 267
    :cond_11
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 275
    :goto_7
    const v0, 0x7f070371

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 276
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 277
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    :cond_12
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->l:Landroid/app/ProgressDialog;

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->l:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v10}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->l:Landroid/app/ProgressDialog;

    const v1, 0x7f0b0041

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    move-object v0, v2

    .line 285
    goto/16 :goto_1

    .line 269
    :cond_13
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    :cond_14
    move-object v1, v0

    goto/16 :goto_6
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 563
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 564
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    .line 565
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 569
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 570
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    if-eqz v1, :cond_3

    .line 571
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->n:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 584
    :cond_0
    :goto_0
    return v0

    .line 574
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0705a6

    if-ne v1, v2, :cond_3

    .line 575
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->m:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 577
    iget v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->i:I

    if-ne v1, v0, :cond_2

    .line 578
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->a()V

    goto :goto_0

    .line 579
    :cond_2
    iget v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->i:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 580
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailFragment;->c()V

    goto :goto_0

    .line 584
    :cond_3
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
