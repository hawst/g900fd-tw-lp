.class public Lcom/sec/chaton/multimedia/image/b;
.super Landroid/os/AsyncTask;
.source "GifViewTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Lcom/sec/chaton/multimedia/image/GifView;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/view/ViewGroup;

.field private c:Landroid/view/View;

.field private d:Lcom/sec/chaton/multimedia/image/GifView;

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/view/ViewGroup;Landroid/view/View;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/b;->a:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/sec/chaton/multimedia/image/b;->b:Landroid/view/ViewGroup;

    .line 24
    iput-object p3, p0, Lcom/sec/chaton/multimedia/image/b;->c:Landroid/view/View;

    .line 25
    iput-object p4, p0, Lcom/sec/chaton/multimedia/image/b;->e:Landroid/os/Handler;

    .line 26
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Lcom/sec/chaton/multimedia/image/GifView;
    .locals 4

    .prologue
    .line 30
    new-instance v0, Lcom/sec/chaton/multimedia/image/GifView;

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/b;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/GifView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 31
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/b;->d:Lcom/sec/chaton/multimedia/image/GifView;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/b;->d:Lcom/sec/chaton/multimedia/image/GifView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/GifView;->a()V

    .line 60
    :cond_0
    return-void
.end method

.method protected a(Lcom/sec/chaton/multimedia/image/GifView;)V
    .locals 3

    .prologue
    .line 36
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/image/GifView;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/b;->e:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 38
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/b;->e:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/b;->a:Ljava/lang/String;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 40
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/b;->e:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 54
    :cond_1
    :goto_0
    return-void

    .line 45
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/b;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 46
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/b;->d:Lcom/sec/chaton/multimedia/image/GifView;

    .line 47
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/b;->d:Lcom/sec/chaton/multimedia/image/GifView;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/GifView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/b;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 51
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/b;->c:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/b;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/image/b;->a([Ljava/lang/String;)Lcom/sec/chaton/multimedia/image/GifView;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lcom/sec/chaton/multimedia/image/GifView;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/image/b;->a(Lcom/sec/chaton/multimedia/image/GifView;)V

    return-void
.end method
