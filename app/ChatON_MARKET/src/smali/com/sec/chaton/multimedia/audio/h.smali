.class public Lcom/sec/chaton/multimedia/audio/h;
.super Landroid/os/AsyncTask;
.source "PlayVoiceTask.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;",
        "Landroid/media/MediaPlayer$OnCompletionListener;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field protected a:Landroid/view/View;

.field protected b:J

.field protected c:Landroid/widget/ImageView;

.field protected d:Landroid/widget/TextView;

.field protected e:Z

.field protected f:I

.field protected g:I

.field protected h:Landroid/graphics/drawable/AnimationDrawable;

.field protected i:J

.field private k:Ljava/lang/String;

.field private l:Ljava/util/Timer;

.field private m:Landroid/media/MediaPlayer;

.field private n:Landroid/media/AudioManager;

.field private o:Z

.field private p:Z

.field private q:Landroid/media/AudioManager$OnAudioFocusChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/chaton/multimedia/audio/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/audio/h;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Ljava/lang/String;JZ)V
    .locals 3

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 274
    new-instance v0, Lcom/sec/chaton/multimedia/audio/j;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/audio/j;-><init>(Lcom/sec/chaton/multimedia/audio/h;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->q:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 47
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/h;->a:Landroid/view/View;

    .line 48
    iput-object p2, p0, Lcom/sec/chaton/multimedia/audio/h;->k:Ljava/lang/String;

    .line 49
    iput-wide p3, p0, Lcom/sec/chaton/multimedia/audio/h;->b:J

    .line 50
    iput-boolean p5, p0, Lcom/sec/chaton/multimedia/audio/h;->e:Z

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "messageId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/multimedia/audio/h;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPath:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/audio/h;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/h;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/h;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/h;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/h;Z)Z
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/chaton/multimedia/audio/h;->p:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/audio/h;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/h;->o:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/audio/h;Z)Z
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/chaton/multimedia/audio/h;->o:Z

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/audio/h;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/h;->p:Z

    return v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/chaton/multimedia/audio/h;->j:Ljava/lang/String;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->l:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->l:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 143
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/h;->o:Z

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 148
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/audio/h;->b()V

    .line 159
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->n:Landroid/media/AudioManager;

    if-eqz v0, :cond_3

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->n:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->q:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 173
    :cond_3
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->c:Landroid/widget/ImageView;

    const v1, 0x7f02011e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 188
    iget v0, p0, Lcom/sec/chaton/multimedia/audio/h;->f:I

    if-gtz v0, :cond_1

    iget v0, p0, Lcom/sec/chaton/multimedia/audio/h;->g:I

    if-lez v0, :cond_2

    .line 189
    :cond_1
    const-string v0, "%d:%02d/%d:%02d"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/sec/chaton/multimedia/audio/h;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/sec/chaton/multimedia/audio/h;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 190
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    :cond_2
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 329
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 330
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 331
    const-string v1, "ChatON"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 332
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 333
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 57
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->n:Landroid/media/AudioManager;

    .line 58
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->n:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->q:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 73
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/h;->i()V

    .line 75
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 80
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->k:Ljava/lang/String;

    const-string v1, "file:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->k:Ljava/lang/String;

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/h;->o:Z

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    const v1, 0xea60

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/chaton/multimedia/audio/h;->f:I

    .line 106
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    rem-int/lit8 v0, v0, 0x3c

    iput v0, p0, Lcom/sec/chaton/multimedia/audio/h;->g:I

    .line 108
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0, v6}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->l:Ljava/util/Timer;

    .line 109
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->l:Ljava/util/Timer;

    new-instance v1, Lcom/sec/chaton/multimedia/audio/i;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/audio/i;-><init>(Lcom/sec/chaton/multimedia/audio/h;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x12c

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 123
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/audio/h;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/h;->o:Z

    if-nez v0, :cond_2

    .line 126
    :cond_3
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/h;->o:Z

    if-nez v0, :cond_4

    .line 127
    new-array v0, v6, [Ljava/lang/Integer;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/audio/h;->publishProgress([Ljava/lang/Object;)V

    .line 129
    const-wide/16 v0, 0xc8

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_4

    .line 134
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->k:Ljava/lang/String;

    :goto_1
    return-object v0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    sget-object v1, Lcom/sec/chaton/multimedia/audio/h;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 93
    const-string v0, "null"

    goto :goto_1

    .line 94
    :catch_1
    move-exception v0

    .line 95
    sget-object v1, Lcom/sec/chaton/multimedia/audio/h;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 96
    const-string v0, "null"

    goto :goto_1

    .line 97
    :catch_2
    move-exception v0

    .line 98
    sget-object v1, Lcom/sec/chaton/multimedia/audio/h;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 99
    const-string v0, "null"

    goto :goto_1

    .line 100
    :catch_3
    move-exception v0

    .line 101
    sget-object v1, Lcom/sec/chaton/multimedia/audio/h;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 102
    const-string v0, "null"

    goto :goto_1

    .line 130
    :catch_4
    move-exception v0

    .line 131
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method protected a()V
    .locals 5

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/el;

    .line 215
    iget-wide v1, v0, Lcom/sec/chaton/chat/el;->c:J

    iput-wide v1, p0, Lcom/sec/chaton/multimedia/audio/h;->i:J

    .line 216
    iget-wide v1, p0, Lcom/sec/chaton/multimedia/audio/h;->i:J

    iget-wide v3, p0, Lcom/sec/chaton/multimedia/audio/h;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 217
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/audio/h;->e:Z

    if-eqz v1, :cond_1

    .line 218
    iget-object v1, v0, Lcom/sec/chaton/chat/el;->R:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->c:Landroid/widget/ImageView;

    .line 219
    iget-object v0, v0, Lcom/sec/chaton/chat/el;->S:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->d:Landroid/widget/TextView;

    .line 224
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->c:Landroid/widget/ImageView;

    const v1, 0x7f02015a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->h:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 231
    :cond_0
    return-void

    .line 221
    :cond_1
    iget-object v1, v0, Lcom/sec/chaton/chat/el;->w:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->c:Landroid/widget/ImageView;

    .line 222
    iget-object v0, v0, Lcom/sec/chaton/chat/el;->x:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->d:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/h;->a:Landroid/view/View;

    .line 346
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/audio/h;->a()V

    .line 347
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/h;->g()V

    .line 244
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/h;->h()V

    .line 245
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 246
    return-void
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 251
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/chat/el;

    .line 253
    iget-wide v0, v0, Lcom/sec/chaton/chat/el;->c:J

    iput-wide v0, p0, Lcom/sec/chaton/multimedia/audio/h;->i:J

    .line 254
    iget-wide v0, p0, Lcom/sec/chaton/multimedia/audio/h;->i:J

    iget-wide v2, p0, Lcom/sec/chaton/multimedia/audio/h;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 255
    aget-object v0, p1, v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v1, 0xea60

    div-int/2addr v0, v1

    .line 256
    aget-object v1, p1, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    div-int/lit16 v1, v1, 0x3e8

    rem-int/lit8 v1, v1, 0x3c

    .line 258
    const-string v2, "%d:%02d/%d:%02d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    iget v1, p0, Lcom/sec/chaton/multimedia/audio/h;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x3

    iget v1, p0, Lcom/sec/chaton/multimedia/audio/h;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 259
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->d:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 265
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 176
    invoke-static {}, Lcom/sec/chaton/multimedia/audio/b;->a()Lcom/sec/chaton/multimedia/audio/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/multimedia/audio/b;->a(Lcom/sec/chaton/multimedia/audio/h;)V

    .line 177
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/h;->k:Ljava/lang/String;

    .line 351
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 341
    iget-wide v0, p0, Lcom/sec/chaton/multimedia/audio/h;->b:J

    return-wide v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/h;->k:Ljava/lang/String;

    return-object v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/h;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 359
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 360
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/h;->m:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    const/16 v2, 0x1f4

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    .line 362
    :cond_0
    return v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/h;->g()V

    .line 270
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/h;->h()V

    .line 271
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 272
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/h;->o:Z

    .line 338
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/h;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/audio/h;->a()V

    .line 209
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 210
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/h;->a([Ljava/lang/Integer;)V

    return-void
.end method
