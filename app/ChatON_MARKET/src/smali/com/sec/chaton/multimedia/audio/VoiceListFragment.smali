.class public Lcom/sec/chaton/multimedia/audio/VoiceListFragment;
.super Landroid/support/v4/app/Fragment;
.source "VoiceListFragment.java"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/sec/chaton/e/a/v;

.field private c:Lcom/sec/chaton/e/a/u;

.field private d:Landroid/app/Activity;

.field private e:Landroid/widget/ListView;

.field private f:Lcom/sec/chaton/multimedia/audio/q;

.field private g:Landroid/database/Cursor;

.field private h:Landroid/app/ProgressDialog;

.field private i:Landroid/view/ViewStub;

.field private j:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 109
    new-instance v0, Lcom/sec/chaton/multimedia/audio/v;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/audio/v;-><init>(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a:Lcom/sec/chaton/e/a/v;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->g:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->g:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->d:Landroid/app/Activity;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->j:Landroid/view/View;

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->i:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->j:Landroid/view/View;

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->j:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 180
    const v1, 0x7f020350

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 182
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->j:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 183
    const v1, 0x7f0b00ae

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 187
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->h:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->d:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->h:Landroid/app/ProgressDialog;

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 222
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->d()V

    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->h:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->d:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 228
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->b()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Lcom/sec/chaton/multimedia/audio/q;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->f:Lcom/sec/chaton/multimedia/audio/q;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 191
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 193
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->d:Landroid/app/Activity;

    .line 194
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 46
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->c:Lcom/sec/chaton/e/a/u;

    .line 48
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 52
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 54
    const v0, 0x7f030118

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 55
    const v0, 0x7f0704a4

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->e:Landroid/widget/ListView;

    .line 56
    const v0, 0x7f0704a3

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 57
    const v3, 0x7f07014c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 58
    iget-object v3, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->d:Landroid/app/Activity;

    const v4, 0x7f0c0060

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 59
    const v3, 0x7f0b03cd

    new-array v4, v1, [Ljava/lang/Object;

    const-string v5, "3gp, 3ga, m4a, amr"

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    const v0, 0x7f0704a5

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->i:Landroid/view/ViewStub;

    .line 63
    new-instance v0, Lcom/sec/chaton/multimedia/audio/q;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f030125

    invoke-direct {v0, v3, v4, v2}, Lcom/sec/chaton/multimedia/audio/q;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->f:Lcom/sec/chaton/multimedia/audio/q;

    .line 64
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->e:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->f:Lcom/sec/chaton/multimedia/audio/q;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->e:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/chaton/multimedia/audio/u;

    invoke-direct {v3, p0}, Lcom/sec/chaton/multimedia/audio/u;-><init>(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 90
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v4, v6

    const-string v0, "_id"

    aput-object v0, v4, v1

    const/4 v0, 0x2

    const-string v3, "mime_type"

    aput-object v3, v4, v0

    const/4 v0, 0x3

    const-string v3, "_display_name"

    aput-object v3, v4, v0

    const/4 v0, 0x4

    const-string v3, "date_modified"

    aput-object v3, v4, v0

    const/4 v0, 0x5

    const-string v3, "duration"

    aput-object v3, v4, v0

    .line 93
    const-string v5, "_data LIKE \'%.3gp\' OR _data LIKE \'%.amr\' OR _data LIKE \'%.3ga\' OR _data LIKE \'%.m4a\'"

    .line 96
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->c:Lcom/sec/chaton/e/a/u;

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v7, "_display_name"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->c()V

    .line 106
    return-object v8
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 205
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->f:Lcom/sec/chaton/multimedia/audio/q;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->f:Lcom/sec/chaton/multimedia/audio/q;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/q;->a()V

    .line 211
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->d()V

    .line 212
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 198
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 200
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->d:Landroid/app/Activity;

    .line 201
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 168
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->g:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 170
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->b()V

    .line 173
    :cond_1
    return-void
.end method
