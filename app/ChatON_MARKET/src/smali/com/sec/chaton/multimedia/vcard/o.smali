.class public Lcom/sec/chaton/multimedia/vcard/o;
.super Landroid/widget/BaseAdapter;
.source "VCardReadContactAdapter.java"


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/sec/chaton/multimedia/vcard/o;->c:Landroid/content/Context;

    .line 27
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/o;->a:Landroid/view/LayoutInflater;

    .line 28
    iput-object p2, p0, Lcom/sec/chaton/multimedia/vcard/o;->b:Ljava/util/List;

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/vcard/o;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/o;->c:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/view/View;Lcom/sec/chaton/multimedia/vcard/c;)V
    .locals 3

    .prologue
    const v0, 0x7f0b00c0

    .line 87
    iget v1, p2, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 89
    iget v1, p2, Lcom/sec/chaton/multimedia/vcard/c;->a:I

    packed-switch v1, :pswitch_data_0

    .line 104
    :goto_0
    :pswitch_0
    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 113
    :cond_0
    :goto_1
    return-void

    .line 94
    :pswitch_1
    const v0, 0x7f0b00c1

    .line 95
    goto :goto_0

    .line 97
    :pswitch_2
    const v0, 0x7f0b00c2

    .line 98
    goto :goto_0

    .line 100
    :pswitch_3
    const v0, 0x7f0b00c3

    goto :goto_0

    .line 106
    :cond_1
    iget v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 107
    check-cast p1, Landroid/widget/TextView;

    const v0, 0x7f0b003f

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 109
    :cond_2
    iget v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 110
    check-cast p1, Landroid/widget/TextView;

    const v0, 0x7f0b0152

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 89
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(Landroid/view/View;Lcom/sec/chaton/multimedia/vcard/c;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 117
    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p2, Lcom/sec/chaton/multimedia/vcard/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/chaton/multimedia/vcard/c;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/o;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/vcard/o;->a(I)Lcom/sec/chaton/multimedia/vcard/c;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 77
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/o;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 33
    .line 34
    if-nez p2, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/o;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030123

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 37
    new-instance v2, Lcom/sec/chaton/multimedia/vcard/p;

    move-object v0, v1

    check-cast v0, Lcom/sec/chaton/widget/CheckableRelativeLayout;

    invoke-direct {v2, p0, v0}, Lcom/sec/chaton/multimedia/vcard/p;-><init>(Lcom/sec/chaton/multimedia/vcard/o;Lcom/sec/chaton/widget/CheckableRelativeLayout;)V

    move-object p2, v1

    move-object v1, v2

    .line 41
    :goto_0
    iget-object v2, v1, Lcom/sec/chaton/multimedia/vcard/p;->c:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/o;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-boolean v0, v0, Lcom/sec/chaton/multimedia/vcard/c;->f:Z

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 42
    iget-object v2, v1, Lcom/sec/chaton/multimedia/vcard/p;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/o;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    invoke-direct {p0, v2, v0}, Lcom/sec/chaton/multimedia/vcard/o;->a(Landroid/view/View;Lcom/sec/chaton/multimedia/vcard/c;)V

    .line 43
    iget-object v2, v1, Lcom/sec/chaton/multimedia/vcard/p;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/o;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/c;

    invoke-direct {p0, v2, v0}, Lcom/sec/chaton/multimedia/vcard/o;->b(Landroid/view/View;Lcom/sec/chaton/multimedia/vcard/c;)V

    .line 45
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 46
    return-object p2

    .line 39
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/p;

    move-object v1, v0

    goto :goto_0
.end method
