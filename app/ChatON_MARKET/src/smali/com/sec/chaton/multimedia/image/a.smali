.class public Lcom/sec/chaton/multimedia/image/a;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "GestureDetectorListener.java"


# instance fields
.field a:Lcom/sec/chaton/multimedia/image/as;


# direct methods
.method public constructor <init>(Lcom/sec/chaton/multimedia/image/as;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/a;->a:Lcom/sec/chaton/multimedia/image/as;

    .line 25
    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 38
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 48
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 41
    :pswitch_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 42
    const-string v0, "ACTION_DOUBLE_TAP"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/a;->a:Lcom/sec/chaton/multimedia/image/as;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/image/as;->a(FF)V

    goto :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/a;->a:Lcom/sec/chaton/multimedia/image/as;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/as;->c()V

    .line 55
    const/4 v0, 0x1

    return v0
.end method
