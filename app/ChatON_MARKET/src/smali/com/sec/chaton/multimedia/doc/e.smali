.class Lcom/sec/chaton/multimedia/doc/e;
.super Landroid/os/AsyncTask;
.source "FileExplorerFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/sec/chaton/multimedia/doc/g;",
        ">;>;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/chaton/multimedia/doc/g;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/multimedia/doc/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Lcom/sec/chaton/multimedia/doc/d;)V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/doc/e;-><init>(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)V

    return-void
.end method

.method private a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 294
    .line 296
    if-eqz p2, :cond_0

    array-length v0, p2

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 323
    :cond_0
    :goto_0
    return-object v4

    .line 300
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 303
    if-eqz v1, :cond_0

    .line 304
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 305
    aget-object v0, p2, v6

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 308
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 310
    const/4 v2, 0x0

    :try_start_0
    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 315
    :goto_1
    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 316
    const/4 v0, 0x1

    aget-object v0, p2, v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 320
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 311
    :catch_0
    move-exception v0

    move-object v0, v4

    goto :goto_1

    :cond_4
    move-object v0, v4

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x2

    .line 267
    if-eqz p2, :cond_0

    array-length v0, p2

    if-ge v0, v7, :cond_1

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->f(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 274
    if-eqz v1, :cond_0

    .line 275
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 276
    const/4 v0, 0x0

    aget-object v0, p2, v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 277
    const/4 v0, 0x1

    aget-object v0, p2, v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 278
    const/4 v0, -0x1

    .line 279
    array-length v5, p2

    if-le v5, v7, :cond_2

    .line 280
    aget-object v0, p2, v7

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 283
    :cond_2
    sget-boolean v5, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v5, :cond_3

    .line 284
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "data : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_3
    iget-object v5, p0, Lcom/sec/chaton/multimedia/doc/e;->b:Ljava/util/HashMap;

    new-instance v6, Lcom/sec/chaton/multimedia/doc/j;

    invoke-direct {v6, v3, v4, v0}, Lcom/sec/chaton/multimedia/doc/j;-><init>(JI)V

    invoke-virtual {v5, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 288
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 211
    .line 213
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v0, "/"

    invoke-direct {v2, p1, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 214
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 215
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 218
    :cond_0
    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 220
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 222
    new-array v1, v7, [Ljava/lang/String;

    const-string v3, "_data"

    aput-object v3, v1, v5

    const-string v3, "bucket_id"

    aput-object v3, v1, v6

    .line 223
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_display_name = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 224
    invoke-direct {p0, v2, v1, v0, p1}, Lcom/sec/chaton/multimedia/doc/e;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 228
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 230
    new-array v0, v7, [Ljava/lang/String;

    const-string v3, "_data"

    aput-object v3, v0, v5

    const-string v3, "_id"

    aput-object v3, v0, v6

    .line 231
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_id = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 233
    invoke-direct {p0, v2, v0, v1}, Lcom/sec/chaton/multimedia/doc/e;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_2
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 239
    .line 241
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v0, "/"

    invoke-direct {v2, p1, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 242
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 243
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 246
    :cond_0
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 248
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 250
    new-array v1, v7, [Ljava/lang/String;

    const-string v3, "_data"

    aput-object v3, v1, v5

    const-string v3, "bucket_id"

    aput-object v3, v1, v6

    .line 251
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_display_name = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 252
    invoke-direct {p0, v2, v1, v0, p1}, Lcom/sec/chaton/multimedia/doc/e;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 256
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 258
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "_data"

    aput-object v3, v0, v5

    const-string v3, "_id"

    aput-object v3, v0, v6

    const-string v3, "orientation"

    aput-object v3, v0, v7

    .line 259
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_id = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 261
    invoke-direct {p0, v2, v0, v1}, Lcom/sec/chaton/multimedia/doc/e;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_2
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/multimedia/doc/g;Lcom/sec/chaton/multimedia/doc/g;)I
    .locals 2

    .prologue
    .line 338
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v1, :cond_2

    invoke-virtual {p2}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v1, :cond_2

    .line 339
    :cond_1
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/doc/g;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/chaton/multimedia/doc/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 341
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v1, :cond_3

    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected varargs a([Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/doc/g;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 170
    aget-object v1, p1, v0

    .line 171
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 172
    sget-boolean v3, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v3, :cond_0

    .line 173
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fullPath: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 177
    invoke-direct {p0, v1}, Lcom/sec/chaton/multimedia/doc/e;->b(Ljava/lang/String;)V

    .line 178
    invoke-direct {p0, v1}, Lcom/sec/chaton/multimedia/doc/e;->a(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 181
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 182
    if-eqz v1, :cond_5

    .line 183
    array-length v3, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    .line 184
    iget-object v5, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v5, v4}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;Ljava/io/File;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v5

    .line 185
    sget-object v6, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v5, v6, :cond_1

    .line 186
    new-instance v6, Lcom/sec/chaton/multimedia/doc/g;

    invoke-direct {v6}, Lcom/sec/chaton/multimedia/doc/g;-><init>()V

    .line 187
    invoke-virtual {v6, v5}, Lcom/sec/chaton/multimedia/doc/g;->a(Lcom/sec/chaton/multimedia/doc/b;)V

    .line 188
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/sec/chaton/multimedia/doc/g;->a(Ljava/lang/String;)V

    .line 189
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/multimedia/doc/g;->a(J)V

    .line 190
    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/sec/chaton/multimedia/doc/g;->b(J)V

    .line 191
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/sec/chaton/multimedia/doc/g;->b(Ljava/lang/String;)V

    .line 192
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/chaton/multimedia/doc/g;

    .line 197
    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v4, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    if-eq v0, v4, :cond_4

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v4, Lcom/sec/chaton/multimedia/doc/b;->l:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v4, :cond_3

    .line 198
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/doc/g;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 199
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/doc/g;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/doc/j;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/doc/g;->a(Lcom/sec/chaton/multimedia/doc/j;)V

    goto :goto_1

    .line 205
    :cond_5
    invoke-static {v2, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 206
    return-object v2
.end method

.method protected a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/doc/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->g(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    .line 329
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->a(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->h(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Lcom/sec/chaton/multimedia/doc/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/h;->notifyDataSetChanged()V

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->i(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 333
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 334
    return-void
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 155
    check-cast p1, Lcom/sec/chaton/multimedia/doc/g;

    check-cast p2, Lcom/sec/chaton/multimedia/doc/g;

    invoke-virtual {p0, p1, p2}, Lcom/sec/chaton/multimedia/doc/e;->a(Lcom/sec/chaton/multimedia/doc/g;Lcom/sec/chaton/multimedia/doc/g;)I

    move-result v0

    return v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 155
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/doc/e;->a([Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 155
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/doc/e;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->g(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->a:Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;->g(Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 163
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/e;->b:Ljava/util/HashMap;

    .line 165
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 166
    return-void
.end method
