.class public Lcom/sec/chaton/multimedia/audio/b;
.super Ljava/lang/Object;
.source "MediaPlayerManager.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/sec/chaton/multimedia/audio/b;


# instance fields
.field private c:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/audio/h;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/chaton/multimedia/audio/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/audio/b;->a:Ljava/lang/String;

    .line 30
    new-instance v0, Lcom/sec/chaton/multimedia/audio/b;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/audio/b;-><init>()V

    sput-object v0, Lcom/sec/chaton/multimedia/audio/b;->b:Lcom/sec/chaton/multimedia/audio/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 41
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->e:Ljava/util/HashMap;

    .line 34
    return-void
.end method

.method public static a()Lcom/sec/chaton/multimedia/audio/b;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/chaton/multimedia/audio/b;->b:Lcom/sec/chaton/multimedia/audio/b;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/b;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method private a(Landroid/database/Cursor;ILandroid/os/Handler;Lcom/sec/chaton/e/r;)V
    .locals 7

    .prologue
    .line 77
    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 79
    new-instance v0, Lcom/sec/chaton/multimedia/audio/c;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/audio/c;-><init>(Lcom/sec/chaton/multimedia/audio/b;Landroid/database/Cursor;ILandroid/os/Handler;Lcom/sec/chaton/e/r;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 118
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 165
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/h;

    .line 167
    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "messageId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/audio/b;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 172
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_1

    .line 173
    new-array v1, v4, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v4, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/audio/h;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 179
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/audio/b;->b()V

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 191
    sget-object v1, Lcom/sec/chaton/multimedia/audio/b;->a:Ljava/lang/String;

    monitor-enter v1

    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/h;

    .line 194
    if-eqz v0, :cond_0

    .line 195
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/sec/chaton/multimedia/audio/h;->cancel(Z)Z

    .line 196
    iget-object v3, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/h;

    .line 155
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->c()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 156
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/h;->cancel(Z)Z

    .line 157
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 161
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/b;->e()V

    .line 162
    return-void
.end method

.method public a(JLandroid/view/View;)V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->e:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/String;JZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 122
    invoke-static {}, Lcom/sec/chaton/util/am;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/util/am;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 123
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00dd

    invoke-static {v0, v1, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 146
    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    invoke-virtual {p0, p3, p4}, Lcom/sec/chaton/multimedia/audio/b;->e(J)Lcom/sec/chaton/multimedia/audio/h;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_3

    .line 130
    invoke-virtual {v0, p2}, Lcom/sec/chaton/multimedia/audio/h;->b(Ljava/lang/String;)V

    .line 136
    :goto_1
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->c()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/multimedia/audio/b;->b(J)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_1

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/audio/b;->f:Z

    if-nez v1, :cond_1

    .line 137
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "messageId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/audio/b;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_4

    .line 140
    new-array v1, v6, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 132
    :cond_3
    new-instance v0, Lcom/sec/chaton/multimedia/audio/h;

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/audio/h;-><init>(Landroid/view/View;Ljava/lang/String;JZ)V

    .line 133
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 142
    :cond_4
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v6, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/audio/h;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public a(Lcom/sec/chaton/multimedia/audio/h;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 150
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/b;->e()V

    .line 151
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->d:Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    return-void
.end method

.method public a(Ljava/lang/String;JLandroid/view/View;ZLandroid/database/Cursor;ILandroid/os/Handler;Lcom/sec/chaton/e/r;)V
    .locals 1

    .prologue
    .line 71
    invoke-virtual/range {p0 .. p5}, Lcom/sec/chaton/multimedia/audio/b;->a(Ljava/lang/String;JLandroid/view/View;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    invoke-direct {p0, p6, p7, p8, p9}, Lcom/sec/chaton/multimedia/audio/b;->a(Landroid/database/Cursor;ILandroid/os/Handler;Lcom/sec/chaton/e/r;)V

    .line 74
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 275
    iput-boolean p1, p0, Lcom/sec/chaton/multimedia/audio/b;->f:Z

    .line 276
    if-nez p1, :cond_0

    .line 277
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/b;->e()V

    .line 279
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;JLandroid/view/View;Z)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 47
    invoke-virtual {p0, p2, p3}, Lcom/sec/chaton/multimedia/audio/b;->b(J)Z

    move-result v6

    .line 48
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/b;->f()V

    .line 50
    if-nez v6, :cond_0

    .line 51
    new-instance v0, Lcom/sec/chaton/multimedia/audio/h;

    move-object v1, p4

    move-object v2, p1

    move-wide v3, p2

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/audio/h;-><init>(Landroid/view/View;Ljava/lang/String;JZ)V

    .line 52
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->c()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/multimedia/audio/b;->b(J)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/audio/b;->f:Z

    if-nez v1, :cond_0

    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "messageId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/audio/b;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_1

    .line 57
    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/audio/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 65
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "messageId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "isPlaying: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/audio/b;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    return v6

    .line 59
    :cond_1
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v7, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/chaton/multimedia/audio/h;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/b;->f()V

    .line 188
    return-void
.end method

.method public b(J)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/h;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->c()J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    .line 206
    const/4 v0, 0x1

    .line 210
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 254
    return-void
.end method

.method public c(J)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/h;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->c()J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/h;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v2, :cond_0

    .line 216
    const/4 v0, 0x1

    .line 219
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 268
    return-void
.end method

.method public d(J)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/h;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->c()J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/h;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/h;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const/4 v0, 0x1

    .line 229
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public e(J)Lcom/sec/chaton/multimedia/audio/h;
    .locals 4

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/b;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/audio/h;

    .line 237
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/audio/h;->c()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 243
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
