.class public Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "FileExplorerActivity.java"


# static fields
.field private static b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/multimedia/doc/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/sec/chaton/multimedia/doc/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 141
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->b:Ljava/util/HashMap;

    .line 142
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c:Ljava/util/HashMap;

    .line 150
    const-string v0, "pdf"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->c:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "pdf"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 151
    const-string v0, "gul"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->d:Lcom/sec/chaton/multimedia/doc/b;

    const-string v2, "application/jungumword"

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 152
    const-string v0, "hwp"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->e:Lcom/sec/chaton/multimedia/doc/b;

    const-string v2, "application/x-hwp"

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 153
    const-string v0, "ppt"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->f:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "ppt"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 154
    const-string v0, "pptx"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->f:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "pptx"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 155
    const-string v0, "doc"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->g:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "doc"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 156
    const-string v0, "docx"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->g:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "docx"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 157
    const-string v0, "rtf"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->g:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "rtf"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 158
    const-string v0, "xls"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->h:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "xls"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 159
    const-string v0, "xlsx"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->h:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "xlsx"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 160
    const-string v0, "txt"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->i:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "txt"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 161
    const-string v0, "snb"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->j:Lcom/sec/chaton/multimedia/doc/b;

    const-string v2, "application/snb"

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 162
    const-string v0, "scc"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->m:Lcom/sec/chaton/multimedia/doc/b;

    const-string v2, "application/scc"

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 164
    const-string v0, "jpg"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "jpg"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 165
    const-string v0, "jpeg"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "jpeg"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 166
    const-string v0, "png"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "png"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 167
    const-string v0, "gif"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "gif"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 168
    const-string v0, "mp4"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->l:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "mp4"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 169
    const-string v0, "3gp"

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->l:Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const-string v3, "3gp"

    invoke-virtual {v2, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    .line 80
    return-void
.end method

.method public static a(Lcom/sec/chaton/multimedia/doc/b;)Lcom/sec/chaton/e/w;
    .locals 2

    .prologue
    .line 221
    sget-object v0, Lcom/sec/chaton/multimedia/doc/a;->a:[I

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/doc/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 241
    sget-object v0, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    :goto_0
    return-object v0

    .line 228
    :pswitch_0
    sget-object v0, Lcom/sec/chaton/e/w;->j:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 233
    :pswitch_1
    sget-object v0, Lcom/sec/chaton/e/w;->m:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 236
    :pswitch_2
    sget-object v0, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 239
    :pswitch_3
    sget-object v0, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    goto :goto_0

    .line 221
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 173
    if-nez p0, :cond_0

    .line 174
    const/4 v0, 0x0

    .line 177
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->b:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/b;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    return-void
.end method

.method public static b(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;
    .locals 2

    .prologue
    .line 183
    if-eqz p0, :cond_0

    sget-object v0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 184
    :cond_0
    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    .line 187
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->c:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/doc/b;

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;
    .locals 3

    .prologue
    .line 193
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 194
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v0, "."

    invoke-direct {v1, p0, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const/4 v0, 0x0

    .line 196
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 197
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 200
    :cond_0
    invoke-static {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->b(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    .line 202
    :goto_1
    return-object v0

    :cond_1
    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    goto :goto_1
.end method

.method public static c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 210
    sget-object v0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 211
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 215
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 67
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 70
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->startActivity(Landroid/content/Intent;)V

    .line 76
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/doc/FileExplorerFragment;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a:Lcom/sec/chaton/multimedia/doc/c;

    .line 35
    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a:Lcom/sec/chaton/multimedia/doc/c;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a:Lcom/sec/chaton/multimedia/doc/c;

    invoke-interface {v0}, Lcom/sec/chaton/multimedia/doc/c;->a()V

    .line 51
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 247
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 248
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 251
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    if-eqz p1, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/doc/c;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->a:Lcom/sec/chaton/multimedia/doc/c;

    .line 44
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/doc/FileExplorerActivity;->d()V

    .line 56
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 59
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 63
    :cond_0
    return-void
.end method
