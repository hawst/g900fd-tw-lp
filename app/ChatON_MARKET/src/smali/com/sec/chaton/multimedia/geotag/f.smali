.class Lcom/sec/chaton/multimedia/geotag/f;
.super Ljava/lang/Object;
.source "GeotagActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/sec/chaton/multimedia/geotag/f;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 428
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 429
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/f;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->c(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 431
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/f;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->finish()V

    .line 455
    :goto_0
    return-void

    .line 443
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/f;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->d(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 444
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/f;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->d(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    .line 445
    if-eqz v0, :cond_2

    .line 446
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/f;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-virtual {v0}, Landroid/location/LocationProvider;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 452
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/f;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->c(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00e0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 453
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/f;->a:Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->finish()V

    goto :goto_0
.end method
