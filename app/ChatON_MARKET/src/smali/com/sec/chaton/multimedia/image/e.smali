.class Lcom/sec/chaton/multimedia/image/e;
.super Ljava/lang/Object;
.source "ImageEffectFragment.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/e;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 257
    if-eqz p2, :cond_0

    .line 258
    const v0, 0x7f070327

    :try_start_0
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020298

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 259
    const v0, 0x7f070328

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/e;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 269
    :goto_0
    return-void

    .line 261
    :cond_0
    const v0, 0x7f070327

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 262
    const v0, 0x7f070328

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/e;->a:Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->b(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 266
    :catch_1
    move-exception v0

    .line 267
    invoke-static {}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
