.class public final enum Lcom/sec/chaton/multimedia/doc/b;
.super Ljava/lang/Enum;
.source "FileExplorerActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/chaton/multimedia/doc/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum b:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum c:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum d:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum e:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum f:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum g:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum h:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum i:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum j:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum k:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum l:Lcom/sec/chaton/multimedia/doc/b;

.field public static final enum m:Lcom/sec/chaton/multimedia/doc/b;

.field private static final synthetic o:[Lcom/sec/chaton/multimedia/doc/b;


# instance fields
.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 81
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    .line 82
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "DIRECTORY"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    .line 84
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "PDF"

    invoke-direct {v0, v1, v6, v5}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->c:Lcom/sec/chaton/multimedia/doc/b;

    .line 85
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "GUL"

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->d:Lcom/sec/chaton/multimedia/doc/b;

    .line 86
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "HWP"

    invoke-direct {v0, v1, v8, v7}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->e:Lcom/sec/chaton/multimedia/doc/b;

    .line 87
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "PPT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->f:Lcom/sec/chaton/multimedia/doc/b;

    .line 88
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "DOC"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->g:Lcom/sec/chaton/multimedia/doc/b;

    .line 89
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "XLS"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->h:Lcom/sec/chaton/multimedia/doc/b;

    .line 90
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "TXT"

    const/16 v2, 0x8

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->i:Lcom/sec/chaton/multimedia/doc/b;

    .line 91
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "SNB"

    const/16 v2, 0x9

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->j:Lcom/sec/chaton/multimedia/doc/b;

    .line 92
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "IMAGE"

    const/16 v2, 0xa

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    .line 93
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "VIDEO"

    const/16 v2, 0xb

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->l:Lcom/sec/chaton/multimedia/doc/b;

    .line 94
    new-instance v0, Lcom/sec/chaton/multimedia/doc/b;

    const-string v1, "SCC"

    const/16 v2, 0xc

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/chaton/multimedia/doc/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->m:Lcom/sec/chaton/multimedia/doc/b;

    .line 80
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/sec/chaton/multimedia/doc/b;

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->a:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->c:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->d:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/chaton/multimedia/doc/b;->e:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->f:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->g:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->h:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->i:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->j:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->k:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->l:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->m:Lcom/sec/chaton/multimedia/doc/b;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/chaton/multimedia/doc/b;->o:[Lcom/sec/chaton/multimedia/doc/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 99
    iput p3, p0, Lcom/sec/chaton/multimedia/doc/b;->n:I

    .line 100
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/chaton/multimedia/doc/b;
    .locals 1

    .prologue
    .line 80
    const-class v0, Lcom/sec/chaton/multimedia/doc/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/doc/b;

    return-object v0
.end method

.method public static values()[Lcom/sec/chaton/multimedia/doc/b;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/sec/chaton/multimedia/doc/b;->o:[Lcom/sec/chaton/multimedia/doc/b;

    invoke-virtual {v0}, [Lcom/sec/chaton/multimedia/doc/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/chaton/multimedia/doc/b;

    return-object v0
.end method
