.class public Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;
.super Landroid/support/v4/app/Fragment;
.source "PostONImagePagerFragment.java"

# interfaces
.implements Lcom/sec/chaton/multimedia/image/at;


# static fields
.field private static final b:Ljava/lang/String;

.field private static i:Ljava/lang/String;

.field private static j:Ljava/lang/String;

.field private static k:Ljava/lang/String;

.field private static l:Ljava/lang/String;


# instance fields
.field private A:Ljava/io/File;

.field private B:Ljava/lang/String;

.field private C:Lcom/sec/chaton/widget/c;

.field private D:Lcom/sec/common/f/c;

.field private E:Ljava/lang/String;

.field private F:Landroid/app/Activity;

.field private G:Landroid/widget/FrameLayout;

.field private H:Landroid/widget/ImageView;

.field private I:Landroid/view/View;

.field private J:Landroid/view/ViewTreeObserver;

.field private K:I

.field private L:I

.field private M:Landroid/os/Handler;

.field private N:Landroid/view/View$OnClickListener;

.field private O:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field a:Landroid/view/View$OnClickListener;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private m:I

.field private n:Lcom/sec/chaton/multimedia/image/ar;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/ImageButton;

.field private q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-class v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->b:Ljava/lang/String;

    .line 55
    const-string v0, "1"

    sput-object v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->i:Ljava/lang/String;

    .line 56
    const-string v0, "2"

    sput-object v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->j:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/poston/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->k:Ljava/lang/String;

    .line 58
    const-string v0, "pager"

    sput-object v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->l:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 99
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 48
    const-string v0, "postonId"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c:Ljava/lang/String;

    .line 49
    const-string v0, "downloadUri"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->d:Ljava/lang/String;

    .line 50
    const-string v0, "msgType"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->e:Ljava/lang/String;

    .line 51
    const-string v0, "content"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->f:Ljava/lang/String;

    .line 52
    const-string v0, "sender"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->g:Ljava/lang/String;

    .line 53
    const-string v0, "sequence"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->h:Ljava/lang/String;

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&imei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v1

    const-string v2, "imei"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->E:Ljava/lang/String;

    .line 227
    new-instance v0, Lcom/sec/chaton/multimedia/image/ak;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/ak;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->a:Landroid/view/View$OnClickListener;

    .line 345
    new-instance v0, Lcom/sec/chaton/multimedia/image/am;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/am;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->M:Landroid/os/Handler;

    .line 487
    new-instance v0, Lcom/sec/chaton/multimedia/image/ao;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/ao;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->N:Landroid/view/View$OnClickListener;

    .line 574
    new-instance v0, Lcom/sec/chaton/multimedia/image/ap;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/ap;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->O:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 101
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;I)I
    .locals 0

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->K:I

    return p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;Ljava/io/File;)Ljava/io/File;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->A:Ljava/io/File;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->z:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;I)I
    .locals 0

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->L:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/io/File;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->A:Ljava/io/File;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->g()V

    return-void
.end method

.method static synthetic g(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Lcom/sec/chaton/multimedia/image/ar;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->n:Lcom/sec/chaton/multimedia/image/ar;

    return-object v0
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    if-nez v0, :cond_0

    .line 320
    new-instance v0, Lcom/sec/chaton/widget/c;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->F:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/chaton/widget/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    .line 321
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    const v1, 0x7f0b0226

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setTitle(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    const v1, 0x7f0b01ce

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/c;->setMessage(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/c;->setCancelable(Z)V

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/chaton/multimedia/image/al;

    invoke-direct {v3, p0}, Lcom/sec/chaton/multimedia/image/al;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/chaton/widget/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->show()V

    .line 332
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0, v4}, Lcom/sec/chaton/widget/c;->a(I)V

    .line 335
    :try_start_0
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->M:Landroid/os/Handler;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->E:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->A:Ljava/io/File;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    :cond_1
    :goto_0
    return-void

    .line 337
    :catch_0
    move-exception v0

    .line 338
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_1

    .line 339
    sget-object v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->M:Landroid/os/Handler;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->J:Landroid/view/ViewTreeObserver;

    .line 571
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->J:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->O:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 572
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Lcom/sec/chaton/widget/c;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->K:I

    return v0
.end method

.method static synthetic m(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->I:Landroid/view/View;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->L:I

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 6

    .prologue
    .line 200
    sget-object v5, Lcom/sec/chaton/poston/bb;->d:Ljava/lang/String;

    .line 202
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 204
    if-lez v0, :cond_0

    .line 205
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 206
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->y:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 209
    sget-object v5, Lcom/sec/chaton/poston/bb;->e:Ljava/lang/String;

    .line 220
    :cond_0
    new-instance v0, Lcom/sec/chaton/poston/bb;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->B:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->t:Ljava/lang/String;

    sget-object v4, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    sget-object v4, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->l:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/poston/bb;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 221
    sget-boolean v1, Lcom/sec/chaton/util/y;->a:Z

    if-eqz v1, :cond_1

    .line 222
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "downloadMultimedia() mDownUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->D:Lcom/sec/common/f/c;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 225
    return-void
.end method

.method public b()Lcom/sec/chaton/multimedia/image/ar;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->n:Lcom/sec/chaton/multimedia/image/ar;

    return-object v0
.end method

.method protected c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->F:Landroid/app/Activity;

    check-cast v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    .line 526
    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 560
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 561
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->b()Lcom/sec/chaton/multimedia/image/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/ar;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f()V

    .line 565
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->F:Landroid/app/Activity;

    .line 532
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 533
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 389
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 392
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->I:Landroid/view/View;

    .line 394
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->h()V

    .line 396
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->K:I

    .line 397
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->L:I

    .line 401
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->m:I

    .line 402
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "buddyId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->B:Ljava/lang/String;

    .line 403
    new-instance v0, Lcom/sec/chaton/multimedia/image/ar;

    invoke-direct {v0, p0, p0}, Lcom/sec/chaton/multimedia/image/ar;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;Lcom/sec/chaton/multimedia/image/at;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->n:Lcom/sec/chaton/multimedia/image/ar;

    .line 404
    return-void

    .line 401
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 109
    const v0, 0x7f0300b4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 110
    const v0, 0x7f070331

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->G:Landroid/widget/FrameLayout;

    .line 111
    const v0, 0x7f070332

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    .line 113
    const v0, 0x7f070333

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->p:Landroid/widget/ImageButton;

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->B:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->y:Ljava/lang/String;

    .line 117
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->D:Lcom/sec/common/f/c;

    .line 122
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->c()Ljava/util/HashMap;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->c()Ljava/util/HashMap;

    move-result-object v2

    iget v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/aj;

    .line 125
    if-eqz v0, :cond_1

    .line 127
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_0

    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Item]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/aj;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_0
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/aj;->a:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->s:Ljava/lang/String;

    .line 133
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/aj;->c:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->t:Ljava/lang/String;

    .line 134
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/aj;->d:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    .line 135
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/aj;->e:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->v:Ljava/lang/String;

    .line 136
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/aj;->f:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->w:Ljava/lang/String;

    .line 137
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/aj;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->x:Ljava/lang/String;

    .line 165
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 166
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->s:Ljava/lang/String;

    invoke-virtual {v0, v2, p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a(Ljava/lang/String;Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V

    .line 169
    :cond_2
    return-object v1

    .line 140
    :cond_3
    if-eqz p3, :cond_1

    .line 142
    const-string v0, "postonId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->s:Ljava/lang/String;

    .line 143
    const-string v0, "downloadUri"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    .line 144
    const-string v0, "msgType"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->t:Ljava/lang/String;

    .line 145
    const-string v0, "content"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->v:Ljava/lang/String;

    .line 146
    const-string v0, "sender"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->w:Ljava/lang/String;

    .line 147
    const-string v0, "sequence"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->x:Ljava/lang/String;

    .line 149
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPostonNo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->s:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", mMetaType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->t:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", mDownUri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", mContent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->v:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", mSender: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->w:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", mSequence: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[restore from onSaveInstanceState]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 432
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 433
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->M:Landroid/os/Handler;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->E:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;)Z

    .line 435
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->C:Lcom/sec/chaton/widget/c;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/c;->dismiss()V

    .line 437
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 543
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 545
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 546
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d()Lcom/sec/common/f/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 547
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a(Ljava/lang/String;)V

    .line 549
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->H:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->H:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 551
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->c()Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d()Lcom/sec/common/f/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->H:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    .line 555
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 556
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 537
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->F:Landroid/app/Activity;

    .line 538
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 539
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 412
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 413
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 415
    if-eqz v0, :cond_0

    .line 416
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->H:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->H:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 422
    if-eqz v0, :cond_1

    .line 423
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 428
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->a()V

    .line 445
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->p:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 447
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    const-string v1, "file:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->t:Ljava/lang/String;

    sget-object v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->p:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 460
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 465
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 466
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 467
    new-instance v1, Lcom/sec/chaton/multimedia/image/an;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/image/an;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V

    iput-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 476
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 479
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 480
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 484
    :cond_2
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 485
    return-void

    .line 462
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->n:Lcom/sec/chaton/multimedia/image/ar;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 175
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPostonNo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", mediaType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", sender: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", content: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", downloadUri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", mSequence: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onSaveInstanceState]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_0
    const-string v0, "downloadUri"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v0, "msgType"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v0, "postonId"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v0, "content"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v0, "sender"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v0, "sequence"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 197
    return-void
.end method
