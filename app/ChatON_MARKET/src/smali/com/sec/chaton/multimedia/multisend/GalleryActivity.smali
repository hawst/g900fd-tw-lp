.class public Lcom/sec/chaton/multimedia/multisend/GalleryActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "GalleryActivity.java"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:Z

.field private c:Landroid/content/BroadcastReceiver;

.field private d:Lcom/sec/chaton/multimedia/multisend/GalleryView;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/multisend/GalleryActivity;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->e()V

    return-void
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 161
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/f;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/multisend/f;-><init>(Lcom/sec/chaton/multimedia/multisend/GalleryActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->c:Landroid/content/BroadcastReceiver;

    .line 168
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 169
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 170
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 171
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 172
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 173
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->e()V

    .line 174
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 177
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 178
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->a:Z

    if-nez v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 180
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->a:Z

    .line 182
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->finish()V

    .line 185
    :cond_1
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 203
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/GalleryView;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/multisend/GalleryView;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->d:Lcom/sec/chaton/multimedia/multisend/GalleryView;

    .line 54
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->d:Lcom/sec/chaton/multimedia/multisend/GalleryView;

    return-object v0
.end method

.method public a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/k;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/k;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v10, 0x0

    .line 61
    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v3

    const-string v0, "_id"

    aput-object v0, v2, v1

    const/4 v0, 0x2

    const-string v1, "bucket_id"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "orientation"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "bucket_display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "count(*) AS Num"

    aput-object v1, v2, v0

    .line 67
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 71
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "datetaken DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v1

    .line 75
    if-eqz v1, :cond_6

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 77
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 79
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 82
    if-eqz v7, :cond_4

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->e:Z

    .line 84
    const v0, 0x7f0b0240

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 85
    const-string v8, ""

    .line 86
    const-string v0, "orientation"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 88
    const-string v0, "bucket_display_name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 91
    new-instance v3, Lcom/sec/chaton/multimedia/multisend/k;

    invoke-direct/range {v3 .. v9}, Lcom/sec/chaton/multimedia/multisend/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    :goto_0
    if-eqz v1, :cond_0

    .line 101
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 107
    :cond_0
    :try_start_2
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->e:Z

    if-nez v0, :cond_2

    .line 108
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "1) GROUP BY (bucket_id"

    const/4 v4, 0x0

    const-string v5, "bucket_display_name"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 113
    if-eqz v10, :cond_2

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    :cond_1
    const-string v0, "_data"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 124
    const-string v0, "_id"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 125
    const-string v0, "bucket_display_name"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 126
    const-string v0, "bucket_id"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 127
    const-string v0, "orientation"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 128
    const-string v0, "bucket_display_name"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 129
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/k;

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/multimedia/multisend/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    .line 135
    :cond_2
    if-eqz v10, :cond_3

    .line 136
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 141
    :cond_3
    return-object p1

    .line 94
    :cond_4
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->e:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_5

    .line 101
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 100
    :cond_5
    throw v0

    .line 97
    :cond_6
    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->e:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 135
    :catchall_1
    move-exception v0

    if-eqz v10, :cond_7

    .line 136
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 135
    :cond_7
    throw v0

    .line 100
    :catchall_2
    move-exception v0

    move-object v1, v10

    goto :goto_1
.end method

.method public c(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 145
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->setResult(ILandroid/content/Intent;)V

    .line 146
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->finish()V

    .line 147
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 189
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 190
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 191
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 197
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onPause()V

    .line 198
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->f()V

    .line 199
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 154
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 156
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->d()V

    .line 157
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 158
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 215
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 222
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 218
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/GalleryActivity;->finish()V

    goto :goto_0

    .line 215
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
