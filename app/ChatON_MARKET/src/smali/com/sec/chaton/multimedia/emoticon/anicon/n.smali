.class public Lcom/sec/chaton/multimedia/emoticon/anicon/n;
.super Lcom/sec/common/f/a;
.source "RemoteImageResourceDispatcherTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const v0, 0x7fffffff

    .line 50
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;II)V

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;-><init>(Ljava/lang/String;IIZ)V

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIZ)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 60
    iput p2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->c:I

    .line 61
    iput p3, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->d:I

    .line 62
    iput-boolean p4, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->e:Z

    .line 65
    const v0, 0x7f0203cb

    iput v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->a:I

    .line 66
    const v0, 0x7f020117

    iput v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->b:I

    .line 67
    return-void
.end method

.method private a(Landroid/widget/ImageView;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 4

    .prologue
    const/high16 v3, 0x41a00000    # 20.0f

    .line 169
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 170
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 172
    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 174
    if-eqz p2, :cond_0

    .line 176
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 177
    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 181
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v3}, Lcom/sec/chaton/util/an;->c(F)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 188
    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 190
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    invoke-virtual {p1}, Landroid/widget/ImageView;->invalidate()V

    .line 194
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 3

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->e()Landroid/widget/ImageView;

    move-result-object v1

    .line 89
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->a:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 90
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 92
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 94
    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    .line 95
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 98
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->b:I

    .line 83
    return-void
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 128
    .line 137
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->e()Landroid/widget/ImageView;

    move-result-object v0

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 139
    if-eqz p1, :cond_1

    .line 140
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->f()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 141
    invoke-virtual {v0, v5}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    .line 142
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->e:Z

    if-eqz v1, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->e()Landroid/widget/ImageView;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->a(Landroid/widget/ImageView;Landroid/graphics/drawable/BitmapDrawable;)V

    .line 151
    :cond_0
    :goto_0
    if-eqz p2, :cond_2

    .line 152
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->e()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 161
    :goto_1
    return-void

    .line 148
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 154
    :cond_2
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 156
    new-instance v2, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    aput-object v1, v3, v4

    aput-object v0, v3, v5

    invoke-direct {v2, v3}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->e()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 159
    const/16 v0, 0x64

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->e()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 105
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->e()Landroid/widget/ImageView;

    move-result-object v0

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 106
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p0, v0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->a(Ljava/util/concurrent/Callable;J)V

    .line 107
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->k()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/util/a/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 112
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 115
    :try_start_0
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/util/a/a;->a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 117
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->k()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->c:I

    iget v3, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->d:I

    invoke-static {v0, v2, v1, v3}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 118
    const/16 v1, 0xa0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setDensity(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-object v0

    .line 121
    :catch_0
    move-exception v0

    .line 122
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 202
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->a(Landroid/view/View;)V

    .line 204
    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 207
    :cond_0
    return-void
.end method

.method public e()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 211
    invoke-super {p0}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public f()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 216
    invoke-super {p0}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Landroid/view/View;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/n;->e()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method
