.class Lcom/sec/chaton/multimedia/audio/u;
.super Ljava/lang/Object;
.source "VoiceListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;


# direct methods
.method constructor <init>(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/u;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/u;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/u;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/u;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/u;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/u;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/database/Cursor;

    move-result-object v1

    const-string v2, "_data"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 74
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send voice: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/u;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v1}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->b(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 79
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 80
    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 81
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/u;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->b(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/app/Activity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 82
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/u;->a:Lcom/sec/chaton/multimedia/audio/VoiceListFragment;

    invoke-static {v0}, Lcom/sec/chaton/multimedia/audio/VoiceListFragment;->b(Lcom/sec/chaton/multimedia/audio/VoiceListFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 86
    :cond_1
    return-void
.end method
