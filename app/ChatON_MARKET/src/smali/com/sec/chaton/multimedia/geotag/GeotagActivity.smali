.class public Lcom/sec/chaton/multimedia/geotag/GeotagActivity;
.super Lcom/sec/common/actionbar/ActionBarMapActivity;
.source "GeotagActivity.java"

# interfaces
.implements Landroid/location/LocationListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private A:Landroid/os/Handler;

.field private B:Lcom/sec/common/a/d;

.field private C:Z

.field private D:Landroid/location/Location;

.field private E:Ljava/lang/String;

.field private F:Lcom/sec/chaton/multimedia/geotag/m;

.field private G:Z

.field public final a:Landroid/os/Handler;

.field private c:Landroid/content/Context;

.field private d:Landroid/os/Bundle;

.field private e:Landroid/app/Dialog;

.field private f:Lcom/sec/chaton/widget/ClearableEditText;

.field private g:Landroid/widget/ImageButton;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Lcom/google/android/maps/MapView;

.field private j:Lcom/google/android/maps/MapController;

.field private k:Landroid/location/LocationManager;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/maps/Overlay;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/sec/chaton/multimedia/geotag/a;

.field private n:Lcom/google/android/maps/GeoPoint;

.field private o:Lcom/google/android/maps/GeoPoint;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Lcom/google/android/maps/GeoPoint;

.field private t:Ljava/lang/String;

.field private u:Landroid/widget/ImageButton;

.field private v:Landroid/widget/ImageButton;

.field private w:Landroid/view/MenuItem;

.field private x:Landroid/view/View;

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 65
    invoke-direct {p0}, Lcom/sec/common/actionbar/ActionBarMapActivity;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k:Landroid/location/LocationManager;

    .line 97
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->p:Z

    .line 98
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->q:Z

    .line 100
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->r:Z

    .line 873
    new-instance v0, Lcom/sec/chaton/multimedia/geotag/j;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/geotag/j;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a:Landroid/os/Handler;

    .line 1045
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Lcom/google/android/maps/GeoPoint;)Lcom/google/android/maps/GeoPoint;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->n:Lcom/google/android/maps/GeoPoint;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/google/android/maps/MapController;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->j:Lcom/google/android/maps/MapController;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Lcom/sec/common/a/d;)Lcom/sec/common/a/d;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->B:Lcom/sec/common/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->t:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/location/Location;)V
    .locals 7

    .prologue
    const-wide v5, 0x412e848000000000L    # 1000000.0

    .line 542
    const/4 v0, 0x0

    .line 546
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->p:Z

    if-eqz v1, :cond_0

    .line 547
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    .line 548
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    .line 550
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    mul-double/2addr v1, v5

    double-to-int v1, v1

    mul-double v2, v3, v5

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 551
    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->n:Lcom/google/android/maps/GeoPoint;

    .line 552
    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->o:Lcom/google/android/maps/GeoPoint;

    .line 556
    :cond_0
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->r:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->y:Z

    if-nez v1, :cond_2

    .line 557
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->j:Lcom/google/android/maps/MapController;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 560
    new-instance v1, Lcom/google/android/maps/OverlayItem;

    const-string v2, "test"

    const-string v3, "Current location"

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m:Lcom/sec/chaton/multimedia/geotag/a;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/geotag/a;->b(Lcom/google/android/maps/OverlayItem;)V

    .line 562
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m:Lcom/sec/chaton/multimedia/geotag/a;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/geotag/a;->a(Lcom/google/android/maps/OverlayItem;)V

    .line 564
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 566
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m:Lcom/sec/chaton/multimedia/geotag/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 572
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 573
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 576
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->r:Z

    .line 577
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->w:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 579
    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Z)Z
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->y:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;Lcom/google/android/maps/GeoPoint;)Lcom/google/android/maps/GeoPoint;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->s:Lcom/google/android/maps/GeoPoint;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/google/android/maps/MapView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->i:Lcom/google/android/maps/MapView;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/location/LocationManager;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k:Landroid/location/LocationManager;

    return-object v0
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 177
    const v0, 0x7f07031c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->x:Landroid/view/View;

    .line 178
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->x:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f:Lcom/sec/chaton/widget/ClearableEditText;

    .line 179
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f:Lcom/sec/chaton/widget/ClearableEditText;

    const v1, 0x7f0b0010

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setHint(I)V

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f:Lcom/sec/chaton/widget/ClearableEditText;

    const v1, 0x10000003

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setImeOptions(I)V

    .line 181
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f:Lcom/sec/chaton/widget/ClearableEditText;

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setInputType(I)V

    .line 193
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f:Lcom/sec/chaton/widget/ClearableEditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    new-instance v2, Lcom/sec/chaton/util/w;

    const/16 v3, 0x1e

    invoke-direct {v2, p0, v3}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 195
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->x:Landroid/view/View;

    const v1, 0x7f0702d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->g:Landroid/widget/ImageButton;

    .line 196
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->g:Landroid/widget/ImageButton;

    const v1, 0x7f0202b8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 197
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    const v0, 0x7f07031d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/MapView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->i:Lcom/google/android/maps/MapView;

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->i:Lcom/google/android/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->j:Lcom/google/android/maps/MapController;

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->j:Lcom/google/android/maps/MapController;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 219
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->i:Lcom/google/android/maps/MapView;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/MapView;->setBuiltInZoomControls(Z)V

    .line 220
    const v0, 0x7f07031f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->u:Landroid/widget/ImageButton;

    .line 221
    const v0, 0x7f07031e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->v:Landroid/widget/ImageButton;

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->u:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/multimedia/geotag/b;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/geotag/b;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->v:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/chaton/multimedia/geotag/c;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/geotag/c;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 266
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->i:Lcom/google/android/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->l:Ljava/util/List;

    .line 268
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->h:Landroid/graphics/drawable/Drawable;

    .line 270
    new-instance v0, Lcom/sec/chaton/multimedia/geotag/a;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->h:Landroid/graphics/drawable/Drawable;

    invoke-direct {v0, v1, p0}, Lcom/sec/chaton/multimedia/geotag/a;-><init>(Landroid/graphics/drawable/Drawable;Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m:Lcom/sec/chaton/multimedia/geotag/a;

    .line 285
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f:Lcom/sec/chaton/widget/ClearableEditText;

    new-instance v1, Lcom/sec/chaton/multimedia/geotag/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/geotag/d;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 303
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k:Landroid/location/LocationManager;

    .line 305
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->d:Landroid/os/Bundle;

    .line 306
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->d:Landroid/os/Bundle;

    const-string v1, "Geo Point"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    iput-boolean v4, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->p:Z

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 334
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->d:Landroid/os/Bundle;

    const-string v1, "sendbutton"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->d:Landroid/os/Bundle;

    const-string v1, "sendbutton"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->G:Z

    .line 339
    :cond_0
    invoke-static {p0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    .line 340
    const v1, 0x7f0b0036

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    .line 341
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 343
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 344
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/chaton/multimedia/geotag/e;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/geotag/e;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 353
    return-void

    .line 312
    :cond_1
    iput-boolean v5, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->p:Z

    .line 314
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 379
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    .line 380
    if-eqz v0, :cond_0

    .line 381
    invoke-virtual {v0}, Landroid/location/LocationProvider;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Ljava/lang/String;)V

    .line 382
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->C:Z

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    .line 391
    if-eqz v0, :cond_1

    .line 392
    invoke-virtual {v0}, Landroid/location/LocationProvider;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Ljava/lang/String;)V

    .line 403
    :cond_1
    :goto_0
    return-void

    .line 399
    :cond_2
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->z:Z

    return v0
.end method

.method static synthetic f(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 407
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00af

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/multimedia/geotag/g;

    invoke-direct {v2, p0}, Lcom/sec/chaton/multimedia/geotag/g;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/chaton/multimedia/geotag/f;

    invoke-direct {v2, p0}, Lcom/sec/chaton/multimedia/geotag/f;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 457
    invoke-interface {v0, v3}, Lcom/sec/common/a/d;->setCanceledOnTouchOutside(Z)V

    .line 458
    invoke-interface {v0, v3}, Lcom/sec/common/a/d;->setCancelable(Z)V

    .line 459
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 460
    return-void
.end method

.method private g()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const-wide v5, 0x412e848000000000L    # 1000000.0

    .line 466
    const/4 v0, 0x0

    .line 468
    new-instance v1, Ljava/util/StringTokenizer;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->d:Landroid/os/Bundle;

    const-string v3, "Geo Point"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\n"

    invoke-direct {v1, v2, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 470
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 473
    :cond_0
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 475
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*** RECEIVED GEO TAG COORD : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    :cond_1
    if-nez v0, :cond_2

    .line 480
    const v0, 0x7f0b00df

    invoke-static {p0, v0, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 504
    :goto_1
    return-void

    .line 484
    :cond_2
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 486
    new-instance v1, Lcom/google/android/maps/GeoPoint;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    mul-double/2addr v2, v5

    double-to-int v2, v2

    aget-object v0, v0, v4

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v0, v3

    invoke-direct {v1, v2, v0}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 488
    new-instance v0, Lcom/google/android/maps/OverlayItem;

    const-string v2, "test"

    const-string v3, "Searched location"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-object v2, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m:Lcom/sec/chaton/multimedia/geotag/a;

    invoke-virtual {v2, v0}, Lcom/sec/chaton/multimedia/geotag/a;->a(Lcom/google/android/maps/OverlayItem;)V

    .line 493
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 495
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->l:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m:Lcom/sec/chaton/multimedia/geotag/a;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 497
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->j:Lcom/google/android/maps/MapController;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    goto :goto_1
.end method

.method static synthetic g(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->r:Z

    return v0
.end method

.method private h()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const-wide v7, 0x412e848000000000L    # 1000000.0

    .line 846
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->s:Lcom/google/android/maps/GeoPoint;

    if-eqz v0, :cond_1

    .line 848
    new-instance v0, Lcom/sec/chaton/multimedia/geotag/m;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->s:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v1

    int-to-double v3, v1

    div-double/2addr v3, v7

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->s:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v1

    int-to-double v5, v1

    div-double/2addr v5, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/multimedia/geotag/m;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;ZDD)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->F:Lcom/sec/chaton/multimedia/geotag/m;

    .line 869
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 870
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->F:Lcom/sec/chaton/multimedia/geotag/m;

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/geotag/m;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 871
    return-void

    .line 857
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->n:Lcom/google/android/maps/GeoPoint;

    if-eqz v0, :cond_0

    .line 859
    new-instance v0, Lcom/sec/chaton/multimedia/geotag/m;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->n:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v1

    int-to-double v3, v1

    div-double/2addr v3, v7

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->n:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v1

    int-to-double v5, v1

    div-double/2addr v5, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/multimedia/geotag/m;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;ZDD)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->F:Lcom/sec/chaton/multimedia/geotag/m;

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->y:Z

    return v0
.end method

.method static synthetic i(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/sec/chaton/multimedia/geotag/m;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->F:Lcom/sec/chaton/multimedia/geotag/m;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 999
    const-string v0, "showPasswordLockActivity"

    const-string v1, "FacebookSubMenuActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1002
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1003
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1004
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1006
    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->startActivity(Landroid/content/Intent;)V

    .line 1008
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/location/Location;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->D:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->B:Lcom/sec/common/a/d;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e()V

    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/google/android/maps/GeoPoint;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->n:Lcom/google/android/maps/GeoPoint;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Lcom/sec/chaton/multimedia/geotag/a;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m:Lcom/sec/chaton/multimedia/geotag/a;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->l:Ljava/util/List;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->w:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->t:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 357
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->i:Lcom/google/android/maps/MapView;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->i:Lcom/google/android/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/maps/MapView;->getZoomLevel()I

    move-result v0

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 359
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 360
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 372
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->i:Lcom/google/android/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/maps/MapView;->getZoomLevel()I

    move-result v0

    if-gt v0, v4, :cond_3

    .line 362
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->j:Lcom/google/android/maps/MapController;

    if-eqz v0, :cond_2

    .line 363
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->j:Lcom/google/android/maps/MapController;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 365
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 366
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 368
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 369
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/maps/GeoPoint;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 727
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->p:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 740
    iput-object p1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->s:Lcom/google/android/maps/GeoPoint;

    .line 742
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->j:Lcom/google/android/maps/MapController;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 745
    new-instance v0, Lcom/google/android/maps/OverlayItem;

    const-string v1, "test"

    const-string v2, "Current location"

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m:Lcom/sec/chaton/multimedia/geotag/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/geotag/a;->b(Lcom/google/android/maps/OverlayItem;)V

    .line 748
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m:Lcom/sec/chaton/multimedia/geotag/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/geotag/a;->a(Lcom/google/android/maps/OverlayItem;)V

    .line 750
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 752
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->m:Lcom/sec/chaton/multimedia/geotag/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 753
    iput-object v3, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->E:Ljava/lang/String;

    .line 754
    iput-object v3, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->t:Ljava/lang/String;

    .line 757
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 599
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 600
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestCurrentLocation() provider : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->D:Landroid/location/Location;

    .line 617
    if-eqz p1, :cond_3

    .line 618
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k:Landroid/location/LocationManager;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    move-object v1, p1

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 620
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->A:Landroid/os/Handler;

    .line 621
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->A:Landroid/os/Handler;

    new-instance v1, Lcom/sec/chaton/multimedia/geotag/h;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/geotag/h;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 644
    :goto_0
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->q:Z

    if-eqz v0, :cond_1

    .line 645
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 647
    iput-boolean v6, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->q:Z

    .line 670
    :cond_1
    if-eqz p1, :cond_4

    .line 674
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->w:Landroid/view/MenuItem;

    if-eqz v0, :cond_2

    .line 675
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->w:Landroid/view/MenuItem;

    invoke-interface {v0, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 679
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 692
    :goto_1
    return-void

    .line 641
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f()V

    goto :goto_0

    .line 684
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->w:Landroid/view/MenuItem;

    if-eqz v0, :cond_5

    .line 685
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->w:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 689
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1
.end method

.method public b()V
    .locals 3

    .prologue
    .line 939
    :try_start_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 943
    if-eqz v0, :cond_0

    .line 944
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 949
    :cond_0
    :goto_0
    return-void

    .line 947
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    const-wide/16 v3, 0x0

    .line 698
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 699
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 700
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->c:Landroid/content/Context;

    const v1, 0x7f0b003e

    invoke-static {v0, v1, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 724
    :goto_0
    return-void

    .line 706
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/chaton/multimedia/geotag/i;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/geotag/i;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 718
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 719
    iput-object p1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->E:Ljava/lang/String;

    .line 721
    new-instance v0, Lcom/sec/chaton/multimedia/geotag/m;

    move-object v1, p0

    move-wide v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/multimedia/geotag/m;-><init>(Lcom/sec/chaton/multimedia/geotag/GeotagActivity;ZDD)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->F:Lcom/sec/chaton/multimedia/geotag/m;

    .line 722
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->F:Lcom/sec/chaton/multimedia/geotag/m;

    new-array v1, v2, [Ljava/lang/String;

    aput-object p1, v1, v7

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/geotag/m;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 509
    const/4 v0, 0x0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 953
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e()V

    .line 954
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 821
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 842
    :goto_0
    return-void

    .line 824
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->b()V

    .line 825
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->o:Lcom/google/android/maps/GeoPoint;

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->o:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Lcom/google/android/maps/GeoPoint;)V

    goto :goto_0

    .line 829
    :cond_0
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->q:Z

    .line 830
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->r:Z

    .line 833
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e()V

    goto :goto_0

    .line 821
    :pswitch_data_0
    .packed-switch 0x7f0702d7
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarMapActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 168
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 171
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 140
    invoke-static {p0, p1}, Lcom/sec/chaton/base/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 146
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 148
    iput-object p0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->c:Landroid/content/Context;

    .line 149
    const v0, 0x7f0300ab

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->setContentView(I)V

    .line 151
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->d()V

    .line 153
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->p:Z

    if-eqz v0, :cond_0

    .line 154
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e()V

    .line 160
    :goto_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 161
    return-void

    .line 157
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->g()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 959
    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->z:Z

    .line 961
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 962
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 965
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->B:Lcom/sec/common/a/d;

    if-eqz v0, :cond_1

    .line 966
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->B:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 969
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->A:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 970
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->A:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 973
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->F:Lcom/sec/chaton/multimedia/geotag/m;

    if-eqz v0, :cond_3

    .line 974
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->F:Lcom/sec/chaton/multimedia/geotag/m;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/geotag/m;->cancel(Z)Z

    .line 977
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 979
    invoke-super {p0}, Lcom/sec/common/actionbar/ActionBarMapActivity;->onDestroy()V

    .line 980
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 514
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onLocationChanged() provider : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    :cond_0
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->z:Z

    if-eqz v0, :cond_1

    .line 539
    :goto_0
    return-void

    .line 527
    :cond_1
    const-string v0, "network"

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->C:Z

    if-eqz v0, :cond_2

    .line 528
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onLocationChanged() return : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    iput-object p1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->D:Landroid/location/Location;

    goto :goto_0

    .line 533
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->a(Landroid/location/Location;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 799
    invoke-super {p0}, Lcom/sec/common/actionbar/ActionBarMapActivity;->onPause()V

    .line 810
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->k:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 813
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_0

    .line 814
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 815
    iget-object v1, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->f:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 817
    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 762
    const-string v0, "gps"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 763
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->C:Z

    .line 766
    :cond_0
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 771
    const-string v0, "gps"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 772
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->C:Z

    .line 775
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 786
    invoke-super {p0}, Lcom/sec/common/actionbar/ActionBarMapActivity;->onResume()V

    .line 788
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 789
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 793
    :cond_0
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->i()V

    .line 794
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 781
    return-void
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1014
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->p:Z

    if-eqz v0, :cond_0

    .line 1015
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0021

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1017
    const v0, 0x7f0705a6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->w:Landroid/view/MenuItem;

    .line 1019
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->G:Z

    if-eqz v0, :cond_1

    .line 1020
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->w:Landroid/view/MenuItem;

    const v1, 0x7f0b00e9

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 1025
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->w:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1028
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarMapActivity;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 1022
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->w:Landroid/view/MenuItem;

    const v1, 0x7f0b02d6

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1033
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-eq v1, v2, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0705a5

    if-ne v1, v2, :cond_1

    .line 1034
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->finish()V

    .line 1041
    :goto_0
    return v0

    .line 1036
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0705a6

    if-ne v1, v2, :cond_2

    .line 1037
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->h()V

    goto :goto_0

    .line 1041
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/common/actionbar/ActionBarMapActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 985
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 990
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/geotag/GeotagActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 995
    :goto_0
    return-void

    .line 992
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
