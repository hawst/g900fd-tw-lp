.class public Lcom/sec/chaton/multimedia/image/ImagePagerActivity;
.super Lcom/sec/chaton/base/BaseActivity;
.source "ImagePagerActivity.java"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private F:Landroid/view/View$OnClickListener;

.field private G:Landroid/database/ContentObserver;

.field private H:Landroid/os/Handler;

.field private I:Landroid/os/Handler;

.field a:Lcom/sec/chaton/e/a/v;

.field private c:Landroid/support/v4/view/ViewPager;

.field private d:J

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/sec/chaton/multimedia/image/s;

.field private h:Lcom/sec/chaton/e/a/u;

.field private i:I

.field private j:I

.field private k:Lcom/sec/common/f/c;

.field private l:Landroid/content/Context;

.field private m:Landroid/view/View;

.field private n:Landroid/content/BroadcastReceiver;

.field private o:Z

.field private p:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/chaton/multimedia/image/ImagePagerFragment;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/chaton/multimedia/image/t;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcom/sec/chaton/e/r;

.field private t:Landroid/view/MenuItem;

.field private u:Landroid/view/MenuItem;

.field private v:Lcom/sec/common/a/d;

.field private w:Landroid/widget/ImageView;

.field private x:Landroid/widget/ImageView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->p:Ljava/util/HashMap;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->q:Ljava/util/HashMap;

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    .line 233
    new-instance v0, Lcom/sec/chaton/multimedia/image/k;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/k;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->E:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 252
    new-instance v0, Lcom/sec/chaton/multimedia/image/l;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/l;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->F:Landroid/view/View$OnClickListener;

    .line 290
    new-instance v0, Lcom/sec/chaton/multimedia/image/m;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/m;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a:Lcom/sec/chaton/e/a/v;

    .line 395
    new-instance v0, Lcom/sec/chaton/multimedia/image/n;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/multimedia/image/n;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->G:Landroid/database/ContentObserver;

    .line 764
    new-instance v0, Lcom/sec/chaton/multimedia/image/p;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/p;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->H:Landroid/os/Handler;

    .line 791
    new-instance v0, Lcom/sec/chaton/multimedia/image/q;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/q;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->I:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    return v0
.end method

.method private a(I)V
    .locals 9

    .prologue
    const/4 v8, -0x2

    const/4 v7, 0x4

    const/16 v6, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 455
    iput p1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    .line 459
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 461
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->q:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/t;

    .line 462
    if-eqz v0, :cond_8

    .line 465
    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->B:Z

    if-eqz v1, :cond_9

    .line 466
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->w:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 471
    :goto_0
    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->i:I

    add-int/lit8 v1, v1, -0x1

    if-ge p1, v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->B:Z

    if-eqz v1, :cond_a

    .line 472
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->x:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 477
    :goto_1
    iget-object v4, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    .line 480
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v5, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-ne v1, v5, :cond_b

    .line 481
    invoke-static {v4}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v1

    .line 486
    :goto_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "thumbnail"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 487
    :cond_2
    iget v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-virtual {p0, v4, v3}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(IZ)V

    .line 488
    iget v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-virtual {p0, v4, v3}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c(IZ)V

    .line 494
    :goto_3
    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->B:Z

    if-eqz v1, :cond_d

    .line 495
    :cond_3
    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-virtual {p0, v1, v3}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b(IZ)V

    .line 500
    :goto_4
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    invoke-static {v1}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 501
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->u:Landroid/view/MenuItem;

    invoke-static {v1}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 504
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->D:Z

    if-eqz v1, :cond_e

    .line 505
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 507
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v4, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v1, v4, :cond_4

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v4, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v1, v4, :cond_6

    :cond_4
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    const-string v4, "thumbnail"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 510
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->p:Ljava/util/HashMap;

    iget-wide v4, v0, Lcom/sec/chaton/multimedia/image/t;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 511
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->p:Ljava/util/HashMap;

    iget-wide v4, v0, Lcom/sec/chaton/multimedia/image/t;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;

    .line 512
    invoke-virtual {v1}, Lcom/sec/chaton/localbackup/chatview/BackupImagePagerFragment;->b()V

    .line 538
    :cond_6
    :goto_5
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->B:Z

    if-eqz v1, :cond_12

    .line 539
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->z:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 540
    iput-boolean v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->A:Z

    .line 549
    :cond_8
    :goto_6
    return-void

    .line 468
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->w:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 474
    :cond_a
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->x:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 482
    :cond_b
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v5, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v1, v5, :cond_13

    move v1, v2

    .line 483
    goto/16 :goto_2

    .line 490
    :cond_c
    iget v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-virtual {p0, v4, v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(IZ)V

    .line 491
    iget v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-virtual {p0, v4, v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c(IZ)V

    goto/16 :goto_3

    .line 497
    :cond_d
    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b(IZ)V

    goto/16 :goto_4

    .line 517
    :cond_e
    iput-boolean v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->C:Z

    .line 519
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->s:Lcom/sec/chaton/e/r;

    sget-object v4, Lcom/sec/chaton/e/r;->d:Lcom/sec/chaton/e/r;

    if-eq v1, v4, :cond_f

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->s:Lcom/sec/chaton/e/r;

    sget-object v4, Lcom/sec/chaton/e/r;->e:Lcom/sec/chaton/e/r;

    if-eq v1, v4, :cond_f

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 520
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->d:Ljava/lang/String;

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 521
    array-length v1, v1

    if-le v1, v7, :cond_f

    iget v1, v0, Lcom/sec/chaton/multimedia/image/t;->g:I

    const/4 v4, -0x1

    if-eq v1, v4, :cond_f

    iget v1, v0, Lcom/sec/chaton/multimedia/image/t;->g:I

    if-eqz v1, :cond_f

    .line 523
    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->C:Z

    .line 527
    :cond_f
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->C:Z

    if-eqz v1, :cond_11

    .line 528
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->B:Z

    if-eqz v1, :cond_10

    .line 529
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 531
    :cond_10
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 534
    :cond_11
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 542
    :cond_12
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->z:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 543
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->y:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->l:Landroid/content/Context;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/t;->h:Ljava/lang/String;

    const/high16 v4, 0x41f00000    # 30.0f

    invoke-static {v4}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v4

    float-to-int v4, v4

    invoke-static {v3, v0, v4}, Lcom/sec/chaton/multimedia/emoticon/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 544
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->y:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 545
    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->A:Z

    goto/16 :goto_6

    :cond_13
    move v1, v3

    goto/16 :goto_2
.end method

.method private a(JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 693
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->q:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->q:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/t;

    .line 695
    if-eqz v0, :cond_0

    .line 696
    iput-object p3, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    .line 699
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;I)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;I)I
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->i:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;I)I
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    return p1
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->q:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->l:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->o:Z

    return v0
.end method

.method static synthetic h(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->d:J

    return-wide v0
.end method

.method static synthetic h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 432
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 433
    const-string v0, "showPasswordLockActivity"

    sget-object v1, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 437
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 438
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 439
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 440
    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->startActivity(Landroid/content/Intent;)V

    .line 442
    :cond_1
    return-void
.end method

.method static synthetic j(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->i:I

    return v0
.end method

.method private j()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 706
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 734
    :cond_0
    :goto_0
    return-void

    .line 710
    :cond_1
    invoke-static {}, Lcom/sec/chaton/util/ck;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 712
    invoke-static {p0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01bc

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b01e2

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 718
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->q:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/t;

    .line 721
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 722
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    .line 724
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 725
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 729
    :cond_3
    new-instance v1, Lcom/sec/chaton/multimedia/a/a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ChatON"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2, v4}, Lcom/sec/chaton/multimedia/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 730
    new-array v0, v4, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method static synthetic k(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->h:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 737
    new-instance v0, Lcom/sec/chaton/multimedia/image/o;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/o;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->n:Landroid/content/BroadcastReceiver;

    .line 744
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 745
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 746
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 747
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 748
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 749
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m()V

    .line 750
    return-void
.end method

.method static synthetic l(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->D:Z

    return v0
.end method

.method private m()V
    .locals 3

    .prologue
    .line 753
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 754
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 755
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->finish()V

    .line 758
    :cond_0
    return-void
.end method

.method static synthetic m(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m()V

    return-void
.end method

.method static synthetic n(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->p:Ljava/util/HashMap;

    return-object v0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 762
    return-void
.end method

.method private o()V
    .locals 6

    .prologue
    const v5, 0x7f0b0166

    const/4 v2, 0x0

    .line 891
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 920
    :cond_0
    :goto_0
    return-void

    .line 895
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 896
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->q:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    iget v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/t;

    .line 898
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 902
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    .line 904
    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    const-string v4, "file:"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 905
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 908
    :cond_2
    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v4, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v3, v4, :cond_3

    iget-object v3, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v4, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v3, v4, :cond_4

    .line 909
    :cond_3
    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1, v2, v2}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 915
    :goto_1
    if-eqz v0, :cond_0

    .line 916
    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 911
    :cond_4
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v3, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    if-ne v0, v3, :cond_5

    .line 912
    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1, v2, v2}, Lcom/sec/chaton/util/ch;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method static synthetic o(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->o()V

    return-void
.end method

.method private p()V
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->v:Lcom/sec/common/a/d;

    if-nez v0, :cond_0

    .line 925
    new-instance v0, Lcom/sec/chaton/multimedia/image/r;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/r;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;)V

    invoke-static {p0, v0}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->v:Lcom/sec/common/a/d;

    .line 933
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->v:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 934
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method protected a(IZ)V
    .locals 1

    .prologue
    .line 644
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    if-ne v0, p1, :cond_0

    .line 645
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 648
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 651
    :cond_0
    return-void
.end method

.method protected a(J)V
    .locals 2

    .prologue
    .line 835
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->p:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 836
    return-void
.end method

.method protected a(JLcom/sec/chaton/multimedia/image/ImagePagerFragment;)V
    .locals 2

    .prologue
    .line 831
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->p:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 832
    return-void
.end method

.method protected a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 938
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->B:Z

    if-nez v0, :cond_3

    .line 939
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->u:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 940
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->u:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 942
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 943
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 946
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->z:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 948
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->C:Z

    if-eqz v0, :cond_2

    .line 949
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 952
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 953
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 956
    iput-boolean v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->B:Z

    .line 984
    :goto_0
    return-void

    .line 958
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->u:Landroid/view/MenuItem;

    if-eqz v0, :cond_4

    .line 959
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->u:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 961
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    if-eqz v0, :cond_5

    if-nez p1, :cond_5

    .line 962
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 965
    :cond_5
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->A:Z

    if-eqz v0, :cond_6

    .line 966
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->z:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 969
    :cond_6
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->C:Z

    if-eqz v0, :cond_7

    .line 970
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 973
    :cond_7
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    if-lez v0, :cond_8

    .line 974
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 977
    :cond_8
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->i:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_9

    .line 978
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 982
    :cond_9
    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->B:Z

    goto :goto_0
.end method

.method protected b()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/chaton/multimedia/image/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 676
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->q:Ljava/util/HashMap;

    return-object v0
.end method

.method protected b(IZ)V
    .locals 1

    .prologue
    .line 654
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    if-ne v0, p1, :cond_0

    .line 655
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 659
    :cond_0
    return-void
.end method

.method protected c()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 680
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    return-object v0
.end method

.method protected c(IZ)V
    .locals 1

    .prologue
    .line 662
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    if-ne v0, p1, :cond_0

    .line 663
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->u:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->u:Landroid/view/MenuItem;

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 666
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->u:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 669
    :cond_0
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected e()Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 688
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->k:Lcom/sec/common/f/c;

    return-object v0
.end method

.method protected f()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->H:Landroid/os/Handler;

    return-object v0
.end method

.method public g()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 843
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->I:Landroid/os/Handler;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 123
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 125
    const v0, 0x7f0300b2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->setContentView(I)V

    .line 126
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->k:Lcom/sec/common/f/c;

    .line 127
    iput-object p0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->l:Landroid/content/Context;

    .line 129
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 134
    const v0, 0x7f07032f

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m:Landroid/view/View;

    .line 135
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m:Landroid/view/View;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->F:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m:Landroid/view/View;

    const v4, 0x7f07014c

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 138
    const v4, 0x7f0b040f

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 140
    const v0, 0x7f070330

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 142
    new-instance v0, Lcom/sec/chaton/multimedia/image/s;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-direct {v0, p0, v4}, Lcom/sec/chaton/multimedia/image/s;-><init>(Lcom/sec/chaton/multimedia/image/ImagePagerActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->g:Lcom/sec/chaton/multimedia/image/s;

    .line 144
    const v0, 0x7f07032a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c:Landroid/support/v4/view/ViewPager;

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->g:Lcom/sec/chaton/multimedia/image/s;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 146
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->c:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->E:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 148
    const v0, 0x7f07032b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->w:Landroid/widget/ImageView;

    .line 149
    const v0, 0x7f07032c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->x:Landroid/widget/ImageView;

    .line 151
    const v0, 0x7f07032d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->z:Landroid/widget/LinearLayout;

    .line 152
    const v0, 0x7f07032e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->y:Landroid/widget/TextView;

    .line 154
    if-eqz p1, :cond_4

    .line 156
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "[restore from onSaveInstanceState]"

    sget-object v3, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    const-string v0, "messageId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->d:J

    .line 161
    const-string v0, "inboxNo"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e:Ljava/lang/String;

    .line 162
    const-string v0, "isValid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->o:Z

    .line 163
    const-string v0, "chatType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->s:Lcom/sec/chaton/e/r;

    .line 164
    const-string v0, "backup"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->D:Z

    .line 165
    const-string v0, "sessionID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f:Ljava/lang/String;

    .line 175
    :cond_1
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_2

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "messageId: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->d:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", mInboxNo: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->s:Lcom/sec/chaton/e/r;

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(Lcom/sec/chaton/e/r;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 180
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 183
    :cond_3
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v3, v4}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->h:Lcom/sec/chaton/e/a/u;

    .line 186
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->D:Z

    if-eqz v0, :cond_5

    .line 187
    sget-object v3, Lcom/sec/chaton/localbackup/database/a;->a:Landroid/net/Uri;

    .line 192
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->h:Lcom/sec/chaton/e/a/u;

    const-string v5, "message_inbox_no=? AND ( message_content_type=? OR message_content_type=? OR message_content_type=? ) AND message_is_truncated = \'N\'"

    const/4 v4, 0x4

    new-array v6, v4, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e:Ljava/lang/String;

    aput-object v4, v6, v8

    sget-object v4, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    invoke-virtual {v4}, Lcom/sec/chaton/e/w;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v1

    const/4 v4, 0x2

    sget-object v7, Lcom/sec/chaton/e/w;->d:Lcom/sec/chaton/e/w;

    invoke-virtual {v7}, Lcom/sec/chaton/e/w;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v4, 0x3

    sget-object v7, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    invoke-virtual {v7}, Lcom/sec/chaton/e/w;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    const-string v7, "message_is_failed , message_time , _id"

    move-object v4, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/common/actionbar/a;->b(Z)V

    .line 206
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/common/actionbar/a;->a(Z)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/common/actionbar/a;->c(Z)V

    .line 208
    return-void

    .line 166
    :cond_4
    if-eqz v3, :cond_1

    .line 167
    const-string v0, "messageId"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->d:J

    .line 168
    const-string v0, "inboxNo"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e:Ljava/lang/String;

    .line 169
    const-string v0, "isValid"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->o:Z

    .line 170
    const-string v0, "chatType"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/chaton/e/r;->a(I)Lcom/sec/chaton/e/r;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->s:Lcom/sec/chaton/e/r;

    .line 171
    const-string v0, "backup"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->D:Z

    .line 172
    const-string v0, "sessionID"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 189
    :cond_5
    sget-object v3, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->k:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 448
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->v:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->v:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 451
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onDestroy()V

    .line 452
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 426
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->n()V

    .line 427
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->G:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 428
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onPause()V

    .line 429
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 418
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->l()V

    .line 419
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->i()V

    .line 420
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/v;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->G:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 421
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onResume()V

    .line 422
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->q:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/t;

    .line 216
    if-eqz v0, :cond_1

    iget-wide v1, v0, Lcom/sec/chaton/multimedia/image/t;->b:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    .line 218
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 219
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onSaveInstanceState] messageId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v0, Lcom/sec/chaton/multimedia/image/t;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", inboxNo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_0
    const-string v1, "messageId"

    iget-wide v2, v0, Lcom/sec/chaton/multimedia/image/t;->b:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 223
    const-string v0, "inboxNo"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v0, "isValid"

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->o:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 225
    const-string v0, "chatType"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->s:Lcom/sec/chaton/e/r;

    invoke-virtual {v1}, Lcom/sec/chaton/e/r;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 226
    const-string v0, "backup"

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 227
    const-string v0, "sessionID"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 231
    return-void
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0031

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 851
    const v0, 0x7f07058c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->t:Landroid/view/MenuItem;

    .line 852
    const v0, 0x7f0705b2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->u:Landroid/view/MenuItem;

    .line 854
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->a(I)V

    .line 855
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 861
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x7f07058c

    if-ne v0, v2, :cond_1

    .line 862
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j()V

    .line 886
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 863
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x7f0705b2

    if-ne v0, v2, :cond_0

    .line 865
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 866
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->q:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->r:Ljava/util/HashMap;

    iget v3, p0, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/t;

    .line 868
    if-eqz v0, :cond_0

    .line 872
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v2

    const-string v3, "TrunkShareCheckPopup"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_4

    .line 873
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v3, Lcom/sec/chaton/e/w;->c:Lcom/sec/chaton/e/w;

    if-eq v2, v3, :cond_2

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/t;->f:Lcom/sec/chaton/e/w;

    sget-object v3, Lcom/sec/chaton/e/w;->n:Lcom/sec/chaton/e/w;

    if-ne v2, v3, :cond_4

    .line 874
    :cond_2
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/t;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/chaton/trunk/c/f;->b(Ljava/lang/String;)Z

    move-result v0

    .line 878
    :goto_1
    if-eqz v0, :cond_3

    .line 879
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->p()V

    goto :goto_0

    .line 881
    :cond_3
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImagePagerActivity;->o()V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method
