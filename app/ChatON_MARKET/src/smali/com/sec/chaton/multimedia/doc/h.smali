.class public Lcom/sec/chaton/multimedia/doc/h;
.super Landroid/widget/BaseAdapter;
.source "FileListAdapter.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/view/LayoutInflater;

.field private c:I

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/doc/g;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/sec/common/f/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/doc/g;",
            ">;",
            "Lcom/sec/common/f/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/chaton/multimedia/doc/h;->a:Landroid/content/Context;

    .line 28
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/h;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/doc/h;->b:Landroid/view/LayoutInflater;

    .line 29
    iput-object p3, p0, Lcom/sec/chaton/multimedia/doc/h;->d:Ljava/util/ArrayList;

    .line 30
    iput p2, p0, Lcom/sec/chaton/multimedia/doc/h;->c:I

    .line 31
    iput-object p4, p0, Lcom/sec/chaton/multimedia/doc/h;->e:Lcom/sec/common/f/c;

    .line 33
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 47
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 53
    .line 55
    if-nez p2, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/h;->b:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/multimedia/doc/h;->c:I

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 57
    new-instance v0, Lcom/sec/chaton/multimedia/doc/i;

    invoke-direct {v0, p2}, Lcom/sec/chaton/multimedia/doc/i;-><init>(Landroid/view/View;)V

    .line 58
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 63
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/doc/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/doc/g;

    .line 65
    new-instance v2, Lcom/sec/chaton/multimedia/doc/f;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/sec/chaton/multimedia/doc/f;-><init>(Ljava/lang/String;Lcom/sec/chaton/multimedia/doc/g;)V

    .line 66
    iget-object v3, p0, Lcom/sec/chaton/multimedia/doc/h;->e:Lcom/sec/common/f/c;

    iget-object v4, v1, Lcom/sec/chaton/multimedia/doc/i;->a:Landroid/widget/ImageView;

    invoke-virtual {v3, v4, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 76
    iget-object v2, v1, Lcom/sec/chaton/multimedia/doc/i;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v2, v1, Lcom/sec/chaton/multimedia/doc/i;->c:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/doc/g;->a()Lcom/sec/chaton/multimedia/doc/b;

    move-result-object v0

    sget-object v2, Lcom/sec/chaton/multimedia/doc/b;->b:Lcom/sec/chaton/multimedia/doc/b;

    if-ne v0, v2, :cond_1

    .line 80
    iget-object v0, v1, Lcom/sec/chaton/multimedia/doc/i;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 85
    :goto_1
    return-object p2

    .line 60
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/doc/i;

    move-object v1, v0

    goto :goto_0

    .line 82
    :cond_1
    iget-object v0, v1, Lcom/sec/chaton/multimedia/doc/i;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
