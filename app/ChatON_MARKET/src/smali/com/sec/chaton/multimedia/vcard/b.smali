.class public Lcom/sec/chaton/multimedia/vcard/b;
.super Ljava/lang/Object;
.source "ContactStruct.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/f;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/d;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/g;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/e;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    if-nez v0, :cond_0

    .line 250
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    new-instance v1, Lcom/sec/chaton/multimedia/vcard/f;

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2, p3, p4}, Lcom/sec/chaton/multimedia/vcard/f;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    return-void
.end method

.method public b(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    if-nez v0, :cond_0

    .line 258
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    new-instance v1, Lcom/sec/chaton/multimedia/vcard/d;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/sec/chaton/multimedia/vcard/d;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    return-void
.end method
