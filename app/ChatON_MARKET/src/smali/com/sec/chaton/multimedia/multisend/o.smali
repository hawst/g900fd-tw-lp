.class Lcom/sec/chaton/multimedia/multisend/o;
.super Landroid/widget/ArrayAdapter;
.source "PreviewPageAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field a:I

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Lcom/sec/common/f/c;

.field private e:Lcom/sec/chaton/multimedia/multisend/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;Lcom/sec/chaton/multimedia/multisend/PreviewPageView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;",
            "Lcom/sec/common/f/c;",
            "Lcom/sec/chaton/multimedia/multisend/PreviewPageView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 31
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/o;->b:Landroid/content/Context;

    .line 32
    iput-object p4, p0, Lcom/sec/chaton/multimedia/multisend/o;->d:Lcom/sec/common/f/c;

    .line 34
    iput p2, p0, Lcom/sec/chaton/multimedia/multisend/o;->a:I

    .line 36
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/o;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/o;->c:Landroid/view/LayoutInflater;

    .line 38
    iput-object p5, p0, Lcom/sec/chaton/multimedia/multisend/o;->e:Lcom/sec/chaton/multimedia/multisend/p;

    .line 40
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 47
    if-nez p2, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/o;->c:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/multimedia/multisend/o;->a:I

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 49
    new-instance v1, Lcom/sec/chaton/multimedia/multisend/q;

    invoke-direct {v1}, Lcom/sec/chaton/multimedia/multisend/q;-><init>()V

    .line 50
    const v0, 0x7f07044c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/multisend/q;->a:Landroid/widget/ImageView;

    .line 51
    const v0, 0x7f07044d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/multisend/q;->b:Landroid/widget/ImageView;

    .line 52
    iget-object v0, v1, Lcom/sec/chaton/multimedia/multisend/q;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v8, v1

    .line 57
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/multisend/o;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/PreviewData;

    .line 59
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/sec/chaton/multimedia/multisend/q;->c:Ljava/lang/String;

    .line 60
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/sec/chaton/multimedia/multisend/q;->d:Ljava/lang/String;

    .line 61
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/sec/chaton/multimedia/multisend/q;->e:Ljava/lang/String;

    .line 62
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/multisend/PreviewData;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/sec/chaton/multimedia/multisend/q;->f:Ljava/lang/String;

    .line 64
    iget-object v0, v8, Lcom/sec/chaton/multimedia/multisend/q;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 65
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/i;

    iget-object v1, v8, Lcom/sec/chaton/multimedia/multisend/q;->c:Ljava/lang/String;

    iget-object v2, v8, Lcom/sec/chaton/multimedia/multisend/q;->d:Ljava/lang/String;

    iget-object v5, v8, Lcom/sec/chaton/multimedia/multisend/q;->f:Ljava/lang/String;

    move v4, v3

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/multimedia/multisend/i;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Landroid/widget/CheckBox;Landroid/widget/ImageView;)V

    .line 70
    :goto_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/o;->d:Lcom/sec/common/f/c;

    iget-object v2, v8, Lcom/sec/chaton/multimedia/multisend/q;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 72
    iget-object v0, v8, Lcom/sec/chaton/multimedia/multisend/q;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 74
    return-object p2

    .line 55
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/q;

    move-object v8, v0

    goto :goto_0

    .line 67
    :cond_1
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/i;

    iget-object v1, v8, Lcom/sec/chaton/multimedia/multisend/q;->e:Ljava/lang/String;

    iget-object v2, v8, Lcom/sec/chaton/multimedia/multisend/q;->d:Ljava/lang/String;

    const/4 v4, 0x1

    iget-object v5, v8, Lcom/sec/chaton/multimedia/multisend/q;->f:Ljava/lang/String;

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/multimedia/multisend/i;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Landroid/widget/CheckBox;Landroid/widget/ImageView;)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/q;

    .line 92
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/o;->e:Lcom/sec/chaton/multimedia/multisend/p;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/multisend/q;->c:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/sec/chaton/multimedia/multisend/p;->a(Ljava/lang/String;)V

    .line 94
    return-void
.end method
