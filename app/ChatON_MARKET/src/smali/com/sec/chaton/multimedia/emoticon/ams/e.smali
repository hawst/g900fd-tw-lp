.class public Lcom/sec/chaton/multimedia/emoticon/ams/e;
.super Lcom/sec/common/f/a;
.source "AmsItemDispatcherTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 32
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Lcom/sec/common/f/a;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 49
    check-cast p1, Landroid/graphics/Bitmap;

    .line 50
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/ams/e;->e()Landroid/widget/ImageView;

    move-result-object v0

    .line 52
    if-nez p1, :cond_0

    .line 53
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 77
    :goto_0
    return-void

    .line 61
    :cond_0
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/ams/e;->k()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 62
    invoke-virtual {v1, v6}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    .line 67
    if-eqz p2, :cond_1

    .line 68
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 70
    :cond_1
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 72
    new-instance v3, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    aput-object v2, v4, v5

    aput-object v1, v4, v6

    invoke-direct {v3, v4}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 73
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 75
    const/16 v0, 0x64

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/ams/e;->e()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 37
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/ams/e;->a()Ljava/lang/String;

    move-result-object v0

    .line 43
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/ams/e;->e()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v2

    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/common/util/j;->a(Landroid/content/Context;Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/ams/e;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 86
    :cond_0
    return-void
.end method

.method public e()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public f()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 100
    invoke-super {p0}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/ams/e;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Landroid/view/View;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/ams/e;->e()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/ams/e;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
