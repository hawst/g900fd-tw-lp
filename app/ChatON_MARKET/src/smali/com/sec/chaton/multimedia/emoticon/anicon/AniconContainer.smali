.class public Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;
.super Landroid/support/v4/app/Fragment;
.source "AniconContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/chaton/settings/downloads/ab;
.implements Lcom/sec/chaton/settings/downloads/ac;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/chaton/d/j;

.field private c:Lcom/sec/common/f/c;

.field private d:Lcom/sec/common/f/c;

.field private e:Landroid/app/Activity;

.field private f:Landroid/widget/HorizontalScrollView;

.field private g:Landroid/widget/LinearLayout;

.field private h:Landroid/widget/GridView;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/Button;

.field private m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

.field private n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Landroid/database/Cursor;

.field private r:Lcom/sec/chaton/multimedia/emoticon/anicon/h;

.field private s:I

.field private t:Lcom/sec/chaton/e/a/u;

.field private u:Landroid/widget/RelativeLayout;

.field private v:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 705
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;I)I
    .locals 0

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->s:I

    return p1
.end method

.method private a(Ljava/lang/String;ZI)Landroid/view/View;
    .locals 5

    .prologue
    .line 505
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030047

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 507
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090279

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09027a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 508
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 510
    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 511
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 513
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-lt v1, p3, :cond_0

    .line 514
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 519
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->n:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    if-eqz p2, :cond_1

    .line 524
    const v1, 0x7f0200aa

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 529
    :goto_1
    return-object v0

    .line 516
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 526
    :cond_1
    const v1, 0x7f020401

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;)Landroid/widget/HorizontalScrollView;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->f:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method public static final a(I)Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->a(ILjava/lang/String;)Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;

    move-result-object v0

    return-object v0
.end method

.method public static final a(ILjava/lang/String;)Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 95
    const-string v1, "layoutResId"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;

    invoke-direct {v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;-><init>()V

    .line 99
    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->setArguments(Landroid/os/Bundle;)V

    .line 101
    return-object v1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 376
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/multimedia/emoticon/CategoryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 377
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 378
    return-void
.end method

.method private c()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 390
    sget-object v0, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-static {v0}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "anicon_group"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 394
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 13

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v7, -0x1

    const v12, 0x7f0701ce

    .line 404
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->d()V

    .line 407
    const-string v0, "-1"

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->d(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 408
    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 409
    const v2, 0x7f020115

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 412
    const-string v0, "-2"

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->d(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 413
    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 414
    const v2, 0x7f020118

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    invoke-interface {v0, v7}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    .line 427
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 428
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    const-string v5, "item_id"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 430
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    const-string v5, "newly_installed"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_2

    move v2, v1

    .line 432
    :goto_1
    iget-object v4, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    iget-object v5, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    const-string v9, "data1"

    invoke-interface {v5, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 433
    iget-object v5, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    iget-object v9, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    const-string v10, "data2"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v5, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 436
    iget-object v9, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    if-nez v9, :cond_1

    .line 437
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    move-object p1, v8

    .line 443
    :cond_1
    :try_start_0
    iget-object v9, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    iget-object v10, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    const-string v11, "extras"

    invoke-interface {v10, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 446
    invoke-static {v9}, Lcom/sec/chaton/e/a/b;->a(Ljava/lang/String;)Lcom/sec/chaton/e/a/c;

    move-result-object v9

    .line 450
    const-string v10, "0036"

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "001"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-eqz v4, :cond_7

    move v5, v6

    move v4, v3

    .line 455
    :goto_2
    :try_start_1
    invoke-direct {p0, v8, v2, v5}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->a(Ljava/lang/String;ZI)Landroid/view/View;

    move-result-object v0

    .line 456
    const v2, 0x7f0701ce

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 458
    if-ne v5, v7, :cond_3

    .line 459
    new-instance v2, Lcom/sec/chaton/multimedia/emoticon/anicon/i;

    invoke-virtual {v9}, Lcom/sec/chaton/e/a/c;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v9, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->b:Lcom/sec/chaton/d/j;

    invoke-direct {v2, v8, v5, v9}, Lcom/sec/chaton/multimedia/emoticon/anicon/i;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/chaton/d/j;)V

    .line 460
    iget-object v5, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->c:Lcom/sec/common/f/c;

    invoke-virtual {v5, v0, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    :goto_3
    move v0, v4

    .line 468
    goto/16 :goto_0

    :cond_2
    move v2, v3

    .line 430
    goto :goto_1

    .line 462
    :cond_3
    const v2, 0x7f020114

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 464
    :catch_0
    move-exception v0

    move-object v2, v0

    move v0, v4

    .line 465
    :goto_4
    sget-boolean v4, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v4, :cond_0

    .line 466
    sget-object v4, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->a:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 471
    :cond_4
    if-nez v0, :cond_5

    .line 473
    const-string v0, "-4"

    invoke-direct {p0, v0, v1, v6}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->a(Ljava/lang/String;ZI)Landroid/view/View;

    move-result-object v0

    .line 474
    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 475
    const v1, 0x7f020114

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 479
    :cond_5
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 481
    const-string p1, "-4"

    .line 485
    :cond_6
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e(Ljava/lang/String;)V

    .line 486
    return-void

    .line 464
    :catch_1
    move-exception v2

    goto :goto_4

    :cond_7
    move v5, v7

    move v4, v0

    goto :goto_2
.end method

.method private d(Ljava/lang/String;)Landroid/view/View;
    .locals 2

    .prologue
    .line 494
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->a(Ljava/lang/String;ZI)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 536
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->p:Ljava/lang/String;

    .line 539
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 540
    const-string v2, "-1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "-2"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 544
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->n:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 545
    const v2, 0x7f0701ce

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 547
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->c:Lcom/sec/common/f/c;

    invoke-virtual {v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    goto :goto_0

    .line 550
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 552
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 553
    return-void
.end method

.method private e()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 682
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/anicon/f;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/f;-><init>(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method private e(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    const/16 v6, 0x8

    .line 562
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 647
    :cond_0
    :goto_0
    return-void

    .line 567
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 568
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->n:Ljava/util/Map;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->p:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 570
    invoke-virtual {v0, v7}, Landroid/view/View;->setSelected(Z)V

    .line 573
    :cond_2
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->p:Ljava/lang/String;

    .line 576
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 577
    if-eqz v0, :cond_3

    .line 578
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 581
    :cond_3
    const-string v0, "-2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 582
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    invoke-virtual {v0, v7}, Landroid/widget/GridView;->setVisibility(I)V

    .line 583
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->i:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 584
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->j:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 585
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->u:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 587
    sget-object v0, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "recentused"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 589
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 591
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->changeCursor(Landroid/database/Cursor;)V

    .line 592
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->notifyDataSetInvalidated()V

    .line 594
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setVisibility(I)V

    .line 596
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->i:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 600
    :cond_4
    const-string v0, "-1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 601
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setVisibility(I)V

    .line 602
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->i:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 603
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->j:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 604
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->u:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 612
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->k:Landroid/widget/TextView;

    const v1, 0x7f0b01d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 614
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    invoke-virtual {v0, v2}, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->changeCursor(Landroid/database/Cursor;)V

    .line 615
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->notifyDataSetInvalidated()V

    goto/16 :goto_0

    .line 616
    :cond_5
    const-string v0, "-4"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 618
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setVisibility(I)V

    .line 619
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->i:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 620
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->j:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 621
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->u:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 624
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    invoke-virtual {v0, v7}, Landroid/widget/GridView;->setVisibility(I)V

    .line 625
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->i:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 626
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->j:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 627
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->u:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 630
    sget-object v0, Lcom/sec/chaton/e/ao;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "group"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 640
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 642
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->changeCursor(Landroid/database/Cursor;)V

    .line 643
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->notifyDataSetInvalidated()V

    .line 645
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    invoke-virtual {v0, v7}, Landroid/widget/GridView;->setSelection(I)V

    goto/16 :goto_0
.end method

.method private f()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 694
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/anicon/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/g;-><init>(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method private f(Ljava/lang/String;)V
    .locals 8

    .prologue
    const v4, 0x7f020401

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 650
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 651
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 652
    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 654
    const/16 v0, 0x1f

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "item_id"

    aput-object v2, v0, v1

    const-string v2, " IN ( SELECT "

    aput-object v2, v0, v7

    const/4 v2, 0x2

    const-string v3, "item_id"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    const-string v3, " FROM ( "

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string v3, "\tSELECT "

    aput-object v3, v0, v2

    const/4 v2, 0x5

    const-string v3, "data1"

    aput-object v3, v0, v2

    const/4 v2, 0x6

    const-string v3, ","

    aput-object v3, v0, v2

    const/4 v2, 0x7

    const-string v3, "data2"

    aput-object v3, v0, v2

    const/16 v2, 0x8

    const-string v3, "\tFROM "

    aput-object v3, v0, v2

    const/16 v2, 0x9

    const-string v3, "download_item"

    aput-object v3, v0, v2

    const/16 v2, 0xa

    const-string v3, "\tWHERE "

    aput-object v3, v0, v2

    const/16 v2, 0xb

    const-string v3, "item_type"

    aput-object v3, v0, v2

    const/16 v2, 0xc

    const-string v3, "=\'"

    aput-object v3, v0, v2

    const/16 v2, 0xd

    sget-object v3, Lcom/sec/chaton/e/ar;->e:Lcom/sec/chaton/e/ar;

    invoke-virtual {v3}, Lcom/sec/chaton/e/ar;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/16 v2, 0xe

    const-string v3, "\' AND "

    aput-object v3, v0, v2

    const/16 v2, 0xf

    const-string v3, "install"

    aput-object v3, v0, v2

    const/16 v2, 0x10

    const-string v3, " != 0 AND "

    aput-object v3, v0, v2

    const/16 v2, 0x11

    const-string v3, "item_id"

    aput-object v3, v0, v2

    const/16 v2, 0x12

    const-string v3, "=?) D1"

    aput-object v3, v0, v2

    const/16 v2, 0x13

    const-string v3, "\tINNER JOIN "

    aput-object v3, v0, v2

    const/16 v2, 0x14

    const-string v3, "download_item"

    aput-object v3, v0, v2

    const/16 v2, 0x15

    const-string v3, " D2 "

    aput-object v3, v0, v2

    const/16 v2, 0x16

    const-string v3, "\tON D1."

    aput-object v3, v0, v2

    const/16 v2, 0x17

    const-string v3, "data1"

    aput-object v3, v0, v2

    const/16 v2, 0x18

    const-string v3, " = D2."

    aput-object v3, v0, v2

    const/16 v2, 0x19

    const-string v3, "data1"

    aput-object v3, v0, v2

    const/16 v2, 0x1a

    const-string v3, " AND D1."

    aput-object v3, v0, v2

    const/16 v2, 0x1b

    const-string v3, "data2"

    aput-object v3, v0, v2

    const/16 v2, 0x1c

    const-string v3, " = D2."

    aput-object v3, v0, v2

    const/16 v2, 0x1d

    const-string v3, "data2"

    aput-object v3, v0, v2

    const/16 v2, 0x1e

    const-string v3, ")"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 668
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 669
    const-string v0, "newly_installed"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 670
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->t:Lcom/sec/chaton/e/a/u;

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/chaton/e/aq;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v6, "init_newly_installed"

    invoke-virtual {v3, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    new-array v6, v7, [Ljava/lang/String;

    aput-object p1, v6, v1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/chaton/e/a/u;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 679
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/chaton/multimedia/emoticon/anicon/h;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->r:Lcom/sec/chaton/multimedia/emoticon/anicon/h;

    .line 140
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/anicon/d;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/d;-><init>(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->post(Ljava/lang/Runnable;)Z

    .line 353
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/anicon/e;

    invoke-direct {v1, p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/e;-><init>(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->post(Ljava/lang/Runnable;)Z

    .line 370
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 146
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    .line 147
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 317
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    :goto_0
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->l:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->b()V

    goto :goto_0

    .line 323
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->v:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 324
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/downloads/ActivityAniconPackageDetail;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 325
    const-string v1, "ANICON_PACKAGE_ID"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 326
    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 329
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 332
    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->f(Ljava/lang/String;)V

    .line 334
    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 158
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 160
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 161
    const-string v1, "layoutResId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->o:I

    .line 163
    iput-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->p:Ljava/lang/String;

    .line 165
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->n:Ljava/util/Map;

    .line 168
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/common/f/c;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->c:Lcom/sec/common/f/c;

    .line 169
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->f()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/common/f/c;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->d:Lcom/sec/common/f/c;

    .line 171
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    invoke-static {v0, v2}, Lcom/sec/chaton/d/j;->a(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/chaton/d/j;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->b:Lcom/sec/chaton/d/j;

    .line 172
    new-instance v0, Lcom/sec/chaton/e/a/u;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->t:Lcom/sec/chaton/e/a/u;

    .line 173
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 177
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 183
    if-eqz p3, :cond_1

    .line 184
    const-string v0, "currentTabId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 185
    const-string v0, "currentIndicatorPosition"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    move-object v3, v1

    move v1, v0

    .line 190
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 191
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const-string v5, "Restored. PackageId: "

    aput-object v5, v0, v2

    aput-object v3, v0, v7

    const/4 v5, 0x2

    const-string v6, ", Tab Indicator Position: "

    aput-object v6, v0, v5

    const/4 v5, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v5

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->a:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :cond_0
    iget v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->o:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 196
    const v0, 0x7f07028b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->f:Landroid/widget/HorizontalScrollView;

    .line 197
    const v0, 0x7f070292

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->g:Landroid/widget/LinearLayout;

    .line 198
    const v0, 0x7f07028a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    .line 199
    const v0, 0x7f07028c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->i:Landroid/view/View;

    .line 202
    const v0, 0x7f07028d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->j:Landroid/view/View;

    .line 203
    const v0, 0x7f07028e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->l:Landroid/widget/Button;

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->l:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    const v0, 0x7f07028f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->k:Landroid/widget/TextView;

    .line 208
    const v0, 0x7f070290

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->u:Landroid/widget/RelativeLayout;

    .line 209
    const v0, 0x7f070291

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->v:Landroid/widget/LinearLayout;

    .line 210
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->c()Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    .line 217
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    iget-object v5, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    iget-object v6, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->d:Lcom/sec/common/f/c;

    invoke-direct {v0, v5, v4, v7, v6}, Lcom/sec/chaton/multimedia/emoticon/anicon/a;-><init>(Landroid/content/Context;Landroid/database/Cursor;ILcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    invoke-virtual {v0, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 221
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->h:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 224
    invoke-direct {p0, v3}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->c(Ljava/lang/String;)V

    .line 227
    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Lcom/sec/chaton/settings/downloads/ab;)V

    .line 228
    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->a(Lcom/sec/chaton/settings/downloads/ac;)V

    .line 230
    iput v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->s:I

    .line 232
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->f:Landroid/widget/HorizontalScrollView;

    new-instance v3, Lcom/sec/chaton/multimedia/emoticon/anicon/c;

    invoke-direct {v3, p0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/c;-><init>(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;I)V

    invoke-virtual {v0, v3}, Landroid/widget/HorizontalScrollView;->post(Ljava/lang/Runnable;)Z

    .line 240
    return-object v2

    :cond_1
    move v1, v2

    move-object v3, v4

    .line 187
    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 281
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->c:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 284
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->d:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 285
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 262
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 265
    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->b(Lcom/sec/chaton/settings/downloads/ab;)V

    .line 266
    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/u;->b(Lcom/sec/chaton/settings/downloads/ac;)V

    .line 268
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->m:Lcom/sec/chaton/multimedia/emoticon/anicon/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->changeCursor(Landroid/database/Cursor;)V

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 273
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->q:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->d()V

    .line 277
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 151
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->e:Landroid/app/Activity;

    .line 154
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 302
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;

    .line 303
    iget-object v0, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->c:Ljava/lang/String;

    .line 305
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->r:Lcom/sec/chaton/multimedia/emoticon/anicon/h;

    if-eqz v1, :cond_0

    .line 310
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->r:Lcom/sec/chaton/multimedia/emoticon/anicon/h;

    invoke-interface {v1, p0, v0}, Lcom/sec/chaton/multimedia/emoticon/anicon/h;->a(Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 245
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 247
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->n:Ljava/util/Map;

    const-string v1, "-1"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 248
    const v1, 0x7f0701cf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 250
    invoke-static {}, Lcom/sec/chaton/settings/downloads/u;->a()I

    move-result v1

    .line 252
    if-nez v1, :cond_0

    .line 253
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    :goto_0
    return-void

    .line 255
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 256
    invoke-static {v0, v1}, Lcom/sec/chaton/util/cp;->a(Landroid/widget/TextView;I)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 289
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 291
    const-string v0, "currentTabId"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    iget v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->s:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 294
    const-string v0, "currentIndicatorPosition"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->f:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 298
    :goto_0
    return-void

    .line 296
    :cond_0
    const-string v0, "currentIndicatorPosition"

    iget v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/AniconContainer;->s:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method
