.class Lcom/sec/chaton/multimedia/multisend/g;
.super Landroid/widget/ArrayAdapter;
.source "GalleryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/chaton/multimedia/multisend/k;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Lcom/sec/common/f/c;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;"
        }
    .end annotation
.end field

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/common/f/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/k;",
            ">;",
            "Lcom/sec/common/f/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 40
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/g;->b:Landroid/content/Context;

    .line 42
    iput-object p4, p0, Lcom/sec/chaton/multimedia/multisend/g;->d:Lcom/sec/common/f/c;

    .line 44
    iput p2, p0, Lcom/sec/chaton/multimedia/multisend/g;->a:I

    .line 46
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/g;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/multisend/g;->c:Landroid/view/LayoutInflater;

    .line 48
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->a()Landroid/app/Application;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 49
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/multisend/g;->f:I

    .line 52
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/g;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 104
    :cond_0
    return-void
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/multisend/PreviewData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/g;->e:Ljava/util/ArrayList;

    .line 98
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 58
    if-nez p2, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/g;->c:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/chaton/multimedia/multisend/g;->a:I

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 60
    new-instance v1, Lcom/sec/chaton/multimedia/multisend/h;

    invoke-direct {v1}, Lcom/sec/chaton/multimedia/multisend/h;-><init>()V

    .line 61
    const v0, 0x7f070446

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/multisend/h;->a:Landroid/widget/ImageView;

    .line 62
    const v0, 0x7f070447

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/multisend/h;->b:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f070449

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/sec/chaton/multimedia/multisend/h;->c:Landroid/widget/TextView;

    .line 64
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v8, v1

    .line 69
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/multisend/g;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/sec/chaton/multimedia/multisend/k;

    .line 70
    iget-object v0, v8, Lcom/sec/chaton/multimedia/multisend/h;->b:Landroid/widget/TextView;

    iget-object v1, v2, Lcom/sec/chaton/multimedia/multisend/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, v8, Lcom/sec/chaton/multimedia/multisend/h;->c:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v2, Lcom/sec/chaton/multimedia/multisend/k;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    const v0, 0x7f070448

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v8, Lcom/sec/chaton/multimedia/multisend/h;->d:Landroid/widget/RelativeLayout;

    .line 77
    iget-object v0, v8, Lcom/sec/chaton/multimedia/multisend/h;->b:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/chaton/multimedia/multisend/g;->f:I

    int-to-float v1, v1

    const/high16 v3, 0x43250000    # 165.0f

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    sub-float/2addr v1, v3

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 79
    iget-object v0, v2, Lcom/sec/chaton/multimedia/multisend/k;->f:Ljava/lang/String;

    iput-object v0, v8, Lcom/sec/chaton/multimedia/multisend/h;->e:Ljava/lang/String;

    .line 81
    new-instance v0, Lcom/sec/chaton/multimedia/multisend/i;

    iget-object v1, v2, Lcom/sec/chaton/multimedia/multisend/k;->c:Ljava/lang/String;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/multisend/k;->d:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/sec/chaton/multimedia/multisend/h;->e:Ljava/lang/String;

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/chaton/multimedia/multisend/i;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Landroid/widget/CheckBox;Landroid/widget/ImageView;)V

    .line 83
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/g;->d:Lcom/sec/common/f/c;

    iget-object v2, v8, Lcom/sec/chaton/multimedia/multisend/h;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    .line 85
    return-object p2

    .line 66
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/multisend/h;

    move-object v8, v0

    goto :goto_0
.end method
