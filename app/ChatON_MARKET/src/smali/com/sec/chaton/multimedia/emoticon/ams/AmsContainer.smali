.class public Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;
.super Landroid/support/v4/app/Fragment;
.source "AmsContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/chaton/settings/downloads/s;
.implements Lcom/sec/chaton/settings/downloads/t;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Landroid/os/Handler;


# instance fields
.field private c:Ljava/lang/String;

.field private d:Landroid/app/Activity;

.field private e:Landroid/widget/GridView;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Lcom/sec/chaton/multimedia/emoticon/ams/f;

.field private o:Lcom/sec/chaton/multimedia/emoticon/ams/g;

.field private p:Lcom/sec/vip/amschaton/a/f;

.field private q:Lcom/sec/common/f/c;

.field private r:Lcom/sec/chaton/multimedia/emoticon/ams/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->a:Ljava/lang/String;

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->b:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 355
    return-void
.end method

.method public static final a()Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/chaton/d/e;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 333
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->b:Landroid/os/Handler;

    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/ams/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/multimedia/emoticon/ams/a;-><init>(Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;Lcom/sec/chaton/d/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 341
    return-void
.end method

.method public a(Lcom/sec/chaton/multimedia/emoticon/ams/c;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->r:Lcom/sec/chaton/multimedia/emoticon/ams/c;

    .line 90
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 274
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    const-string v0, "download"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 279
    const-string v0, "download"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->c:Ljava/lang/String;

    .line 281
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 285
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->e:Landroid/widget/GridView;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 289
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/q;->c()I

    move-result v0

    if-nez v0, :cond_2

    .line 290
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->h:Landroid/widget/TextView;

    const v1, 0x7f0b034f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 295
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->e:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 292
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->h:Landroid/widget/TextView;

    const v1, 0x7f0b01d8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 296
    :cond_3
    const-string v0, "recent"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 297
    const-string v0, "recent"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->c:Ljava/lang/String;

    .line 299
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 300
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 301
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 303
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->e:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setVisibility(I)V

    .line 304
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->p:Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/f;->c()Landroid/database/Cursor;

    move-result-object v0

    .line 308
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->n:Lcom/sec/chaton/multimedia/emoticon/ams/f;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/emoticon/ams/f;->changeCursor(Landroid/database/Cursor;)V

    .line 310
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->e:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->n:Lcom/sec/chaton/multimedia/emoticon/ams/f;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 312
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->n:Lcom/sec/chaton/multimedia/emoticon/ams/f;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/ams/f;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->e:Landroid/widget/GridView;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 316
    :cond_4
    const-string v0, "myWorks"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    const-string v0, "myWorks"

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->c:Ljava/lang/String;

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 320
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 321
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 323
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->e:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->e:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->o:Lcom/sec/chaton/multimedia/emoticon/ams/g;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_0
.end method

.method public b(Lcom/sec/chaton/d/e;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 345
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->b:Landroid/os/Handler;

    new-instance v1, Lcom/sec/chaton/multimedia/emoticon/ams/b;

    invoke-direct {v1, p0, p1}, Lcom/sec/chaton/multimedia/emoticon/ams/b;-><init>(Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;Lcom/sec/chaton/d/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 353
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 96
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->d:Landroid/app/Activity;

    .line 97
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->k:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    const-string v0, "download"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->a(Ljava/lang/String;)V

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->l:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 224
    const-string v0, "recent"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->m:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 226
    const-string v0, "myWorks"

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 227
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->i:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->d:Landroid/app/Activity;

    const-class v2, Lcom/sec/chaton/settings/downloads/ActivityAmsItemDownloads;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 229
    const-string v1, "amsType"

    sget-object v2, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 231
    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 101
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 103
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->q:Lcom/sec/common/f/c;

    .line 106
    iput-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->c:Ljava/lang/String;

    .line 111
    if-eqz p3, :cond_3

    .line 112
    const-string v0, "currentTabId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 116
    const-string v0, "myWorks"

    move-object v2, v0

    .line 120
    :goto_1
    const v0, 0x7f030058

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 122
    const v0, 0x7f070245

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->e:Landroid/widget/GridView;

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->e:Landroid/widget/GridView;

    invoke-virtual {v0, v4}, Landroid/widget/GridView;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->e:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 127
    new-instance v0, Lcom/sec/vip/amschaton/a/f;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->d:Landroid/app/Activity;

    invoke-direct {v0, v4}, Lcom/sec/vip/amschaton/a/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->p:Lcom/sec/vip/amschaton/a/f;

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->p:Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/f;->b()Lcom/sec/vip/amschaton/a/f;

    .line 131
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/q;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 132
    sget-boolean v0, Lcom/sec/chaton/util/y;->d:Z

    if-eqz v0, :cond_0

    .line 133
    const-string v0, "AMSFileManager isn\'t loaded, loading now."

    sget-object v4, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->a:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_0
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->d:Landroid/app/Activity;

    invoke-virtual {v0, v4}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;)Z

    .line 139
    :cond_1
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/ams/f;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->d:Landroid/app/Activity;

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->q:Lcom/sec/common/f/c;

    invoke-direct {v0, v4, v1, v5, v6}, Lcom/sec/chaton/multimedia/emoticon/ams/f;-><init>(Landroid/content/Context;Landroid/database/Cursor;ILcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->n:Lcom/sec/chaton/multimedia/emoticon/ams/f;

    .line 140
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/ams/g;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->d:Landroid/app/Activity;

    iget-object v4, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->q:Lcom/sec/common/f/c;

    invoke-direct {v0, v1, v4}, Lcom/sec/chaton/multimedia/emoticon/ams/g;-><init>(Landroid/content/Context;Lcom/sec/common/f/c;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->o:Lcom/sec/chaton/multimedia/emoticon/ams/g;

    .line 142
    const v0, 0x7f070247

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->f:Landroid/view/View;

    .line 144
    const v0, 0x7f070248

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->g:Landroid/view/View;

    .line 145
    const v0, 0x7f07024a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->h:Landroid/widget/TextView;

    .line 146
    const v0, 0x7f070249

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->i:Landroid/widget/Button;

    .line 147
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    const v0, 0x7f07024b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->k:Landroid/view/View;

    .line 150
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->k:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    const v0, 0x7f07024c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->j:Landroid/widget/TextView;

    .line 154
    const v0, 0x7f07024d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->l:Landroid/view/View;

    .line 155
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->l:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    const v0, 0x7f07024e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->m:Landroid/view/View;

    .line 158
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->m:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    invoke-virtual {p0, v2}, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->a(Ljava/lang/String;)V

    .line 162
    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/settings/downloads/s;)V

    .line 163
    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/settings/downloads/t;)V

    .line 165
    return-object v3

    :cond_2
    move-object v2, v0

    goto/16 :goto_1

    :cond_3
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 188
    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/q;->b(Lcom/sec/chaton/settings/downloads/s;)V

    .line 189
    invoke-static {p0}, Lcom/sec/chaton/settings/downloads/q;->b(Lcom/sec/chaton/settings/downloads/t;)V

    .line 191
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->n:Lcom/sec/chaton/multimedia/emoticon/ams/f;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->n:Lcom/sec/chaton/multimedia/emoticon/ams/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/emoticon/ams/f;->changeCursor(Landroid/database/Cursor;)V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->p:Lcom/sec/vip/amschaton/a/f;

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->p:Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/f;->a()V

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->q:Lcom/sec/common/f/c;

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->q:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 202
    :cond_2
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 206
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->d:Landroid/app/Activity;

    .line 209
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->c:Ljava/lang/String;

    const-string v1, "recent"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 238
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->n:Lcom/sec/chaton/multimedia/emoticon/ams/f;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/multimedia/emoticon/ams/f;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 240
    const-string v1, "ams_path"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 241
    const-string v2, "ams_type"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 243
    const/4 v0, 0x0

    .line 245
    const/16 v3, 0x7d0

    if-ne v2, v3, :cond_2

    .line 246
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    .line 253
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->r:Lcom/sec/chaton/multimedia/emoticon/ams/c;

    invoke-interface {v2, p0, v0, v1}, Lcom/sec/chaton/multimedia/emoticon/ams/c;->a(Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;Lcom/sec/chaton/multimedia/emoticon/ams/d;Ljava/lang/String;)V

    .line 265
    :cond_1
    :goto_1
    return-void

    .line 247
    :cond_2
    const/16 v3, 0x7d2

    if-ne v2, v3, :cond_3

    .line 248
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->b:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    goto :goto_0

    .line 249
    :cond_3
    const/16 v3, 0x7d1

    if-ne v2, v3, :cond_0

    .line 250
    sget-object v0, Lcom/sec/chaton/multimedia/emoticon/ams/d;->c:Lcom/sec/chaton/multimedia/emoticon/ams/d;

    goto :goto_0

    .line 254
    :cond_4
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->c:Ljava/lang/String;

    const-string v1, "myWorks"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->o:Lcom/sec/chaton/multimedia/emoticon/ams/g;

    invoke-virtual {v0, p3}, Lcom/sec/chaton/multimedia/emoticon/ams/g;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 257
    const-wide/16 v1, 0x0

    cmp-long v1, p4, v1

    if-nez v1, :cond_5

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->r:Lcom/sec/chaton/multimedia/emoticon/ams/c;

    invoke-interface {v0, p0}, Lcom/sec/chaton/multimedia/emoticon/ams/c;->a(Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;)V

    goto :goto_1

    .line 260
    :cond_5
    invoke-static {p4, p5}, Lcom/sec/chaton/multimedia/emoticon/ams/d;->a(J)Lcom/sec/chaton/multimedia/emoticon/ams/d;

    move-result-object v1

    .line 262
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->r:Lcom/sec/chaton/multimedia/emoticon/ams/c;

    invoke-interface {v2, p0, v1, v0}, Lcom/sec/chaton/multimedia/emoticon/ams/c;->a(Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;Lcom/sec/chaton/multimedia/emoticon/ams/d;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 170
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 172
    sget-object v0, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v0

    .line 174
    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->j:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->o:Lcom/sec/chaton/multimedia/emoticon/ams/g;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/emoticon/ams/g;->notifyDataSetChanged()V

    .line 182
    return-void

    .line 177
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->j:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 178
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->j:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/sec/chaton/util/cp;->a(Landroid/widget/TextView;I)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 213
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 216
    const-string v0, "currentTabId"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/ams/AmsContainer;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    return-void
.end method
