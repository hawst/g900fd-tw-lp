.class public Lcom/sec/chaton/multimedia/audio/d;
.super Landroid/os/AsyncTask;
.source "PlaySoundTask.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;",
        "Landroid/media/MediaPlayer$OnCompletionListener;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field a:Ljava/lang/String;

.field b:Landroid/os/Handler;

.field c:Lcom/sec/chaton/multimedia/audio/g;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Landroid/media/MediaPlayer;

.field private i:Landroid/media/AudioManager;

.field private j:Z

.field private k:Lcom/sec/chaton/multimedia/audio/f;

.field private l:Landroid/media/AudioManager$OnAudioFocusChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/chaton/multimedia/audio/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/audio/d;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 28
    iput-boolean v1, p0, Lcom/sec/chaton/multimedia/audio/d;->g:Z

    .line 40
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->g:Lcom/sec/chaton/multimedia/audio/f;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->k:Lcom/sec/chaton/multimedia/audio/f;

    .line 508
    new-instance v0, Lcom/sec/chaton/multimedia/audio/e;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/audio/e;-><init>(Lcom/sec/chaton/multimedia/audio/d;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->l:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 145
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/sec/chaton/multimedia/audio/d;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Z)V

    .line 146
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Z)V
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/d;->g:Z

    .line 40
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->g:Lcom/sec/chaton/multimedia/audio/f;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->k:Lcom/sec/chaton/multimedia/audio/f;

    .line 508
    new-instance v0, Lcom/sec/chaton/multimedia/audio/e;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/audio/e;-><init>(Lcom/sec/chaton/multimedia/audio/d;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->l:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 149
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/chaton/multimedia/audio/d;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Z)V

    .line 150
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/d;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/audio/d;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/sec/chaton/multimedia/audio/d;->j:Z

    return p1
.end method

.method private d()V
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 411
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/d;->j:Z

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 435
    :cond_1
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 525
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 526
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 527
    const-string v1, "ChatON"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 528
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 529
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/chaton/multimedia/audio/f;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->k:Lcom/sec/chaton/multimedia/audio/f;

    return-object v0
.end method

.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 269
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/d;->g:Z

    if-eqz v0, :cond_4

    .line 270
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->f:Ljava/lang/String;

    .line 272
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/audio/d;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 273
    invoke-static {}, Lcom/sec/common/util/a/a;->a()Lcom/sec/common/util/a/a;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/util/a/a;->a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 274
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->e:Lcom/sec/chaton/multimedia/audio/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/audio/d;->a(Lcom/sec/chaton/multimedia/audio/f;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 307
    :goto_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->i:Landroid/media/AudioManager;

    .line 308
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->i:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/d;->l:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 321
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/d;->e()V

    .line 323
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    .line 325
    :try_start_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 328
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->e:Ljava/lang/String;

    const-string v1, "file:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->e:Ljava/lang/String;

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/d;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/d;->j:Z

    .line 336
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    .line 374
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/audio/d;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/d;->j:Z

    if-eqz v0, :cond_5

    .line 377
    const-wide/16 v0, 0x64

    :try_start_2
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 378
    :catch_0
    move-exception v0

    .line 379
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 275
    :catch_1
    move-exception v0

    .line 276
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 277
    sget-object v1, Lcom/sec/chaton/multimedia/audio/d;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 279
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/chaton/multimedia/audio/d;->cancel(Z)Z

    .line 280
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->d:Lcom/sec/chaton/multimedia/audio/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/audio/d;->a(Lcom/sec/chaton/multimedia/audio/f;)V

    goto/16 :goto_0

    .line 281
    :catch_2
    move-exception v0

    .line 282
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_3

    .line 283
    sget-object v1, Lcom/sec/chaton/multimedia/audio/d;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 285
    :cond_3
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->c:Lcom/sec/chaton/multimedia/audio/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/audio/d;->a(Lcom/sec/chaton/multimedia/audio/f;)V

    .line 286
    invoke-virtual {p0, v3}, Lcom/sec/chaton/multimedia/audio/d;->cancel(Z)Z

    goto/16 :goto_0

    .line 290
    :cond_4
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->e:Lcom/sec/chaton/multimedia/audio/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/audio/d;->a(Lcom/sec/chaton/multimedia/audio/f;)V

    goto/16 :goto_0

    .line 339
    :catch_3
    move-exception v0

    .line 340
    sget-object v1, Lcom/sec/chaton/multimedia/audio/d;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 341
    const-string v0, "null"

    .line 391
    :goto_2
    return-object v0

    .line 342
    :catch_4
    move-exception v0

    .line 343
    sget-object v1, Lcom/sec/chaton/multimedia/audio/d;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 344
    const-string v0, "null"

    goto :goto_2

    .line 345
    :catch_5
    move-exception v0

    .line 346
    sget-object v1, Lcom/sec/chaton/multimedia/audio/d;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 347
    const-string v0, "null"

    goto :goto_2

    .line 348
    :catch_6
    move-exception v0

    .line 349
    sget-object v1, Lcom/sec/chaton/multimedia/audio/d;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 350
    const-string v0, "null"

    goto :goto_2

    .line 391
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->e:Ljava/lang/String;

    goto :goto_2
.end method

.method a(Lcom/sec/chaton/multimedia/audio/f;)V
    .locals 2

    .prologue
    .line 256
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/d;->k:Lcom/sec/chaton/multimedia/audio/f;

    .line 257
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->b:Landroid/os/Handler;

    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/audio/f;->a()I

    move-result v1

    invoke-static {v0, v1, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->c:Lcom/sec/chaton/multimedia/audio/g;

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->c:Lcom/sec/chaton/multimedia/audio/g;

    invoke-interface {v0, p0}, Lcom/sec/chaton/multimedia/audio/g;->a(Lcom/sec/chaton/multimedia/audio/d;)V

    .line 263
    :cond_1
    return-void
.end method

.method public a(Lcom/sec/chaton/multimedia/audio/g;)V
    .locals 0

    .prologue
    .line 559
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/d;->c:Lcom/sec/chaton/multimedia/audio/g;

    .line 560
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 488
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/d;->d()V

    .line 490
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->g:Lcom/sec/chaton/multimedia/audio/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/audio/d;->a(Lcom/sec/chaton/multimedia/audio/f;)V

    .line 494
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 495
    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Z)V
    .locals 3

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/chaton/multimedia/audio/d;->a:Ljava/lang/String;

    .line 154
    iput-boolean p4, p0, Lcom/sec/chaton/multimedia/audio/d;->g:Z

    .line 155
    iput-object p2, p0, Lcom/sec/chaton/multimedia/audio/d;->f:Ljava/lang/String;

    .line 156
    iput-object p2, p0, Lcom/sec/chaton/multimedia/audio/d;->e:Ljava/lang/String;

    .line 157
    iput-object p3, p0, Lcom/sec/chaton/multimedia/audio/d;->b:Landroid/os/Handler;

    .line 160
    if-eqz p4, :cond_0

    .line 163
    if-eqz p2, :cond_0

    .line 164
    :try_start_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/util/a/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 165
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 166
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->e:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "constructor(), mFilePath (is_url/filepath) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/audio/d;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_1
    return-void

    .line 168
    :catch_0
    move-exception v0

    .line 169
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 170
    sget-object v1, Lcom/sec/chaton/multimedia/audio/d;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 204
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 205
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/audio/d;->b:Landroid/os/Handler;

    .line 186
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/d;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 499
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/audio/d;->d()V

    .line 501
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->h:Lcom/sec/chaton/multimedia/audio/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/audio/d;->a(Lcom/sec/chaton/multimedia/audio/f;)V

    .line 505
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 506
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 533
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/audio/d;->j:Z

    .line 534
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/d;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 223
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/audio/d;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 224
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    sget-object v0, Lcom/sec/chaton/multimedia/audio/f;->b:Lcom/sec/chaton/multimedia/audio/f;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/audio/d;->a(Lcom/sec/chaton/multimedia/audio/f;)V

    .line 252
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 253
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/chaton/multimedia/audio/d;->a([Ljava/lang/Integer;)V

    return-void
.end method
