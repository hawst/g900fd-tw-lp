.class public Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;
.super Landroid/support/v4/app/ListFragment;
.source "ReadVCardFragment.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lcom/sec/chaton/multimedia/vcard/b;

.field private d:Lcom/sec/chaton/multimedia/vcard/i;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/chaton/multimedia/vcard/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/sec/chaton/multimedia/vcard/b;

.field private g:Landroid/view/MenuItem;

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->b:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    .line 38
    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->d:Lcom/sec/chaton/multimedia/vcard/i;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->e:Ljava/util/List;

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 87
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.INSERT"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 88
    const-string v0, "vnd.android.cursor.dir/contact"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    const-string v0, "name"

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    const-string v0, "company"

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v0, "notes"

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 96
    :try_start_0
    const-string v2, "phone_type"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->a:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 97
    const-string v2, "phone"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    .line 101
    :goto_0
    :try_start_1
    const-string v2, "secondary_phone_type"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->a:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 102
    const-string v2, "secondary_phone"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 106
    :goto_1
    :try_start_2
    const-string v2, "tertiary_phone_type"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    const/4 v3, 0x2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->a:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 107
    const-string v2, "tertiary_phone"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    const/4 v3, 0x2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 112
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 114
    :try_start_3
    const-string v2, "email_type"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/d;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/d;->a:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 115
    const-string v2, "email"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/d;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 119
    :goto_3
    :try_start_4
    const-string v2, "secondary_email_type"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/d;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/d;->a:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 120
    const-string v2, "secondary_email"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/d;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 124
    :goto_4
    :try_start_5
    const-string v2, "tertiary_email_type"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    const/4 v3, 0x2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/d;

    iget v0, v0, Lcom/sec/chaton/multimedia/vcard/d;->a:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 125
    const-string v2, "tertiary_email"

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    const/4 v3, 0x2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/d;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 130
    :cond_1
    :goto_5
    invoke-virtual {p0, v1, v4}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 131
    return-void

    .line 126
    :catch_0
    move-exception v0

    goto :goto_5

    .line 121
    :catch_1
    move-exception v0

    goto :goto_4

    .line 116
    :catch_2
    move-exception v0

    goto :goto_3

    .line 108
    :catch_3
    move-exception v0

    goto/16 :goto_2

    .line 103
    :catch_4
    move-exception v0

    goto/16 :goto_1

    .line 98
    :catch_5
    move-exception v0

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 277
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/ReadVCardActivity;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardActivity;->c()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 280
    :cond_0
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 135
    new-instance v3, Lcom/sec/chaton/multimedia/vcard/l;

    invoke-direct {v3}, Lcom/sec/chaton/multimedia/vcard/l;-><init>()V

    .line 139
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 140
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->b:Ljava/lang/String;

    const-string v2, "file://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 141
    array-length v2, v0

    if-ne v2, v5, :cond_1

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->b:Ljava/lang/String;

    .line 152
    :goto_0
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 153
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    :goto_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 156
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 162
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_0

    .line 163
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 162
    :cond_0
    throw v0

    .line 143
    :cond_1
    array-length v2, v0

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    .line 144
    aget-object v0, v0, v5

    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    :cond_3
    move-object v0, v1

    goto :goto_0

    .line 158
    :cond_4
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "**** READ VCARD : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->a:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/chaton/multimedia/vcard/l;->a(Ljava/lang/String;)Lcom/sec/chaton/multimedia/vcard/b;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 162
    if-eqz v2, :cond_5

    .line 163
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 166
    :cond_5
    return-void

    .line 162
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method private c()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 216
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->f:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->f:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->f:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 226
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->f:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    .line 230
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    if-eqz v0, :cond_5

    move v6, v7

    .line 231
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v6, :cond_5

    .line 233
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/vcard/f;->c:Ljava/lang/String;

    .line 235
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/multimedia/vcard/f;

    iget v1, v1, Lcom/sec/chaton/multimedia/vcard/f;->a:I

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/multimedia/vcard/f;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/f;->b:Ljava/lang/String;

    const/4 v4, 0x5

    if-nez v6, :cond_4

    move v5, v8

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/vcard/c;-><init>(ILjava/lang/String;Ljava/lang/String;IZ)V

    .line 236
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_4
    move v5, v7

    .line 235
    goto :goto_1

    .line 239
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    if-eqz v0, :cond_7

    move v6, v7

    .line 240
    :goto_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v6, :cond_7

    .line 243
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/d;

    iget-object v3, v0, Lcom/sec/chaton/multimedia/vcard/d;->c:Ljava/lang/String;

    .line 245
    iget-object v9, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->e:Ljava/util/List;

    new-instance v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/multimedia/vcard/d;

    iget v1, v1, Lcom/sec/chaton/multimedia/vcard/d;->a:I

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/chaton/multimedia/vcard/d;

    iget-object v2, v2, Lcom/sec/chaton/multimedia/vcard/d;->b:Ljava/lang/String;

    if-nez v6, :cond_6

    move v5, v8

    :goto_3
    move v4, v8

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/vcard/c;-><init>(ILjava/lang/String;Ljava/lang/String;IZ)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    :cond_6
    move v5, v7

    .line 245
    goto :goto_3

    .line 249
    :cond_7
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 250
    iget-object v6, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->e:Ljava/util/List;

    new-instance v0, Lcom/sec/chaton/multimedia/vcard/c;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v2, v1, Lcom/sec/chaton/multimedia/vcard/b;->d:Ljava/lang/String;

    const/4 v3, 0x0

    const/16 v4, 0x9

    move v1, v7

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/vcard/c;-><init>(ILjava/lang/String;Ljava/lang/String;IZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 255
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v1, v1, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->h:Ljava/lang/String;

    .line 274
    :cond_9
    :goto_4
    return-void

    .line 258
    :cond_a
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->h:Ljava/lang/String;

    goto :goto_4

    .line 261
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 262
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->h:Ljava/lang/String;

    goto :goto_4

    .line 264
    :cond_c
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 265
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->h:Ljava/lang/String;

    goto :goto_4

    .line 266
    :cond_d
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    if-eqz v0, :cond_e

    .line 267
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->f:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/f;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/f;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->h:Ljava/lang/String;

    goto :goto_4

    .line 268
    :cond_e
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    if-eqz v0, :cond_9

    .line 269
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/b;->g:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/d;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/d;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->h:Ljava/lang/String;

    goto :goto_4
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    .line 188
    if-eqz p1, :cond_0

    .line 190
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 191
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 194
    :catch_0
    move-exception v0

    .line 195
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 201
    if-eqz p1, :cond_0

    .line 203
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mailto:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 204
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SENDTO"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 206
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/sec/chaton/buddy/BuddyProfileActivity;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 209
    :catch_0
    move-exception v0

    .line 210
    sget-object v1, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 284
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 285
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 288
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "URI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "URI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->b:Ljava/lang/String;

    .line 54
    :cond_0
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/b;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/vcard/b;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c:Lcom/sec/chaton/multimedia/vcard/b;

    .line 55
    new-instance v0, Lcom/sec/chaton/multimedia/vcard/b;

    invoke-direct {v0}, Lcom/sec/chaton/multimedia/vcard/b;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->f:Lcom/sec/chaton/multimedia/vcard/b;

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->setHasOptionsMenu(Z)V

    .line 58
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 306
    const v0, 0x7f0f0023

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 308
    const v0, 0x7f0705a6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->g:Landroid/view/MenuItem;

    .line 309
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->g:Landroid/view/MenuItem;

    const v1, 0x7f0b003c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 310
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 311
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 62
    const v0, 0x7f0300c6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 65
    :try_start_0
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 72
    :goto_0
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->c()V

    .line 74
    new-instance v1, Lcom/sec/chaton/multimedia/vcard/i;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->e:Ljava/util/List;

    invoke-direct {v1, v2, v3}, Lcom/sec/chaton/multimedia/vcard/i;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->d:Lcom/sec/chaton/multimedia/vcard/i;

    .line 75
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->d:Lcom/sec/chaton/multimedia/vcard/i;

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 77
    return-object v0

    .line 66
    :catch_0
    move-exception v1

    .line 67
    sget-object v2, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :catch_1
    move-exception v1

    .line 69
    sget-object v2, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 171
    invoke-super/range {p0 .. p5}, Landroid/support/v4/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 174
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcard/j;

    .line 175
    iget-object v1, v0, Lcom/sec/chaton/multimedia/vcard/j;->c:Lcom/sec/chaton/multimedia/vcard/c;

    .line 177
    if-eqz v1, :cond_0

    .line 178
    const/4 v2, 0x5

    iget v3, v1, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    if-ne v2, v3, :cond_1

    .line 179
    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/j;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->a(Ljava/lang/CharSequence;)V

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    const/4 v2, 0x1

    iget v1, v1, Lcom/sec/chaton/multimedia/vcard/c;->d:I

    if-ne v2, v1, :cond_0

    .line 181
    iget-object v0, v0, Lcom/sec/chaton/multimedia/vcard/j;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->b(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 292
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 293
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 294
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 301
    :goto_0
    return v0

    .line 297
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0705a6

    if-ne v1, v2, :cond_1

    .line 298
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->a()V

    goto :goto_0

    .line 301
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->h:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/vcard/ReadVCardFragment;->a(Ljava/lang/String;)V

    .line 83
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 84
    return-void
.end method
