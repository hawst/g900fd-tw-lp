.class public Lcom/sec/chaton/multimedia/multisend/i;
.super Lcom/sec/common/f/a;
.source "GalleryDispatcherTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/f/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:J

.field private d:Z

.field private e:Z

.field private i:Ljava/lang/String;

.field private j:Landroid/widget/CheckBox;

.field private k:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/chaton/multimedia/multisend/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/multisend/i;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Landroid/widget/CheckBox;Landroid/widget/ImageView;)V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/common/f/a;-><init>(Ljava/lang/Object;)V

    .line 34
    if-eqz p2, :cond_1

    .line 35
    invoke-static {p2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/chaton/multimedia/multisend/i;->c:J

    .line 39
    :goto_0
    iput-object p1, p0, Lcom/sec/chaton/multimedia/multisend/i;->b:Ljava/lang/String;

    .line 40
    iput-boolean p3, p0, Lcom/sec/chaton/multimedia/multisend/i;->d:Z

    .line 41
    iput-boolean p4, p0, Lcom/sec/chaton/multimedia/multisend/i;->e:Z

    .line 42
    iput-object p5, p0, Lcom/sec/chaton/multimedia/multisend/i;->i:Ljava/lang/String;

    .line 43
    iput-object p6, p0, Lcom/sec/chaton/multimedia/multisend/i;->j:Landroid/widget/CheckBox;

    .line 44
    iput-object p7, p0, Lcom/sec/chaton/multimedia/multisend/i;->k:Landroid/widget/ImageView;

    .line 46
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[init] key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/multisend/i;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_0
    return-void

    .line 37
    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/chaton/multimedia/multisend/i;->c:J

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 185
    invoke-super {p0}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 3

    .prologue
    .line 134
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onPostDispatch] key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->g:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/multisend/i;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    if-eqz p1, :cond_3

    :try_start_0
    move-object v0, p1

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 140
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/i;->a()Landroid/widget/ImageView;

    move-result-object v1

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 141
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->j:Landroid/widget/CheckBox;

    if-eqz v1, :cond_1

    .line 142
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->j:Landroid/widget/CheckBox;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 143
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->j:Landroid/widget/CheckBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 145
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->k:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 146
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->k:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/i;->a()Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 170
    :cond_2
    :goto_0
    return-void

    .line 153
    :cond_3
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->j:Landroid/widget/CheckBox;

    if-eqz v1, :cond_4

    .line 154
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->j:Landroid/widget/CheckBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 155
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->j:Landroid/widget/CheckBox;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 157
    :cond_4
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->k:Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    .line 158
    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->k:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 159
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/i;->a()Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 162
    :cond_5
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/i;->a()Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020440

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 164
    :catch_0
    move-exception v1

    .line 166
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :catch_1
    move-exception v1

    .line 168
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/i;->a()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 54
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 58
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onDispatch] content : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/multisend/i;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/multisend/i;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/i;->b:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 69
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/i;->d:Z

    if-eqz v0, :cond_3

    .line 70
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v3, p0, Lcom/sec/chaton/multimedia/multisend/i;->c:J

    const/4 v1, 0x3

    const/4 v5, 0x0

    invoke-static {v0, v3, v4, v1, v5}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 71
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/i;->i:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 72
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/i;->i:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 74
    :goto_0
    if-eqz v0, :cond_2

    .line 75
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 76
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 129
    :cond_1
    :goto_1
    return-object v0

    .line 81
    :cond_2
    if-eqz v1, :cond_6

    move-object v0, v1

    .line 82
    goto :goto_1

    .line 87
    :cond_3
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/multisend/i;->e:Z

    if-eqz v0, :cond_4

    .line 88
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/multisend/i;->b:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v0, v1, v3, v4, v5}, Lcom/sec/chaton/util/r;->a(Landroid/content/Context;Ljava/io/File;ZZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 93
    :cond_4
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v3, p0, Lcom/sec/chaton/multimedia/multisend/i;->c:J

    const/4 v1, 0x1

    const/4 v5, 0x0

    invoke-static {v0, v3, v4, v1, v5}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 94
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/i;->i:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 95
    iget-object v0, p0, Lcom/sec/chaton/multimedia/multisend/i;->i:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/chaton/util/ad;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 97
    :goto_2
    if-eqz v0, :cond_5

    .line 98
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 99
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 112
    :catch_0
    move-exception v0

    .line 114
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v0, v2

    .line 115
    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 105
    goto :goto_1

    .line 116
    :catch_1
    move-exception v0

    .line 117
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v0, v2

    .line 118
    goto :goto_1

    .line 121
    :cond_6
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_7

    .line 122
    const-string v0, "thumbBitmap1 is null"

    sget-object v1, Lcom/sec/chaton/multimedia/multisend/i;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_7
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_8

    .line 126
    const-string v0, "return null!!!"

    sget-object v1, Lcom/sec/chaton/multimedia/multisend/i;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move-object v0, v2

    .line 129
    goto :goto_1

    :cond_9
    move-object v0, v2

    goto :goto_2

    :cond_a
    move-object v0, v2

    goto/16 :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/i;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 176
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/multisend/i;->a(Landroid/view/View;)V

    .line 178
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 179
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 181
    :cond_0
    return-void
.end method

.method public e()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 190
    invoke-super {p0}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/i;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Landroid/view/View;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/multisend/i;->a()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method
