.class public Lcom/sec/chaton/multimedia/image/ImageEffectFragment;
.super Landroid/support/v4/app/Fragment;
.source "ImageEffectFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Bitmap;

.field private f:Landroid/graphics/Bitmap;

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/image/h;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/chaton/multimedia/image/i;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/widget/Toast;

.field private j:Landroid/app/Activity;

.field private k:I

.field private l:I

.field private m:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    .line 438
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->l:I

    return v0
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->e:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Landroid/graphics/Bitmap;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a(Lcom/sec/chaton/multimedia/image/i;Landroid/graphics/Bitmap;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/sec/chaton/multimedia/image/i;Landroid/graphics/Bitmap;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 496
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 499
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 500
    sget-object v1, Lcom/sec/chaton/multimedia/image/f;->a:[I

    invoke-virtual {p1}, Lcom/sec/chaton/multimedia/image/i;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 552
    :goto_0
    :pswitch_0
    return-object v0

    .line 504
    :pswitch_1
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->x(Landroid/graphics/Bitmap;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 544
    :catch_0
    move-exception v0

    .line 545
    sget-object v1, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 507
    :pswitch_2
    :try_start_1
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->r(Landroid/graphics/Bitmap;)Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 547
    :catch_1
    move-exception v0

    .line 548
    sget-object v1, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 510
    :pswitch_3
    :try_start_2
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->B(Landroid/graphics/Bitmap;)Z

    goto :goto_0

    .line 513
    :pswitch_4
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->n(Landroid/graphics/Bitmap;)Z

    goto :goto_0

    .line 516
    :pswitch_5
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->q(Landroid/graphics/Bitmap;)Z

    goto :goto_0

    .line 519
    :pswitch_6
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->g(Landroid/graphics/Bitmap;)Z

    goto :goto_0

    .line 522
    :pswitch_7
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->c(Landroid/graphics/Bitmap;)Z

    goto :goto_0

    .line 525
    :pswitch_8
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->u(Landroid/graphics/Bitmap;)Z

    goto :goto_0

    .line 528
    :pswitch_9
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->A(Landroid/graphics/Bitmap;)Z

    goto :goto_0

    .line 531
    :pswitch_a
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->e(Landroid/graphics/Bitmap;)Z

    goto :goto_0

    .line 534
    :pswitch_b
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->a(Landroid/graphics/Bitmap;)Z

    goto :goto_0

    .line 537
    :pswitch_c
    invoke-static {v0}, Lcom/sec/vip/imagefilter/a;->F(Landroid/graphics/Bitmap;)Z
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 552
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 500
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 170
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    .line 178
    new-instance v0, Landroid/media/ExifInterface;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 179
    const-string v1, "Orientation"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    .line 181
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 182
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 185
    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 186
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 196
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    .line 202
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->c:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 218
    const v0, 0x7f070326

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 219
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 221
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->a:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b0234

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->b:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b0238

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->c:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b023c

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->d:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b0237

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->e:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b0235

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->f:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b010a

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->g:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b0236

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->h:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b0105

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->i:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b0239

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->j:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b023a

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->k:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b010c

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->l:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b0107

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/chaton/multimedia/image/h;

    sget-object v4, Lcom/sec/chaton/multimedia/image/i;->m:Lcom/sec/chaton/multimedia/image/i;

    const v5, 0x7f0b023b

    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/chaton/multimedia/image/h;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Lcom/sec/chaton/multimedia/image/i;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/sec/chaton/multimedia/image/h;

    .line 236
    const v2, 0x7f0300b1

    invoke-virtual {v1, v2, v0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 238
    const v2, 0x7f070327

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 239
    const v3, 0x7f070328

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 241
    iget-object v7, v4, Lcom/sec/chaton/multimedia/image/h;->d:Ljava/lang/String;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iput-object v2, v4, Lcom/sec/chaton/multimedia/image/h;->a:Landroid/widget/ImageView;

    .line 244
    iput-object v3, v4, Lcom/sec/chaton/multimedia/image/h;->b:Landroid/widget/TextView;

    .line 245
    iget-object v2, v4, Lcom/sec/chaton/multimedia/image/h;->a:Landroid/widget/ImageView;

    iget-object v3, v4, Lcom/sec/chaton/multimedia/image/h;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v2, v4, Lcom/sec/chaton/multimedia/image/h;->c:Lcom/sec/chaton/multimedia/image/i;

    invoke-static {v2}, Lcom/sec/chaton/multimedia/image/i;->a(Lcom/sec/chaton/multimedia/image/i;)I

    move-result v2

    invoke-virtual {v6, v2}, Landroid/view/View;->setId(I)V

    .line 248
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 250
    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    new-instance v2, Lcom/sec/chaton/multimedia/image/e;

    invoke-direct {v2, p0}, Lcom/sec/chaton/multimedia/image/e;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)V

    invoke-virtual {v6, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 273
    new-instance v2, Lcom/sec/chaton/multimedia/image/j;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->b:Ljava/lang/String;

    invoke-direct {v2, p0, v6, v3}, Lcom/sec/chaton/multimedia/image/j;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Landroid/view/View;Ljava/lang/String;)V

    .line 274
    new-array v3, v8, [Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/chaton/multimedia/image/j;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2

    .line 187
    :cond_3
    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 188
    const/high16 v0, 0x43340000    # 180.0f

    :try_start_1
    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 189
    :cond_4
    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 190
    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 193
    :cond_5
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto/16 :goto_1

    .line 204
    :catch_0
    move-exception v0

    .line 205
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->i:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 206
    sget-object v1, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0, v8}, Landroid/app/Activity;->setResult(I)V

    .line 208
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 210
    :catch_1
    move-exception v0

    .line 211
    sget-object v1, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    const v1, 0x7f0b016c

    invoke-static {v0, v1, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0, v8}, Landroid/app/Activity;->setResult(I)V

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 278
    :cond_6
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/h;

    .line 279
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/h;->c:Lcom/sec/chaton/multimedia/image/i;

    sget-object v2, Lcom/sec/chaton/multimedia/image/i;->a:Lcom/sec/chaton/multimedia/image/i;

    if-ne v1, v2, :cond_0

    .line 280
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/h;->b:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->l:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 281
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/h;->a:Landroid/widget/ImageView;

    const v1, 0x7f020298

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 282
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->e:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->k:I

    return v0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->f:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private b()V
    .locals 4

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 291
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 292
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->h:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 295
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Restart PreviewFilterTask : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/multimedia/image/i;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/image/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_1
    new-instance v1, Lcom/sec/chaton/multimedia/image/j;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->b:Ljava/lang/String;

    invoke-direct {v1, p0, v0, v3}, Lcom/sec/chaton/multimedia/image/j;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Landroid/view/View;Ljava/lang/String;)V

    .line 299
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/image/j;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 303
    :cond_2
    return-void
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->m:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 309
    const/4 v2, 0x0

    .line 310
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/tempEffect.jpg"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 311
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->e:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 313
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    :try_start_1
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->e:Landroid/graphics/Bitmap;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 334
    if-eqz v1, :cond_0

    .line 336
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    .line 342
    :cond_0
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 343
    const-string v2, "filterResultUri"

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 345
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 348
    :cond_1
    :goto_1
    return-void

    .line 315
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 316
    :goto_2
    :try_start_3
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    const v3, 0x7f0b016c

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 317
    sget-object v2, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 334
    if-eqz v1, :cond_1

    .line 336
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 337
    :catch_1
    move-exception v0

    .line 338
    sget-object v1, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    :goto_3
    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 321
    :catch_2
    move-exception v0

    move-object v1, v2

    .line 322
    :goto_4
    :try_start_5
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->i:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 323
    sget-object v2, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 325
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 334
    if-eqz v1, :cond_1

    .line 336
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 337
    :catch_3
    move-exception v0

    .line 338
    sget-object v1, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    goto :goto_3

    .line 327
    :catch_4
    move-exception v0

    move-object v1, v2

    .line 328
    :goto_5
    :try_start_7
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    const v3, 0x7f0b016c

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 329
    sget-object v2, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 334
    if-eqz v1, :cond_1

    .line 336
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_1

    .line 337
    :catch_5
    move-exception v0

    .line 338
    sget-object v1, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    goto :goto_3

    .line 334
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_6
    if-eqz v1, :cond_2

    .line 336
    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 334
    :cond_2
    :goto_7
    throw v0

    .line 337
    :catch_6
    move-exception v1

    .line 338
    sget-object v2, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_7

    .line 337
    :catch_7
    move-exception v1

    .line 338
    sget-object v2, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 334
    :catchall_1
    move-exception v0

    goto :goto_6

    .line 327
    :catch_8
    move-exception v0

    goto :goto_5

    .line 321
    :catch_9
    move-exception v0

    goto :goto_4

    .line 315
    :catch_a
    move-exception v0

    goto/16 :goto_2
.end method

.method private d()V
    .locals 4

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/h;

    .line 368
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/h;->a:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 369
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/h;->b:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->k:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 371
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d()V

    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->i:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->h:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 590
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 592
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    .line 593
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 352
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->b()V

    .line 360
    new-instance v0, Lcom/sec/chaton/multimedia/image/g;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    invoke-direct {v0, p0, p1, v1}, Lcom/sec/chaton/multimedia/image/g;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectFragment;Landroid/view/View;Landroid/graphics/Bitmap;)V

    .line 361
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/image/g;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 131
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 132
    if-eqz v0, :cond_0

    .line 133
    const-string v1, "filterOrgUri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->b:Ljava/lang/String;

    .line 136
    :cond_0
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "image uri : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->h:Ljava/util/HashMap;

    .line 141
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    const v1, 0x7f0b0149

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->i:Landroid/widget/Toast;

    .line 142
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->m:Landroid/app/ProgressDialog;

    .line 143
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->setHasOptionsMenu(Z)V

    .line 144
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 618
    const v0, 0x7f0f0021

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 620
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 621
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 148
    const v0, 0x7f0300b0

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 149
    const v0, 0x7f070325

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->c:Landroid/widget/ImageView;

    .line 151
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v2

    if-le v2, v4, :cond_0

    .line 155
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 156
    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    iput v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->k:I

    .line 158
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 159
    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    iput v2, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->l:I

    .line 161
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 164
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->a(Landroid/view/View;)V

    .line 165
    return-object v1

    .line 152
    nop

    :array_0
    .array-data 4
        0x7f010091
        0x7f010092
    .end array-data
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 557
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 559
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 560
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 563
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 564
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 567
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 568
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 571
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 572
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 575
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/h;

    .line 576
    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/h;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 577
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/h;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 578
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_4

    .line 579
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 583
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 584
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 597
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 599
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    .line 600
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 604
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-eq v1, v2, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0705a5

    if-ne v1, v2, :cond_1

    .line 605
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    if-eqz v1, :cond_2

    .line 606
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->j:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 613
    :goto_0
    return v0

    .line 609
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0705a6

    if-ne v1, v2, :cond_2

    .line 610
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->c()V

    goto :goto_0

    .line 613
    :cond_2
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
