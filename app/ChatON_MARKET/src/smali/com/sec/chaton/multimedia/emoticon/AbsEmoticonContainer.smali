.class public abstract Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;
.super Landroid/support/v4/app/Fragment;
.source "AbsEmoticonContainer.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field protected a:Landroid/app/Activity;

.field protected b:Landroid/widget/GridView;

.field protected c:Landroid/view/View;

.field protected d:Landroid/widget/ListAdapter;

.field private e:I

.field private f:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->c()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->b:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 124
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->b()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->b:Landroid/widget/GridView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 129
    return-void
.end method


# virtual methods
.method protected abstract a()Landroid/widget/ListAdapter;
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 41
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->a:Landroid/app/Activity;

    .line 42
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    const-string v1, "layoutResId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->e:I

    .line 63
    new-instance v0, Lcom/sec/chaton/multimedia/emoticon/a;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/emoticon/a;-><init>(Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->f:Landroid/database/DataSetObserver;

    .line 82
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 93
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 95
    iget v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->e:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 97
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->d:Landroid/widget/ListAdapter;

    .line 98
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->d:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->f:Landroid/database/DataSetObserver;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 101
    const v0, 0x7f07028a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->b:Landroid/widget/GridView;

    .line 102
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->b:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->d:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 103
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->b:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 105
    const v0, 0x7f07028c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->c:Landroid/view/View;

    .line 107
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->c()V

    .line 111
    :cond_0
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 118
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->d:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->f:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 119
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/AbsEmoticonContainer;->a:Landroid/app/Activity;

    .line 49
    return-void
.end method
