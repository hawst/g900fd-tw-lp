.class public Lcom/sec/chaton/multimedia/image/ImageEffectActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "ImageEffectActivity.java"


# instance fields
.field private a:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/ImageEffectActivity;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->e()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 60
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 61
    const-string v0, "showPasswordLockActivity"

    const-class v1, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 65
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->startActivity(Landroid/content/Intent;)V

    .line 70
    :cond_1
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lcom/sec/chaton/multimedia/image/d;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/d;-><init>(Lcom/sec/chaton/multimedia/image/ImageEffectActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->a:Landroid/content/BroadcastReceiver;

    .line 82
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 83
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 84
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 85
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 86
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 87
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->e()V

    .line 88
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 91
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    const v0, 0x7f0b003d

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 93
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->finish()V

    .line 96
    :cond_0
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 100
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->b(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 29
    new-instance v1, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;

    invoke-direct {v1}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;-><init>()V

    .line 30
    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/image/ImageEffectFragment;->setArguments(Landroid/os/Bundle;)V

    .line 31
    return-object v1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 48
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 51
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onPause()V

    .line 56
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->f()V

    .line 57
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 37
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->c()V

    .line 38
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;->d()V

    .line 40
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 44
    :cond_0
    return-void
.end method
