.class public Lcom/sec/chaton/multimedia/emoticon/anicon/a;
.super Landroid/support/v4/widget/CursorAdapter;
.source "AniconAdapter.java"


# instance fields
.field private a:I

.field private b:Lcom/sec/common/f/c;

.field private c:Landroid/view/LayoutInflater;

.field private d:Lcom/sec/chaton/settings/downloads/z;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;ILcom/sec/common/f/c;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 41
    invoke-direct {p0, p4}, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->a(Lcom/sec/common/f/c;)V

    .line 42
    return-void
.end method

.method private a(Lcom/sec/common/f/c;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 53
    iput-object p1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->b:Lcom/sec/common/f/c;

    .line 55
    const v1, 0x7f0901ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->a:I

    .line 56
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->c:Landroid/view/LayoutInflater;

    .line 57
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;

    .line 79
    const-string v1, "anicon_id"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 80
    iput-object v1, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->c:Ljava/lang/String;

    .line 82
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->d:Lcom/sec/chaton/settings/downloads/z;

    if-nez v2, :cond_0

    .line 83
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/chaton/settings/downloads/u;->i(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/chaton/settings/downloads/z;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->d:Lcom/sec/chaton/settings/downloads/z;

    .line 86
    :cond_0
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->d:Lcom/sec/chaton/settings/downloads/z;

    sget-object v3, Lcom/sec/chaton/settings/downloads/z;->a:Lcom/sec/chaton/settings/downloads/z;

    if-ne v2, v3, :cond_1

    .line 87
    iget-object v2, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->b:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    :goto_0
    const/high16 v2, -0x80000000

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 101
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->b:Lcom/sec/common/f/c;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Lcom/sec/common/f/c;->a(Landroid/view/View;)V

    move-object v0, p1

    .line 103
    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f020115

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 104
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 111
    :goto_1
    return-void

    .line 89
    :cond_1
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->d:Lcom/sec/chaton/settings/downloads/z;

    sget-object v3, Lcom/sec/chaton/settings/downloads/z;->c:Lcom/sec/chaton/settings/downloads/z;

    if-ne v2, v3, :cond_2

    .line 90
    iget-object v2, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->b:Landroid/widget/ImageView;

    const v3, 0x7f02014d

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 96
    :goto_2
    iget-object v2, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->b:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    iget-object v2, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 91
    :cond_2
    iget-object v2, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->d:Lcom/sec/chaton/settings/downloads/z;

    sget-object v3, Lcom/sec/chaton/settings/downloads/z;->e:Lcom/sec/chaton/settings/downloads/z;

    if-ne v2, v3, :cond_3

    .line 92
    iget-object v2, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->b:Landroid/widget/ImageView;

    const v3, 0x7f02014e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 94
    :cond_3
    iget-object v2, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->b:Landroid/widget/ImageView;

    const v3, 0x7f02014f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 106
    :cond_4
    new-instance v2, Lcom/sec/chaton/multimedia/emoticon/anicon/o;

    invoke-static {}, Lcom/sec/common/util/i;->e()I

    move-result v3

    invoke-static {}, Lcom/sec/common/util/i;->f()I

    move-result v4

    invoke-direct {v2, v1, v3, v4}, Lcom/sec/chaton/multimedia/emoticon/anicon/o;-><init>(Ljava/lang/String;II)V

    .line 107
    iget-object v1, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Lcom/sec/chaton/multimedia/emoticon/anicon/o;->a(Landroid/view/View;)V

    .line 109
    iget-object v1, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->b:Lcom/sec/common/f/c;

    iget-object v0, v0, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0, v2}, Lcom/sec/common/f/c;->a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030066

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 62
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    const/4 v0, -0x1

    iget v3, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->a:I

    invoke-direct {v2, v0, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 66
    new-instance v3, Lcom/sec/chaton/multimedia/emoticon/anicon/b;

    invoke-direct {v3}, Lcom/sec/chaton/multimedia/emoticon/anicon/b;-><init>()V

    .line 67
    const v0, 0x7f070293

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v3, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->a:Landroid/widget/ImageView;

    .line 68
    const v0, 0x7f070111

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v3, Lcom/sec/chaton/multimedia/emoticon/anicon/b;->b:Landroid/widget/ImageView;

    .line 70
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 71
    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 73
    return-object v1
.end method

.method public notifyDataSetInvalidated()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/emoticon/anicon/a;->d:Lcom/sec/chaton/settings/downloads/z;

    .line 116
    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetInvalidated()V

    .line 117
    return-void
.end method
