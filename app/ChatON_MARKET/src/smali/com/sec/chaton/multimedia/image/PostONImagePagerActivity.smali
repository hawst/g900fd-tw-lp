.class public Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;
.super Lcom/sec/chaton/base/BaseActivity;
.source "PostONImagePagerActivity.java"

# interfaces
.implements Lcom/sec/chaton/multimedia/image/aq;


# static fields
.field private static final b:Ljava/lang/String;

.field private static c:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Lcom/sec/chaton/e/a/u;

.field private C:Landroid/widget/LinearLayout;

.field private D:Z

.field private E:Ljava/lang/String;

.field private F:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private G:Landroid/view/View$OnClickListener;

.field private H:Landroid/database/ContentObserver;

.field private I:Lcom/sec/chaton/e/a/v;

.field private J:Landroid/os/Handler;

.field public a:I

.field private d:Landroid/support/v4/view/ViewPager;

.field private e:Lcom/sec/chaton/multimedia/image/ai;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Lcom/sec/common/f/c;

.field private m:Landroid/content/Context;

.field private n:Landroid/view/View;

.field private o:Landroid/content/BroadcastReceiver;

.field private p:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/multimedia/image/aj;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:Landroid/view/MenuItem;

.field private u:Landroid/view/MenuItem;

.field private v:Landroid/widget/TextView;

.field private w:Lcom/sec/common/a/d;

.field private x:Landroid/widget/ImageView;

.field private y:Landroid/widget/ImageView;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61
    const-class v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/poston/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 67
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a:I

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->p:Ljava/util/HashMap;

    .line 96
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->q:Ljava/util/HashMap;

    .line 97
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->s:Ljava/util/HashMap;

    .line 231
    new-instance v0, Lcom/sec/chaton/multimedia/image/ac;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/ac;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->F:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 250
    new-instance v0, Lcom/sec/chaton/multimedia/image/ad;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/ad;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->G:Landroid/view/View$OnClickListener;

    .line 364
    new-instance v0, Lcom/sec/chaton/multimedia/image/ae;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/chaton/multimedia/image/ae;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->H:Landroid/database/ContentObserver;

    .line 476
    new-instance v0, Lcom/sec/chaton/multimedia/image/af;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/af;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->I:Lcom/sec/chaton/e/a/v;

    .line 749
    new-instance v0, Lcom/sec/chaton/multimedia/image/ah;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/ah;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->J:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->k:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    return-object v0
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 419
    iput p1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    .line 423
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 425
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->q:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/aj;

    .line 426
    if-eqz v0, :cond_4

    .line 427
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/aj;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->k:Ljava/lang/String;

    .line 430
    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->A:Z

    if-eqz v1, :cond_5

    .line 431
    :cond_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->x:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 436
    :goto_0
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge p1, v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->A:Z

    if-eqz v1, :cond_6

    .line 437
    :cond_1
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->y:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 442
    :goto_1
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/aj;->d:Ljava/lang/String;

    .line 444
    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->t:Landroid/view/MenuItem;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->u:Landroid/view/MenuItem;

    if-eqz v2, :cond_3

    .line 445
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/sec/chaton/c/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "thumbnail"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 446
    :cond_2
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->t:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 447
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->u:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 453
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->z:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/sec/chaton/multimedia/image/aj;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 454
    iget-boolean v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->A:Z

    if-eqz v1, :cond_8

    .line 455
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->n:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 456
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 467
    :goto_3
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/image/aj;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 470
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/aj;->j:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b(Ljava/lang/String;)V

    .line 474
    :cond_4
    return-void

    .line 433
    :cond_5
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->x:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 439
    :cond_6
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->y:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 449
    :cond_7
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->u:Landroid/view/MenuItem;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2

    .line 458
    :cond_8
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->n:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 459
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 460
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 462
    :cond_9
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;I)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;I)I
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    return p1
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->q:Ljava/util/HashMap;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 357
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->v:Landroid/widget/TextView;

    const v1, 0x7f02043e

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 362
    :goto_0
    return-void

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->v:Landroid/widget/TextView;

    const v1, 0x7f02043d

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->m:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->s:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Lcom/sec/chaton/e/a/u;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->B:Lcom/sec/chaton/e/a/u;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 396
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 397
    const-string v0, "showPasswordLockActivity"

    sget-object v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 401
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 402
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 403
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 404
    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->startActivity(Landroid/content/Intent;)V

    .line 406
    :cond_1
    return-void
.end method

.method static synthetic i(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->h:Ljava/lang/String;

    return-object v0
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 696
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 719
    :cond_0
    :goto_0
    return-void

    .line 700
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->q:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/aj;

    .line 703
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/aj;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 704
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/aj;->d:Ljava/lang/String;

    .line 706
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 707
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 711
    :cond_2
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 712
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    .line 714
    new-instance v1, Lcom/sec/chaton/multimedia/a/a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ChatON"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2, v4}, Lcom/sec/chaton/multimedia/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 715
    new-array v0, v4, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/chaton/multimedia/a/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method static synthetic j(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 722
    new-instance v0, Lcom/sec/chaton/multimedia/image/ag;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/image/ag;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->o:Landroid/content/BroadcastReceiver;

    .line 729
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 730
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 731
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 732
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 733
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->o:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 734
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->l()V

    .line 735
    return-void
.end method

.method static synthetic k(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->l()V

    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 738
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 739
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 740
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->finish()V

    .line 743
    :cond_0
    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->o:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 747
    return-void
.end method

.method private n()V
    .locals 6

    .prologue
    const v5, 0x7f0b0166

    const/4 v3, 0x0

    .line 862
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 889
    :cond_0
    :goto_0
    return-void

    .line 866
    :cond_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 867
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->q:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/aj;

    .line 869
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/aj;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 873
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/aj;->d:Ljava/lang/String;

    .line 874
    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 875
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v1, v2

    .line 877
    iget-object v1, v0, Lcom/sec/chaton/multimedia/image/aj;->c:Ljava/lang/String;

    const-string v4, "1"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 878
    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/sec/chaton/util/ch;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    .line 884
    :cond_2
    :goto_1
    if-eqz v3, :cond_0

    .line 885
    invoke-virtual {p0, v3}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 880
    :cond_3
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/aj;->c:Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 881
    invoke-virtual {p0, v5}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->i:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1, v3, v3}, Lcom/sec/chaton/util/ch;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    goto :goto_1
.end method


# virtual methods
.method protected a()Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method public a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 950
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 951
    const-string v1, "video/*"

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 952
    const-string v1, "android.intent.extra.finishOnCompletion"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 954
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 962
    :cond_0
    :goto_0
    return-void

    .line 955
    :catch_0
    move-exception v0

    .line 956
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0414

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 957
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_0

    .line 958
    sget-object v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 820
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->p:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    return-void
.end method

.method protected a(Ljava/lang/String;Lcom/sec/chaton/multimedia/image/PostONImagePagerFragment;)V
    .locals 1

    .prologue
    .line 816
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->p:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    return-void
.end method

.method protected b()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/chaton/multimedia/image/aj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 670
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->q:Ljava/util/HashMap;

    return-object v0
.end method

.method protected c()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 674
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    return-object v0
.end method

.method protected d()Lcom/sec/common/f/c;
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->l:Lcom/sec/common/f/c;

    return-object v0
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 692
    iget v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    return v0
.end method

.method protected f()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 907
    iget-boolean v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->A:Z

    if-nez v0, :cond_0

    .line 908
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->u:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 909
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->t:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 911
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 912
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 914
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 916
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 917
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 921
    iput-boolean v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->A:Z

    .line 947
    :goto_0
    return-void

    .line 923
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->u:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 924
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->t:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 926
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 927
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 928
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 934
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 936
    iget v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    if-lez v0, :cond_1

    .line 937
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 940
    :cond_1
    iget v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 941
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 945
    :cond_2
    iput-boolean v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->A:Z

    goto :goto_0

    .line 930
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 314
    invoke-super {p0, p1, p2, p3}, Lcom/sec/chaton/base/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 316
    packed-switch p1, :pswitch_data_0

    .line 353
    :cond_0
    :goto_0
    return-void

    .line 318
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 319
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "IS_DELETED"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 320
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_1

    .line 321
    const-string v0, "Inform the item is deleted"

    sget-object v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->finish()V

    goto :goto_0

    .line 327
    :cond_2
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->E:Ljava/lang/String;

    .line 328
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->D:Z

    .line 330
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->E:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->v:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 337
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->q:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 338
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->q:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/aj;

    .line 339
    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->E:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/chaton/multimedia/image/aj;->i:Ljava/lang/String;

    .line 340
    const-string v1, "0"

    iput-object v1, v0, Lcom/sec/chaton/multimedia/image/aj;->j:Ljava/lang/String;

    .line 342
    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/aj;->j:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 316
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 224
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 225
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    sget-object v1, Lcom/sec/chaton/poston/PostONDetailFragment;->h:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->D:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 227
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->setResult(ILandroid/content/Intent;)V

    .line 228
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->finish()V

    .line 229
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v4, 0x3

    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 123
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 125
    const v0, 0x7f0300b2

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->setContentView(I)V

    .line 126
    new-instance v0, Lcom/sec/common/f/c;

    invoke-direct {v0}, Lcom/sec/common/f/c;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->l:Lcom/sec/common/f/c;

    .line 127
    iput-object p0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->m:Landroid/content/Context;

    .line 128
    invoke-static {}, Lcom/sec/chaton/util/aa;->a()Lcom/sec/chaton/util/ab;

    move-result-object v0

    const-string v1, "chaton_id"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Lcom/sec/chaton/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g:Ljava/lang/String;

    .line 129
    new-instance v0, Lcom/sec/chaton/e/a/u;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->I:Lcom/sec/chaton/e/a/v;

    invoke-direct {v0, v1, v3}, Lcom/sec/chaton/e/a/u;-><init>(Landroid/content/ContentResolver;Lcom/sec/chaton/e/a/v;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->B:Lcom/sec/chaton/e/a/u;

    .line 131
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 136
    const v0, 0x7f070330

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->n:Landroid/view/View;

    .line 137
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->n:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->G:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    const v0, 0x7f07032d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->C:Landroid/widget/LinearLayout;

    .line 141
    new-instance v0, Lcom/sec/chaton/multimedia/image/ai;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/sec/chaton/multimedia/image/ai;-><init>(Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e:Lcom/sec/chaton/multimedia/image/ai;

    .line 143
    const v0, 0x7f07032a

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d:Landroid/support/v4/view/ViewPager;

    .line 144
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e:Lcom/sec/chaton/multimedia/image/ai;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 145
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->d:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->F:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 147
    const v0, 0x7f07032b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->x:Landroid/widget/ImageView;

    .line 148
    const v0, 0x7f07032c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->y:Landroid/widget/ImageView;

    .line 150
    const v0, 0x7f07032e

    invoke-virtual {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->z:Landroid/widget/TextView;

    .line 153
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->n:Landroid/view/View;

    const v3, 0x7f07014c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->v:Landroid/widget/TextView;

    .line 156
    if-eqz p1, :cond_2

    .line 158
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "[restore from onSaveInstanceState]"

    sget-object v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :cond_0
    const-string v0, "buddyId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f:Ljava/lang/String;

    .line 163
    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->h:Ljava/lang/String;

    .line 172
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->B:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a:I

    sget-object v3, Lcom/sec/chaton/e/ad;->b:Landroid/net/Uri;

    const-string v5, "poston_metatype=? OR poston_metatype=? OR poston_metatype=?"

    new-array v6, v4, [Ljava/lang/String;

    const-string v4, "1"

    aput-object v4, v6, v8

    const-string v4, "2"

    aput-object v4, v6, v9

    const-string v4, "3"

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :goto_1
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 193
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ad;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->H:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v9, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 198
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->i:Ljava/lang/String;

    .line 199
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/common/actionbar/a;->b(Z)V

    .line 200
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/common/actionbar/a;->a(Z)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->k()Lcom/sec/common/actionbar/a;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/common/actionbar/a;->c(Z)V

    .line 202
    return-void

    .line 165
    :cond_2
    if-eqz v1, :cond_1

    .line 166
    const-string v0, "buddyId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f:Ljava/lang/String;

    .line 167
    const-string v0, "url"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->h:Ljava/lang/String;

    goto :goto_0

    .line 182
    :cond_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->B:Lcom/sec/chaton/e/a/u;

    iget v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a:I

    sget-object v3, Lcom/sec/chaton/e/aa;->b:Landroid/net/Uri;

    const-string v5, "poston_metatype=? OR poston_metatype=? OR poston_metatype=?"

    new-array v6, v4, [Ljava/lang/String;

    const-string v4, "1"

    aput-object v4, v6, v8

    const-string v4, "2"

    aput-object v4, v6, v9

    const-string v4, "3"

    aput-object v4, v6, v7

    move-object v4, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/chaton/e/a/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 195
    :cond_4
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/aa;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->H:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v9, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->l:Lcom/sec/common/f/c;

    invoke-virtual {v0}, Lcom/sec/common/f/c;->a()V

    .line 412
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->w:Lcom/sec/common/a/d;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->w:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 415
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onDestroy()V

    .line 416
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 390
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->m()V

    .line 391
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->H:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 392
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onPause()V

    .line 393
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 378
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j()V

    .line 379
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->h()V

    .line 380
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/ad;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->H:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 385
    :goto_0
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onResume()V

    .line 386
    return-void

    .line 383
    :cond_0
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/e/aa;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->H:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->q:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/aj;

    .line 211
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_0

    .line 212
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onSaveInstanceState] postonNo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/chaton/multimedia/image/aj;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_0
    const-string v0, "buddyId"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v0, "url"

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 219
    return-void
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 834
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0031

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 836
    const v0, 0x7f07058c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->t:Landroid/view/MenuItem;

    .line 837
    const v0, 0x7f0705b2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->u:Landroid/view/MenuItem;

    .line 839
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->e()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->a(I)V

    .line 840
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 846
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f07058c

    if-ne v0, v1, :cond_1

    .line 847
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->i()V

    .line 858
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 848
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0705b2

    if-ne v0, v1, :cond_0

    .line 850
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    iget v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 851
    iget-object v0, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->q:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->r:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/image/aj;

    .line 853
    if-eqz v0, :cond_0

    .line 854
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/image/PostONImagePagerActivity;->n()V

    goto :goto_0
.end method
