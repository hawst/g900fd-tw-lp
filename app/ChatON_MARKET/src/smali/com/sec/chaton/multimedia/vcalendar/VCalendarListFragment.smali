.class public Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;
.super Landroid/support/v4/app/Fragment;
.source "VCalendarListFragment.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;
.implements Lcom/sec/chaton/by;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Landroid/view/View;

.field private e:Landroid/content/Context;

.field private f:Lcom/sec/chaton/widget/ClearableEditText;

.field private g:Landroid/widget/ExpandableListView;

.field private h:Landroid/view/ViewStub;

.field private i:Landroid/view/View;

.field private j:Lcom/sec/chaton/multimedia/vcalendar/e;

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/vcalendar/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/vcalendar/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private o:Landroid/app/Dialog;

.field private p:Landroid/app/ProgressDialog;

.field private q:Landroid/content/BroadcastReceiver;

.field private r:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 246
    new-instance v0, Lcom/sec/chaton/multimedia/vcalendar/g;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/vcalendar/g;-><init>(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->r:Landroid/text/TextWatcher;

    .line 270
    new-instance v0, Lcom/sec/chaton/multimedia/vcalendar/h;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/vcalendar/h;-><init>(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->b:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->k:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a()V
    .locals 7

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->d:Landroid/view/View;

    const v1, 0x7f070372

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 193
    const v0, 0x7f07014c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/widget/ClearableEditText;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f:Lcom/sec/chaton/widget/ClearableEditText;

    .line 194
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f:Lcom/sec/chaton/widget/ClearableEditText;

    const v2, 0x7f0b0096

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setHint(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f:Lcom/sec/chaton/widget/ClearableEditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    new-instance v4, Lcom/sec/chaton/util/w;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const/16 v6, 0x1e

    invoke-direct {v4, v5, v6}, Lcom/sec/chaton/util/w;-><init>(Landroid/content/Context;I)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/ClearableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 206
    const v0, 0x7f0702d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->d:Landroid/view/View;

    const v1, 0x7f070373

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    .line 214
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 215
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 218
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->d:Landroid/view/View;

    const v1, 0x7f070374

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->h:Landroid/view/ViewStub;

    .line 222
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 12

    .prologue
    .line 352
    const-string v0, "createEventsDataSet()"

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy/MM/dd"

    invoke-direct {v6, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 354
    const-string v0, ""

    .line 356
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 358
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 359
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CURSOR POSITION : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 364
    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 366
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMdd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 369
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 370
    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 372
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMdd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 375
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "*** 1 MONTH BEFORE : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "*** 1 MONTH AFTER  : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CURSOR POSITION : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 381
    const-string v0, "*** SKIP : DELETED ***"

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 385
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 386
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 387
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 391
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 393
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "*** EVENT DATE  : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", title :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    if-lt v0, v8, :cond_1

    if-le v0, v9, :cond_2

    .line 396
    :cond_1
    const-string v0, "*** SKIP ***"

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 401
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 402
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 406
    const/4 v1, 0x4

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-wide/16 v10, 0x1

    cmp-long v1, v3, v10

    if-gez v1, :cond_3

    .line 407
    const/4 v1, 0x1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 411
    :cond_3
    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-wide/16 v10, 0x1

    cmp-long v1, v3, v10

    if-gez v1, :cond_5

    move-object v0, v2

    .line 417
    :goto_1
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 419
    iget-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->k:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 420
    iget-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->k:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    :cond_4
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 431
    const v1, 0x7f0b019b

    invoke-virtual {p0, v1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 437
    :goto_2
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 438
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 439
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    const-string v4, ""

    .line 440
    :goto_3
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    const-string v5, ""

    .line 442
    :goto_4
    new-instance v0, Lcom/sec/chaton/multimedia/vcalendar/f;

    invoke-direct/range {v0 .. v5}, Lcom/sec/chaton/multimedia/vcalendar/f;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 414
    :cond_5
    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_1

    .line 434
    :cond_6
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 439
    :cond_7
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 440
    :cond_8
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 445
    :cond_9
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->k:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 447
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 449
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 450
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_a
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/multimedia/vcalendar/f;

    .line 451
    new-instance v5, Ljava/util/Date;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/vcalendar/f;->b()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v5, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 452
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 453
    new-instance v5, Lcom/sec/chaton/multimedia/vcalendar/f;

    invoke-direct {v5, v1}, Lcom/sec/chaton/multimedia/vcalendar/f;-><init>(Lcom/sec/chaton/multimedia/vcalendar/f;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 457
    :cond_b
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 497
    :cond_c
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;Z)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 14

    .prologue
    const/4 v13, -0x1

    const/4 v12, 0x0

    .line 525
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 526
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 537
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 539
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 541
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcalendar/f;

    .line 542
    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/chaton/util/as;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 543
    new-instance v6, Lcom/sec/chaton/multimedia/vcalendar/f;

    invoke-direct {v6, v0}, Lcom/sec/chaton/multimedia/vcalendar/f;-><init>(Lcom/sec/chaton/multimedia/vcalendar/f;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 549
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcalendar/f;

    .line 550
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy/MM/dd"

    invoke-direct {v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/util/Date;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 552
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 553
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 557
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 559
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 560
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/multimedia/vcalendar/f;

    .line 561
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v9, "yyyy/MM/dd"

    invoke-direct {v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/util/Date;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/vcalendar/f;->b()Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v8, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    .line 562
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 563
    new-instance v8, Lcom/sec/chaton/multimedia/vcalendar/f;

    invoke-direct {v8, v1}, Lcom/sec/chaton/multimedia/vcalendar/f;-><init>(Lcom/sec/chaton/multimedia/vcalendar/f;)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 567
    :cond_6
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 570
    :cond_7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 571
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v1, v13, v12, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 572
    invoke-direct {p0, v12}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Z)V

    .line 574
    invoke-direct {p0, v2, v3}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 580
    :goto_4
    return-void

    .line 577
    :cond_8
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    invoke-direct {v1, v13, v12, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 578
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a(Z)V

    goto :goto_4
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/multimedia/vcalendar/f;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 583
    const-string v0, "supplyDataToAdapter()"

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 585
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 592
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v4

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 593
    iget-object v6, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 594
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "yyyy/MM/dd"

    invoke-direct {v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v3, v1

    .line 598
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 601
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 602
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 604
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcalendar/f;

    .line 605
    new-instance v7, Lcom/sec/chaton/multimedia/vcalendar/f;

    invoke-direct {v7, v0}, Lcom/sec/chaton/multimedia/vcalendar/f;-><init>(Lcom/sec/chaton/multimedia/vcalendar/f;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 608
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 611
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mAdapter.getGroupCount() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/e;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/vcalendar/e;->getGroupCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/e;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/multimedia/vcalendar/e;->a(Ljava/lang/String;)V

    .line 615
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/e;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/e;->notifyDataSetChanged()V

    .line 617
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mGroupData.size() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mAdapterGroupData.size() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mAdapter.getGroupCount() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/e;

    invoke-virtual {v1}, Lcom/sec/chaton/multimedia/vcalendar/e;->getGroupCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    :goto_3
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/e;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/e;->getGroupCount()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 622
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 621
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 625
    :cond_4
    if-eq v3, v4, :cond_5

    .line 626
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "todayPosition() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v3}, Landroid/widget/ExpandableListView;->setSelectedGroup(I)V

    .line 635
    :goto_4
    return-void

    .line 631
    :cond_5
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setSelectedGroup(I)V

    goto :goto_4
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 500
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->i:Landroid/view/View;

    if-nez v0, :cond_0

    .line 522
    :goto_0
    return-void

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->i:Landroid/view/View;

    if-nez v0, :cond_1

    .line 505
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->h:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->i:Landroid/view/View;

    .line 507
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->i:Landroid/view/View;

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 508
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 510
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->i:Landroid/view/View;

    const v1, 0x7f07014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 511
    const v1, 0x7f0b03fd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 513
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->i:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 514
    const v1, 0x7f0b03fe

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 517
    :cond_1
    if-eqz p1, :cond_2

    .line 518
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->i:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v3, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 520
    :cond_2
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->i:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Landroid/widget/ExpandableListView;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->l:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->o:Landroid/app/Dialog;

    return-object v0
.end method

.method private d()V
    .locals 7

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->r:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->b(Landroid/text/TextWatcher;)V

    .line 229
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->r:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->a(Landroid/text/TextWatcher;)V

    .line 230
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/e;

    if-nez v0, :cond_0

    .line 231
    new-instance v0, Lcom/sec/chaton/multimedia/vcalendar/e;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->m:Ljava/util/ArrayList;

    const v4, 0x7f030151

    iget-object v5, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->n:Ljava/util/ArrayList;

    const v6, 0x7f030123

    invoke-direct/range {v0 .. v6}, Lcom/sec/chaton/multimedia/vcalendar/e;-><init>(Landroid/widget/ExpandableListView;Landroid/content/Context;Ljava/util/ArrayList;ILjava/util/ArrayList;I)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/e;

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->j:Lcom/sec/chaton/multimedia/vcalendar/e;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 239
    :cond_1
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    .line 240
    const v1, 0x7f0b0036

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->o:Landroid/app/Dialog;

    .line 241
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->o:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 243
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->b:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 244
    return-void
.end method

.method static synthetic e(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)Lcom/sec/chaton/widget/ClearableEditText;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f:Lcom/sec/chaton/widget/ClearableEditText;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 717
    new-instance v0, Lcom/sec/chaton/multimedia/vcalendar/i;

    invoke-direct {v0, p0}, Lcom/sec/chaton/multimedia/vcalendar/i;-><init>(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->q:Landroid/content/BroadcastReceiver;

    .line 724
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 725
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 726
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 727
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 728
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 729
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f()V

    .line 730
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 733
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->e:Landroid/content/Context;

    const v1, 0x7f0b003d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 735
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 738
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f()V

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 741
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 742
    return-void
.end method


# virtual methods
.method public b()V
    .locals 1

    .prologue
    .line 707
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 708
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 712
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 713
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 674
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 696
    :cond_0
    :goto_0
    return-void

    .line 678
    :cond_1
    if-ne p2, v1, :cond_2

    .line 679
    if-ne p1, v3, :cond_0

    .line 680
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1, p3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 681
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 684
    :cond_2
    if-nez p2, :cond_0

    .line 685
    if-ne p1, v3, :cond_0

    .line 687
    if-eqz p3, :cond_3

    const-string v0, "backKey"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 689
    :cond_3
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v2, p3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 690
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 6

    .prologue
    .line 641
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/multimedia/vcalendar/f;

    .line 643
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "******** "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "******** "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy/MM/dd hh:mm:ss a"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/util/Date;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->b()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "******** "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy/MM/dd hh:mm:ss a"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/util/Date;

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->c()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "******** "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "******** "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->p:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 651
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->p:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 655
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->e:Landroid/content/Context;

    const-class v3, Lcom/sec/chaton/multimedia/vcalendar/VCalendarDetailActivity2;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 656
    const-string v2, "extra summary"

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 657
    const-string v2, "extra dtstart"

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->b()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 658
    const-string v2, "extra dtend"

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->c()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 659
    const-string v2, "extra location"

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 660
    const-string v2, "extra desc"

    invoke-virtual {v0}, Lcom/sec/chaton/multimedia/vcalendar/f;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 662
    const-string v0, "ACTIVITY_PURPOSE"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 665
    const-string v0, "inbox_NO"

    iget-object v2, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 667
    const/4 v0, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 669
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 92
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 93
    const-string v0, "onCreate()"

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0, v2}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->setHasOptionsMenu(Z)V

    .line 107
    invoke-static {p0, v2}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 109
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/high16 v3, 0x40900000    # 4.5f

    const/4 v4, 0x0

    .line 113
    const-string v0, "onCreateView()"

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const v0, 0x7f0300c5

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 116
    const v0, 0x7f070372

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 118
    const v2, 0x7f0702e5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 120
    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v3}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v4, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 121
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 123
    iput-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->d:Landroid/view/View;

    .line 125
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->e:Landroid/content/Context;

    .line 127
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "inbox_NO"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->c:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->k:Ljava/util/ArrayList;

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->l:Ljava/util/ArrayList;

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->m:Ljava/util/ArrayList;

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->n:Ljava/util/ArrayList;

    .line 135
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/j;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/j;

    move-result-object v0

    const v2, 0x7f0b0036

    invoke-virtual {v0, v2}, Lcom/sec/chaton/widget/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    iput-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->p:Landroid/app/ProgressDialog;

    .line 138
    return-object v1
.end method

.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 1

    .prologue
    .line 701
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 748
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 749
    invoke-virtual {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 750
    const/4 v0, 0x1

    .line 752
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->g()V

    .line 172
    :try_start_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f:Lcom/sec/chaton/widget/ClearableEditText;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f:Lcom/sec/chaton/widget/ClearableEditText;

    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->r:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/ClearableEditText;->b(Landroid/text/TextWatcher;)V

    .line 174
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->e:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 175
    iget-object v1, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->f:Lcom/sec/chaton/widget/ClearableEditText;

    invoke-virtual {v1}, Lcom/sec/chaton/widget/ClearableEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->p:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->p:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->p:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 187
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 188
    return-void

    .line 177
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 150
    const-string v0, "onResume()"

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 154
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->e()V

    .line 157
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a()V

    .line 159
    invoke-direct {p0}, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->d()V

    .line 162
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 145
    const-string v0, "onStart()"

    sget-object v1, Lcom/sec/chaton/multimedia/vcalendar/VCalendarListFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    return-void
.end method
