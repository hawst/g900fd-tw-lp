.class public Lcom/sec/chaton/i/a/d;
.super Ljava/lang/Thread;
.source "LogMessageTestThread.java"


# static fields
.field private static a:Lcom/sec/chaton/i/a/d;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/chaton/i/a/d;->a:Lcom/sec/chaton/i/a/d;

    .line 15
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 26
    return-void
.end method

.method public static a(Z)V
    .locals 0

    .prologue
    .line 18
    sput-boolean p0, Lcom/sec/chaton/i/a/d;->b:Z

    .line 19
    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 22
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    return v0
.end method

.method public static b()Lcom/sec/chaton/i/a/d;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/chaton/i/a/d;->a:Lcom/sec/chaton/i/a/d;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/sec/chaton/i/a/d;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/d;-><init>()V

    sput-object v0, Lcom/sec/chaton/i/a/d;->a:Lcom/sec/chaton/i/a/d;

    .line 32
    :cond_0
    sget-object v0, Lcom/sec/chaton/i/a/d;->a:Lcom/sec/chaton/i/a/d;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 40
    :goto_0
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 43
    :try_start_0
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 44
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 45
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-nez v0, :cond_1

    .line 406
    :cond_0
    return-void

    .line 49
    :cond_1
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030002"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 50
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 51
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 55
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030003"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 56
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 57
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 61
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030004"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 62
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 63
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 67
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030005"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 68
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 69
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 73
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030006"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 74
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 75
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 79
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030007"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 80
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 81
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 85
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030008"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 86
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 87
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 91
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030009"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 92
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 93
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 97
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030015"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 98
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 99
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 103
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030009"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 104
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 105
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 109
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030011"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 110
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 111
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 115
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030020"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 116
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 117
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 121
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00050001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 122
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 123
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 127
    new-instance v0, Lcom/sec/chaton/i/a/c;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 128
    const-string v1, "00010002"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 129
    const-string v1, "1235786123468"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 130
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 131
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 132
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 136
    new-instance v0, Lcom/sec/chaton/i/a/c;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 137
    const-string v1, "00050011"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 138
    const-string v1, "35135356123468"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 139
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 140
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 141
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 145
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00060001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 146
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 147
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 151
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00070001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 152
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 153
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 157
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00080001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 158
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 159
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 163
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00090001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 164
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 165
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 169
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100003"

    const-string v2, "00000002"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 171
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 175
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100003"

    const-string v2, "00000001"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 177
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 181
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100003"

    const-string v2, "00000003"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 183
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 187
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100003"

    const-string v2, "00000004"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 189
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 193
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100003"

    const-string v2, "00000005"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 195
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 199
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100003"

    const-string v2, "00000006"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 201
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 205
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100013"

    const-string v2, "00000001"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 207
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 211
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100013"

    const-string v2, "00000002"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 213
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 217
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100013"

    const-string v2, "00000003"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 219
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 223
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100013"

    const-string v2, "00000004"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 225
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 229
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100011"

    const-string v2, "00000001"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 231
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 235
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100011"

    const-string v2, "00000002"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 237
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 241
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100011"

    const-string v2, "00000001"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 243
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 247
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100011"

    const-string v2, "00000002"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 249
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 253
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100011"

    const-string v2, "00000003"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 255
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 259
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100011"

    const-string v2, "00000004"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 261
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 265
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100011"

    const-string v2, "00000005"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 267
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 271
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00100011"

    const-string v2, "00000006"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 273
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 277
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030006"

    const-string v2, "00000001"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 279
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 283
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00030006"

    const-string v2, "00000002"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 285
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 289
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00090001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 290
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 291
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 295
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00060001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 296
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 297
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 301
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00070001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 302
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 303
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 307
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00080001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 308
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 309
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 313
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00090001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 314
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 315
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 319
    new-instance v0, Lcom/sec/chaton/i/a/c;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 320
    const-string v1, "00010002"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 321
    const-string v1, "1235786123468"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 322
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 323
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 324
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 328
    new-instance v0, Lcom/sec/chaton/i/a/c;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/c;-><init>()V

    .line 329
    const-string v1, "00050011"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/c;->d(Ljava/lang/String;)V

    .line 330
    const-string v1, "35135356123468"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/c;->a(Ljava/lang/String;)V

    .line 331
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 332
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 333
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 337
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v0

    const-string v1, "00090001"

    invoke-virtual {v0, v1}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;)V

    .line 338
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 339
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 343
    new-instance v0, Lcom/sec/chaton/i/a/b;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/b;-><init>()V

    .line 344
    const-string v1, "01000011"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->c(Ljava/lang/String;)V

    .line 345
    const-string v1, "00010204"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->d(Ljava/lang/String;)V

    .line 346
    const-string v1, "CFS-11001"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;)V

    .line 347
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 348
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 349
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 353
    new-instance v0, Lcom/sec/chaton/i/a/b;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/b;-><init>()V

    .line 354
    const-string v1, "01000002"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->c(Ljava/lang/String;)V

    .line 355
    const-string v1, "00010204"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->d(Ljava/lang/String;)V

    .line 356
    const-string v1, "CFS-11001"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;)V

    .line 357
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 358
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 359
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 363
    new-instance v0, Lcom/sec/chaton/i/a/b;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/b;-><init>()V

    .line 364
    const-string v1, "01000004"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->c(Ljava/lang/String;)V

    .line 365
    const-string v1, "01010401"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->d(Ljava/lang/String;)V

    .line 366
    const-string v1, "CFS-11001"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;)V

    .line 367
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 368
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 369
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 373
    new-instance v0, Lcom/sec/chaton/i/a/b;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/b;-><init>()V

    .line 374
    const-string v1, "01000016"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->c(Ljava/lang/String;)V

    .line 375
    const-string v1, "00010404"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->d(Ljava/lang/String;)V

    .line 376
    const-string v1, "CFS-11001"

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/b;->a(Ljava/lang/String;)V

    .line 377
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 378
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 379
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 383
    new-instance v0, Lcom/sec/chaton/i/a/a;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/a;-><init>()V

    .line 384
    const-string v1, "\"java.lang.ArithmeticException: divide by zero\n\tat com.sec.chaton.msgbox.MsgboxFragment.onTabSelected(MsgboxFragment.java:1483)\n\tat com.sec.chaton.HomeTabFragment.onTabChanged(HomeTabFragment.java:219)\n\tat android.widget.TabHost.invokeOnTabChangeListener(TabHost.java:402)\n\tat android.widget.TabHost.setCurrentTab(TabHost.java:387)\n\tat android.widget.TabHost$2.onTabSelectionChanged(TabHost.java:150)\n\tat android.widget.TabWidget$TabClickListener.onClick(TabWidget.java:560)\n\tat android.view.View.performClick(View.java:4211)\n\tat android.view.View$PerformClick.run(View.java:17267)\n\tat android.os.Handler.handleCallback(Handler.java:615)\n\tat android.os.Handler.dispatchMessage(Handler.java:92)\n\tat android.os.Looper.loop(Looper.java:137)\n\tat android.app.ActivityThread.main(ActivityThread.java:4898)\n\tat java.lang.reflect.Method.invokeNative(Native Method)\n\tat java.lang.reflect.Method.invoke(Method.java:511)\n\tat com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:1006)\n\tat com.android.internal.os.ZygoteInit.main(ZygoteInit.java:773)\n\tat dalvik.system.NativeStart.main(Native Method)\n\""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/a;->a(Ljava/lang/String;)V

    .line 386
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 387
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 388
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 392
    new-instance v0, Lcom/sec/chaton/i/a/a;

    invoke-direct {v0}, Lcom/sec/chaton/i/a/a;-><init>()V

    .line 393
    const-string v1, "\"java.lang.NullPointerException\n\tat com.sec.chaton.msgbox.MsgboxFragment.onTabSelected(MsgboxFragment.java:1483)\n\tat com.sec.chaton.HomeTabFragment.onTabChanged(HomeTabFragment.java:219)\n\tat android.widget.TabHost.invokeOnTabChangeListener(TabHost.java:402)\n\tat android.widget.TabHost.setCurrentTab(TabHost.java:387)\n\tat android.widget.TabHost$2.onTabSelectionChanged(TabHost.java:150)\n\tat android.widget.TabWidget$TabClickListener.onClick(TabWidget.java:560)\n\tat android.view.View.performClick(View.java:4211)\n\tat android.view.View$PerformClick.run(View.java:17267)\n\tat android.os.Handler.handleCallback(Handler.java:615)\n\tat android.os.Handler.dispatchMessage(Handler.java:92)\n\tat android.os.Looper.loop(Looper.java:137)\n\tat android.app.ActivityThread.main(ActivityThread.java:4898)\n\tat java.lang.reflect.Method.invokeNative(Native Method)\n\tat java.lang.reflect.Method.invoke(Method.java:511)\n\tat com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:1006)\n\tat com.android.internal.os.ZygoteInit.main(ZygoteInit.java:773)\n\tat dalvik.system.NativeStart.main(Native Method)\n\""

    invoke-virtual {v0, v1}, Lcom/sec/chaton/i/a/a;->a(Ljava/lang/String;)V

    .line 395
    invoke-static {}, Lcom/sec/common/util/log/collector/h;->a()Lcom/sec/common/util/log/collector/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 396
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 397
    sget-boolean v0, Lcom/sec/chaton/i/a/d;->b:Z

    if-eqz v0, :cond_0

    .line 401
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 402
    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method
