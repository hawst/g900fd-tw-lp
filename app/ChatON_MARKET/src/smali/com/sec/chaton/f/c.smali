.class public Lcom/sec/chaton/f/c;
.super Landroid/widget/BaseAdapter;
.source "DialogMenuAdapter.java"


# instance fields
.field public a:Lcom/sec/chaton/chat/ChatFragment;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/f/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/chaton/chat/ChatFragment;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/chat/ChatFragment;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/f/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/chaton/f/c;->c:Ljava/util/ArrayList;

    .line 27
    iput-object p1, p0, Lcom/sec/chaton/f/c;->a:Lcom/sec/chaton/chat/ChatFragment;

    .line 28
    iget-object v0, p0, Lcom/sec/chaton/f/c;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v0}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/chaton/f/c;->b:Landroid/view/LayoutInflater;

    .line 29
    iput-object p2, p0, Lcom/sec/chaton/f/c;->c:Ljava/util/ArrayList;

    .line 30
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/chaton/f/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/chaton/f/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 74
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 34
    invoke-virtual {p0, p1}, Lcom/sec/chaton/f/c;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/chaton/f/g;

    .line 36
    if-nez p2, :cond_0

    .line 37
    iget-object v1, p0, Lcom/sec/chaton/f/c;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f030046

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 39
    new-instance v2, Lcom/sec/chaton/f/e;

    invoke-direct {v2, p0, v4}, Lcom/sec/chaton/f/e;-><init>(Lcom/sec/chaton/f/c;Lcom/sec/chaton/f/d;)V

    .line 40
    const v1, 0x7f0701cc

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/sec/chaton/f/e;->a:Landroid/widget/ImageView;

    .line 41
    const v1, 0x7f0701cd

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/sec/chaton/f/e;->b:Landroid/widget/TextView;

    .line 43
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 46
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/chaton/f/e;

    .line 49
    :try_start_0
    iget-object v2, v1, Lcom/sec/chaton/f/e;->a:Landroid/widget/ImageView;

    iget v3, v0, Lcom/sec/chaton/f/g;->b:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :goto_0
    iget-object v1, v1, Lcom/sec/chaton/f/e;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/chaton/f/c;->a:Lcom/sec/chaton/chat/ChatFragment;

    invoke-virtual {v2}, Lcom/sec/chaton/chat/ChatFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v0, v0, Lcom/sec/chaton/f/g;->a:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    return-object p2

    .line 50
    :catch_0
    move-exception v2

    .line 51
    sget-boolean v3, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v3, :cond_1

    .line 52
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 54
    :cond_1
    iget-object v2, v1, Lcom/sec/chaton/f/e;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
