.class public Lcom/sec/chaton/f/a;
.super Landroid/app/Dialog;
.source "DialogMenu.java"


# static fields
.field private static final f:Z


# instance fields
.field private a:Lcom/sec/chaton/f/c;

.field private b:Lcom/sec/chaton/chat/ChatFragment;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/f/g;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/widget/GridView;

.field private e:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/chaton/f/a;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sec/chaton/chat/ChatFragment;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/chaton/chat/ChatFragment;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/chaton/f/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p1}, Lcom/sec/chaton/chat/ChatFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0c0101

    invoke-direct {p0, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/chaton/f/a;->requestWindowFeature(I)Z

    .line 41
    invoke-virtual {p0}, Lcom/sec/chaton/f/a;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 44
    iput-object p1, p0, Lcom/sec/chaton/f/a;->b:Lcom/sec/chaton/chat/ChatFragment;

    .line 45
    iput-object p2, p0, Lcom/sec/chaton/f/a;->c:Ljava/util/ArrayList;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/sec/chaton/f/a;)Lcom/sec/chaton/chat/ChatFragment;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/f/a;->b:Lcom/sec/chaton/chat/ChatFragment;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 62
    const v0, 0x7f03002b

    invoke-virtual {p0, v0}, Lcom/sec/chaton/f/a;->setContentView(I)V

    .line 65
    const v0, 0x7f07012c

    invoke-virtual {p0, v0}, Lcom/sec/chaton/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/chaton/f/a;->e:Landroid/widget/LinearLayout;

    .line 66
    const v0, 0x7f07012d

    invoke-virtual {p0, v0}, Lcom/sec/chaton/f/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/chaton/f/a;->d:Landroid/widget/GridView;

    .line 68
    iget-object v0, p0, Lcom/sec/chaton/f/a;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Lcom/sec/chaton/f/c;

    iget-object v1, p0, Lcom/sec/chaton/f/a;->b:Lcom/sec/chaton/chat/ChatFragment;

    iget-object v2, p0, Lcom/sec/chaton/f/a;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/sec/chaton/f/c;-><init>(Lcom/sec/chaton/chat/ChatFragment;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/chaton/f/a;->a:Lcom/sec/chaton/f/c;

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sec/chaton/f/a;->d:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/chaton/f/a;->a:Lcom/sec/chaton/f/c;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 73
    iget-object v0, p0, Lcom/sec/chaton/f/a;->d:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/chaton/f/b;

    invoke-direct {v1, p0}, Lcom/sec/chaton/f/b;-><init>(Lcom/sec/chaton/f/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 84
    sget-boolean v0, Lcom/sec/chaton/f/a;->f:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v1, 0xa0

    if-ne v0, v1, :cond_1

    .line 86
    iget-object v0, p0, Lcom/sec/chaton/f/a;->d:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/chaton/f/a;->e:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sec/chaton/f/a;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/sec/chaton/f/a;->d:Landroid/widget/GridView;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 91
    const v1, 0x440e8000    # 570.0f

    invoke-static {v1}, Lcom/sec/chaton/util/an;->a(F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 93
    iget-object v1, p0, Lcom/sec/chaton/f/a;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 96
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/sec/chaton/f/a;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/chaton/f/a;->c:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-direct {p0}, Lcom/sec/chaton/f/a;->a()V

    .line 53
    return-void
.end method
