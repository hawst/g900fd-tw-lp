.class public final Lcom/sec/common/e;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final ActionBarCompat:[I

.field public static final ActionBarCompat_backgroundStacked:I = 0x6

.field public static final ActionBarCompat_displayOptions:I = 0x3

.field public static final ActionBarCompat_height:I = 0x1

.field public static final ActionBarCompat_logo:I = 0x2

.field public static final ActionBarCompat_subtitleTextStyle:I = 0x5

.field public static final ActionBarCompat_titleTextStyle:I = 0x4

.field public static final ActionBarCompat_windowNoTitle:I = 0x0

.field public static final AlertDialogCompat:[I

.field public static final AlertDialogCompat_bottomBright:I = 0x7

.field public static final AlertDialogCompat_bottomDark:I = 0x3

.field public static final AlertDialogCompat_bottomMedium:I = 0x8

.field public static final AlertDialogCompat_centerBright:I = 0x6

.field public static final AlertDialogCompat_centerDark:I = 0x2

.field public static final AlertDialogCompat_centerMedium:I = 0x9

.field public static final AlertDialogCompat_fullBright:I = 0x4

.field public static final AlertDialogCompat_fullDark:I = 0x0

.field public static final AlertDialogCompat_topBright:I = 0x5

.field public static final AlertDialogCompat_topDark:I = 0x1

.field public static final CommonTheme:[I

.field public static final CommonTheme_actionBarDivider:I = 0x6

.field public static final CommonTheme_actionBarSize:I = 0x4

.field public static final CommonTheme_actionBarStyle:I = 0x0

.field public static final CommonTheme_actionBarTabBarStyle:I = 0x2

.field public static final CommonTheme_actionBarTabStyle:I = 0x1

.field public static final CommonTheme_actionBarTabTextStyle:I = 0x3

.field public static final CommonTheme_actionButtonStyle:I = 0x5

.field public static final CommonTheme_actionDropDownStyle:I = 0xb

.field public static final CommonTheme_actionMenuTextAppearance:I = 0x9

.field public static final CommonTheme_actionMenuTextColor:I = 0xa

.field public static final CommonTheme_alertDialogStyle:I = 0x10

.field public static final CommonTheme_alertDialogTheme:I = 0xf

.field public static final CommonTheme_buttonBarButtonStyle:I = 0x14

.field public static final CommonTheme_buttonBarStyle:I = 0x13

.field public static final CommonTheme_dropDownListViewStyle:I = 0xe

.field public static final CommonTheme_homeAsUpIndicator:I = 0x7

.field public static final CommonTheme_listDividerAlertDialog:I = 0x11

.field public static final CommonTheme_listPopupWindowStyle:I = 0xd

.field public static final CommonTheme_selectableItemBackground:I = 0x8

.field public static final CommonTheme_spinnerDropDownItemStyle:I = 0xc

.field public static final CommonTheme_textColorAlertDialogListItem:I = 0x12

.field public static final PagerPositionStrip:[I

.field public static final PagerPositionStrip_firstIndicator:I = 0x3

.field public static final PagerPositionStrip_indicator:I = 0x0

.field public static final PagerPositionStrip_indicatorMargin:I = 0x5

.field public static final PagerPositionStrip_lastIndicator:I = 0x4

.field public static final PagerPositionStrip_leftIndicator:I = 0x1

.field public static final PagerPositionStrip_maxIndicatorCount:I = 0x6

.field public static final PagerPositionStrip_rightIndicator:I = 0x2

.field public static final SpinnerCompat:[I

.field public static final SpinnerCompat_android_dropDownHorizontalOffset:I = 0x5

.field public static final SpinnerCompat_android_dropDownSelector:I = 0x1

.field public static final SpinnerCompat_android_dropDownVerticalOffset:I = 0x6

.field public static final SpinnerCompat_android_dropDownWidth:I = 0x4

.field public static final SpinnerCompat_android_gravity:I = 0x0

.field public static final SpinnerCompat_android_popupBackground:I = 0x2

.field public static final SpinnerCompat_android_popupPromptView:I = 0x7

.field public static final SpinnerCompat_android_prompt:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 127
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/common/e;->ActionBarCompat:[I

    .line 135
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/common/e;->AlertDialogCompat:[I

    .line 146
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/common/e;->CommonTheme:[I

    .line 168
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/common/e;->PagerPositionStrip:[I

    .line 176
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sec/common/e;->SpinnerCompat:[I

    return-void

    .line 127
    nop

    :array_0
    .array-data 4
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
    .end array-data

    .line 135
    :array_1
    .array-data 4
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
    .end array-data

    .line 146
    :array_2
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
    .end array-data

    .line 168
    :array_3
    .array-data 4
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
    .end array-data

    .line 176
    :array_4
    .array-data 4
        0x10100af
        0x1010175
        0x1010176
        0x101017b
        0x1010262
        0x10102ac
        0x10102ad
        0x1010411
    .end array-data
.end method
