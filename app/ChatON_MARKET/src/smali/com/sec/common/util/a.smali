.class public abstract Lcom/sec/common/util/a;
.super Ljava/lang/Object;
.source "AsyncWorker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/sec/common/util/f;


# instance fields
.field private c:Lcom/sec/common/util/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/common/util/h",
            "<TParams;TResult;>;"
        }
    .end annotation
.end field

.field protected d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TProgress;"
        }
    .end annotation
.end field

.field private e:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask",
            "<TResult;>;"
        }
    .end annotation
.end field

.field private f:Ljava/util/concurrent/ExecutorService;

.field private g:Lcom/sec/common/util/g;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    const-class v0, Lcom/sec/common/util/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/common/util/a;->a:Ljava/lang/String;

    .line 32
    new-instance v0, Lcom/sec/common/util/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/common/util/f;-><init>(Lcom/sec/common/util/b;)V

    sput-object v0, Lcom/sec/common/util/a;->b:Lcom/sec/common/util/f;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lcom/sec/common/util/b;

    invoke-direct {v0, p0}, Lcom/sec/common/util/b;-><init>(Lcom/sec/common/util/a;)V

    iput-object v0, p0, Lcom/sec/common/util/a;->c:Lcom/sec/common/util/h;

    .line 57
    new-instance v0, Lcom/sec/common/util/c;

    iget-object v1, p0, Lcom/sec/common/util/a;->c:Lcom/sec/common/util/h;

    invoke-direct {v0, p0, v1}, Lcom/sec/common/util/c;-><init>(Lcom/sec/common/util/a;Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Lcom/sec/common/util/a;->e:Ljava/util/concurrent/FutureTask;

    .line 86
    iput-object p1, p0, Lcom/sec/common/util/a;->f:Ljava/util/concurrent/ExecutorService;

    .line 88
    sget-object v0, Lcom/sec/common/util/g;->a:Lcom/sec/common/util/g;

    iput-object v0, p0, Lcom/sec/common/util/a;->g:Lcom/sec/common/util/g;

    .line 89
    return-void
.end method

.method static synthetic a(Lcom/sec/common/util/a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/common/util/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method private c(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/sec/common/util/a;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const/4 p1, 0x0

    .line 154
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/common/util/a;->a(Ljava/lang/Object;)V

    .line 155
    sget-object v0, Lcom/sec/common/util/g;->c:Lcom/sec/common/util/g;

    iput-object v0, p0, Lcom/sec/common/util/a;->g:Lcom/sec/common/util/g;

    .line 156
    return-void
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/common/util/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k()Lcom/sec/common/util/f;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/common/util/a;->b:Lcom/sec/common/util/f;

    return-object v0
.end method


# virtual methods
.method protected varargs abstract a([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 141
    return-void
.end method

.method public a(Z)Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/common/util/a;->e:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method protected b(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TProgress;)V"
        }
    .end annotation

    .prologue
    .line 147
    return-void
.end method

.method public final varargs d([Ljava/lang/Object;)Lcom/sec/common/util/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)",
            "Lcom/sec/common/util/a",
            "<TParams;TProgress;TResult;>;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/common/util/a;->g:Lcom/sec/common/util/g;

    sget-object v1, Lcom/sec/common/util/g;->a:Lcom/sec/common/util/g;

    if-eq v0, v1, :cond_0

    .line 117
    sget-object v0, Lcom/sec/common/util/d;->a:[I

    iget-object v1, p0, Lcom/sec/common/util/a;->g:Lcom/sec/common/util/g;

    invoke-virtual {v1}, Lcom/sec/common/util/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 125
    :cond_0
    sget-object v0, Lcom/sec/common/util/g;->b:Lcom/sec/common/util/g;

    iput-object v0, p0, Lcom/sec/common/util/a;->g:Lcom/sec/common/util/g;

    .line 127
    invoke-virtual {p0}, Lcom/sec/common/util/a;->e()V

    .line 129
    iget-object v0, p0, Lcom/sec/common/util/a;->c:Lcom/sec/common/util/h;

    iput-object p1, v0, Lcom/sec/common/util/h;->b:[Ljava/lang/Object;

    .line 130
    iget-object v0, p0, Lcom/sec/common/util/a;->f:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/sec/common/util/a;->e:Ljava/util/concurrent/FutureTask;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 132
    return-object p0

    .line 119
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task is already running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task has already been executed (a task can be executed only once)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method protected final varargs e([Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TProgress;)V"
        }
    .end annotation

    .prologue
    .line 159
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/sec/common/util/a;->d:Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/sec/common/util/a;->b:Lcom/sec/common/util/f;

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/common/util/e;

    invoke-direct {v2, p0, p1}, Lcom/sec/common/util/e;-><init>(Lcom/sec/common/util/a;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/util/f;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 162
    return-void
.end method

.method protected f()V
    .locals 0

    .prologue
    .line 144
    return-void
.end method

.method public final g()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TProgress;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/common/util/a;->d:Ljava/lang/Object;

    return-object v0
.end method

.method public final h()Lcom/sec/common/util/g;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/common/util/a;->g:Lcom/sec/common/util/g;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/common/util/a;->e:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isCancelled()Z

    move-result v0

    return v0
.end method
