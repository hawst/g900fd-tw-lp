.class Lcom/sec/common/util/c;
.super Ljava/util/concurrent/FutureTask;
.source "AsyncWorker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<TResult;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/common/util/a;


# direct methods
.method constructor <init>(Lcom/sec/common/util/a;Ljava/util/concurrent/Callable;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/common/util/c;->a:Lcom/sec/common/util/a;

    invoke-direct {p0, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    return-void
.end method


# virtual methods
.method protected done()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 62
    .line 65
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/common/util/c;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 81
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/common/util/a;->k()Lcom/sec/common/util/f;

    move-result-object v1

    new-instance v2, Lcom/sec/common/util/e;

    iget-object v3, p0, Lcom/sec/common/util/c;->a:Lcom/sec/common/util/a;

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-direct {v2, v3, v4}, Lcom/sec/common/util/e;-><init>(Lcom/sec/common/util/a;[Ljava/lang/Object;)V

    invoke-virtual {v1, v6, v2}, Lcom/sec/common/util/f;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 83
    :goto_1
    return-void

    .line 66
    :catch_0
    move-exception v1

    .line 67
    sget-object v2, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v2, v2, Lcom/sec/common/c/a;->d:Z

    if-eqz v2, :cond_0

    .line 68
    sget-object v2, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    invoke-static {}, Lcom/sec/common/util/a;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/sec/common/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :catch_1
    move-exception v0

    .line 71
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "An error occured while executing doInBackground()"

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 72
    :catch_2
    move-exception v1

    .line 73
    invoke-static {}, Lcom/sec/common/util/a;->k()Lcom/sec/common/util/f;

    move-result-object v1

    const/4 v2, 0x3

    new-instance v3, Lcom/sec/common/util/e;

    iget-object v4, p0, Lcom/sec/common/util/c;->a:Lcom/sec/common/util/a;

    check-cast v0, [Ljava/lang/Object;

    invoke-direct {v3, v4, v0}, Lcom/sec/common/util/e;-><init>(Lcom/sec/common/util/a;[Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/util/f;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    .line 77
    :catch_3
    move-exception v0

    .line 78
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "An error occured while executing doInBackground()"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
