.class Lcom/sec/common/util/log/collector/d;
.super Ljava/lang/Object;
.source "LogCollectorSender.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/sec/common/util/log/collector/LogCollectorSender;


# direct methods
.method constructor <init>(Lcom/sec/common/util/log/collector/LogCollectorSender;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/common/util/log/collector/d;->a:Lcom/sec/common/util/log/collector/LogCollectorSender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/common/util/log/collector/d;->a:Lcom/sec/common/util/log/collector/LogCollectorSender;

    invoke-static {p2}, Lcom/sec/spp/push/dlc/api/IDlcService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/spp/push/dlc/api/IDlcService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/common/util/log/collector/LogCollectorSender;->a(Lcom/sec/common/util/log/collector/LogCollectorSender;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 236
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->c:Z

    if-eqz v0, :cond_0

    .line 237
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    const-string v1, "LogCollectorService is connected"

    invoke-static {}, Lcom/sec/common/util/log/collector/LogCollectorSender;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/sec/common/util/log/collector/d;->a:Lcom/sec/common/util/log/collector/LogCollectorSender;

    invoke-static {v0}, Lcom/sec/common/util/log/collector/LogCollectorSender;->a(Lcom/sec/common/util/log/collector/LogCollectorSender;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 241
    iget-object v0, p0, Lcom/sec/common/util/log/collector/d;->a:Lcom/sec/common/util/log/collector/LogCollectorSender;

    iget-object v1, p0, Lcom/sec/common/util/log/collector/d;->a:Lcom/sec/common/util/log/collector/LogCollectorSender;

    invoke-static {v1}, Lcom/sec/common/util/log/collector/LogCollectorSender;->b(Lcom/sec/common/util/log/collector/LogCollectorSender;)Lcom/sec/spp/push/dlc/api/IDlcService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/common/util/log/collector/LogCollectorSender;->b(Lcom/sec/common/util/log/collector/LogCollectorSender;Lcom/sec/spp/push/dlc/api/IDlcService;)V

    .line 245
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/sec/common/util/log/collector/d;->a:Lcom/sec/common/util/log/collector/LogCollectorSender;

    iget-object v1, p0, Lcom/sec/common/util/log/collector/d;->a:Lcom/sec/common/util/log/collector/LogCollectorSender;

    invoke-static {v1}, Lcom/sec/common/util/log/collector/LogCollectorSender;->b(Lcom/sec/common/util/log/collector/LogCollectorSender;)Lcom/sec/spp/push/dlc/api/IDlcService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/common/util/log/collector/LogCollectorSender;->c(Lcom/sec/common/util/log/collector/LogCollectorSender;Lcom/sec/spp/push/dlc/api/IDlcService;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/common/util/log/collector/d;->a:Lcom/sec/common/util/log/collector/LogCollectorSender;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/common/util/log/collector/LogCollectorSender;->a(Lcom/sec/common/util/log/collector/LogCollectorSender;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 250
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->c:Z

    if-eqz v0, :cond_0

    .line 251
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    const-string v1, "LogCollectorService is disconnected"

    invoke-static {}, Lcom/sec/common/util/log/collector/LogCollectorSender;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :cond_0
    return-void
.end method
