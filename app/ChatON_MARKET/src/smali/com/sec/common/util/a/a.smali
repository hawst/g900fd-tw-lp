.class public Lcom/sec/common/util/a/a;
.super Ljava/lang/Object;
.source "FileDownloadManager.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/common/util/a/a;


# instance fields
.field private final c:Ljava/lang/Object;

.field private d:I

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/common/util/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/ThreadFactory;

.field private final g:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/common/util/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/common/util/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/common/util/a/a;->c:Ljava/lang/Object;

    .line 59
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/common/util/a/a;->d:I

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/common/util/a/a;->e:Ljava/util/Map;

    .line 63
    new-instance v0, Lcom/sec/common/util/a/b;

    invoke-direct {v0, p0}, Lcom/sec/common/util/a/b;-><init>(Lcom/sec/common/util/a/a;)V

    iput-object v0, p0, Lcom/sec/common/util/a/a;->f:Ljava/util/concurrent/ThreadFactory;

    .line 72
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sec/common/util/a/a;->f:Ljava/util/concurrent/ThreadFactory;

    invoke-static {v0, v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/util/a/a;->g:Ljava/util/concurrent/ExecutorService;

    .line 73
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/common/util/a/a;
    .locals 2

    .prologue
    .line 81
    const-class v1, Lcom/sec/common/util/a/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/common/util/a/a;->b:Lcom/sec/common/util/a/a;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lcom/sec/common/util/a/a;

    invoke-direct {v0}, Lcom/sec/common/util/a/a;-><init>()V

    sput-object v0, Lcom/sec/common/util/a/a;->b:Lcom/sec/common/util/a/a;

    .line 85
    :cond_0
    sget-object v0, Lcom/sec/common/util/a/a;->b:Lcom/sec/common/util/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;)Ljava/io/File;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/sec/common/util/n;->c:Lcom/sec/common/util/n;

    invoke-static {p0, v0}, Lcom/sec/common/util/a/a;->a(Landroid/content/Context;Lcom/sec/common/util/n;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/sec/common/util/n;)Ljava/io/File;
    .locals 5

    .prologue
    .line 99
    invoke-static {p0, p1}, Lcom/sec/common/util/l;->b(Landroid/content/Context;Lcom/sec/common/util/n;)Ljava/io/File;

    move-result-object v0

    .line 101
    new-instance v1, Ljava/io/File;

    const-string v2, "download"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 103
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Ljava/io/IOException;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "Couldn\'t create temp download directory. option : "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const-string v4, " / "

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v1, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    return-object v1
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    .locals 6

    .prologue
    .line 199
    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 200
    :catch_0
    move-exception v0

    .line 202
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->b:Z

    if-eqz v1, :cond_0

    .line 203
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/util/a/a;->a:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    const-string v5, "\'s caller thread is interrupted."

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/sec/common/util/a/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 207
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v2, p1}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;)Z

    .line 208
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    throw v0

    .line 208
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 211
    :catch_1
    move-exception v0

    .line 212
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    .line 143
    iget-object v2, p0, Lcom/sec/common/util/a/a;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 144
    :try_start_0
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 145
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/common/util/a/a;->a:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "File download task count: "

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/common/util/a/a;->e:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/sec/common/util/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/util/a/c;

    .line 150
    if-nez v0, :cond_3

    .line 151
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_1

    .line 152
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/common/util/a/a;->a:Ljava/lang/String;

    const-string v3, "Create new FileDownloadTask."

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_1
    new-instance v1, Lcom/sec/common/util/a/c;

    invoke-direct {v1, p0}, Lcom/sec/common/util/a/c;-><init>(Lcom/sec/common/util/a/a;)V

    .line 158
    new-instance v0, Lcom/sec/common/util/a/d;

    const/4 v3, 0x1

    invoke-direct {v0, p2, p3, v3}, Lcom/sec/common/util/a/d;-><init>(Ljava/lang/String;Ljava/io/File;Z)V

    iput-object v0, v1, Lcom/sec/common/util/a/c;->a:Lcom/sec/common/util/a/d;

    .line 159
    if-eqz p1, :cond_2

    .line 160
    iget-object v0, v1, Lcom/sec/common/util/a/c;->a:Lcom/sec/common/util/a/d;

    invoke-virtual {v0, p1, p4}, Lcom/sec/common/util/a/d;->a(Landroid/os/Handler;Ljava/lang/Object;)V

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/sec/common/util/a/a;->g:Ljava/util/concurrent/ExecutorService;

    iget-object v3, v1, Lcom/sec/common/util/a/c;->a:Lcom/sec/common/util/a/d;

    invoke-interface {v0, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/common/util/a/c;->b:Ljava/util/concurrent/Future;

    .line 165
    const/4 v0, 0x1

    iput v0, v1, Lcom/sec/common/util/a/c;->c:I

    .line 167
    iget-object v0, v1, Lcom/sec/common/util/a/c;->b:Ljava/util/concurrent/Future;

    .line 169
    iget-object v3, p0, Lcom/sec/common/util/a/a;->e:Ljava/util/Map;

    invoke-interface {v3, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    :goto_0
    monitor-exit v2

    .line 184
    return-object v0

    .line 171
    :cond_3
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->b:Z

    if-eqz v1, :cond_4

    .line 172
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v3, Lcom/sec/common/util/a/a;->a:Ljava/lang/String;

    const-string v4, "Using created FileDownloadTask."

    invoke-virtual {v1, v3, v4}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_4
    iget-object v1, v0, Lcom/sec/common/util/a/c;->b:Ljava/util/concurrent/Future;

    .line 176
    iget v3, v0, Lcom/sec/common/util/a/c;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/sec/common/util/a/c;->c:I

    .line 178
    if-eqz p1, :cond_5

    .line 179
    iget-object v0, v0, Lcom/sec/common/util/a/c;->a:Lcom/sec/common/util/a/d;

    invoke-virtual {v0, p1, p4}, Lcom/sec/common/util/a/d;->a(Landroid/os/Handler;Ljava/lang/Object;)V

    :cond_5
    move-object v0, v1

    goto :goto_0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 319
    iget-object v1, p0, Lcom/sec/common/util/a/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 320
    :try_start_0
    iget-object v0, p0, Lcom/sec/common/util/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    monitor-exit v1

    .line 322
    return-void

    .line 321
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/os/Handler;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 281
    iget-object v3, p0, Lcom/sec/common/util/a/a;->c:Ljava/lang/Object;

    monitor-enter v3

    .line 282
    :try_start_0
    iget-object v0, p0, Lcom/sec/common/util/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/util/a/c;

    .line 284
    if-nez v0, :cond_1

    .line 285
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->d:Z

    if-eqz v0, :cond_0

    .line 286
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/util/a/a;->a:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "Unknown url. "

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Lcom/sec/common/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    :cond_0
    monitor-exit v3

    move v0, v1

    .line 310
    :goto_0
    return v0

    .line 292
    :cond_1
    if-eqz p1, :cond_2

    .line 293
    iget-object v1, v0, Lcom/sec/common/util/a/c;->a:Lcom/sec/common/util/a/d;

    invoke-virtual {v1, p1}, Lcom/sec/common/util/a/d;->a(Landroid/os/Handler;)I

    .line 296
    :cond_2
    iget v1, v0, Lcom/sec/common/util/a/c;->c:I

    if-ne v1, v2, :cond_4

    .line 297
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->d:Z

    if-eqz v1, :cond_3

    .line 298
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v4, Lcom/sec/common/util/a/a;->a:Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    const/4 v6, 0x1

    const-string v7, "\'s reference count is 0. Stop download."

    aput-object v7, v5, v6

    invoke-static {v5}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/sec/common/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_3
    iget-object v1, v0, Lcom/sec/common/util/a/c;->a:Lcom/sec/common/util/a/d;

    invoke-virtual {v1}, Lcom/sec/common/util/a/d;->b()V

    .line 302
    iget-object v0, v0, Lcom/sec/common/util/a/c;->b:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 304
    iget-object v0, p0, Lcom/sec/common/util/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    :goto_1
    monitor-exit v3

    move v0, v2

    .line 310
    goto :goto_0

    .line 306
    :cond_4
    iget v1, v0, Lcom/sec/common/util/a/c;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/sec/common/util/a/c;->c:I

    goto :goto_1

    .line 308
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/sec/common/util/a/a;->d:I

    return v0
.end method

.method public b(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;
    .locals 6

    .prologue
    .line 257
    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/io/File;Ljava/lang/Object;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 258
    :catch_0
    move-exception v0

    .line 260
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->b:Z

    if-eqz v1, :cond_0

    .line 261
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/util/a/a;->a:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    const-string v5, "\'s caller thread is interrupted."

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_0
    iget-object v1, p0, Lcom/sec/common/util/a/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 265
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v2, p2}, Lcom/sec/common/util/a/a;->a(Landroid/os/Handler;Ljava/lang/String;)Z

    .line 266
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268
    throw v0

    .line 266
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 269
    :catch_1
    move-exception v0

    .line 270
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
