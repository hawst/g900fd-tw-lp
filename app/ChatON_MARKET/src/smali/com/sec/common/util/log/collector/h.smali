.class public Lcom/sec/common/util/log/collector/h;
.super Ljava/lang/Object;
.source "LogWriter.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/common/util/log/collector/h;


# instance fields
.field private c:Ljava/util/concurrent/ExecutorService;

.field private d:Lorg/a/a/a/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/f",
            "<",
            "Lcom/sec/common/util/log/collector/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/common/util/log/collector/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/common/util/log/collector/h;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x1

    new-instance v1, Lcom/sec/common/util/log/collector/i;

    invoke-direct {v1, p0}, Lcom/sec/common/util/log/collector/i;-><init>(Lcom/sec/common/util/log/collector/h;)V

    invoke-static {v0, v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/util/log/collector/h;->c:Ljava/util/concurrent/ExecutorService;

    .line 61
    new-instance v0, Lorg/a/a/a/a/p;

    new-instance v1, Lcom/sec/common/util/log/collector/k;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/common/util/log/collector/k;-><init>(Lcom/sec/common/util/log/collector/h;Lcom/sec/common/util/log/collector/i;)V

    invoke-direct {v0, v1}, Lorg/a/a/a/a/p;-><init>(Lorg/a/a/a/h;)V

    iput-object v0, p0, Lcom/sec/common/util/log/collector/h;->d:Lorg/a/a/a/f;

    .line 62
    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/common/util/log/collector/h;
    .locals 2

    .prologue
    .line 39
    const-class v1, Lcom/sec/common/util/log/collector/h;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/common/util/log/collector/h;->b:Lcom/sec/common/util/log/collector/h;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/sec/common/util/log/collector/h;

    invoke-direct {v0}, Lcom/sec/common/util/log/collector/h;-><init>()V

    sput-object v0, Lcom/sec/common/util/log/collector/h;->b:Lcom/sec/common/util/log/collector/h;

    .line 43
    :cond_0
    sget-object v0, Lcom/sec/common/util/log/collector/h;->b:Lcom/sec/common/util/log/collector/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/sec/common/util/log/collector/h;)Lorg/a/a/a/f;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/common/util/log/collector/h;->d:Lorg/a/a/a/f;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/common/util/log/collector/h;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/common/util/log/collector/b;)V
    .locals 5

    .prologue
    .line 115
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 116
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/common/util/log/collector/h;->a:Ljava/lang/String;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "Requested writing log message. pageId="

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p1, Lcom/sec/common/util/log/collector/b;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, ", actionId="

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p1, Lcom/sec/common/util/log/collector/b;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/common/util/log/collector/h;->d:Lorg/a/a/a/f;

    invoke-interface {v0}, Lorg/a/a/a/f;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/util/log/collector/j;

    .line 124
    iput-object p1, v0, Lcom/sec/common/util/log/collector/j;->a:Lcom/sec/common/util/log/collector/b;

    .line 126
    iget-object v1, p0, Lcom/sec/common/util/log/collector/h;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :cond_1
    :goto_0
    return-void

    .line 127
    :catch_0
    move-exception v0

    .line 128
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_1

    .line 129
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/util/log/collector/h;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/common/util/log/collector/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 80
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 81
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/common/util/log/collector/h;->a:Ljava/lang/String;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "Requested writing log message. pageId = "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const-string v4, " actionId = "

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/sec/common/util/log/collector/e;->a()Lcom/sec/common/util/log/collector/b;

    move-result-object v1

    .line 88
    if-nez v1, :cond_2

    .line 107
    :cond_1
    :goto_0
    return-void

    .line 92
    :cond_2
    invoke-virtual {v1, p1}, Lcom/sec/common/util/log/collector/b;->c(Ljava/lang/String;)V

    .line 93
    invoke-virtual {v1, p2}, Lcom/sec/common/util/log/collector/b;->d(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/sec/common/util/log/collector/h;->d:Lorg/a/a/a/f;

    invoke-interface {v0}, Lorg/a/a/a/f;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/util/log/collector/j;

    .line 99
    iput-object v1, v0, Lcom/sec/common/util/log/collector/j;->a:Lcom/sec/common/util/log/collector/b;

    .line 101
    iget-object v1, p0, Lcom/sec/common/util/log/collector/h;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 102
    :catch_0
    move-exception v0

    .line 103
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_1

    .line 104
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/util/log/collector/h;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
