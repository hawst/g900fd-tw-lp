.class Lcom/sec/common/util/log/collector/j;
.super Ljava/lang/Thread;
.source "LogWriter.java"


# instance fields
.field public a:Lcom/sec/common/util/log/collector/b;

.field final synthetic b:Lcom/sec/common/util/log/collector/h;


# direct methods
.method private constructor <init>(Lcom/sec/common/util/log/collector/h;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/common/util/log/collector/j;->b:Lcom/sec/common/util/log/collector/h;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/common/util/log/collector/h;Lcom/sec/common/util/log/collector/i;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/sec/common/util/log/collector/j;-><init>(Lcom/sec/common/util/log/collector/h;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 165
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 167
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_0

    .line 168
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    invoke-static {}, Lcom/sec/common/util/log/collector/h;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Save log to database. ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/common/util/log/collector/j;->a:Lcom/sec/common/util/log/collector/b;

    iget-object v5, v5, Lcom/sec/common/util/log/collector/b;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, ","

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/common/util/log/collector/j;->a:Lcom/sec/common/util/log/collector/b;

    iget-object v5, v5, Lcom/sec/common/util/log/collector/b;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/sec/common/util/log/collector/j;->a:Lcom/sec/common/util/log/collector/b;

    invoke-static {v0}, Lcom/sec/common/util/log/collector/a;->a(Lcom/sec/common/util/log/collector/b;)V

    .line 174
    :try_start_0
    iget-object v0, p0, Lcom/sec/common/util/log/collector/j;->b:Lcom/sec/common/util/log/collector/h;

    invoke-static {v0}, Lcom/sec/common/util/log/collector/h;->a(Lcom/sec/common/util/log/collector/h;)Lorg/a/a/a/f;

    move-result-object v0

    invoke-interface {v0, p0}, Lorg/a/a/a/f;->a(Ljava/lang/Object;)V

    .line 176
    iget-object v0, p0, Lcom/sec/common/util/log/collector/j;->a:Lcom/sec/common/util/log/collector/b;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/sec/common/util/log/collector/b;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/sec/common/util/log/collector/j;->a:Lcom/sec/common/util/log/collector/b;

    invoke-static {v0}, Lcom/sec/common/util/log/collector/e;->a(Lcom/sec/common/util/log/collector/b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :cond_1
    :goto_0
    return-void

    .line 179
    :catch_0
    move-exception v0

    .line 180
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_1

    .line 181
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    invoke-static {}, Lcom/sec/common/util/log/collector/h;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
