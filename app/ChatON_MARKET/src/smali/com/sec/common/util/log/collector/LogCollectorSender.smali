.class public Lcom/sec/common/util/log/collector/LogCollectorSender;
.super Landroid/app/IntentService;
.source "LogCollectorSender.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/spp/push/dlc/api/IDlcService;

.field private c:Z

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/common/util/log/collector/LogCollectorSender;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 41
    sget-object v0, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->b:Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 35
    iput-boolean v1, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->c:Z

    .line 36
    iput-boolean v1, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->d:Z

    .line 231
    new-instance v0, Lcom/sec/common/util/log/collector/d;

    invoke-direct {v0, p0}, Lcom/sec/common/util/log/collector/d;-><init>(Lcom/sec/common/util/log/collector/LogCollectorSender;)V

    iput-object v0, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->f:Landroid/content/ServiceConnection;

    .line 43
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Lcom/sec/common/util/i;->a()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 44
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMdd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 46
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->e:Ljava/lang/String;

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/sec/common/util/log/collector/LogCollectorSender;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->b:Lcom/sec/spp/push/dlc/api/IDlcService;

    return-object p1
.end method

.method private a(Lcom/sec/spp/push/dlc/api/IDlcService;)V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 64
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->c:Z

    if-eqz v0, :cond_0

    .line 65
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    const-string v2, "sendlogs "

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    invoke-static {v12}, Lcom/sec/common/util/log/collector/a;->a(I)Landroid/database/Cursor;

    move-result-object v8

    .line 74
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt v0, v13, :cond_3

    .line 84
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 86
    :cond_1
    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 87
    const-string v0, "category"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 88
    const-string v0, "date"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    const-string v2, "page_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 90
    const-string v2, "action_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 91
    const-string v3, "meta_info"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 92
    const-string v4, "count"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 94
    const/16 v6, 0xb

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v12

    const-string v7, "\u0001"

    aput-object v7, v6, v13

    const/4 v7, 0x2

    aput-object v0, v6, v7

    const/4 v7, 0x3

    const-string v10, "|"

    aput-object v10, v6, v7

    const/4 v7, 0x4

    aput-object v5, v6, v7

    const/4 v7, 0x5

    const-string v10, "|"

    aput-object v10, v6, v7

    const/4 v7, 0x6

    aput-object v2, v6, v7

    const/4 v2, 0x7

    const-string v7, "|"

    aput-object v7, v6, v2

    const/16 v2, 0x8

    aput-object v3, v6, v2

    const/16 v2, 0x9

    const-string v3, "|"

    aput-object v3, v6, v2

    const/16 v2, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-static {v6}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 96
    const-string v2, "QOS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "01000001"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 124
    :cond_2
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 128
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 129
    iput-boolean v12, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->c:Z

    .line 130
    return-void

    .line 100
    :cond_4
    sget-object v2, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v2, v2, Lcom/sec/common/c/a;->c:Z

    if-eqz v2, :cond_5

    .line 101
    sget-object v2, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v3, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sendlogs() log "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_5
    :try_start_0
    iget-boolean v2, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->d:Z

    if-ne v2, v13, :cond_6

    .line 106
    iget-object v2, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_2

    .line 110
    :cond_6
    if-eqz p1, :cond_2

    .line 111
    const-string v2, "006"

    invoke-static {}, Lcom/sec/common/util/i;->a()J

    move-result-wide v3

    const-wide/16 v10, 0x3e8

    div-long/2addr v3, v10

    const/4 v6, 0x0

    move-object v0, p1

    invoke-interface/range {v0 .. v7}, Lcom/sec/spp/push/dlc/api/IDlcService;->requestSend(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 112
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->c:Z

    if-eqz v1, :cond_7

    .line 113
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mService.requestSend\'s ret = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_7
    if-nez v0, :cond_2

    .line 116
    invoke-static {v9}, Lcom/sec/common/util/log/collector/a;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 121
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/common/util/log/collector/LogCollectorSender;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/common/util/log/collector/LogCollectorSender;)Lcom/sec/spp/push/dlc/api/IDlcService;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->b:Lcom/sec/spp/push/dlc/api/IDlcService;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/common/util/log/collector/LogCollectorSender;Lcom/sec/spp/push/dlc/api/IDlcService;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sec/common/util/log/collector/LogCollectorSender;->b(Lcom/sec/spp/push/dlc/api/IDlcService;)V

    return-void
.end method

.method private b(Lcom/sec/spp/push/dlc/api/IDlcService;)V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 133
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->c:Z

    if-eqz v0, :cond_0

    .line 134
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    const-string v2, "sendUrgentlogs "

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_0
    invoke-static {v12}, Lcom/sec/common/util/log/collector/a;->a(I)Landroid/database/Cursor;

    move-result-object v8

    .line 143
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt v0, v13, :cond_3

    .line 153
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 155
    :cond_1
    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 156
    const-string v0, "category"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 158
    const-string v0, "QOS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 189
    :cond_2
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 193
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 194
    iput-boolean v12, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->c:Z

    .line 195
    return-void

    .line 162
    :cond_4
    const-string v0, "date"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 163
    const-string v2, "page_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 164
    const-string v2, "action_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 165
    const-string v3, "meta_info"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 166
    const-string v4, "count"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 168
    const/16 v6, 0xb

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v12

    const-string v7, "\u0001"

    aput-object v7, v6, v13

    const/4 v7, 0x2

    aput-object v0, v6, v7

    const/4 v0, 0x3

    const-string v7, "|"

    aput-object v7, v6, v0

    const/4 v0, 0x4

    aput-object v5, v6, v0

    const/4 v0, 0x5

    const-string v7, "|"

    aput-object v7, v6, v0

    const/4 v0, 0x6

    aput-object v2, v6, v0

    const/4 v0, 0x7

    const-string v2, "|"

    aput-object v2, v6, v0

    const/16 v0, 0x8

    aput-object v3, v6, v0

    const/16 v0, 0x9

    const-string v2, "|"

    aput-object v2, v6, v0

    const/16 v0, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-static {v6}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 170
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->c:Z

    if-eqz v0, :cond_5

    .line 171
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendlogs() log "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_5
    if-eqz p1, :cond_2

    .line 176
    :try_start_0
    const-string v2, "006"

    invoke-static {}, Lcom/sec/common/util/i;->a()J

    move-result-wide v3

    const-wide/16 v10, 0x3e8

    div-long/2addr v3, v10

    const/4 v6, 0x0

    move-object v0, p1

    invoke-interface/range {v0 .. v7}, Lcom/sec/spp/push/dlc/api/IDlcService;->requestSendUrgent(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 177
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->c:Z

    if-eqz v1, :cond_6

    .line 178
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mService.requestSend\'s ret = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_6
    if-nez v0, :cond_2

    .line 181
    invoke-static {v9}, Lcom/sec/common/util/log/collector/a;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 186
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/sec/common/util/log/collector/LogCollectorSender;Lcom/sec/spp/push/dlc/api/IDlcService;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sec/common/util/log/collector/LogCollectorSender;->a(Lcom/sec/spp/push/dlc/api/IDlcService;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 200
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 201
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.spp.push"

    const-string v3, "com.sec.spp.push.dlc.writer.WriterService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 202
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->f:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->c:Z

    if-eqz v0, :cond_0

    .line 204
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    const-string v1, "bind service failed"

    sget-object v2, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_0
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 51
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->c:Z

    if-eqz v0, :cond_0

    .line 52
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/common/util/log/collector/LogCollectorSender;->a:Ljava/lang/String;

    const-string v2, "onHandleIntent "

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_0
    const-string v0, "bUrgent"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->c:Z

    .line 56
    const-string v0, "bFlush"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->d:Z

    .line 58
    iget-object v0, p0, Lcom/sec/common/util/log/collector/LogCollectorSender;->b:Lcom/sec/spp/push/dlc/api/IDlcService;

    if-nez v0, :cond_1

    .line 59
    invoke-virtual {p0}, Lcom/sec/common/util/log/collector/LogCollectorSender;->a()V

    .line 61
    :cond_1
    return-void
.end method
