.class public Lcom/sec/common/util/log/collector/LogCollectorReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LogCollectorReceiver.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/common/util/log/collector/LogCollectorReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/common/util/log/collector/LogCollectorReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 40
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->c:Z

    if-eqz v0, :cond_2

    .line 44
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/common/util/log/collector/LogCollectorReceiver;->a:Ljava/lang/String;

    const-string v2, "onReceive()"

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.spp.push.DLC_AVAILABLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 49
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/common/util/log/collector/LogCollectorSender;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    const-string v1, "bFlush"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 51
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 53
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.spp.push.DLC_UNAVAILABLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_0

    .line 61
    const-string v1, "EXTRA_STR_ACTION"

    const-string v2, "NULL"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->c:Z

    if-eqz v1, :cond_4

    .line 63
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recieve broadcast msg - extra : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/common/util/log/collector/LogCollectorReceiver;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_4
    const-string v1, "ACTION_START_ULOG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 68
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->c:Z

    if-eqz v0, :cond_5

    .line 69
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    const-string v1, "intent extra : ACTION_START_URGENT_LOG"

    sget-object v2, Lcom/sec/common/util/log/collector/LogCollectorReceiver;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_5
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/common/util/log/collector/LogCollectorSender;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    const-string v1, "bUrgent"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 74
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 77
    :cond_6
    const-string v1, "ACTION_STOP_ULOG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->c:Z

    if-eqz v0, :cond_0

    .line 80
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    const-string v1, "intent extra : ACTION_STOP_URGENT_LOG"

    sget-object v2, Lcom/sec/common/util/log/collector/LogCollectorReceiver;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
