.class Lcom/sec/common/util/log/collector/a;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DatabaseHelper.java"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Lcom/sec/common/util/log/collector/a;

.field private static c:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "LogMessageDatabaseHelper"

    sput-object v0, Lcom/sec/common/util/log/collector/a;->a:Ljava/lang/String;

    .line 33
    invoke-static {}, Lcom/sec/common/util/log/collector/a;->a()Lcom/sec/common/util/log/collector/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/util/log/collector/a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/sec/common/util/log/collector/a;->c:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 74
    const-string v0, "log.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 75
    return-void
.end method

.method public static a(I)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 118
    .line 119
    if-nez p0, :cond_0

    .line 120
    sget-object v0, Lcom/sec/common/util/log/collector/a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "log_message"

    const-string v7, "_id"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    .line 123
    :cond_0
    sget-object v0, Lcom/sec/common/util/log/collector/a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "log_message"

    const-string v7, "_id"

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized a()Lcom/sec/common/util/log/collector/a;
    .locals 3

    .prologue
    .line 51
    const-class v1, Lcom/sec/common/util/log/collector/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/common/util/log/collector/a;->b:Lcom/sec/common/util/log/collector/a;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/sec/common/util/log/collector/a;

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/common/util/log/collector/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/common/util/log/collector/a;->b:Lcom/sec/common/util/log/collector/a;

    .line 55
    :cond_0
    sget-object v0, Lcom/sec/common/util/log/collector/a;->b:Lcom/sec/common/util/log/collector/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/sec/common/util/log/collector/b;)V
    .locals 8

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/common/util/log/collector/b;->a()Ljava/lang/String;

    move-result-object v3

    .line 182
    invoke-virtual {p0}, Lcom/sec/common/util/log/collector/b;->b()Ljava/lang/String;

    move-result-object v4

    .line 183
    invoke-virtual {p0}, Lcom/sec/common/util/log/collector/b;->c()Ljava/lang/String;

    move-result-object v1

    .line 184
    invoke-virtual {p0}, Lcom/sec/common/util/log/collector/b;->d()Lorg/json/JSONObject;

    move-result-object v5

    .line 191
    const-string v0, "0100"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const-string v0, "QOS"

    move-object v2, v0

    .line 200
    :goto_0
    if-nez v1, :cond_1

    .line 201
    const-string v0, ""

    move-object v1, v0

    .line 206
    :goto_1
    if-nez v5, :cond_2

    .line 207
    const-string v0, ""

    .line 212
    :goto_2
    const/16 v5, 0x2e

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "INSERT OR REPLACE INTO "

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "log_message"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, " ("

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "category"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, ","

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "date"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, ","

    aput-object v7, v5, v6

    const/4 v6, 0x7

    const-string v7, "page_id"

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, ","

    aput-object v7, v5, v6

    const/16 v6, 0x9

    const-string v7, "action_id"

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, ","

    aput-object v7, v5, v6

    const/16 v6, 0xb

    const-string v7, "meta_info"

    aput-object v7, v5, v6

    const/16 v6, 0xc

    const-string v7, ","

    aput-object v7, v5, v6

    const/16 v6, 0xd

    const-string v7, "count"

    aput-object v7, v5, v6

    const/16 v6, 0xe

    const-string v7, ") VALUES(\'"

    aput-object v7, v5, v6

    const/16 v6, 0xf

    aput-object v2, v5, v6

    const/16 v2, 0x10

    const-string v6, "\',\'"

    aput-object v6, v5, v2

    const/16 v2, 0x11

    aput-object v3, v5, v2

    const/16 v2, 0x12

    const-string v6, "\',\'"

    aput-object v6, v5, v2

    const/16 v2, 0x13

    aput-object v4, v5, v2

    const/16 v2, 0x14

    const-string v6, "\',\'"

    aput-object v6, v5, v2

    const/16 v2, 0x15

    aput-object v1, v5, v2

    const/16 v2, 0x16

    const-string v6, "\',\'"

    aput-object v6, v5, v2

    const/16 v2, 0x17

    aput-object v0, v5, v2

    const/16 v2, 0x18

    const-string v6, "\',"

    aput-object v6, v5, v2

    const/16 v2, 0x19

    const-string v6, "(select "

    aput-object v6, v5, v2

    const/16 v2, 0x1a

    const-string v6, "count"

    aput-object v6, v5, v2

    const/16 v2, 0x1b

    const-string v6, " from "

    aput-object v6, v5, v2

    const/16 v2, 0x1c

    const-string v6, "log_message"

    aput-object v6, v5, v2

    const/16 v2, 0x1d

    const-string v6, " where "

    aput-object v6, v5, v2

    const/16 v2, 0x1e

    const-string v6, "date"

    aput-object v6, v5, v2

    const/16 v2, 0x1f

    const-string v6, "=\'"

    aput-object v6, v5, v2

    const/16 v2, 0x20

    aput-object v3, v5, v2

    const/16 v2, 0x21

    const-string v3, "\' and "

    aput-object v3, v5, v2

    const/16 v2, 0x22

    const-string v3, "page_id"

    aput-object v3, v5, v2

    const/16 v2, 0x23

    const-string v3, "=\'"

    aput-object v3, v5, v2

    const/16 v2, 0x24

    aput-object v4, v5, v2

    const/16 v2, 0x25

    const-string v3, "\' and "

    aput-object v3, v5, v2

    const/16 v2, 0x26

    const-string v3, "action_id"

    aput-object v3, v5, v2

    const/16 v2, 0x27

    const-string v3, "=\'"

    aput-object v3, v5, v2

    const/16 v2, 0x28

    aput-object v1, v5, v2

    const/16 v1, 0x29

    const-string v2, "\' and "

    aput-object v2, v5, v1

    const/16 v1, 0x2a

    const-string v2, "meta_info"

    aput-object v2, v5, v1

    const/16 v1, 0x2b

    const-string v2, "=\'"

    aput-object v2, v5, v1

    const/16 v1, 0x2c

    aput-object v0, v5, v1

    const/16 v0, 0x2d

    const-string v1, "\') + 1);"

    aput-object v1, v5, v0

    invoke-static {v5}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 220
    sget-object v1, Lcom/sec/common/util/log/collector/a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 221
    return-void

    .line 195
    :cond_0
    const-string v0, "BIZ"

    move-object v2, v0

    goto/16 :goto_0

    .line 203
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    .line 209
    :cond_2
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public static a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 154
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "DELETE FROM "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "log_message"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " WHERE _id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 156
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->c:Z

    if-eqz v1, :cond_0

    .line 157
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/util/log/collector/a;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteLogMessages sql : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    sget-object v1, Lcom/sec/common/util/log/collector/a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method public static b()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 230
    sget-object v0, Lcom/sec/common/util/log/collector/a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "log_message"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 98
    const-class v1, Lcom/sec/common/util/log/collector/a;

    monitor-enter v1

    .line 99
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 100
    const/4 v0, 0x0

    :try_start_1
    sput-object v0, Lcom/sec/common/util/log/collector/a;->b:Lcom/sec/common/util/log/collector/a;

    .line 102
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V

    .line 103
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 105
    return-void

    .line 103
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 104
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public declared-synchronized getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 80
    const/16 v0, 0x1b

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "CREATE TABLE "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "log_message"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, " ("

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "category"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, " TEXT NOT NULL DEFAULT \'\',"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "date"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, " TEXT NOT NULL DEFAULT \'\',"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "page_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, " TEXT NOT NULL DEFAULT \'\',"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "action_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, " TEXT NOT NULL DEFAULT \'\',"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "meta_info"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, " TEXT NOT NULL DEFAULT \'\',"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "count"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, " INTEGER NOT NULL DEFAULT 1,"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "UNIQUE("

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "date"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "page_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "action_id"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "meta_info"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, ")"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
