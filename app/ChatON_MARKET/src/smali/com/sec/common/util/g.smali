.class public final enum Lcom/sec/common/util/g;
.super Ljava/lang/Enum;
.source "AsyncWorker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/common/util/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/common/util/g;

.field public static final enum b:Lcom/sec/common/util/g;

.field public static final enum c:Lcom/sec/common/util/g;

.field private static final synthetic d:[Lcom/sec/common/util/g;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, Lcom/sec/common/util/g;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v2}, Lcom/sec/common/util/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/common/util/g;->a:Lcom/sec/common/util/g;

    .line 43
    new-instance v0, Lcom/sec/common/util/g;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, Lcom/sec/common/util/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/common/util/g;->b:Lcom/sec/common/util/g;

    .line 44
    new-instance v0, Lcom/sec/common/util/g;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v4}, Lcom/sec/common/util/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/common/util/g;->c:Lcom/sec/common/util/g;

    .line 41
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/common/util/g;

    sget-object v1, Lcom/sec/common/util/g;->a:Lcom/sec/common/util/g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/common/util/g;->b:Lcom/sec/common/util/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/common/util/g;->c:Lcom/sec/common/util/g;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/common/util/g;->d:[Lcom/sec/common/util/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/common/util/g;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/common/util/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/common/util/g;

    return-object v0
.end method

.method public static values()[Lcom/sec/common/util/g;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/common/util/g;->d:[Lcom/sec/common/util/g;

    invoke-virtual {v0}, [Lcom/sec/common/util/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/common/util/g;

    return-object v0
.end method
