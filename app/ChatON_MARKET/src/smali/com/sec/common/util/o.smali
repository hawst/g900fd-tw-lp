.class public Lcom/sec/common/util/o;
.super Ljava/lang/Object;
.source "StringUtils.java"


# static fields
.field private static a:Lorg/a/a/a/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/a/a/a/f",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lorg/a/a/a/a/p;

    new-instance v1, Lcom/sec/common/util/q;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/common/util/q;-><init>(Lcom/sec/common/util/p;)V

    invoke-direct {v0, v1}, Lorg/a/a/a/a/p;-><init>(Lorg/a/a/a/h;)V

    sput-object v0, Lcom/sec/common/util/o;->a:Lorg/a/a/a/f;

    .line 21
    return-void
.end method

.method public static varargs a([Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 24
    const/4 v1, 0x0

    .line 27
    :try_start_0
    sget-object v0, Lcom/sec/common/util/o;->a:Lorg/a/a/a/f;

    invoke-interface {v0}, Lorg/a/a/a/f;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :try_start_1
    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p0, v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 31
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 29
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 34
    :catch_0
    move-exception v3

    .line 35
    :try_start_3
    const-string v3, "null"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 40
    :catch_1
    move-exception v1

    move-object v1, v0

    .line 41
    :goto_2
    :try_start_4
    const-string v0, ""
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 44
    if-eqz v1, :cond_0

    .line 45
    :try_start_5
    sget-object v2, Lcom/sec/common/util/o;->a:Lorg/a/a/a/f;

    invoke-interface {v2, v1}, Lorg/a/a/a/f;->a(Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 49
    :cond_0
    :goto_3
    return-object v0

    .line 39
    :cond_1
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v1

    .line 44
    if-eqz v0, :cond_2

    .line 45
    :try_start_7
    sget-object v2, Lcom/sec/common/util/o;->a:Lorg/a/a/a/f;

    invoke-interface {v2, v0}, Lorg/a/a/a/f;->a(Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    :cond_2
    :goto_4
    move-object v0, v1

    .line 49
    goto :goto_3

    .line 43
    :catchall_0
    move-exception v0

    .line 44
    :goto_5
    if-eqz v1, :cond_3

    .line 45
    :try_start_8
    sget-object v2, Lcom/sec/common/util/o;->a:Lorg/a/a/a/f;

    invoke-interface {v2, v1}, Lorg/a/a/a/f;->a(Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 49
    :cond_3
    :goto_6
    throw v0

    .line 47
    :catch_2
    move-exception v1

    goto :goto_6

    .line 43
    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_5

    .line 47
    :catch_3
    move-exception v1

    goto :goto_3

    .line 40
    :catch_4
    move-exception v0

    goto :goto_2

    .line 47
    :catch_5
    move-exception v0

    goto :goto_4
.end method
