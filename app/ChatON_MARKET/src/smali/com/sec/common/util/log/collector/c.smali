.class public Lcom/sec/common/util/log/collector/c;
.super Ljava/lang/Object;
.source "LogCollectorManager.java"


# static fields
.field public static a:Ljava/lang/Boolean;

.field private static final b:Ljava/lang/String;

.field private static c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/common/util/log/collector/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/common/util/log/collector/c;->b:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/sec/common/util/log/collector/c;->a:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(I)V
    .locals 3

    .prologue
    .line 84
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "common_library_prefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 85
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 86
    const-string v1, "log_collector_enabled"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 87
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 88
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    sput-object p0, Lcom/sec/common/util/log/collector/c;->c:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public static a()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 31
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "common_library_prefs"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 33
    const-string v1, "log_collector_activated"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "common_library_prefs"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 54
    const-string v2, "log_collector_enabled"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    const/16 v2, 0xf

    if-lt v1, v2, :cond_0

    sget-object v1, Lcom/sec/common/util/log/collector/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 55
    const/4 v0, 0x1

    .line 58
    :cond_0
    return v0
.end method

.method public static c()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 67
    invoke-static {}, Lcom/sec/common/util/log/collector/c;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 81
    :goto_0
    return-void

    .line 72
    :cond_0
    :try_start_0
    const-string v2, "com.sec.spp.push"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 73
    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 74
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->b:Z

    if-eqz v1, :cond_1

    .line 75
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/util/log/collector/c;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "- ###writeWithParam - verName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :cond_1
    :goto_1
    invoke-static {v0}, Lcom/sec/common/util/log/collector/c;->a(I)V

    goto :goto_0

    .line 77
    :catch_0
    move-exception v1

    .line 78
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/sec/common/util/log/collector/c;->a(I)V

    goto :goto_1
.end method

.method public static d()V
    .locals 0

    .prologue
    .line 90
    invoke-static {}, Lcom/sec/common/util/log/collector/a;->a()Lcom/sec/common/util/log/collector/a;

    invoke-static {}, Lcom/sec/common/util/log/collector/a;->b()I

    .line 91
    return-void
.end method
