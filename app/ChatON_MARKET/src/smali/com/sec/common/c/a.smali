.class public abstract Lcom/sec/common/c/a;
.super Ljava/lang/Object;
.source "AbstractLogger.java"


# instance fields
.field protected a:Lcom/sec/common/c/a;

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/common/c/a;-><init>(Lcom/sec/common/c/a;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/sec/common/c/a;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    .line 26
    iput-boolean v0, p0, Lcom/sec/common/c/a;->b:Z

    .line 27
    iput-boolean v0, p0, Lcom/sec/common/c/a;->c:Z

    .line 28
    iput-boolean v0, p0, Lcom/sec/common/c/a;->d:Z

    .line 29
    iput-boolean v0, p0, Lcom/sec/common/c/a;->e:Z

    .line 30
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/common/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 76
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    return-void
.end method

.method public a(ZZZZ)V
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/common/c/a;->b:Z

    .line 34
    iput-boolean p2, p0, Lcom/sec/common/c/a;->c:Z

    .line 35
    iput-boolean p3, p0, Lcom/sec/common/c/a;->d:Z

    .line 36
    iput-boolean p4, p0, Lcom/sec/common/c/a;->e:Z

    .line 37
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/common/c/a;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method protected abstract b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/common/c/a;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sec/common/c/a;->a:Lcom/sec/common/c/a;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/common/c/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    return-void
.end method

.method protected abstract e(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract f(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract g(Ljava/lang/String;Ljava/lang/String;)V
.end method
