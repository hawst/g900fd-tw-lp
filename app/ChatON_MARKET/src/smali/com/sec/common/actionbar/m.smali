.class Lcom/sec/common/actionbar/m;
.super Lcom/sec/common/actionbar/c;
.source "ActionBarImpl.java"


# instance fields
.field final synthetic a:Lcom/sec/common/actionbar/k;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Landroid/view/View;

.field private e:Lcom/sec/common/actionbar/d;

.field private f:Lcom/sec/common/actionbar/n;


# direct methods
.method public constructor <init>(Lcom/sec/common/actionbar/k;)V
    .locals 1

    .prologue
    .line 301
    iput-object p1, p0, Lcom/sec/common/actionbar/m;->a:Lcom/sec/common/actionbar/k;

    invoke-direct {p0}, Lcom/sec/common/actionbar/c;-><init>()V

    .line 302
    invoke-static {p1}, Lcom/sec/common/actionbar/k;->a(Lcom/sec/common/actionbar/k;)Lcom/sec/common/actionbar/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/actionbar/m;->e:Lcom/sec/common/actionbar/d;

    .line 303
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/common/actionbar/m;->d:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/view/View;)Lcom/sec/common/actionbar/c;
    .locals 2

    .prologue
    .line 317
    iput-object p1, p0, Lcom/sec/common/actionbar/m;->d:Landroid/view/View;

    .line 319
    iget-object v0, p0, Lcom/sec/common/actionbar/m;->f:Lcom/sec/common/actionbar/n;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/common/actionbar/m;->f:Lcom/sec/common/actionbar/n;

    iget-object v1, p0, Lcom/sec/common/actionbar/m;->d:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/sec/common/actionbar/n;->a(Landroid/view/View;)V

    .line 323
    :cond_0
    return-object p0
.end method

.method public a(Lcom/sec/common/actionbar/d;)Lcom/sec/common/actionbar/c;
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/sec/common/actionbar/m;->e:Lcom/sec/common/actionbar/d;

    .line 387
    return-object p0
.end method

.method a(I)V
    .locals 0

    .prologue
    .line 342
    iput p1, p0, Lcom/sec/common/actionbar/m;->b:I

    .line 343
    return-void
.end method

.method a(Lcom/sec/common/actionbar/n;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/sec/common/actionbar/m;->f:Lcom/sec/common/actionbar/n;

    .line 396
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/sec/common/actionbar/m;->b:I

    return v0
.end method

.method public c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/common/actionbar/m;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/sec/common/actionbar/d;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/common/actionbar/m;->e:Lcom/sec/common/actionbar/d;

    return-object v0
.end method
