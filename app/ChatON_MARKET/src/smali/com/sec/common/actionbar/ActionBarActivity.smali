.class public Lcom/sec/common/actionbar/ActionBarActivity;
.super Landroid/app/Activity;
.source "ActionBarActivity.java"

# interfaces
.implements Lcom/sec/common/actionbar/s;


# instance fields
.field private final a:Lcom/sec/common/actionbar/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    invoke-static {p0}, Lcom/sec/common/actionbar/e;->a(Landroid/app/Activity;)Lcom/sec/common/actionbar/e;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    return-void
.end method


# virtual methods
.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-super {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/os/Bundle;)V

    .line 44
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 60
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 61
    instance-of v0, p1, Lcom/sec/common/actionbar/t;

    if-eqz v0, :cond_0

    .line 63
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    or-int/2addr v0, v2

    .line 64
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 75
    :goto_0
    return v0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    .line 73
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    or-int/2addr v0, v2

    .line 74
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 75
    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 98
    const/4 v0, 0x0

    .line 99
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 100
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/MenuItem;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 101
    return v0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->b(Landroid/os/Bundle;)V

    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 51
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 86
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 87
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->b(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 88
    return v0
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x1

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public onSupportPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 132
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/actionbar/e;->a(Ljava/lang/CharSequence;I)V

    .line 133
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(I)V

    .line 113
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/View;)V

    .line 119
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/actionbar/e;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    return-void
.end method
