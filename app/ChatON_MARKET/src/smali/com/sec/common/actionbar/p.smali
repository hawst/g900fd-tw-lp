.class Lcom/sec/common/actionbar/p;
.super Lcom/sec/common/actionbar/a;
.source "ActionBarWrapper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Landroid/app/ActionBar;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/common/actionbar/a;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/common/actionbar/p;->a:Landroid/app/Activity;

    .line 28
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    .line 30
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 37
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/common/actionbar/p;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getNavigationMode()I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 95
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 60
    :cond_0
    return-void
.end method

.method public a(Landroid/widget/SpinnerAdapter;Lcom/sec/common/actionbar/b;)V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    new-instance v1, Lcom/sec/common/actionbar/q;

    invoke-direct {v1, p0, p2}, Lcom/sec/common/actionbar/q;-><init>(Lcom/sec/common/actionbar/p;Lcom/sec/common/actionbar/b;)V

    invoke-virtual {v0, p1, v1}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 207
    return-void
.end method

.method public a(Lcom/sec/common/actionbar/c;Z)V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    check-cast p1, Lcom/sec/common/actionbar/r;

    invoke-static {p1}, Lcom/sec/common/actionbar/r;->a(Lcom/sec/common/actionbar/r;)Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 115
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 88
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 175
    return-void
.end method

.method public b()Lcom/sec/common/actionbar/c;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Lcom/sec/common/actionbar/r;

    iget-object v1, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/common/actionbar/r;-><init>(Lcom/sec/common/actionbar/p;Landroid/app/ActionBar$Tab;)V

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 81
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 74
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 190
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getTabCount()I

    move-result v0

    return v0
.end method

.method public c(I)Lcom/sec/common/actionbar/c;
    .locals 2

    .prologue
    .line 134
    new-instance v0, Lcom/sec/common/actionbar/r;

    iget-object v1, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v1, p1}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/common/actionbar/r;-><init>(Lcom/sec/common/actionbar/p;Landroid/app/ActionBar$Tab;)V

    return-object v0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 180
    return-void
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 150
    return-void
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 194
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 197
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 160
    return-void
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 170
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 165
    return-void
.end method

.method public g()Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/common/actionbar/p;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
