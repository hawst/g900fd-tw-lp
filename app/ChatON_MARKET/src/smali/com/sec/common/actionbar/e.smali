.class abstract Lcom/sec/common/actionbar/e;
.super Ljava/lang/Object;
.source "ActionBarHelper.java"


# instance fields
.field protected a:Landroid/app/Activity;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/sec/common/actionbar/e;->a:Landroid/app/Activity;

    .line 57
    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/sec/common/actionbar/e;
    .locals 2

    .prologue
    .line 46
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 47
    new-instance v0, Lcom/sec/common/actionbar/j;

    invoke-direct {v0, p0}, Lcom/sec/common/actionbar/j;-><init>(Landroid/app/Activity;)V

    .line 51
    :goto_0
    return-object v0

    .line 48
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 49
    new-instance v0, Lcom/sec/common/actionbar/i;

    invoke-direct {v0, p0}, Lcom/sec/common/actionbar/i;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    .line 51
    :cond_1
    new-instance v0, Lcom/sec/common/actionbar/f;

    invoke-direct {v0, p0}, Lcom/sec/common/actionbar/f;-><init>(Landroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;
    .locals 0

    .prologue
    .line 134
    return-object p1
.end method

.method public a()V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public abstract a(I)V
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method protected abstract a(Ljava/lang/CharSequence;I)V
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    return v0
.end method

.method public abstract b()Lcom/sec/common/actionbar/a;
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public b(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x1

    return v0
.end method
