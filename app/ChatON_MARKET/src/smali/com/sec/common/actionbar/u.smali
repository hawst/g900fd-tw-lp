.class Lcom/sec/common/actionbar/u;
.super Ljava/lang/Object;
.source "MenuItemImpl.java"

# interfaces
.implements Landroid/view/MenuItem;


# instance fields
.field private a:Lcom/sec/common/actionbar/t;

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Landroid/view/MenuItem;

.field private l:I

.field private m:Landroid/view/MenuItem$OnMenuItemClickListener;


# direct methods
.method public constructor <init>(Lcom/sec/common/actionbar/t;IIILjava/lang/CharSequence;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput v0, p0, Lcom/sec/common/actionbar/u;->h:I

    .line 42
    iput-boolean v1, p0, Lcom/sec/common/actionbar/u;->i:Z

    .line 43
    iput-boolean v1, p0, Lcom/sec/common/actionbar/u;->j:Z

    .line 45
    iput v0, p0, Lcom/sec/common/actionbar/u;->l:I

    .line 49
    iput-object p1, p0, Lcom/sec/common/actionbar/u;->a:Lcom/sec/common/actionbar/t;

    .line 50
    iput p2, p0, Lcom/sec/common/actionbar/u;->b:I

    .line 51
    iput p3, p0, Lcom/sec/common/actionbar/u;->c:I

    .line 52
    iput p4, p0, Lcom/sec/common/actionbar/u;->d:I

    .line 53
    iput-object p5, p0, Lcom/sec/common/actionbar/u;->e:Ljava/lang/CharSequence;

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/common/actionbar/u;->f:Ljava/lang/CharSequence;

    .line 55
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->a:Lcom/sec/common/actionbar/t;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->a:Lcom/sec/common/actionbar/t;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/t;->c()V

    .line 235
    :cond_0
    return-void
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/sec/common/actionbar/u;->l:I

    return v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 242
    iput p1, p0, Lcom/sec/common/actionbar/u;->l:I

    .line 244
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 245
    iget v0, p0, Lcom/sec/common/actionbar/u;->l:I

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 254
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/common/actionbar/u;->c()V

    .line 255
    return-void

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/sec/common/actionbar/u;->j:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method a(Landroid/view/MenuItem;)V
    .locals 2

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    .line 60
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/sec/common/actionbar/u;->e:Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 61
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/sec/common/actionbar/u;->f:Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 63
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/sec/common/actionbar/u;->g:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 67
    :cond_0
    iget v0, p0, Lcom/sec/common/actionbar/u;->h:I

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget v1, p0, Lcom/sec/common/actionbar/u;->h:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/sec/common/actionbar/u;->i:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 72
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/sec/common/actionbar/u;->j:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 74
    iget v0, p0, Lcom/sec/common/actionbar/u;->l:I

    if-eqz v0, :cond_3

    .line 75
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 82
    :goto_0
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->m:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v0, :cond_2

    .line 83
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/sec/common/actionbar/u;->m:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 85
    :cond_2
    return-void

    .line 79
    :cond_3
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/sec/common/actionbar/u;->j:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method b()Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->m:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-object v0
.end method

.method public collapseActionView()Z
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    return v0
.end method

.method public expandActionView()Z
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    return v0
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    return-object v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x0

    return v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/sec/common/actionbar/u;->b:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->g:Landroid/graphics/drawable/Drawable;

    .line 181
    :goto_0
    return-object v0

    .line 177
    :cond_0
    iget v0, p0, Lcom/sec/common/actionbar/u;->h:I

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->a:Lcom/sec/common/actionbar/t;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/t;->b()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sec/common/actionbar/u;->h:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 181
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/common/actionbar/u;->c:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 407
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    .prologue
    .line 353
    const/4 v0, 0x0

    return v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/common/actionbar/u;->d:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 401
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/common/actionbar/u;->f:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->e:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public hasSubMenu()Z
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x0

    return v0
.end method

.method public isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    return v0
.end method

.method public isCheckable()Z
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x0

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 389
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/sec/common/actionbar/u;->i:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/sec/common/actionbar/u;->j:Z

    return v0
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 293
    return-object p0
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 419
    return-object p0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 413
    return-object p0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 359
    return-object p0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 371
    return-object p0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 383
    return-object p0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 186
    iput-boolean p1, p0, Lcom/sec/common/actionbar/u;->i:Z

    .line 188
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 192
    :cond_0
    invoke-direct {p0}, Lcom/sec/common/actionbar/u;->c()V

    .line 194
    return-object p0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/common/actionbar/u;->g:Landroid/graphics/drawable/Drawable;

    .line 160
    iput p1, p0, Lcom/sec/common/actionbar/u;->h:I

    .line 162
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 166
    :cond_0
    invoke-direct {p0}, Lcom/sec/common/actionbar/u;->c()V

    .line 168
    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/common/actionbar/u;->h:I

    .line 146
    iput-object p1, p0, Lcom/sec/common/actionbar/u;->g:Landroid/graphics/drawable/Drawable;

    .line 148
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 152
    :cond_0
    invoke-direct {p0}, Lcom/sec/common/actionbar/u;->c()V

    .line 154
    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 329
    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 347
    return-object p0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 323
    return-object p0
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/common/actionbar/u;->m:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 275
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/sec/common/actionbar/u;->m:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 279
    :cond_0
    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 341
    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 0

    .prologue
    .line 259
    return-void
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x0

    return-object v0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->a:Lcom/sec/common/actionbar/t;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/t;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/common/actionbar/u;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 118
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 122
    :cond_0
    invoke-direct {p0}, Lcom/sec/common/actionbar/u;->c()V

    .line 124
    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/common/actionbar/u;->e:Ljava/lang/CharSequence;

    .line 105
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 109
    :cond_0
    invoke-direct {p0}, Lcom/sec/common/actionbar/u;->c()V

    .line 111
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/common/actionbar/u;->f:Ljava/lang/CharSequence;

    .line 135
    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 209
    iput-boolean p1, p0, Lcom/sec/common/actionbar/u;->j:Z

    .line 211
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 212
    iget v0, p0, Lcom/sec/common/actionbar/u;->l:I

    if-eqz v0, :cond_1

    .line 213
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 221
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/common/actionbar/u;->c()V

    .line 223
    return-object p0

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/sec/common/actionbar/u;->k:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/sec/common/actionbar/u;->j:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
