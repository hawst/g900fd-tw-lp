.class public Lcom/sec/common/actionbar/ActionBarMapActivity;
.super Lcom/google/android/maps/MapActivity;
.source "ActionBarMapActivity.java"

# interfaces
.implements Lcom/sec/common/actionbar/s;


# instance fields
.field private final a:Lcom/sec/common/actionbar/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/maps/MapActivity;-><init>()V

    .line 22
    invoke-static {p0}, Lcom/sec/common/actionbar/e;->a(Landroid/app/Activity;)Lcom/sec/common/actionbar/e;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    return-void
.end method


# virtual methods
.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method protected isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/os/Bundle;)V

    .line 29
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 46
    instance-of v0, p1, Lcom/sec/common/actionbar/t;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v0

    or-int/2addr v0, v2

    .line 49
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 60
    :goto_0
    return v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    .line 58
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    or-int/2addr v0, v2

    .line 59
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 60
    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 88
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 89
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/MenuItem;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 90
    return v0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->b(Landroid/os/Bundle;)V

    .line 35
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 36
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 70
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 71
    const/4 v0, 0x0

    .line 72
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 73
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->b(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 77
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method public onSupportPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0, p1, p2}, Lcom/google/android/maps/MapActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 121
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/actionbar/e;->a(Ljava/lang/CharSequence;I)V

    .line 122
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(I)V

    .line 102
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/View;)V

    .line 108
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarMapActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/actionbar/e;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    return-void
.end method
