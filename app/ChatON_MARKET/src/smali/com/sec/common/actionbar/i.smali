.class Lcom/sec/common/actionbar/i;
.super Lcom/sec/common/actionbar/e;
.source "ActionBarHelperHoneycomb.java"


# instance fields
.field private b:Lcom/sec/common/actionbar/p;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/common/actionbar/e;-><init>(Landroid/app/Activity;)V

    .line 35
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/common/actionbar/i;->b:Lcom/sec/common/actionbar/p;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/sec/common/actionbar/p;

    iget-object v1, p0, Lcom/sec/common/actionbar/i;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/common/actionbar/p;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/common/actionbar/i;->b:Lcom/sec/common/actionbar/p;

    .line 72
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Lcom/sec/common/actionbar/e;->a()V

    .line 56
    iget-object v0, p0, Lcom/sec/common/actionbar/i;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 57
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/common/actionbar/i;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->setContentView(I)V

    .line 85
    invoke-direct {p0}, Lcom/sec/common/actionbar/i;->c()V

    .line 86
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/common/actionbar/i;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    .line 92
    invoke-direct {p0}, Lcom/sec/common/actionbar/i;->c()V

    .line 93
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/common/actionbar/i;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    invoke-direct {p0}, Lcom/sec/common/actionbar/i;->c()V

    .line 100
    return-void
.end method

.method protected a(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/sec/common/actionbar/i;->c()V

    .line 119
    iget-object v0, p0, Lcom/sec/common/actionbar/i;->b:Lcom/sec/common/actionbar/p;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/p;->a(Ljava/lang/CharSequence;)V

    .line 120
    return-void
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/common/actionbar/i;->a:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/s;

    invoke-interface {v0, p1}, Lcom/sec/common/actionbar/s;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/common/actionbar/i;->a:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/s;

    invoke-interface {v0, p1}, Lcom/sec/common/actionbar/s;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public b()Lcom/sec/common/actionbar/a;
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/common/actionbar/i;->c()V

    .line 78
    iget-object v0, p0, Lcom/sec/common/actionbar/i;->b:Lcom/sec/common/actionbar/p;

    return-object v0
.end method

.method public b(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/common/actionbar/i;->a:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/s;

    invoke-interface {v0, p1}, Lcom/sec/common/actionbar/s;->onSupportPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
