.class public Lcom/sec/common/actionbar/ActionBarListActivity;
.super Landroid/app/ListActivity;
.source "ActionBarListActivity.java"

# interfaces
.implements Lcom/sec/common/actionbar/s;


# instance fields
.field private final a:Lcom/sec/common/actionbar/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 21
    invoke-static {p0}, Lcom/sec/common/actionbar/e;->a(Landroid/app/Activity;)Lcom/sec/common/actionbar/e;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    return-void
.end method


# virtual methods
.method public a()Lcom/sec/common/actionbar/a;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/e;->b()Lcom/sec/common/actionbar/a;

    move-result-object v0

    return-object v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-super {p0}, Landroid/app/ListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/os/Bundle;)V

    .line 27
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 59
    instance-of v0, p1, Lcom/sec/common/actionbar/t;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v0

    or-int/2addr v0, v2

    .line 62
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 73
    :goto_0
    return v0

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    .line 71
    :cond_1
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    or-int/2addr v0, v2

    .line 72
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 73
    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 101
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 102
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/MenuItem;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 103
    return v0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->b(Landroid/os/Bundle;)V

    .line 32
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 33
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 83
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 85
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 86
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->b(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 90
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x1

    return v0
.end method

.method public onSupportPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0, p1, p2}, Landroid/app/ListActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 120
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/actionbar/e;->a(Ljava/lang/CharSequence;I)V

    .line 121
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/app/ListActivity;->setContentView(I)V

    .line 39
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(I)V

    .line 40
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/app/ListActivity;->setContentView(Landroid/view/View;)V

    .line 46
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/View;)V

    .line 47
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Landroid/app/ListActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarListActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/actionbar/e;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 54
    return-void
.end method
