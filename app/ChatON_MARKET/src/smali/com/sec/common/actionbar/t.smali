.class public Lcom/sec/common/actionbar/t;
.super Ljava/lang/Object;
.source "MenuImpl.java"

# interfaces
.implements Landroid/view/Menu;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/content/res/Resources;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/common/actionbar/u;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/view/Menu;

.field private e:Lcom/sec/common/actionbar/ActionBarView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/common/actionbar/ActionBarView;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/sec/common/actionbar/t;->a:Landroid/content/Context;

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/actionbar/t;->b:Landroid/content/res/Resources;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    .line 36
    iput-object p2, p0, Lcom/sec/common/actionbar/t;->e:Lcom/sec/common/actionbar/ActionBarView;

    .line 37
    return-void
.end method

.method private static a(Ljava/util/List;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Landroid/view/MenuItem;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 93
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 94
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 95
    invoke-interface {v0}, Landroid/view/MenuItem;->getOrder()I

    move-result v0

    if-gt v0, p1, :cond_0

    .line 96
    add-int/lit8 v0, v1, 0x1

    .line 100
    :goto_1
    return v0

    .line 93
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 100
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 6

    .prologue
    .line 82
    new-instance v0, Lcom/sec/common/actionbar/u;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/common/actionbar/u;-><init>(Lcom/sec/common/actionbar/t;IIILjava/lang/CharSequence;)V

    .line 83
    iget-object v1, p0, Lcom/sec/common/actionbar/t;->d:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 84
    iget-object v1, p0, Lcom/sec/common/actionbar/t;->d:Landroid/view/Menu;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    .line 86
    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/u;->a(Landroid/view/MenuItem;)V

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-static {v2, p3}, Lcom/sec/common/actionbar/t;->a(Ljava/util/List;I)I

    move-result v2

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 89
    return-object v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 122
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/u;

    .line 128
    iget-object v1, p0, Lcom/sec/common/actionbar/t;->d:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/sec/common/actionbar/t;->d:Landroid/view/Menu;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/u;->getItemId()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 3

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/common/actionbar/t;->size()I

    move-result v2

    .line 106
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 107
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 108
    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 113
    :goto_1
    return v0

    .line 106
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 113
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->a:Landroid/content/Context;

    return-object v0
.end method

.method a(Landroid/view/Menu;)V
    .locals 6

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/common/actionbar/t;->d:Landroid/view/Menu;

    .line 42
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/u;

    .line 43
    invoke-virtual {v0}, Lcom/sec/common/actionbar/u;->getGroupId()I

    move-result v2

    invoke-virtual {v0}, Lcom/sec/common/actionbar/u;->getItemId()I

    move-result v3

    invoke-virtual {v0}, Lcom/sec/common/actionbar/u;->getOrder()I

    move-result v4

    invoke-virtual {v0}, Lcom/sec/common/actionbar/u;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {p1, v2, v3, v4, v5}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    .line 46
    invoke-virtual {v0, v2}, Lcom/sec/common/actionbar/u;->a(Landroid/view/MenuItem;)V

    goto :goto_0

    .line 48
    :cond_0
    return-void
.end method

.method public add(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v1, v1, v0}, Lcom/sec/common/actionbar/t;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/common/actionbar/t;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/common/actionbar/t;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, v0, v0, v0, p1}, Lcom/sec/common/actionbar/t;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 2

    .prologue
    .line 227
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .locals 2

    .prologue
    .line 211
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 2

    .prologue
    .line 221
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 2

    .prologue
    .line 216
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 2

    .prologue
    .line 206
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->b:Landroid/content/res/Resources;

    return-object v0
.end method

.method c()V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->e:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarView;->c()V

    .line 170
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/sec/common/actionbar/t;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 171
    invoke-virtual {p0, v1}, Lcom/sec/common/actionbar/t;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/u;

    .line 173
    invoke-virtual {v0}, Lcom/sec/common/actionbar/u;->a()I

    move-result v2

    if-eqz v2, :cond_0

    .line 174
    iget-object v2, p0, Lcom/sec/common/actionbar/t;->e:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v2, v0}, Lcom/sec/common/actionbar/ActionBarView;->a(Landroid/view/MenuItem;)V

    .line 170
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 177
    :cond_1
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 137
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->d:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->d:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 141
    :cond_0
    invoke-virtual {p0}, Lcom/sec/common/actionbar/t;->c()V

    .line 142
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 247
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public findItem(I)Landroid/view/MenuItem;
    .locals 4

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/sec/common/actionbar/t;->size()I

    move-result v2

    .line 147
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 148
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 149
    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 154
    :goto_1
    return-object v0

    .line 147
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 154
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    return-object v0
.end method

.method public hasVisibleItems()Z
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/u;

    .line 194
    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    const/4 v0, 0x1

    .line 199
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 257
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public performIdentifierAction(II)Z
    .locals 2

    .prologue
    .line 262
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 2

    .prologue
    .line 252
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeGroup(I)V
    .locals 2

    .prologue
    .line 232
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeItem(I)V
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0, p1}, Lcom/sec/common/actionbar/t;->a(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/common/actionbar/t;->b(I)V

    .line 119
    return-void
.end method

.method public setGroupCheckable(IZZ)V
    .locals 2

    .prologue
    .line 237
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setGroupEnabled(IZ)V
    .locals 2

    .prologue
    .line 242
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setGroupVisible(IZ)V
    .locals 4

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/common/actionbar/t;->size()I

    move-result v2

    .line 183
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 184
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 185
    invoke-interface {v0}, Landroid/view/MenuItem;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 186
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 183
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 189
    :cond_1
    return-void
.end method

.method public setQwertyMode(Z)V
    .locals 2

    .prologue
    .line 267
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This operation is not supported for SimpleMenu"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/common/actionbar/t;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
