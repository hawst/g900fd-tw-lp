.class final Lcom/sec/common/actionbar/h;
.super Landroid/view/MenuInflater;
.source "ActionBarHelperBase.java"


# instance fields
.field final synthetic a:Lcom/sec/common/actionbar/f;

.field private b:Landroid/view/MenuInflater;


# direct methods
.method constructor <init>(Lcom/sec/common/actionbar/f;Landroid/content/Context;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcom/sec/common/actionbar/h;->a:Lcom/sec/common/actionbar/f;

    .line 342
    invoke-direct {p0, p2}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 343
    iput-object p3, p0, Lcom/sec/common/actionbar/h;->b:Landroid/view/MenuInflater;

    .line 344
    return-void
.end method

.method private a(I)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 370
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 372
    const/4 v1, 0x0

    .line 374
    :try_start_0
    iget-object v3, p0, Lcom/sec/common/actionbar/h;->a:Lcom/sec/common/actionbar/f;

    iget-object v3, v3, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 376
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v3

    .line 381
    :goto_0
    if-nez v0, :cond_3

    .line 382
    packed-switch v3, :pswitch_data_0

    .line 407
    :cond_0
    :goto_1
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v3

    goto :goto_0

    .line 384
    :pswitch_0
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "item"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 388
    const-string v3, "http://schemas.android.com/apk/res/android"

    const-string v5, "id"

    const/4 v6, 0x0

    invoke-interface {v1, v3, v5, v6}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    .line 390
    if-eqz v3, :cond_0

    .line 394
    const-string v5, "http://schemas.android.com/apk/res/android"

    const-string v6, "showAsAction"

    const/4 v7, -0x1

    invoke-interface {v1, v5, v6, v7}, Landroid/content/res/XmlResourceParser;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    .line 396
    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    if-ne v5, v2, :cond_0

    .line 398
    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 409
    :catch_0
    move-exception v0

    .line 410
    :try_start_1
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 415
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_2
    throw v0

    :pswitch_1
    move v0, v2

    .line 403
    goto :goto_1

    .line 414
    :cond_3
    if-eqz v1, :cond_4

    .line 415
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    .line 419
    :cond_4
    return-object v4

    .line 411
    :catch_1
    move-exception v0

    .line 412
    :try_start_2
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 382
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public inflate(ILandroid/view/Menu;)V
    .locals 4

    .prologue
    .line 349
    invoke-direct {p0, p1}, Lcom/sec/common/actionbar/h;->a(I)Ljava/util/List;

    move-result-object v2

    .line 352
    iget-object v0, p0, Lcom/sec/common/actionbar/h;->b:Landroid/view/MenuInflater;

    invoke-virtual {v0, p1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 354
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p2}, Landroid/view/Menu;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 355
    invoke-interface {p2, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 357
    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 358
    check-cast v0, Lcom/sec/common/actionbar/u;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/sec/common/actionbar/u;->a(I)V

    .line 354
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 361
    :cond_1
    return-void
.end method
