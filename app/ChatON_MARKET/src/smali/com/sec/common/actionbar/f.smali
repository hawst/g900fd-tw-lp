.class Lcom/sec/common/actionbar/f;
.super Lcom/sec/common/actionbar/e;
.source "ActionBarHelperBase.java"

# interfaces
.implements Lcom/sec/common/actionbar/o;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Landroid/view/ViewGroup;

.field private d:Landroid/view/ViewGroup;

.field private e:Lcom/sec/common/actionbar/ActionBarView;

.field private f:Lcom/sec/common/actionbar/a;

.field private g:I

.field private h:Lcom/sec/common/actionbar/t;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/sec/common/actionbar/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/common/actionbar/f;->b:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/common/actionbar/e;-><init>(Landroid/app/Activity;)V

    .line 67
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/sec/common/actionbar/f;->d()V

    .line 191
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->f:Lcom/sec/common/actionbar/a;

    if-nez v0, :cond_0

    .line 192
    new-instance v0, Lcom/sec/common/actionbar/k;

    iget-object v1, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/common/actionbar/k;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/common/actionbar/f;->f:Lcom/sec/common/actionbar/a;

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->e:Lcom/sec/common/actionbar/ActionBarView;

    if-nez v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    sget v1, Lcom/sec/common/c;->actionbar_view:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarView;

    iput-object v0, p0, Lcom/sec/common/actionbar/f;->e:Lcom/sec/common/actionbar/ActionBarView;

    .line 197
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->f:Lcom/sec/common/actionbar/a;

    iget-object v1, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 199
    :cond_1
    return-void
.end method

.method private d()V
    .locals 8

    .prologue
    const v7, 0x1020002

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 205
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->c:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/common/actionbar/f;->c:Landroid/view/ViewGroup;

    .line 208
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/sec/common/e;->ActionBarCompat:[I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 210
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 211
    invoke-virtual {p0, v6}, Lcom/sec/common/actionbar/f;->b(I)Z

    .line 214
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    if-nez v0, :cond_4

    .line 219
    const/4 v0, 0x0

    .line 221
    iget-object v1, p0, Lcom/sec/common/actionbar/f;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_5

    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 224
    iget-object v1, p0, Lcom/sec/common/actionbar/f;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    .line 225
    iget-object v4, p0, Lcom/sec/common/actionbar/f;->c:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 226
    iget-object v5, p0, Lcom/sec/common/actionbar/f;->c:Landroid/view/ViewGroup;

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 227
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 232
    :goto_1
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/sec/common/d;->actionbar_compat_decor:I

    iget-object v3, p0, Lcom/sec/common/actionbar/f;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 234
    sget v0, Lcom/sec/common/c;->content:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    .line 236
    if-eqz v1, :cond_3

    .line 237
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 238
    iget-object v3, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 242
    :cond_3
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->c:Landroid/view/ViewGroup;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    .line 243
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setId(I)V

    .line 245
    invoke-virtual {p0, v6}, Lcom/sec/common/actionbar/f;->c(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    sget v0, Lcom/sec/common/c;->actionbar_layout:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 249
    :cond_4
    return-void

    :cond_5
    move-object v1, v0

    goto :goto_1
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 308
    invoke-direct {p0}, Lcom/sec/common/actionbar/f;->c()V

    .line 311
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->e:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarView;->c()V

    .line 314
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->h:Lcom/sec/common/actionbar/t;

    if-nez v0, :cond_0

    .line 315
    new-instance v0, Lcom/sec/common/actionbar/t;

    iget-object v1, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/common/actionbar/f;->e:Lcom/sec/common/actionbar/ActionBarView;

    invoke-direct {v0, v1, v3}, Lcom/sec/common/actionbar/t;-><init>(Landroid/content/Context;Lcom/sec/common/actionbar/ActionBarView;)V

    iput-object v0, p0, Lcom/sec/common/actionbar/f;->h:Lcom/sec/common/actionbar/t;

    .line 317
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->e:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0, p0}, Lcom/sec/common/actionbar/ActionBarView;->a(Lcom/sec/common/actionbar/o;)V

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->h:Lcom/sec/common/actionbar/t;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/t;->clear()V

    .line 321
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/common/actionbar/f;->h:Lcom/sec/common/actionbar/t;

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    .line 322
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/sec/common/actionbar/f;->h:Lcom/sec/common/actionbar/t;

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/Activity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    .line 324
    new-instance v0, Lcom/sec/common/actionbar/u;

    iget-object v1, p0, Lcom/sec/common/actionbar/f;->h:Lcom/sec/common/actionbar/t;

    const v3, 0x102002c

    iget-object v4, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v5, v4, Landroid/content/pm/ApplicationInfo;->name:Ljava/lang/String;

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/common/actionbar/u;-><init>(Lcom/sec/common/actionbar/t;IIILjava/lang/CharSequence;)V

    .line 326
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 327
    iget-object v1, p0, Lcom/sec/common/actionbar/f;->e:Lcom/sec/common/actionbar/ActionBarView;

    new-instance v2, Lcom/sec/common/actionbar/g;

    invoke-direct {v2, p0, v0}, Lcom/sec/common/actionbar/g;-><init>(Lcom/sec/common/actionbar/f;Landroid/view/MenuItem;)V

    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/ActionBarView;->a(Landroid/view/View$OnClickListener;)V

    .line 333
    return-void
.end method


# virtual methods
.method public a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 304
    new-instance v0, Lcom/sec/common/actionbar/h;

    iget-object v1, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/common/actionbar/h;-><init>(Lcom/sec/common/actionbar/f;Landroid/content/Context;Landroid/view/MenuInflater;)V

    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 293
    invoke-super {p0}, Lcom/sec/common/actionbar/e;->a()V

    .line 295
    invoke-direct {p0}, Lcom/sec/common/actionbar/f;->e()V

    .line 296
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/common/actionbar/f;->d()V

    .line 108
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 109
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 111
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_0

    .line 113
    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 115
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/sec/common/actionbar/f;->d()V

    .line 122
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 123
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 125
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    .line 127
    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 129
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/MenuItem;)V
    .locals 5

    .prologue
    .line 87
    :try_start_0
    move-object v0, p2

    check-cast v0, Lcom/sec/common/actionbar/u;

    move-object v1, v0

    .line 89
    invoke-virtual {v1}, Lcom/sec/common/actionbar/u;->b()Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v1

    .line 91
    if-eqz v1, :cond_0

    invoke-interface {v1, p2}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v1

    .line 95
    sget-object v2, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v2, v2, Lcom/sec/common/c/a;->e:Z

    if-eqz v2, :cond_0

    .line 96
    sget-object v2, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v3, Lcom/sec/common/actionbar/f;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/sec/common/actionbar/f;->d()V

    .line 136
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 137
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 139
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_0

    .line 141
    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 143
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->f:Lcom/sec/common/actionbar/a;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->f:Lcom/sec/common/actionbar/a;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/a;->a(Ljava/lang/CharSequence;)V

    .line 259
    :cond_0
    return-void
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 268
    instance-of v0, p1, Lcom/sec/common/actionbar/t;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/s;

    invoke-interface {v0, p1}, Lcom/sec/common/actionbar/s;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 274
    :goto_0
    return v0

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->h:Lcom/sec/common/actionbar/t;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/t;->a(Landroid/view/Menu;)V

    .line 274
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 284
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-eq v0, v1, :cond_0

    .line 285
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->h:Lcom/sec/common/actionbar/t;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/t;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/s;

    invoke-interface {v0, p1}, Lcom/sec/common/actionbar/s;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public b()Lcom/sec/common/actionbar/a;
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/sec/common/actionbar/f;->c()V

    .line 182
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->f:Lcom/sec/common/actionbar/a;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/common/actionbar/f;->c()V

    .line 81
    invoke-direct {p0}, Lcom/sec/common/actionbar/f;->e()V

    .line 82
    return-void
.end method

.method public b(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 148
    iget-object v1, p0, Lcom/sec/common/actionbar/f;->d:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 149
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "requestFeature() must be called before adding content"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 163
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 159
    :pswitch_1
    iget v1, p0, Lcom/sec/common/actionbar/f;->g:I

    shl-int v2, v0, p1

    or-int/2addr v1, v2

    iput v1, p0, Lcom/sec/common/actionbar/f;->g:I

    goto :goto_0

    .line 152
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public b(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/common/actionbar/f;->a:Landroid/app/Activity;

    check-cast v0, Lcom/sec/common/actionbar/s;

    iget-object v1, p0, Lcom/sec/common/actionbar/f;->h:Lcom/sec/common/actionbar/t;

    invoke-interface {v0, v1}, Lcom/sec/common/actionbar/s;->onSupportPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public c(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 174
    iget v1, p0, Lcom/sec/common/actionbar/f;->g:I

    shl-int v2, v0, p1

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
