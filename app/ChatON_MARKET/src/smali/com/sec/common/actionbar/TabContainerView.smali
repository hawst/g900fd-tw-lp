.class Lcom/sec/common/actionbar/TabContainerView;
.super Lcom/sec/common/widget/HoneycombLinearLayout;
.source "TabContainerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/common/actionbar/TabContainerView$TabView;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/sec/common/actionbar/w;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/common/widget/HoneycombLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/common/actionbar/TabContainerView;->a:Ljava/util/List;

    .line 41
    sget-object v0, Lcom/sec/common/e;->ActionBarCompat:[I

    sget v1, Lcom/sec/common/b;->actionBarStyle:I

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 42
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/common/actionbar/TabContainerView;->setBackgroundResource(I)V

    .line 44
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    return-void
.end method

.method private b(Lcom/sec/common/actionbar/c;)Lcom/sec/common/actionbar/TabContainerView$TabView;
    .locals 3

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/sec/common/actionbar/TabContainerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/sec/common/d;->actionbar_compat_tab_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/TabContainerView$TabView;

    .line 55
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    check-cast p1, Lcom/sec/common/actionbar/m;

    invoke-static {v0, p1}, Lcom/sec/common/actionbar/TabContainerView$TabView;->a(Lcom/sec/common/actionbar/TabContainerView$TabView;Lcom/sec/common/actionbar/m;)Lcom/sec/common/actionbar/m;

    .line 58
    invoke-static {v0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->a(Lcom/sec/common/actionbar/TabContainerView$TabView;)Lcom/sec/common/actionbar/m;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/common/actionbar/TabContainerView;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/m;->a(I)V

    .line 59
    invoke-static {v0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->a(Lcom/sec/common/actionbar/TabContainerView$TabView;)Lcom/sec/common/actionbar/m;

    move-result-object v1

    new-instance v2, Lcom/sec/common/actionbar/v;

    invoke-direct {v2, p0, v0}, Lcom/sec/common/actionbar/v;-><init>(Lcom/sec/common/actionbar/TabContainerView;Lcom/sec/common/actionbar/TabContainerView$TabView;)V

    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/m;->a(Lcom/sec/common/actionbar/n;)V

    .line 71
    return-object v0
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/common/actionbar/TabContainerView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(Lcom/sec/common/actionbar/c;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 102
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 105
    invoke-direct {p0, p1}, Lcom/sec/common/actionbar/TabContainerView;->b(Lcom/sec/common/actionbar/c;)Lcom/sec/common/actionbar/TabContainerView$TabView;

    move-result-object v1

    .line 106
    invoke-virtual {p1}, Lcom/sec/common/actionbar/c;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/TabContainerView$TabView;->setTitle(Ljava/lang/CharSequence;)V

    .line 107
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/TabContainerView$TabView;->setFocusable(Z)V

    .line 108
    invoke-virtual {v1, p0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    invoke-virtual {p1}, Lcom/sec/common/actionbar/c;->a()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 110
    invoke-virtual {p1}, Lcom/sec/common/actionbar/c;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/common/actionbar/TabContainerView$TabView;->setCustomView(Landroid/view/View;)V

    .line 113
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/sec/common/actionbar/TabContainerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    return-void
.end method

.method public a(Lcom/sec/common/actionbar/w;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/common/actionbar/TabContainerView;->b:Lcom/sec/common/actionbar/w;

    .line 149
    return-void
.end method

.method b(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 80
    iget-object v0, p0, Lcom/sec/common/actionbar/TabContainerView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 94
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 84
    :goto_0
    iget-object v0, p0, Lcom/sec/common/actionbar/TabContainerView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 85
    if-ne v1, p1, :cond_3

    const/4 v0, 0x1

    move v3, v0

    .line 87
    :goto_1
    iget-object v0, p0, Lcom/sec/common/actionbar/TabContainerView;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/TabContainerView$TabView;

    .line 88
    invoke-virtual {v0, v3}, Lcom/sec/common/actionbar/TabContainerView$TabView;->setSelected(Z)V

    .line 90
    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/common/actionbar/TabContainerView;->b:Lcom/sec/common/actionbar/w;

    if-eqz v3, :cond_2

    .line 91
    iget-object v3, p0, Lcom/sec/common/actionbar/TabContainerView;->b:Lcom/sec/common/actionbar/w;

    invoke-static {v0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->a(Lcom/sec/common/actionbar/TabContainerView$TabView;)Lcom/sec/common/actionbar/m;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/sec/common/actionbar/w;->b(Lcom/sec/common/actionbar/c;)V

    .line 84
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    move v3, v2

    .line 85
    goto :goto_1
.end method

.method public c(I)Lcom/sec/common/actionbar/c;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/common/actionbar/TabContainerView;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/TabContainerView$TabView;

    invoke-static {v0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->a(Lcom/sec/common/actionbar/TabContainerView$TabView;)Lcom/sec/common/actionbar/m;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 133
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_1

    .line 135
    invoke-virtual {p0, v0}, Lcom/sec/common/actionbar/TabContainerView;->b(I)V

    .line 140
    :cond_0
    return-void

    .line 133
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
