.class public Lcom/sec/common/actionbar/TabContainerView$TabView;
.super Landroid/widget/LinearLayout;
.source "TabContainerView.java"


# instance fields
.field private a:Lcom/sec/common/actionbar/m;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 162
    invoke-direct {p0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->a()V

    .line 163
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 168
    invoke-direct {p0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->a()V

    .line 169
    return-void
.end method

.method static synthetic a(Lcom/sec/common/actionbar/TabContainerView$TabView;)Lcom/sec/common/actionbar/m;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->a:Lcom/sec/common/actionbar/m;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/common/actionbar/TabContainerView$TabView;Lcom/sec/common/actionbar/m;)Lcom/sec/common/actionbar/m;
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->a:Lcom/sec/common/actionbar/m;

    return-object p1
.end method

.method private a()V
    .locals 4

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/common/e;->CommonTheme:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->c:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 175
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->b:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->b:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 179
    :cond_0
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 180
    if-eq v1, p0, :cond_1

    .line 181
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->c:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/common/actionbar/TabContainerView$TabView;->addView(Landroid/view/View;)V

    .line 202
    :cond_1
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 203
    return-void

    .line 185
    :cond_2
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->b:Landroid/widget/TextView;

    if-nez v1, :cond_3

    .line 186
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->b:Landroid/widget/TextView;

    .line 189
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 190
    iget-object v2, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 192
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/sec/common/actionbar/TabContainerView$TabView;->addView(Landroid/view/View;)V

    .line 195
    :cond_3
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->c:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 196
    iget-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->c:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/common/actionbar/TabContainerView$TabView;->removeView(Landroid/view/View;)V

    .line 198
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->c:Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method public setCustomView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->c:Landroid/view/View;

    .line 226
    invoke-direct {p0}, Lcom/sec/common/actionbar/TabContainerView$TabView;->a()V

    .line 227
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/common/actionbar/TabContainerView$TabView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    :cond_0
    return-void
.end method
