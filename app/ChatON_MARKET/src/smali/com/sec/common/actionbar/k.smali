.class Lcom/sec/common/actionbar/k;
.super Lcom/sec/common/actionbar/a;
.source "ActionBarImpl.java"

# interfaces
.implements Lcom/sec/common/actionbar/w;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Landroid/view/View;

.field private c:Lcom/sec/common/actionbar/ActionBarView;

.field private d:I

.field private e:Lcom/sec/common/actionbar/TabContainerView;

.field private f:Lcom/sec/common/actionbar/m;

.field private g:Lcom/sec/common/actionbar/d;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/common/actionbar/a;-><init>()V

    .line 37
    new-instance v0, Lcom/sec/common/actionbar/l;

    invoke-direct {v0, p0}, Lcom/sec/common/actionbar/l;-><init>(Lcom/sec/common/actionbar/k;)V

    iput-object v0, p0, Lcom/sec/common/actionbar/k;->g:Lcom/sec/common/actionbar/d;

    .line 55
    iput-object p1, p0, Lcom/sec/common/actionbar/k;->a:Landroid/app/Activity;

    .line 57
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->a:Landroid/app/Activity;

    sget v1, Lcom/sec/common/c;->actionbar_layout:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/actionbar/k;->b:Landroid/view/View;

    .line 58
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->a:Landroid/app/Activity;

    sget v1, Lcom/sec/common/c;->actionbar_view:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarView;

    iput-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    .line 59
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->a:Landroid/app/Activity;

    sget v1, Lcom/sec/common/c;->actionbar_tab_container_view:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/TabContainerView;

    iput-object v0, p0, Lcom/sec/common/actionbar/k;->e:Lcom/sec/common/actionbar/TabContainerView;

    .line 60
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->e:Lcom/sec/common/actionbar/TabContainerView;

    invoke-virtual {v0, p0}, Lcom/sec/common/actionbar/TabContainerView;->a(Lcom/sec/common/actionbar/w;)V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/common/actionbar/k;->f:Lcom/sec/common/actionbar/m;

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/common/actionbar/k;->a(I)V

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/sec/common/actionbar/k;)Lcom/sec/common/actionbar/d;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->g:Lcom/sec/common/actionbar/d;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/common/actionbar/k;->d:I

    return v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 112
    iput p1, p0, Lcom/sec/common/actionbar/k;->d:I

    .line 113
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/ActionBarView;->b(I)V

    .line 115
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->e:Lcom/sec/common/actionbar/TabContainerView;

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/TabContainerView;->setVisibility(I)V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    if-nez p1, :cond_2

    .line 119
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->e:Lcom/sec/common/actionbar/TabContainerView;

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/TabContainerView;->setVisibility(I)V

    goto :goto_0

    .line 121
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->e:Lcom/sec/common/actionbar/TabContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/TabContainerView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(II)V
    .locals 4

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarView;->a()I

    move-result v0

    .line 222
    iget-object v1, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/sec/common/actionbar/ActionBarView;->a(I)V

    .line 223
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 81
    if-nez v0, :cond_0

    .line 82
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 84
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/ActionBarView;->a(Landroid/view/View;)V

    .line 88
    return-void
.end method

.method public a(Landroid/widget/SpinnerAdapter;Lcom/sec/common/actionbar/b;)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/ActionBarView;->a(Landroid/widget/SpinnerAdapter;)V

    .line 253
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0, p2}, Lcom/sec/common/actionbar/ActionBarView;->a(Lcom/sec/common/actionbar/b;)V

    .line 254
    return-void
.end method

.method public a(Lcom/sec/common/actionbar/c;)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->e:Lcom/sec/common/actionbar/TabContainerView;

    invoke-virtual {p1}, Lcom/sec/common/actionbar/c;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/TabContainerView;->b(I)V

    .line 159
    return-void
.end method

.method public a(Lcom/sec/common/actionbar/c;Z)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->e:Lcom/sec/common/actionbar/TabContainerView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/TabContainerView;->a(Lcom/sec/common/actionbar/c;)V

    .line 144
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/ActionBarView;->a(Ljava/lang/CharSequence;)V

    .line 98
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 227
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/common/actionbar/k;->a(II)V

    .line 228
    return-void

    .line 227
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lcom/sec/common/actionbar/c;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/sec/common/actionbar/m;

    invoke-direct {v0, p0}, Lcom/sec/common/actionbar/m;-><init>(Lcom/sec/common/actionbar/k;)V

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/common/actionbar/k;->a(Ljava/lang/CharSequence;)V

    .line 93
    return-void
.end method

.method public b(Lcom/sec/common/actionbar/c;)V
    .locals 3

    .prologue
    .line 258
    if-nez p1, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    const/4 v1, 0x0

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->a:Landroid/app/Activity;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->disallowAddToBackStack()Landroid/support/v4/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 273
    :goto_1
    :try_start_1
    check-cast p1, Lcom/sec/common/actionbar/m;

    .line 275
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->f:Lcom/sec/common/actionbar/m;

    if-ne v0, p1, :cond_2

    .line 276
    invoke-virtual {p1}, Lcom/sec/common/actionbar/m;->d()Lcom/sec/common/actionbar/d;

    move-result-object v0

    invoke-interface {v0, p1, v1}, Lcom/sec/common/actionbar/d;->c(Lcom/sec/common/actionbar/c;Landroid/support/v4/app/FragmentTransaction;)V

    .line 286
    :goto_2
    iput-object p1, p0, Lcom/sec/common/actionbar/k;->f:Lcom/sec/common/actionbar/m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 279
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->f:Lcom/sec/common/actionbar/m;

    if-eqz v0, :cond_3

    .line 280
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->f:Lcom/sec/common/actionbar/m;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/m;->d()Lcom/sec/common/actionbar/d;

    move-result-object v0

    invoke-interface {v0, p1, v1}, Lcom/sec/common/actionbar/d;->b(Lcom/sec/common/actionbar/c;Landroid/support/v4/app/FragmentTransaction;)V

    .line 283
    :cond_3
    invoke-virtual {p1}, Lcom/sec/common/actionbar/m;->d()Lcom/sec/common/actionbar/d;

    move-result-object v0

    invoke-interface {v0, p1, v1}, Lcom/sec/common/actionbar/d;->a(Lcom/sec/common/actionbar/c;Landroid/support/v4/app/FragmentTransaction;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 288
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 289
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_4
    throw v0

    .line 268
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/ActionBarView;->b(Ljava/lang/CharSequence;)V

    .line 108
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 242
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/common/actionbar/k;->a(II)V

    .line 243
    return-void

    .line 242
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->e:Lcom/sec/common/actionbar/TabContainerView;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/TabContainerView;->a()I

    move-result v0

    return v0
.end method

.method public c(I)Lcom/sec/common/actionbar/c;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->e:Lcom/sec/common/actionbar/TabContainerView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/TabContainerView;->c(I)Lcom/sec/common/actionbar/c;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 232
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/common/actionbar/k;->a(II)V

    .line 233
    return-void

    .line 232
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 185
    iget v0, p0, Lcom/sec/common/actionbar/k;->d:I

    packed-switch v0, :pswitch_data_0

    .line 195
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setSelectedNavigationIndex not valid for current navigation mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :pswitch_0
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/ActionBarView;->c(I)V

    .line 197
    :goto_0
    return-void

    .line 191
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/common/actionbar/k;->c(I)Lcom/sec/common/actionbar/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/common/actionbar/k;->a(Lcom/sec/common/actionbar/c;)V

    goto :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/ActionBarView;->a(Z)V

    .line 248
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 207
    return-void
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/ActionBarView;->a(I)V

    .line 217
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 212
    return-void
.end method

.method public g()Landroid/view/View;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/common/actionbar/k;->c:Lcom/sec/common/actionbar/ActionBarView;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarView;->b()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
