.class public Lcom/sec/common/actionbar/ActionBarFragmentActivity;
.super Landroid/support/v4/app/ActionBarHookActivity;
.source "ActionBarFragmentActivity.java"

# interfaces
.implements Lcom/sec/common/actionbar/s;


# instance fields
.field private final a:Lcom/sec/common/actionbar/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/ActionBarHookActivity;-><init>()V

    .line 22
    invoke-static {p0}, Lcom/sec/common/actionbar/e;->a(Landroid/app/Activity;)Lcom/sec/common/actionbar/e;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    return-void
.end method


# virtual methods
.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-super {p0}, Landroid/support/v4/app/ActionBarHookActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public k()Lcom/sec/common/actionbar/a;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/e;->b()Lcom/sec/common/actionbar/a;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarHookActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/os/Bundle;)V

    .line 29
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 46
    instance-of v0, p1, Lcom/sec/common/actionbar/t;

    if-eqz v0, :cond_0

    .line 48
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarHookActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    or-int/2addr v0, v2

    .line 49
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 60
    :goto_0
    return v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    .line 58
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarHookActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    or-int/2addr v0, v2

    .line 59
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 60
    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 89
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarHookActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 90
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/MenuItem;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 91
    return v0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->b(Landroid/os/Bundle;)V

    .line 35
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarHookActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 36
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 75
    const/4 v0, 0x0

    .line 76
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarHookActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 77
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v1, p1}, Lcom/sec/common/actionbar/e;->b(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 78
    return v0
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return v0
.end method

.method public onSupportPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ActionBarHookActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 122
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/actionbar/e;->a(Ljava/lang/CharSequence;I)V

    .line 123
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(I)V

    .line 103
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/e;->a(Landroid/view/View;)V

    .line 109
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/actionbar/e;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 115
    return-void
.end method

.method public supportInvalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarFragmentActivity;->a:Lcom/sec/common/actionbar/e;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/e;->a()V

    .line 66
    return-void
.end method
