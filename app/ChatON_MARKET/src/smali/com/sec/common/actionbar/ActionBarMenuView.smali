.class public Lcom/sec/common/actionbar/ActionBarMenuView;
.super Lcom/sec/common/widget/HoneycombLinearLayout;
.source "ActionBarMenuView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:I

.field private b:Lcom/sec/common/actionbar/o;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/sec/common/widget/HoneycombLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 35
    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/common/actionbar/ActionBarMenuView;->a:I

    .line 36
    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 118
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/sec/common/actionbar/ActionBarMenuView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 119
    invoke-virtual {p0, v0}, Lcom/sec/common/actionbar/ActionBarMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    :cond_0
    invoke-virtual {p0}, Lcom/sec/common/actionbar/ActionBarMenuView;->removeAllViews()V

    .line 123
    return-void
.end method

.method a(Landroid/view/MenuItem;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 74
    .line 76
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/common/actionbar/ActionBarMenuView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/common/b;->actionButtonStyle:I

    invoke-direct {v0, v1, v6, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 79
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 99
    :goto_0
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 100
    iget v1, p0, Lcom/sec/common/actionbar/ActionBarMenuView;->a:I

    iget v2, p0, Lcom/sec/common/actionbar/ActionBarMenuView;->a:I

    invoke-virtual {v0, v1, v5, v2, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 102
    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 103
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    invoke-interface {p1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 107
    invoke-interface {p1}, Landroid/view/MenuItem;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 114
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/common/actionbar/ActionBarMenuView;->addView(Landroid/view/View;)V

    .line 115
    return-void

    .line 84
    :cond_0
    invoke-virtual {p0}, Lcom/sec/common/actionbar/ActionBarMenuView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/common/e;->CommonTheme:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 86
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 87
    const/16 v2, 0xa

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 89
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 91
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/common/actionbar/ActionBarMenuView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/sec/common/b;->actionButtonStyle:I

    invoke-direct {v0, v3, v6, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 92
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/common/actionbar/ActionBarMenuView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 111
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method protected a(I)Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 44
    invoke-virtual {p0}, Lcom/sec/common/actionbar/ActionBarMenuView;->getChildCount()I

    move-result v5

    .line 46
    if-eqz p1, :cond_0

    if-ne p1, v5, :cond_1

    :cond_0
    move v0, v2

    .line 69
    :goto_0
    return v0

    .line 53
    :cond_1
    if-le p1, v1, :cond_5

    .line 54
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/common/actionbar/ActionBarMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    move-object v4, v0

    .line 57
    :goto_1
    if-eq p1, v5, :cond_4

    .line 58
    invoke-virtual {p0, p1}, Lcom/sec/common/actionbar/ActionBarMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 61
    :goto_2
    if-eqz v4, :cond_2

    invoke-interface {v4}, Landroid/view/MenuItem;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    .line 62
    goto :goto_0

    .line 65
    :cond_2
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    .line 66
    goto :goto_0

    :cond_3
    move v0, v2

    .line 69
    goto :goto_0

    :cond_4
    move-object v0, v3

    goto :goto_2

    :cond_5
    move-object v4, v3

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarMenuView;->b:Lcom/sec/common/actionbar/o;

    if-eqz v0, :cond_0

    .line 128
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarMenuView;->b:Lcom/sec/common/actionbar/o;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    invoke-interface {v1, p1, v0}, Lcom/sec/common/actionbar/o;->a(Landroid/view/View;Landroid/view/MenuItem;)V

    .line 130
    :cond_0
    return-void
.end method

.method public setOnMenuItemClickListener(Lcom/sec/common/actionbar/o;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/common/actionbar/ActionBarMenuView;->b:Lcom/sec/common/actionbar/o;

    .line 40
    return-void
.end method
