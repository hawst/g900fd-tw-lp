.class Lcom/sec/common/actionbar/r;
.super Lcom/sec/common/actionbar/c;
.source "ActionBarWrapper.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# instance fields
.field final synthetic a:Lcom/sec/common/actionbar/p;

.field private b:Landroid/app/ActionBar$Tab;

.field private c:Lcom/sec/common/actionbar/d;


# direct methods
.method public constructor <init>(Lcom/sec/common/actionbar/p;Landroid/app/ActionBar$Tab;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/common/actionbar/r;->a:Lcom/sec/common/actionbar/p;

    invoke-direct {p0}, Lcom/sec/common/actionbar/c;-><init>()V

    .line 214
    iput-object p2, p0, Lcom/sec/common/actionbar/r;->b:Landroid/app/ActionBar$Tab;

    .line 215
    return-void
.end method

.method static synthetic a(Lcom/sec/common/actionbar/r;)Landroid/app/ActionBar$Tab;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->b:Landroid/app/ActionBar$Tab;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->b:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->getCustomView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;)Lcom/sec/common/actionbar/c;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->b:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar$Tab;->setCustomView(Landroid/view/View;)Landroid/app/ActionBar$Tab;

    .line 233
    return-object p0
.end method

.method public a(Lcom/sec/common/actionbar/d;)Lcom/sec/common/actionbar/c;
    .locals 1

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/common/actionbar/r;->c:Lcom/sec/common/actionbar/d;

    .line 293
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->b:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 295
    return-object p0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->b:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v0

    return v0
.end method

.method public c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->b:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->c:Lcom/sec/common/actionbar/d;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->a:Lcom/sec/common/actionbar/p;

    invoke-static {v0}, Lcom/sec/common/actionbar/p;->a(Lcom/sec/common/actionbar/p;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->a:Lcom/sec/common/actionbar/p;

    invoke-static {v0}, Lcom/sec/common/actionbar/p;->a(Lcom/sec/common/actionbar/p;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->disallowAddToBackStack()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lcom/sec/common/actionbar/r;->c:Lcom/sec/common/actionbar/d;

    invoke-interface {v1, p0, v0}, Lcom/sec/common/actionbar/d;->c(Lcom/sec/common/actionbar/c;Landroid/support/v4/app/FragmentTransaction;)V

    .line 351
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 352
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 356
    :cond_0
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->c:Lcom/sec/common/actionbar/d;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->a:Lcom/sec/common/actionbar/p;

    invoke-static {v0}, Lcom/sec/common/actionbar/p;->a(Lcom/sec/common/actionbar/p;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->a:Lcom/sec/common/actionbar/p;

    invoke-static {v0}, Lcom/sec/common/actionbar/p;->a(Lcom/sec/common/actionbar/p;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->disallowAddToBackStack()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 309
    iget-object v1, p0, Lcom/sec/common/actionbar/r;->c:Lcom/sec/common/actionbar/d;

    invoke-interface {v1, p0, v0}, Lcom/sec/common/actionbar/d;->a(Lcom/sec/common/actionbar/c;Landroid/support/v4/app/FragmentTransaction;)V

    .line 311
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 312
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 316
    :cond_0
    return-void
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->c:Lcom/sec/common/actionbar/d;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->a:Lcom/sec/common/actionbar/p;

    invoke-static {v0}, Lcom/sec/common/actionbar/p;->a(Lcom/sec/common/actionbar/p;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/sec/common/actionbar/r;->a:Lcom/sec/common/actionbar/p;

    invoke-static {v0}, Lcom/sec/common/actionbar/p;->a(Lcom/sec/common/actionbar/p;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->disallowAddToBackStack()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 329
    iget-object v1, p0, Lcom/sec/common/actionbar/r;->c:Lcom/sec/common/actionbar/d;

    invoke-interface {v1, p0, v0}, Lcom/sec/common/actionbar/d;->b(Lcom/sec/common/actionbar/c;Landroid/support/v4/app/FragmentTransaction;)V

    .line 331
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 332
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 336
    :cond_0
    return-void
.end method
