.class Lcom/sec/common/actionbar/ActionBarView;
.super Landroid/widget/LinearLayout;
.source "ActionBarView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/common/widget/g;


# instance fields
.field private final a:I

.field private b:I

.field private c:I

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/view/ViewGroup;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Lcom/sec/common/widget/IcsSpinner;

.field private l:Landroid/widget/SpinnerAdapter;

.field private m:Landroid/view/View;

.field private n:Lcom/sec/common/actionbar/ActionBarMenuView;

.field private o:Landroid/view/View$OnClickListener;

.field private p:Lcom/sec/common/actionbar/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 110
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    iput v5, p0, Lcom/sec/common/actionbar/ActionBarView;->a:I

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/common/actionbar/ActionBarView;->b:I

    .line 115
    sget-object v0, Lcom/sec/common/e;->ActionBarCompat:[I

    sget v1, Lcom/sec/common/b;->actionBarStyle:I

    invoke-virtual {p1, p2, v0, v1, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 117
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->d:Landroid/graphics/drawable/Drawable;

    .line 120
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 122
    sget v2, Lcom/sec/common/d;->actionbar_compat_view:I

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 124
    sget v0, Lcom/sec/common/c;->actionbar_home_navigation_layout:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->e:Landroid/view/ViewGroup;

    .line 126
    sget v0, Lcom/sec/common/c;->actionbar_homeLayout:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->f:Landroid/view/View;

    .line 127
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->f:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    sget v0, Lcom/sec/common/c;->actionbar_up:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->g:Landroid/widget/ImageView;

    .line 130
    sget v0, Lcom/sec/common/c;->actionbar_logo:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->h:Landroid/widget/ImageView;

    .line 131
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->h:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/common/actionbar/ActionBarView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 133
    sget v0, Lcom/sec/common/c;->actionbar_compat_title:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->i:Landroid/widget/TextView;

    .line 134
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/common/actionbar/ActionBarView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 136
    sget v0, Lcom/sec/common/c;->actionbar_compat_subtitle:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->j:Landroid/widget/TextView;

    .line 137
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/common/actionbar/ActionBarView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 139
    sget v0, Lcom/sec/common/c;->actionbar_menu_layout:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/common/actionbar/ActionBarMenuView;

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->n:Lcom/sec/common/actionbar/ActionBarMenuView;

    .line 141
    const/4 v0, 0x3

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/common/actionbar/ActionBarView;->a(I)V

    .line 142
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 143
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 185
    if-eqz p1, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->g:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 191
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->g:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 194
    if-eqz p1, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->j:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 209
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 212
    if-eqz p1, :cond_0

    .line 213
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->h:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->h:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->m:Landroid/view/View;

    if-nez v0, :cond_0

    .line 239
    :goto_0
    return-void

    .line 234
    :cond_0
    if-eqz p1, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/sec/common/actionbar/ActionBarView;->b:I

    return v0
.end method

.method public a(I)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 150
    iget v0, p0, Lcom/sec/common/actionbar/ActionBarView;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    iget v0, p0, Lcom/sec/common/actionbar/ActionBarView;->b:I

    .line 151
    :goto_0
    iput p1, p0, Lcom/sec/common/actionbar/ActionBarView;->b:I

    .line 153
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    .line 154
    and-int/lit8 v1, p1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_7

    move v1, v2

    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/common/actionbar/ActionBarView;->b(Z)V

    .line 157
    :cond_0
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_1

    .line 158
    and-int/lit8 v1, p1, 0x1

    if-ne v1, v2, :cond_1

    .line 163
    :cond_1
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_2

    .line 164
    and-int/lit8 v1, p1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_8

    move v1, v2

    :goto_2
    invoke-direct {p0, v1}, Lcom/sec/common/actionbar/ActionBarView;->d(Z)V

    .line 167
    :cond_2
    and-int/lit8 v1, p1, 0x4

    if-nez v1, :cond_3

    and-int/lit8 v1, p1, 0x2

    if-eqz v1, :cond_9

    .line 169
    :cond_3
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->f:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 175
    :goto_3
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_4

    .line 176
    and-int/lit8 v1, p1, 0x8

    if-ne v1, v5, :cond_a

    move v1, v2

    :goto_4
    invoke-direct {p0, v1}, Lcom/sec/common/actionbar/ActionBarView;->c(Z)V

    .line 179
    :cond_4
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 180
    and-int/lit8 v0, p1, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_b

    :goto_5
    invoke-direct {p0, v2}, Lcom/sec/common/actionbar/ActionBarView;->e(Z)V

    .line 182
    :cond_5
    return-void

    .line 150
    :cond_6
    iget v0, p0, Lcom/sec/common/actionbar/ActionBarView;->b:I

    xor-int/2addr v0, p1

    goto :goto_0

    :cond_7
    move v1, v3

    .line 154
    goto :goto_1

    :cond_8
    move v1, v3

    .line 164
    goto :goto_2

    .line 172
    :cond_9
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->f:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_a
    move v1, v3

    .line 176
    goto :goto_4

    :cond_b
    move v2, v3

    .line 180
    goto :goto_5
.end method

.method public a(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->n:Lcom/sec/common/actionbar/ActionBarMenuView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/ActionBarMenuView;->a(Landroid/view/MenuItem;)V

    .line 284
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/common/actionbar/ActionBarView;->o:Landroid/view/View$OnClickListener;

    .line 276
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 246
    iget v0, p0, Lcom/sec/common/actionbar/ActionBarView;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    .line 248
    :goto_0
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->m:Landroid/view/View;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 249
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->e:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/sec/common/actionbar/ActionBarView;->m:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 252
    :cond_0
    iput-object p1, p0, Lcom/sec/common/actionbar/ActionBarView;->m:Landroid/view/View;

    .line 254
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->m:Landroid/view/View;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 255
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 257
    :cond_1
    return-void

    .line 246
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/widget/SpinnerAdapter;)V
    .locals 1

    .prologue
    .line 317
    iput-object p1, p0, Lcom/sec/common/actionbar/ActionBarView;->l:Landroid/widget/SpinnerAdapter;

    .line 319
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0, p1}, Lcom/sec/common/widget/IcsSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 322
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/common/actionbar/b;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/sec/common/actionbar/ActionBarView;->p:Lcom/sec/common/actionbar/b;

    .line 338
    return-void
.end method

.method public a(Lcom/sec/common/actionbar/o;)V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->n:Lcom/sec/common/actionbar/ActionBarMenuView;

    invoke-virtual {v0, p1}, Lcom/sec/common/actionbar/ActionBarMenuView;->setOnMenuItemClickListener(Lcom/sec/common/actionbar/o;)V

    .line 280
    return-void
.end method

.method public a(Lcom/sec/common/widget/IcsAdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/common/widget/IcsAdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 363
    return-void
.end method

.method public a(Lcom/sec/common/widget/IcsAdapterView;Landroid/view/View;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/common/widget/IcsAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->p:Lcom/sec/common/actionbar/b;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->p:Lcom/sec/common/actionbar/b;

    invoke-interface {v0, p3, p4, p5}, Lcom/sec/common/actionbar/b;->a(IJ)Z

    .line 358
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 221
    if-eqz p1, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->f:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 227
    :goto_0
    return-void

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->m:Landroid/view/View;

    return-object v0
.end method

.method public b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 291
    iput p1, p0, Lcom/sec/common/actionbar/ActionBarView;->c:I

    .line 293
    iget v0, p0, Lcom/sec/common/actionbar/ActionBarView;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 294
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    if-nez v0, :cond_0

    .line 295
    new-instance v0, Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {p0}, Lcom/sec/common/actionbar/ActionBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/common/b;->actionDropDownStyle:I

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/common/widget/IcsSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    .line 297
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 299
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 301
    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v1, v0}, Lcom/sec/common/widget/IcsSpinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 302
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->l:Landroid/widget/SpinnerAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/common/widget/IcsSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0, p0}, Lcom/sec/common/widget/IcsSpinner;->setOnItemSelectedListener(Lcom/sec/common/widget/g;)V

    .line 306
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 314
    :cond_1
    :goto_0
    return-void

    .line 308
    :cond_2
    iget v0, p0, Lcom/sec/common/actionbar/ActionBarView;->c:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/sec/common/actionbar/ActionBarView;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 309
    :cond_3
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    if-eqz v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0, v3}, Lcom/sec/common/widget/IcsSpinner;->setOnItemSelectedListener(Lcom/sec/common/widget/g;)V

    .line 311
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 264
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->j:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    :goto_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    return-void

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->j:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->n:Lcom/sec/common/actionbar/ActionBarMenuView;

    invoke-virtual {v0}, Lcom/sec/common/actionbar/ActionBarMenuView;->a()V

    .line 288
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->k:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0, p1}, Lcom/sec/common/widget/IcsSpinner;->setSelection(I)V

    .line 330
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->f:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->o:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/sec/common/actionbar/ActionBarView;->o:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 351
    :cond_0
    return-void
.end method
