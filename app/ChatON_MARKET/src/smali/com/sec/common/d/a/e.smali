.class public Lcom/sec/common/d/a/e;
.super Ljava/lang/Object;
.source "HttpResponseEntry.java"


# instance fields
.field private a:Lcom/sec/common/d/a/c;

.field private b:Z

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/sec/common/d/a/c;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/sec/common/d/a/e;->a:Lcom/sec/common/d/a/c;

    .line 28
    iput-boolean v1, p0, Lcom/sec/common/d/a/e;->b:Z

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/common/d/a/e;->c:Ljava/util/Map;

    .line 32
    iput v1, p0, Lcom/sec/common/d/a/e;->d:I

    .line 33
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/common/d/a/e;->d:I

    .line 53
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/common/d/a/e;->e:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/common/d/a/e;->b:Z

    .line 69
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/common/d/a/e;->f:Ljava/lang/Object;

    .line 77
    return-void
.end method

.method public k()Lcom/sec/common/d/a/c;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/common/d/a/e;->a:Lcom/sec/common/d/a/c;

    return-object v0
.end method

.method public l()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/common/d/a/e;->c:Ljava/util/Map;

    return-object v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/common/d/a/e;->d:I

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/common/d/a/e;->b:Z

    return v0
.end method

.method public o()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/common/d/a/e;->f:Ljava/lang/Object;

    return-object v0
.end method
