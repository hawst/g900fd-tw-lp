.class public Lcom/sec/common/d/a/c;
.super Ljava/lang/Object;
.source "HttpRequestEntry.java"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/sec/common/d/a/d;

.field private m:Lcom/sec/common/d/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/16 v0, 0x7530

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v0, p0, Lcom/sec/common/d/a/c;->a:I

    .line 18
    iput v0, p0, Lcom/sec/common/d/a/c;->b:I

    .line 19
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/common/d/a/c;->c:I

    .line 34
    iget v0, p0, Lcom/sec/common/d/a/c;->a:I

    iput v0, p0, Lcom/sec/common/d/a/c;->d:I

    .line 35
    iget v0, p0, Lcom/sec/common/d/a/c;->b:I

    iput v0, p0, Lcom/sec/common/d/a/c;->e:I

    .line 36
    iget v0, p0, Lcom/sec/common/d/a/c;->c:I

    iput v0, p0, Lcom/sec/common/d/a/c;->f:I

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/common/d/a/c;->j:Ljava/util/Map;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/common/d/a/c;->k:Ljava/util/Map;

    .line 41
    sget-object v0, Lcom/sec/common/d/a/d;->a:Lcom/sec/common/d/a/d;

    iput-object v0, p0, Lcom/sec/common/d/a/c;->l:Lcom/sec/common/d/a/d;

    .line 42
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/common/d/a/a;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/common/d/a/c;->m:Lcom/sec/common/d/a/a;

    .line 134
    return-void
.end method

.method public a(Lcom/sec/common/d/a/d;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/common/d/a/c;->l:Lcom/sec/common/d/a/d;

    .line 86
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/common/d/a/c;->j:Ljava/util/Map;

    .line 78
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/common/d/a/c;->h:Z

    .line 58
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 93
    iput p1, p0, Lcom/sec/common/d/a/c;->d:I

    .line 94
    return-void
.end method

.method public b(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/common/d/a/c;->k:Ljava/util/Map;

    .line 126
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/common/d/a/c;->i:Z

    .line 66
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/common/d/a/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 101
    iput p1, p0, Lcom/sec/common/d/a/c;->e:I

    .line 102
    return-void
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/common/d/a/c;->h:Z

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/sec/common/d/a/c;->i:Z

    return v0
.end method

.method public j()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/common/d/a/c;->j:Ljava/util/Map;

    return-object v0
.end method

.method public k()Lcom/sec/common/d/a/d;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/common/d/a/c;->l:Lcom/sec/common/d/a/d;

    return-object v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/common/d/a/c;->d:I

    return v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/common/d/a/c;->e:I

    return v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/common/d/a/c;->f:I

    return v0
.end method

.method public o()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/common/d/a/c;->k:Ljava/util/Map;

    return-object v0
.end method

.method public p()Lcom/sec/common/d/a/a;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/common/d/a/c;->m:Lcom/sec/common/d/a/a;

    return-object v0
.end method
