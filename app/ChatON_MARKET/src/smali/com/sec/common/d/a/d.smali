.class public final enum Lcom/sec/common/d/a/d;
.super Ljava/lang/Enum;
.source "HttpRequestEntry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/common/d/a/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/common/d/a/d;

.field public static final enum b:Lcom/sec/common/d/a/d;

.field public static final enum c:Lcom/sec/common/d/a/d;

.field public static final enum d:Lcom/sec/common/d/a/d;

.field private static final synthetic e:[Lcom/sec/common/d/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 137
    new-instance v0, Lcom/sec/common/d/a/d;

    const-string v1, "GET"

    invoke-direct {v0, v1, v2}, Lcom/sec/common/d/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/common/d/a/d;->a:Lcom/sec/common/d/a/d;

    .line 138
    new-instance v0, Lcom/sec/common/d/a/d;

    const-string v1, "POST"

    invoke-direct {v0, v1, v3}, Lcom/sec/common/d/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/common/d/a/d;->b:Lcom/sec/common/d/a/d;

    .line 139
    new-instance v0, Lcom/sec/common/d/a/d;

    const-string v1, "PUT"

    invoke-direct {v0, v1, v4}, Lcom/sec/common/d/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/common/d/a/d;->c:Lcom/sec/common/d/a/d;

    .line 140
    new-instance v0, Lcom/sec/common/d/a/d;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v5}, Lcom/sec/common/d/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/common/d/a/d;->d:Lcom/sec/common/d/a/d;

    .line 136
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/common/d/a/d;

    sget-object v1, Lcom/sec/common/d/a/d;->a:Lcom/sec/common/d/a/d;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/common/d/a/d;->b:Lcom/sec/common/d/a/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/common/d/a/d;->c:Lcom/sec/common/d/a/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/common/d/a/d;->d:Lcom/sec/common/d/a/d;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/common/d/a/d;->e:[Lcom/sec/common/d/a/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/common/d/a/d;
    .locals 1

    .prologue
    .line 136
    const-class v0, Lcom/sec/common/d/a/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/common/d/a/d;

    return-object v0
.end method

.method public static values()[Lcom/sec/common/d/a/d;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/sec/common/d/a/d;->e:[Lcom/sec/common/d/a/d;

    invoke-virtual {v0}, [Lcom/sec/common/d/a/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/common/d/a/d;

    return-object v0
.end method
