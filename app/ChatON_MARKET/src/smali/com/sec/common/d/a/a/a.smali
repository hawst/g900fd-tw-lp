.class public abstract Lcom/sec/common/d/a/a/a;
.super Lcom/sec/common/d/a/b;
.source "AbstractHttpTextTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ReqType:",
        "Lcom/sec/common/d/a/a/b;",
        "ResType:",
        "Lcom/sec/common/d/a/a/c;",
        ">",
        "Lcom/sec/common/d/a/b",
        "<",
        "Ljava/lang/String;",
        "TReqType;TResType;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/sec/common/d/a/a/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TReqType;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/common/d/a/b;-><init>(Lcom/sec/common/d/a/c;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected a(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V
    .locals 4

    .prologue
    .line 37
    if-eqz p2, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/sec/common/d/a/a/a;->j()Lcom/sec/common/d/a/c;

    move-result-object v0

    check-cast v0, Lcom/sec/common/d/a/a/b;

    .line 39
    invoke-virtual {p0}, Lcom/sec/common/d/a/a/a;->k()Lcom/sec/common/d/a/e;

    move-result-object v1

    check-cast v1, Lcom/sec/common/d/a/a/c;

    .line 41
    invoke-virtual {v0}, Lcom/sec/common/d/a/a/b;->q()Lcom/sec/common/d/a/a;

    move-result-object v0

    .line 43
    invoke-virtual {v1}, Lcom/sec/common/d/a/a/c;->m()I

    move-result v2

    const/16 v3, 0xcc

    if-eq v2, v3, :cond_0

    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {v0, p2}, Lcom/sec/common/d/a/a;->parse(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/common/d/a/a/c;->c(Ljava/lang/Object;)V

    .line 47
    :cond_0
    return-void
.end method

.method protected a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V
    .locals 6

    .prologue
    const/16 v1, 0x100

    .line 51
    invoke-virtual {p0}, Lcom/sec/common/d/a/a/a;->l()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 52
    const/4 v2, 0x0

    .line 54
    invoke-virtual {p0}, Lcom/sec/common/d/a/a/a;->l()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    move v0, v1

    .line 57
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 58
    new-instance v0, Ljava/lang/InterruptedException;

    const-string v1, "writeToOutputStream is interrupted."

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_1
    add-int v4, v2, v0

    array-length v5, v3

    if-le v4, v5, :cond_2

    .line 62
    array-length v0, v3

    sub-int/2addr v0, v2

    .line 65
    :cond_2
    invoke-virtual {p2, v3, v2, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 66
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    .line 68
    add-int/2addr v2, v0

    .line 70
    if-eq v0, v1, :cond_0

    .line 75
    :cond_3
    return-void
.end method
