.class public abstract Lcom/sec/common/d/a/b;
.super Ljava/lang/Object;
.source "AbstractHttpTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ContentType:",
        "Ljava/lang/Object;",
        "ReqType:",
        "Lcom/sec/common/d/a/c;",
        "ResType:",
        "Lcom/sec/common/d/a/e;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<TResType;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/net/HttpURLConnection;

.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TContentType;"
        }
    .end annotation
.end field

.field private c:Lcom/sec/common/d/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TReqType;"
        }
    .end annotation
.end field

.field protected d:Ljava/lang/String;

.field private e:Lcom/sec/common/d/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TResType;"
        }
    .end annotation
.end field

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/sec/common/d/a/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TReqType;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    .line 52
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/d/a/b;->d:Ljava/lang/String;

    .line 53
    return-void
.end method

.method private a()Ljava/net/HttpURLConnection;
    .locals 2

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/sec/common/d/a/b;->o()Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 280
    invoke-virtual {p0, v0}, Lcom/sec/common/d/a/b;->a(Ljava/net/HttpURLConnection;)V

    .line 282
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->b:Z

    if-eqz v1, :cond_0

    .line 283
    iget-object v1, p0, Lcom/sec/common/d/a/b;->d:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/common/d/a/f;->a(Ljava/lang/String;Ljava/net/HttpURLConnection;)V

    .line 286
    :cond_0
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 288
    return-object v0
.end method

.method private b(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 318
    const-string v0, "Content-Encoding"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getRequestProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 320
    const-string v1, "gzip"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    new-instance v0, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v0, p2}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object p2, v0

    .line 324
    :cond_0
    return-object p2
.end method

.method private c(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 300
    const-string v0, "Content-Encoding"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 302
    const-string v1, "gzip"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, p2}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p2, v0

    .line 306
    :cond_0
    return-object p2
.end method


# virtual methods
.method protected abstract a(Lcom/sec/common/d/a/c;)Lcom/sec/common/d/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TReqType;)TResType;"
        }
    .end annotation
.end method

.method protected abstract a(Lcom/sec/common/d/a/e;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResType;)V"
        }
    .end annotation
.end method

.method protected a(Ljava/net/HttpURLConnection;)V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->k()Lcom/sec/common/d/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/d/a/d;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->l()I

    move-result v0

    if-lez v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->l()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->m()I

    move-result v0

    if-lez v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->m()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 208
    const-string v0, "Accept-Encoding"

    const-string v1, "gzip"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :goto_0
    iget-object v0, p0, Lcom/sec/common/d/a/b;->b:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 217
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 219
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->k()Lcom/sec/common/d/a/d;

    move-result-object v0

    sget-object v1, Lcom/sec/common/d/a/d;->b:Lcom/sec/common/d/a/d;

    if-ne v0, v1, :cond_2

    .line 221
    const-string v0, "Content-Encoding"

    const-string v1, "gzip"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_2
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->j()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 227
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 212
    :cond_3
    const-string v0, "Accept-Encoding"

    const-string v1, "identify"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 229
    :cond_4
    return-void
.end method

.method protected abstract a(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V
.end method

.method protected abstract a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V
.end method

.method protected b(Ljava/net/HttpURLConnection;)V
    .locals 4

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/common/d/a/b;->e:Lcom/sec/common/d/a/e;

    invoke-virtual {v0}, Lcom/sec/common/d/a/e;->l()Ljava/util/Map;

    move-result-object v1

    .line 235
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 236
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 237
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 239
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 240
    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/sec/common/d/a/b;->e:Lcom/sec/common/d/a/e;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/common/d/a/e;->a(I)V

    .line 249
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v0

    .line 251
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 252
    iget-object v1, p0, Lcom/sec/common/d/a/b;->e:Lcom/sec/common/d/a/e;

    invoke-virtual {v1, v0}, Lcom/sec/common/d/a/e;->a(Ljava/lang/String;)V

    .line 254
    :cond_2
    return-void
.end method

.method protected b(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 261
    if-eqz p2, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->p()Lcom/sec/common/d/a/a;

    move-result-object v0

    .line 264
    if-eqz v0, :cond_0

    .line 265
    iget-object v1, p0, Lcom/sec/common/d/a/b;->e:Lcom/sec/common/d/a/e;

    invoke-virtual {v0, p2}, Lcom/sec/common/d/a/a;->parse(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/common/d/a/e;->b(Ljava/lang/Object;)V

    .line 269
    :cond_0
    return-void
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/sec/common/d/a/b;->n()Lcom/sec/common/d/a/e;

    move-result-object v0

    return-object v0
.end method

.method protected abstract h()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TContentType;"
        }
    .end annotation
.end method

.method public j()Lcom/sec/common/d/a/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TReqType;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    return-object v0
.end method

.method public k()Lcom/sec/common/d/a/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResType;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/common/d/a/b;->e:Lcom/sec/common/d/a/e;

    return-object v0
.end method

.method public l()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TContentType;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/common/d/a/b;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/common/d/a/b;->f:Z

    return v0
.end method

.method public n()Lcom/sec/common/d/a/e;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResType;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 80
    :try_start_0
    iget-object v1, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {p0, v1}, Lcom/sec/common/d/a/b;->a(Lcom/sec/common/d/a/c;)Lcom/sec/common/d/a/e;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/common/d/a/b;->e:Lcom/sec/common/d/a/e;

    .line 83
    invoke-virtual {p0}, Lcom/sec/common/d/a/b;->h()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/common/d/a/b;->b:Ljava/lang/Object;

    .line 85
    iget-boolean v1, p0, Lcom/sec/common/d/a/b;->f:Z

    if-nez v1, :cond_6

    .line 86
    invoke-static {}, Lcom/sec/common/util/i;->k()Z

    move-result v1

    if-nez v1, :cond_1

    .line 87
    new-instance v0, Ljava/io/IOException;

    const-string v1, "The network isn\'t available."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/sec/common/d/a/b;->e:Lcom/sec/common/d/a/e;

    invoke-virtual {p0, v1}, Lcom/sec/common/d/a/b;->a(Lcom/sec/common/d/a/e;)V

    throw v0

    :cond_1
    move v1, v0

    .line 90
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v0}, Lcom/sec/common/d/a/c;->n()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-gt v1, v0, :cond_2

    .line 93
    :try_start_2
    invoke-direct {p0}, Lcom/sec/common/d/a/b;->a()Ljava/net/HttpURLConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;
    :try_end_2
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 109
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/sec/common/d/a/b;->e:Lcom/sec/common/d/a/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/d/a/e;->a(Z)V

    .line 111
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_3

    .line 112
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-object v1, p0, Lcom/sec/common/d/a/b;->d:Ljava/lang/String;

    const-string v3, "Connection is established."

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_3
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getDoOutput()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    .line 120
    :try_start_4
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    iget-object v1, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/common/d/a/b;->b(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)Ljava/io/OutputStream;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v1

    .line 121
    :try_start_5
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, v0, v1}, Lcom/sec/common/d/a/b;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 123
    :try_start_6
    invoke-static {v1}, Lcom/sec/common/util/l;->a(Ljava/io/OutputStream;)V

    .line 128
    :cond_4
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, v0}, Lcom/sec/common/d/a/b;->b(Ljava/net/HttpURLConnection;)V

    .line 130
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_5

    .line 131
    iget-object v0, p0, Lcom/sec/common/d/a/b;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-static {v0, v1}, Lcom/sec/common/d/a/f;->b(Ljava/lang/String;Ljava/net/HttpURLConnection;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 138
    :cond_5
    :try_start_7
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0x190

    if-lt v0, v1, :cond_b

    .line 139
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    iget-object v1, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/common/d/a/b;->c(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v2

    .line 140
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, v0, v2}, Lcom/sec/common/d/a/b;->b(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 147
    :goto_1
    :try_start_8
    invoke-static {v2}, Lcom/sec/common/util/l;->a(Ljava/io/InputStream;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 151
    :cond_6
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_7

    .line 152
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 154
    :cond_7
    iget-object v0, p0, Lcom/sec/common/d/a/b;->e:Lcom/sec/common/d/a/e;

    invoke-virtual {p0, v0}, Lcom/sec/common/d/a/b;->a(Lcom/sec/common/d/a/e;)V

    .line 158
    iget-object v0, p0, Lcom/sec/common/d/a/b;->e:Lcom/sec/common/d/a/e;

    return-object v0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    :try_start_9
    iget-object v3, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v3}, Lcom/sec/common/d/a/c;->n()I

    move-result v3

    if-ne v1, v3, :cond_9

    .line 98
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->e:Z

    if-eqz v1, :cond_8

    .line 99
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-object v2, p0, Lcom/sec/common/d/a/b;->d:Ljava/lang/String;

    const-string v3, "Can\'t connect server."

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_8
    throw v0

    .line 103
    :cond_9
    sget-object v3, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v3, v3, Lcom/sec/common/c/a;->d:Z

    if-eqz v3, :cond_a

    .line 104
    sget-object v3, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-object v4, p0, Lcom/sec/common/d/a/b;->d:Ljava/lang/String;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "Retry connect to server. ["

    aput-object v7, v5, v6

    const/4 v6, 0x1

    add-int/lit8 v7, v1, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "], Reason: "

    aput-object v7, v5, v6

    const/4 v6, 0x3

    invoke-virtual {v0}, Ljava/io/InterruptedIOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v5}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/sec/common/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 123
    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, Lcom/sec/common/util/l;->a(Ljava/io/OutputStream;)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 143
    :cond_b
    :try_start_a
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    iget-object v1, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/common/d/a/b;->c(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v2

    .line 144
    iget-object v0, p0, Lcom/sec/common/d/a/b;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {p0, v0, v2}, Lcom/sec/common/d/a/b;->a(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    goto :goto_1

    .line 147
    :catchall_2
    move-exception v0

    :try_start_b
    invoke-static {v2}, Lcom/sec/common/util/l;->a(Ljava/io/InputStream;)V

    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 123
    :catchall_3
    move-exception v0

    goto :goto_2
.end method

.method protected o()Ljava/net/HttpURLConnection;
    .locals 6

    .prologue
    .line 174
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v1}, Lcom/sec/common/d/a/c;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/common/d/a/b;->c:Lcom/sec/common/d/a/c;

    invoke-virtual {v2}, Lcom/sec/common/d/a/c;->o()Ljava/util/Map;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/common/d/a/f;->a(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 178
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->b:Z

    if-eqz v1, :cond_0

    .line 179
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-object v2, p0, Lcom/sec/common/d/a/b;->d:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "Connect to "

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_0
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    return-object v0
.end method

.method protected final p()V
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/common/d/a/b;->f:Z

    .line 190
    return-void
.end method
