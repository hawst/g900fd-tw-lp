.class Lcom/sec/common/widget/s;
.super Lcom/sec/common/widget/IcsListPopupWindow;
.source "IcsSpinner.java"

# interfaces
.implements Lcom/sec/common/widget/u;


# instance fields
.field final synthetic a:Lcom/sec/common/widget/IcsSpinner;

.field private b:Ljava/lang/CharSequence;

.field private c:Landroid/widget/ListAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/common/widget/IcsSpinner;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 643
    iput-object p1, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    .line 644
    invoke-direct {p0, p2, p3, v1, p4}, Lcom/sec/common/widget/IcsListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 646
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/s;->a(Landroid/view/View;)V

    .line 647
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/s;->a(Z)V

    .line 648
    invoke-virtual {p0, v1}, Lcom/sec/common/widget/s;->a(I)V

    .line 649
    new-instance v0, Lcom/sec/common/widget/t;

    invoke-direct {v0, p0, p1}, Lcom/sec/common/widget/t;-><init>(Lcom/sec/common/widget/s;Lcom/sec/common/widget/IcsSpinner;)V

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/s;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 656
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 675
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsSpinner;->getPaddingLeft()I

    move-result v1

    .line 676
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    iget v0, v0, Lcom/sec/common/widget/IcsSpinner;->F:I

    const/4 v2, -0x2

    if-ne v0, v2, :cond_1

    .line 677
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsSpinner;->getWidth()I

    move-result v2

    .line 678
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsSpinner;->getPaddingRight()I

    move-result v3

    .line 679
    iget-object v4, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    iget-object v0, p0, Lcom/sec/common/widget/s;->c:Landroid/widget/ListAdapter;

    check-cast v0, Landroid/widget/SpinnerAdapter;

    iget-object v5, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v5}, Lcom/sec/common/widget/IcsSpinner;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lcom/sec/common/widget/IcsSpinner;->a(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v0

    sub-int/2addr v2, v1

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/s;->d(I)V

    .line 689
    :goto_0
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsSpinner;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 690
    const/4 v0, 0x0

    .line 691
    if-eqz v2, :cond_0

    .line 692
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    invoke-static {v0}, Lcom/sec/common/widget/IcsSpinner;->a(Lcom/sec/common/widget/IcsSpinner;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 693
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    invoke-static {v0}, Lcom/sec/common/widget/IcsSpinner;->a(Lcom/sec/common/widget/IcsSpinner;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    .line 695
    :cond_0
    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/s;->b(I)V

    .line 696
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/s;->e(I)V

    .line 697
    invoke-super {p0}, Lcom/sec/common/widget/IcsListPopupWindow;->a()V

    .line 698
    invoke-virtual {p0}, Lcom/sec/common/widget/s;->e()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 699
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    iget-object v1, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v1}, Lcom/sec/common/widget/IcsSpinner;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/common/widget/IcsSpinner;->setSelection(I)V

    .line 700
    return-void

    .line 682
    :cond_1
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    iget v0, v0, Lcom/sec/common/widget/IcsSpinner;->F:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 683
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsSpinner;->getWidth()I

    move-result v0

    .line 684
    iget-object v2, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    invoke-virtual {v2}, Lcom/sec/common/widget/IcsSpinner;->getPaddingRight()I

    move-result v2

    .line 685
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/s;->d(I)V

    goto :goto_0

    .line 687
    :cond_2
    iget-object v0, p0, Lcom/sec/common/widget/s;->a:Lcom/sec/common/widget/IcsSpinner;

    iget v0, v0, Lcom/sec/common/widget/IcsSpinner;->F:I

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/s;->d(I)V

    goto :goto_0
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 0

    .prologue
    .line 660
    invoke-super {p0, p1}, Lcom/sec/common/widget/IcsListPopupWindow;->a(Landroid/widget/ListAdapter;)V

    .line 661
    iput-object p1, p0, Lcom/sec/common/widget/s;->c:Landroid/widget/ListAdapter;

    .line 662
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 670
    iput-object p1, p0, Lcom/sec/common/widget/s;->b:Ljava/lang/CharSequence;

    .line 671
    return-void
.end method
