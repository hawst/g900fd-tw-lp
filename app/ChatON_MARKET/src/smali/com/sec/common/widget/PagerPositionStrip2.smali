.class public Lcom/sec/common/widget/PagerPositionStrip2;
.super Landroid/view/View;
.source "PagerPositionStrip2.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field private a:Landroid/support/v4/view/ViewPager;

.field private b:Landroid/support/v4/view/PagerAdapter;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:I

.field private o:I

.field private p:I

.field private q:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->o:I

    .line 44
    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->p:I

    .line 45
    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/sec/common/widget/PagerPositionStrip2;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->o:I

    .line 44
    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->p:I

    .line 45
    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    .line 56
    invoke-direct {p0, p1, p2}, Lcom/sec/common/widget/PagerPositionStrip2;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 300
    iget v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->d:I

    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->n:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->g:I

    .line 303
    iget v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->c:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->n:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->h:I

    .line 306
    iget v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->g:I

    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->n:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->e:I

    .line 309
    iget v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->e:I

    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->n:I

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->f:I

    .line 311
    iget v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->f:I

    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->c:I

    if-lt v0, v1, :cond_0

    .line 312
    iget v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->f:I

    .line 314
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 60
    sget-object v0, Lcom/sec/common/e;->PagerPositionStrip:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 62
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->i:Landroid/graphics/drawable/Drawable;

    .line 64
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->l:Landroid/graphics/drawable/Drawable;

    .line 65
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->m:Landroid/graphics/drawable/Drawable;

    .line 67
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->j:Landroid/graphics/drawable/Drawable;

    .line 68
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->k:Landroid/graphics/drawable/Drawable;

    .line 69
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->o:I

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->o:I

    .line 70
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->n:I

    .line 73
    iget-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->p:I

    .line 74
    iget-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    .line 76
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    .line 77
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    .line 79
    iget-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 80
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->p:I

    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->p:I

    .line 81
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    .line 84
    :cond_0
    iget-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 85
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->p:I

    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->p:I

    .line 86
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    .line 89
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 90
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 94
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 96
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 97
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 98
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 106
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->g:I

    if-lez v1, :cond_1

    .line 107
    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->l:Landroid/graphics/drawable/Drawable;

    .line 110
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->getPaddingLeft()I

    move-result v3

    .line 112
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    add-int v1, v3, v0

    .line 115
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->getPaddingTop()I

    move-result v0

    .line 117
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    iget v5, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    if-ge v4, v5, :cond_0

    .line 118
    iget v4, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    sub-int/2addr v4, v5

    shr-int/lit8 v4, v4, 0x1

    add-int/2addr v0, v4

    .line 121
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 122
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move v0, v1

    .line 126
    :cond_1
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->e:I

    move v7, v1

    move v1, v0

    move v0, v7

    :goto_0
    iget v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->f:I

    if-gt v0, v2, :cond_7

    .line 128
    if-nez v0, :cond_3

    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_3

    .line 129
    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->j:Landroid/graphics/drawable/Drawable;

    move-object v4, v2

    .line 137
    :goto_1
    if-nez v0, :cond_5

    iget v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->g:I

    if-nez v2, :cond_5

    .line 138
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->getPaddingLeft()I

    move-result v1

    move v2, v1

    .line 143
    :goto_2
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v1, v2

    .line 146
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->getPaddingTop()I

    move-result v3

    .line 148
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    iget v6, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    if-ge v5, v6, :cond_2

    .line 149
    iget v5, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    sub-int/2addr v5, v6

    shr-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 153
    :cond_2
    iget v5, p0, Lcom/sec/common/widget/PagerPositionStrip2;->d:I

    if-ne v5, v0, :cond_6

    .line 154
    sget-object v5, Lcom/sec/common/widget/PagerPositionStrip2;->SELECTED_STATE_SET:[I

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 160
    :goto_3
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 161
    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130
    :cond_3
    iget v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->c:I

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_4

    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_4

    .line 131
    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->k:Landroid/graphics/drawable/Drawable;

    move-object v4, v2

    goto :goto_1

    .line 133
    :cond_4
    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->i:Landroid/graphics/drawable/Drawable;

    move-object v4, v2

    goto :goto_1

    .line 140
    :cond_5
    iget v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->o:I

    add-int/2addr v1, v2

    move v2, v1

    goto :goto_2

    .line 156
    :cond_6
    sget-object v5, Lcom/sec/common/widget/PagerPositionStrip2;->EMPTY_STATE_SET:[I

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_3

    .line 165
    :cond_7
    iget v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->g:I

    iget v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->h:I

    if-eq v0, v2, :cond_9

    .line 166
    iget-object v2, p0, Lcom/sec/common/widget/PagerPositionStrip2;->m:Landroid/graphics/drawable/Drawable;

    .line 169
    iget v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->o:I

    add-int/2addr v1, v0

    .line 171
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    add-int/2addr v0, v1

    .line 174
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->getPaddingTop()I

    move-result v0

    .line 176
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    iget v4, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    if-ge v3, v4, :cond_8

    .line 177
    iget v3, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    sub-int/2addr v3, v4

    shr-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 180
    :cond_8
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 181
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 183
    :cond_9
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 187
    .line 190
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 191
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 194
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 195
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 198
    iget v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->p:I

    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->o:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->f:I

    iget v7, p0, Lcom/sec/common/widget/PagerPositionStrip2;->e:I

    sub-int/2addr v1, v7

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->o:I

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->g:I

    if-lez v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget v7, p0, Lcom/sec/common/widget/PagerPositionStrip2;->o:I

    add-int/2addr v1, v7

    add-int/2addr v0, v1

    .line 204
    :cond_0
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->g:I

    iget v7, p0, Lcom/sec/common/widget/PagerPositionStrip2;->h:I

    if-eq v1, v7, :cond_1

    .line 205
    iget-object v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget v7, p0, Lcom/sec/common/widget/PagerPositionStrip2;->o:I

    add-int/2addr v1, v7

    add-int/2addr v0, v1

    .line 209
    :cond_1
    iget v1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->q:I

    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->getPaddingTop()I

    move-result v7

    add-int/2addr v1, v7

    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->getPaddingBottom()I

    move-result v7

    add-int/2addr v1, v7

    .line 211
    sparse-switch v3, :sswitch_data_0

    move v3, v2

    .line 225
    :goto_0
    sparse-switch v4, :sswitch_data_1

    move v0, v2

    .line 239
    :goto_1
    invoke-virtual {p0, v3, v0}, Lcom/sec/common/widget/PagerPositionStrip2;->setMeasuredDimension(II)V

    .line 240
    return-void

    .line 213
    :sswitch_0
    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v3, v0

    .line 214
    goto :goto_0

    :sswitch_1
    move v3, v0

    .line 218
    goto :goto_0

    :sswitch_2
    move v3, v0

    .line 221
    goto :goto_0

    .line 227
    :sswitch_3
    invoke-static {v6, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1

    :sswitch_4
    move v0, v1

    .line 232
    goto :goto_1

    :sswitch_5
    move v0, v1

    .line 235
    goto :goto_1

    .line 211
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_1
    .end sparse-switch

    .line 225
    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x0 -> :sswitch_5
        0x40000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 296
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    .prologue
    .line 281
    return-void
.end method

.method public onPageSelected(I)V
    .locals 0

    .prologue
    .line 285
    iput p1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->d:I

    .line 287
    invoke-direct {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->a()V

    .line 289
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->requestLayout()V

    .line 290
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->invalidate()V

    .line 291
    return-void
.end method

.method public setFirstIndicator(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->j:Landroid/graphics/drawable/Drawable;

    .line 260
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->requestLayout()V

    .line 261
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->invalidate()V

    .line 262
    return-void
.end method

.method public setIndicator(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->i:Landroid/graphics/drawable/Drawable;

    .line 267
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->requestLayout()V

    .line 268
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->invalidate()V

    .line 269
    return-void
.end method

.method public setLastIndicator(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->k:Landroid/graphics/drawable/Drawable;

    .line 274
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->requestLayout()V

    .line 275
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->invalidate()V

    .line 276
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 1

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/common/widget/PagerPositionStrip2;->a:Landroid/support/v4/view/ViewPager;

    .line 244
    iget-object v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->b:Landroid/support/v4/view/PagerAdapter;

    .line 246
    iget-object v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 248
    iget-object v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->d:I

    .line 249
    iget-object v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->b:Landroid/support/v4/view/PagerAdapter;

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/common/widget/PagerPositionStrip2;->c:I

    .line 251
    invoke-direct {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->a()V

    .line 253
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->requestLayout()V

    .line 254
    invoke-virtual {p0}, Lcom/sec/common/widget/PagerPositionStrip2;->invalidate()V

    .line 255
    return-void
.end method
