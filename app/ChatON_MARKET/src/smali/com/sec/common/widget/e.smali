.class Lcom/sec/common/widget/e;
.super Landroid/database/DataSetObserver;
.source "IcsAdapterView.java"


# instance fields
.field final synthetic a:Lcom/sec/common/widget/IcsAdapterView;

.field private b:Landroid/os/Parcelable;


# direct methods
.method constructor <init>(Lcom/sec/common/widget/IcsAdapterView;)V
    .locals 1

    .prologue
    .line 761
    iput-object p1, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 763
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/common/widget/e;->b:Landroid/os/Parcelable;

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 767
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/common/widget/IcsAdapterView;->v:Z

    .line 768
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iget-object v1, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iget v1, v1, Lcom/sec/common/widget/IcsAdapterView;->A:I

    iput v1, v0, Lcom/sec/common/widget/IcsAdapterView;->B:I

    .line 769
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iget-object v1, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    invoke-virtual {v1}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    iput v1, v0, Lcom/sec/common/widget/IcsAdapterView;->A:I

    .line 773
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/common/widget/e;->b:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iget v0, v0, Lcom/sec/common/widget/IcsAdapterView;->B:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iget v0, v0, Lcom/sec/common/widget/IcsAdapterView;->A:I

    if-lez v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iget-object v1, p0, Lcom/sec/common/widget/e;->b:Landroid/os/Parcelable;

    invoke-static {v0, v1}, Lcom/sec/common/widget/IcsAdapterView;->a(Lcom/sec/common/widget/IcsAdapterView;Landroid/os/Parcelable;)V

    .line 776
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/common/widget/e;->b:Landroid/os/Parcelable;

    .line 780
    :goto_0
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsAdapterView;->l()V

    .line 781
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsAdapterView;->requestLayout()V

    .line 782
    return-void

    .line 778
    :cond_0
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsAdapterView;->q()V

    goto :goto_0
.end method

.method public onInvalidated()V
    .locals 6

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 786
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/common/widget/IcsAdapterView;->v:Z

    .line 788
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    invoke-static {v0}, Lcom/sec/common/widget/IcsAdapterView;->a(Lcom/sec/common/widget/IcsAdapterView;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/widget/e;->b:Landroid/os/Parcelable;

    .line 795
    :cond_0
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iget-object v1, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iget v1, v1, Lcom/sec/common/widget/IcsAdapterView;->A:I

    iput v1, v0, Lcom/sec/common/widget/IcsAdapterView;->B:I

    .line 796
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iput v3, v0, Lcom/sec/common/widget/IcsAdapterView;->A:I

    .line 797
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iput v2, v0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    .line 798
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iput-wide v4, v0, Lcom/sec/common/widget/IcsAdapterView;->z:J

    .line 799
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iput v2, v0, Lcom/sec/common/widget/IcsAdapterView;->w:I

    .line 800
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iput-wide v4, v0, Lcom/sec/common/widget/IcsAdapterView;->x:J

    .line 801
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    iput-boolean v3, v0, Lcom/sec/common/widget/IcsAdapterView;->p:Z

    .line 803
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsAdapterView;->l()V

    .line 804
    iget-object v0, p0, Lcom/sec/common/widget/e;->a:Lcom/sec/common/widget/IcsAdapterView;

    invoke-virtual {v0}, Lcom/sec/common/widget/IcsAdapterView;->requestLayout()V

    .line 805
    return-void
.end method
