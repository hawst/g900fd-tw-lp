.class Lcom/sec/common/widget/k;
.super Landroid/widget/ListView;
.source "IcsListPopupWindow.java"


# instance fields
.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 544
    const/4 v0, 0x0

    sget v1, Lcom/sec/common/b;->dropDownListViewStyle:I

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545
    iput-boolean p2, p0, Lcom/sec/common/widget/k;->b:Z

    .line 547
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/k;->setCacheColorHint(I)V

    .line 548
    return-void
.end method

.method static synthetic a(Lcom/sec/common/widget/k;Z)Z
    .locals 0

    .prologue
    .line 510
    iput-boolean p1, p0, Lcom/sec/common/widget/k;->a:Z

    return p1
.end method


# virtual methods
.method public hasFocus()Z
    .locals 1

    .prologue
    .line 579
    iget-boolean v0, p0, Lcom/sec/common/widget/k;->b:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/ListView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWindowFocus()Z
    .locals 1

    .prologue
    .line 569
    iget-boolean v0, p0, Lcom/sec/common/widget/k;->b:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/ListView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFocused()Z
    .locals 1

    .prologue
    .line 574
    iget-boolean v0, p0, Lcom/sec/common/widget/k;->b:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/ListView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInTouchMode()Z
    .locals 1

    .prologue
    .line 564
    iget-boolean v0, p0, Lcom/sec/common/widget/k;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/common/widget/k;->a:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
