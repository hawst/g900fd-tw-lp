.class public Lcom/sec/common/widget/HoneycombLinearLayout;
.super Landroid/widget/LinearLayout;
.source "HoneycombLinearLayout.java"


# static fields
.field private static final a:[I


# instance fields
.field private b:Landroid/graphics/drawable/Drawable;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/common/widget/HoneycombLinearLayout;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1010129
        0x1010329
        0x101032a
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->g:Z

    .line 59
    iget-boolean v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->g:Z

    if-eqz v0, :cond_0

    .line 60
    sget-object v0, Lcom/sec/common/widget/HoneycombLinearLayout;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 62
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/common/widget/HoneycombLinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 63
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->e:I

    .line 64
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->f:I

    .line 66
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 68
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 57
    goto :goto_0
.end method


# virtual methods
.method a(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getChildCount()I

    move-result v2

    .line 184
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 185
    invoke-virtual {p0, v1}, Lcom/sec/common/widget/HoneycombLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 187
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-eq v0, v4, :cond_0

    .line 188
    invoke-virtual {p0, v1}, Lcom/sec/common/widget/HoneycombLinearLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 190
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    sub-int v0, v3, v0

    .line 191
    invoke-virtual {p0, p1, v0}, Lcom/sec/common/widget/HoneycombLinearLayout;->a(Landroid/graphics/Canvas;I)V

    .line 184
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 196
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/common/widget/HoneycombLinearLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 199
    if-nez v0, :cond_3

    .line 200
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->d:I

    sub-int/2addr v0, v1

    .line 205
    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/sec/common/widget/HoneycombLinearLayout;->a(Landroid/graphics/Canvas;I)V

    .line 207
    :cond_2
    return-void

    .line 203
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    goto :goto_1
.end method

.method a(Landroid/graphics/Canvas;I)V
    .locals 4

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->f:I

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->f:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->d:I

    add-int/2addr v3, p2

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 239
    iget-object v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 240
    return-void
.end method

.method protected a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 257
    if-nez p1, :cond_2

    .line 258
    iget v2, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->e:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 271
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 258
    goto :goto_0

    .line 259
    :cond_2
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getChildCount()I

    move-result v2

    if-ne p1, v2, :cond_3

    .line 260
    iget v2, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->e:I

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 261
    :cond_3
    iget v2, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->e:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_4

    .line 263
    add-int/lit8 v2, p1, -0x1

    :goto_1
    if-ltz v2, :cond_5

    .line 264
    invoke-virtual {p0, v2}, Lcom/sec/common/widget/HoneycombLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    .line 263
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 271
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method b(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getChildCount()I

    move-result v2

    .line 211
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 212
    invoke-virtual {p0, v1}, Lcom/sec/common/widget/HoneycombLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 214
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-eq v0, v4, :cond_0

    .line 215
    invoke-virtual {p0, v1}, Lcom/sec/common/widget/HoneycombLinearLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 217
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    sub-int v0, v3, v0

    .line 218
    invoke-virtual {p0, p1, v0}, Lcom/sec/common/widget/HoneycombLinearLayout;->b(Landroid/graphics/Canvas;I)V

    .line 211
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 223
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/common/widget/HoneycombLinearLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 224
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 226
    if-nez v0, :cond_3

    .line 227
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->c:I

    sub-int/2addr v0, v1

    .line 232
    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/sec/common/widget/HoneycombLinearLayout;->b(Landroid/graphics/Canvas;I)V

    .line 234
    :cond_2
    return-void

    .line 230
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    goto :goto_1
.end method

.method b(Landroid/graphics/Canvas;I)V
    .locals 5

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->f:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->c:I

    add-int/2addr v2, p2

    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->f:I

    sub-int/2addr v3, v4

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 245
    iget-object v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 246
    return-void
.end method

.method public getShowDividers()I
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->g:Z

    if-eqz v0, :cond_0

    .line 96
    iget v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->e:I

    .line 99
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->getShowDividers()I

    move-result v0

    goto :goto_0
.end method

.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 138
    iget-boolean v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->g:Z

    if-eqz v0, :cond_1

    .line 139
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/HoneycombLinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 140
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getOrientation()I

    move-result v2

    .line 141
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 142
    invoke-virtual {p0, v1}, Lcom/sec/common/widget/HoneycombLinearLayout;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 143
    if-ne v2, v5, :cond_2

    .line 145
    iget v3, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->d:I

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 152
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getChildCount()I

    move-result v3

    .line 153
    add-int/lit8 v4, v3, -0x1

    if-ne v1, v4, :cond_1

    .line 154
    invoke-virtual {p0, v3}, Lcom/sec/common/widget/HoneycombLinearLayout;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 155
    if-ne v2, v5, :cond_3

    .line 156
    iget v1, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->d:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 164
    :cond_1
    :goto_1
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 165
    return-void

    .line 148
    :cond_2
    iget v3, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->c:I

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 158
    :cond_3
    iget v1, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->c:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->g:Z

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 172
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/HoneycombLinearLayout;->a(Landroid/graphics/Canvas;)V

    .line 179
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 180
    return-void

    .line 174
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/HoneycombLinearLayout;->b(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-boolean v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->g:Z

    if-eqz v0, :cond_4

    .line 111
    iget-object v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 116
    :cond_0
    instance-of v0, p1, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v0, v2, :cond_1

    .line 117
    new-instance v0, Lcom/sec/common/widget/i;

    check-cast p1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Lcom/sec/common/widget/i;-><init>(Landroid/graphics/drawable/ColorDrawable;)V

    move-object p1, v0

    .line 120
    :cond_1
    iput-object p1, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    .line 121
    if-eqz p1, :cond_2

    .line 122
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->c:I

    .line 123
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->d:I

    .line 128
    :goto_1
    if-nez p1, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0, v0}, Lcom/sec/common/widget/HoneycombLinearLayout;->setWillNotDraw(Z)V

    .line 129
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->requestLayout()V

    goto :goto_0

    .line 125
    :cond_2
    iput v1, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->c:I

    .line 126
    iput v1, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->d:I

    goto :goto_1

    :cond_3
    move v0, v1

    .line 128
    goto :goto_2

    .line 132
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setShowDividers(I)V
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->g:Z

    if-eqz v0, :cond_1

    .line 79
    iget v0, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->e:I

    if-eq p1, v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->requestLayout()V

    .line 81
    invoke-virtual {p0}, Lcom/sec/common/widget/HoneycombLinearLayout;->invalidate()V

    .line 83
    :cond_0
    iput p1, p0, Lcom/sec/common/widget/HoneycombLinearLayout;->e:I

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    goto :goto_0
.end method
