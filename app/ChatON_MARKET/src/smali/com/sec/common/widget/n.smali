.class Lcom/sec/common/widget/n;
.super Ljava/lang/Object;
.source "IcsListPopupWindow.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/common/widget/IcsListPopupWindow;


# direct methods
.method private constructor <init>(Lcom/sec/common/widget/IcsListPopupWindow;)V
    .locals 0

    .prologue
    .line 631
    iput-object p1, p0, Lcom/sec/common/widget/n;->a:Lcom/sec/common/widget/IcsListPopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/common/widget/IcsListPopupWindow;Lcom/sec/common/widget/j;)V
    .locals 0

    .prologue
    .line 631
    invoke-direct {p0, p1}, Lcom/sec/common/widget/n;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;)V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 635
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 638
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/sec/common/widget/n;->a:Lcom/sec/common/widget/IcsListPopupWindow;

    invoke-static {v0}, Lcom/sec/common/widget/IcsListPopupWindow;->f(Lcom/sec/common/widget/IcsListPopupWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/common/widget/n;->a:Lcom/sec/common/widget/IcsListPopupWindow;

    invoke-static {v0}, Lcom/sec/common/widget/IcsListPopupWindow;->c(Lcom/sec/common/widget/IcsListPopupWindow;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/sec/common/widget/n;->a:Lcom/sec/common/widget/IcsListPopupWindow;

    invoke-static {v0}, Lcom/sec/common/widget/IcsListPopupWindow;->e(Lcom/sec/common/widget/IcsListPopupWindow;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/common/widget/n;->a:Lcom/sec/common/widget/IcsListPopupWindow;

    invoke-static {v1}, Lcom/sec/common/widget/IcsListPopupWindow;->d(Lcom/sec/common/widget/IcsListPopupWindow;)Lcom/sec/common/widget/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 641
    iget-object v0, p0, Lcom/sec/common/widget/n;->a:Lcom/sec/common/widget/IcsListPopupWindow;

    invoke-static {v0}, Lcom/sec/common/widget/IcsListPopupWindow;->d(Lcom/sec/common/widget/IcsListPopupWindow;)Lcom/sec/common/widget/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/widget/p;->run()V

    .line 643
    :cond_0
    return-void
.end method
