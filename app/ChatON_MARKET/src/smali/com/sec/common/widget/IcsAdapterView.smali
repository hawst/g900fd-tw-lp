.class public abstract Lcom/sec/common/widget/IcsAdapterView;
.super Landroid/view/ViewGroup;
.source "IcsAdapterView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/widget/Adapter;",
        ">",
        "Landroid/view/ViewGroup;"
    }
.end annotation


# instance fields
.field A:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field B:I

.field C:I

.field D:J

.field E:Z

.field private a:I

.field private b:Landroid/view/View;

.field private c:Z

.field private d:Z

.field private e:Lcom/sec/common/widget/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/common/widget/IcsAdapterView",
            "<TT;>.com/sec/common/widget/h;"
        }
    .end annotation
.end field

.field k:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "scrolling"
    .end annotation
.end field

.field l:I

.field m:I

.field n:J

.field o:J

.field p:Z

.field q:I

.field r:Z

.field s:Lcom/sec/common/widget/g;

.field t:Landroid/widget/AdapterView$OnItemClickListener;

.field u:Lcom/sec/common/widget/f;

.field v:Z

.field w:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field x:J

.field y:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field z:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const-wide/high16 v1, -0x8000000000000000L

    const/4 v0, 0x0

    .line 228
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 67
    iput v0, p0, Lcom/sec/common/widget/IcsAdapterView;->k:I

    .line 84
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->n:J

    .line 94
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->p:Z

    .line 126
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->r:Z

    .line 152
    iput v3, p0, Lcom/sec/common/widget/IcsAdapterView;->w:I

    .line 158
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->x:J

    .line 163
    iput v3, p0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    .line 169
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->z:J

    .line 201
    iput v3, p0, Lcom/sec/common/widget/IcsAdapterView;->C:I

    .line 206
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->D:J

    .line 225
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->E:Z

    .line 229
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const-wide/high16 v1, -0x8000000000000000L

    const/4 v0, 0x0

    .line 232
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    iput v0, p0, Lcom/sec/common/widget/IcsAdapterView;->k:I

    .line 84
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->n:J

    .line 94
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->p:Z

    .line 126
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->r:Z

    .line 152
    iput v3, p0, Lcom/sec/common/widget/IcsAdapterView;->w:I

    .line 158
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->x:J

    .line 163
    iput v3, p0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    .line 169
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->z:J

    .line 201
    iput v3, p0, Lcom/sec/common/widget/IcsAdapterView;->C:I

    .line 206
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->D:J

    .line 225
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->E:Z

    .line 233
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const-wide/high16 v1, -0x8000000000000000L

    const/4 v0, 0x0

    .line 236
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    iput v0, p0, Lcom/sec/common/widget/IcsAdapterView;->k:I

    .line 84
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->n:J

    .line 94
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->p:Z

    .line 126
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->r:Z

    .line 152
    iput v3, p0, Lcom/sec/common/widget/IcsAdapterView;->w:I

    .line 158
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->x:J

    .line 163
    iput v3, p0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    .line 169
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->z:J

    .line 201
    iput v3, p0, Lcom/sec/common/widget/IcsAdapterView;->C:I

    .line 206
    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->D:J

    .line 225
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->E:Z

    .line 237
    return-void
.end method

.method static synthetic a(Lcom/sec/common/widget/IcsAdapterView;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    .line 856
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->s:Lcom/sec/common/widget/g;

    if-nez v0, :cond_0

    .line 868
    :goto_0
    return-void

    .line 860
    :cond_0
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->g()I

    move-result v3

    .line 861
    if-ltz v3, :cond_1

    .line 862
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->c()Landroid/view/View;

    move-result-object v2

    .line 863
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->s:Lcom/sec/common/widget/g;

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/sec/common/widget/g;->a(Lcom/sec/common/widget/IcsAdapterView;Landroid/view/View;IJ)V

    goto :goto_0

    .line 866
    :cond_1
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->s:Lcom/sec/common/widget/g;

    invoke-interface {v0, p0}, Lcom/sec/common/widget/g;->a(Lcom/sec/common/widget/IcsAdapterView;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/common/widget/IcsAdapterView;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/IcsAdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 697
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    move p1, v1

    .line 701
    :cond_0
    if-eqz p1, :cond_3

    .line 702
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->b:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 703
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 704
    invoke-virtual {p0, v2}, Lcom/sec/common/widget/IcsAdapterView;->setVisibility(I)V

    .line 713
    :goto_0
    iget-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->v:Z

    if-eqz v0, :cond_1

    .line 714
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->getTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->getRight()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->getBottom()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/common/widget/IcsAdapterView;->onLayout(ZIIII)V

    .line 721
    :cond_1
    :goto_1
    return-void

    .line 707
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/common/widget/IcsAdapterView;->setVisibility(I)V

    goto :goto_0

    .line 717
    :cond_3
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->b:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 719
    :cond_4
    invoke-virtual {p0, v1}, Lcom/sec/common/widget/IcsAdapterView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/sec/common/widget/IcsAdapterView;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/common/widget/IcsAdapterView;->a()V

    return-void
.end method

.method private b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 919
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v1

    .line 920
    if-eqz v1, :cond_1

    .line 921
    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    .line 922
    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->i()I

    move-result v2

    if-gtz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->j()I

    move-result v2

    add-int/lit8 v1, v1, -0x1

    if-ge v2, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 925
    :cond_1
    return v0
.end method


# virtual methods
.method public a(I)J
    .locals 2

    .prologue
    .line 735
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v0

    .line 736
    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_1
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public addView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 425
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 438
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 466
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 451
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method b(I)V
    .locals 2

    .prologue
    .line 1108
    iput p1, p0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    .line 1109
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/IcsAdapterView;->a(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/common/widget/IcsAdapterView;->z:J

    .line 1110
    return-void
.end method

.method c(IZ)I
    .locals 0

    .prologue
    .line 1100
    return p1
.end method

.method public abstract c()Landroid/view/View;
.end method

.method c(I)V
    .locals 2

    .prologue
    .line 1118
    iput p1, p0, Lcom/sec/common/widget/IcsAdapterView;->w:I

    .line 1119
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/IcsAdapterView;->a(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/common/widget/IcsAdapterView;->x:J

    .line 1121
    iget-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->p:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->q:I

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    .line 1122
    iput p1, p0, Lcom/sec/common/widget/IcsAdapterView;->m:I

    .line 1123
    iget-wide v0, p0, Lcom/sec/common/widget/IcsAdapterView;->x:J

    iput-wide v0, p0, Lcom/sec/common/widget/IcsAdapterView;->n:J

    .line 1125
    :cond_0
    return-void
.end method

.method protected canAnimate()Z
    .locals 1

    .prologue
    .line 930
    invoke-super {p0}, Landroid/view/ViewGroup;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->A:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    .line 872
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->c()Landroid/view/View;

    move-result-object v0

    .line 873
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 875
    const/4 v0, 0x1

    .line 877
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 758
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/IcsAdapterView;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 759
    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 750
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/IcsAdapterView;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 751
    return-void
.end method

.method public e()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 555
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->A:I

    return v0
.end method

.method public abstract f()Landroid/widget/Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public g()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 516
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->w:I

    return v0
.end method

.method public h()J
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 525
    iget-wide v0, p0, Lcom/sec/common/widget/IcsAdapterView;->x:J

    return-wide v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 598
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->k:I

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 608
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->k:I

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method k()Z
    .locals 1

    .prologue
    .line 648
    const/4 v0, 0x0

    return v0
.end method

.method l()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 678
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v4

    .line 679
    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    move v0, v1

    .line 680
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_1
    move v3, v1

    .line 684
    :goto_1
    if-eqz v3, :cond_7

    iget-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->d:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_2
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 685
    if-eqz v3, :cond_8

    iget-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->c:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 686
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->b:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 687
    if-eqz v4, :cond_2

    invoke-interface {v4}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/common/widget/IcsAdapterView;->a(Z)V

    .line 689
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 679
    goto :goto_0

    :cond_6
    move v3, v2

    .line 680
    goto :goto_1

    :cond_7
    move v0, v2

    .line 684
    goto :goto_2

    :cond_8
    move v0, v2

    .line 685
    goto :goto_3
.end method

.method m()V
    .locals 2

    .prologue
    .line 834
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->s:Lcom/sec/common/widget/g;

    if-eqz v0, :cond_2

    .line 835
    iget-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->r:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->E:Z

    if-eqz v0, :cond_4

    .line 840
    :cond_0
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->e:Lcom/sec/common/widget/h;

    if-nez v0, :cond_1

    .line 841
    new-instance v0, Lcom/sec/common/widget/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/common/widget/h;-><init>(Lcom/sec/common/widget/IcsAdapterView;Lcom/sec/common/widget/d;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->e:Lcom/sec/common/widget/h;

    .line 843
    :cond_1
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->e:Lcom/sec/common/widget/h;

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAdapterView;->post(Ljava/lang/Runnable;)Z

    .line 850
    :cond_2
    :goto_0
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_3

    .line 851
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAdapterView;->sendAccessibilityEvent(I)V

    .line 853
    :cond_3
    return-void

    .line 845
    :cond_4
    invoke-direct {p0}, Lcom/sec/common/widget/IcsAdapterView;->a()V

    goto :goto_0
.end method

.method n()V
    .locals 8

    .prologue
    const-wide/high16 v6, -0x8000000000000000L

    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 934
    iget v4, p0, Lcom/sec/common/widget/IcsAdapterView;->A:I

    .line 937
    if-lez v4, :cond_6

    .line 942
    iget-boolean v0, p0, Lcom/sec/common/widget/IcsAdapterView;->p:Z

    if-eqz v0, :cond_5

    .line 945
    iput-boolean v1, p0, Lcom/sec/common/widget/IcsAdapterView;->p:Z

    .line 949
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->p()I

    move-result v0

    .line 950
    if-ltz v0, :cond_5

    .line 952
    invoke-virtual {p0, v0, v2}, Lcom/sec/common/widget/IcsAdapterView;->c(IZ)I

    move-result v3

    .line 953
    if-ne v3, v0, :cond_5

    .line 955
    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAdapterView;->c(I)V

    move v3, v2

    .line 960
    :goto_0
    if-nez v3, :cond_3

    .line 962
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->g()I

    move-result v0

    .line 965
    if-lt v0, v4, :cond_0

    .line 966
    add-int/lit8 v0, v4, -0x1

    .line 968
    :cond_0
    if-gez v0, :cond_1

    move v0, v1

    .line 973
    :cond_1
    invoke-virtual {p0, v0, v2}, Lcom/sec/common/widget/IcsAdapterView;->c(IZ)I

    move-result v4

    .line 974
    if-gez v4, :cond_4

    .line 976
    invoke-virtual {p0, v0, v1}, Lcom/sec/common/widget/IcsAdapterView;->c(IZ)I

    move-result v0

    .line 978
    :goto_1
    if-ltz v0, :cond_3

    .line 979
    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAdapterView;->c(I)V

    .line 980
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->o()V

    move v0, v2

    .line 985
    :goto_2
    if-nez v0, :cond_2

    .line 987
    iput v5, p0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    .line 988
    iput-wide v6, p0, Lcom/sec/common/widget/IcsAdapterView;->z:J

    .line 989
    iput v5, p0, Lcom/sec/common/widget/IcsAdapterView;->w:I

    .line 990
    iput-wide v6, p0, Lcom/sec/common/widget/IcsAdapterView;->x:J

    .line 991
    iput-boolean v1, p0, Lcom/sec/common/widget/IcsAdapterView;->p:Z

    .line 992
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->o()V

    .line 994
    :cond_2
    return-void

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v0, v4

    goto :goto_1

    :cond_5
    move v3, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method o()V
    .locals 4

    .prologue
    .line 997
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    iget v1, p0, Lcom/sec/common/widget/IcsAdapterView;->C:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/sec/common/widget/IcsAdapterView;->z:J

    iget-wide v2, p0, Lcom/sec/common/widget/IcsAdapterView;->D:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 998
    :cond_0
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->m()V

    .line 999
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    iput v0, p0, Lcom/sec/common/widget/IcsAdapterView;->C:I

    .line 1000
    iget-wide v0, p0, Lcom/sec/common/widget/IcsAdapterView;->z:J

    iput-wide v0, p0, Lcom/sec/common/widget/IcsAdapterView;->D:J

    .line 1002
    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 814
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 815
    iget-object v0, p0, Lcom/sec/common/widget/IcsAdapterView;->e:Lcom/sec/common/widget/h;

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAdapterView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 816
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 906
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 907
    invoke-direct {p0}, Lcom/sec/common/widget/IcsAdapterView;->b()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    .line 908
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->c()Landroid/view/View;

    move-result-object v0

    .line 909
    if-eqz v0, :cond_0

    .line 910
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 912
    :cond_0
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setCurrentItemIndex(I)V

    .line 913
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 914
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->j()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 915
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->e()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 916
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 896
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 897
    invoke-direct {p0}, Lcom/sec/common/widget/IcsAdapterView;->b()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 898
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->c()Landroid/view/View;

    move-result-object v0

    .line 899
    if-eqz v0, :cond_0

    .line 900
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 902
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/common/widget/IcsAdapterView;->a:I

    .line 507
    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 882
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 884
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 885
    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAdapterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 887
    invoke-virtual {p1, v0}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 888
    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->appendRecord(Landroid/view/accessibility/AccessibilityRecord;)V

    .line 889
    const/4 v0, 0x1

    .line 891
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method p()I
    .locals 15

    .prologue
    const/4 v2, 0x1

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 1013
    iget v9, p0, Lcom/sec/common/widget/IcsAdapterView;->A:I

    .line 1015
    if-nez v9, :cond_1

    move v5, v6

    .line 1088
    :cond_0
    :goto_0
    return v5

    .line 1019
    :cond_1
    iget-wide v10, p0, Lcom/sec/common/widget/IcsAdapterView;->n:J

    .line 1020
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->m:I

    .line 1023
    const-wide/high16 v3, -0x8000000000000000L

    cmp-long v3, v10, v3

    if-nez v3, :cond_2

    move v5, v6

    .line 1024
    goto :goto_0

    .line 1028
    :cond_2
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1029
    add-int/lit8 v3, v9, -0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1031
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    const-wide/16 v7, 0x64

    add-long v12, v3, v7

    .line 1052
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v14

    .line 1053
    if-nez v14, :cond_b

    move v5, v6

    .line 1054
    goto :goto_0

    .line 1072
    :cond_3
    if-nez v7, :cond_4

    if-eqz v0, :cond_9

    if-nez v8, :cond_9

    .line 1074
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v5, v0

    move v0, v1

    .line 1057
    :cond_5
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    cmp-long v7, v7, v12

    if-gtz v7, :cond_6

    .line 1058
    invoke-interface {v14, v5}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v7

    .line 1059
    cmp-long v7, v7, v10

    if-eqz v7, :cond_0

    .line 1064
    add-int/lit8 v7, v9, -0x1

    if-ne v3, v7, :cond_7

    move v8, v2

    .line 1065
    :goto_2
    if-nez v4, :cond_8

    move v7, v2

    .line 1067
    :goto_3
    if-eqz v8, :cond_3

    if-eqz v7, :cond_3

    :cond_6
    move v5, v6

    .line 1088
    goto :goto_0

    :cond_7
    move v8, v1

    .line 1064
    goto :goto_2

    :cond_8
    move v7, v1

    .line 1065
    goto :goto_3

    .line 1078
    :cond_9
    if-nez v8, :cond_a

    if-nez v0, :cond_5

    if-nez v7, :cond_5

    .line 1080
    :cond_a
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    move v5, v0

    move v0, v2

    .line 1083
    goto :goto_1

    :cond_b
    move v3, v0

    move v4, v0

    move v5, v0

    move v0, v1

    goto :goto_1
.end method

.method q()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1133
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1134
    iput-boolean v4, p0, Lcom/sec/common/widget/IcsAdapterView;->p:Z

    .line 1135
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->a:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/common/widget/IcsAdapterView;->o:J

    .line 1136
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    if-ltz v0, :cond_2

    .line 1138
    iget v0, p0, Lcom/sec/common/widget/IcsAdapterView;->y:I

    iget v1, p0, Lcom/sec/common/widget/IcsAdapterView;->k:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1139
    iget-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->x:J

    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->n:J

    .line 1140
    iget v1, p0, Lcom/sec/common/widget/IcsAdapterView;->w:I

    iput v1, p0, Lcom/sec/common/widget/IcsAdapterView;->m:I

    .line 1141
    if-eqz v0, :cond_0

    .line 1142
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/sec/common/widget/IcsAdapterView;->l:I

    .line 1144
    :cond_0
    iput v3, p0, Lcom/sec/common/widget/IcsAdapterView;->q:I

    .line 1161
    :cond_1
    :goto_0
    return-void

    .line 1147
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/common/widget/IcsAdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1148
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v1

    .line 1149
    iget v2, p0, Lcom/sec/common/widget/IcsAdapterView;->k:I

    if-ltz v2, :cond_4

    iget v2, p0, Lcom/sec/common/widget/IcsAdapterView;->k:I

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 1150
    iget v2, p0, Lcom/sec/common/widget/IcsAdapterView;->k:I

    invoke-interface {v1, v2}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->n:J

    .line 1154
    :goto_1
    iget v1, p0, Lcom/sec/common/widget/IcsAdapterView;->k:I

    iput v1, p0, Lcom/sec/common/widget/IcsAdapterView;->m:I

    .line 1155
    if-eqz v0, :cond_3

    .line 1156
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/sec/common/widget/IcsAdapterView;->l:I

    .line 1158
    :cond_3
    iput v4, p0, Lcom/sec/common/widget/IcsAdapterView;->q:I

    goto :goto_0

    .line 1152
    :cond_4
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/sec/common/widget/IcsAdapterView;->n:J

    goto :goto_1
.end method

.method public removeAllViews()V
    .locals 2

    .prologue
    .line 501
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeAllViews() is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 479
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeViewAt(I)V
    .locals 2

    .prologue
    .line 491
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeViewAt(int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract setAdapter(Landroid/widget/Adapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 623
    iput-object p1, p0, Lcom/sec/common/widget/IcsAdapterView;->b:Landroid/view/View;

    .line 625
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v0

    .line 626
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 627
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/common/widget/IcsAdapterView;->a(Z)V

    .line 628
    return-void

    .line 626
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFocusable(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 653
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v0

    .line 654
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    .line 656
    :goto_0
    iput-boolean p1, p0, Lcom/sec/common/widget/IcsAdapterView;->c:Z

    .line 657
    if-nez p1, :cond_1

    .line 658
    iput-boolean v1, p0, Lcom/sec/common/widget/IcsAdapterView;->d:Z

    .line 661
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    :goto_1
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 662
    return-void

    :cond_3
    move v0, v1

    .line 654
    goto :goto_0

    :cond_4
    move v2, v1

    .line 661
    goto :goto_1
.end method

.method public setFocusableInTouchMode(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 666
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->f()Landroid/widget/Adapter;

    move-result-object v0

    .line 667
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    .line 669
    :goto_0
    iput-boolean p1, p0, Lcom/sec/common/widget/IcsAdapterView;->d:Z

    .line 670
    if-eqz p1, :cond_1

    .line 671
    iput-boolean v2, p0, Lcom/sec/common/widget/IcsAdapterView;->c:Z

    .line 674
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    :goto_1
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 675
    return-void

    :cond_3
    move v0, v1

    .line 667
    goto :goto_0

    :cond_4
    move v2, v1

    .line 674
    goto :goto_1
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 741
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Don\'t call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/common/widget/IcsAdapterView;->t:Landroid/widget/AdapterView$OnItemClickListener;

    .line 247
    return-void
.end method

.method public setOnItemLongClickListener(Lcom/sec/common/widget/f;)V
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAdapterView;->isLongClickable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAdapterView;->setLongClickable(Z)V

    .line 312
    :cond_0
    iput-object p1, p0, Lcom/sec/common/widget/IcsAdapterView;->u:Lcom/sec/common/widget/f;

    .line 313
    return-void
.end method

.method public setOnItemSelectedListener(Lcom/sec/common/widget/g;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/sec/common/widget/IcsAdapterView;->s:Lcom/sec/common/widget/g;

    .line 363
    return-void
.end method

.method public abstract setSelection(I)V
.end method
