.class public abstract Lcom/sec/common/widget/IcsAbsSpinner;
.super Lcom/sec/common/widget/IcsAdapterView;
.source "IcsAbsSpinner.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/common/widget/IcsAdapterView",
        "<",
        "Landroid/widget/SpinnerAdapter;",
        ">;"
    }
.end annotation


# static fields
.field private static final F:Z


# instance fields
.field private G:Landroid/database/DataSetObserver;

.field a:Landroid/widget/SpinnerAdapter;

.field b:I

.field c:I

.field d:Z

.field e:I

.field f:I

.field g:I

.field h:I

.field final i:Landroid/graphics/Rect;

.field final j:Lcom/sec/common/widget/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/common/widget/IcsAbsSpinner;->F:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, p1}, Lcom/sec/common/widget/IcsAdapterView;-><init>(Landroid/content/Context;)V

    .line 46
    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->e:I

    .line 47
    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->f:I

    .line 48
    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->g:I

    .line 49
    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->h:I

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    .line 52
    new-instance v0, Lcom/sec/common/widget/b;

    invoke-direct {v0, p0}, Lcom/sec/common/widget/b;-><init>(Lcom/sec/common/widget/IcsAbsSpinner;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->j:Lcom/sec/common/widget/b;

    .line 60
    invoke-direct {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->r()V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/common/widget/IcsAbsSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/common/widget/IcsAdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->e:I

    .line 47
    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->f:I

    .line 48
    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->g:I

    .line 49
    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->h:I

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    .line 52
    new-instance v0, Lcom/sec/common/widget/b;

    invoke-direct {v0, p0}, Lcom/sec/common/widget/b;-><init>(Lcom/sec/common/widget/IcsAbsSpinner;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->j:Lcom/sec/common/widget/b;

    .line 69
    invoke-direct {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->r()V

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/sec/common/widget/IcsAbsSpinner;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 37
    invoke-virtual {p0, p1, p2}, Lcom/sec/common/widget/IcsAbsSpinner;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method private r()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAbsSpinner;->setFocusable(Z)V

    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAbsSpinner;->setWillNotDraw(Z)V

    .line 94
    return-void
.end method


# virtual methods
.method a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method a()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 146
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->v:Z

    .line 147
    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->p:Z

    .line 149
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->removeAllViewsInLayout()V

    .line 150
    iput v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->C:I

    .line 151
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->D:J

    .line 153
    invoke-virtual {p0, v2}, Lcom/sec/common/widget/IcsAbsSpinner;->b(I)V

    .line 154
    invoke-virtual {p0, v2}, Lcom/sec/common/widget/IcsAbsSpinner;->c(I)V

    .line 155
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->invalidate()V

    .line 156
    return-void
.end method

.method a(IZ)V
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->C:I

    if-eq p1, v0, :cond_0

    .line 302
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->d:Z

    .line 303
    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->y:I

    sub-int v0, p1, v0

    .line 304
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/IcsAbsSpinner;->c(I)V

    .line 305
    invoke-virtual {p0, v0, p2}, Lcom/sec/common/widget/IcsAbsSpinner;->b(IZ)V

    .line 306
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->d:Z

    .line 308
    :cond_0
    return-void
.end method

.method b(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 252
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method b()V
    .locals 6

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->getChildCount()I

    move-result v1

    .line 264
    iget-object v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->j:Lcom/sec/common/widget/b;

    .line 265
    iget v3, p0, Lcom/sec/common/widget/IcsAbsSpinner;->k:I

    .line 268
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 269
    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAbsSpinner;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 270
    add-int v5, v3, v0

    .line 271
    invoke-virtual {v2, v5, v4}, Lcom/sec/common/widget/b;->a(ILandroid/view/View;)V

    .line 268
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 273
    :cond_0
    return-void
.end method

.method abstract b(IZ)V
.end method

.method public c()Landroid/view/View;
    .locals 2

    .prologue
    .line 314
    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->A:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->y:I

    if-ltz v0, :cond_0

    .line 315
    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->y:I

    iget v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->k:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAbsSpinner;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 318
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Landroid/widget/SpinnerAdapter;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->a:Landroid/widget/SpinnerAdapter;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 341
    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->A:I

    return v0
.end method

.method public synthetic f()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->d()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 257
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 168
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 172
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->getPaddingLeft()I

    move-result v0

    .line 173
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->getPaddingTop()I

    move-result v1

    .line 174
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->getPaddingRight()I

    move-result v2

    .line 175
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->getPaddingBottom()I

    move-result v3

    .line 177
    iget-object v7, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v8, p0, Lcom/sec/common/widget/IcsAbsSpinner;->e:I

    if-le v0, v8, :cond_5

    :goto_0
    iput v0, v7, Landroid/graphics/Rect;->left:I

    .line 179
    iget-object v7, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->f:I

    if-le v1, v0, :cond_6

    move v0, v1

    :goto_1
    iput v0, v7, Landroid/graphics/Rect;->top:I

    .line 181
    iget-object v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->g:I

    if-le v2, v0, :cond_7

    move v0, v2

    :goto_2
    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 183
    iget-object v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->h:I

    if-le v3, v0, :cond_8

    move v0, v3

    :goto_3
    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 186
    iget-boolean v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->v:Z

    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->n()V

    .line 194
    :cond_0
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->g()I

    move-result v1

    .line 195
    if-ltz v1, :cond_a

    iget-object v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->a:Landroid/widget/SpinnerAdapter;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->a:Landroid/widget/SpinnerAdapter;

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 197
    iget-object v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->j:Lcom/sec/common/widget/b;

    invoke-virtual {v0, v1}, Lcom/sec/common/widget/b;->a(I)Landroid/view/View;

    move-result-object v0

    .line 198
    if-nez v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->a:Landroid/widget/SpinnerAdapter;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 203
    :cond_1
    if-eqz v0, :cond_2

    .line 205
    iget-object v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->j:Lcom/sec/common/widget/b;

    invoke-virtual {v2, v1, v0}, Lcom/sec/common/widget/b;->a(ILandroid/view/View;)V

    .line 208
    :cond_2
    if-eqz v0, :cond_a

    .line 209
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-nez v1, :cond_3

    .line 210
    iput-boolean v5, p0, Lcom/sec/common/widget/IcsAbsSpinner;->d:Z

    .line 211
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    iput-boolean v4, p0, Lcom/sec/common/widget/IcsAbsSpinner;->d:Z

    .line 214
    :cond_3
    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/common/widget/IcsAbsSpinner;->measureChild(Landroid/view/View;II)V

    .line 216
    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAbsSpinner;->a(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    .line 217
    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAbsSpinner;->b(Landroid/view/View;)I

    move-result v0

    iget-object v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    move v2, v4

    .line 223
    :goto_4
    if-eqz v2, :cond_4

    .line 225
    iget-object v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    .line 226
    if-nez v6, :cond_4

    .line 227
    iget-object v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    .line 231
    :cond_4
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 232
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 234
    sget-boolean v0, Lcom/sec/common/widget/IcsAbsSpinner;->F:Z

    if-eqz v0, :cond_9

    .line 235
    invoke-static {v1, p2, v4}, Lcom/sec/common/widget/IcsAbsSpinner;->resolveSizeAndState(III)I

    move-result v0

    .line 236
    invoke-static {v2, p1, v4}, Lcom/sec/common/widget/IcsAbsSpinner;->resolveSizeAndState(III)I

    move-result v1

    .line 242
    :goto_5
    invoke-virtual {p0, v1, v0}, Lcom/sec/common/widget/IcsAbsSpinner;->setMeasuredDimension(II)V

    .line 243
    iput p2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->b:I

    .line 244
    iput p1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->c:I

    .line 245
    return-void

    .line 177
    :cond_5
    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->e:I

    goto/16 :goto_0

    .line 179
    :cond_6
    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->f:I

    goto/16 :goto_1

    .line 181
    :cond_7
    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->g:I

    goto/16 :goto_2

    .line 183
    :cond_8
    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->h:I

    goto/16 :goto_3

    .line 238
    :cond_9
    invoke-static {v1, p2}, Lcom/sec/common/widget/IcsAbsSpinner;->resolveSize(II)I

    move-result v0

    .line 239
    invoke-static {v2, p1}, Lcom/sec/common/widget/IcsAbsSpinner;->resolveSize(II)I

    move-result v1

    goto :goto_5

    :cond_a
    move v2, v5

    move v0, v4

    move v1, v4

    goto :goto_4
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 434
    check-cast p1, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;

    .line 436
    invoke-virtual {p1}, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/sec/common/widget/IcsAdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 438
    iget-wide v0, p1, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;->selectedId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 439
    iput-boolean v4, p0, Lcom/sec/common/widget/IcsAbsSpinner;->v:Z

    .line 440
    iput-boolean v4, p0, Lcom/sec/common/widget/IcsAbsSpinner;->p:Z

    .line 441
    iget-wide v0, p1, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;->selectedId:J

    iput-wide v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->n:J

    .line 442
    iget v0, p1, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;->position:I

    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->m:I

    .line 443
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->q:I

    .line 444
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->requestLayout()V

    .line 446
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    .prologue
    .line 421
    invoke-super {p0}, Lcom/sec/common/widget/IcsAdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 422
    new-instance v1, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;

    invoke-direct {v1, v0}, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 423
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->h()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;->selectedId:J

    .line 424
    iget-wide v2, v1, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;->selectedId:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    .line 425
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->g()I

    move-result v0

    iput v0, v1, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;->position:I

    .line 429
    :goto_0
    return-object v1

    .line 427
    :cond_0
    const/4 v0, -0x1

    iput v0, v1, Lcom/sec/common/widget/IcsAbsSpinner$SavedState;->position:I

    goto :goto_0
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->d:Z

    if-nez v0, :cond_0

    .line 330
    invoke-super {p0}, Lcom/sec/common/widget/IcsAdapterView;->requestLayout()V

    .line 332
    :cond_0
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0, p1}, Lcom/sec/common/widget/IcsAbsSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/SpinnerAdapter;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 104
    iget-object v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->a:Landroid/widget/SpinnerAdapter;

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->a:Landroid/widget/SpinnerAdapter;

    iget-object v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->G:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/SpinnerAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->a()V

    .line 109
    :cond_0
    iput-object p1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->a:Landroid/widget/SpinnerAdapter;

    .line 111
    iput v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->C:I

    .line 112
    const-wide/high16 v1, -0x8000000000000000L

    iput-wide v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->D:J

    .line 114
    iget-object v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->a:Landroid/widget/SpinnerAdapter;

    if-eqz v1, :cond_3

    .line 115
    iget v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->A:I

    iput v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->B:I

    .line 116
    iget-object v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->a:Landroid/widget/SpinnerAdapter;

    invoke-interface {v1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->A:I

    .line 117
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->l()V

    .line 119
    new-instance v1, Lcom/sec/common/widget/e;

    invoke-direct {v1, p0}, Lcom/sec/common/widget/e;-><init>(Lcom/sec/common/widget/IcsAdapterView;)V

    iput-object v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->G:Landroid/database/DataSetObserver;

    .line 120
    iget-object v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->a:Landroid/widget/SpinnerAdapter;

    iget-object v2, p0, Lcom/sec/common/widget/IcsAbsSpinner;->G:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/SpinnerAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 122
    iget v1, p0, Lcom/sec/common/widget/IcsAbsSpinner;->A:I

    if-lez v1, :cond_1

    const/4 v0, 0x0

    .line 124
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAbsSpinner;->b(I)V

    .line 125
    invoke-virtual {p0, v0}, Lcom/sec/common/widget/IcsAbsSpinner;->c(I)V

    .line 127
    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->A:I

    if-nez v0, :cond_2

    .line 129
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->o()V

    .line 139
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->requestLayout()V

    .line 140
    return-void

    .line 133
    :cond_3
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->l()V

    .line 134
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->a()V

    .line 136
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->o()V

    goto :goto_0
.end method

.method public setSelection(I)V
    .locals 0

    .prologue
    .line 287
    invoke-virtual {p0, p1}, Lcom/sec/common/widget/IcsAbsSpinner;->c(I)V

    .line 288
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->requestLayout()V

    .line 289
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->invalidate()V

    .line 290
    return-void
.end method

.method public setSelection(IZ)V
    .locals 2

    .prologue
    .line 280
    if-eqz p2, :cond_0

    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->k:I

    if-gt v0, p1, :cond_0

    iget v0, p0, Lcom/sec/common/widget/IcsAbsSpinner;->k:I

    invoke-virtual {p0}, Lcom/sec/common/widget/IcsAbsSpinner;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    .line 282
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/sec/common/widget/IcsAbsSpinner;->a(IZ)V

    .line 283
    return-void

    .line 280
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
