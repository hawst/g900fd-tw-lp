.class public Lcom/sec/common/widget/IcsListPopupWindow;
.super Ljava/lang/Object;
.source "IcsListPopupWindow.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/widget/PopupWindow;

.field private c:Landroid/widget/ListAdapter;

.field private d:Lcom/sec/common/widget/k;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Z

.field private j:I

.field private k:Landroid/view/View;

.field private l:I

.field private m:Landroid/database/DataSetObserver;

.field private n:Landroid/view/View;

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Landroid/widget/AdapterView$OnItemClickListener;

.field private q:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private final r:Lcom/sec/common/widget/p;

.field private final s:Lcom/sec/common/widget/o;

.field private final t:Lcom/sec/common/widget/n;

.field private final u:Lcom/sec/common/widget/l;

.field private v:Landroid/os/Handler;

.field private w:Landroid/graphics/Rect;

.field private x:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, -0x2

    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->e:I

    .line 44
    iput v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    .line 49
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->j:I

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->l:I

    .line 63
    new-instance v0, Lcom/sec/common/widget/p;

    invoke-direct {v0, p0, v1}, Lcom/sec/common/widget/p;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;Lcom/sec/common/widget/j;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->r:Lcom/sec/common/widget/p;

    .line 64
    new-instance v0, Lcom/sec/common/widget/o;

    invoke-direct {v0, p0, v1}, Lcom/sec/common/widget/o;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;Lcom/sec/common/widget/j;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->s:Lcom/sec/common/widget/o;

    .line 65
    new-instance v0, Lcom/sec/common/widget/n;

    invoke-direct {v0, p0, v1}, Lcom/sec/common/widget/n;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;Lcom/sec/common/widget/j;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->t:Lcom/sec/common/widget/n;

    .line 66
    new-instance v0, Lcom/sec/common/widget/l;

    invoke-direct {v0, p0, v1}, Lcom/sec/common/widget/l;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;Lcom/sec/common/widget/j;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->u:Lcom/sec/common/widget/l;

    .line 68
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->v:Landroid/os/Handler;

    .line 70
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    .line 82
    iput-object p1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->a:Landroid/content/Context;

    .line 83
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    .line 84
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2

    .prologue
    const/4 v0, -0x2

    const/4 v1, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->e:I

    .line 44
    iput v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    .line 49
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->j:I

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->l:I

    .line 63
    new-instance v0, Lcom/sec/common/widget/p;

    invoke-direct {v0, p0, v1}, Lcom/sec/common/widget/p;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;Lcom/sec/common/widget/j;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->r:Lcom/sec/common/widget/p;

    .line 64
    new-instance v0, Lcom/sec/common/widget/o;

    invoke-direct {v0, p0, v1}, Lcom/sec/common/widget/o;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;Lcom/sec/common/widget/j;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->s:Lcom/sec/common/widget/o;

    .line 65
    new-instance v0, Lcom/sec/common/widget/n;

    invoke-direct {v0, p0, v1}, Lcom/sec/common/widget/n;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;Lcom/sec/common/widget/j;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->t:Lcom/sec/common/widget/n;

    .line 66
    new-instance v0, Lcom/sec/common/widget/l;

    invoke-direct {v0, p0, v1}, Lcom/sec/common/widget/l;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;Lcom/sec/common/widget/j;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->u:Lcom/sec/common/widget/l;

    .line 68
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->v:Landroid/os/Handler;

    .line 70
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    .line 88
    iput-object p1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->a:Landroid/content/Context;

    .line 89
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 90
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, p1, p4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 91
    new-instance v1, Landroid/widget/PopupWindow;

    invoke-direct {v1, v0, p2, p3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    .line 95
    :goto_0
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 96
    return-void

    .line 93
    :cond_0
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    goto :goto_0
.end method

.method private a(IIIII)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 436
    iget-object v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->c:Landroid/widget/ListAdapter;

    .line 437
    if-nez v3, :cond_1

    .line 438
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v0}, Lcom/sec/common/widget/k;->getListPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v1}, Lcom/sec/common/widget/k;->getListPaddingBottom()I

    move-result v1

    add-int p4, v0, v1

    .line 486
    :cond_0
    :goto_0
    return p4

    .line 442
    :cond_1
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v0}, Lcom/sec/common/widget/k;->getListPaddingTop()I

    move-result v0

    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v2}, Lcom/sec/common/widget/k;->getListPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    .line 443
    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v2}, Lcom/sec/common/widget/k;->getDividerHeight()I

    move-result v2

    if-lez v2, :cond_5

    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v2}, Lcom/sec/common/widget/k;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v2}, Lcom/sec/common/widget/k;->getDividerHeight()I

    move-result v2

    .line 451
    :goto_1
    const/4 v4, -0x1

    if-ne p3, v4, :cond_2

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    add-int/lit8 p3, v3, -0x1

    .line 453
    :cond_2
    :goto_2
    if-gt p2, p3, :cond_8

    .line 454
    iget-object v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->c:Landroid/widget/ListAdapter;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-interface {v3, p2, v4, v5}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 455
    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v4}, Lcom/sec/common/widget/k;->getCacheColorHint()I

    move-result v4

    if-eqz v4, :cond_3

    .line 456
    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v4}, Lcom/sec/common/widget/k;->getCacheColorHint()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 459
    :cond_3
    invoke-direct {p0, v3, p2, p1}, Lcom/sec/common/widget/IcsListPopupWindow;->a(Landroid/view/View;II)V

    .line 461
    if-lez p2, :cond_4

    .line 463
    add-int/2addr v0, v2

    .line 466
    :cond_4
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v0, v3

    .line 468
    if-lt v0, p4, :cond_6

    .line 471
    if-ltz p5, :cond_0

    if-le p2, p5, :cond_0

    if-lez v1, :cond_0

    if-eq v0, p4, :cond_0

    move p4, v1

    goto :goto_0

    :cond_5
    move v2, v1

    .line 443
    goto :goto_1

    .line 479
    :cond_6
    if-ltz p5, :cond_7

    if-lt p2, p5, :cond_7

    move v1, v0

    .line 453
    :cond_7
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :cond_8
    move p4, v0

    .line 486
    goto :goto_0
.end method

.method private a(Landroid/view/View;IZ)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 409
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 410
    invoke-virtual {p1, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 412
    const/4 v0, 0x2

    new-array v2, v0, [I

    .line 413
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 415
    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 416
    if-eqz p3, :cond_0

    .line 417
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 418
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 420
    :cond_0
    aget v3, v2, v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int/2addr v0, v3

    sub-int/2addr v0, p2

    .line 421
    aget v2, v2, v5

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v2, v1

    add-int/2addr v1, p2

    .line 424
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 425
    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 426
    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 427
    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 430
    :cond_1
    return v0
.end method

.method static synthetic a(Lcom/sec/common/widget/IcsListPopupWindow;)Lcom/sec/common/widget/k;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    return-object v0
.end method

.method private a(Landroid/view/View;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 489
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$LayoutParams;

    .line 490
    if-nez v0, :cond_0

    .line 491
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(III)V

    .line 493
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 498
    :cond_0
    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v1}, Lcom/sec/common/widget/k;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v2}, Lcom/sec/common/widget/k;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/widget/AbsListView$LayoutParams;->width:I

    invoke-static {p3, v1, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 500
    iget v0, v0, Landroid/widget/AbsListView$LayoutParams;->height:I

    .line 502
    if-lez v0, :cond_1

    .line 503
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 507
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 508
    return-void

    .line 505
    :cond_1
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/common/widget/IcsListPopupWindow;)I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->j:I

    return v0
.end method

.method static synthetic c(Lcom/sec/common/widget/IcsListPopupWindow;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/common/widget/IcsListPopupWindow;)Lcom/sec/common/widget/p;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->r:Lcom/sec/common/widget/p;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/common/widget/IcsListPopupWindow;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->v:Landroid/os/Handler;

    return-object v0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/common/widget/IcsListPopupWindow;)Z
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/common/widget/IcsListPopupWindow;->f()Z

    move-result v0

    return v0
.end method

.method private g()I
    .locals 8

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 285
    .line 287
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    if-nez v0, :cond_4

    .line 288
    iget-object v5, p0, Lcom/sec/common/widget/IcsListPopupWindow;->a:Landroid/content/Context;

    .line 290
    new-instance v4, Lcom/sec/common/widget/k;

    iget-boolean v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->x:Z

    if-nez v0, :cond_3

    move v0, v2

    :goto_0
    invoke-direct {v4, v5, v0}, Lcom/sec/common/widget/k;-><init>(Landroid/content/Context;Z)V

    iput-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    .line 291
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Lcom/sec/common/widget/k;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->c:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v4}, Lcom/sec/common/widget/k;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 295
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->p:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v4}, Lcom/sec/common/widget/k;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 296
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v0, v2}, Lcom/sec/common/widget/k;->setFocusable(Z)V

    .line 297
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v0, v2}, Lcom/sec/common/widget/k;->setFocusableInTouchMode(Z)V

    .line 298
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    new-instance v4, Lcom/sec/common/widget/j;

    invoke-direct {v4, p0}, Lcom/sec/common/widget/j;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;)V

    invoke-virtual {v0, v4}, Lcom/sec/common/widget/k;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 314
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->t:Lcom/sec/common/widget/n;

    invoke-virtual {v0, v4}, Lcom/sec/common/widget/k;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 316
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->q:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->q:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v4}, Lcom/sec/common/widget/k;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    .line 322
    iget-object v6, p0, Lcom/sec/common/widget/IcsListPopupWindow;->k:Landroid/view/View;

    .line 323
    if-eqz v6, :cond_a

    .line 326
    new-instance v4, Landroid/widget/LinearLayout;

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 327
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 329
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v5, v3, v1, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 333
    iget v7, p0, Lcom/sec/common/widget/IcsListPopupWindow;->l:I

    packed-switch v7, :pswitch_data_0

    .line 350
    :goto_1
    iget v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    const/high16 v5, -0x80000000

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 352
    invoke-virtual {v6, v0, v1}, Landroid/view/View;->measure(II)V

    .line 354
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 355
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    iget v6, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v5, v6

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v5

    .line 361
    :goto_2
    iget-object v5, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v5, v4}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    move v6, v0

    .line 376
    :goto_3
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 377
    if-eqz v0, :cond_8

    .line 378
    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 379
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v4

    .line 383
    iget-boolean v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->i:Z

    if-nez v4, :cond_2

    .line 384
    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    iput v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->h:I

    :cond_2
    move v7, v0

    .line 389
    :goto_4
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_5

    .line 391
    :goto_5
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->n:Landroid/view/View;

    iget v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->h:I

    invoke-direct {p0, v0, v4, v2}, Lcom/sec/common/widget/IcsListPopupWindow;->a(Landroid/view/View;IZ)I

    move-result v0

    .line 394
    iget v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->e:I

    if-ne v2, v3, :cond_6

    .line 395
    add-int/2addr v0, v7

    .line 405
    :goto_6
    return v0

    :cond_3
    move v0, v1

    .line 290
    goto/16 :goto_0

    .line 335
    :pswitch_0
    invoke-virtual {v4, v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 336
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 340
    :pswitch_1
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 341
    invoke-virtual {v4, v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 363
    :cond_4
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 364
    iget-object v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->k:Landroid/view/View;

    .line 365
    if-eqz v4, :cond_9

    .line 366
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 368
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iget v5, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v4, v5

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v4

    move v6, v0

    goto :goto_3

    :cond_5
    move v2, v1

    .line 389
    goto :goto_5

    .line 398
    :cond_6
    sub-int v4, v0, v6

    move-object v0, p0

    move v2, v1

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/common/widget/IcsListPopupWindow;->a(IIIII)I

    move-result v0

    .line 402
    if-lez v0, :cond_7

    add-int/2addr v6, v7

    .line 405
    :cond_7
    add-int/2addr v0, v6

    goto :goto_6

    :cond_8
    move v7, v1

    goto :goto_4

    :cond_9
    move v6, v1

    goto :goto_3

    :cond_a
    move-object v4, v0

    move v0, v1

    goto/16 :goto_2

    .line 333
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x2

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 155
    invoke-direct {p0}, Lcom/sec/common/widget/IcsListPopupWindow;->g()I

    move-result v5

    .line 160
    invoke-direct {p0}, Lcom/sec/common/widget/IcsListPopupWindow;->f()Z

    move-result v2

    .line 163
    iget-object v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 164
    iget v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    if-ne v3, v0, :cond_2

    move v4, v0

    .line 174
    :goto_0
    iget v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->e:I

    if-ne v3, v0, :cond_8

    .line 177
    if-eqz v2, :cond_4

    .line 178
    :goto_1
    if-eqz v2, :cond_6

    .line 179
    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    iget v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    if-ne v3, v0, :cond_5

    :goto_2
    invoke-virtual {v2, v0, v1}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 194
    :cond_0
    :goto_3
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v7}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 196
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->n:Landroid/view/View;

    iget v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->g:I

    iget v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->h:I

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    .line 237
    :cond_1
    :goto_4
    return-void

    .line 168
    :cond_2
    iget v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    if-ne v3, v6, :cond_3

    .line 169
    iget-object v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->n:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v4

    goto :goto_0

    .line 171
    :cond_3
    iget v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    goto :goto_0

    :cond_4
    move v5, v0

    .line 177
    goto :goto_1

    :cond_5
    move v0, v1

    .line 179
    goto :goto_2

    .line 183
    :cond_6
    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    iget v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    if-ne v3, v0, :cond_7

    move v1, v0

    :cond_7
    invoke-virtual {v2, v1, v0}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    goto :goto_3

    .line 188
    :cond_8
    iget v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->e:I

    if-eq v0, v6, :cond_0

    .line 191
    iget v5, p0, Lcom/sec/common/widget/IcsListPopupWindow;->e:I

    goto :goto_3

    .line 199
    :cond_9
    iget v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    if-ne v2, v0, :cond_c

    move v2, v0

    .line 209
    :goto_5
    iget v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->e:I

    if-ne v3, v0, :cond_e

    move v1, v0

    .line 219
    :goto_6
    iget-object v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v2, v1}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 224
    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v7}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 225
    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->s:Lcom/sec/common/widget/o;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 226
    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->n:Landroid/view/View;

    iget v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->g:I

    iget v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->h:I

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 228
    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v1, v0}, Lcom/sec/common/widget/k;->setSelection(I)V

    .line 230
    iget-boolean v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->x:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    invoke-virtual {v0}, Lcom/sec/common/widget/k;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 231
    :cond_a
    invoke-virtual {p0}, Lcom/sec/common/widget/IcsListPopupWindow;->c()V

    .line 233
    :cond_b
    iget-boolean v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->x:Z

    if-nez v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->v:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->u:Lcom/sec/common/widget/l;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_4

    .line 202
    :cond_c
    iget v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    if-ne v2, v6, :cond_d

    .line 203
    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->n:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v2, v1

    goto :goto_5

    .line 205
    :cond_d
    iget-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    iget v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v2, v1

    goto :goto_5

    .line 212
    :cond_e
    iget v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->e:I

    if-ne v3, v6, :cond_f

    .line 213
    iget-object v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto :goto_6

    .line 215
    :cond_f
    iget-object v3, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    iget v4, p0, Lcom/sec/common/widget/IcsListPopupWindow;->e:I

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto :goto_6
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->l:I

    .line 116
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 125
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->n:Landroid/view/View;

    .line 129
    return-void
.end method

.method public a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->p:Landroid/widget/AdapterView$OnItemClickListener;

    .line 152
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->m:Landroid/database/DataSetObserver;

    if-nez v0, :cond_3

    .line 100
    new-instance v0, Lcom/sec/common/widget/m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/common/widget/m;-><init>(Lcom/sec/common/widget/IcsListPopupWindow;Lcom/sec/common/widget/j;)V

    iput-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->m:Landroid/database/DataSetObserver;

    .line 104
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->c:Landroid/widget/ListAdapter;

    .line 105
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->c:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->m:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->c:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/common/widget/k;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 112
    :cond_2
    return-void

    .line 101
    :cond_3
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->c:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->c:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->m:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->x:Z

    .line 120
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 121
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 240
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 241
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 243
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 244
    check-cast v0, Landroid/view/ViewGroup;

    .line 245
    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 249
    iput-object v2, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    .line 250
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->v:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->r:Lcom/sec/common/widget/p;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 251
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->g:I

    .line 133
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    .line 263
    if-eqz v0, :cond_0

    .line 265
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/common/widget/k;->a(Lcom/sec/common/widget/k;Z)Z

    .line 267
    invoke-virtual {v0}, Lcom/sec/common/widget/k;->requestLayout()V

    .line 269
    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->h:I

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->i:Z

    .line 138
    return-void
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 142
    if-eqz v0, :cond_0

    .line 143
    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 144
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->w:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_0
    iput p1, p0, Lcom/sec/common/widget/IcsListPopupWindow;->f:I

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public e()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->d:Lcom/sec/common/widget/k;

    return-object v0
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/common/widget/IcsListPopupWindow;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 259
    return-void
.end method
