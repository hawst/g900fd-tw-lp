.class public final Lcom/sec/common/f/c;
.super Ljava/lang/Object;
.source "ResourceDispatcher.java"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field protected final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;>;"
        }
    .end annotation
.end field

.field protected final b:Lcom/sec/common/f/e;

.field private final d:I

.field private final e:Ljava/lang/Object;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:Lcom/sec/common/b/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/common/b/a",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/sec/common/f/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/common/f/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/common/f/c;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lcom/sec/common/b/b;

    invoke-direct {v0}, Lcom/sec/common/b/b;-><init>()V

    new-instance v1, Lcom/sec/common/f/d;

    invoke-direct {v1}, Lcom/sec/common/f/d;-><init>()V

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/common/f/c;-><init>(Lcom/sec/common/b/b;Ljava/util/concurrent/ExecutorService;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Lcom/sec/common/b/b;Ljava/util/concurrent/ExecutorService;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/common/b/b",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/16 v0, 0x3c

    iput v0, p0, Lcom/sec/common/f/c;->d:I

    .line 110
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/common/f/c;->e:Ljava/lang/Object;

    .line 112
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    .line 113
    iput-object p1, p0, Lcom/sec/common/f/c;->g:Lcom/sec/common/b/a;

    .line 115
    iput-object p2, p0, Lcom/sec/common/f/c;->f:Ljava/util/concurrent/ExecutorService;

    .line 117
    new-instance v0, Lcom/sec/common/f/e;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lcom/sec/common/f/e;-><init>(Lcom/sec/common/f/c;Lcom/sec/common/f/c;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/common/f/c;->b:Lcom/sec/common/f/e;

    .line 118
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/sec/common/b/b;

    invoke-direct {v0}, Lcom/sec/common/b/b;-><init>()V

    invoke-direct {p0, v0, p1}, Lcom/sec/common/f/c;-><init>(Lcom/sec/common/b/b;Ljava/util/concurrent/ExecutorService;)V

    .line 83
    return-void
.end method

.method static synthetic a(Lcom/sec/common/f/c;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/common/f/c;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/common/f/c;Lcom/sec/common/f/a;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/common/f/c;->c(Lcom/sec/common/f/a;)V

    return-void
.end method

.method private a(Ljava/util/concurrent/Future;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 262
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    const/4 v0, 0x1

    .line 266
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/common/f/c;)Lcom/sec/common/b/a;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/common/f/c;->g:Lcom/sec/common/b/a;

    return-object v0
.end method

.method private b(Lcom/sec/common/f/a;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/common/f/a",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 270
    iget-object v1, p0, Lcom/sec/common/f/c;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 271
    :try_start_0
    iget-object v0, p0, Lcom/sec/common/f/c;->g:Lcom/sec/common/b/a;

    invoke-virtual {p1}, Lcom/sec/common/f/a;->i()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/sec/common/b/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/common/f/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/common/f/c;)Lcom/sec/common/f/f;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/common/f/c;->h:Lcom/sec/common/f/f;

    return-object v0
.end method

.method private c(Lcom/sec/common/f/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 276
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/common/f/c;->b(Lcom/sec/common/f/a;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 277
    invoke-virtual {p1}, Lcom/sec/common/f/a;->d()V

    .line 279
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Lcom/sec/common/f/a;)Lcom/sec/common/f/a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/sec/common/f/a",
            "<*>;)",
            "Lcom/sec/common/f/a",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v1, p0, Lcom/sec/common/f/c;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    :try_start_0
    invoke-virtual {p2, p0}, Lcom/sec/common/f/a;->a(Lcom/sec/common/f/c;)V

    .line 129
    invoke-virtual {p2, p1}, Lcom/sec/common/f/a;->a(Landroid/view/View;)V

    .line 131
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/f/a;

    .line 134
    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {v0}, Lcom/sec/common/f/a;->l()Ljava/util/concurrent/Future;

    move-result-object v2

    .line 137
    invoke-virtual {v0, p2}, Lcom/sec/common/f/a;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object p2, v0

    .line 148
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/sec/common/f/a;->l()Ljava/util/concurrent/Future;

    move-result-object v0

    if-nez v0, :cond_1

    .line 149
    invoke-direct {p0, p2}, Lcom/sec/common/f/c;->b(Lcom/sec/common/f/a;)Ljava/lang/Object;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_4

    .line 152
    invoke-virtual {p2, v0}, Lcom/sec/common/f/a;->a(Ljava/lang/Object;)V

    .line 153
    const/4 v2, 0x1

    invoke-virtual {p2, v2}, Lcom/sec/common/f/a;->a(Z)V

    .line 155
    const/4 v2, 0x1

    invoke-virtual {p2, v0, v2}, Lcom/sec/common/f/a;->a(Ljava/lang/Object;Z)V

    .line 157
    iget-object v0, p0, Lcom/sec/common/f/c;->h:Lcom/sec/common/f/f;

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/sec/common/f/c;->h:Lcom/sec/common/f/f;

    invoke-interface {v0, p1, p2}, Lcom/sec/common/f/f;->b(Landroid/view/View;Lcom/sec/common/f/a;)V

    .line 166
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    monitor-exit v1

    return-object p2

    .line 140
    :cond_2
    invoke-direct {p0, v2}, Lcom/sec/common/f/c;->a(Ljava/util/concurrent/Future;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 141
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 143
    :cond_3
    :try_start_1
    invoke-direct {p0, v0}, Lcom/sec/common/f/c;->c(Lcom/sec/common/f/a;)V

    goto :goto_0

    .line 161
    :cond_4
    invoke-virtual {p2}, Lcom/sec/common/f/a;->b()V

    .line 162
    iget-object v0, p0, Lcom/sec/common/f/c;->f:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/sec/common/f/a;->a(Ljava/util/concurrent/Future;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public a()V
    .locals 5

    .prologue
    .line 197
    iget-object v2, p0, Lcom/sec/common/f/c;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 199
    :try_start_0
    iget-object v0, p0, Lcom/sec/common/f/c;->f:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 201
    iget-object v0, p0, Lcom/sec/common/f/c;->f:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v3, 0x3c

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v3, v4, v1}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->d:Z

    if-eqz v0, :cond_0

    .line 203
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/common/f/c;->c:Ljava/lang/String;

    const-string v3, "Thread pool isn\'t terminated."

    invoke-virtual {v0, v1, v3}, Lcom/sec/common/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 211
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 214
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/f/a;

    .line 217
    invoke-virtual {v0}, Lcom/sec/common/f/a;->d()V

    goto :goto_0

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 221
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 222
    iget-object v0, p0, Lcom/sec/common/f/c;->g:Lcom/sec/common/b/a;

    invoke-interface {v0}, Lcom/sec/common/b/a;->a()V

    .line 224
    :goto_1
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 225
    return-void

    .line 206
    :catch_0
    move-exception v0

    .line 207
    :try_start_3
    iget-object v0, p0, Lcom/sec/common/f/c;->f:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 209
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 211
    :try_start_4
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 212
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 214
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/f/a;

    .line 217
    invoke-virtual {v0}, Lcom/sec/common/f/a;->d()V

    goto :goto_2

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 222
    iget-object v0, p0, Lcom/sec/common/f/c;->g:Lcom/sec/common/b/a;

    invoke-interface {v0}, Lcom/sec/common/b/a;->a()V

    goto :goto_1

    .line 211
    :catchall_1
    move-exception v0

    move-object v1, v0

    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 212
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 214
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 215
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/f/a;

    .line 217
    invoke-virtual {v0}, Lcom/sec/common/f/a;->d()V

    goto :goto_3

    .line 221
    :cond_3
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 222
    iget-object v0, p0, Lcom/sec/common/f/c;->g:Lcom/sec/common/b/a;

    invoke-interface {v0}, Lcom/sec/common/b/a;->a()V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 178
    iget-object v1, p0, Lcom/sec/common/f/c;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/f/a;

    .line 180
    if-nez v0, :cond_0

    .line 181
    monitor-exit v1

    .line 191
    :goto_0
    return-void

    .line 184
    :cond_0
    invoke-virtual {v0}, Lcom/sec/common/f/a;->l()Ljava/util/concurrent/Future;

    move-result-object v2

    .line 185
    invoke-direct {p0, v2}, Lcom/sec/common/f/c;->a(Ljava/util/concurrent/Future;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 186
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 190
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 188
    :cond_1
    :try_start_1
    invoke-direct {p0, v0}, Lcom/sec/common/f/c;->c(Lcom/sec/common/f/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method a(Lcom/sec/common/f/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/common/f/a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 282
    invoke-virtual {p1}, Lcom/sec/common/f/a;->m()Z

    move-result v0

    .line 284
    iget-object v1, p0, Lcom/sec/common/f/c;->b:Lcom/sec/common/f/e;

    invoke-static {v1, p1, v0}, Lcom/sec/common/f/e;->a(Lcom/sec/common/f/e;Lcom/sec/common/f/a;Z)V

    .line 285
    return-void
.end method

.method a(Lcom/sec/common/f/a;Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/common/f/a",
            "<*>;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/common/f/c;->b:Lcom/sec/common/f/e;

    invoke-virtual {v0, p1, p2}, Lcom/sec/common/f/e;->a(Lcom/sec/common/f/a;Ljava/util/concurrent/Callable;)V

    .line 289
    return-void
.end method

.method public a(Lcom/sec/common/f/f;)V
    .locals 2

    .prologue
    .line 233
    iget-object v1, p0, Lcom/sec/common/f/c;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 234
    :try_start_0
    iput-object p1, p0, Lcom/sec/common/f/c;->h:Lcom/sec/common/f/f;

    .line 235
    monitor-exit v1

    .line 236
    return-void

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
