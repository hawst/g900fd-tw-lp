.class public Lcom/sec/common/f/e;
.super Landroid/os/Handler;
.source "ResourceDispatcher.java"


# instance fields
.field final synthetic a:Lcom/sec/common/f/c;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Lcom/sec/common/f/c;


# direct methods
.method constructor <init>(Lcom/sec/common/f/c;Lcom/sec/common/f/c;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 298
    iput-object p1, p0, Lcom/sec/common/f/e;->a:Lcom/sec/common/f/c;

    .line 299
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 292
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/common/f/e;->b:I

    .line 293
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/common/f/e;->c:I

    .line 294
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/common/f/e;->d:I

    .line 301
    iput-object p2, p0, Lcom/sec/common/f/e;->e:Lcom/sec/common/f/c;

    .line 302
    return-void
.end method

.method private a(Lcom/sec/common/f/a;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/common/f/a",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 368
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/common/f/e;->removeMessages(I)V

    .line 370
    if-eqz p2, :cond_0

    .line 371
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/common/f/e;->sendMessage(Landroid/os/Message;)Z

    .line 375
    :goto_0
    return-void

    .line 373
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/common/f/e;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/common/f/e;Lcom/sec/common/f/a;Z)V
    .locals 0

    .prologue
    .line 291
    invoke-direct {p0, p1, p2}, Lcom/sec/common/f/e;->a(Lcom/sec/common/f/a;Z)V

    return-void
.end method


# virtual methods
.method a(Lcom/sec/common/f/a;Ljava/util/concurrent/Callable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/common/f/a",
            "<*>;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 357
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/common/f/e;->a(Lcom/sec/common/f/a;Ljava/util/concurrent/Callable;J)V

    .line 358
    return-void
.end method

.method a(Lcom/sec/common/f/a;Ljava/util/concurrent/Callable;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/common/f/a",
            "<*>;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 362
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/common/f/e;->removeMessages(I)V

    .line 364
    const/16 v0, 0x64

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {p0, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, p3, p4}, Lcom/sec/common/f/e;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 365
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/16 v5, 0x64

    .line 307
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 309
    iget-object v0, p0, Lcom/sec/common/f/e;->e:Lcom/sec/common/f/c;

    invoke-static {v0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/c;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 311
    const/4 v1, 0x0

    .line 313
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v5, :cond_2

    .line 314
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 315
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Lcom/sec/common/f/a;

    .line 316
    const/4 v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Ljava/util/concurrent/Callable;

    move-object v2, v1

    move-object v1, v0

    .line 321
    :goto_0
    iget-object v0, p0, Lcom/sec/common/f/e;->e:Lcom/sec/common/f/c;

    iget-object v0, v0, Lcom/sec/common/f/c;->a:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/common/f/a;

    .line 323
    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Lcom/sec/common/f/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/sec/common/f/e;->a:Lcom/sec/common/f/c;

    invoke-static {v0, v2}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/c;Lcom/sec/common/f/a;)V

    .line 353
    :cond_1
    :goto_1
    monitor-exit v3

    .line 354
    return-void

    .line 318
    :cond_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/common/f/a;

    move-object v2, v0

    goto :goto_0

    .line 326
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v5, :cond_4

    .line 328
    :try_start_1
    invoke-interface {v1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 329
    :catch_0
    move-exception v0

    .line 330
    :try_start_2
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->d:Z

    if-eqz v1, :cond_1

    .line 331
    new-instance v1, Ljava/io/PrintWriter;

    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    invoke-direct {v1, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 332
    invoke-virtual {v0, v1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 334
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    invoke-static {}, Lcom/sec/common/f/c;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "Callable.call() method throws exception."

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 338
    :cond_4
    :try_start_3
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_5

    .line 339
    iget-object v0, p0, Lcom/sec/common/f/e;->a:Lcom/sec/common/f/c;

    invoke-static {v0}, Lcom/sec/common/f/c;->b(Lcom/sec/common/f/c;)Lcom/sec/common/b/a;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/common/f/a;->i()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Lcom/sec/common/b/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 342
    :cond_5
    iget-object v0, p0, Lcom/sec/common/f/e;->a:Lcom/sec/common/f/c;

    invoke-static {v0}, Lcom/sec/common/f/c;->c(Lcom/sec/common/f/c;)Lcom/sec/common/f/f;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 343
    invoke-virtual {v2}, Lcom/sec/common/f/a;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 344
    iget-object v0, p0, Lcom/sec/common/f/e;->a:Lcom/sec/common/f/c;

    invoke-static {v0}, Lcom/sec/common/f/c;->c(Lcom/sec/common/f/c;)Lcom/sec/common/f/f;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lcom/sec/common/f/f;->b(Landroid/view/View;Lcom/sec/common/f/a;)V

    .line 350
    :cond_6
    :goto_2
    invoke-virtual {v2}, Lcom/sec/common/f/a;->g()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Lcom/sec/common/f/a;->a(Ljava/lang/Object;Z)V

    goto :goto_1

    .line 346
    :cond_7
    iget-object v0, p0, Lcom/sec/common/f/e;->a:Lcom/sec/common/f/c;

    invoke-static {v0}, Lcom/sec/common/f/c;->c(Lcom/sec/common/f/c;)Lcom/sec/common/f/f;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/common/f/a;->h()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lcom/sec/common/f/f;->a(Landroid/view/View;Lcom/sec/common/f/a;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method
