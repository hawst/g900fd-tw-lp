.class public abstract Lcom/sec/common/f/a;
.super Ljava/lang/Object;
.source "AbstractResourceDispatcherTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TK:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Lcom/sec/common/f/c;

.field protected f:Landroid/view/View;

.field protected g:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTK;"
        }
    .end annotation
.end field

.field protected h:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTK;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/common/f/a;->g:Ljava/lang/Object;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/common/f/a;->f:Landroid/view/View;

    .line 44
    return-void
.end method

.method public a(Lcom/sec/common/f/c;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/common/f/a;->c:Lcom/sec/common/f/c;

    .line 60
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/common/f/a;->h:Ljava/lang/Object;

    .line 56
    return-void
.end method

.method public abstract a(Ljava/lang/Object;Z)V
.end method

.method protected a(Ljava/util/concurrent/Callable;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/common/f/a;->c:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0, p1}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/a;Ljava/util/concurrent/Callable;)V

    .line 116
    return-void
.end method

.method public a(Ljava/util/concurrent/Future;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/common/f/a;->a:Ljava/util/concurrent/Future;

    .line 72
    return-void
.end method

.method a(Z)V
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/sec/common/f/a;->b:Z

    .line 80
    return-void
.end method

.method public abstract b()V
.end method

.method public abstract c()Ljava/lang/Object;
.end method

.method public abstract d()V
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 120
    instance-of v0, p1, Lcom/sec/common/f/a;

    if-eqz v0, :cond_0

    .line 121
    check-cast p1, Lcom/sec/common/f/a;

    .line 123
    invoke-virtual {p0}, Lcom/sec/common/f/a;->i()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/common/f/a;->i()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    const/4 v0, 0x1

    .line 128
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/f/a;->h:Ljava/lang/Object;

    return-object v0
.end method

.method public h()Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/common/f/a;->f:Landroid/view/View;

    return-object v0
.end method

.method public i()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTK;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/common/f/a;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public k()Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/common/f/a;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/common/f/a;->a:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sec/common/f/a;->b:Z

    return v0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 97
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/common/f/a;->c()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/f/a;->h:Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lcom/sec/common/f/a;->h:Ljava/lang/Object;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/common/f/a;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/common/f/a;->b:Z

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/common/f/a;->c:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 106
    iget-object v0, p0, Lcom/sec/common/f/a;->c:Lcom/sec/common/f/c;

    invoke-virtual {v0, p0}, Lcom/sec/common/f/c;->a(Lcom/sec/common/f/a;)V

    goto :goto_0
.end method
