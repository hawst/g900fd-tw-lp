.class public Lcom/sec/common/e/b;
.super Ljava/lang/Object;
.source "Picasso.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/common/e/a;

.field private c:Lcom/sec/common/e/a;

.field private d:Lcom/sec/common/e/a;

.field private e:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/common/e/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/common/e/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    iget-object v2, p0, Lcom/sec/common/e/b;->e:Ljava/io/File;

    if-nez v2, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "File SHOULD be not null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    iget-object v2, p0, Lcom/sec/common/e/b;->b:Lcom/sec/common/e/a;

    if-nez v2, :cond_1

    .line 95
    new-instance v2, Lcom/sec/common/e/c;

    invoke-static {}, Lcom/sec/common/util/i;->b()I

    move-result v3

    invoke-static {}, Lcom/sec/common/util/i;->c()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/sec/common/e/c;-><init>(II)V

    iput-object v2, p0, Lcom/sec/common/e/b;->b:Lcom/sec/common/e/a;

    .line 98
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 100
    iget-object v4, p0, Lcom/sec/common/e/b;->b:Lcom/sec/common/e/a;

    iget-object v5, p0, Lcom/sec/common/e/b;->e:Ljava/io/File;

    invoke-interface {v4, v5, v0}, Lcom/sec/common/e/a;->a(Ljava/io/File;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 102
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Picasso draw(). resize: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v2, v5, v2

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget-object v2, p0, Lcom/sec/common/e/b;->c:Lcom/sec/common/e/a;

    if-eqz v2, :cond_2

    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 108
    iget-object v4, p0, Lcom/sec/common/e/b;->c:Lcom/sec/common/e/a;

    iget-object v5, p0, Lcom/sec/common/e/b;->e:Ljava/io/File;

    invoke-interface {v4, v5, v0}, Lcom/sec/common/e/a;->a(Ljava/io/File;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 110
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", adjust: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v2, v5, v2

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_2
    iget-object v2, p0, Lcom/sec/common/e/b;->d:Lcom/sec/common/e/a;

    if-eqz v2, :cond_3

    .line 115
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 117
    iget-object v4, p0, Lcom/sec/common/e/b;->d:Lcom/sec/common/e/a;

    iget-object v5, p0, Lcom/sec/common/e/b;->e:Ljava/io/File;

    invoke-interface {v4, v5, v0}, Lcom/sec/common/e/a;->a(Ljava/io/File;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 119
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", rotate: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v2, v5, v2

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    :cond_3
    sget-object v2, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v2, v2, Lcom/sec/common/c/a;->b:Z

    if-eqz v2, :cond_4

    .line 123
    sget-object v2, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v3, Lcom/sec/common/e/b;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_4
    return-object v0
.end method

.method public a(II)Lcom/sec/common/e/b;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/sec/common/e/c;

    invoke-direct {v0, p1, p2}, Lcom/sec/common/e/c;-><init>(II)V

    iput-object v0, p0, Lcom/sec/common/e/b;->b:Lcom/sec/common/e/a;

    .line 64
    return-object p0
.end method

.method public a(Ljava/io/File;)Lcom/sec/common/e/b;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/common/e/b;->e:Ljava/io/File;

    .line 58
    return-object p0
.end method
