.class Lcom/sec/common/e/c;
.super Ljava/lang/Object;
.source "ResizeCommand.java"

# interfaces
.implements Lcom/sec/common/e/a;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:I

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/common/e/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/common/e/c;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(II)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput p1, p0, Lcom/sec/common/e/c;->b:I

    .line 28
    iput p2, p0, Lcom/sec/common/e/c;->c:I

    .line 29
    return-void
.end method


# virtual methods
.method public a(Ljava/io/File;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 33
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    .line 35
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 36
    iput-boolean v6, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 37
    iput v6, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 40
    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 42
    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    shr-int/lit8 v1, v0, 0x1

    .line 43
    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    shr-int/lit8 v0, v0, 0x1

    .line 45
    :goto_0
    iget v4, p0, Lcom/sec/common/e/c;->b:I

    if-le v1, v4, :cond_0

    iget v4, p0, Lcom/sec/common/e/c;->c:I

    if-le v0, v4, :cond_0

    .line 46
    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    shl-int/lit8 v4, v4, 0x1

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 48
    shr-int/lit8 v1, v1, 0x1

    .line 49
    shr-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_0
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v0, v0, Lcom/sec/common/c/a;->b:Z

    if-eqz v0, :cond_1

    .line 53
    sget-object v0, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v1, Lcom/sec/common/e/c;->a:Ljava/lang/String;

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "Sampling size: "

    aput-object v5, v4, v7

    iget v5, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v4}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :cond_1
    iput-boolean v7, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 57
    iput-boolean v6, v3, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 58
    iput-boolean v6, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 60
    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 62
    if-nez v0, :cond_2

    .line 63
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t decode file"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_2
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    iget-boolean v1, v1, Lcom/sec/common/c/a;->b:Z

    if-eqz v1, :cond_3

    .line 67
    sget-object v1, Lcom/sec/common/f;->a:Lcom/sec/common/c/a;

    sget-object v2, Lcom/sec/common/e/c;->a:Ljava/lang/String;

    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Request(Width: "

    aput-object v4, v3, v7

    iget v4, p0, Lcom/sec/common/e/c;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, ", Height: "

    aput-object v4, v3, v8

    const/4 v4, 0x3

    iget v5, p0, Lcom/sec/common/e/c;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "), Response(Width: "

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, ", Height: "

    aput-object v5, v3, v4

    const/4 v4, 0x7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, ")"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/sec/common/util/o;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_3
    return-object v0
.end method
