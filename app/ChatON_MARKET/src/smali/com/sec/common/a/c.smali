.class public Lcom/sec/common/a/c;
.super Lcom/sec/common/a/a;
.source "AlertDialogBuilderWrapper.java"


# instance fields
.field private a:Landroid/app/AlertDialog$Builder;

.field private b:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/common/a/a;-><init>(Landroid/content/Context;)V

    .line 31
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    .line 32
    return-void
.end method


# virtual methods
.method public a(I)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 70
    return-object p0
.end method

.method public a(IILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2, p3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 154
    return-object p0
.end method

.method public a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 126
    return-object p0
.end method

.method public a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 254
    return-object p0
.end method

.method public a(Landroid/view/View;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 77
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 63
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 2

    .prologue
    .line 190
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 197
    :goto_0
    return-object p0

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public a(Z)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 185
    return-object p0
.end method

.method public a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2, p3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 140
    return-object p0
.end method

.method public a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 133
    return-object p0
.end method

.method public a()Lcom/sec/common/a/d;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 42
    new-instance v0, Lcom/sec/common/a/m;

    iget-object v1, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/common/a/m;-><init>(Landroid/app/AlertDialog;)V

    .line 44
    iget-object v1, p0, Lcom/sec/common/a/c;->b:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/sec/common/a/c;->b:Landroid/view/View;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v5}, Lcom/sec/common/a/d;->a(Landroid/view/View;IIII)V

    .line 48
    :cond_0
    return-object v0
.end method

.method public b(I)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 91
    return-object p0
.end method

.method public b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 2

    .prologue
    .line 202
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 209
    :goto_0
    return-object p0

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public b(Landroid/view/View;)Lcom/sec/common/a/a;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/common/a/c;->b:Landroid/view/View;

    .line 98
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 84
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 2

    .prologue
    .line 228
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 235
    :goto_0
    return-object p0

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public b(Z)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 268
    return-object p0
.end method

.method public b()Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/sec/common/a/c;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 54
    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 56
    return-object v0
.end method

.method public c(I)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 112
    return-object p0
.end method

.method public c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 223
    return-object p0
.end method

.method public d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 2

    .prologue
    .line 240
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 247
    :goto_0
    return-object p0

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/sec/common/a/c;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method
