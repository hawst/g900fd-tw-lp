.class Lcom/sec/common/a/b;
.super Lcom/sec/common/a/a;
.source "AlertDialogBuilderImpl.java"


# instance fields
.field A:Landroid/content/DialogInterface$OnClickListener;

.field B:Landroid/content/DialogInterface$OnCancelListener;

.field a:Landroid/content/Context;

.field b:I

.field c:Z

.field d:[Ljava/lang/CharSequence;

.field e:Landroid/widget/ListAdapter;

.field f:Landroid/database/Cursor;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:I

.field j:[Z

.field k:Z

.field l:Z

.field m:Landroid/content/DialogInterface$OnClickListener;

.field n:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

.field o:Landroid/widget/AdapterView$OnItemSelectedListener;

.field p:Landroid/content/DialogInterface$OnKeyListener;

.field q:Z

.field r:Ljava/lang/CharSequence;

.field s:Landroid/view/View;

.field t:Ljava/lang/CharSequence;

.field u:Landroid/view/View;

.field v:Ljava/lang/CharSequence;

.field w:Landroid/content/DialogInterface$OnClickListener;

.field x:Ljava/lang/CharSequence;

.field y:Landroid/content/DialogInterface$OnClickListener;

.field z:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 65
    invoke-static {p1}, Lcom/sec/common/a/b;->b(Landroid/content/Context;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/sec/common/a/b;-><init>(Landroid/content/Context;I)V

    .line 66
    return-void
.end method

.method constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/sec/common/a/a;-><init>(Landroid/content/Context;I)V

    .line 71
    iput-object p1, p0, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    .line 72
    iput p2, p0, Lcom/sec/common/a/b;->b:I

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/common/a/b;->q:Z

    .line 74
    return-void
.end method

.method private static b(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 77
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 78
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lcom/sec/common/b;->alertDialogTheme:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 79
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    return v0
.end method


# virtual methods
.method public a(I)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/a/b;->r:Ljava/lang/CharSequence;

    .line 113
    return-object p0
.end method

.method public a(IILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/a/b;->d:[Ljava/lang/CharSequence;

    .line 203
    iput p2, p0, Lcom/sec/common/a/b;->i:I

    .line 204
    iput-object p3, p0, Lcom/sec/common/a/b;->m:Landroid/content/DialogInterface$OnClickListener;

    .line 206
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/common/a/b;->k:Z

    .line 208
    return-object p0
.end method

.method public a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/a/b;->d:[Ljava/lang/CharSequence;

    .line 165
    iput-object p2, p0, Lcom/sec/common/a/b;->m:Landroid/content/DialogInterface$OnClickListener;

    .line 167
    return-object p0
.end method

.method public a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/sec/common/a/a;
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/sec/common/a/b;->B:Landroid/content/DialogInterface$OnCancelListener;

    .line 340
    return-object p0
.end method

.method public a(Landroid/view/View;)Lcom/sec/common/a/a;
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/common/a/b;->s:Landroid/view/View;

    .line 120
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/common/a/b;->r:Ljava/lang/CharSequence;

    .line 106
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 2

    .prologue
    .line 266
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 267
    iput-object p1, p0, Lcom/sec/common/a/b;->z:Ljava/lang/CharSequence;

    .line 268
    iput-object p2, p0, Lcom/sec/common/a/b;->A:Landroid/content/DialogInterface$OnClickListener;

    .line 275
    :goto_0
    return-object p0

    .line 271
    :cond_0
    iput-object p1, p0, Lcom/sec/common/a/b;->v:Ljava/lang/CharSequence;

    .line 272
    iput-object p2, p0, Lcom/sec/common/a/b;->w:Landroid/content/DialogInterface$OnClickListener;

    goto :goto_0
.end method

.method public a(Z)Lcom/sec/common/a/a;
    .locals 0

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/sec/common/a/b;->c:Z

    .line 261
    return-object p0
.end method

.method public a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/common/a/b;->d:[Ljava/lang/CharSequence;

    .line 181
    iput p2, p0, Lcom/sec/common/a/b;->i:I

    .line 182
    iput-object p3, p0, Lcom/sec/common/a/b;->m:Landroid/content/DialogInterface$OnClickListener;

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/common/a/b;->k:Z

    .line 186
    return-object p0
.end method

.method public a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/sec/common/a/b;->d:[Ljava/lang/CharSequence;

    .line 173
    iput-object p2, p0, Lcom/sec/common/a/b;->m:Landroid/content/DialogInterface$OnClickListener;

    .line 175
    return-object p0
.end method

.method public a()Lcom/sec/common/a/d;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 84
    new-instance v0, Lcom/sec/common/a/e;

    invoke-direct {v0, p0}, Lcom/sec/common/a/e;-><init>(Lcom/sec/common/a/b;)V

    .line 85
    invoke-virtual {v0}, Lcom/sec/common/a/e;->c()V

    .line 87
    iget-object v1, p0, Lcom/sec/common/a/b;->u:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/sec/common/a/b;->u:Landroid/view/View;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/common/a/e;->a(Landroid/view/View;IIII)V

    .line 91
    :cond_0
    return-object v0
.end method

.method public b(I)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/a/b;->t:Ljava/lang/CharSequence;

    .line 134
    return-object p0
.end method

.method public b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 2

    .prologue
    .line 280
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/a/b;->z:Ljava/lang/CharSequence;

    .line 282
    iput-object p2, p0, Lcom/sec/common/a/b;->A:Landroid/content/DialogInterface$OnClickListener;

    .line 289
    :goto_0
    return-object p0

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/a/b;->v:Ljava/lang/CharSequence;

    .line 286
    iput-object p2, p0, Lcom/sec/common/a/b;->w:Landroid/content/DialogInterface$OnClickListener;

    goto :goto_0
.end method

.method public b(Landroid/view/View;)Lcom/sec/common/a/a;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/common/a/b;->u:Landroid/view/View;

    .line 141
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/common/a/b;->t:Ljava/lang/CharSequence;

    .line 127
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 2

    .prologue
    .line 310
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 311
    iput-object p1, p0, Lcom/sec/common/a/b;->v:Ljava/lang/CharSequence;

    .line 312
    iput-object p2, p0, Lcom/sec/common/a/b;->w:Landroid/content/DialogInterface$OnClickListener;

    .line 319
    :goto_0
    return-object p0

    .line 315
    :cond_0
    iput-object p1, p0, Lcom/sec/common/a/b;->z:Ljava/lang/CharSequence;

    .line 316
    iput-object p2, p0, Lcom/sec/common/a/b;->A:Landroid/content/DialogInterface$OnClickListener;

    goto :goto_0
.end method

.method public b(Z)Lcom/sec/common/a/a;
    .locals 0

    .prologue
    .line 359
    iput-boolean p1, p0, Lcom/sec/common/a/b;->q:Z

    .line 361
    return-object p0
.end method

.method public b()Lcom/sec/common/a/d;
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/sec/common/a/b;->a()Lcom/sec/common/a/d;

    move-result-object v0

    .line 97
    invoke-interface {v0}, Lcom/sec/common/a/d;->a()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 99
    return-object v0
.end method

.method public c(I)Lcom/sec/common/a/a;
    .locals 0

    .prologue
    .line 153
    return-object p0
.end method

.method public c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/a/b;->x:Ljava/lang/CharSequence;

    .line 303
    iput-object p2, p0, Lcom/sec/common/a/b;->y:Landroid/content/DialogInterface$OnClickListener;

    .line 305
    return-object p0
.end method

.method public d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
    .locals 2

    .prologue
    .line 324
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 325
    iget-object v0, p0, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/a/b;->v:Ljava/lang/CharSequence;

    .line 326
    iput-object p2, p0, Lcom/sec/common/a/b;->w:Landroid/content/DialogInterface$OnClickListener;

    .line 333
    :goto_0
    return-object p0

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/a/b;->z:Ljava/lang/CharSequence;

    .line 330
    iput-object p2, p0, Lcom/sec/common/a/b;->A:Landroid/content/DialogInterface$OnClickListener;

    goto :goto_0
.end method
