.class Lcom/sec/common/a/k;
.super Ljava/lang/Object;
.source "AlertDialogImpl.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ListView;

.field final synthetic b:Landroid/app/Dialog;

.field final synthetic c:Lcom/sec/common/a/e;


# direct methods
.method constructor <init>(Lcom/sec/common/a/e;Landroid/widget/ListView;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 569
    iput-object p1, p0, Lcom/sec/common/a/k;->c:Lcom/sec/common/a/e;

    iput-object p2, p0, Lcom/sec/common/a/k;->a:Landroid/widget/ListView;

    iput-object p3, p0, Lcom/sec/common/a/k;->b:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 572
    iget-object v0, p0, Lcom/sec/common/a/k;->c:Lcom/sec/common/a/e;

    invoke-static {v0}, Lcom/sec/common/a/e;->h(Lcom/sec/common/a/e;)[Z

    move-result-object v0

    if-eqz v0, :cond_0

    .line 573
    iget-object v0, p0, Lcom/sec/common/a/k;->c:Lcom/sec/common/a/e;

    invoke-static {v0}, Lcom/sec/common/a/e;->h(Lcom/sec/common/a/e;)[Z

    move-result-object v0

    iget-object v1, p0, Lcom/sec/common/a/k;->a:Landroid/widget/ListView;

    invoke-virtual {v1, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v1

    aput-boolean v1, v0, p3

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/sec/common/a/k;->c:Lcom/sec/common/a/e;

    invoke-static {v0}, Lcom/sec/common/a/e;->m(Lcom/sec/common/a/e;)Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/common/a/k;->b:Landroid/app/Dialog;

    iget-object v2, p0, Lcom/sec/common/a/k;->a:Landroid/widget/ListView;

    invoke-virtual {v2, p3}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v2

    invoke-interface {v0, v1, p3, v2}, Landroid/content/DialogInterface$OnMultiChoiceClickListener;->onClick(Landroid/content/DialogInterface;IZ)V

    .line 577
    return-void
.end method
