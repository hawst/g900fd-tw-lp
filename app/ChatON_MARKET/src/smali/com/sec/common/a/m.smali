.class Lcom/sec/common/a/m;
.super Ljava/lang/Object;
.source "AlertDialogWrapper.java"

# interfaces
.implements Lcom/sec/common/a/d;


# instance fields
.field private a:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    .line 28
    return-void
.end method


# virtual methods
.method public a()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public a(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/app/AlertDialog;->setView(Landroid/view/View;IIII)V

    .line 103
    return-void
.end method

.method public b()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 43
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    return v0
.end method

.method public setCancelable(Z)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 53
    return-void
.end method

.method public setCanceledOnTouchOutside(Z)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 58
    return-void
.end method

.method public setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 63
    return-void
.end method

.method public setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 68
    return-void
.end method

.method public setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 73
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/common/a/m;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 38
    return-void
.end method
