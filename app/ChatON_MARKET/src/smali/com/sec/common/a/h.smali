.class Lcom/sec/common/a/h;
.super Landroid/support/v4/widget/CursorAdapter;
.source "AlertDialogImpl.java"


# instance fields
.field final synthetic a:Landroid/widget/ListView;

.field final synthetic b:Lcom/sec/common/a/e;

.field private final c:I

.field private final d:I


# direct methods
.method constructor <init>(Lcom/sec/common/a/e;Landroid/content/Context;Landroid/database/Cursor;ZLandroid/widget/ListView;)V
    .locals 2

    .prologue
    .line 497
    iput-object p1, p0, Lcom/sec/common/a/h;->b:Lcom/sec/common/a/e;

    iput-object p5, p0, Lcom/sec/common/a/h;->a:Landroid/widget/ListView;

    invoke-direct {p0, p2, p3, p4}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 502
    invoke-virtual {p0}, Lcom/sec/common/a/h;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 503
    iget-object v1, p0, Lcom/sec/common/a/h;->b:Lcom/sec/common/a/e;

    invoke-static {v1}, Lcom/sec/common/a/e;->i(Lcom/sec/common/a/e;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/common/a/h;->c:I

    .line 504
    iget-object v1, p0, Lcom/sec/common/a/h;->b:Lcom/sec/common/a/e;

    invoke-static {v1}, Lcom/sec/common/a/e;->j(Lcom/sec/common/a/e;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/common/a/h;->d:I

    .line 505
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 509
    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 510
    iget v2, p0, Lcom/sec/common/a/h;->c:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 511
    iget-object v2, p0, Lcom/sec/common/a/h;->a:Landroid/widget/ListView;

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    iget v0, p0, Lcom/sec/common/a/h;->d:I

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 512
    return-void

    .line 511
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 516
    iget-object v0, p0, Lcom/sec/common/a/h;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/sec/common/d;->layout_dialog_listview_multichoice:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
