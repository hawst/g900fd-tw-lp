.class Lcom/sec/common/a/e;
.super Landroid/app/Dialog;
.source "AlertDialogImpl.java"

# interfaces
.implements Lcom/sec/common/a/d;


# instance fields
.field private A:Ljava/lang/CharSequence;

.field private B:Landroid/content/DialogInterface$OnClickListener;

.field private C:Landroid/os/Message;

.field private D:Ljava/lang/CharSequence;

.field private E:Landroid/content/DialogInterface$OnClickListener;

.field private F:Landroid/os/Message;

.field private G:Ljava/lang/CharSequence;

.field private H:Landroid/content/DialogInterface$OnClickListener;

.field private I:Landroid/content/DialogInterface$OnCancelListener;

.field private J:Landroid/content/DialogInterface$OnKeyListener;

.field private K:Landroid/widget/TextView;

.field private L:Landroid/widget/Button;

.field private M:Landroid/widget/Button;

.field private N:Landroid/widget/Button;

.field private O:Landroid/widget/ScrollView;

.field private P:Landroid/widget/TextView;

.field private Q:Landroid/widget/ListView;

.field private R:Lcom/sec/common/a/l;

.field private S:Landroid/view/View$OnClickListener;

.field private a:Landroid/content/Context;

.field private b:I

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Z

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Z

.field private m:Z

.field private n:I

.field private o:[Z

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:[Ljava/lang/CharSequence;

.field private s:Landroid/widget/ListAdapter;

.field private t:Landroid/database/Cursor;

.field private u:Landroid/content/DialogInterface$OnClickListener;

.field private v:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private w:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

.field private x:Z

.field private y:Z

.field private z:Landroid/os/Message;


# direct methods
.method public constructor <init>(Lcom/sec/common/a/b;)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p1, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    iget v1, p1, Lcom/sec/common/a/b;->b:I

    invoke-direct {p0, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 111
    new-instance v0, Lcom/sec/common/a/f;

    invoke-direct {v0, p0}, Lcom/sec/common/a/f;-><init>(Lcom/sec/common/a/e;)V

    iput-object v0, p0, Lcom/sec/common/a/e;->S:Landroid/view/View$OnClickListener;

    .line 159
    new-instance v0, Lcom/sec/common/a/l;

    invoke-direct {v0, p0}, Lcom/sec/common/a/l;-><init>(Landroid/content/DialogInterface;)V

    iput-object v0, p0, Lcom/sec/common/a/e;->R:Lcom/sec/common/a/l;

    .line 161
    invoke-direct {p0, p1}, Lcom/sec/common/a/e;->a(Lcom/sec/common/a/b;)V

    .line 162
    return-void
.end method

.method static synthetic a(Lcom/sec/common/a/e;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->L:Landroid/widget/Button;

    return-object v0
.end method

.method private a(Landroid/app/Dialog;)Landroid/widget/ListView;
    .locals 10

    .prologue
    const v4, 0x1020014

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 474
    iget-object v0, p0, Lcom/sec/common/a/e;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/sec/common/d;->layout_dialog_lisview:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 477
    iget-boolean v0, p0, Lcom/sec/common/a/e;->m:Z

    if-eqz v0, :cond_5

    .line 478
    iget-object v0, p0, Lcom/sec/common/a/e;->t:Landroid/database/Cursor;

    if-nez v0, :cond_4

    .line 479
    new-instance v0, Lcom/sec/common/a/g;

    iget-object v2, p0, Lcom/sec/common/a/e;->a:Landroid/content/Context;

    sget v3, Lcom/sec/common/d;->layout_dialog_listview_multichoice:I

    iget-object v5, p0, Lcom/sec/common/a/e;->r:[Ljava/lang/CharSequence;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/common/a/g;-><init>(Lcom/sec/common/a/e;Landroid/content/Context;II[Ljava/lang/CharSequence;Landroid/widget/ListView;)V

    .line 543
    :cond_0
    :goto_0
    iput-object v0, p0, Lcom/sec/common/a/e;->s:Landroid/widget/ListAdapter;

    .line 545
    iget-object v0, p0, Lcom/sec/common/a/e;->v:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    .line 546
    iget-object v0, p0, Lcom/sec/common/a/e;->v:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 549
    :cond_1
    iget-boolean v0, p0, Lcom/sec/common/a/e;->l:Z

    if-eqz v0, :cond_9

    .line 550
    invoke-virtual {v6, v9}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 556
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/common/a/e;->u:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_a

    .line 557
    new-instance v0, Lcom/sec/common/a/j;

    invoke-direct {v0, p0, p1}, Lcom/sec/common/a/j;-><init>(Lcom/sec/common/a/e;Landroid/app/Dialog;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 581
    :cond_3
    :goto_2
    return-object v6

    .line 497
    :cond_4
    new-instance v1, Lcom/sec/common/a/h;

    iget-object v3, p0, Lcom/sec/common/a/e;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/common/a/e;->t:Landroid/database/Cursor;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/sec/common/a/h;-><init>(Lcom/sec/common/a/e;Landroid/content/Context;Landroid/database/Cursor;ZLandroid/widget/ListView;)V

    move-object v0, v1

    goto :goto_0

    .line 524
    :cond_5
    iget-boolean v0, p0, Lcom/sec/common/a/e;->l:Z

    if-eqz v0, :cond_6

    sget v2, Lcom/sec/common/d;->layout_dialog_listview_singlechoice:I

    .line 526
    :goto_3
    iget-object v0, p0, Lcom/sec/common/a/e;->t:Landroid/database/Cursor;

    if-nez v0, :cond_8

    .line 527
    iget-object v0, p0, Lcom/sec/common/a/e;->s:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/common/a/e;->s:Landroid/widget/ListAdapter;

    .line 533
    :goto_4
    iget-object v1, p0, Lcom/sec/common/a/e;->u:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v1, :cond_0

    .line 534
    new-instance v1, Lcom/sec/common/a/i;

    invoke-direct {v1, p0, p1}, Lcom/sec/common/a/i;-><init>(Lcom/sec/common/a/e;Landroid/app/Dialog;)V

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    .line 524
    :cond_6
    sget v2, Lcom/sec/common/d;->layout_dialog_listview_item:I

    goto :goto_3

    .line 527
    :cond_7
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/sec/common/a/e;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/common/a/e;->r:[Ljava/lang/CharSequence;

    invoke-direct {v0, v1, v2, v4, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    goto :goto_4

    .line 530
    :cond_8
    new-instance v0, Landroid/support/v4/widget/SimpleCursorAdapter;

    iget-object v1, p0, Lcom/sec/common/a/e;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/common/a/e;->t:Landroid/database/Cursor;

    new-array v7, v9, [Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/common/a/e;->q:Ljava/lang/String;

    aput-object v8, v7, v5

    new-array v8, v9, [I

    aput v4, v8, v5

    move-object v4, v7

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    goto :goto_4

    .line 552
    :cond_9
    iget-boolean v0, p0, Lcom/sec/common/a/e;->m:Z

    if-eqz v0, :cond_2

    .line 553
    const/4 v0, 0x2

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto :goto_1

    .line 568
    :cond_a
    iget-object v0, p0, Lcom/sec/common/a/e;->w:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    if-eqz v0, :cond_3

    .line 569
    new-instance v0, Lcom/sec/common/a/k;

    invoke-direct {v0, p0, v6, p1}, Lcom/sec/common/a/k;-><init>(Lcom/sec/common/a/e;Landroid/widget/ListView;Landroid/app/Dialog;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_2
.end method

.method private a(Landroid/content/res/TypedArray;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;Landroid/view/View;Landroid/view/View;ZZ)V
    .locals 17

    .prologue
    .line 664
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v14

    .line 665
    const/4 v1, 0x4

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v13

    .line 666
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 667
    const/4 v1, 0x2

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    .line 668
    const/4 v1, 0x3

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v12

    .line 669
    const/4 v1, 0x5

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    .line 670
    const/4 v1, 0x6

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v8

    .line 671
    const/4 v1, 0x7

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v11

    .line 672
    const/16 v1, 0x8

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v10

    .line 674
    const/4 v1, 0x4

    new-array v15, v1, [Landroid/view/View;

    .line 675
    const/4 v1, 0x4

    new-array v0, v1, [Z

    move-object/from16 v16, v0

    .line 676
    const/4 v4, 0x0

    .line 677
    const/4 v3, 0x0

    .line 679
    const/4 v1, 0x0

    .line 680
    if-eqz p7, :cond_0

    .line 681
    aput-object p2, v15, v1

    .line 682
    const/4 v2, 0x0

    aput-boolean v2, v16, v1

    .line 683
    const/4 v1, 0x1

    .line 686
    :cond_0
    invoke-virtual/range {p3 .. p3}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    const/16 v5, 0x8

    if-ne v2, v5, :cond_1

    const/16 p3, 0x0

    :cond_1
    aput-object p3, v15, v1

    .line 688
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/common/a/e;->Q:Landroid/widget/ListView;

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    aput-boolean v2, v16, v1

    .line 689
    add-int/lit8 v1, v1, 0x1

    .line 690
    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 691
    aput-object p4, v15, v1

    .line 692
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/common/a/e;->y:Z

    aput-boolean v2, v16, v1

    .line 693
    add-int/lit8 v1, v1, 0x1

    .line 695
    :cond_2
    if-eqz p6, :cond_3

    .line 696
    aput-object p5, v15, v1

    .line 697
    const/4 v2, 0x1

    aput-boolean v2, v16, v1

    .line 700
    :cond_3
    const/4 v1, 0x0

    .line 701
    const/4 v2, 0x0

    move v5, v2

    move v2, v3

    move-object v3, v4

    :goto_1
    array-length v4, v15

    if-ge v5, v4, :cond_a

    .line 702
    aget-object v4, v15, v5

    .line 703
    if-nez v4, :cond_5

    .line 701
    :goto_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 688
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 706
    :cond_5
    if-eqz v3, :cond_6

    .line 707
    if-nez v1, :cond_8

    .line 708
    if-eqz v2, :cond_7

    move v1, v6

    :goto_3
    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 712
    :goto_4
    const/4 v1, 0x1

    .line 715
    :cond_6
    aget-boolean v2, v16, v5

    move-object v3, v4

    goto :goto_2

    :cond_7
    move v1, v7

    .line 708
    goto :goto_3

    .line 710
    :cond_8
    if-eqz v2, :cond_9

    move v1, v8

    :goto_5
    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4

    :cond_9
    move v1, v9

    goto :goto_5

    .line 718
    :cond_a
    if-eqz v3, :cond_b

    .line 719
    if-eqz v1, :cond_e

    .line 725
    if-eqz v2, :cond_d

    if-eqz p6, :cond_c

    move v1, v10

    :goto_6
    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 731
    :cond_b
    :goto_7
    return-void

    :cond_c
    move v1, v11

    .line 725
    goto :goto_6

    :cond_d
    move v1, v12

    goto :goto_6

    .line 728
    :cond_e
    if-eqz v2, :cond_f

    move v1, v13

    :goto_8
    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_7

    :cond_f
    move v1, v14

    goto :goto_8
.end method

.method private a(Lcom/sec/common/a/b;)V
    .locals 3

    .prologue
    .line 371
    iget v0, p1, Lcom/sec/common/a/b;->b:I

    iput v0, p0, Lcom/sec/common/a/e;->b:I

    .line 373
    iget v0, p0, Lcom/sec/common/a/e;->b:I

    if-nez v0, :cond_0

    .line 374
    iget-object v0, p1, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/common/a/e;->a:Landroid/content/Context;

    .line 380
    :goto_0
    iget-object v0, p1, Lcom/sec/common/a/b;->r:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/sec/common/a/e;->c:Ljava/lang/CharSequence;

    .line 381
    iget-object v0, p1, Lcom/sec/common/a/b;->t:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/sec/common/a/e;->d:Ljava/lang/CharSequence;

    .line 383
    iget-object v0, p1, Lcom/sec/common/a/b;->s:Landroid/view/View;

    iput-object v0, p0, Lcom/sec/common/a/e;->e:Landroid/view/View;

    .line 384
    iget-object v0, p1, Lcom/sec/common/a/b;->u:Landroid/view/View;

    iput-object v0, p0, Lcom/sec/common/a/e;->f:Landroid/view/View;

    .line 386
    iget-boolean v0, p1, Lcom/sec/common/a/b;->k:Z

    iput-boolean v0, p0, Lcom/sec/common/a/e;->l:Z

    .line 387
    iget-boolean v0, p1, Lcom/sec/common/a/b;->l:Z

    iput-boolean v0, p0, Lcom/sec/common/a/e;->m:Z

    .line 388
    iget v0, p1, Lcom/sec/common/a/b;->i:I

    iput v0, p0, Lcom/sec/common/a/e;->n:I

    .line 389
    iget-object v0, p1, Lcom/sec/common/a/b;->j:[Z

    iput-object v0, p0, Lcom/sec/common/a/e;->o:[Z

    .line 390
    iget-object v0, p1, Lcom/sec/common/a/b;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/common/a/e;->p:Ljava/lang/String;

    .line 391
    iget-object v0, p1, Lcom/sec/common/a/b;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/common/a/e;->q:Ljava/lang/String;

    .line 393
    iget-object v0, p1, Lcom/sec/common/a/b;->d:[Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/sec/common/a/e;->r:[Ljava/lang/CharSequence;

    .line 394
    iget-object v0, p1, Lcom/sec/common/a/b;->e:Landroid/widget/ListAdapter;

    iput-object v0, p0, Lcom/sec/common/a/e;->s:Landroid/widget/ListAdapter;

    .line 395
    iget-object v0, p1, Lcom/sec/common/a/b;->f:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/sec/common/a/e;->t:Landroid/database/Cursor;

    .line 396
    iget-object v0, p1, Lcom/sec/common/a/b;->m:Landroid/content/DialogInterface$OnClickListener;

    iput-object v0, p0, Lcom/sec/common/a/e;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 397
    iget-object v0, p1, Lcom/sec/common/a/b;->o:Landroid/widget/AdapterView$OnItemSelectedListener;

    iput-object v0, p0, Lcom/sec/common/a/e;->v:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 398
    iget-object v0, p1, Lcom/sec/common/a/b;->n:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    iput-object v0, p0, Lcom/sec/common/a/e;->w:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    .line 400
    iget-boolean v0, p1, Lcom/sec/common/a/b;->q:Z

    iput-boolean v0, p0, Lcom/sec/common/a/e;->x:Z

    .line 401
    iget-boolean v0, p1, Lcom/sec/common/a/b;->c:Z

    iput-boolean v0, p0, Lcom/sec/common/a/e;->y:Z

    .line 403
    iget-object v0, p1, Lcom/sec/common/a/b;->v:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/sec/common/a/e;->A:Ljava/lang/CharSequence;

    .line 404
    iget-object v0, p1, Lcom/sec/common/a/b;->w:Landroid/content/DialogInterface$OnClickListener;

    iput-object v0, p0, Lcom/sec/common/a/e;->B:Landroid/content/DialogInterface$OnClickListener;

    .line 406
    iget-object v0, p1, Lcom/sec/common/a/b;->x:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/sec/common/a/e;->D:Ljava/lang/CharSequence;

    .line 407
    iget-object v0, p1, Lcom/sec/common/a/b;->y:Landroid/content/DialogInterface$OnClickListener;

    iput-object v0, p0, Lcom/sec/common/a/e;->E:Landroid/content/DialogInterface$OnClickListener;

    .line 409
    iget-object v0, p1, Lcom/sec/common/a/b;->z:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/sec/common/a/e;->G:Ljava/lang/CharSequence;

    .line 410
    iget-object v0, p1, Lcom/sec/common/a/b;->A:Landroid/content/DialogInterface$OnClickListener;

    iput-object v0, p0, Lcom/sec/common/a/e;->H:Landroid/content/DialogInterface$OnClickListener;

    .line 412
    iget-object v0, p1, Lcom/sec/common/a/b;->B:Landroid/content/DialogInterface$OnCancelListener;

    iput-object v0, p0, Lcom/sec/common/a/e;->I:Landroid/content/DialogInterface$OnCancelListener;

    .line 413
    iget-object v0, p1, Lcom/sec/common/a/b;->p:Landroid/content/DialogInterface$OnKeyListener;

    iput-object v0, p0, Lcom/sec/common/a/e;->J:Landroid/content/DialogInterface$OnKeyListener;

    .line 414
    return-void

    .line 377
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p1, Lcom/sec/common/a/b;->a:Landroid/content/Context;

    iget v2, p1, Lcom/sec/common/a/b;->b:I

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/common/a/e;->a:Landroid/content/Context;

    goto :goto_0
.end method

.method private a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 602
    .line 604
    if-nez p4, :cond_0

    if-eqz p3, :cond_0

    .line 605
    iget-object v0, p0, Lcom/sec/common/a/e;->R:Lcom/sec/common/a/l;

    invoke-virtual {v0, p1, p3}, Lcom/sec/common/a/l;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p4

    .line 608
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 628
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown button type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 610
    :pswitch_0
    iget-object v0, p0, Lcom/sec/common/a/e;->L:Landroid/widget/Button;

    .line 611
    iput-object p4, p0, Lcom/sec/common/a/e;->z:Landroid/os/Message;

    .line 612
    iput-object p2, p0, Lcom/sec/common/a/e;->A:Ljava/lang/CharSequence;

    .line 631
    :goto_0
    iget-object v2, p0, Lcom/sec/common/a/e;->S:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 633
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 634
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 635
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 637
    const/4 v0, 0x1

    .line 642
    :goto_1
    return v0

    .line 616
    :pswitch_1
    iget-object v0, p0, Lcom/sec/common/a/e;->M:Landroid/widget/Button;

    .line 617
    iput-object p4, p0, Lcom/sec/common/a/e;->C:Landroid/os/Message;

    .line 618
    iput-object p2, p0, Lcom/sec/common/a/e;->D:Ljava/lang/CharSequence;

    goto :goto_0

    .line 622
    :pswitch_2
    iget-object v0, p0, Lcom/sec/common/a/e;->N:Landroid/widget/Button;

    .line 623
    iput-object p4, p0, Lcom/sec/common/a/e;->F:Landroid/os/Message;

    .line 624
    iput-object p2, p0, Lcom/sec/common/a/e;->G:Ljava/lang/CharSequence;

    goto :goto_0

    .line 640
    :cond_1
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    move v0, v1

    .line 642
    goto :goto_1

    .line 608
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/app/Dialog;Landroid/widget/LinearLayout;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 646
    const/4 v1, 0x0

    .line 648
    sget v0, Lcom/sec/common/c;->button1:I

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/common/a/e;->L:Landroid/widget/Button;

    .line 649
    const/4 v0, -0x1

    iget-object v2, p0, Lcom/sec/common/a/e;->A:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/sec/common/a/e;->B:Landroid/content/DialogInterface$OnClickListener;

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/sec/common/a/e;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)Z

    move-result v0

    or-int/2addr v1, v0

    .line 651
    sget v0, Lcom/sec/common/c;->button2:I

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/common/a/e;->M:Landroid/widget/Button;

    .line 652
    const/4 v0, -0x3

    iget-object v2, p0, Lcom/sec/common/a/e;->D:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/sec/common/a/e;->E:Landroid/content/DialogInterface$OnClickListener;

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/sec/common/a/e;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)Z

    move-result v0

    or-int/2addr v1, v0

    .line 654
    sget v0, Lcom/sec/common/c;->button3:I

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/common/a/e;->N:Landroid/widget/Button;

    .line 655
    const/4 v0, -0x2

    iget-object v2, p0, Lcom/sec/common/a/e;->G:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/sec/common/a/e;->H:Landroid/content/DialogInterface$OnClickListener;

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/sec/common/a/e;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)Z

    move-result v0

    or-int/2addr v0, v1

    .line 657
    return v0
.end method

.method private a(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 734
    invoke-virtual {p1}, Landroid/view/View;->onCheckIsTextEditor()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 752
    :goto_0
    return v0

    .line 738
    :cond_0
    instance-of v2, p1, Landroid/view/ViewGroup;

    if-nez v2, :cond_1

    move v0, v1

    .line 739
    goto :goto_0

    .line 742
    :cond_1
    check-cast p1, Landroid/view/ViewGroup;

    .line 743
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 744
    :cond_2
    if-lez v2, :cond_3

    .line 745
    add-int/lit8 v2, v2, -0x1

    .line 746
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 747
    invoke-direct {p0, v3}, Lcom/sec/common/a/e;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 752
    goto :goto_0
.end method

.method private a(Landroid/widget/FrameLayout;)Z
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 585
    iget-object v0, p0, Lcom/sec/common/a/e;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 586
    sget v0, Lcom/sec/common/c;->custom:I

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 587
    iget-object v1, p0, Lcom/sec/common/a/e;->f:Landroid/view/View;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 589
    iget-boolean v1, p0, Lcom/sec/common/a/e;->g:Z

    if-eqz v1, :cond_0

    .line 590
    iget v1, p0, Lcom/sec/common/a/e;->h:I

    iget v2, p0, Lcom/sec/common/a/e;->i:I

    iget v3, p0, Lcom/sec/common/a/e;->j:I

    iget v4, p0, Lcom/sec/common/a/e;->k:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 593
    :cond_0
    const/4 v0, 0x1

    .line 598
    :goto_0
    return v0

    .line 596
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 598
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/widget/LinearLayout;)Z
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 417
    const/4 v1, 0x1

    .line 419
    iget-object v0, p0, Lcom/sec/common/a/e;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 420
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 423
    iget-object v2, p0, Lcom/sec/common/a/e;->e:Landroid/view/View;

    invoke-virtual {p1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 425
    sget v0, Lcom/sec/common/c;->title_template:I

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 426
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    move v0, v1

    .line 442
    :goto_0
    return v0

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/sec/common/a/e;->c:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 430
    sget v0, Lcom/sec/common/c;->alertTitle:I

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/common/a/e;->K:Landroid/widget/TextView;

    .line 432
    iget-object v0, p0, Lcom/sec/common/a/e;->K:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/common/a/e;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    goto :goto_0

    .line 435
    :cond_1
    sget v0, Lcom/sec/common/c;->title_template:I

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 436
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 438
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/common/a/e;)Landroid/os/Message;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->z:Landroid/os/Message;

    return-object v0
.end method

.method private b(Landroid/widget/LinearLayout;)Z
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 446
    const/4 v1, 0x1

    .line 448
    sget v0, Lcom/sec/common/c;->scrollView:I

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/sec/common/a/e;->O:Landroid/widget/ScrollView;

    .line 449
    iget-object v0, p0, Lcom/sec/common/a/e;->O:Landroid/widget/ScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 451
    sget v0, Lcom/sec/common/c;->message:I

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/common/a/e;->P:Landroid/widget/TextView;

    .line 453
    iget-object v0, p0, Lcom/sec/common/a/e;->d:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/sec/common/a/e;->P:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/common/a/e;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 470
    :goto_0
    return v1

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/sec/common/a/e;->P:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 458
    iget-object v0, p0, Lcom/sec/common/a/e;->O:Landroid/widget/ScrollView;

    iget-object v2, p0, Lcom/sec/common/a/e;->P:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->removeView(Landroid/view/View;)V

    .line 460
    iget-object v0, p0, Lcom/sec/common/a/e;->Q:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 461
    iget-object v0, p0, Lcom/sec/common/a/e;->O:Landroid/widget/ScrollView;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 462
    iget-object v0, p0, Lcom/sec/common/a/e;->Q:Landroid/widget/ListView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 463
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 466
    :cond_1
    invoke-virtual {p1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/common/a/e;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->M:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/common/a/e;)Landroid/os/Message;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->C:Landroid/os/Message;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/common/a/e;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->N:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/common/a/e;)Landroid/os/Message;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->F:Landroid/os/Message;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/common/a/e;)Lcom/sec/common/a/l;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->R:Lcom/sec/common/a/l;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/common/a/e;)[Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->o:[Z

    return-object v0
.end method

.method static synthetic i(Lcom/sec/common/a/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/common/a/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/common/a/e;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->u:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/common/a/e;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/common/a/e;->l:Z

    return v0
.end method

.method static synthetic m(Lcom/sec/common/a/e;)Landroid/content/DialogInterface$OnMultiChoiceClickListener;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/common/a/e;->w:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/app/Dialog;
    .locals 0

    .prologue
    .line 172
    return-object p0
.end method

.method public a(Landroid/view/View;IIII)V
    .locals 1

    .prologue
    .line 301
    iput-object p1, p0, Lcom/sec/common/a/e;->f:Landroid/view/View;

    .line 302
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/common/a/e;->g:Z

    .line 303
    iput p2, p0, Lcom/sec/common/a/e;->h:I

    .line 304
    iput p3, p0, Lcom/sec/common/a/e;->i:I

    .line 305
    iput p4, p0, Lcom/sec/common/a/e;->j:I

    .line 306
    iput p5, p0, Lcom/sec/common/a/e;->k:I

    .line 307
    return-void
.end method

.method public b()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/common/a/e;->Q:Landroid/widget/ListView;

    return-object v0
.end method

.method c()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/common/a/e;->r:[Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/common/a/e;->t:Landroid/database/Cursor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/common/a/e;->s:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 166
    :cond_0
    invoke-direct {p0, p0}, Lcom/sec/common/a/e;->a(Landroid/app/Dialog;)Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/common/a/e;->Q:Landroid/widget/ListView;

    .line 168
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/high16 v2, 0x20000

    const/16 v9, 0x8

    const/4 v8, 0x1

    .line 177
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 179
    invoke-virtual {p0}, Lcom/sec/common/a/e;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 180
    invoke-virtual {v0, v8}, Landroid/view/Window;->requestFeature(I)Z

    .line 182
    iget-object v1, p0, Lcom/sec/common/a/e;->f:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/common/a/e;->f:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/sec/common/a/e;->a(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 183
    :cond_0
    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/sec/common/a/e;->a:Landroid/content/Context;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/common/e;->AlertDialogCompat:[I

    sget v3, Lcom/sec/common/b;->alertDialogStyle:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 190
    sget v0, Lcom/sec/common/d;->layout_dialog:I

    invoke-virtual {p0, v0}, Lcom/sec/common/a/e;->setContentView(I)V

    .line 193
    sget v0, Lcom/sec/common/c;->topPanel:I

    invoke-virtual {p0, v0}, Lcom/sec/common/a/e;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 194
    invoke-direct {p0, v2}, Lcom/sec/common/a/e;->a(Landroid/widget/LinearLayout;)Z

    move-result v7

    .line 197
    sget v0, Lcom/sec/common/c;->buttonPanel:I

    invoke-virtual {p0, v0}, Lcom/sec/common/a/e;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 198
    invoke-direct {p0, p0, v5}, Lcom/sec/common/a/e;->a(Landroid/app/Dialog;Landroid/widget/LinearLayout;)Z

    move-result v6

    .line 199
    if-nez v6, :cond_2

    .line 200
    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 204
    :cond_2
    sget v0, Lcom/sec/common/c;->contentPanel:I

    invoke-virtual {p0, v0}, Lcom/sec/common/a/e;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 205
    invoke-direct {p0, v3}, Lcom/sec/common/a/e;->b(Landroid/widget/LinearLayout;)Z

    .line 207
    sget v0, Lcom/sec/common/c;->customPanel:I

    invoke-virtual {p0, v0}, Lcom/sec/common/a/e;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout;

    .line 208
    invoke-direct {p0, v4}, Lcom/sec/common/a/e;->a(Landroid/widget/FrameLayout;)Z

    move-result v0

    .line 209
    if-nez v0, :cond_3

    .line 210
    invoke-virtual {v4, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_3
    move-object v0, p0

    .line 213
    invoke-direct/range {v0 .. v7}, Lcom/sec/common/a/e;->a(Landroid/content/res/TypedArray;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;Landroid/view/View;Landroid/view/View;ZZ)V

    .line 216
    iget-object v0, p0, Lcom/sec/common/a/e;->Q:Landroid/widget/ListView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/common/a/e;->s:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_4

    .line 217
    iget-object v0, p0, Lcom/sec/common/a/e;->Q:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/common/a/e;->s:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 219
    iget v0, p0, Lcom/sec/common/a/e;->n:I

    const/4 v2, -0x1

    if-le v0, v2, :cond_4

    .line 220
    iget-object v0, p0, Lcom/sec/common/a/e;->Q:Landroid/widget/ListView;

    iget v2, p0, Lcom/sec/common/a/e;->n:I

    invoke-virtual {v0, v2, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 221
    iget-object v0, p0, Lcom/sec/common/a/e;->Q:Landroid/widget/ListView;

    iget v2, p0, Lcom/sec/common/a/e;->n:I

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 225
    :cond_4
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 227
    iget-boolean v0, p0, Lcom/sec/common/a/e;->x:Z

    invoke-virtual {p0, v0}, Lcom/sec/common/a/e;->setCancelable(Z)V

    .line 228
    iget-object v0, p0, Lcom/sec/common/a/e;->I:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p0, v0}, Lcom/sec/common/a/e;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 229
    iget-object v0, p0, Lcom/sec/common/a/e;->J:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {p0, v0}, Lcom/sec/common/a/e;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 230
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/common/a/e;->O:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/common/a/e;->O:Landroid/widget/ScrollView;

    invoke-virtual {v0, p2}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    const/4 v0, 0x1

    .line 257
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/common/a/e;->O:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/common/a/e;->O:Landroid/widget/ScrollView;

    invoke-virtual {v0, p2}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const/4 v0, 0x1

    .line 266
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 271
    invoke-super {p0, p1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 272
    iput-object p1, p0, Lcom/sec/common/a/e;->c:Ljava/lang/CharSequence;

    .line 274
    iget-object v0, p0, Lcom/sec/common/a/e;->K:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sec/common/a/e;->K:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/common/a/e;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    :cond_0
    return-void
.end method
