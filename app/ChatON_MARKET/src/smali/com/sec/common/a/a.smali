.class public abstract Lcom/sec/common/a/a;
.super Ljava/lang/Object;
.source "AlertDialogBuilderCompat.java"


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/sec/common/a/a;
    .locals 2

    .prologue
    .line 23
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 24
    new-instance v0, Lcom/sec/common/a/b;

    invoke-direct {v0, p0}, Lcom/sec/common/a/b;-><init>(Landroid/content/Context;)V

    .line 27
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/common/a/c;

    invoke-direct {v0, p0}, Lcom/sec/common/a/c;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a(I)Lcom/sec/common/a/a;
.end method

.method public abstract a(IILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
.end method

.method public abstract a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
.end method

.method public abstract a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/sec/common/a/a;
.end method

.method public abstract a(Landroid/view/View;)Lcom/sec/common/a/a;
.end method

.method public abstract a(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;
.end method

.method public abstract a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
.end method

.method public abstract a(Z)Lcom/sec/common/a/a;
.end method

.method public abstract a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
.end method

.method public abstract a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
.end method

.method public abstract a()Lcom/sec/common/a/d;
.end method

.method public abstract b(I)Lcom/sec/common/a/a;
.end method

.method public abstract b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
.end method

.method public abstract b(Landroid/view/View;)Lcom/sec/common/a/a;
.end method

.method public abstract b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;
.end method

.method public abstract b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
.end method

.method public abstract b(Z)Lcom/sec/common/a/a;
.end method

.method public abstract b()Lcom/sec/common/a/d;
.end method

.method public abstract c(I)Lcom/sec/common/a/a;
.end method

.method public abstract c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
.end method

.method public abstract d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;
.end method
