.class public Lcom/sec/vip/imagefilter/a;
.super Ljava/lang/Object;
.source "ImageEffectEngine.java"


# direct methods
.method public static A(Landroid/graphics/Bitmap;)Z
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 866
    if-nez p0, :cond_0

    .line 867
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    :goto_0
    return v8

    .line 871
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 872
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 873
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 874
    mul-int v0, v3, v7

    new-array v9, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 876
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 878
    invoke-static {v1, v9, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyClassic([I[IIII)I

    move-result v10

    .line 880
    if-nez v10, :cond_2

    move-object v0, p0

    move-object v1, v9

    move v4, v2

    move v5, v2

    move v6, v3

    .line 881
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 887
    :goto_1
    if-nez v10, :cond_1

    move v2, v8

    :cond_1
    move v8, v2

    goto :goto_0

    .line 883
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static B(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 891
    if-nez p0, :cond_1

    .line 892
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    :cond_0
    :goto_0
    return v2

    .line 896
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 897
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 898
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 899
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 901
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 904
    const/16 v0, 0x32

    .line 905
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applySoftglow([I[IIII)I

    move-result v9

    .line 907
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 908
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 914
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 910
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static C(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 918
    if-nez p0, :cond_1

    .line 919
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    :cond_0
    :goto_0
    return v2

    .line 923
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 924
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 925
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 926
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 928
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 930
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyMagicPen([I[IIII)I

    move-result v9

    .line 932
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 933
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 939
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 935
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static D(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 943
    if-nez p0, :cond_1

    .line 944
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 964
    :cond_0
    :goto_0
    return v2

    .line 948
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 949
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 950
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 951
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 953
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 955
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyOilpaint([I[IIII)I

    move-result v9

    .line 957
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 958
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 964
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 960
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static E(Landroid/graphics/Bitmap;)Z
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 968
    if-nez p0, :cond_0

    .line 969
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 991
    :goto_0
    return v8

    .line 973
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 974
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 975
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 976
    mul-int v0, v3, v7

    new-array v9, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 978
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 981
    const/4 v0, 0x3

    .line 982
    invoke-static {v1, v9, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyPosterize([I[IIII)I

    move-result v10

    .line 984
    if-nez v10, :cond_2

    move-object v0, p0

    move-object v1, v9

    move v4, v2

    move v5, v2

    move v6, v3

    .line 985
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 991
    :goto_1
    if-nez v10, :cond_1

    move v2, v8

    :cond_1
    move v8, v2

    goto :goto_0

    .line 987
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static F(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 995
    if-nez p0, :cond_1

    .line 996
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1017
    :cond_0
    :goto_0
    return v2

    .line 1000
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1001
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 1002
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 1003
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 1005
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1007
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyFusain([I[IIII)I

    move-result v9

    .line 1009
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 1010
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1017
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 1012
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static G(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1022
    if-nez p0, :cond_1

    .line 1023
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1046
    :cond_0
    :goto_0
    return v2

    .line 1027
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1028
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 1029
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 1030
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 1032
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1035
    const/4 v0, 0x3

    .line 1036
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyMosaic([I[IIII)I

    move-result v9

    .line 1038
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 1039
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1046
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 1041
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 183
    if-nez p0, :cond_1

    .line 184
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :cond_0
    :goto_0
    return v2

    .line 188
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 189
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 190
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 191
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 193
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 195
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyGray([I[IIII)I

    move-result v9

    .line 197
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 198
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 205
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 200
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Landroid/graphics/Bitmap;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 54
    const/16 v1, 0x7d0

    if-lt p1, v1, :cond_0

    const/16 v1, 0x7f3

    if-lt p1, v1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 170
    :goto_1
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 64
    :pswitch_1
    :try_start_0
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->f(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 166
    :catch_0
    move-exception v1

    goto :goto_0

    .line 67
    :pswitch_2
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->c(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 70
    :pswitch_3
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->d(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 73
    :pswitch_4
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->a(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 76
    :pswitch_5
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->b(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 85
    :pswitch_6
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->h(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 88
    :pswitch_7
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->e(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 91
    :pswitch_8
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->F(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 94
    :pswitch_9
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->i(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 97
    :pswitch_a
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->j(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 100
    :pswitch_b
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->k(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 103
    :pswitch_c
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->l(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 106
    :pswitch_d
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->m(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 109
    :pswitch_e
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->g(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 112
    :pswitch_f
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->n(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 115
    :pswitch_10
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->o(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 118
    :pswitch_11
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->p(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 121
    :pswitch_12
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->G(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 124
    :pswitch_13
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->r(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 127
    :pswitch_14
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->s(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 130
    :pswitch_15
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->t(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 133
    :pswitch_16
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->u(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 136
    :pswitch_17
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->v(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 139
    :pswitch_18
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->w(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 142
    :pswitch_19
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->x(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 145
    :pswitch_1a
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->y(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 148
    :pswitch_1b
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->z(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 151
    :pswitch_1c
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->A(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 154
    :pswitch_1d
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->B(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 157
    :pswitch_1e
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->C(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 160
    :pswitch_1f
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->D(Landroid/graphics/Bitmap;)Z

    goto :goto_1

    .line 163
    :pswitch_20
    invoke-static {p0}, Lcom/sec/vip/imagefilter/a;->E(Landroid/graphics/Bitmap;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 58
    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
    .end packed-switch
.end method

.method public static b(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 209
    if-nez p0, :cond_1

    .line 210
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_0
    :goto_0
    return v2

    .line 214
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 215
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 216
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 217
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 219
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 221
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyNegative([I[IIII)I

    move-result v9

    .line 223
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 224
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 231
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 226
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static c(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 235
    if-nez p0, :cond_1

    .line 236
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_0
    :goto_0
    return v2

    .line 240
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 241
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 242
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 243
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 245
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 248
    const/16 v0, 0x3c

    .line 249
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyBright([I[IIII)I

    move-result v9

    .line 251
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 252
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 259
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 254
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static d(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 263
    if-nez p0, :cond_1

    .line 264
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_0
    :goto_0
    return v2

    .line 268
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 269
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 270
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 271
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 273
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 276
    const/16 v0, 0x3c

    .line 277
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyDark([I[IIII)I

    move-result v9

    .line 279
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 280
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 287
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 282
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static e(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 291
    if-nez p0, :cond_1

    .line 292
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_0
    :goto_0
    return v2

    .line 296
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 297
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 298
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 299
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 301
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 304
    const/16 v0, 0x32

    .line 305
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applySepia([I[IIII)I

    move-result v9

    .line 307
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 308
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 314
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 310
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static f(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 318
    if-nez p0, :cond_1

    .line 319
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :cond_0
    :goto_0
    return v2

    .line 323
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 324
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 325
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 326
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 328
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 331
    const/16 v0, 0x19

    .line 332
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyBlur([I[IIII)I

    move-result v9

    .line 334
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 335
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 342
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 337
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static g(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 346
    if-nez p0, :cond_1

    .line 347
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :cond_0
    :goto_0
    return v2

    .line 351
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 352
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 353
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 354
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 356
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 359
    const/16 v0, 0x3c

    .line 360
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyFadedColor([I[IIII)I

    move-result v9

    .line 362
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 363
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 369
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 365
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static h(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 373
    if-nez p0, :cond_1

    .line 374
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    :cond_0
    :goto_0
    return v2

    .line 378
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 379
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 380
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 381
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 383
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 386
    const/16 v0, 0x3c

    .line 387
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyPencilSketch([I[IIII)I

    move-result v9

    .line 389
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 390
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 396
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 392
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static i(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 400
    if-nez p0, :cond_1

    .line 401
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :cond_0
    :goto_0
    return v2

    .line 405
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 406
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 407
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 408
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 410
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 413
    const/4 v0, 0x5

    .line 414
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyPenSketch([I[IIII)I

    move-result v9

    .line 416
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 417
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 423
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 419
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static j(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 427
    if-nez p0, :cond_1

    .line 428
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    :cond_0
    :goto_0
    return v2

    .line 432
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 433
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 434
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 435
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 437
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 440
    const/4 v0, 0x5

    .line 441
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyPastelSketch([I[IIII)I

    move-result v9

    .line 443
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 444
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 450
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 446
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static k(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 454
    if-nez p0, :cond_1

    .line 455
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_0
    :goto_0
    return v2

    .line 459
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 460
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 461
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 462
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 464
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 467
    const/4 v0, 0x5

    .line 468
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyColorSketch([I[IIII)I

    move-result v9

    .line 470
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 471
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 477
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 473
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static l(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 481
    if-nez p0, :cond_1

    .line 482
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :cond_0
    :goto_0
    return v2

    .line 486
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 487
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 488
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 489
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 491
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 494
    const/16 v0, 0x3c

    .line 495
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyPencilPastelSketch([I[IIII)I

    move-result v9

    .line 497
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 498
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 504
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 500
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static m(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 508
    if-nez p0, :cond_1

    .line 509
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    :cond_0
    :goto_0
    return v2

    .line 513
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 514
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 515
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 516
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 518
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 521
    const/16 v0, 0x3c

    .line 522
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyPencilColorSketch([I[IIII)I

    move-result v9

    .line 524
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 525
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 531
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 527
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static n(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 535
    if-nez p0, :cond_1

    .line 536
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    :cond_0
    :goto_0
    return v2

    .line 540
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 541
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 542
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 543
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 545
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 548
    const/16 v0, 0x82

    .line 549
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyVivid([I[IIII)I

    move-result v9

    .line 551
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 552
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 558
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 554
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static o(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 562
    if-nez p0, :cond_1

    .line 563
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    :cond_0
    :goto_0
    return v2

    .line 567
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 568
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 569
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 570
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 572
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 575
    const/16 v0, 0xfe

    .line 576
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyColorize([I[IIII)I

    move-result v9

    .line 578
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 579
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 585
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 581
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static p(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 589
    if-nez p0, :cond_1

    .line 590
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    :cond_0
    :goto_0
    return v2

    .line 594
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 595
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 596
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 597
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 599
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 602
    const/4 v0, 0x6

    .line 603
    invoke-static {v1, v8, v3, v7, v0}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyCartoonize([I[IIII)I

    move-result v9

    .line 605
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 606
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 612
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 608
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static q(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 616
    if-nez p0, :cond_1

    .line 617
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    :cond_0
    :goto_0
    return v2

    .line 621
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 622
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 623
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 624
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 626
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 628
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyVignette([I[IIII)I

    move-result v9

    .line 630
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 631
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 637
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 633
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static r(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 641
    if-nez p0, :cond_1

    .line 642
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    :cond_0
    :goto_0
    return v2

    .line 646
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 647
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 648
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 649
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 651
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 653
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyVintage([I[IIII)I

    move-result v9

    .line 655
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 656
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 662
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 658
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static s(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 666
    if-nez p0, :cond_1

    .line 667
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :cond_0
    :goto_0
    return v2

    .line 671
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 672
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 673
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 674
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 676
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 678
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyOldPhoto([I[IIII)I

    move-result v9

    .line 680
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 681
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 687
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 683
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static t(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 691
    if-nez p0, :cond_1

    .line 692
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    :cond_0
    :goto_0
    return v2

    .line 696
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 697
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 698
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 699
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 701
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 703
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyPopArt([I[IIII)I

    move-result v9

    .line 705
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 706
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 712
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 708
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static u(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 716
    if-nez p0, :cond_1

    .line 717
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    :cond_0
    :goto_0
    return v2

    .line 721
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 722
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 723
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 724
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 726
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 728
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyRetro([I[IIII)I

    move-result v9

    .line 730
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 731
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 737
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 733
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static v(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 741
    if-nez p0, :cond_1

    .line 742
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    :cond_0
    :goto_0
    return v2

    .line 746
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 747
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 748
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 749
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 751
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 753
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applySunshine([I[IIII)I

    move-result v9

    .line 755
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 756
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 762
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 758
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static w(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 766
    if-nez p0, :cond_1

    .line 767
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    :cond_0
    :goto_0
    return v2

    .line 771
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 772
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 773
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 774
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 776
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 778
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyDownlight([I[IIII)I

    move-result v9

    .line 780
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 781
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 787
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 783
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static x(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 791
    if-nez p0, :cond_1

    .line 792
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    :cond_0
    :goto_0
    return v2

    .line 796
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 797
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 798
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 799
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 801
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 803
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyBluewash([I[IIII)I

    move-result v9

    .line 805
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 806
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 812
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 808
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static y(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 816
    if-nez p0, :cond_1

    .line 817
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    :cond_0
    :goto_0
    return v2

    .line 821
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 822
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 823
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 824
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 826
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 828
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyNostalgia([I[IIII)I

    move-result v9

    .line 830
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 831
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 837
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 833
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static z(Landroid/graphics/Bitmap;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 841
    if-nez p0, :cond_1

    .line 842
    const-string v0, "ImageEffectEngine"

    const-string v1, "Input Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    :cond_0
    :goto_0
    return v2

    .line 846
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 847
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 848
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 849
    mul-int v0, v3, v7

    new-array v8, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 851
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 853
    invoke-static {v1, v8, v3, v7, v2}, Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;->applyYellowglow([I[IIII)I

    move-result v9

    .line 855
    if-nez v9, :cond_2

    move-object v0, p0

    move-object v1, v8

    move v4, v2

    move v5, v2

    move v6, v3

    .line 856
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 862
    :goto_1
    if-nez v9, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 858
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ImageEffectEngine] error mesaage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageEffectEngine"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
