.class public Lcom/sec/vip/imagefilter/ImageEffectEngineJNI;
.super Ljava/lang/Object;
.source "ImageEffectEngineJNI.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    const-string v0, "ChatOnAMSImageFilterLibs-1.0.2"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public static native applyBluewash([I[IIII)I
.end method

.method public static native applyBlur([I[IIII)I
.end method

.method public static native applyBright([I[IIII)I
.end method

.method public static native applyCartoonize([I[IIII)I
.end method

.method public static native applyClassic([I[IIII)I
.end method

.method public static native applyColorSketch([I[IIII)I
.end method

.method public static native applyColorize([I[IIII)I
.end method

.method public static native applyDark([I[IIII)I
.end method

.method public static native applyDownlight([I[IIII)I
.end method

.method public static native applyFadedColor([I[IIII)I
.end method

.method public static native applyFusain([I[IIII)I
.end method

.method public static native applyGray([I[IIII)I
.end method

.method public static native applyMagicPen([I[IIII)I
.end method

.method public static native applyMosaic([I[IIII)I
.end method

.method public static native applyNegative([I[IIII)I
.end method

.method public static native applyNostalgia([I[IIII)I
.end method

.method public static native applyOilpaint([I[IIII)I
.end method

.method public static native applyOldPhoto([I[IIII)I
.end method

.method public static native applyPastelSketch([I[IIII)I
.end method

.method public static native applyPenSketch([I[IIII)I
.end method

.method public static native applyPencilColorSketch([I[IIII)I
.end method

.method public static native applyPencilPastelSketch([I[IIII)I
.end method

.method public static native applyPencilSketch([I[IIII)I
.end method

.method public static native applyPopArt([I[IIII)I
.end method

.method public static native applyPosterize([I[IIII)I
.end method

.method public static native applyRetro([I[IIII)I
.end method

.method public static native applySepia([I[IIII)I
.end method

.method public static native applySoftglow([I[IIII)I
.end method

.method public static native applySunshine([I[IIII)I
.end method

.method public static native applyVignette([I[IIII)I
.end method

.method public static native applyVintage([I[IIII)I
.end method

.method public static native applyVivid([I[IIII)I
.end method

.method public static native applyYellowglow([I[IIII)I
.end method
