.class Lcom/sec/vip/cropimage/ImageModifyView;
.super Lcom/sec/vip/cropimage/ImageViewTouchBase;
.source "ImageModify.java"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/vip/cropimage/p;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/sec/vip/cropimage/p;

.field c:F

.field d:F

.field e:I

.field f:Z

.field g:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1489
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1458
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    .line 1459
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    .line 1462
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->f:Z

    .line 1490
    return-void
.end method

.method private a(FFFFF)Landroid/graphics/PointF;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1611
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1612
    float-to-int v1, p5

    sparse-switch v1, :sswitch_data_0

    .line 1624
    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 1627
    :goto_0
    return-object v0

    .line 1614
    :sswitch_0
    sub-float v1, p3, p1

    invoke-virtual {v0, p2, v1}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1617
    :sswitch_1
    sub-float v1, p3, p1

    sub-float v2, p4, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1620
    :sswitch_2
    sub-float v1, p4, p2

    invoke-virtual {v0, v1, p1}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1612
    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch
.end method

.method private b(Lcom/sec/vip/cropimage/p;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1637
    iget-object v5, p1, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    .line 1639
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getLeft()I

    move-result v0

    iget v1, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1640
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getRight()I

    move-result v0

    iget v1, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1642
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getTop()I

    move-result v0

    iget v1, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1643
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getBottom()I

    move-result v1

    iget v6, v5, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v6

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1645
    if-eqz v3, :cond_2

    .line 1646
    :goto_0
    if-eqz v0, :cond_3

    .line 1648
    :goto_1
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getWidth()I

    move-result v4

    if-le v1, v4, :cond_5

    move v1, v2

    .line 1651
    :goto_2
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getHeight()I

    move-result v4

    if-le v3, v4, :cond_4

    .line 1654
    :goto_3
    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    .line 1655
    :cond_0
    int-to-float v0, v1

    int-to-float v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/cropimage/ImageModifyView;->b(FF)V

    .line 1657
    :cond_1
    return-void

    :cond_2
    move v3, v4

    .line 1645
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1646
    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_3

    :cond_5
    move v1, v3

    goto :goto_2
.end method

.method private c(Lcom/sec/vip/cropimage/p;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, 0x3f19999a    # 0.6f

    .line 1667
    iget-object v0, p1, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    .line 1669
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    .line 1670
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    .line 1672
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    .line 1673
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    .line 1675
    div-float v1, v2, v1

    mul-float/2addr v1, v4

    .line 1676
    div-float v0, v3, v0

    mul-float/2addr v0, v4

    .line 1678
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1679
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->a()F

    move-result v1

    mul-float/2addr v0, v1

    .line 1680
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1682
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->a()F

    move-result v1

    sub-float v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v1, v1

    const-wide v3, 0x3fb999999999999aL    # 0.1

    cmpl-double v1, v1, v3

    if-lez v1, :cond_0

    .line 1683
    const/4 v1, 0x2

    new-array v1, v1, [F

    iget-object v2, p1, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    aput v2, v1, v5

    iget-object v2, p1, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    aput v2, v1, v6

    .line 1684
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1685
    aget v2, v1, v5

    aget v1, v1, v6

    const/high16 v3, 0x43960000    # 300.0f

    invoke-virtual {p0, v0, v2, v1, v3}, Lcom/sec/vip/cropimage/ImageModifyView;->a(FFFF)V

    .line 1688
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/vip/cropimage/ImageModifyView;->b(Lcom/sec/vip/cropimage/p;)V

    .line 1689
    return-void
.end method


# virtual methods
.method protected a(FF)V
    .locals 3

    .prologue
    .line 1521
    invoke-super {p0, p1, p2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(FF)V

    .line 1522
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1523
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    .line 1524
    iget-object v2, v0, Lcom/sec/vip/cropimage/p;->f:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1525
    invoke-virtual {v0}, Lcom/sec/vip/cropimage/p;->c()V

    .line 1522
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1527
    :cond_0
    return-void
.end method

.method protected a(FFF)V
    .locals 4

    .prologue
    .line 1494
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(FFF)V

    .line 1495
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    .line 1496
    iget-object v2, v0, Lcom/sec/vip/cropimage/p;->f:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1497
    invoke-virtual {v0}, Lcom/sec/vip/cropimage/p;->c()V

    goto :goto_0

    .line 1499
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/vip/cropimage/p;)V
    .locals 1

    .prologue
    .line 1707
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1708
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->invalidate()V

    .line 1709
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1713
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1714
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1693
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->onDraw(Landroid/graphics/Canvas;)V

    .line 1694
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1695
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    invoke-virtual {v0, p1}, Lcom/sec/vip/cropimage/p;->a(Landroid/graphics/Canvas;)V

    .line 1696
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/ImageModifyView;->b(Lcom/sec/vip/cropimage/p;)V

    .line 1694
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1698
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 1468
    invoke-super/range {p0 .. p5}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->onLayout(ZIIII)V

    .line 1469
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/al;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1470
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    .line 1471
    iget-object v2, v0, Lcom/sec/vip/cropimage/p;->f:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1472
    invoke-virtual {v0}, Lcom/sec/vip/cropimage/p;->c()V

    .line 1473
    iget-boolean v2, v0, Lcom/sec/vip/cropimage/p;->b:Z

    if-eqz v2, :cond_0

    .line 1474
    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/ImageModifyView;->c(Lcom/sec/vip/cropimage/p;)V

    goto :goto_0

    .line 1478
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/16 v5, 0x20

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 1531
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->g:Landroid/content/Context;

    move-object v6, v0

    check-cast v6, Lcom/sec/vip/cropimage/ImageModify;

    .line 1532
    iget-boolean v0, v6, Lcom/sec/vip/cropimage/ImageModify;->a:Z

    if-eqz v0, :cond_0

    .line 1607
    :goto_0
    return v7

    .line 1536
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 1537
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1539
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1592
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :cond_2
    :goto_2
    move v7, v8

    .line 1607
    goto :goto_0

    :pswitch_0
    move v1, v7

    .line 1541
    :goto_3
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1542
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    .line 1543
    invoke-virtual {v0, v2, v3}, Lcom/sec/vip/cropimage/p;->a(FF)I

    move-result v4

    .line 1544
    if-eq v4, v8, :cond_4

    .line 1545
    iput v4, p0, Lcom/sec/vip/cropimage/ImageModifyView;->e:I

    .line 1546
    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    .line 1547
    iput v2, p0, Lcom/sec/vip/cropimage/ImageModifyView;->c:F

    .line 1548
    iput v3, p0, Lcom/sec/vip/cropimage/ImageModifyView;->d:F

    .line 1549
    iput-boolean v8, p0, Lcom/sec/vip/cropimage/ImageModifyView;->f:Z

    .line 1550
    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    if-ne v4, v5, :cond_3

    sget-object v0, Lcom/sec/vip/cropimage/q;->b:Lcom/sec/vip/cropimage/q;

    :goto_4
    invoke-virtual {v1, v0}, Lcom/sec/vip/cropimage/p;->a(Lcom/sec/vip/cropimage/q;)V

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/sec/vip/cropimage/q;->c:Lcom/sec/vip/cropimage/q;

    goto :goto_4

    .line 1541
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1556
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    if-eqz v0, :cond_5

    .line 1557
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/ImageModifyView;->c(Lcom/sec/vip/cropimage/p;)V

    .line 1558
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    sget-object v1, Lcom/sec/vip/cropimage/q;->a:Lcom/sec/vip/cropimage/q;

    invoke-virtual {v0, v1}, Lcom/sec/vip/cropimage/p;->a(Lcom/sec/vip/cropimage/q;)V

    .line 1560
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    goto :goto_1

    .line 1563
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    if-eqz v0, :cond_1

    .line 1565
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->e:I

    if-ne v0, v5, :cond_7

    .line 1566
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/ImageModify;->a()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/ImageModify;->b()I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/ImageModify;->c()F

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/cropimage/ImageModifyView;->a(FFFFF)Landroid/graphics/PointF;

    move-result-object v9

    .line 1567
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->f:Z

    if-eqz v0, :cond_6

    .line 1568
    iget v1, p0, Lcom/sec/vip/cropimage/ImageModifyView;->c:F

    iget v2, p0, Lcom/sec/vip/cropimage/ImageModifyView;->d:F

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/ImageModify;->a()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/ImageModify;->b()I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/ImageModify;->c()F

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/cropimage/ImageModifyView;->a(FFFFF)Landroid/graphics/PointF;

    move-result-object v0

    .line 1569
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iput v1, p0, Lcom/sec/vip/cropimage/ImageModifyView;->c:F

    .line 1570
    iget v0, v0, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->d:F

    .line 1571
    iput-boolean v7, p0, Lcom/sec/vip/cropimage/ImageModifyView;->f:Z

    .line 1573
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    iget v1, p0, Lcom/sec/vip/cropimage/ImageModifyView;->e:I

    iget v2, v9, Landroid/graphics/PointF;->x:F

    iget v3, p0, Lcom/sec/vip/cropimage/ImageModifyView;->c:F

    sub-float/2addr v2, v3

    iget v3, v9, Landroid/graphics/PointF;->y:F

    iget v4, p0, Lcom/sec/vip/cropimage/ImageModifyView;->d:F

    sub-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/vip/cropimage/p;->a(IFF)V

    .line 1574
    iget v0, v9, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->c:F

    .line 1575
    iget v0, v9, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->d:F

    .line 1587
    :goto_5
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/ImageModifyView;->b(Lcom/sec/vip/cropimage/p;)V

    goto/16 :goto_1

    .line 1577
    :cond_7
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModifyView;->b:Lcom/sec/vip/cropimage/p;

    iget v1, p0, Lcom/sec/vip/cropimage/ImageModifyView;->e:I

    iget v4, p0, Lcom/sec/vip/cropimage/ImageModifyView;->c:F

    sub-float v4, v2, v4

    iget v5, p0, Lcom/sec/vip/cropimage/ImageModifyView;->d:F

    sub-float v5, v3, v5

    invoke-virtual {v0, v1, v4, v5}, Lcom/sec/vip/cropimage/p;->a(IFF)V

    .line 1578
    iput v2, p0, Lcom/sec/vip/cropimage/ImageModifyView;->c:F

    .line 1579
    iput v3, p0, Lcom/sec/vip/cropimage/ImageModifyView;->d:F

    goto :goto_5

    .line 1594
    :pswitch_3
    invoke-virtual {p0, v8, v8}, Lcom/sec/vip/cropimage/ImageModifyView;->a(ZZ)V

    goto/16 :goto_2

    .line 1601
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModifyView;->a()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 1602
    invoke-virtual {p0, v8, v8}, Lcom/sec/vip/cropimage/ImageModifyView;->a(ZZ)V

    goto/16 :goto_2

    .line 1539
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1592
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
