.class public abstract Lcom/sec/vip/cropimage/MonitoredActivity;
.super Lcom/sec/chaton/base/BaseActivity;
.source "MonitoredActivity.java"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/vip/cropimage/ak;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseActivity;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/MonitoredActivity;->a:Ljava/util/ArrayList;

    .line 61
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 200
    const-string v0, "showPasswordLockActivity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/MonitoredActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 203
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 205
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    invoke-virtual {p0, v1}, Lcom/sec/vip/cropimage/MonitoredActivity;->startActivity(Landroid/content/Intent;)V

    .line 209
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public a(Lcom/sec/vip/cropimage/ak;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/vip/cropimage/MonitoredActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/MonitoredActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public abstract b()I
.end method

.method public b(Lcom/sec/vip/cropimage/ak;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/vip/cropimage/MonitoredActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method

.method public abstract c()F
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 104
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    iget-object v0, p0, Lcom/sec/vip/cropimage/MonitoredActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/ak;

    .line 106
    invoke-interface {v0, p0}, Lcom/sec/vip/cropimage/ak;->d(Lcom/sec/vip/cropimage/MonitoredActivity;)V

    goto :goto_0

    .line 108
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onDestroy()V

    .line 113
    iget-object v0, p0, Lcom/sec/vip/cropimage/MonitoredActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/ak;

    .line 114
    invoke-interface {v0, p0}, Lcom/sec/vip/cropimage/ak;->a(Lcom/sec/vip/cropimage/MonitoredActivity;)V

    goto :goto_0

    .line 116
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 194
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onResume()V

    .line 196
    invoke-direct {p0}, Lcom/sec/vip/cropimage/MonitoredActivity;->d()V

    .line 197
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onStart()V

    .line 121
    iget-object v0, p0, Lcom/sec/vip/cropimage/MonitoredActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/ak;

    .line 122
    invoke-interface {v0, p0}, Lcom/sec/vip/cropimage/ak;->c(Lcom/sec/vip/cropimage/MonitoredActivity;)V

    goto :goto_0

    .line 124
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Lcom/sec/chaton/base/BaseActivity;->onStop()V

    .line 129
    iget-object v0, p0, Lcom/sec/vip/cropimage/MonitoredActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/ak;

    .line 130
    invoke-interface {v0, p0}, Lcom/sec/vip/cropimage/ak;->b(Lcom/sec/vip/cropimage/MonitoredActivity;)V

    goto :goto_0

    .line 132
    :cond_0
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 137
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/MonitoredActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 177
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
