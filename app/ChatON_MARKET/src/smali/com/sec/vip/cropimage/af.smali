.class Lcom/sec/vip/cropimage/af;
.super Landroid/os/AsyncTask;
.source "ImageModify.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Lcom/sec/vip/cropimage/ae;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/vip/cropimage/ImageModify;

.field private b:Lcom/sec/vip/cropimage/ImageModify;


# direct methods
.method public constructor <init>(Lcom/sec/vip/cropimage/ImageModify;Lcom/sec/vip/cropimage/ImageModify;)V
    .locals 1

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/vip/cropimage/af;->a:Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    .line 157
    iput-object p2, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    .line 158
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Lcom/sec/vip/cropimage/ae;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/ImageModify;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/ImageModify;->d()Lcom/sec/vip/cropimage/ae;

    move-result-object v0

    .line 166
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/vip/cropimage/ae;->b:Lcom/sec/vip/cropimage/ae;

    goto :goto_0
.end method

.method protected a(Lcom/sec/vip/cropimage/ae;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 182
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/ImageModify;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v0}, Lcom/sec/vip/cropimage/ImageModify;->b(Lcom/sec/vip/cropimage/ImageModify;)V

    .line 185
    sget-object v0, Lcom/sec/vip/cropimage/ab;->a:[I

    invoke-virtual {p1}, Lcom/sec/vip/cropimage/ae;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 187
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v0}, Lcom/sec/vip/cropimage/ImageModify;->c(Lcom/sec/vip/cropimage/ImageModify;)V

    goto :goto_0

    .line 190
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->a:Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/ImageModify;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b016c

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 191
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0, v2}, Lcom/sec/vip/cropimage/ImageModify;->setResult(I)V

    .line 192
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    goto :goto_0

    .line 195
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->a:Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/ImageModify;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0149

    invoke-static {v0, v1, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 196
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0, v2}, Lcom/sec/vip/cropimage/ImageModify;->setResult(I)V

    .line 197
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    goto :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 153
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/vip/cropimage/af;->a([Ljava/lang/String;)Lcom/sec/vip/cropimage/ae;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 153
    check-cast p1, Lcom/sec/vip/cropimage/ae;

    invoke-virtual {p0, p1}, Lcom/sec/vip/cropimage/af;->a(Lcom/sec/vip/cropimage/ae;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 173
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 174
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/ImageModify;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sec/vip/cropimage/af;->b:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v0}, Lcom/sec/vip/cropimage/ImageModify;->a(Lcom/sec/vip/cropimage/ImageModify;)V

    .line 177
    :cond_0
    return-void
.end method
