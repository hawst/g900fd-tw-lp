.class final enum Lcom/sec/vip/cropimage/q;
.super Ljava/lang/Enum;
.source "HighlightView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/vip/cropimage/q;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/vip/cropimage/q;

.field public static final enum b:Lcom/sec/vip/cropimage/q;

.field public static final enum c:Lcom/sec/vip/cropimage/q;

.field private static final synthetic d:[Lcom/sec/vip/cropimage/q;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 706
    new-instance v0, Lcom/sec/vip/cropimage/q;

    const-string v1, "None"

    invoke-direct {v0, v1, v2}, Lcom/sec/vip/cropimage/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/vip/cropimage/q;->a:Lcom/sec/vip/cropimage/q;

    .line 707
    new-instance v0, Lcom/sec/vip/cropimage/q;

    const-string v1, "Move"

    invoke-direct {v0, v1, v3}, Lcom/sec/vip/cropimage/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/vip/cropimage/q;->b:Lcom/sec/vip/cropimage/q;

    .line 708
    new-instance v0, Lcom/sec/vip/cropimage/q;

    const-string v1, "Grow"

    invoke-direct {v0, v1, v4}, Lcom/sec/vip/cropimage/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/vip/cropimage/q;->c:Lcom/sec/vip/cropimage/q;

    .line 705
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/vip/cropimage/q;

    sget-object v1, Lcom/sec/vip/cropimage/q;->a:Lcom/sec/vip/cropimage/q;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/vip/cropimage/q;->b:Lcom/sec/vip/cropimage/q;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/vip/cropimage/q;->c:Lcom/sec/vip/cropimage/q;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/vip/cropimage/q;->d:[Lcom/sec/vip/cropimage/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 705
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/vip/cropimage/q;
    .locals 1

    .prologue
    .line 705
    const-class v0, Lcom/sec/vip/cropimage/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/q;

    return-object v0
.end method

.method public static values()[Lcom/sec/vip/cropimage/q;
    .locals 1

    .prologue
    .line 705
    sget-object v0, Lcom/sec/vip/cropimage/q;->d:[Lcom/sec/vip/cropimage/q;

    invoke-virtual {v0}, [Lcom/sec/vip/cropimage/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/vip/cropimage/q;

    return-object v0
.end method
