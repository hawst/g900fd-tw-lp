.class Lcom/sec/vip/cropimage/g;
.super Ljava/lang/Object;
.source "CropImage.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/vip/cropimage/f;


# direct methods
.method constructor <init>(Lcom/sec/vip/cropimage/f;)V
    .locals 0

    .prologue
    .line 801
    iput-object p1, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 805
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v3, :cond_0

    .line 806
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    invoke-static {v0}, Lcom/sec/vip/cropimage/f;->a(Lcom/sec/vip/cropimage/f;)V

    .line 808
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/CropImageView;->invalidate()V

    .line 809
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 810
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v2, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    iput-object v0, v2, Lcom/sec/vip/cropimage/CropImage;->a:Lcom/sec/vip/cropimage/p;

    .line 811
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    iget-object v0, v0, Lcom/sec/vip/cropimage/CropImage;->a:Lcom/sec/vip/cropimage/p;

    invoke-virtual {v0, v3}, Lcom/sec/vip/cropimage/p;->a(Z)V

    .line 814
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    iget-object v0, v0, Lcom/sec/vip/cropimage/CropImage;->a:Lcom/sec/vip/cropimage/p;

    iget-object v2, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v2, v2, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v2}, Lcom/sec/vip/cropimage/CropImage;->n(Lcom/sec/vip/cropimage/CropImage;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/vip/cropimage/p;->a(I)V

    .line 816
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    iget-object v0, v0, Lcom/sec/vip/cropimage/CropImage;->a:Lcom/sec/vip/cropimage/p;

    iget-object v2, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v2, v2, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v2}, Lcom/sec/vip/cropimage/CropImage;->o(Lcom/sec/vip/cropimage/CropImage;)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v3, v3, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v3}, Lcom/sec/vip/cropimage/CropImage;->d(Lcom/sec/vip/cropimage/CropImage;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v4, v4, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v4}, Lcom/sec/vip/cropimage/CropImage;->h(Lcom/sec/vip/cropimage/CropImage;)F

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/vip/cropimage/p;->a(Landroid/graphics/Bitmap;IF)V

    .line 820
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/CropImageView;->getMeasuredWidth()I

    move-result v4

    .line 821
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/CropImageView;->getMeasuredHeight()I

    move-result v5

    .line 822
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;

    move-result-object v0

    move v2, v1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/cropimage/CropImageView;->onLayout(ZIIII)V

    .line 825
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->p(Lcom/sec/vip/cropimage/CropImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 826
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->p(Lcom/sec/vip/cropimage/CropImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 827
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->p(Lcom/sec/vip/cropimage/CropImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 828
    iget-object v0, p0, Lcom/sec/vip/cropimage/g;->a:Lcom/sec/vip/cropimage/f;

    iget-object v0, v0, Lcom/sec/vip/cropimage/f;->c:Lcom/sec/vip/cropimage/CropImage;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/vip/cropimage/CropImage;->b(Lcom/sec/vip/cropimage/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 836
    :cond_2
    return-void
.end method
