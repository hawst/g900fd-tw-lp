.class abstract Lcom/sec/vip/cropimage/ImageViewTouchBase;
.super Landroid/widget/ImageView;
.source "ImageViewTouchBase.java"


# instance fields
.field private final a:Landroid/graphics/Matrix;

.field private final b:[F

.field private c:Lcom/sec/vip/cropimage/ai;

.field private d:Ljava/lang/Runnable;

.field protected h:Landroid/graphics/Matrix;

.field protected i:Landroid/graphics/Matrix;

.field protected final j:Lcom/sec/vip/cropimage/al;

.field k:I

.field l:I

.field m:F

.field protected n:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 280
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 41
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->h:Landroid/graphics/Matrix;

    .line 48
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    .line 52
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    .line 55
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->b:[F

    .line 58
    new-instance v0, Lcom/sec/vip/cropimage/al;

    invoke-direct {v0, v2}, Lcom/sec/vip/cropimage/al;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    .line 60
    iput v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->k:I

    iput v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->l:I

    .line 124
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->n:Landroid/os/Handler;

    .line 168
    iput-object v2, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->d:Ljava/lang/Runnable;

    .line 281
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->e()V

    .line 282
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 293
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->h:Landroid/graphics/Matrix;

    .line 48
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    .line 52
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    .line 55
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->b:[F

    .line 58
    new-instance v0, Lcom/sec/vip/cropimage/al;

    invoke-direct {v0, v2}, Lcom/sec/vip/cropimage/al;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    .line 60
    iput v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->k:I

    iput v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->l:I

    .line 124
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->n:Landroid/os/Handler;

    .line 168
    iput-object v2, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->d:Ljava/lang/Runnable;

    .line 294
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->e()V

    .line 295
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;I)V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 144
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 146
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/al;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v1, p1}, Lcom/sec/vip/cropimage/al;->a(Landroid/graphics/Bitmap;)V

    .line 151
    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v1, p2}, Lcom/sec/vip/cropimage/al;->a(I)V

    .line 154
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 156
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 159
    :cond_1
    return-void
.end method

.method private a(Lcom/sec/vip/cropimage/al;Landroid/graphics/Matrix;)V
    .locals 8

    .prologue
    const/high16 v7, 0x40400000    # 3.0f

    const/high16 v6, 0x40000000    # 2.0f

    .line 350
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 351
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 353
    invoke-virtual {p1}, Lcom/sec/vip/cropimage/al;->f()I

    move-result v2

    int-to-float v2, v2

    .line 354
    invoke-virtual {p1}, Lcom/sec/vip/cropimage/al;->e()I

    move-result v3

    int-to-float v3, v3

    .line 355
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 359
    div-float v4, v0, v2

    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 360
    div-float v5, v1, v3

    invoke-static {v5, v7}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 361
    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 363
    invoke-virtual {p1}, Lcom/sec/vip/cropimage/al;->c()Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {p2, v5}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 364
    invoke-virtual {p2, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 366
    mul-float/2addr v2, v4

    sub-float/2addr v0, v2

    div-float/2addr v0, v6

    mul-float v2, v3, v4

    sub-float/2addr v1, v2

    div-float/2addr v1, v6

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 367
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 301
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 302
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method protected a(Landroid/graphics/Matrix;)F
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(Landroid/graphics/Matrix;I)F

    move-result v0

    return v0
.end method

.method protected a(Landroid/graphics/Matrix;I)F
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->b:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 315
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->b:[F

    array-length v0, v0

    if-ge p2, v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->b:[F

    aget v0, v0, p2

    .line 318
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 466
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 467
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    .line 469
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(FFF)V

    .line 470
    return-void
.end method

.method protected a(FF)V
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 564
    return-void
.end method

.method protected a(FFF)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 415
    iget v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->m:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 416
    iget p1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->m:F

    .line 419
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a()F

    move-result v0

    .line 420
    div-float v0, p1, v0

    .line 422
    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v0, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 423
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->c()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 424
    invoke-virtual {p0, v2, v2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(ZZ)V

    .line 425
    return-void
.end method

.method protected a(FFFF)V
    .locals 10

    .prologue
    .line 440
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a()F

    move-result v0

    sub-float v0, p1, v0

    div-float v6, v0, p4

    .line 441
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a()F

    move-result v5

    .line 442
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 444
    iget-object v9, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->n:Landroid/os/Handler;

    new-instance v0, Lcom/sec/vip/cropimage/ah;

    move-object v1, p0

    move v2, p4

    move v7, p2

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/sec/vip/cropimage/ah;-><init>(Lcom/sec/vip/cropimage/ImageViewTouchBase;FJFFFF)V

    invoke-virtual {v9, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 457
    return-void
.end method

.method public a(ZZ)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 232
    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v1}, Lcom/sec/vip/cropimage/al;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_0

    .line 271
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->c()Landroid/graphics/Matrix;

    move-result-object v1

    .line 238
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v3}, Lcom/sec/vip/cropimage/al;->b()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v4}, Lcom/sec/vip/cropimage/al;->b()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v2, v0, v0, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 240
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 242
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 243
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v3

    .line 247
    if-eqz p2, :cond_6

    .line 248
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->getHeight()I

    move-result v4

    .line 249
    int-to-float v5, v4

    cmpg-float v5, v1, v5

    if-gez v5, :cond_2

    .line 250
    int-to-float v4, v4

    sub-float v1, v4, v1

    div-float/2addr v1, v6

    iget v4, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v4

    .line 258
    :goto_1
    if-eqz p1, :cond_1

    .line 259
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->getWidth()I

    move-result v4

    .line 260
    int-to-float v5, v4

    cmpg-float v5, v3, v5

    if-gez v5, :cond_4

    .line 261
    int-to-float v0, v4

    sub-float/2addr v0, v3

    div-float/2addr v0, v6

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v2

    .line 269
    :cond_1
    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(FF)V

    .line 270
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->c()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 251
    :cond_2
    iget v1, v2, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_3

    .line 252
    iget v1, v2, Landroid/graphics/RectF;->top:F

    neg-float v1, v1

    goto :goto_1

    .line 253
    :cond_3
    iget v1, v2, Landroid/graphics/RectF;->bottom:F

    int-to-float v4, v4

    cmpg-float v1, v1, v4

    if-gez v1, :cond_6

    .line 254
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v4

    goto :goto_1

    .line 262
    :cond_4
    iget v3, v2, Landroid/graphics/RectF;->left:F

    cmpl-float v3, v3, v0

    if-lez v3, :cond_5

    .line 263
    iget v0, v2, Landroid/graphics/RectF;->left:F

    neg-float v0, v0

    goto :goto_2

    .line 264
    :cond_5
    iget v3, v2, Landroid/graphics/RectF;->right:F

    int-to-float v5, v4

    cmpg-float v3, v3, v5

    if-gez v3, :cond_1

    .line 265
    int-to-float v0, v4

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v2

    goto :goto_2

    :cond_6
    move v1, v0

    goto :goto_1
.end method

.method public b()V
    .locals 3

    .prologue
    .line 165
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageBitmapResetBase(Landroid/graphics/Bitmap;IZ)V

    .line 166
    return-void
.end method

.method protected b(FF)V
    .locals 1

    .prologue
    .line 575
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(FF)V

    .line 576
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->c()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 577
    return-void
.end method

.method protected c()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 378
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 379
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    return-object v0
.end method

.method protected d()F
    .locals 3

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/al;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 395
    const/high16 v0, 0x3f800000    # 1.0f

    .line 401
    :goto_0
    return v0

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/al;->f()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->k:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 399
    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v1}, Lcom/sec/vip/cropimage/al;->e()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->l:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 400
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    .line 401
    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 104
    const/4 v0, 0x1

    .line 106
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 111
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a()F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 117
    invoke-virtual {p0, v1}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(F)V

    .line 118
    const/4 v0, 0x1

    .line 121
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 86
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 87
    sub-int v0, p4, p2

    iput v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->k:I

    .line 88
    sub-int v0, p5, p3

    iput v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->l:I

    .line 89
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->d:Ljava/lang/Runnable;

    .line 90
    if-eqz v0, :cond_0

    .line 91
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->d:Ljava/lang/Runnable;

    .line 92
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/al;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->j:Lcom/sec/vip/cropimage/al;

    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->h:Landroid/graphics/Matrix;

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(Lcom/sec/vip/cropimage/al;Landroid/graphics/Matrix;)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->c()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 98
    :cond_1
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(Landroid/graphics/Bitmap;I)V

    .line 132
    return-void
.end method

.method public setImageBitmapResetBase(Landroid/graphics/Bitmap;IZ)V
    .locals 1

    .prologue
    .line 180
    new-instance v0, Lcom/sec/vip/cropimage/al;

    invoke-direct {v0, p1, p2}, Lcom/sec/vip/cropimage/al;-><init>(Landroid/graphics/Bitmap;I)V

    invoke-virtual {p0, v0, p3}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageRotateBitmapResetBase(Lcom/sec/vip/cropimage/al;Z)V

    .line 181
    return-void
.end method

.method public setImageRotateBitmapResetBase(Lcom/sec/vip/cropimage/al;Z)V
    .locals 2

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->getWidth()I

    move-result v0

    .line 194
    if-gtz v0, :cond_0

    .line 195
    new-instance v0, Lcom/sec/vip/cropimage/ag;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/vip/cropimage/ag;-><init>(Lcom/sec/vip/cropimage/ImageViewTouchBase;Lcom/sec/vip/cropimage/al;Z)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->d:Ljava/lang/Runnable;

    .line 217
    :goto_0
    return-void

    .line 204
    :cond_0
    invoke-virtual {p1}, Lcom/sec/vip/cropimage/al;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 205
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->h:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(Lcom/sec/vip/cropimage/al;Landroid/graphics/Matrix;)V

    .line 206
    invoke-virtual {p1}, Lcom/sec/vip/cropimage/al;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/vip/cropimage/al;->a()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(Landroid/graphics/Bitmap;I)V

    .line 212
    :goto_1
    if-eqz p2, :cond_1

    .line 213
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->c()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 216
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->d()F

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->m:F

    goto :goto_0

    .line 208
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 209
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public setRecycler(Lcom/sec/vip/cropimage/ai;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/vip/cropimage/ImageViewTouchBase;->c:Lcom/sec/vip/cropimage/ai;

    .line 80
    return-void
.end method
