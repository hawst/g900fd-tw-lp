.class public Lcom/sec/vip/cropimage/l;
.super Landroid/os/AsyncTask;
.source "CropImage.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/vip/cropimage/CropImage;

.field private b:Landroid/view/View;

.field private c:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lcom/sec/vip/cropimage/CropImage;Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1528
    iput-object p1, p0, Lcom/sec/vip/cropimage/l;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1529
    iput-object p2, p0, Lcom/sec/vip/cropimage/l;->b:Landroid/view/View;

    .line 1530
    iput-object p3, p0, Lcom/sec/vip/cropimage/l;->c:Landroid/graphics/Bitmap;

    .line 1531
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 1546
    iget-object v0, p0, Lcom/sec/vip/cropimage/l;->a:Lcom/sec/vip/cropimage/CropImage;

    iget-object v1, p0, Lcom/sec/vip/cropimage/l;->c:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/cropimage/l;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/vip/cropimage/CropImage;->a(Lcom/sec/vip/cropimage/CropImage;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1552
    if-eqz p1, :cond_0

    .line 1554
    iget-object v0, p0, Lcom/sec/vip/cropimage/l;->a:Lcom/sec/vip/cropimage/CropImage;

    iget-object v1, p0, Lcom/sec/vip/cropimage/l;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v1}, Lcom/sec/vip/cropimage/CropImage;->o(Lcom/sec/vip/cropimage/CropImage;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/vip/cropimage/CropImage;->b(Lcom/sec/vip/cropimage/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1556
    iget-object v0, p0, Lcom/sec/vip/cropimage/l;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0, p1}, Lcom/sec/vip/cropimage/CropImage;->c(Lcom/sec/vip/cropimage/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1557
    iget-object v0, p0, Lcom/sec/vip/cropimage/l;->a:Lcom/sec/vip/cropimage/CropImage;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/vip/cropimage/CropImage;->d(Lcom/sec/vip/cropimage/CropImage;Z)V

    .line 1559
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/l;->a:Lcom/sec/vip/cropimage/CropImage;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/vip/cropimage/CropImage;->e(Lcom/sec/vip/cropimage/CropImage;Z)Z

    .line 1560
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1561
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1524
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/vip/cropimage/l;->a([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1524
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/sec/vip/cropimage/l;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1535
    iget-object v0, p0, Lcom/sec/vip/cropimage/l;->a:Lcom/sec/vip/cropimage/CropImage;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/vip/cropimage/CropImage;->e(Lcom/sec/vip/cropimage/CropImage;Z)Z

    .line 1536
    iget-object v0, p0, Lcom/sec/vip/cropimage/l;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->r(Lcom/sec/vip/cropimage/CropImage;)V

    .line 1538
    iget-object v0, p0, Lcom/sec/vip/cropimage/l;->b:Landroid/view/View;

    const v1, 0x7f070062

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1539
    iget-object v0, p0, Lcom/sec/vip/cropimage/l;->b:Landroid/view/View;

    const v1, 0x7f070061

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1540
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1541
    return-void
.end method
