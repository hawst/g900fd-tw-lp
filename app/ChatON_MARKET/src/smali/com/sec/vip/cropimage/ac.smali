.class Lcom/sec/vip/cropimage/ac;
.super Lcom/sec/vip/cropimage/aj;
.source "ImageModify.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/sec/vip/cropimage/MonitoredActivity;

.field private final b:Ljava/lang/Runnable;

.field private final c:Landroid/os/Handler;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/sec/vip/cropimage/MonitoredActivity;Ljava/lang/Runnable;Landroid/app/ProgressDialog;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 649
    invoke-direct {p0}, Lcom/sec/vip/cropimage/aj;-><init>()V

    .line 624
    new-instance v0, Lcom/sec/vip/cropimage/ad;

    invoke-direct {v0, p0}, Lcom/sec/vip/cropimage/ad;-><init>(Lcom/sec/vip/cropimage/ac;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ac;->d:Ljava/lang/Runnable;

    .line 650
    iput-object p1, p0, Lcom/sec/vip/cropimage/ac;->a:Lcom/sec/vip/cropimage/MonitoredActivity;

    .line 652
    iput-object p2, p0, Lcom/sec/vip/cropimage/ac;->b:Ljava/lang/Runnable;

    .line 653
    iget-object v0, p0, Lcom/sec/vip/cropimage/ac;->a:Lcom/sec/vip/cropimage/MonitoredActivity;

    invoke-virtual {v0, p0}, Lcom/sec/vip/cropimage/MonitoredActivity;->a(Lcom/sec/vip/cropimage/ak;)V

    .line 654
    iput-object p4, p0, Lcom/sec/vip/cropimage/ac;->c:Landroid/os/Handler;

    .line 655
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/cropimage/ac;)Lcom/sec/vip/cropimage/MonitoredActivity;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/sec/vip/cropimage/ac;->a:Lcom/sec/vip/cropimage/MonitoredActivity;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/vip/cropimage/MonitoredActivity;)V
    .locals 2

    .prologue
    .line 673
    iget-object v0, p0, Lcom/sec/vip/cropimage/ac;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 674
    iget-object v0, p0, Lcom/sec/vip/cropimage/ac;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/vip/cropimage/ac;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 675
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 663
    :try_start_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ac;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 665
    iget-object v0, p0, Lcom/sec/vip/cropimage/ac;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/vip/cropimage/ac;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 667
    return-void

    .line 665
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/vip/cropimage/ac;->c:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/vip/cropimage/ac;->d:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    throw v0
.end method
