.class Lcom/sec/vip/cropimage/w;
.super Ljava/lang/Object;
.source "ImageModify.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:F

.field b:Landroid/graphics/Matrix;

.field final synthetic c:Lcom/sec/vip/cropimage/ImageModify;


# direct methods
.method constructor <init>(Lcom/sec/vip/cropimage/ImageModify;)V
    .locals 1

    .prologue
    .line 721
    iput-object p1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 722
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/vip/cropimage/w;->a:F

    return-void
.end method

.method private a()V
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 729
    new-instance v0, Lcom/sec/vip/cropimage/p;

    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->f(Lcom/sec/vip/cropimage/ImageModify;)Lcom/sec/vip/cropimage/ImageModifyView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/cropimage/p;-><init>(Landroid/view/View;)V

    .line 731
    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->h(Lcom/sec/vip/cropimage/ImageModify;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 732
    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->h(Lcom/sec/vip/cropimage/ImageModify;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 733
    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->h(Lcom/sec/vip/cropimage/ImageModify;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 735
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v8, v8, v7, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 741
    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 744
    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->i(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->j(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v1

    if-eqz v1, :cond_4

    .line 746
    invoke-static {v7, v4}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 749
    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->i(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v1

    iget-object v5, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v5}, Lcom/sec/vip/cropimage/ImageModify;->j(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v5

    if-le v1, v5, :cond_1

    .line 750
    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->j(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v1

    mul-int/2addr v1, v6

    iget-object v5, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v5}, Lcom/sec/vip/cropimage/ImageModify;->i(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v5

    div-int v5, v1, v5

    .line 751
    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->j(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v1

    mul-int/2addr v1, v3

    iget-object v9, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v9}, Lcom/sec/vip/cropimage/ImageModify;->i(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v9

    div-int/2addr v1, v9

    move v10, v3

    move v3, v5

    move v5, v10

    .line 757
    :goto_0
    if-gt v5, v7, :cond_3

    if-gt v1, v4, :cond_3

    .line 763
    :goto_1
    sub-int v3, v7, v5

    div-int/lit8 v6, v3, 0x2

    .line 764
    sub-int v3, v4, v1

    div-int/lit8 v4, v3, 0x2

    .line 768
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v7, v6

    int-to-float v9, v4

    add-int/2addr v5, v6

    int-to-float v5, v5

    add-int/2addr v1, v4

    int-to-float v1, v1

    invoke-direct {v3, v7, v9, v5, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 769
    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->b:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v4}, Lcom/sec/vip/cropimage/ImageModify;->k(Lcom/sec/vip/cropimage/ImageModify;)Z

    move-result v4

    iget-object v5, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v5}, Lcom/sec/vip/cropimage/ImageModify;->l(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v5

    iget-object v6, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v6}, Lcom/sec/vip/cropimage/ImageModify;->m(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v7}, Lcom/sec/vip/cropimage/ImageModify;->i(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v7}, Lcom/sec/vip/cropimage/ImageModify;->j(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v7

    if-eqz v7, :cond_2

    const/4 v7, 0x1

    :goto_2
    invoke-virtual/range {v0 .. v7}, Lcom/sec/vip/cropimage/p;->a(Landroid/graphics/Matrix;Landroid/graphics/Rect;Landroid/graphics/RectF;ZIIZ)V

    .line 770
    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->f(Lcom/sec/vip/cropimage/ImageModify;)Lcom/sec/vip/cropimage/ImageModifyView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/vip/cropimage/ImageModifyView;->a(Lcom/sec/vip/cropimage/p;)V

    .line 772
    :cond_0
    return-void

    .line 753
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v1}, Lcom/sec/vip/cropimage/ImageModify;->i(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v1

    mul-int/2addr v1, v6

    iget-object v5, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v5}, Lcom/sec/vip/cropimage/ImageModify;->j(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v5

    div-int/2addr v1, v5

    .line 754
    iget-object v5, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v5}, Lcom/sec/vip/cropimage/ImageModify;->i(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v5

    mul-int/2addr v5, v3

    iget-object v9, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v9}, Lcom/sec/vip/cropimage/ImageModify;->j(Lcom/sec/vip/cropimage/ImageModify;)I

    move-result v9

    div-int/2addr v5, v9

    move v10, v3

    move v3, v6

    move v6, v1

    move v1, v10

    goto :goto_0

    :cond_2
    move v7, v8

    .line 769
    goto :goto_2

    :cond_3
    move v1, v3

    move v5, v6

    goto :goto_1

    :cond_4
    move v1, v4

    move v5, v7

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/vip/cropimage/w;)V
    .locals 0

    .prologue
    .line 721
    invoke-direct {p0}, Lcom/sec/vip/cropimage/w;->a()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 779
    iget-object v0, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v0}, Lcom/sec/vip/cropimage/ImageModify;->f(Lcom/sec/vip/cropimage/ImageModify;)Lcom/sec/vip/cropimage/ImageModifyView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/ImageModifyView;->c()Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/w;->b:Landroid/graphics/Matrix;

    .line 782
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/sec/vip/cropimage/w;->a:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/cropimage/w;->a:F

    .line 783
    iget-object v0, p0, Lcom/sec/vip/cropimage/w;->c:Lcom/sec/vip/cropimage/ImageModify;

    invoke-static {v0}, Lcom/sec/vip/cropimage/ImageModify;->g(Lcom/sec/vip/cropimage/ImageModify;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/vip/cropimage/x;

    invoke-direct {v1, p0}, Lcom/sec/vip/cropimage/x;-><init>(Lcom/sec/vip/cropimage/w;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 806
    return-void
.end method
