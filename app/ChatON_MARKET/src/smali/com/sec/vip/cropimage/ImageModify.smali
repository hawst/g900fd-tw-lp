.class public Lcom/sec/vip/cropimage/ImageModify;
.super Lcom/sec/vip/cropimage/MonitoredActivity;
.source "ImageModify.java"


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private A:Landroid/app/ProgressDialog;

.field private B:Ljava/lang/String;

.field private C:Landroid/content/Context;

.field private D:Landroid/content/Intent;

.field private E:Ljava/lang/String;

.field private F:I

.field private G:Ljava/lang/String;

.field private H:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Lcom/sec/vip/cropimage/ae;",
            ">;"
        }
    .end annotation
.end field

.field private I:Landroid/os/Handler;

.field a:Z

.field b:Lcom/sec/vip/cropimage/p;

.field public c:F

.field d:Ljava/lang/Runnable;

.field private final g:I

.field private h:I

.field private i:I

.field private final j:Landroid/os/Handler;

.field private k:I

.field private l:I

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Lcom/sec/vip/cropimage/ImageModifyView;

.field private t:Landroid/graphics/Bitmap;

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:Landroid/graphics/Bitmap;

.field private z:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-class v0, Lcom/sec/vip/cropimage/ImageModify;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 87
    invoke-direct {p0}, Lcom/sec/vip/cropimage/MonitoredActivity;-><init>()V

    .line 94
    iput v2, p0, Lcom/sec/vip/cropimage/ImageModify;->g:I

    .line 97
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->j:Landroid/os/Handler;

    .line 103
    iput-boolean v2, p0, Lcom/sec/vip/cropimage/ImageModify;->n:Z

    .line 104
    iput-boolean v1, p0, Lcom/sec/vip/cropimage/ImageModify;->o:Z

    .line 105
    iput-boolean v2, p0, Lcom/sec/vip/cropimage/ImageModify;->p:Z

    .line 109
    iput-boolean v1, p0, Lcom/sec/vip/cropimage/ImageModify;->r:Z

    .line 119
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->c:F

    .line 120
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->u:I

    .line 121
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->v:I

    .line 122
    iput v1, p0, Lcom/sec/vip/cropimage/ImageModify;->w:I

    .line 123
    iput v1, p0, Lcom/sec/vip/cropimage/ImageModify;->x:I

    .line 124
    iput-object v3, p0, Lcom/sec/vip/cropimage/ImageModify;->y:Landroid/graphics/Bitmap;

    .line 130
    iput-object v3, p0, Lcom/sec/vip/cropimage/ImageModify;->A:Landroid/app/ProgressDialog;

    .line 131
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->B:Ljava/lang/String;

    .line 134
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->E:Ljava/lang/String;

    .line 721
    new-instance v0, Lcom/sec/vip/cropimage/w;

    invoke-direct {v0, p0}, Lcom/sec/vip/cropimage/w;-><init>(Lcom/sec/vip/cropimage/ImageModify;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->d:Ljava/lang/Runnable;

    .line 1114
    new-instance v0, Lcom/sec/vip/cropimage/y;

    invoke-direct {v0, p0}, Lcom/sec/vip/cropimage/y;-><init>(Lcom/sec/vip/cropimage/ImageModify;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->I:Landroid/os/Handler;

    return-void
.end method

.method private static a(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZZ)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const v7, 0x3f666666    # 0.9f

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1310
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int v3, v2, p2

    .line 1311
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v4, v2, p3

    .line 1312
    if-nez p4, :cond_3

    if-ltz v3, :cond_0

    if-gez v4, :cond_3

    .line 1319
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1320
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1322
    div-int/lit8 v3, v3, 0x2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1323
    div-int/lit8 v4, v4, 0x2

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1324
    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-static {p2, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-static {p3, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    add-int/2addr v7, v1

    invoke-direct {v4, v3, v1, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1325
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int v1, p2, v1

    div-int/lit8 v1, v1, 0x2

    .line 1326
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, p3, v3

    div-int/lit8 v3, v3, 0x2

    .line 1327
    new-instance v6, Landroid/graphics/Rect;

    sub-int v7, p2, v1

    sub-int v8, p3, v3

    invoke-direct {v6, v1, v3, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1328
    invoke-virtual {v5, p1, v4, v6, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1329
    if-eqz p5, :cond_1

    .line 1330
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    move-object v0, v2

    .line 1379
    :cond_2
    :goto_0
    return-object v0

    .line 1334
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    .line 1335
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    .line 1337
    div-float v4, v2, v3

    .line 1338
    int-to-float v5, p2

    int-to-float v6, p3

    div-float/2addr v5, v6

    .line 1340
    cmpl-float v4, v4, v5

    if-lez v4, :cond_8

    .line 1341
    int-to-float v2, p3

    div-float/2addr v2, v3

    .line 1342
    cmpg-float v3, v2, v7

    if-ltz v3, :cond_4

    cmpl-float v3, v2, v8

    if-lez v3, :cond_7

    .line 1343
    :cond_4
    invoke-virtual {p0, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    :goto_1
    move-object v5, p0

    .line 1357
    :goto_2
    if-eqz v5, :cond_b

    .line 1359
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v2, v0

    .line 1364
    :goto_3
    if-eqz p5, :cond_5

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1365
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1368
    :cond_5
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int/2addr v0, p2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1369
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v3, p3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1371
    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v1, v1, 0x2

    invoke-static {v2, v0, v1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1373
    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1374
    if-nez p5, :cond_6

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1375
    :cond_6
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_7
    move-object p0, v0

    .line 1345
    goto :goto_1

    .line 1348
    :cond_8
    int-to-float v3, p2

    div-float v2, v3, v2

    .line 1349
    cmpg-float v3, v2, v7

    if-ltz v3, :cond_9

    cmpl-float v3, v2, v8

    if-lez v3, :cond_a

    .line 1350
    :cond_9
    invoke-virtual {p0, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    move-object v5, p0

    goto :goto_2

    :cond_a
    move-object v5, v0

    .line 1352
    goto :goto_2

    :cond_b
    move-object v2, p1

    .line 1361
    goto :goto_3
.end method

.method static synthetic a(Lcom/sec/vip/cropimage/ImageModify;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->h()V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/cropimage/ImageModify;Z)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/vip/cropimage/ImageModify;->a(Z)V

    return-void
.end method

.method private static a(Lcom/sec/vip/cropimage/MonitoredActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 715
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/vip/cropimage/ac;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p3, v2, p4}, Lcom/sec/vip/cropimage/ac;-><init>(Lcom/sec/vip/cropimage/MonitoredActivity;Ljava/lang/Runnable;Landroid/app/ProgressDialog;Landroid/os/Handler;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 716
    return-void
.end method

.method private a(Ljava/io/FileOutputStream;)V
    .locals 1

    .prologue
    .line 1150
    if-eqz p1, :cond_0

    .line 1151
    :try_start_0
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1156
    :cond_0
    :goto_0
    return-void

    .line 1153
    :catch_0
    move-exception v0

    .line 1154
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 13

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 818
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 819
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->z:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 820
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-static {v0, v1, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->z:Landroid/widget/Toast;

    .line 822
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->z:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1098
    :cond_1
    :goto_0
    return-void

    .line 829
    :cond_2
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    invoke-static {v0, v1, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v8, :cond_3

    .line 830
    invoke-static {p0}, Lcom/sec/common/util/i;->a(Landroid/app/Activity;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageModify;->setRequestedOrientation(I)V

    .line 834
    :cond_3
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModify;->a:Z

    if-nez v0, :cond_1

    .line 837
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->b:Lcom/sec/vip/cropimage/p;

    if-eqz v0, :cond_1

    .line 840
    iput-boolean v8, p0, Lcom/sec/vip/cropimage/ImageModify;->a:Z

    .line 843
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModify;->p:Z

    if-ne v0, v8, :cond_4

    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->y:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 844
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->y:Landroid/graphics/Bitmap;

    move-object v2, v0

    .line 848
    :goto_1
    if-nez v2, :cond_5

    .line 849
    const-string v0, "[onSaveClicked] bmpSrc is NULL!!!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 846
    :cond_4
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    move-object v2, v0

    goto :goto_1

    .line 853
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->b:Lcom/sec/vip/cropimage/p;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/p;->b()Landroid/graphics/Rect;

    move-result-object v0

    .line 854
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 855
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    .line 871
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/vip/cropimage/ImageModify;->o:Z

    if-nez v1, :cond_d

    iget v1, p0, Lcom/sec/vip/cropimage/ImageModify;->u:I

    const/16 v5, 0x3e8

    if-ne v1, v5, :cond_d

    .line 872
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 893
    :goto_2
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 894
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v10, v10, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 895
    invoke-virtual {v5, v2, v0, v6, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 898
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModify;->o:Z

    if-eqz v0, :cond_6

    .line 901
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 902
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 903
    int-to-float v6, v3

    div-float/2addr v6, v7

    int-to-float v4, v4

    div-float/2addr v4, v7

    int-to-float v3, v3

    div-float/2addr v3, v7

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v5, v6, v4, v3, v7}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 904
    sget-object v3, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {v0, v5, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 905
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v10, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 909
    :cond_6
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->k:I

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->l:I

    if-eqz v0, :cond_9

    .line 910
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModify;->m:Z

    if-eqz v0, :cond_10

    .line 916
    :try_start_1
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iget v2, p0, Lcom/sec/vip/cropimage/ImageModify;->k:I

    iget v3, p0, Lcom/sec/vip/cropimage/ImageModify;->l:I

    iget-boolean v4, p0, Lcom/sec/vip/cropimage/ImageModify;->n:Z

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/sec/vip/cropimage/ImageModify;->a(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v2

    .line 925
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 926
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->c:F

    invoke-virtual {v7, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 927
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    move v3, v10

    move v4, v10

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 930
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->u:I

    const/16 v3, 0x3e8

    if-eq v0, v3, :cond_e

    .line 931
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 932
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 933
    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 934
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 936
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 937
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v5, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 939
    iget v5, p0, Lcom/sec/vip/cropimage/ImageModify;->u:I

    packed-switch v5, :pswitch_data_0

    move-object v0, v9

    .line 954
    :goto_3
    new-instance v5, Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-direct {v5, v11, v11, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v3, v0, v9, v5, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 974
    :cond_7
    :goto_4
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 975
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_8
    move-object v1, v2

    .line 1008
    :cond_9
    :goto_5
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1009
    if-eqz v0, :cond_15

    const-string v2, "data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_a

    const-string v2, "return-data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1010
    :cond_a
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1013
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int/2addr v0, v2

    .line 1014
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bitmap byte size : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    const-string v0, "exceed_size"

    invoke-virtual {v3, v0, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1021
    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 1024
    :goto_6
    iget-boolean v2, p0, Lcom/sec/vip/cropimage/ImageModify;->r:Z

    if-eqz v2, :cond_12

    .line 1025
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/tmpCrop_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1035
    :goto_7
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1039
    :try_start_2
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1043
    :try_start_3
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v5

    .line 1047
    if-eqz v5, :cond_13

    invoke-virtual {v5}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ActivityBackgroundChange"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-ne v5, v8, :cond_13

    .line 1048
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x64

    invoke-virtual {v1, v5, v6, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1063
    :goto_8
    invoke-direct {p0, v2}, Lcom/sec/vip/cropimage/ImageModify;->a(Ljava/io/FileOutputStream;)V

    .line 1064
    invoke-direct {p0, v2}, Lcom/sec/vip/cropimage/ImageModify;->b(Ljava/io/FileOutputStream;)V

    .line 1067
    :goto_9
    if-nez v8, :cond_b

    .line 1068
    invoke-virtual {p0, v10, v9}, Lcom/sec/vip/cropimage/ImageModify;->setResult(ILandroid/content/Intent;)V

    .line 1069
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    .line 1071
    :cond_b
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_c

    .line 1072
    invoke-virtual {p0, v10, v9}, Lcom/sec/vip/cropimage/ImageModify;->setResult(ILandroid/content/Intent;)V

    .line 1073
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    .line 1076
    :cond_c
    const-string v1, "temp_file_path"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1077
    const-string v1, "activity_orientation"

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1078
    const-string v1, "sendWithText"

    invoke-virtual {v3, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1079
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1081
    invoke-virtual {v1, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1083
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "isgroup"

    invoke-virtual {v2, v3, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1084
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "groupname"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/vip/cropimage/ImageModify;->E:Ljava/lang/String;

    .line 1085
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1086
    iget-object v3, p0, Lcom/sec/vip/cropimage/ImageModify;->I:Landroid/os/Handler;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/vip/cropimage/ImageModify;->E:Ljava/lang/String;

    invoke-static {v3, v4, v2, v5}, Lcom/sec/chaton/d/m;->a(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1087
    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->B:Ljava/lang/String;

    .line 1088
    iput-object v1, p0, Lcom/sec/vip/cropimage/ImageModify;->D:Landroid/content/Intent;

    .line 1089
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->h()V

    goto/16 :goto_0

    .line 874
    :cond_d
    :try_start_4
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v1

    goto/16 :goto_2

    .line 877
    :catch_0
    move-exception v0

    .line 878
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0149

    invoke-static {v1, v2, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 879
    sget-object v1, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 880
    invoke-virtual {p0, v10}, Lcom/sec/vip/cropimage/ImageModify;->setResult(I)V

    .line 881
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    goto/16 :goto_0

    .line 883
    :catch_1
    move-exception v0

    .line 884
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b016c

    invoke-static {v1, v2, v8}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 885
    sget-object v1, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 886
    invoke-virtual {p0, v10}, Lcom/sec/vip/cropimage/ImageModify;->setResult(I)V

    .line 887
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    goto/16 :goto_0

    .line 917
    :catch_2
    move-exception v0

    .line 918
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0149

    invoke-static {v1, v2, v10}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 919
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 920
    invoke-virtual {p0, v10}, Lcom/sec/vip/cropimage/ImageModify;->setResult(I)V

    .line 921
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    goto/16 :goto_0

    .line 941
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02041d

    invoke-static {v5, v6, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_3

    .line 944
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02041b

    invoke-static {v5, v6, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_3

    .line 947
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02041e

    invoke-static {v5, v6, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_3

    .line 950
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02041c

    invoke-static {v5, v6, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_3

    .line 955
    :cond_e
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->v:I

    const/16 v3, 0x7d6

    if-eq v0, v3, :cond_f

    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->v:I

    const/16 v3, 0x7d7

    if-ne v0, v3, :cond_7

    .line 956
    :cond_f
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 957
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 958
    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 961
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->v:I

    packed-switch v0, :pswitch_data_1

    move-object v0, v9

    .line 969
    :goto_a
    if-eqz v0, :cond_7

    .line 970
    new-instance v5, Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-direct {v5, v11, v11, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v3, v0, v9, v5, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 963
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f02041f

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_a

    .line 966
    :pswitch_5
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f020422

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_a

    .line 982
    :cond_10
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->k:I

    iget v3, p0, Lcom/sec/vip/cropimage/ImageModify;->l:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 983
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 985
    iget-object v4, p0, Lcom/sec/vip/cropimage/ImageModify;->b:Lcom/sec/vip/cropimage/p;

    invoke-virtual {v4}, Lcom/sec/vip/cropimage/p;->b()Landroid/graphics/Rect;

    move-result-object v4

    .line 986
    new-instance v5, Landroid/graphics/Rect;

    iget v6, p0, Lcom/sec/vip/cropimage/ImageModify;->k:I

    iget v7, p0, Lcom/sec/vip/cropimage/ImageModify;->l:I

    invoke-direct {v5, v10, v10, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 988
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    .line 989
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v11

    sub-int/2addr v7, v11

    div-int/lit8 v7, v7, 0x2

    .line 992
    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-static {v10, v7}, Ljava/lang/Math;->max(II)I

    move-result v12

    invoke-virtual {v4, v11, v12}, Landroid/graphics/Rect;->inset(II)V

    .line 995
    neg-int v6, v6

    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    neg-int v7, v7

    invoke-static {v10, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->inset(II)V

    .line 998
    invoke-virtual {v3, v2, v4, v5, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1001
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    move-object v1, v0

    .line 1002
    goto/16 :goto_5

    .line 1021
    :cond_11
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    .line 1028
    :cond_12
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/tmpCropImage.jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 1050
    :cond_13
    :try_start_5
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x46

    invoke-virtual {v1, v5, v6, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_8

    .line 1054
    :catch_3
    move-exception v1

    .line 1055
    :goto_b
    :try_start_6
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1056
    const-string v1, "callingClName is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1063
    invoke-direct {p0, v2}, Lcom/sec/vip/cropimage/ImageModify;->a(Ljava/io/FileOutputStream;)V

    .line 1064
    invoke-direct {p0, v2}, Lcom/sec/vip/cropimage/ImageModify;->b(Ljava/io/FileOutputStream;)V

    move v8, v10

    .line 1065
    goto/16 :goto_9

    .line 1058
    :catch_4
    move-exception v1

    move-object v2, v9

    .line 1059
    :goto_c
    :try_start_7
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1060
    const-string v1, "Temporary file saving failed!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1063
    invoke-direct {p0, v2}, Lcom/sec/vip/cropimage/ImageModify;->a(Ljava/io/FileOutputStream;)V

    .line 1064
    invoke-direct {p0, v2}, Lcom/sec/vip/cropimage/ImageModify;->b(Ljava/io/FileOutputStream;)V

    move v8, v10

    .line 1065
    goto/16 :goto_9

    .line 1063
    :catchall_0
    move-exception v0

    move-object v2, v9

    :goto_d
    invoke-direct {p0, v2}, Lcom/sec/vip/cropimage/ImageModify;->a(Ljava/io/FileOutputStream;)V

    .line 1064
    invoke-direct {p0, v2}, Lcom/sec/vip/cropimage/ImageModify;->b(Ljava/io/FileOutputStream;)V

    .line 1063
    throw v0

    .line 1091
    :cond_14
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/cropimage/ImageModify;->setResult(ILandroid/content/Intent;)V

    .line 1092
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    goto/16 :goto_0

    .line 1096
    :cond_15
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    goto/16 :goto_0

    .line 1063
    :catchall_1
    move-exception v0

    goto :goto_d

    .line 1058
    :catch_5
    move-exception v1

    goto :goto_c

    .line 1054
    :catch_6
    move-exception v1

    move-object v2, v9

    goto :goto_b

    .line 939
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 961
    :pswitch_data_1
    .packed-switch 0x7d6
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/vip/cropimage/ImageModify;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->i()V

    return-void
.end method

.method private b(Ljava/io/FileOutputStream;)V
    .locals 1

    .prologue
    .line 1160
    if-eqz p1, :cond_0

    .line 1161
    :try_start_0
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1166
    :cond_0
    :goto_0
    return-void

    .line 1163
    :catch_0
    move-exception v0

    .line 1164
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/vip/cropimage/ImageModify;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->g()V

    return-void
.end method

.method static synthetic d(Lcom/sec/vip/cropimage/ImageModify;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->j()V

    return-void
.end method

.method static synthetic e(Lcom/sec/vip/cropimage/ImageModify;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->f()V

    return-void
.end method

.method private e()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 465
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 466
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 468
    if-eqz v1, :cond_1

    .line 469
    const-string v0, "circleCrop"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 470
    iput-boolean v3, p0, Lcom/sec/vip/cropimage/ImageModify;->o:Z

    .line 471
    iput v3, p0, Lcom/sec/vip/cropimage/ImageModify;->h:I

    .line 472
    iput v3, p0, Lcom/sec/vip/cropimage/ImageModify;->i:I

    .line 474
    :cond_0
    const-string v0, "data"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    .line 475
    const-string v0, "aspectX"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->h:I

    .line 476
    const-string v0, "aspectY"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->i:I

    .line 477
    const-string v0, "outputX"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->k:I

    .line 478
    const-string v0, "outputY"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->l:I

    .line 479
    const-string v0, "effect"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModify;->p:Z

    .line 480
    const-string v0, "scale"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModify;->m:Z

    .line 481
    const-string v0, "scaleUpIfNeeded"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModify;->n:Z

    .line 482
    const-string v0, "sendMode"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModify;->q:Z

    .line 483
    const-string v0, "randomFName"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModify;->r:Z

    .line 487
    :cond_1
    const v0, 0x7f0300b5

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageModify;->setContentView(I)V

    .line 488
    const v0, 0x7f07025c

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageModify;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/ImageModifyView;

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->s:Lcom/sec/vip/cropimage/ImageModifyView;

    .line 489
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->s:Lcom/sec/vip/cropimage/ImageModifyView;

    iput-object p0, v0, Lcom/sec/vip/cropimage/ImageModifyView;->g:Landroid/content/Context;

    .line 492
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 494
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->s:Lcom/sec/vip/cropimage/ImageModifyView;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Lcom/sec/vip/cropimage/ImageModifyView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 499
    :cond_2
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getExternalCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/tempCropImage.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 500
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->G:Ljava/lang/String;

    .line 506
    const v0, 0x7f070335

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/ImageModify;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 507
    new-instance v1, Lcom/sec/vip/cropimage/r;

    invoke-direct {v1, p0}, Lcom/sec/vip/cropimage/r;-><init>(Lcom/sec/vip/cropimage/ImageModify;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 514
    const v1, 0x7f070336

    invoke-virtual {p0, v1}, Lcom/sec/vip/cropimage/ImageModify;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 515
    new-instance v2, Lcom/sec/vip/cropimage/s;

    invoke-direct {v2, p0}, Lcom/sec/vip/cropimage/s;-><init>(Lcom/sec/vip/cropimage/ImageModify;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 522
    const v1, 0x7f070325

    invoke-virtual {p0, v1}, Lcom/sec/vip/cropimage/ImageModify;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 523
    new-instance v2, Lcom/sec/vip/cropimage/t;

    invoke-direct {v2, p0}, Lcom/sec/vip/cropimage/t;-><init>(Lcom/sec/vip/cropimage/ImageModify;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 532
    iget-boolean v1, p0, Lcom/sec/vip/cropimage/ImageModify;->q:Z

    if-nez v1, :cond_3

    .line 533
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 538
    :cond_3
    return v3
.end method

.method static synthetic f(Lcom/sec/vip/cropimage/ImageModify;)Lcom/sec/vip/cropimage/ImageModifyView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->s:Lcom/sec/vip/cropimage/ImageModifyView;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 544
    const/4 v0, 0x0

    .line 545
    :try_start_0
    iget v1, p0, Lcom/sec/vip/cropimage/ImageModify;->F:I

    rem-int/lit16 v1, v1, 0x168

    const/16 v2, 0x5a

    if-ne v1, v2, :cond_2

    .line 546
    const/4 v0, 0x6

    .line 553
    :cond_0
    :goto_0
    new-instance v1, Landroid/media/ExifInterface;

    iget-object v2, p0, Lcom/sec/vip/cropimage/ImageModify;->G:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 554
    const-string v2, "Orientation"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    invoke-virtual {v1}, Landroid/media/ExifInterface;->saveAttributes()V

    .line 557
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 558
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "orientation set : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    :cond_1
    :goto_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/multimedia/image/ImageEffectActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 567
    const-string v1, "filterOrgUri"

    iget-object v2, p0, Lcom/sec/vip/cropimage/ImageModify;->G:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 568
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/cropimage/ImageModify;->startActivityForResult(Landroid/content/Intent;I)V

    .line 569
    return-void

    .line 547
    :cond_2
    :try_start_1
    iget v1, p0, Lcom/sec/vip/cropimage/ImageModify;->F:I

    rem-int/lit16 v1, v1, 0x168

    const/16 v2, 0xb4

    if-ne v1, v2, :cond_3

    .line 548
    const/4 v0, 0x3

    goto :goto_0

    .line 549
    :cond_3
    iget v1, p0, Lcom/sec/vip/cropimage/ImageModify;->F:I

    rem-int/lit16 v1, v1, 0x168
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v2, 0x10e

    if-ne v1, v2, :cond_0

    .line 550
    const/16 v0, 0x8

    goto :goto_0

    .line 561
    :catch_0
    move-exception v0

    .line 562
    sget-object v1, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic g(Lcom/sec/vip/cropimage/ImageModify;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->j:Landroid/os/Handler;

    return-object v0
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 575
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    :goto_0
    return-void

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->s:Lcom/sec/vip/cropimage/ImageModifyView;

    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/sec/vip/cropimage/ImageModify;->c:F

    float-to-int v2, v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/vip/cropimage/ImageModifyView;->setImageBitmapResetBase(Landroid/graphics/Bitmap;IZ)V

    .line 581
    new-instance v0, Lcom/sec/vip/cropimage/u;

    invoke-direct {v0, p0}, Lcom/sec/vip/cropimage/u;-><init>(Lcom/sec/vip/cropimage/ImageModify;)V

    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageModify;->j:Landroid/os/Handler;

    invoke-static {p0, v4, v4, v0, v1}, Lcom/sec/vip/cropimage/ImageModify;->a(Lcom/sec/vip/cropimage/MonitoredActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/vip/cropimage/ImageModify;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 1101
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->A:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 1102
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->A:Landroid/app/ProgressDialog;

    .line 1106
    :goto_0
    return-void

    .line 1104
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/vip/cropimage/ImageModify;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->h:I

    return v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->A:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 1110
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1112
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/vip/cropimage/ImageModify;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->i:I

    return v0
.end method

.method private j()V
    .locals 8

    .prologue
    const/high16 v2, 0x43870000    # 270.0f

    const/4 v7, 0x0

    .line 1172
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1173
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 1175
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1177
    div-int/lit8 v0, v3, 0x2

    int-to-float v0, v0

    div-int/lit8 v1, v4, 0x2

    int-to-float v1, v1

    invoke-virtual {v5, v2, v0, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1179
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->F:I

    int-to-float v0, v0

    add-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->F:I

    .line 1180
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->F:I

    int-to-float v0, v0

    const/high16 v1, 0x43b40000    # 360.0f

    rem-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->F:I

    .line 1184
    :try_start_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1199
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->s:Lcom/sec/vip/cropimage/ImageModifyView;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/ImageModifyView;->b()V

    .line 1201
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->g()V

    .line 1202
    :goto_0
    return-void

    .line 1192
    :catch_0
    move-exception v0

    .line 1193
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0149

    invoke-static {v1, v2, v7}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1194
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1195
    invoke-virtual {p0, v7}, Lcom/sec/vip/cropimage/ImageModify;->setResult(I)V

    .line 1196
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    goto :goto_0
.end method

.method static synthetic k(Lcom/sec/vip/cropimage/ImageModify;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/ImageModify;->o:Z

    return v0
.end method

.method static synthetic l(Lcom/sec/vip/cropimage/ImageModify;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->u:I

    return v0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 1447
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1448
    const-string v1, "restart"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1449
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/sec/vip/cropimage/ImageModify;->setResult(ILandroid/content/Intent;)V

    .line 1450
    return-void
.end method

.method static synthetic m(Lcom/sec/vip/cropimage/ImageModify;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->v:I

    return v0
.end method

.method static synthetic n(Lcom/sec/vip/cropimage/ImageModify;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->D:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/vip/cropimage/ImageModify;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->C:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/vip/cropimage/ImageModify;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/vip/cropimage/ImageModify;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->I:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/vip/cropimage/ImageModify;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->E:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->w:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->x:I

    return v0
.end method

.method public c()F
    .locals 1

    .prologue
    .line 303
    iget v0, p0, Lcom/sec/vip/cropimage/ImageModify;->c:F

    return v0
.end method

.method public d()Lcom/sec/vip/cropimage/ae;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 309
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 311
    new-instance v9, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getExternalCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/tempCropImage.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v9, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 313
    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    if-nez v1, :cond_8

    .line 314
    const/4 v2, 0x0

    .line 316
    const/4 v3, 0x0

    .line 318
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 319
    if-nez v1, :cond_3

    .line 320
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_0

    .line 321
    const-string v0, "Uri == null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_0
    sget-object v0, Lcom/sec/vip/cropimage/ae;->b:Lcom/sec/vip/cropimage/ae;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 431
    if-eqz v7, :cond_1

    .line 433
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c

    .line 438
    :cond_1
    :goto_0
    if-eqz v7, :cond_2

    .line 440
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_d

    .line 454
    :cond_2
    :goto_1
    return-object v0

    .line 326
    :cond_3
    :try_start_3
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 328
    invoke-static {v1}, Lcom/sec/chaton/util/ad;->c(Landroid/net/Uri;)I

    move-result v0

    iput v0, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 329
    sget-boolean v0, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v0, :cond_4

    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Target URI: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "options.inSampleSize : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :cond_4
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "file://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 335
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 358
    :goto_2
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_5

    .line 359
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "filepath: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 365
    invoke-static {v0, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v8, v7

    .line 374
    :goto_3
    :try_start_4
    const-string v1, ""

    .line 375
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 376
    const-string v0, "Orientation"

    invoke-virtual {v1, v0}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 378
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 379
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 381
    const-string v1, "6"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 382
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 392
    :goto_4
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 393
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    .line 397
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_19

    .line 398
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_17
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_14
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_11
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 399
    :try_start_5
    iget-object v1, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_18
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_15
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_12
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 431
    :goto_5
    if-eqz v8, :cond_7

    .line 433
    :try_start_6
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_e

    .line 438
    :cond_7
    :goto_6
    if-eqz v0, :cond_8

    .line 440
    :try_start_7
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_f

    .line 448
    :cond_8
    :goto_7
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    if-nez v0, :cond_18

    .line 449
    const-string v0, "mBitmap is null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    sget-object v0, Lcom/sec/vip/cropimage/ae;->b:Lcom/sec/vip/cropimage/ae;

    goto/16 :goto_1

    .line 336
    :cond_9
    :try_start_8
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "content://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v0

    if-eqz v0, :cond_1a

    .line 340
    :try_start_9
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v2

    move-object v3, v7

    .line 342
    :cond_a
    :goto_8
    if-eqz v2, :cond_b

    :try_start_a
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 343
    const-string v0, "_data"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 344
    const/4 v4, -0x1

    if-eq v0, v4, :cond_a

    .line 345
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_19
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    move-result-object v3

    goto :goto_8

    .line 351
    :cond_b
    if-eqz v2, :cond_c

    .line 352
    :try_start_b
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :cond_c
    move-object v0, v3

    .line 354
    goto/16 :goto_2

    .line 348
    :catch_0
    move-exception v0

    move-object v2, v7

    move-object v3, v7

    .line 349
    :goto_9
    :try_start_c
    sget-object v4, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    .line 351
    if-eqz v2, :cond_d

    .line 352
    :try_start_d
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_d
    move-object v0, v3

    .line 354
    goto/16 :goto_2

    .line 351
    :catchall_0
    move-exception v0

    move-object v2, v7

    :goto_a
    if-eqz v2, :cond_e

    .line 352
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 351
    :cond_e
    throw v0
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 405
    :catch_1
    move-exception v0

    move-object v1, v7

    .line 407
    :goto_b
    :try_start_e
    const-string v0, "FileNotFound"

    sget-object v2, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    sget-object v0, Lcom/sec/vip/cropimage/ae;->b:Lcom/sec/vip/cropimage/ae;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    .line 431
    if-eqz v1, :cond_f

    .line 433
    :try_start_f
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_9

    .line 438
    :cond_f
    :goto_c
    if-eqz v7, :cond_2

    .line 440
    :try_start_10
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2

    goto/16 :goto_1

    .line 441
    :catch_2
    move-exception v1

    .line 442
    sget-object v2, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    :goto_d
    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 367
    :cond_10
    :try_start_11
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 368
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 369
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_11
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_11} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_11 .. :try_end_11} :catch_3
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_5
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    move-result-object v0

    .line 370
    const/4 v1, 0x0

    :try_start_12
    invoke-static {v0, v1, v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;
    :try_end_12
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_12} :catch_16
    .catch Ljava/lang/OutOfMemoryError; {:try_start_12 .. :try_end_12} :catch_13
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_10
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    move-object v8, v0

    move-object v0, v2

    goto/16 :goto_3

    .line 383
    :cond_11
    :try_start_13
    const-string v1, "3"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 384
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    .line 385
    :cond_12
    const-string v1, "8"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 386
    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    .line 389
    :cond_13
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_13
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_13} :catch_17
    .catch Ljava/lang/OutOfMemoryError; {:try_start_13 .. :try_end_13} :catch_14
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_11
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    move-result-object v0

    goto/16 :goto_4

    .line 417
    :catch_3
    move-exception v0

    move-object v8, v7

    .line 423
    :goto_e
    :try_start_14
    sget-object v1, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 424
    sget-object v0, Lcom/sec/vip/cropimage/ae;->c:Lcom/sec/vip/cropimage/ae;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    .line 431
    if-eqz v8, :cond_14

    .line 433
    :try_start_15
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_a

    .line 438
    :cond_14
    :goto_f
    if-eqz v7, :cond_2

    .line 440
    :try_start_16
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_4

    goto/16 :goto_1

    .line 441
    :catch_4
    move-exception v1

    .line 442
    sget-object v2, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    goto :goto_d

    .line 425
    :catch_5
    move-exception v0

    move-object v8, v7

    .line 427
    :goto_10
    :try_start_17
    sget-object v1, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 428
    sget-object v0, Lcom/sec/vip/cropimage/ae;->b:Lcom/sec/vip/cropimage/ae;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_3

    .line 431
    if-eqz v8, :cond_15

    .line 433
    :try_start_18
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_b

    .line 438
    :cond_15
    :goto_11
    if-eqz v7, :cond_2

    .line 440
    :try_start_19
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_6

    goto/16 :goto_1

    .line 441
    :catch_6
    move-exception v1

    .line 442
    sget-object v2, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    goto :goto_d

    .line 431
    :catchall_1
    move-exception v0

    move-object v8, v7

    :goto_12
    if-eqz v8, :cond_16

    .line 433
    :try_start_1a
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_7

    .line 438
    :cond_16
    :goto_13
    if-eqz v7, :cond_17

    .line 440
    :try_start_1b
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_8

    .line 431
    :cond_17
    :goto_14
    throw v0

    .line 454
    :cond_18
    sget-object v0, Lcom/sec/vip/cropimage/ae;->a:Lcom/sec/vip/cropimage/ae;

    goto/16 :goto_1

    .line 434
    :catch_7
    move-exception v1

    goto :goto_13

    .line 441
    :catch_8
    move-exception v1

    .line 442
    sget-object v2, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_14

    .line 434
    :catch_9
    move-exception v1

    goto/16 :goto_c

    :catch_a
    move-exception v1

    goto :goto_f

    :catch_b
    move-exception v1

    goto :goto_11

    :catch_c
    move-exception v1

    goto/16 :goto_0

    .line 441
    :catch_d
    move-exception v1

    .line 442
    sget-object v2, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    goto/16 :goto_d

    .line 434
    :catch_e
    move-exception v1

    goto/16 :goto_6

    .line 441
    :catch_f
    move-exception v0

    .line 442
    sget-object v1, Lcom/sec/vip/cropimage/ImageModify;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 431
    :catchall_2
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_12

    :catchall_3
    move-exception v0

    goto :goto_12

    :catchall_4
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_12

    :catchall_5
    move-exception v0

    move-object v8, v1

    goto :goto_12

    .line 425
    :catch_10
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_10

    :catch_11
    move-exception v0

    goto :goto_10

    :catch_12
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_10

    .line 417
    :catch_13
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_e

    :catch_14
    move-exception v0

    goto :goto_e

    :catch_15
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto/16 :goto_e

    .line 405
    :catch_16
    move-exception v1

    move-object v1, v0

    goto/16 :goto_b

    :catch_17
    move-exception v0

    move-object v1, v8

    goto/16 :goto_b

    :catch_18
    move-exception v1

    move-object v7, v0

    move-object v1, v8

    goto/16 :goto_b

    .line 351
    :catchall_6
    move-exception v0

    goto/16 :goto_a

    .line 348
    :catch_19
    move-exception v0

    goto/16 :goto_9

    :cond_19
    move-object v0, v7

    goto/16 :goto_5

    :cond_1a
    move-object v0, v7

    goto/16 :goto_2
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 248
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/cropimage/MonitoredActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 249
    packed-switch p1, :pswitch_data_0

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 252
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 253
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "filterResultUri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 254
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 255
    const/4 v0, 0x0

    .line 257
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 261
    :goto_1
    if-eqz v0, :cond_0

    .line 262
    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    .line 264
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->g()V

    goto :goto_0

    .line 258
    :catch_0
    move-exception v1

    .line 259
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0149

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 1442
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->l()V

    .line 1443
    invoke-super {p0}, Lcom/sec/vip/cropimage/MonitoredActivity;->onBackPressed()V

    .line 1444
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 237
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/MonitoredActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 239
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 242
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 207
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/MonitoredActivity;->onCreate(Landroid/os/Bundle;)V

    .line 208
    iput-object p0, p0, Lcom/sec/vip/cropimage/ImageModify;->C:Landroid/content/Context;

    .line 210
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->w:I

    .line 211
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/ImageModify;->x:I

    .line 214
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->e()Z

    .line 216
    new-instance v0, Lcom/sec/vip/cropimage/af;

    invoke-direct {v0, p0, p0}, Lcom/sec/vip/cropimage/af;-><init>(Lcom/sec/vip/cropimage/ImageModify;Lcom/sec/vip/cropimage/ImageModify;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->H:Landroid/os/AsyncTask;

    .line 217
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->H:Landroid/os/AsyncTask;

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 222
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->H:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 1391
    invoke-super {p0}, Lcom/sec/vip/cropimage/MonitoredActivity;->onDestroy()V

    .line 1392
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1393
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1394
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->t:Landroid/graphics/Bitmap;

    .line 1396
    :cond_0
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->i()V

    .line 1398
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->H:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 1399
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->H:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 1401
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 228
    invoke-super {p0}, Lcom/sec/vip/cropimage/MonitoredActivity;->onResume()V

    .line 230
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    invoke-static {p0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/app/Activity;)V

    .line 233
    :cond_0
    return-void
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1408
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0021

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1409
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/MonitoredActivity;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1414
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1434
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/MonitoredActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 1416
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->l()V

    .line 1417
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    goto :goto_0

    .line 1420
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/vip/cropimage/ImageModify;->l()V

    .line 1421
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/ImageModify;->finish()V

    goto :goto_0

    .line 1424
    :sswitch_2
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->C:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/chaton/j/v;->a(Landroid/content/Context;)I

    move-result v0

    .line 1425
    const/4 v1, -0x3

    if-eq v1, v0, :cond_0

    const/4 v1, -0x2

    if-ne v1, v0, :cond_1

    .line 1426
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/ImageModify;->C:Landroid/content/Context;

    const v1, 0x7f0b0205

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1428
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/vip/cropimage/ImageModify;->a(Z)V

    goto :goto_0

    .line 1414
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0705a5 -> :sswitch_1
        0x7f0705a6 -> :sswitch_2
    .end sparse-switch
.end method
