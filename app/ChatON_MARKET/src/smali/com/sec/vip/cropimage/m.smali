.class Lcom/sec/vip/cropimage/m;
.super Lcom/sec/vip/cropimage/aj;
.source "CropImage.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/sec/vip/cropimage/MonitoredActivity;

.field private final b:Landroid/app/ProgressDialog;

.field private final c:Ljava/lang/Runnable;

.field private final d:Landroid/os/Handler;

.field private final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/sec/vip/cropimage/MonitoredActivity;Ljava/lang/Runnable;Landroid/app/ProgressDialog;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 688
    invoke-direct {p0}, Lcom/sec/vip/cropimage/aj;-><init>()V

    .line 664
    new-instance v0, Lcom/sec/vip/cropimage/n;

    invoke-direct {v0, p0}, Lcom/sec/vip/cropimage/n;-><init>(Lcom/sec/vip/cropimage/m;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/m;->e:Ljava/lang/Runnable;

    .line 689
    iput-object p1, p0, Lcom/sec/vip/cropimage/m;->a:Lcom/sec/vip/cropimage/MonitoredActivity;

    .line 690
    iput-object p3, p0, Lcom/sec/vip/cropimage/m;->b:Landroid/app/ProgressDialog;

    .line 691
    iput-object p2, p0, Lcom/sec/vip/cropimage/m;->c:Ljava/lang/Runnable;

    .line 692
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->a:Lcom/sec/vip/cropimage/MonitoredActivity;

    invoke-virtual {v0, p0}, Lcom/sec/vip/cropimage/MonitoredActivity;->a(Lcom/sec/vip/cropimage/ak;)V

    .line 693
    iput-object p4, p0, Lcom/sec/vip/cropimage/m;->d:Landroid/os/Handler;

    .line 694
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/cropimage/m;)Lcom/sec/vip/cropimage/MonitoredActivity;
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->a:Lcom/sec/vip/cropimage/MonitoredActivity;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/vip/cropimage/m;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->b:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/vip/cropimage/MonitoredActivity;)V
    .locals 2

    .prologue
    .line 712
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 713
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/vip/cropimage/m;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 714
    return-void
.end method

.method public b(Lcom/sec/vip/cropimage/MonitoredActivity;)V
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 719
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    .line 721
    :cond_0
    return-void
.end method

.method public c(Lcom/sec/vip/cropimage/MonitoredActivity;)V
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 728
    :cond_0
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 702
    :try_start_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 704
    iget-object v0, p0, Lcom/sec/vip/cropimage/m;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/vip/cropimage/m;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 706
    return-void

    .line 704
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/vip/cropimage/m;->d:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/vip/cropimage/m;->e:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    throw v0
.end method
