.class public Lcom/sec/vip/cropimage/al;
.super Ljava/lang/Object;
.source "RotateBitmap.java"


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sec/vip/cropimage/al;->a:Landroid/graphics/Bitmap;

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/cropimage/al;->b:I

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;I)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/sec/vip/cropimage/al;->a:Landroid/graphics/Bitmap;

    .line 48
    rem-int/lit16 v0, p2, 0x168

    iput v0, p0, Lcom/sec/vip/cropimage/al;->b:I

    .line 49
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/vip/cropimage/al;->b:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/vip/cropimage/al;->b:I

    .line 59
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/vip/cropimage/al;->a:Landroid/graphics/Bitmap;

    .line 87
    return-void
.end method

.method public b()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/vip/cropimage/al;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public c()Landroid/graphics/Matrix;
    .locals 3

    .prologue
    .line 96
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 97
    iget v1, p0, Lcom/sec/vip/cropimage/al;->b:I

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/sec/vip/cropimage/al;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 102
    iget-object v2, p0, Lcom/sec/vip/cropimage/al;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 103
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 104
    iget v1, p0, Lcom/sec/vip/cropimage/al;->b:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 105
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/al;->f()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/al;->e()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 107
    :cond_0
    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/vip/cropimage/al;->b:I

    div-int/lit8 v0, v0, 0x5a

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/al;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/vip/cropimage/al;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 128
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/al;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/al;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/vip/cropimage/al;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 141
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/al;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_0
.end method
