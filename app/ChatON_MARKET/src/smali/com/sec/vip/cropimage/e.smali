.class Lcom/sec/vip/cropimage/e;
.super Ljava/lang/Object;
.source "CropImage.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/graphics/Bitmap;

.field final synthetic b:Ljava/util/concurrent/CountDownLatch;

.field final synthetic c:Lcom/sec/vip/cropimage/d;


# direct methods
.method constructor <init>(Lcom/sec/vip/cropimage/d;Landroid/graphics/Bitmap;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 631
    iput-object p1, p0, Lcom/sec/vip/cropimage/e;->c:Lcom/sec/vip/cropimage/d;

    iput-object p2, p0, Lcom/sec/vip/cropimage/e;->a:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/sec/vip/cropimage/e;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 634
    iget-object v0, p0, Lcom/sec/vip/cropimage/e;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/vip/cropimage/e;->c:Lcom/sec/vip/cropimage/d;

    iget-object v1, v1, Lcom/sec/vip/cropimage/d;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v1}, Lcom/sec/vip/cropimage/CropImage;->g(Lcom/sec/vip/cropimage/CropImage;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/vip/cropimage/e;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/sec/vip/cropimage/e;->c:Lcom/sec/vip/cropimage/d;

    iget-object v0, v0, Lcom/sec/vip/cropimage/d;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/cropimage/e;->a:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/cropimage/e;->c:Lcom/sec/vip/cropimage/d;

    iget-object v2, v2, Lcom/sec/vip/cropimage/d;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v2}, Lcom/sec/vip/cropimage/CropImage;->h(Lcom/sec/vip/cropimage/CropImage;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/vip/cropimage/CropImageView;->setImageBitmapResetBase(Landroid/graphics/Bitmap;IZ)V

    .line 636
    iget-object v0, p0, Lcom/sec/vip/cropimage/e;->c:Lcom/sec/vip/cropimage/d;

    iget-object v0, v0, Lcom/sec/vip/cropimage/d;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->g(Lcom/sec/vip/cropimage/CropImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 637
    iget-object v0, p0, Lcom/sec/vip/cropimage/e;->c:Lcom/sec/vip/cropimage/d;

    iget-object v0, v0, Lcom/sec/vip/cropimage/d;->a:Lcom/sec/vip/cropimage/CropImage;

    iget-object v1, p0, Lcom/sec/vip/cropimage/e;->a:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/sec/vip/cropimage/CropImage;->a(Lcom/sec/vip/cropimage/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/e;->c:Lcom/sec/vip/cropimage/d;

    iget-object v0, v0, Lcom/sec/vip/cropimage/d;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/CropImageView;->a()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 640
    iget-object v0, p0, Lcom/sec/vip/cropimage/e;->c:Lcom/sec/vip/cropimage/d;

    iget-object v0, v0, Lcom/sec/vip/cropimage/d;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Lcom/sec/vip/cropimage/CropImageView;->a(ZZ)V

    .line 642
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/e;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 643
    return-void
.end method
