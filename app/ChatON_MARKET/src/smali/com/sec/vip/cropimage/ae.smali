.class final enum Lcom/sec/vip/cropimage/ae;
.super Ljava/lang/Enum;
.source "ImageModify.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/vip/cropimage/ae;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/vip/cropimage/ae;

.field public static final enum b:Lcom/sec/vip/cropimage/ae;

.field public static final enum c:Lcom/sec/vip/cropimage/ae;

.field private static final synthetic d:[Lcom/sec/vip/cropimage/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 146
    new-instance v0, Lcom/sec/vip/cropimage/ae;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/sec/vip/cropimage/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/vip/cropimage/ae;->a:Lcom/sec/vip/cropimage/ae;

    .line 147
    new-instance v0, Lcom/sec/vip/cropimage/ae;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, Lcom/sec/vip/cropimage/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/vip/cropimage/ae;->b:Lcom/sec/vip/cropimage/ae;

    .line 148
    new-instance v0, Lcom/sec/vip/cropimage/ae;

    const-string v1, "OOM"

    invoke-direct {v0, v1, v4}, Lcom/sec/vip/cropimage/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/vip/cropimage/ae;->c:Lcom/sec/vip/cropimage/ae;

    .line 145
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/vip/cropimage/ae;

    sget-object v1, Lcom/sec/vip/cropimage/ae;->a:Lcom/sec/vip/cropimage/ae;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/vip/cropimage/ae;->b:Lcom/sec/vip/cropimage/ae;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/vip/cropimage/ae;->c:Lcom/sec/vip/cropimage/ae;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/vip/cropimage/ae;->d:[Lcom/sec/vip/cropimage/ae;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/vip/cropimage/ae;
    .locals 1

    .prologue
    .line 145
    const-class v0, Lcom/sec/vip/cropimage/ae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/ae;

    return-object v0
.end method

.method public static values()[Lcom/sec/vip/cropimage/ae;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/sec/vip/cropimage/ae;->d:[Lcom/sec/vip/cropimage/ae;

    invoke-virtual {v0}, [Lcom/sec/vip/cropimage/ae;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/vip/cropimage/ae;

    return-object v0
.end method
