.class Lcom/sec/vip/cropimage/b;
.super Ljava/lang/Object;
.source "CropImage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/vip/cropimage/CropImage;


# direct methods
.method constructor <init>(Lcom/sec/vip/cropimage/CropImage;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 313
    iget-object v0, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->b(Lcom/sec/vip/cropimage/CropImage;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 346
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->c(Lcom/sec/vip/cropimage/CropImage;)Z

    .line 319
    iget-object v0, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0, v6}, Lcom/sec/vip/cropimage/CropImage;->a(Lcom/sec/vip/cropimage/CropImage;Z)Z

    .line 320
    iget-object v0, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    iget-object v1, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v1}, Lcom/sec/vip/cropimage/CropImage;->d(Lcom/sec/vip/cropimage/CropImage;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/vip/cropimage/CropImage;->a(Lcom/sec/vip/cropimage/CropImage;I)I

    .line 322
    iget-object v0, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    const v1, 0x7f0b0233

    invoke-virtual {v0, v1}, Lcom/sec/vip/cropimage/CropImage;->setTitle(I)V

    .line 324
    iget-object v0, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0, v6}, Lcom/sec/vip/cropimage/CropImage;->b(Lcom/sec/vip/cropimage/CropImage;Z)V

    .line 327
    iget-object v0, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->e(Lcom/sec/vip/cropimage/CropImage;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v0}, Lcom/sec/vip/cropimage/CropImage;->e(Lcom/sec/vip/cropimage/CropImage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    move v1, v2

    .line 330
    :goto_1
    if-ge v1, v0, :cond_3

    .line 331
    iget-object v3, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v3}, Lcom/sec/vip/cropimage/CropImage;->e(Lcom/sec/vip/cropimage/CropImage;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 332
    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v4

    iget-object v5, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-static {v5}, Lcom/sec/vip/cropimage/CropImage;->d(Lcom/sec/vip/cropimage/CropImage;)I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 335
    const v0, 0x7f070062

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 336
    const v0, 0x7f070061

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    move v0, v1

    .line 340
    :goto_2
    iget-object v1, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-virtual {v1}, Lcom/sec/vip/cropimage/CropImage;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 341
    iget-object v3, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    invoke-virtual {v3}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09014f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 342
    mul-int/2addr v0, v3

    sub-int/2addr v1, v3

    int-to-float v1, v1

    mul-float/2addr v1, v7

    add-float/2addr v1, v7

    float-to-int v1, v1

    sub-int v1, v0, v1

    .line 343
    iget-object v0, p0, Lcom/sec/vip/cropimage/b;->a:Lcom/sec/vip/cropimage/CropImage;

    const v3, 0x7f07025a

    invoke-virtual {v0, v3}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto/16 :goto_0

    .line 330
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method
