.class public Lcom/sec/vip/cropimage/CropImageView;
.super Lcom/sec/vip/cropimage/ImageViewTouchBase;
.source "CropImageView.java"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/vip/cropimage/p;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/sec/vip/cropimage/p;

.field d:F

.field e:F

.field f:I

.field g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/CropImageView;->g:Z

    .line 46
    return-void
.end method

.method private a(FFFFF)Landroid/graphics/PointF;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 178
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 179
    float-to-int v1, p5

    sparse-switch v1, :sswitch_data_0

    .line 191
    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 194
    :goto_0
    return-object v0

    .line 181
    :sswitch_0
    sub-float v1, p3, p1

    invoke-virtual {v0, p2, v1}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 184
    :sswitch_1
    sub-float v1, p3, p1

    sub-float v2, p4, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 187
    :sswitch_2
    sub-float v1, p4, p2

    invoke-virtual {v0, v1, p1}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 179
    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch
.end method

.method private a(Lcom/sec/vip/cropimage/p;Z)V
    .locals 7

    .prologue
    const v4, 0x3f19999a    # 0.6f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 228
    iget-object v0, p1, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    .line 230
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    .line 231
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    .line 233
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    .line 234
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    .line 236
    div-float v1, v2, v1

    mul-float/2addr v1, v4

    .line 237
    div-float v0, v3, v0

    mul-float/2addr v0, v4

    .line 239
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 240
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->a()F

    move-result v1

    mul-float/2addr v0, v1

    .line 241
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 243
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->a()F

    move-result v1

    sub-float v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v1, v1

    const-wide v3, 0x3fb999999999999aL    # 0.1

    cmpl-double v1, v1, v3

    if-lez v1, :cond_0

    .line 244
    const/4 v1, 0x2

    new-array v1, v1, [F

    iget-object v2, p1, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    aput v2, v1, v5

    iget-object v2, p1, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    aput v2, v1, v6

    .line 245
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 247
    if-eqz p2, :cond_1

    .line 248
    aget v2, v1, v5

    aget v1, v1, v6

    const/high16 v3, 0x43960000    # 300.0f

    invoke-virtual {p0, v0, v2, v1, v3}, Lcom/sec/vip/cropimage/CropImageView;->a(FFFF)V

    .line 254
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/vip/cropimage/CropImageView;->b(Lcom/sec/vip/cropimage/p;)V

    .line 255
    return-void

    .line 250
    :cond_1
    aget v2, v1, v5

    aget v1, v1, v6

    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/vip/cropimage/CropImageView;->a(FFF)V

    goto :goto_0
.end method

.method private b(Lcom/sec/vip/cropimage/p;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 204
    iget-object v1, p1, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    .line 206
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->getLeft()I

    move-result v0

    iget v2, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 207
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->getRight()I

    move-result v0

    iget v3, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v3

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 209
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->getTop()I

    move-result v0

    iget v4, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v4

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 210
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->getBottom()I

    move-result v4

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v1, v4, v1

    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 212
    if-eqz v2, :cond_2

    .line 213
    :goto_0
    if-eqz v0, :cond_3

    .line 215
    :goto_1
    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    .line 216
    :cond_0
    int-to-float v1, v2

    int-to-float v0, v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/vip/cropimage/CropImageView;->b(FF)V

    .line 218
    :cond_1
    return-void

    :cond_2
    move v2, v3

    .line 212
    goto :goto_0

    :cond_3
    move v0, v1

    .line 213
    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic a()F
    .locals 1

    .prologue
    .line 12
    invoke-super {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a()F

    move-result v0

    return v0
.end method

.method protected a(FF)V
    .locals 4

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(FF)V

    .line 78
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 79
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    .line 81
    iget-object v3, v0, Lcom/sec/vip/cropimage/p;->f:Landroid/graphics/Matrix;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 82
    invoke-virtual {v0}, Lcom/sec/vip/cropimage/p;->c()V

    .line 79
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 84
    :cond_0
    return-void
.end method

.method protected a(FFF)V
    .locals 4

    .prologue
    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(FFF)V

    .line 51
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    .line 52
    iget-object v2, v0, Lcom/sec/vip/cropimage/p;->f:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 53
    invoke-virtual {v0}, Lcom/sec/vip/cropimage/p;->c()V

    goto :goto_0

    .line 55
    :cond_0
    return-void
.end method

.method public a(Lcom/sec/vip/cropimage/p;)V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->invalidate()V

    .line 275
    return-void
.end method

.method public bridge synthetic a(ZZ)V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0, p1, p2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->a(ZZ)V

    return-void
.end method

.method public bridge synthetic b()V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->b()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 259
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->onDraw(Landroid/graphics/Canvas;)V

    .line 260
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 261
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    invoke-virtual {v0, p1}, Lcom/sec/vip/cropimage/p;->a(Landroid/graphics/Canvas;)V

    .line 261
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 264
    :cond_0
    return-void
.end method

.method public bridge synthetic onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 12
    invoke-super {p0, p1, p2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 12
    invoke-super {p0, p1, p2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 23
    invoke-super/range {p0 .. p5}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->onLayout(ZIIII)V

    .line 24
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->j:Lcom/sec/vip/cropimage/al;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/al;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 25
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    .line 26
    iget-object v2, v0, Lcom/sec/vip/cropimage/p;->f:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 27
    invoke-virtual {v0}, Lcom/sec/vip/cropimage/p;->c()V

    .line 28
    iget-boolean v2, v0, Lcom/sec/vip/cropimage/p;->b:Z

    if-eqz v2, :cond_0

    .line 30
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/sec/vip/cropimage/CropImageView;->a(Lcom/sec/vip/cropimage/p;Z)V

    goto :goto_0

    .line 34
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/16 v5, 0x20

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 89
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->a:Landroid/content/Context;

    move-object v6, v0

    check-cast v6, Lcom/sec/vip/cropimage/MonitoredActivity;

    .line 90
    iget-boolean v0, v6, Lcom/sec/vip/cropimage/MonitoredActivity;->e:Z

    if-eqz v0, :cond_0

    move v0, v7

    .line 174
    :goto_0
    return v0

    .line 94
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 95
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 97
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 159
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :cond_2
    :goto_2
    move v0, v8

    .line 174
    goto :goto_0

    .line 99
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 100
    :goto_3
    if-ge v7, v3, :cond_1

    .line 101
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/p;

    .line 102
    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/cropimage/p;->a(FF)I

    move-result v4

    .line 103
    if-eq v4, v8, :cond_4

    .line 104
    iput v4, p0, Lcom/sec/vip/cropimage/CropImageView;->f:I

    .line 105
    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    .line 106
    iput v1, p0, Lcom/sec/vip/cropimage/CropImageView;->d:F

    .line 107
    iput v2, p0, Lcom/sec/vip/cropimage/CropImageView;->e:F

    .line 108
    iput-boolean v8, p0, Lcom/sec/vip/cropimage/CropImageView;->g:Z

    .line 109
    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    if-ne v4, v5, :cond_3

    sget-object v0, Lcom/sec/vip/cropimage/q;->b:Lcom/sec/vip/cropimage/q;

    :goto_4
    invoke-virtual {v1, v0}, Lcom/sec/vip/cropimage/p;->a(Lcom/sec/vip/cropimage/q;)V

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/sec/vip/cropimage/q;->c:Lcom/sec/vip/cropimage/q;

    goto :goto_4

    .line 100
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 115
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    if-eqz v0, :cond_5

    .line 117
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    invoke-direct {p0, v0, v8}, Lcom/sec/vip/cropimage/CropImageView;->a(Lcom/sec/vip/cropimage/p;Z)V

    .line 118
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    sget-object v1, Lcom/sec/vip/cropimage/q;->a:Lcom/sec/vip/cropimage/q;

    invoke-virtual {v0, v1}, Lcom/sec/vip/cropimage/p;->a(Lcom/sec/vip/cropimage/q;)V

    .line 120
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    goto :goto_1

    .line 123
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    if-eqz v0, :cond_1

    .line 124
    iget v0, p0, Lcom/sec/vip/cropimage/CropImageView;->f:I

    if-ne v0, v5, :cond_7

    .line 125
    invoke-virtual {v6}, Lcom/sec/vip/cropimage/MonitoredActivity;->a()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/MonitoredActivity;->b()I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/MonitoredActivity;->c()F

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/cropimage/CropImageView;->a(FFFFF)Landroid/graphics/PointF;

    move-result-object v9

    .line 126
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImageView;->g:Z

    if-eqz v0, :cond_6

    .line 127
    iget v1, p0, Lcom/sec/vip/cropimage/CropImageView;->d:F

    iget v2, p0, Lcom/sec/vip/cropimage/CropImageView;->e:F

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/MonitoredActivity;->a()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/MonitoredActivity;->b()I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/MonitoredActivity;->c()F

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/cropimage/CropImageView;->a(FFFFF)Landroid/graphics/PointF;

    move-result-object v0

    .line 128
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iput v1, p0, Lcom/sec/vip/cropimage/CropImageView;->d:F

    .line 129
    iget v0, v0, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/sec/vip/cropimage/CropImageView;->e:F

    .line 130
    iput-boolean v7, p0, Lcom/sec/vip/cropimage/CropImageView;->g:Z

    .line 134
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    iget v1, p0, Lcom/sec/vip/cropimage/CropImageView;->f:I

    iget v2, v9, Landroid/graphics/PointF;->x:F

    iget v3, p0, Lcom/sec/vip/cropimage/CropImageView;->d:F

    sub-float/2addr v2, v3

    iget v3, v9, Landroid/graphics/PointF;->y:F

    iget v4, p0, Lcom/sec/vip/cropimage/CropImageView;->e:F

    sub-float/2addr v3, v4

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/MonitoredActivity;->c()F

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/vip/cropimage/p;->a(IFFF)V

    .line 136
    iget v0, v9, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/sec/vip/cropimage/CropImageView;->d:F

    .line 137
    iget v0, v9, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/sec/vip/cropimage/CropImageView;->e:F

    .line 153
    :goto_5
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/CropImageView;->b(Lcom/sec/vip/cropimage/p;)V

    goto/16 :goto_1

    .line 141
    :cond_7
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImageView;->c:Lcom/sec/vip/cropimage/p;

    iget v3, p0, Lcom/sec/vip/cropimage/CropImageView;->f:I

    iget v4, p0, Lcom/sec/vip/cropimage/CropImageView;->d:F

    sub-float v4, v1, v4

    iget v5, p0, Lcom/sec/vip/cropimage/CropImageView;->e:F

    sub-float v5, v2, v5

    invoke-virtual {v6}, Lcom/sec/vip/cropimage/MonitoredActivity;->c()F

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/sec/vip/cropimage/p;->a(IFFF)V

    .line 143
    iput v1, p0, Lcom/sec/vip/cropimage/CropImageView;->d:F

    .line 144
    iput v2, p0, Lcom/sec/vip/cropimage/CropImageView;->e:F

    goto :goto_5

    .line 161
    :pswitch_3
    invoke-virtual {p0, v8, v8}, Lcom/sec/vip/cropimage/CropImageView;->a(ZZ)V

    goto/16 :goto_2

    .line 168
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImageView;->a()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 169
    invoke-virtual {p0, v8, v8}, Lcom/sec/vip/cropimage/CropImageView;->a(ZZ)V

    goto/16 :goto_2

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 159
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public bridge synthetic setImageBitmapResetBase(Landroid/graphics/Bitmap;IZ)V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageBitmapResetBase(Landroid/graphics/Bitmap;IZ)V

    return-void
.end method

.method public bridge synthetic setImageRotateBitmapResetBase(Lcom/sec/vip/cropimage/al;Z)V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0, p1, p2}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setImageRotateBitmapResetBase(Lcom/sec/vip/cropimage/al;Z)V

    return-void
.end method

.method public bridge synthetic setRecycler(Lcom/sec/vip/cropimage/ai;)V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/ImageViewTouchBase;->setRecycler(Lcom/sec/vip/cropimage/ai;)V

    return-void
.end method
