.class public Lcom/sec/vip/cropimage/p;
.super Ljava/lang/Object;
.source "HighlightView.java"


# instance fields
.field a:Landroid/view/View;

.field b:Z

.field c:Z

.field d:Landroid/graphics/Rect;

.field e:Landroid/graphics/RectF;

.field f:Landroid/graphics/Matrix;

.field private g:I

.field private h:I

.field private i:Landroid/graphics/Bitmap;

.field private j:Landroid/graphics/Matrix;

.field private k:Lcom/sec/vip/cropimage/q;

.field private l:Landroid/graphics/RectF;

.field private m:Z

.field private n:F

.field private o:Z

.field private p:Landroid/graphics/drawable/Drawable;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Landroid/graphics/drawable/Drawable;

.field private s:Landroid/graphics/drawable/Drawable;

.field private t:Landroid/graphics/drawable/Drawable;

.field private final u:Landroid/graphics/Paint;

.field private final v:Landroid/graphics/Paint;

.field private final w:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/vip/cropimage/p;->g:I

    .line 57
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/vip/cropimage/p;->h:I

    .line 59
    iput-object v2, p0, Lcom/sec/vip/cropimage/p;->i:Landroid/graphics/Bitmap;

    .line 60
    iput-object v2, p0, Lcom/sec/vip/cropimage/p;->j:Landroid/graphics/Matrix;

    .line 711
    sget-object v0, Lcom/sec/vip/cropimage/q;->a:Lcom/sec/vip/cropimage/q;

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->k:Lcom/sec/vip/cropimage/q;

    .line 718
    iput-boolean v1, p0, Lcom/sec/vip/cropimage/p;->m:Z

    .line 720
    iput-boolean v1, p0, Lcom/sec/vip/cropimage/p;->o:Z

    .line 729
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->u:Landroid/graphics/Paint;

    .line 730
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->v:Landroid/graphics/Paint;

    .line 731
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->w:Landroid/graphics/Paint;

    .line 69
    iput-object p1, p0, Lcom/sec/vip/cropimage/p;->a:Landroid/view/View;

    .line 70
    return-void
.end method

.method private a(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/sec/vip/cropimage/p;->g:I

    packed-switch v0, :pswitch_data_0

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    .line 120
    :goto_0
    return-void

    .line 108
    :pswitch_0
    const v0, 0x7f020420

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 111
    :pswitch_1
    const v0, 0x7f020419

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 114
    :pswitch_2
    const v0, 0x7f020421

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 117
    :pswitch_3
    const v0, 0x7f02041a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    .line 662
    if-eqz p2, :cond_0

    .line 664
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    .line 665
    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 666
    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 667
    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 670
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 675
    :try_start_0
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    iget-object v5, p0, Lcom/sec/vip/cropimage/p;->j:Landroid/graphics/Matrix;

    const/4 v6, 0x1

    move-object v0, p2

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 679
    const/4 v1, 0x0

    :try_start_1
    iget-object v2, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v1, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 686
    if-nez v0, :cond_3

    .line 700
    :cond_0
    :goto_0
    return-void

    .line 681
    :catch_0
    move-exception v0

    move-object v0, v7

    .line 682
    :goto_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0149

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 686
    if-eqz v0, :cond_0

    .line 693
    if-eq p2, v0, :cond_0

    .line 694
    :goto_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 693
    :cond_1
    if-eq p2, v7, :cond_2

    .line 694
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 686
    :cond_2
    throw v0

    :catchall_0
    move-exception v0

    :goto_3
    if-nez v7, :cond_1

    goto :goto_0

    .line 693
    :cond_3
    if-eq p2, v0, :cond_0

    goto :goto_2

    .line 686
    :catchall_1
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_3

    .line 681
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private b(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/vip/cropimage/p;->h:I

    packed-switch v0, :pswitch_data_0

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    .line 141
    :goto_0
    return-void

    .line 135
    :pswitch_0
    const v0, 0x7f02041f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 138
    :pswitch_1
    const v0, 0x7f020422

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x7d6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d()V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 77
    const v1, 0x7f020299

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/cropimage/p;->p:Landroid/graphics/drawable/Drawable;

    .line 81
    const v1, 0x7f020078

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/cropimage/p;->q:Landroid/graphics/drawable/Drawable;

    .line 82
    const v1, 0x7f020077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/cropimage/p;->r:Landroid/graphics/drawable/Drawable;

    .line 83
    const v1, 0x7f0202b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/cropimage/p;->s:Landroid/graphics/drawable/Drawable;

    .line 86
    iget v1, p0, Lcom/sec/vip/cropimage/p;->g:I

    const/16 v2, 0x3e8

    if-eq v1, v2, :cond_0

    .line 87
    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/p;->a(Landroid/content/res/Resources;)V

    .line 93
    :goto_0
    return-void

    .line 88
    :cond_0
    iget v1, p0, Lcom/sec/vip/cropimage/p;->h:I

    const/16 v2, 0x7d0

    if-eq v1, v2, :cond_1

    .line 89
    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/p;->b(Landroid/content/res/Resources;)V

    goto :goto_0

    .line 91
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private e()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 566
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 567
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->f:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 568
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method


# virtual methods
.method public a(FF)I
    .locals 9

    .prologue
    const/16 v0, 0x20

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x1

    const/high16 v7, 0x42200000    # 40.0f

    .line 315
    invoke-direct {p0}, Lcom/sec/vip/cropimage/p;->e()Landroid/graphics/Rect;

    move-result-object v5

    .line 319
    iget-boolean v2, p0, Lcom/sec/vip/cropimage/p;->o:Z

    if-eqz v2, :cond_5

    .line 320
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    sub-float v2, p1, v2

    .line 321
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    sub-float v3, p2, v3

    .line 322
    mul-float v4, v2, v2

    mul-float v5, v3, v3

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 323
    iget-object v5, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    .line 324
    sub-int v6, v4, v5

    .line 325
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-float v6, v6

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_4

    .line 326
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 327
    cmpg-float v0, v3, v8

    if-gez v0, :cond_1

    .line 328
    const/16 v0, 0x8

    .line 369
    :cond_0
    :goto_0
    return v0

    .line 330
    :cond_1
    const/16 v0, 0x10

    goto :goto_0

    .line 333
    :cond_2
    cmpg-float v0, v2, v8

    if-gez v0, :cond_3

    .line 334
    const/4 v0, 0x2

    goto :goto_0

    .line 336
    :cond_3
    const/4 v0, 0x4

    goto :goto_0

    .line 339
    :cond_4
    if-lt v4, v5, :cond_0

    move v0, v1

    .line 342
    goto :goto_0

    .line 347
    :cond_5
    iget v2, v5, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    sub-float/2addr v2, v7

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_a

    iget v2, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    add-float/2addr v2, v7

    cmpg-float v2, p2, v2

    if-gez v2, :cond_a

    move v2, v1

    .line 348
    :goto_1
    iget v4, v5, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    sub-float/2addr v4, v7

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_6

    iget v4, v5, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    add-float/2addr v4, v7

    cmpg-float v4, p1, v4

    if-gez v4, :cond_6

    move v3, v1

    .line 351
    :cond_6
    iget v4, v5, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    sub-float/2addr v4, p1

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v7

    if-gez v4, :cond_c

    if-eqz v2, :cond_c

    .line 352
    const/4 v4, 0x3

    .line 354
    :goto_2
    iget v6, v5, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    sub-float/2addr v6, p1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v7

    if-gez v6, :cond_7

    if-eqz v2, :cond_7

    .line 355
    or-int/lit8 v4, v4, 0x4

    .line 357
    :cond_7
    iget v2, v5, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    sub-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v7

    if-gez v2, :cond_8

    if-eqz v3, :cond_8

    .line 358
    or-int/lit8 v4, v4, 0x8

    .line 360
    :cond_8
    iget v2, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    sub-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v7

    if-gez v2, :cond_b

    if-eqz v3, :cond_b

    .line 361
    or-int/lit8 v4, v4, 0x10

    move v2, v4

    .line 365
    :goto_3
    if-ne v2, v1, :cond_9

    float-to-int v1, p1

    float-to-int v3, p2

    invoke-virtual {v5, v1, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_9
    move v0, v2

    goto :goto_0

    :cond_a
    move v2, v3

    .line 347
    goto :goto_1

    :cond_b
    move v2, v4

    goto :goto_3

    :cond_c
    move v4, v1

    goto :goto_2
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 633
    iput p1, p0, Lcom/sec/vip/cropimage/p;->g:I

    .line 634
    invoke-direct {p0}, Lcom/sec/vip/cropimage/p;->d()V

    .line 635
    return-void
.end method

.method a(IFF)V
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 384
    invoke-direct {p0}, Lcom/sec/vip/cropimage/p;->e()Landroid/graphics/Rect;

    move-result-object v3

    .line 385
    if-ne p1, v2, :cond_0

    .line 404
    :goto_0
    return-void

    .line 387
    :cond_0
    const/16 v4, 0x20

    if-ne p1, v4, :cond_1

    .line 389
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    mul-float/2addr v0, p2

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, p3

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/cropimage/p;->b(FF)V

    goto :goto_0

    .line 391
    :cond_1
    and-int/lit8 v4, p1, 0x6

    if-nez v4, :cond_2

    move p2, v0

    .line 395
    :cond_2
    and-int/lit8 v4, p1, 0x18

    if-nez v4, :cond_3

    move p3, v0

    .line 400
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v0, v4

    mul-float v4, p2, v0

    .line 401
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    mul-float v3, p3, v0

    .line 402
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    int-to-float v0, v0

    mul-float/2addr v4, v0

    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    int-to-float v0, v0

    mul-float/2addr v0, v3

    invoke-virtual {p0, v4, v0}, Lcom/sec/vip/cropimage/p;->c(FF)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method a(IFFF)V
    .locals 8

    .prologue
    const/high16 v7, 0x43870000    # 270.0f

    const/high16 v6, 0x42b40000    # 90.0f

    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 408
    invoke-direct {p0}, Lcom/sec/vip/cropimage/p;->e()Landroid/graphics/Rect;

    move-result-object v4

    .line 409
    if-ne p1, v3, :cond_0

    .line 453
    :goto_0
    return-void

    .line 411
    :cond_0
    const/16 v1, 0x20

    if-ne p1, v1, :cond_3

    .line 413
    cmpl-float v0, p4, v6

    if-eqz v0, :cond_1

    cmpl-float v0, p4, v7

    if-nez v0, :cond_2

    .line 414
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    mul-float/2addr v0, p2

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, p3

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/cropimage/p;->b(FF)V

    goto :goto_0

    .line 416
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    mul-float/2addr v0, p2

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, p3

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/cropimage/p;->b(FF)V

    goto :goto_0

    .line 421
    :cond_3
    cmpl-float v1, p4, v6

    if-eqz v1, :cond_4

    cmpl-float v1, p4, v7

    if-nez v1, :cond_7

    .line 424
    :cond_4
    and-int/lit8 v1, p1, 0x6

    if-nez v1, :cond_f

    move v1, v0

    .line 427
    :goto_1
    and-int/lit8 v5, p1, 0x18

    if-nez v5, :cond_5

    move p3, v0

    :cond_5
    move v0, v1

    .line 442
    :goto_2
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v1, v5

    mul-float/2addr v1, p3

    .line 443
    iget-object v5, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v5, v4

    mul-float/2addr v4, v0

    .line 444
    cmpl-float v0, p4, v6

    if-eqz v0, :cond_6

    cmpl-float v0, p4, v7

    if-nez v0, :cond_a

    .line 445
    :cond_6
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_8

    move v0, v2

    :goto_3
    int-to-float v0, v0

    mul-float/2addr v1, v0

    .line 446
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_9

    move v0, v2

    :goto_4
    int-to-float v0, v0

    mul-float/2addr v0, v4

    .line 451
    :goto_5
    invoke-virtual {p0, v1, v0}, Lcom/sec/vip/cropimage/p;->c(FF)V

    goto/16 :goto_0

    .line 433
    :cond_7
    and-int/lit8 v1, p1, 0x6

    if-nez v1, :cond_e

    move v1, v0

    .line 436
    :goto_6
    and-int/lit8 v5, p1, 0x18

    if-nez v5, :cond_d

    move p3, v1

    .line 437
    goto :goto_2

    :cond_8
    move v0, v3

    .line 445
    goto :goto_3

    :cond_9
    move v0, v3

    .line 446
    goto :goto_4

    .line 448
    :cond_a
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_b

    move v0, v2

    :goto_7
    int-to-float v0, v0

    mul-float/2addr v1, v0

    .line 449
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_c

    :goto_8
    int-to-float v0, v2

    mul-float/2addr v0, v4

    goto :goto_5

    :cond_b
    move v0, v3

    .line 448
    goto :goto_7

    :cond_c
    move v2, v3

    .line 449
    goto :goto_8

    :cond_d
    move v0, p3

    move p3, v1

    goto :goto_2

    :cond_e
    move v1, p2

    goto :goto_6

    :cond_f
    move v1, p2

    goto :goto_1
.end method

.method public a(Landroid/graphics/Bitmap;IF)V
    .locals 1

    .prologue
    .line 644
    iput p2, p0, Lcom/sec/vip/cropimage/p;->h:I

    .line 645
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->j:Landroid/graphics/Matrix;

    .line 646
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->j:Landroid/graphics/Matrix;

    invoke-virtual {v0, p3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 647
    iput-object p1, p0, Lcom/sec/vip/cropimage/p;->i:Landroid/graphics/Bitmap;

    .line 648
    invoke-direct {p0}, Lcom/sec/vip/cropimage/p;->d()V

    .line 649
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 183
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->i:Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, v0}, Lcom/sec/vip/cropimage/p;->a(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    .line 185
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/p;->c:Z

    if-eqz v0, :cond_0

    .line 290
    :goto_0
    return-void

    .line 188
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 189
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 190
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/p;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->w:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 192
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 194
    :cond_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 195
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 196
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/p;->o:Z

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    .line 198
    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    .line 199
    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    div-float v5, v0, v6

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    div-float/2addr v3, v6

    add-float/2addr v3, v5

    div-float/2addr v0, v6

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v4, v3, v0, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 200
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->w:Landroid/graphics/Paint;

    const v3, -0x10fb2a

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 211
    :goto_1
    sget-object v0, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 212
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/p;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->u:Landroid/graphics/Paint;

    :goto_2
    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 214
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 216
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->w:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 218
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 221
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->p:Landroid/graphics/drawable/Drawable;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 232
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 235
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->k:Lcom/sec/vip/cropimage/q;

    sget-object v1, Lcom/sec/vip/cropimage/q;->c:Lcom/sec/vip/cropimage/q;

    if-ne v0, v1, :cond_5

    .line 236
    new-array v0, v8, [I

    const v1, 0x10100a7

    aput v1, v0, v7

    .line 237
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 238
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 246
    :goto_3
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/p;->o:Z

    if-eqz v0, :cond_6

    .line 247
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 248
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 250
    const-wide v2, 0x3fe921fb54442d18L    # 0.7853981633974483

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-double v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    .line 251
    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    add-int/2addr v3, v2

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    .line 252
    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    sub-int v2, v3, v2

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    .line 253
    iget-object v2, p0, Lcom/sec/vip/cropimage/p;->s:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 254
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 204
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 205
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 208
    :cond_3
    new-instance v0, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    invoke-direct {v0, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v0, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    goto/16 :goto_1

    .line 212
    :cond_4
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->v:Landroid/graphics/Paint;

    goto/16 :goto_2

    .line 240
    :cond_5
    new-array v0, v8, [I

    const v1, -0x10100a7

    aput v1, v0, v7

    .line 241
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 242
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto/16 :goto_3

    .line 256
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v0, 0x1

    .line 257
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v1, v1, 0x1

    .line 258
    iget-object v2, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/lit8 v2, v2, 0x1

    .line 259
    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, 0x1

    .line 261
    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 262
    iget-object v5, p0, Lcom/sec/vip/cropimage/p;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    .line 263
    iget-object v6, p0, Lcom/sec/vip/cropimage/p;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    .line 264
    iget-object v7, p0, Lcom/sec/vip/cropimage/p;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    .line 266
    iget-object v8, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    iget-object v10, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    .line 267
    iget-object v9, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    iget-object v10, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget-object v11, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    sub-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    .line 276
    iget-object v10, p0, Lcom/sec/vip/cropimage/p;->q:Landroid/graphics/drawable/Drawable;

    sub-int v11, v0, v4

    sub-int v12, v9, v5

    add-int/2addr v0, v4

    add-int v13, v9, v5

    invoke-virtual {v10, v11, v12, v0, v13}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 277
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 279
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->q:Landroid/graphics/drawable/Drawable;

    sub-int v10, v1, v4

    sub-int v11, v9, v5

    add-int/2addr v1, v4

    add-int v4, v9, v5

    invoke-virtual {v0, v10, v11, v1, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 280
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 282
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->r:Landroid/graphics/drawable/Drawable;

    sub-int v1, v8, v7

    sub-int v4, v2, v6

    add-int v5, v8, v7

    add-int/2addr v2, v6

    invoke-virtual {v0, v1, v4, v5, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 283
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 285
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->r:Landroid/graphics/drawable/Drawable;

    sub-int v1, v8, v7

    sub-int v2, v3, v6

    add-int v4, v8, v7

    add-int/2addr v3, v6

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 286
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method public a(Landroid/graphics/Matrix;Landroid/graphics/Rect;Landroid/graphics/RectF;ZIIZ)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 595
    if-eqz p4, :cond_0

    move p7, v0

    .line 598
    :cond_0
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1, p1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v1, p0, Lcom/sec/vip/cropimage/p;->f:Landroid/graphics/Matrix;

    .line 600
    iput-object p3, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    .line 601
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, p2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iput-object v1, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    .line 602
    iput-boolean p7, p0, Lcom/sec/vip/cropimage/p;->m:Z

    .line 603
    iput-boolean p4, p0, Lcom/sec/vip/cropimage/p;->o:Z

    .line 604
    iput p5, p0, Lcom/sec/vip/cropimage/p;->g:I

    .line 605
    iput p6, p0, Lcom/sec/vip/cropimage/p;->h:I

    .line 607
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/vip/cropimage/p;->n:F

    .line 608
    invoke-direct {p0}, Lcom/sec/vip/cropimage/p;->e()Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    .line 615
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->u:Landroid/graphics/Paint;

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 616
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->v:Landroid/graphics/Paint;

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 618
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->w:Landroid/graphics/Paint;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 619
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->w:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 620
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->w:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 622
    sget-object v0, Lcom/sec/vip/cropimage/q;->a:Lcom/sec/vip/cropimage/q;

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->k:Lcom/sec/vip/cropimage/q;

    .line 623
    invoke-direct {p0}, Lcom/sec/vip/cropimage/p;->d()V

    .line 624
    return-void
.end method

.method public a(Lcom/sec/vip/cropimage/q;)V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->k:Lcom/sec/vip/cropimage/q;

    if-eq p1, v0, :cond_0

    .line 300
    iput-object p1, p0, Lcom/sec/vip/cropimage/p;->k:Lcom/sec/vip/cropimage/q;

    .line 301
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 303
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/sec/vip/cropimage/p;->b:Z

    .line 163
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/p;->b:Z

    return v0
.end method

.method public b()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 557
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method b(FF)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 466
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 468
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/RectF;->offset(FF)V

    .line 471
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    invoke-static {v5, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    invoke-static {v5, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->offset(FF)V

    .line 473
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    invoke-static {v5, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->offset(FF)V

    .line 475
    invoke-direct {p0}, Lcom/sec/vip/cropimage/p;->e()Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    .line 476
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 479
    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090146

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 480
    neg-int v2, v1

    neg-int v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Rect;->inset(II)V

    .line 483
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 484
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 575
    invoke-direct {p0}, Lcom/sec/vip/cropimage/p;->e()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    .line 576
    return-void
.end method

.method c(FF)V
    .locals 8

    .prologue
    const/high16 v2, 0x41c80000    # 25.0f

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 495
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/p;->m:Z

    if-eqz v0, :cond_0

    .line 496
    cmpl-float v0, p1, v6

    if-eqz v0, :cond_6

    .line 497
    iget v0, p0, Lcom/sec/vip/cropimage/p;->n:F

    div-float p2, p1, v0

    .line 506
    :cond_0
    :goto_0
    new-instance v3, Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-direct {v3, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 507
    cmpl-float v0, p1, v6

    if-lez v0, :cond_a

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float v1, v7, p1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_a

    .line 508
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v1

    sub-float/2addr v0, v1

    div-float p1, v0, v7

    .line 510
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/p;->m:Z

    if-eqz v0, :cond_a

    .line 511
    iget v0, p0, Lcom/sec/vip/cropimage/p;->n:F

    div-float p2, p1, v0

    move v0, p2

    move v1, p1

    .line 514
    :goto_1
    cmpl-float v4, v0, v6

    if-lez v4, :cond_1

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v4

    mul-float v5, v7, v0

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 515
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float/2addr v0, v4

    div-float/2addr v0, v7

    .line 517
    iget-boolean v4, p0, Lcom/sec/vip/cropimage/p;->m:Z

    if-eqz v4, :cond_1

    .line 518
    iget v1, p0, Lcom/sec/vip/cropimage/p;->n:F

    mul-float/2addr v1, v0

    .line 522
    :cond_1
    neg-float v1, v1

    neg-float v0, v0

    invoke-virtual {v3, v1, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 526
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_2

    .line 527
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v0

    sub-float v0, v2, v0

    neg-float v0, v0

    div-float/2addr v0, v7

    invoke-virtual {v3, v0, v6}, Landroid/graphics/RectF;->inset(FF)V

    .line 529
    :cond_2
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/p;->m:Z

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/sec/vip/cropimage/p;->n:F

    div-float v0, v2, v0

    .line 530
    :goto_2
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpg-float v1, v1, v0

    if-gez v1, :cond_3

    .line 531
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float/2addr v0, v1

    neg-float v0, v0

    div-float/2addr v0, v7

    invoke-virtual {v3, v6, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 535
    :cond_3
    iget v0, v3, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_8

    .line 536
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget v1, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    invoke-virtual {v3, v0, v6}, Landroid/graphics/RectF;->offset(FF)V

    .line 540
    :cond_4
    :goto_3
    iget v0, v3, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_9

    .line 541
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget v1, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    invoke-virtual {v3, v6, v0}, Landroid/graphics/RectF;->offset(FF)V

    .line 546
    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->e:Landroid/graphics/RectF;

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 547
    invoke-direct {p0}, Lcom/sec/vip/cropimage/p;->e()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/p;->d:Landroid/graphics/Rect;

    .line 548
    iget-object v0, p0, Lcom/sec/vip/cropimage/p;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 549
    return-void

    .line 498
    :cond_6
    cmpl-float v0, p2, v6

    if-eqz v0, :cond_0

    .line 499
    iget v0, p0, Lcom/sec/vip/cropimage/p;->n:F

    mul-float p1, p2, v0

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 529
    goto :goto_2

    .line 537
    :cond_8
    iget v0, v3, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 538
    iget v0, v3, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v1

    neg-float v0, v0

    invoke-virtual {v3, v0, v6}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_3

    .line 542
    :cond_9
    iget v0, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 543
    iget v0, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/sec/vip/cropimage/p;->l:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v1

    neg-float v0, v0

    invoke-virtual {v3, v6, v0}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_4

    :cond_a
    move v0, p2

    move v1, p1

    goto/16 :goto_1
.end method
