.class public Lcom/sec/vip/cropimage/CropImage;
.super Lcom/sec/vip/cropimage/MonitoredActivity;
.source "CropImage.java"


# instance fields
.field private A:Landroid/widget/ImageView;

.field private B:Landroid/widget/ImageView;

.field private C:Landroid/widget/ImageView;

.field private D:Ljava/lang/String;

.field private E:Z

.field private F:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/vip/cropimage/o;",
            ">;"
        }
    .end annotation
.end field

.field private G:Z

.field private H:I

.field private I:Landroid/net/Uri;

.field private J:I

.field private K:I

.field private L:I

.field private M:Z

.field private N:Z

.field private O:Landroid/view/View$OnClickListener;

.field private P:Landroid/view/View$OnClickListener;

.field private Q:Landroid/os/Handler;

.field a:Lcom/sec/vip/cropimage/p;

.field b:Ljava/lang/Runnable;

.field private c:Lcom/sec/vip/cropimage/CropImageView;

.field private d:I

.field private f:I

.field private final g:Landroid/os/Handler;

.field private h:I

.field private i:I

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Landroid/graphics/Bitmap;

.field private p:[I

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/LinearLayout;

.field private s:F

.field private t:F

.field private u:I

.field private v:I

.field private w:Landroid/graphics/Bitmap;

.field private x:Landroid/graphics/Bitmap;

.field private y:Landroid/widget/ImageView;

.field private z:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0x7d0

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Lcom/sec/vip/cropimage/MonitoredActivity;-><init>()V

    .line 92
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->g:Landroid/os/Handler;

    .line 98
    iput-boolean v3, p0, Lcom/sec/vip/cropimage/CropImage;->k:Z

    .line 99
    iput-boolean v2, p0, Lcom/sec/vip/cropimage/CropImage;->l:Z

    .line 100
    iput-boolean v2, p0, Lcom/sec/vip/cropimage/CropImage;->m:Z

    .line 101
    iput-boolean v3, p0, Lcom/sec/vip/cropimage/CropImage;->n:Z

    .line 106
    iput-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->p:[I

    .line 107
    iput-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->q:Landroid/widget/LinearLayout;

    .line 108
    iput-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->r:Landroid/widget/LinearLayout;

    .line 113
    iput v4, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    .line 114
    iput v4, p0, Lcom/sec/vip/cropimage/CropImage;->t:F

    .line 115
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->u:I

    .line 116
    iput v5, p0, Lcom/sec/vip/cropimage/CropImage;->v:I

    .line 117
    iput-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->w:Landroid/graphics/Bitmap;

    .line 118
    iput-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->x:Landroid/graphics/Bitmap;

    .line 123
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->D:Ljava/lang/String;

    .line 124
    iput-boolean v2, p0, Lcom/sec/vip/cropimage/CropImage;->E:Z

    .line 125
    iput-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    .line 126
    iput-boolean v2, p0, Lcom/sec/vip/cropimage/CropImage;->G:Z

    .line 128
    iput v5, p0, Lcom/sec/vip/cropimage/CropImage;->H:I

    .line 129
    iput-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->I:Landroid/net/Uri;

    .line 130
    iput v3, p0, Lcom/sec/vip/cropimage/CropImage;->J:I

    .line 137
    iput v2, p0, Lcom/sec/vip/cropimage/CropImage;->K:I

    .line 138
    iput v2, p0, Lcom/sec/vip/cropimage/CropImage;->L:I

    .line 139
    iput-boolean v3, p0, Lcom/sec/vip/cropimage/CropImage;->M:Z

    .line 758
    new-instance v0, Lcom/sec/vip/cropimage/f;

    invoke-direct {v0, p0}, Lcom/sec/vip/cropimage/f;-><init>(Lcom/sec/vip/cropimage/CropImage;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->b:Ljava/lang/Runnable;

    .line 1163
    new-instance v0, Lcom/sec/vip/cropimage/h;

    invoke-direct {v0, p0}, Lcom/sec/vip/cropimage/h;-><init>(Lcom/sec/vip/cropimage/CropImage;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->O:Landroid/view/View$OnClickListener;

    .line 1479
    new-instance v0, Lcom/sec/vip/cropimage/j;

    invoke-direct {v0, p0}, Lcom/sec/vip/cropimage/j;-><init>(Lcom/sec/vip/cropimage/CropImage;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->P:Landroid/view/View$OnClickListener;

    .line 1491
    new-instance v0, Lcom/sec/vip/cropimage/k;

    invoke-direct {v0, p0}, Lcom/sec/vip/cropimage/k;-><init>(Lcom/sec/vip/cropimage/CropImage;)V

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->Q:Landroid/os/Handler;

    .line 1571
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 520
    packed-switch p1, :pswitch_data_0

    .line 528
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 522
    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 524
    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    .line 526
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 520
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/vip/cropimage/CropImage;I)I
    .locals 0

    .prologue
    .line 86
    iput p1, p0, Lcom/sec/vip/cropimage/CropImage;->H:I

    return p1
.end method

.method private static a(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZZ)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const v7, 0x3f666666    # 0.9f

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1217
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int v3, v2, p2

    .line 1218
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v4, v2, p3

    .line 1219
    if-nez p4, :cond_3

    if-ltz v3, :cond_0

    if-gez v4, :cond_3

    .line 1226
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1227
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1229
    div-int/lit8 v3, v3, 0x2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1230
    div-int/lit8 v4, v4, 0x2

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1231
    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-static {p2, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-static {p3, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    add-int/2addr v7, v1

    invoke-direct {v4, v3, v1, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1232
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int v1, p2, v1

    div-int/lit8 v1, v1, 0x2

    .line 1233
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, p3, v3

    div-int/lit8 v3, v3, 0x2

    .line 1234
    new-instance v6, Landroid/graphics/Rect;

    sub-int v7, p2, v1

    sub-int v8, p3, v3

    invoke-direct {v6, v1, v3, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1235
    invoke-virtual {v5, p1, v4, v6, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1236
    if-eqz p5, :cond_1

    .line 1237
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    move-object v0, v2

    .line 1286
    :cond_2
    :goto_0
    return-object v0

    .line 1241
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    .line 1242
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    .line 1244
    div-float v4, v2, v3

    .line 1245
    int-to-float v5, p2

    int-to-float v6, p3

    div-float/2addr v5, v6

    .line 1247
    cmpl-float v4, v4, v5

    if-lez v4, :cond_8

    .line 1248
    int-to-float v2, p3

    div-float/2addr v2, v3

    .line 1249
    cmpg-float v3, v2, v7

    if-ltz v3, :cond_4

    cmpl-float v3, v2, v8

    if-lez v3, :cond_7

    .line 1250
    :cond_4
    invoke-virtual {p0, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    :goto_1
    move-object v5, p0

    .line 1264
    :goto_2
    if-eqz v5, :cond_b

    .line 1266
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v2, v0

    .line 1271
    :goto_3
    if-eqz p5, :cond_5

    if-eq v2, p1, :cond_5

    .line 1272
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1275
    :cond_5
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int/2addr v0, p2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1276
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v3, p3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1278
    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v1, v1, 0x2

    invoke-static {v2, v0, v1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1280
    if-eq v0, v2, :cond_2

    .line 1281
    if-nez p5, :cond_6

    if-eq v2, p1, :cond_2

    .line 1282
    :cond_6
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_7
    move-object p0, v0

    .line 1252
    goto :goto_1

    .line 1255
    :cond_8
    int-to-float v3, p2

    div-float v2, v3, v2

    .line 1256
    cmpg-float v3, v2, v7

    if-ltz v3, :cond_9

    cmpl-float v3, v2, v8

    if-lez v3, :cond_a

    .line 1257
    :cond_9
    invoke-virtual {p0, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    move-object v5, p0

    goto :goto_2

    :cond_a
    move-object v5, v0

    .line 1259
    goto :goto_2

    :cond_b
    move-object v2, p1

    .line 1268
    goto :goto_3
.end method

.method static synthetic a(Lcom/sec/vip/cropimage/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/vip/cropimage/CropImage;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/cropimage/CropImage;->b(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 539
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 544
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    .line 545
    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/cropimage/CropImage;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 550
    :goto_0
    if-nez v0, :cond_1

    .line 551
    const-string v0, "[getRealPathFromURI] cursor is null! - Camera"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    :goto_1
    return-object v3

    .line 547
    :cond_0
    new-instance v4, Landroid/content/CursorLoader;

    move-object v5, p0

    move-object v6, p1

    move-object v7, v2

    move-object v8, v3

    move-object v9, v3

    move-object v10, v3

    invoke-direct/range {v4 .. v10}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    invoke-virtual {v4}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 554
    :cond_1
    const-string v1, "_data"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 555
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 556
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 557
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private a(Landroid/graphics/Bitmap;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 1297
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->n:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x7d6

    if-eq p2, v0, :cond_0

    const/16 v0, 0x7d7

    if-ne p2, v0, :cond_1

    .line 1298
    :cond_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1299
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 1300
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1303
    packed-switch p2, :pswitch_data_0

    move-object v0, v1

    .line 1311
    :goto_0
    if-eqz v0, :cond_1

    .line 1312
    new-instance v4, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    invoke-direct {v4, v7, v7, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v2, v0, v1, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1315
    :cond_1
    return-void

    .line 1305
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f02041f

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1308
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f020422

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1303
    nop

    :pswitch_data_0
    .packed-switch 0x7d6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/vip/cropimage/CropImage;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/vip/cropimage/CropImage;->e()V

    return-void
.end method

.method private static a(Lcom/sec/vip/cropimage/MonitoredActivity;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 748
    const/4 v0, 0x0

    .line 749
    if-eqz p1, :cond_0

    .line 750
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p2, p3, v0, v1}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    .line 752
    :cond_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/vip/cropimage/m;

    invoke-direct {v2, p0, p4, v0, p5}, Lcom/sec/vip/cropimage/m;-><init>(Lcom/sec/vip/cropimage/MonitoredActivity;Ljava/lang/Runnable;Landroid/app/ProgressDialog;Landroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 753
    return-void
.end method

.method private a(Ljava/io/FileOutputStream;)V
    .locals 1

    .prologue
    .line 1045
    if-eqz p1, :cond_0

    .line 1046
    :try_start_0
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1051
    :cond_0
    :goto_0
    return-void

    .line 1048
    :catch_0
    move-exception v0

    .line 1049
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 8

    .prologue
    const v7, 0x7f070244

    const/16 v6, 0xc

    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 460
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->q:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 476
    :goto_0
    return-void

    .line 463
    :cond_0
    if-eqz p1, :cond_1

    .line 464
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090150

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 465
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 466
    const v1, 0x7f070256

    invoke-virtual {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 467
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 468
    invoke-virtual {p0, v7}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 470
    :cond_1
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09014e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 471
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 472
    const v1, 0x7f070256

    invoke-virtual {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 473
    invoke-virtual {p0, v7}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 474
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/vip/cropimage/CropImage;Z)Z
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/vip/cropimage/CropImage;->E:Z

    return p1
.end method

.method private a([ILandroid/graphics/Bitmap;)Z
    .locals 11

    .prologue
    .line 1319
    const/4 v0, 0x0

    .line 1320
    const/4 v7, 0x0

    .line 1323
    :try_start_0
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v2, v1

    .line 1324
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v3, v1

    .line 1326
    cmpl-float v1, v2, v3

    if-lez v1, :cond_1

    .line 1327
    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v1, v4

    .line 1331
    :goto_0
    mul-float v4, v2, v1

    .line 1332
    mul-float/2addr v3, v1

    .line 1333
    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v1, v4

    float-to-int v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v2, v2

    const/4 v5, 0x1

    invoke-static {p2, v1, v2, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1338
    const/4 v1, 0x0

    .line 1339
    const/4 v2, 0x0

    .line 1340
    cmpl-float v5, v4, v3

    if-lez v5, :cond_2

    .line 1342
    sub-float v1, v4, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v1, v4

    float-to-int v1, v1

    move v4, v3

    .line 1350
    :goto_1
    :try_start_1
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1351
    iget v6, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1352
    const/high16 v6, 0x3f000000    # 0.5f

    add-float/2addr v3, v6

    float-to-int v3, v3

    const/high16 v6, 0x3f000000    # 0.5f

    add-float/2addr v4, v6

    float-to-int v4, v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 1357
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    if-eq v0, p2, :cond_0

    .line 1358
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    .line 1371
    :cond_0
    if-nez v5, :cond_4

    .line 1372
    const/4 v0, 0x0

    .line 1451
    :goto_2
    return v0

    .line 1329
    :cond_1
    const/high16 v1, 0x42c80000    # 100.0f

    :try_start_3
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0

    move-result v4

    int-to-float v4, v4

    div-float/2addr v1, v4

    goto :goto_0

    .line 1345
    :cond_2
    sub-float v2, v3, v4

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move v3, v4

    goto :goto_1

    .line 1361
    :catch_0
    move-exception v1

    move-object v1, v0

    move-object v0, v7

    .line 1362
    :goto_3
    const v2, 0x7f0b0149

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1365
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_3

    if-eqz v1, :cond_3

    if-eq v0, v1, :cond_3

    .line 1366
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1369
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 1376
    :cond_4
    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    .line 1377
    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1378
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    .line 1380
    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    .line 1381
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 1382
    const/4 v1, 0x0

    :goto_4
    array-length v3, p1

    if-ge v1, v3, :cond_9

    .line 1383
    aget v3, p1, v1

    .line 1384
    add-int/lit16 v4, v3, -0x7d0

    .line 1385
    if-ltz v4, :cond_6

    array-length v6, v2

    if-lt v4, v6, :cond_8

    .line 1388
    :cond_6
    if-eqz v5, :cond_7

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz v0, :cond_7

    if-eq v5, v0, :cond_7

    .line 1389
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 1392
    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    .line 1394
    :cond_8
    aget-object v4, v2, v4

    .line 1395
    iget-object v6, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/vip/cropimage/o;

    invoke-direct {v7, p0, v3, v4}, Lcom/sec/vip/cropimage/o;-><init>(Lcom/sec/vip/cropimage/CropImage;ILjava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1382
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1399
    :cond_9
    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 1400
    iget-object v2, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/vip/cropimage/o;

    .line 1401
    const v3, 0x7f030011

    iget-object v4, p0, Lcom/sec/vip/cropimage/CropImage;->r:Landroid/widget/LinearLayout;

    const/4 v7, 0x0

    invoke-virtual {v1, v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 1402
    const v3, 0x7f070061

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 1403
    const v4, 0x7f070062

    invoke-virtual {v7, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1405
    iget v8, v2, Lcom/sec/vip/cropimage/o;->c:I

    invoke-virtual {v7, v8}, Landroid/view/View;->setId(I)V

    .line 1406
    iget-object v8, p0, Lcom/sec/vip/cropimage/CropImage;->P:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1407
    iget-object v8, v2, Lcom/sec/vip/cropimage/o;->d:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1409
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v9, 0x1

    invoke-virtual {v5, v8, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1410
    iget v9, v2, Lcom/sec/vip/cropimage/o;->c:I

    .line 1411
    const/16 v10, 0x7d6

    if-eq v9, v10, :cond_a

    const/16 v10, 0x7d7

    if-ne v9, v10, :cond_d

    .line 1413
    :cond_a
    invoke-direct {p0, v8, v9}, Lcom/sec/vip/cropimage/CropImage;->a(Landroid/graphics/Bitmap;I)V

    .line 1427
    :cond_b
    if-eqz v8, :cond_c

    .line 1428
    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1431
    :cond_c
    iput-object v3, v2, Lcom/sec/vip/cropimage/o;->a:Landroid/widget/ImageView;

    .line 1432
    iput-object v4, v2, Lcom/sec/vip/cropimage/o;->b:Landroid/widget/TextView;

    .line 1433
    new-instance v2, Lcom/sec/vip/cropimage/i;

    invoke-direct {v2, p0, v7}, Lcom/sec/vip/cropimage/i;-><init>(Lcom/sec/vip/cropimage/CropImage;Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lcom/sec/vip/cropimage/CropImage;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_5

    .line 1416
    :cond_d
    invoke-static {v8, v9}, Lcom/sec/vip/imagefilter/a;->a(Landroid/graphics/Bitmap;I)Z

    move-result v9

    if-nez v9, :cond_b

    .line 1417
    const v1, 0x7f0b0149

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1420
    if-eqz v5, :cond_e

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_e

    if-eqz v0, :cond_e

    if-eq v5, v0, :cond_e

    .line 1421
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 1424
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1446
    :cond_f
    if-eqz v5, :cond_10

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_10

    if-eqz v0, :cond_10

    if-eq v5, v0, :cond_10

    .line 1447
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 1451
    :cond_10
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 1361
    :catch_1
    move-exception v1

    move-object v1, v0

    move-object v0, v7

    goto/16 :goto_3

    :catch_2
    move-exception v1

    move-object v1, v0

    move-object v0, v5

    goto/16 :goto_3
.end method

.method private b(Landroid/net/Uri;)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/16 v4, 0x18

    const/4 v0, -0x1

    .line 570
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 573
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 574
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 575
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 576
    const/4 v3, 0x0

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 577
    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 578
    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 580
    if-lt v3, v4, :cond_0

    if-ge v1, v4, :cond_3

    .line 588
    :cond_0
    if-eqz v2, :cond_1

    .line 590
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 608
    :cond_1
    :goto_0
    return v0

    .line 584
    :catch_0
    move-exception v1

    .line 585
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 588
    if-eqz v2, :cond_1

    .line 590
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 591
    :catch_1
    move-exception v1

    goto :goto_0

    .line 588
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_2

    .line 590
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 588
    :cond_2
    :goto_1
    throw v0

    :cond_3
    if-eqz v2, :cond_4

    .line 590
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 596
    :cond_4
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "options width height : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    mul-int v0, v3, v1

    int-to-float v1, v0

    .line 601
    const/high16 v0, 0x49960000    # 1228800.0f

    div-float v0, v1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 602
    const v2, 0x49b71b00    # 1500000.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 603
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 591
    :catch_2
    move-exception v1

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_2
.end method

.method static synthetic b(Lcom/sec/vip/cropimage/CropImage;I)I
    .locals 0

    .prologue
    .line 86
    iput p1, p0, Lcom/sec/vip/cropimage/CropImage;->u:I

    return p1
.end method

.method private declared-synchronized b(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1502
    monitor-enter p0

    .line 1504
    :try_start_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1505
    invoke-static {v1, p2}, Lcom/sec/vip/imagefilter/a;->a(Landroid/graphics/Bitmap;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1508
    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->Q:Landroid/os/Handler;

    const v2, 0x7f0b0149

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1519
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1512
    :catch_0
    move-exception v1

    .line 1515
    :try_start_1
    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->Q:Landroid/os/Handler;

    const v2, 0x7f0b0149

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1502
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move-object v0, v1

    .line 1519
    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/vip/cropimage/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/vip/cropimage/CropImage;->x:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 1084
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    .line 1085
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->t:F

    .line 1086
    return-void
.end method

.method static synthetic b(Lcom/sec/vip/cropimage/CropImage;Z)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/sec/vip/cropimage/CropImage;->a(Z)V

    return-void
.end method

.method private b(Ljava/io/FileOutputStream;)V
    .locals 1

    .prologue
    .line 1055
    if-eqz p1, :cond_0

    .line 1056
    :try_start_0
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1061
    :cond_0
    :goto_0
    return-void

    .line 1058
    :catch_0
    move-exception v0

    .line 1059
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 6

    .prologue
    .line 616
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653
    :goto_0
    return-void

    .line 622
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->c:Lcom/sec/vip/cropimage/CropImageView;

    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    float-to-int v2, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/vip/cropimage/CropImageView;->setImageBitmapResetBase(Landroid/graphics/Bitmap;IZ)V

    .line 626
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/vip/cropimage/d;

    invoke-direct {v4, p0}, Lcom/sec/vip/cropimage/d;-><init>(Lcom/sec/vip/cropimage/CropImage;)V

    iget-object v5, p0, Lcom/sec/vip/cropimage/CropImage;->g:Landroid/os/Handler;

    move-object v0, p0

    move v1, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/vip/cropimage/CropImage;->a(Lcom/sec/vip/cropimage/MonitoredActivity;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/vip/cropimage/CropImage;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->G:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/vip/cropimage/CropImage;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/vip/cropimage/CropImage;->w:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private c(I)V
    .locals 7

    .prologue
    const/4 v0, 0x5

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1112
    new-array v4, v0, [I

    fill-array-data v4, :array_0

    .line 1113
    new-array v5, v0, [Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->y:Landroid/widget/ImageView;

    aput-object v0, v5, v1

    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->z:Landroid/widget/ImageView;

    aput-object v0, v5, v3

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/sec/vip/cropimage/CropImage;->A:Landroid/widget/ImageView;

    aput-object v2, v5, v0

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/sec/vip/cropimage/CropImage;->B:Landroid/widget/ImageView;

    aput-object v2, v5, v0

    const/4 v0, 0x4

    iget-object v2, p0, Lcom/sec/vip/cropimage/CropImage;->C:Landroid/widget/ImageView;

    aput-object v2, v5, v0

    move v0, v1

    .line 1114
    :goto_0
    array-length v2, v4

    if-ge v0, v2, :cond_1

    .line 1115
    aget-object v6, v5, v0

    aget v2, v4, v0

    if-ne v2, p1, :cond_0

    move v2, v3

    :goto_1
    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1114
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 1115
    goto :goto_1

    .line 1117
    :cond_1
    return-void

    .line 1112
    nop

    :array_0
    .array-data 4
        0x3e8
        0x3ea
        0x3e9
        0x3ec
        0x3eb
    .end array-data
.end method

.method static synthetic c(Lcom/sec/vip/cropimage/CropImage;I)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/sec/vip/cropimage/CropImage;->c(I)V

    return-void
.end method

.method static synthetic c(Lcom/sec/vip/cropimage/CropImage;)Z
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/vip/cropimage/CropImage;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/sec/vip/cropimage/CropImage;Z)Z
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/vip/cropimage/CropImage;->G:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/vip/cropimage/CropImage;)I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->v:I

    return v0
.end method

.method static synthetic d(Lcom/sec/vip/cropimage/CropImage;I)I
    .locals 0

    .prologue
    .line 86
    iput p1, p0, Lcom/sec/vip/cropimage/CropImage;->v:I

    return p1
.end method

.method private d()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v13, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 849
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->N:Z

    if-eqz v0, :cond_1

    .line 1041
    :cond_0
    :goto_0
    return-void

    .line 852
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->a:Lcom/sec/vip/cropimage/p;

    if-eqz v0, :cond_0

    .line 855
    iput-boolean v5, p0, Lcom/sec/vip/cropimage/CropImage;->N:Z

    .line 858
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->n:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->w:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 859
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->w:Landroid/graphics/Bitmap;

    move-object v2, v0

    .line 863
    :goto_1
    if-nez v2, :cond_3

    .line 864
    const-string v0, "[onSaveClicked] bmpSrc is NULL!!!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 861
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    move-object v2, v0

    goto :goto_1

    .line 868
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->a:Lcom/sec/vip/cropimage/p;

    invoke-virtual {v0}, Lcom/sec/vip/cropimage/p;->b()Landroid/graphics/Rect;

    move-result-object v0

    .line 869
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 870
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    .line 876
    iget-boolean v1, p0, Lcom/sec/vip/cropimage/CropImage;->l:Z

    if-nez v1, :cond_b

    iget v1, p0, Lcom/sec/vip/cropimage/CropImage;->u:I

    const/16 v6, 0x3e8

    if-ne v1, v6, :cond_b

    .line 877
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 882
    :goto_2
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 883
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8, v7, v7, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 884
    invoke-virtual {v6, v2, v0, v8, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 887
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->l:Z

    if-eqz v0, :cond_4

    .line 890
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 891
    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    .line 892
    int-to-float v8, v3

    div-float/2addr v8, v9

    int-to-float v4, v4

    div-float/2addr v4, v9

    int-to-float v3, v3

    div-float/2addr v3, v9

    sget-object v9, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v6, v8, v4, v3, v9}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 893
    sget-object v3, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {v0, v6, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 894
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v7, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 898
    :cond_4
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->h:I

    if-eqz v0, :cond_f

    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->i:I

    if-eqz v0, :cond_f

    .line 899
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->j:Z

    if-eqz v0, :cond_e

    .line 902
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iget v2, p0, Lcom/sec/vip/cropimage/CropImage;->h:I

    iget v3, p0, Lcom/sec/vip/cropimage/CropImage;->i:I

    iget-boolean v4, p0, Lcom/sec/vip/cropimage/CropImage;->k:Z

    invoke-static/range {v0 .. v5}, Lcom/sec/vip/cropimage/CropImage;->a(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 905
    new-instance v11, Landroid/graphics/Matrix;

    invoke-direct {v11}, Landroid/graphics/Matrix;-><init>()V

    .line 906
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    invoke-virtual {v11, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 907
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    move v8, v7

    move v12, v5

    invoke-static/range {v6 .. v12}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 910
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->u:I

    const/16 v3, 0x3e8

    if-eq v0, v3, :cond_c

    .line 911
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 912
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 913
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 914
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v6}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 916
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 917
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v6, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 919
    iget v6, p0, Lcom/sec/vip/cropimage/CropImage;->u:I

    packed-switch v6, :pswitch_data_0

    move-object v0, v13

    .line 934
    :goto_3
    new-instance v6, Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {v6, v14, v14, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v3, v0, v13, v6, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 939
    :cond_5
    :goto_4
    if-eq v1, v2, :cond_6

    .line 940
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_6
    move-object v0, v2

    .line 989
    :cond_7
    :goto_5
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 990
    if-eqz v1, :cond_13

    const-string v2, "data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_8

    const-string v2, "return-data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 991
    :cond_8
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 994
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/2addr v1, v3

    .line 995
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bitmap byte size : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    const v3, 0x186a0

    if-le v1, v3, :cond_12

    .line 998
    const-string v1, "exceed_size"

    invoke-virtual {v2, v1, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1001
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/tmpCropImage.png"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1002
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1006
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1007
    :try_start_1
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x64

    invoke-virtual {v0, v6, v8, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1013
    invoke-direct {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->a(Ljava/io/FileOutputStream;)V

    .line 1014
    invoke-direct {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->b(Ljava/io/FileOutputStream;)V

    .line 1017
    :goto_6
    if-nez v5, :cond_9

    .line 1018
    invoke-virtual {p0, v7, v13}, Lcom/sec/vip/cropimage/CropImage;->setResult(ILandroid/content/Intent;)V

    .line 1019
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->finish()V

    .line 1021
    :cond_9
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_a

    .line 1022
    invoke-virtual {p0, v7, v13}, Lcom/sec/vip/cropimage/CropImage;->setResult(ILandroid/content/Intent;)V

    .line 1023
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->finish()V

    .line 1026
    :cond_a
    const-string v0, "temp_file_path"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1034
    :goto_7
    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1035
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/vip/cropimage/CropImage;->setResult(ILandroid/content/Intent;)V

    .line 1036
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->finish()V

    goto/16 :goto_0

    .line 879
    :cond_b
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_2

    .line 921
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f02041d

    invoke-static {v6, v8, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_3

    .line 924
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f02041b

    invoke-static {v6, v8, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_3

    .line 927
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f02041e

    invoke-static {v6, v8, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_3

    .line 930
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f02041c

    invoke-static {v6, v8, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_3

    .line 935
    :cond_c
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->v:I

    const/16 v3, 0x7d6

    if-eq v0, v3, :cond_d

    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->v:I

    const/16 v3, 0x7d7

    if-ne v0, v3, :cond_5

    .line 936
    :cond_d
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->v:I

    invoke-direct {p0, v2, v0}, Lcom/sec/vip/cropimage/CropImage;->a(Landroid/graphics/Bitmap;I)V

    goto/16 :goto_4

    .line 947
    :cond_e
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->h:I

    iget v3, p0, Lcom/sec/vip/cropimage/CropImage;->i:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 948
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 950
    iget-object v4, p0, Lcom/sec/vip/cropimage/CropImage;->a:Lcom/sec/vip/cropimage/p;

    invoke-virtual {v4}, Lcom/sec/vip/cropimage/p;->b()Landroid/graphics/Rect;

    move-result-object v4

    .line 951
    new-instance v6, Landroid/graphics/Rect;

    iget v8, p0, Lcom/sec/vip/cropimage/CropImage;->h:I

    iget v9, p0, Lcom/sec/vip/cropimage/CropImage;->i:I

    invoke-direct {v6, v7, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 953
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    .line 954
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v10

    sub-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    .line 957
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v10

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-virtual {v4, v10, v11}, Landroid/graphics/Rect;->inset(II)V

    .line 960
    neg-int v8, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    neg-int v9, v9

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-virtual {v6, v8, v9}, Landroid/graphics/Rect;->inset(II)V

    .line 963
    invoke-virtual {v3, v2, v4, v6, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 966
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_5

    .line 974
    :cond_f
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v1, v0, v5}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 975
    new-instance v11, Landroid/graphics/Matrix;

    invoke-direct {v11}, Landroid/graphics/Matrix;-><init>()V

    .line 976
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    invoke-virtual {v11, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 977
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    move v8, v7

    move v12, v5

    invoke-static/range {v6 .. v12}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 978
    if-eq v1, v0, :cond_10

    .line 979
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 981
    :cond_10
    iget-boolean v1, p0, Lcom/sec/vip/cropimage/CropImage;->n:Z

    if-eqz v1, :cond_7

    iget v1, p0, Lcom/sec/vip/cropimage/CropImage;->v:I

    const/16 v2, 0x7d6

    if-eq v1, v2, :cond_11

    iget v1, p0, Lcom/sec/vip/cropimage/CropImage;->v:I

    const/16 v2, 0x7d7

    if-ne v1, v2, :cond_7

    .line 982
    :cond_11
    iget v1, p0, Lcom/sec/vip/cropimage/CropImage;->v:I

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/cropimage/CropImage;->a(Landroid/graphics/Bitmap;I)V

    goto/16 :goto_5

    .line 1008
    :catch_0
    move-exception v0

    move-object v1, v13

    .line 1009
    :goto_8
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1010
    const-string v0, "Temporary file saving failed!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1013
    invoke-direct {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->a(Ljava/io/FileOutputStream;)V

    .line 1014
    invoke-direct {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->b(Ljava/io/FileOutputStream;)V

    move v5, v7

    .line 1015
    goto/16 :goto_6

    .line 1013
    :catchall_0
    move-exception v0

    move-object v1, v13

    :goto_9
    invoke-direct {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->a(Ljava/io/FileOutputStream;)V

    .line 1014
    invoke-direct {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->b(Ljava/io/FileOutputStream;)V

    .line 1013
    throw v0

    .line 1029
    :cond_12
    const-string v1, "exceed_size"

    invoke-virtual {v2, v1, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1030
    const-string v1, "data"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1031
    new-instance v0, Landroid/content/Intent;

    const-string v1, "inline-data"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1039
    :cond_13
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->finish()V

    goto/16 :goto_0

    .line 1013
    :catchall_1
    move-exception v0

    goto :goto_9

    .line 1008
    :catch_1
    move-exception v0

    goto :goto_8

    .line 919
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic d(Lcom/sec/vip/cropimage/CropImage;Z)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/sec/vip/cropimage/CropImage;->b(Z)V

    return-void
.end method

.method static synthetic e(Lcom/sec/vip/cropimage/CropImage;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->r:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/high16 v2, 0x43b40000    # 360.0f

    .line 1069
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    const/high16 v1, 0x43870000    # 270.0f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    .line 1070
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    rem-float/2addr v0, v2

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    .line 1072
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    .line 1073
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    .line 1075
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->b(Z)V

    .line 1076
    return-void
.end method

.method static synthetic e(Lcom/sec/vip/cropimage/CropImage;Z)Z
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/vip/cropimage/CropImage;->M:Z

    return p1
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1092
    const v0, 0x7f070250

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->y:Landroid/widget/ImageView;

    .line 1093
    const v0, 0x7f070252

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->z:Landroid/widget/ImageView;

    .line 1094
    const v0, 0x7f070251

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->A:Landroid/widget/ImageView;

    .line 1095
    const v0, 0x7f070254

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->B:Landroid/widget/ImageView;

    .line 1096
    const v0, 0x7f070253

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->C:Landroid/widget/ImageView;

    .line 1097
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->y:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->O:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1099
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->z:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->O:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1101
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->A:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->O:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1103
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->B:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->O:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1105
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->C:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->O:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1108
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->u:I

    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->c(I)V

    .line 1109
    return-void
.end method

.method static synthetic f(Lcom/sec/vip/cropimage/CropImage;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/vip/cropimage/CropImage;->g()V

    return-void
.end method

.method static synthetic g(Lcom/sec/vip/cropimage/CropImage;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->r:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->p:[I

    if-nez v0, :cond_1

    .line 1294
    :cond_0
    :goto_0
    return-void

    .line 1293
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->p:[I

    iget-object v1, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/cropimage/CropImage;->a([ILandroid/graphics/Bitmap;)Z

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/vip/cropimage/CropImage;)F
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    return v0
.end method

.method private h()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1456
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1474
    :cond_0
    :goto_0
    return v1

    .line 1459
    :cond_1
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1460
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    iget v2, p0, Lcom/sec/vip/cropimage/CropImage;->t:F

    sub-float/2addr v0, v2

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1461
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->t:F

    .line 1462
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/sec/vip/cropimage/o;

    .line 1463
    iget-object v0, v7, Lcom/sec/vip/cropimage/o;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 1464
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1465
    if-eqz v0, :cond_0

    .line 1468
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1469
    iget-object v3, v7, Lcom/sec/vip/cropimage/o;->a:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1470
    if-eq v2, v0, :cond_2

    .line 1471
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    :cond_3
    move v1, v6

    .line 1474
    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/vip/cropimage/CropImage;)Lcom/sec/vip/cropimage/CropImageView;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->c:Lcom/sec/vip/cropimage/CropImageView;

    return-object v0
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1565
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/o;

    .line 1566
    iget-object v2, v0, Lcom/sec/vip/cropimage/o;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1567
    iget-object v0, v0, Lcom/sec/vip/cropimage/o;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_0

    .line 1569
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/vip/cropimage/CropImage;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->g:Landroid/os/Handler;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1584
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1585
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1587
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->w:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->w:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1588
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->w:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1590
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->x:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->x:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1591
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->x:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1594
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 1595
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/o;

    .line 1596
    iget-object v0, v0, Lcom/sec/vip/cropimage/o;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 1597
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1598
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 1603
    :cond_4
    return-void
.end method

.method static synthetic k(Lcom/sec/vip/cropimage/CropImage;)I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->d:I

    return v0
.end method

.method static synthetic l(Lcom/sec/vip/cropimage/CropImage;)I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->f:I

    return v0
.end method

.method static synthetic m(Lcom/sec/vip/cropimage/CropImage;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->l:Z

    return v0
.end method

.method static synthetic n(Lcom/sec/vip/cropimage/CropImage;)I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->u:I

    return v0
.end method

.method static synthetic o(Lcom/sec/vip/cropimage/CropImage;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->w:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/vip/cropimage/CropImage;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->x:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/vip/cropimage/CropImage;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->M:Z

    return v0
.end method

.method static synthetic r(Lcom/sec/vip/cropimage/CropImage;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/vip/cropimage/CropImage;->i()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->K:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 501
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->L:I

    return v0
.end method

.method public c()F
    .locals 1

    .prologue
    .line 510
    iget v0, p0, Lcom/sec/vip/cropimage/CropImage;->s:F

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 145
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/MonitoredActivity;->onCreate(Landroid/os/Bundle;)V

    .line 148
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/sec/vip/cropimage/CropImage;->K:I

    .line 150
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->L:I

    .line 152
    iput-boolean v8, p0, Lcom/sec/vip/cropimage/CropImage;->M:Z

    .line 155
    const v0, 0x7f030059

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->setContentView(I)V

    .line 157
    const v0, 0x7f07025c

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/cropimage/CropImageView;

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->c:Lcom/sec/vip/cropimage/CropImageView;

    .line 159
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v0, v4, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->c:Lcom/sec/vip/cropimage/CropImageView;

    invoke-virtual {v0, v8, v3}, Lcom/sec/vip/cropimage/CropImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->c:Lcom/sec/vip/cropimage/CropImageView;

    iput-object p0, v0, Lcom/sec/vip/cropimage/CropImageView;->a:Landroid/content/Context;

    .line 164
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 165
    if-eqz v4, :cond_3

    .line 166
    const-string v0, "circleCrop"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    iput-boolean v8, p0, Lcom/sec/vip/cropimage/CropImage;->l:Z

    .line 168
    iput v8, p0, Lcom/sec/vip/cropimage/CropImage;->d:I

    .line 169
    iput v8, p0, Lcom/sec/vip/cropimage/CropImage;->f:I

    .line 171
    :cond_1
    const-string v0, "data"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    .line 172
    const-string v0, "aspectX"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->d:I

    .line 173
    const-string v0, "aspectY"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->f:I

    .line 174
    const-string v0, "outputX"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->h:I

    .line 175
    const-string v0, "outputY"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/cropimage/CropImage;->i:I

    .line 176
    const-string v0, "template"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->m:Z

    .line 177
    const-string v0, "effect"

    invoke-virtual {v4, v0, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->n:Z

    .line 178
    const-string v0, "effectFilterList"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->p:[I

    .line 180
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-gt v0, v5, :cond_c

    .line 181
    const-string v0, "titleText"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->D:Ljava/lang/String;

    .line 182
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->D:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 183
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0b01af

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->D:Ljava/lang/String;

    .line 188
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->D:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->setTitle(Ljava/lang/CharSequence;)V

    .line 189
    const-string v0, "scale"

    invoke-virtual {v4, v0, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->j:Z

    .line 190
    const-string v0, "scaleUpIfNeeded"

    invoke-virtual {v4, v0, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->k:Z

    .line 193
    :cond_3
    const v0, 0x7f070259

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->q:Landroid/widget/LinearLayout;

    .line 194
    const v0, 0x7f07025b

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->r:Landroid/widget/LinearLayout;

    .line 195
    invoke-direct {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->a(Z)V

    .line 197
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    if-nez v0, :cond_8

    .line 200
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->I:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 204
    :try_start_1
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->I:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->a(Landroid/net/Uri;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 211
    :goto_1
    :try_start_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 213
    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 215
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 216
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_5

    .line 217
    :cond_4
    const v4, 0x7f0b015e

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 218
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->finish()V

    .line 224
    :cond_5
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 225
    iget-object v5, p0, Lcom/sec/vip/cropimage/CropImage;->I:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v3

    .line 229
    :try_start_3
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 231
    iget-object v5, p0, Lcom/sec/vip/cropimage/CropImage;->I:Landroid/net/Uri;

    invoke-direct {p0, v5}, Lcom/sec/vip/cropimage/CropImage;->b(Landroid/net/Uri;)I

    move-result v5

    iput v5, p0, Lcom/sec/vip/cropimage/CropImage;->J:I

    .line 232
    iget v5, p0, Lcom/sec/vip/cropimage/CropImage;->J:I

    if-gez v5, :cond_6

    .line 233
    const v5, 0x7f0b015e

    const/4 v6, 0x0

    invoke-static {p0, v5, v6}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 234
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->finish()V

    .line 236
    :cond_6
    iget v5, p0, Lcom/sec/vip/cropimage/CropImage;->J:I

    iput v5, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 237
    const/4 v5, 0x1

    iput-boolean v5, v4, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 239
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "options.inSampleSize : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const/4 v5, 0x0

    invoke-static {v3, v5, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    .line 247
    if-nez v0, :cond_7

    .line 248
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->I:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 250
    :cond_7
    new-instance v4, Landroid/media/ExifInterface;

    invoke-direct {v4, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 251
    const-string v0, "Orientation"

    const/4 v5, 0x1

    invoke-virtual {v4, v0, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    .line 252
    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->a(I)I

    move-result v0

    .line 254
    invoke-direct {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->b(I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 271
    if-eqz v3, :cond_8

    .line 273
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 280
    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    if-nez v0, :cond_9

    .line 281
    const-string v0, "mBitmap is null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->finish()V

    .line 291
    :cond_9
    const v0, 0x7f070257

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 292
    new-instance v3, Lcom/sec/vip/cropimage/a;

    invoke-direct {v3, p0}, Lcom/sec/vip/cropimage/a;-><init>(Lcom/sec/vip/cropimage/CropImage;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    const v0, 0x7f07024f

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->m:Z

    if-eqz v0, :cond_f

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 307
    invoke-direct {p0}, Lcom/sec/vip/cropimage/CropImage;->f()V

    .line 309
    const v0, 0x7f070258

    invoke-virtual {p0, v0}, Lcom/sec/vip/cropimage/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 310
    new-instance v3, Lcom/sec/vip/cropimage/b;

    invoke-direct {v3, p0}, Lcom/sec/vip/cropimage/b;-><init>(Lcom/sec/vip/cropimage/CropImage;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348
    iget-boolean v3, p0, Lcom/sec/vip/cropimage/CropImage;->n:Z

    if-eqz v3, :cond_a

    move v2, v1

    :cond_a
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 367
    invoke-direct {p0, v8}, Lcom/sec/vip/cropimage/CropImage;->b(Z)V

    .line 370
    iput-boolean v1, p0, Lcom/sec/vip/cropimage/CropImage;->G:Z

    .line 371
    iget-boolean v0, p0, Lcom/sec/vip/cropimage/CropImage;->n:Z

    if-eqz v0, :cond_b

    .line 372
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/vip/cropimage/c;

    invoke-direct {v1, p0}, Lcom/sec/vip/cropimage/c;-><init>(Lcom/sec/vip/cropimage/CropImage;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 380
    :cond_b
    return-void

    .line 186
    :cond_c
    const-string v0, "titleText"

    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b01af

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/cropimage/CropImage;->D:Ljava/lang/String;

    goto/16 :goto_0

    .line 205
    :catch_0
    move-exception v0

    .line 206
    :try_start_5
    const-string v0, "Permission error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const v0, 0x7f0b015e

    const/4 v4, 0x0

    invoke-static {p0, v0, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 208
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->finish()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v0, v3

    goto/16 :goto_1

    .line 257
    :catch_1
    move-exception v0

    move-object v1, v3

    .line 258
    :goto_4
    :try_start_6
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 271
    :catchall_0
    move-exception v0

    move-object v3, v1

    :goto_5
    if-eqz v3, :cond_d

    .line 273
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 271
    :cond_d
    :goto_6
    throw v0

    .line 259
    :catch_2
    move-exception v0

    .line 261
    :try_start_8
    const-string v0, "OutOfMemory error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const v0, 0x7f0b0149

    const/4 v4, 0x0

    invoke-static {p0, v0, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 263
    if-eqz v3, :cond_e

    .line 265
    :try_start_9
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 269
    :cond_e
    :goto_7
    :try_start_a
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->finish()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 271
    if-eqz v3, :cond_8

    .line 273
    :try_start_b
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3

    goto/16 :goto_2

    .line 274
    :catch_3
    move-exception v0

    goto/16 :goto_2

    :cond_f
    move v0, v2

    .line 305
    goto/16 :goto_3

    .line 266
    :catch_4
    move-exception v0

    goto :goto_7

    .line 274
    :catch_5
    move-exception v1

    goto :goto_6

    .line 271
    :catchall_1
    move-exception v0

    goto :goto_5

    .line 257
    :catch_6
    move-exception v0

    move-object v1, v3

    goto :goto_4
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 482
    invoke-direct {p0}, Lcom/sec/vip/cropimage/CropImage;->j()V

    .line 483
    invoke-super {p0}, Lcom/sec/vip/cropimage/MonitoredActivity;->onDestroy()V

    .line 484
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 385
    invoke-super {p0}, Lcom/sec/vip/cropimage/MonitoredActivity;->onResume()V

    .line 387
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 389
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 391
    const v0, 0x7f09015d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 392
    const v2, 0x7f09015e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 394
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 395
    const/4 v0, -0x1

    .line 398
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/view/Window;->setLayout(II)V

    .line 401
    :cond_1
    return-void
.end method

.method public onSupportCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 405
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 406
    const v1, 0x7f0f0021

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 407
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/MonitoredActivity;->onSupportCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 412
    .line 414
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0705a5

    if-eq v2, v3, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x102002c

    if-ne v2, v3, :cond_5

    .line 415
    :cond_0
    iget-boolean v2, p0, Lcom/sec/vip/cropimage/CropImage;->E:Z

    if-eqz v2, :cond_4

    .line 416
    iput-boolean v1, p0, Lcom/sec/vip/cropimage/CropImage;->E:Z

    .line 417
    iget-object v2, p0, Lcom/sec/vip/cropimage/CropImage;->D:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/vip/cropimage/CropImage;->setTitle(Ljava/lang/CharSequence;)V

    .line 418
    invoke-direct {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->a(Z)V

    .line 421
    iget-object v2, p0, Lcom/sec/vip/cropimage/CropImage;->r:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_1

    .line 422
    const/4 v3, 0x0

    .line 423
    iget-object v2, p0, Lcom/sec/vip/cropimage/CropImage;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    move v4, v1

    .line 424
    :goto_0
    if-ge v4, v5, :cond_8

    .line 425
    iget-object v2, p0, Lcom/sec/vip/cropimage/CropImage;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 426
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v6

    iget v7, p0, Lcom/sec/vip/cropimage/CropImage;->H:I

    if-ne v6, v7, :cond_3

    .line 431
    :goto_1
    if-eqz v2, :cond_1

    .line 432
    iget v3, p0, Lcom/sec/vip/cropimage/CropImage;->H:I

    iput v3, p0, Lcom/sec/vip/cropimage/CropImage;->v:I

    .line 433
    new-instance v3, Lcom/sec/vip/cropimage/l;

    iget-object v4, p0, Lcom/sec/vip/cropimage/CropImage;->o:Landroid/graphics/Bitmap;

    invoke-direct {v3, p0, v2, v4}, Lcom/sec/vip/cropimage/l;-><init>(Lcom/sec/vip/cropimage/CropImage;Landroid/view/View;Landroid/graphics/Bitmap;)V

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/sec/vip/cropimage/l;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 452
    :cond_1
    :goto_2
    if-nez v0, :cond_2

    .line 454
    invoke-super {p0, p1}, Lcom/sec/vip/cropimage/MonitoredActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 456
    :cond_2
    return v0

    .line 424
    :cond_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 438
    :cond_4
    invoke-virtual {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->setResult(I)V

    .line 439
    invoke-virtual {p0}, Lcom/sec/vip/cropimage/CropImage;->finish()V

    goto :goto_2

    .line 442
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0705a6

    if-ne v2, v3, :cond_7

    .line 443
    iget-boolean v2, p0, Lcom/sec/vip/cropimage/CropImage;->E:Z

    if-eqz v2, :cond_6

    .line 444
    iput-boolean v1, p0, Lcom/sec/vip/cropimage/CropImage;->E:Z

    .line 445
    iget-object v2, p0, Lcom/sec/vip/cropimage/CropImage;->D:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/vip/cropimage/CropImage;->setTitle(Ljava/lang/CharSequence;)V

    .line 446
    invoke-direct {p0, v1}, Lcom/sec/vip/cropimage/CropImage;->a(Z)V

    goto :goto_2

    .line 448
    :cond_6
    invoke-direct {p0}, Lcom/sec/vip/cropimage/CropImage;->d()V

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    move-object v2, v3

    goto :goto_1
.end method
