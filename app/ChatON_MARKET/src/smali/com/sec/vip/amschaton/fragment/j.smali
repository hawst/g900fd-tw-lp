.class Lcom/sec/vip/amschaton/fragment/j;
.super Ljava/lang/Object;
.source "AMSBgSelectionFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/16 v4, 0x3eb

    const/16 v3, 0x3ea

    const/4 v2, 0x0

    .line 500
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_3

    .line 501
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Lcom/sec/vip/amschaton/a/d;

    move-result-object v0

    if-nez v0, :cond_1

    .line 554
    :cond_0
    :goto_0
    return-void

    .line 504
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Lcom/sec/vip/amschaton/a/d;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Lcom/sec/vip/amschaton/a/d;->b(J)Landroid/database/Cursor;

    move-result-object v0

    .line 505
    if-eqz v0, :cond_0

    .line 508
    const-string v1, "ams_index"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 509
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 510
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Landroid/widget/GridView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setEnabled(Z)V

    .line 511
    const/16 v1, 0x2710

    if-ge v0, v1, :cond_2

    .line 512
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1, v4}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 513
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 518
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    .line 519
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->j(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)V

    goto :goto_0

    .line 515
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1, v3}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 516
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    add-int/lit16 v0, v0, -0x2710

    invoke-static {v1, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    goto :goto_1

    .line 523
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    const/16 v1, 0xbba

    if-eq v0, v1, :cond_0

    .line 526
    if-nez p3, :cond_4

    .line 528
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 529
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    const/16 v1, 0x3e9

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 530
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->k(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    goto :goto_0

    .line 531
    :cond_4
    const/4 v0, 0x1

    if-ne p3, v0, :cond_5

    .line 532
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Landroid/widget/GridView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setEnabled(Z)V

    .line 533
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 534
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 535
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    .line 536
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->j(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)V

    goto/16 :goto_0

    .line 537
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    if-ge p3, v0, :cond_6

    .line 538
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Landroid/widget/GridView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setEnabled(Z)V

    .line 539
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    add-int/lit8 v1, p3, -0x2

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 540
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0, v3}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 541
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    .line 542
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->j(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)V

    goto/16 :goto_0

    .line 543
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    if-ge p3, v0, :cond_7

    .line 545
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Landroid/widget/GridView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setEnabled(Z)V

    .line 546
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    sub-int v1, p3, v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 547
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0, v4}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 548
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    .line 549
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->j(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)V

    goto/16 :goto_0

    .line 550
    :cond_7
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    if-ne p3, v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/j;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->l(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    goto/16 :goto_0
.end method
