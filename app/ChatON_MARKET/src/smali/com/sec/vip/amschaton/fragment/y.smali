.class public Lcom/sec/vip/amschaton/fragment/y;
.super Landroid/widget/CursorAdapter;
.source "AMSBgSelectionFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

.field private b:Lcom/sec/vip/amschaton/fragment/q;

.field private c:[Z

.field private d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 945
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/y;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    .line 946
    invoke-direct {p0, p2, p3}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 940
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/y;->b:Lcom/sec/vip/amschaton/fragment/q;

    .line 942
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    .line 947
    new-instance v0, Lcom/sec/vip/amschaton/fragment/q;

    invoke-direct {v0, p1}, Lcom/sec/vip/amschaton/fragment/q;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/y;->b:Lcom/sec/vip/amschaton/fragment/q;

    .line 948
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    .line 949
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/y;->a(Z)V

    .line 950
    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1025
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/y;->d:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1044
    :goto_0
    return-void

    .line 1028
    :cond_0
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 1029
    const/16 v0, 0x7d0

    iput v0, v5, Landroid/os/Message;->what:I

    .line 1032
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    if-eqz v0, :cond_2

    move v0, v1

    move v2, v3

    move v4, v1

    .line 1033
    :goto_1
    iget-object v6, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    array-length v6, v6

    if-ge v0, v6, :cond_3

    .line 1034
    iget-object v6, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    aget-boolean v6, v6, v0

    if-eqz v6, :cond_1

    move v4, v3

    .line 1033
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    .line 1037
    goto :goto_2

    :cond_2
    move v2, v3

    move v4, v1

    .line 1041
    :cond_3
    if-eqz v4, :cond_4

    move v0, v3

    :goto_3
    iput v0, v5, Landroid/os/Message;->arg1:I

    .line 1042
    if-eqz v2, :cond_5

    :goto_4
    iput v3, v5, Landroid/os/Message;->arg2:I

    .line 1043
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/y;->d:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1041
    goto :goto_3

    :cond_5
    move v3, v1

    .line 1042
    goto :goto_4
.end method


# virtual methods
.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1047
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/y;->d:Landroid/os/Handler;

    .line 1048
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 958
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    if-eqz v0, :cond_0

    .line 959
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 960
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    aput-boolean p1, v1, v0

    .line 959
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 963
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/y;->notifyDataSetChanged()V

    .line 964
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/y;->b()V

    .line 965
    return-void
.end method

.method public a()[Z
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    return-object v0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 969
    const v0, 0x7f070066

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 970
    const v1, 0x7f070067

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 971
    const v2, 0x7f07006b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 973
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 974
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 976
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/y;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v3}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v3

    const/16 v4, 0xbba

    if-ne v3, v4, :cond_0

    .line 977
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 979
    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 980
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    aget-boolean v3, v3, v4

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 981
    new-instance v3, Lcom/sec/vip/amschaton/fragment/z;

    invoke-direct {v3, p0, v1}, Lcom/sec/vip/amschaton/fragment/z;-><init>(Lcom/sec/vip/amschaton/fragment/y;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 995
    :goto_0
    const-string v0, "ams_index"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 996
    const/16 v1, 0x2710

    if-lt v0, v1, :cond_1

    .line 997
    const-string v0, "ams_path"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 998
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/y;->b:Lcom/sec/vip/amschaton/fragment/q;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "d"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/sec/vip/amschaton/fragment/q;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 1002
    :goto_1
    return-void

    .line 991
    :cond_0
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 992
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 1000
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/y;->b:Lcom/sec/vip/amschaton/fragment/q;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/sec/vip/amschaton/fragment/q;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1006
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/y;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1007
    const v1, 0x7f030015

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1008
    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 1014
    instance-of v0, p1, Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 1015
    check-cast p1, Landroid/widget/CheckBox;

    .line 1016
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1017
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/y;->c:[Z

    aput-boolean p2, v1, v0

    .line 1019
    :cond_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/y;->b()V

    .line 1021
    return-void
.end method
