.class public Lcom/sec/vip/amschaton/AMSEmoticonSelectionActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "AMSEmoticonSelectionActivity.java"

# interfaces
.implements Lcom/sec/vip/amschaton/fragment/av;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;-><init>()V

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSEmoticonSelectionActivity;->finish()V

    .line 59
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 27
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 30
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 34
    const v0, 0x7f09015d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 35
    const v2, 0x7f09015e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 37
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 38
    const/4 v0, -0x1

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSEmoticonSelectionActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/view/Window;->setLayout(II)V

    .line 44
    :cond_1
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 49
    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSEmoticonSelectionActivity;->finish()V

    .line 52
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
