.class Lcom/sec/vip/amschaton/fragment/dr;
.super Ljava/lang/Object;
.source "AMSViewerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V
    .locals 0

    .prologue
    .line 823
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 831
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-nez v0, :cond_1

    .line 832
    const-string v0, "mViewer is null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    :cond_0
    :goto_0
    return-void

    .line 835
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    iget-boolean v0, v0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->N:Z

    if-eqz v0, :cond_5

    .line 837
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->g()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    .line 839
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->B:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 842
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)V

    .line 844
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)V

    .line 845
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->e()V

    goto :goto_0

    .line 849
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->B:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->A:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 852
    :cond_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)V

    .line 854
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b(Z)V

    .line 855
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    goto :goto_0

    .line 860
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->g()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_6

    .line 866
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)V

    .line 868
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)V

    .line 869
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->e()V

    goto :goto_0

    .line 872
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->B:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->A:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 874
    :cond_7
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b(Z)V

    .line 875
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 876
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 877
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 879
    :cond_8
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Z)V

    .line 881
    :cond_9
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    goto/16 :goto_0

    .line 884
    :cond_a
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 886
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 887
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 889
    :cond_b
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Z)V

    goto/16 :goto_0

    .line 892
    :cond_c
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/dr;->a:Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;

    invoke-static {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)V

    goto/16 :goto_0
.end method
