.class public Lcom/sec/vip/amschaton/aw;
.super Landroid/widget/PopupWindow;
.source "AMSToolPopup.java"


# instance fields
.field protected a:Z

.field protected b:Z

.field protected c:Landroid/graphics/Bitmap;

.field protected d:Lcom/sec/vip/amschaton/i;

.field protected e:Lcom/sec/vip/amschaton/bi;

.field protected f:Lcom/sec/vip/amschaton/ay;

.field private g:Landroid/content/Context;

.field private h:Landroid/view/View;

.field private i:I

.field private j:I

.field private k:Landroid/widget/PopupWindow;

.field private l:Landroid/widget/ImageView;

.field private m:Landroid/widget/PopupWindow$OnDismissListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->h:Landroid/view/View;

    .line 23
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/aw;->a:Z

    .line 24
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/aw;->b:Z

    .line 25
    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->c:Landroid/graphics/Bitmap;

    .line 26
    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->d:Lcom/sec/vip/amschaton/i;

    .line 30
    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    .line 31
    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->l:Landroid/widget/ImageView;

    .line 33
    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->e:Lcom/sec/vip/amschaton/bi;

    .line 34
    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->f:Lcom/sec/vip/amschaton/ay;

    .line 94
    new-instance v0, Lcom/sec/vip/amschaton/ax;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/ax;-><init>(Lcom/sec/vip/amschaton/aw;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->m:Landroid/widget/PopupWindow$OnDismissListener;

    .line 42
    iput-boolean p3, p0, Lcom/sec/vip/amschaton/aw;->a:Z

    .line 43
    sget-boolean v0, Lcom/sec/vip/amschaton/AMSActivity;->j:Z

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/aw;->b:Z

    .line 44
    iput-object p1, p0, Lcom/sec/vip/amschaton/aw;->g:Landroid/content/Context;

    .line 46
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->m:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/aw;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 47
    invoke-direct {p0, p2}, Lcom/sec/vip/amschaton/aw;->a(I)V

    .line 48
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x2

    .line 76
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/aw;->h()V

    .line 78
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/aw;->e()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/aw;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 79
    invoke-virtual {p0, v2}, Lcom/sec/vip/amschaton/aw;->setWidth(I)V

    .line 80
    invoke-virtual {p0, v2}, Lcom/sec/vip/amschaton/aw;->setHeight(I)V

    .line 81
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/aw;->setTouchable(Z)V

    .line 82
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/aw;->setFocusable(Z)V

    .line 83
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/aw;->setOutsideTouchable(Z)V

    .line 85
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/aw;->e()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 86
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->h:Landroid/view/View;

    .line 87
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->h:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/aw;->setContentView(Landroid/view/View;)V

    .line 88
    const v0, 0x7f0c0117

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/aw;->setAnimationStyle(I)V

    .line 90
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->h:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->h:Landroid/view/View;

    invoke-virtual {v0, v2, v2}, Landroid/view/View;->measure(II)V

    .line 92
    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;II)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 210
    .line 211
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 212
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 214
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/aw;->a:Z

    if-eqz v2, :cond_1

    .line 216
    aget v2, v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aw;->j:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 217
    aget v0, v0, v4

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 219
    div-int/lit8 v0, p2, 0x2

    sub-int v0, v2, v0

    .line 220
    iget v4, p0, Lcom/sec/vip/amschaton/aw;->i:I

    add-int/2addr v4, v3

    add-int/lit8 v4, v4, -0x4

    .line 222
    if-gez v0, :cond_0

    move v0, v1

    .line 227
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/aw;->e()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901fa

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 228
    sub-int/2addr v4, v5

    invoke-virtual {p0, p1, v1, v0, v4}, Lcom/sec/vip/amschaton/aw;->showAtLocation(Landroid/view/View;III)V

    .line 229
    invoke-virtual {p0, p1, v2, v3, v1}, Lcom/sec/vip/amschaton/aw;->a(Landroid/view/View;IIZ)V

    .line 248
    :goto_0
    return-void

    .line 233
    :cond_1
    aget v2, v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v2, v3

    .line 234
    aget v0, v0, v4

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aw;->i:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    .line 236
    iget v0, p0, Lcom/sec/vip/amschaton/aw;->j:I

    add-int/2addr v0, v2

    add-int/lit8 v4, v0, -0x3

    .line 237
    div-int/lit8 v0, p3, 0x2

    sub-int v0, v3, v0

    .line 239
    if-gez v0, :cond_2

    move v0, v1

    .line 244
    :cond_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/aw;->e()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901fb

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 245
    sub-int/2addr v4, v5

    invoke-virtual {p0, p1, v1, v4, v0}, Lcom/sec/vip/amschaton/aw;->showAtLocation(Landroid/view/View;III)V

    .line 246
    invoke-virtual {p0, p1, v2, v3, v1}, Lcom/sec/vip/amschaton/aw;->a(Landroid/view/View;IIZ)V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;IIZ)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 144
    :cond_0
    if-eqz p4, :cond_1

    .line 145
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2, p3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0
.end method

.method public a(Lcom/sec/vip/amschaton/ay;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/vip/amschaton/aw;->f:Lcom/sec/vip/amschaton/ay;

    .line 64
    return-void
.end method

.method public a(Lcom/sec/vip/amschaton/bi;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/vip/amschaton/aw;->e:Lcom/sec/vip/amschaton/bi;

    .line 60
    return-void
.end method

.method public a(Lcom/sec/vip/amschaton/i;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/vip/amschaton/aw;->d:Lcom/sec/vip/amschaton/i;

    .line 68
    return-void
.end method

.method protected e()Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->g:Landroid/content/Context;

    return-object v0
.end method

.method protected f()Landroid/view/View;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->h:Landroid/view/View;

    return-object v0
.end method

.method public g()Lcom/sec/vip/amschaton/i;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->d:Lcom/sec/vip/amschaton/i;

    return-object v0
.end method

.method protected h()V
    .locals 5

    .prologue
    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 112
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aw;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    .line 113
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/aw;->e()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 115
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 117
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 118
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 120
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aw;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/aw;->l:Landroid/widget/ImageView;

    .line 121
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->l:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 125
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/aw;->a:Z

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/aw;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02007c

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 131
    :goto_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/aw;->i:I

    .line 132
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/aw;->j:I

    .line 134
    iget-object v1, p0, Lcom/sec/vip/amschaton/aw;->l:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 136
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aw;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 137
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    const v1, 0x7f0c0117

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 138
    return-void

    .line 128
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/aw;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02007b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    if-nez v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/vip/amschaton/aw;->k:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0
.end method
