.class public Lcom/sec/vip/amschaton/AMSActivity;
.super Landroid/app/Activity;
.source "AMSActivity.java"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Ljava/lang/String;

.field public static i:Ljava/lang/String;

.field public static j:Z

.field public static k:Z


# instance fields
.field private l:Z

.field private m:Landroid/content/BroadcastReceiver;

.field private n:Z

.field private o:Z

.field private p:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 76
    sput-boolean v0, Lcom/sec/vip/amschaton/AMSActivity;->j:Z

    .line 77
    sput-boolean v0, Lcom/sec/vip/amschaton/AMSActivity;->k:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 79
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->l:Z

    .line 83
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->n:Z

    .line 84
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->o:Z

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->p:Landroid/widget/Toast;

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/AMSActivity;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;->g()V

    return-void
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    .line 276
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[handleExternalStorageState] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    if-nez p1, :cond_1

    .line 280
    const v0, 0x7f0b00ed

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSActivity;->a(I)V

    .line 281
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSActivity;->finish()V

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 284
    :cond_1
    if-nez p2, :cond_0

    .line 287
    const v0, 0x7f0b00ee

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSActivity;->a(I)V

    .line 288
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSActivity;->finish()V

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 219
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 220
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/AMSActivity;->n:Z

    .line 222
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/AMSActivity;->o:Z

    .line 230
    :goto_0
    return-void

    .line 223
    :cond_0
    const-string v1, "mounted_ro"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/AMSActivity;->n:Z

    .line 225
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSActivity;->o:Z

    goto :goto_0

    .line 227
    :cond_1
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSActivity;->n:Z

    .line 228
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSActivity;->o:Z

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;->f()V

    .line 237
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->n:Z

    iget-boolean v1, p0, Lcom/sec/vip/amschaton/AMSActivity;->o:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/AMSActivity;->a(ZZ)V

    .line 238
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 244
    new-instance v0, Lcom/sec/vip/amschaton/a;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/a;-><init>(Lcom/sec/vip/amschaton/AMSActivity;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->m:Landroid/content/BroadcastReceiver;

    .line 251
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 252
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 253
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 254
    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 255
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 256
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSActivity;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/vip/amschaton/AMSActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 257
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;->g()V

    .line 258
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 265
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 304
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/vip/amschaton/AMSActivity;->j:Z

    .line 308
    :goto_0
    return-void

    .line 306
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/vip/amschaton/AMSActivity;->j:Z

    goto :goto_0
.end method

.method private k()V
    .locals 7

    .prologue
    const/16 v6, 0x1e0

    const/16 v5, 0x140

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 314
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 315
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v3

    .line 316
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v4

    .line 317
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 319
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 330
    :goto_0
    if-eqz v0, :cond_1

    .line 331
    if-gt v3, v5, :cond_0

    if-gt v4, v6, :cond_0

    move v0, v1

    :goto_1
    sput-boolean v0, Lcom/sec/vip/amschaton/AMSActivity;->k:Z

    .line 335
    :goto_2
    return-void

    :pswitch_1
    move v0, v2

    .line 323
    goto :goto_0

    :cond_0
    move v0, v2

    .line 331
    goto :goto_1

    .line 333
    :cond_1
    if-gt v3, v6, :cond_2

    if-gt v4, v5, :cond_2

    :goto_3
    sput-boolean v1, Lcom/sec/vip/amschaton/AMSActivity;->k:Z

    goto :goto_2

    :cond_2
    move v1, v2

    goto :goto_3

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private l()V
    .locals 3

    .prologue
    .line 429
    const-string v0, "showPasswordLockActivity"

    const-string v1, "HomeActivity"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 432
    invoke-static {v0}, Lcom/sec/chaton/util/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 433
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/chaton/settings/ActivityPasswordLockSet;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 434
    const-string v0, "MODE"

    const-string v2, "HOME"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    invoke-virtual {p0, v1}, Lcom/sec/vip/amschaton/AMSActivity;->startActivity(Landroid/content/Intent;)V

    .line 438
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->l:Z

    .line 146
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSActivity;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    const-string v0, "[loadStamp] Resources are already loaded!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSActivity;->l:Z

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSActivity;->b()V

    .line 162
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSActivity;->l:Z

    goto :goto_0
.end method

.method protected a(I)V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->p:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 359
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->p:Landroid/widget/Toast;

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->p:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    .line 368
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->p:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 369
    return-void
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 179
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    sget-object v2, Lcom/sec/vip/amschaton/AMSActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2}, Lcom/sec/vip/amschaton/al;->a(Landroid/content/Context;Landroid/content/res/AssetManager;Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method protected c()Z
    .locals 1

    .prologue
    .line 188
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/al;->g()I

    move-result v0

    .line 189
    if-lez v0, :cond_0

    .line 190
    const/4 v0, 0x0

    .line 192
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;->f()V

    .line 202
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->n:Z

    return v0
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;->f()V

    .line 212
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSActivity;->o:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/AMS/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSActivity;->a:Ljava/lang/String;

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "amsbasicfiles/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSActivity;->b:Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "amsuserfiles/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSActivity;->c:Ljava/lang/String;

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "amssentfiles/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSActivity;->d:Ljava/lang/String;

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "userstamp/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSActivity;->e:Ljava/lang/String;

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "temp/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSActivity;->f:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "justTempAMS.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSActivity;->g:Ljava/lang/String;

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "justTempImage.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSActivity;->h:Ljava/lang/String;

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/ChatON/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSActivity;->i:Ljava/lang/String;

    .line 110
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 111
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 112
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 114
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 116
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 117
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 119
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->i:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 122
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;->j()V

    .line 125
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;->k()V

    .line 126
    return-void

    .line 96
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;->i()V

    .line 131
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 132
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;->h()V

    .line 137
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 138
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;->l()V

    .line 139
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 374
    const-string v0, "onUserLeaveHint"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 378
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/p;->b(Landroid/content/Context;)V

    .line 414
    :goto_0
    return-void

    .line 380
    :cond_0
    invoke-static {}, Lcom/sec/chaton/registration/gj;->a()Lcom/sec/chaton/registration/gj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/chaton/registration/gj;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
