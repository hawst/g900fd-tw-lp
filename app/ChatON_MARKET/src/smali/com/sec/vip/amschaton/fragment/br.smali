.class public Lcom/sec/vip/amschaton/fragment/br;
.super Landroid/widget/CursorAdapter;
.source "AMSListFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

.field private b:Lcom/sec/vip/amschaton/fragment/bk;

.field private c:[Z

.field private d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1050
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/br;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    .line 1051
    invoke-direct {p0, p2, p3}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 1045
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/br;->b:Lcom/sec/vip/amschaton/fragment/bk;

    .line 1047
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    .line 1052
    if-eqz p3, :cond_0

    .line 1053
    new-instance v0, Lcom/sec/vip/amschaton/fragment/bk;

    invoke-direct {v0, p1}, Lcom/sec/vip/amschaton/fragment/bk;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/br;->b:Lcom/sec/vip/amschaton/fragment/bk;

    .line 1054
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    .line 1055
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/br;->a(Z)V

    .line 1057
    :cond_0
    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1128
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/br;->d:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1147
    :goto_0
    return-void

    .line 1131
    :cond_0
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 1132
    const/16 v0, 0x3e8

    iput v0, v5, Landroid/os/Message;->what:I

    .line 1135
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    if-eqz v0, :cond_2

    move v0, v1

    move v2, v3

    move v4, v1

    .line 1136
    :goto_1
    iget-object v6, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    array-length v6, v6

    if-ge v0, v6, :cond_3

    .line 1137
    iget-object v6, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    aget-boolean v6, v6, v0

    if-eqz v6, :cond_1

    move v4, v3

    .line 1136
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    .line 1140
    goto :goto_2

    :cond_2
    move v2, v3

    move v4, v1

    .line 1144
    :cond_3
    if-eqz v4, :cond_4

    move v0, v3

    :goto_3
    iput v0, v5, Landroid/os/Message;->arg1:I

    .line 1145
    if-eqz v2, :cond_5

    :goto_4
    iput v3, v5, Landroid/os/Message;->arg2:I

    .line 1146
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/br;->d:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1144
    goto :goto_3

    :cond_5
    move v3, v1

    .line 1145
    goto :goto_4
.end method


# virtual methods
.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1150
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/br;->d:Landroid/os/Handler;

    .line 1151
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    if-eqz v0, :cond_0

    .line 1065
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1066
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    aput-boolean p1, v1, v0

    .line 1065
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1069
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/br;->notifyDataSetChanged()V

    .line 1070
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/br;->b()V

    .line 1071
    return-void
.end method

.method public a()[Z
    .locals 1

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    return-object v0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1074
    const v0, 0x7f070066

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1075
    const v1, 0x7f070067

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 1076
    const v2, 0x7f070065

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1078
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 1079
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 1080
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1081
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/br;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v3}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v3

    const/16 v4, 0x7d2

    if-ne v3, v4, :cond_1

    .line 1082
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1084
    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1085
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    aget-boolean v3, v3, v4

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1087
    new-instance v3, Lcom/sec/vip/amschaton/fragment/bs;

    invoke-direct {v3, p0, v1}, Lcom/sec/vip/amschaton/fragment/bs;-><init>(Lcom/sec/vip/amschaton/fragment/br;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1101
    :goto_0
    const-string v0, "ams_path"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1102
    if-eqz v0, :cond_0

    .line 1103
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/br;->b:Lcom/sec/vip/amschaton/fragment/bk;

    invoke-virtual {v1, v0, v2}, Lcom/sec/vip/amschaton/fragment/bk;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 1105
    :cond_0
    return-void

    .line 1098
    :cond_1
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1099
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/br;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1110
    const v1, 0x7f030013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1111
    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 1117
    instance-of v0, p1, Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 1118
    check-cast p1, Landroid/widget/CheckBox;

    .line 1119
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1120
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/br;->c:[Z

    aput-boolean p2, v1, v0

    .line 1122
    :cond_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/br;->b()V

    .line 1124
    return-void
.end method
