.class Lcom/sec/vip/amschaton/fragment/aq;
.super Landroid/os/AsyncTask;
.source "AMSComposerFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;


# direct methods
.method private constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V
    .locals 0

    .prologue
    .line 1568
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Lcom/sec/vip/amschaton/fragment/aa;)V
    .locals 0

    .prologue
    .line 1568
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/aq;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1573
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-static {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1575
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-static {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)Z

    .line 1576
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1588
    :goto_0
    return-object v0

    .line 1579
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c:Ljava/lang/String;

    .line 1581
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->q(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1583
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->r(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c:Ljava/lang/String;

    .line 1586
    :cond_1
    sput-boolean v2, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a:Z

    .line 1587
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    .line 1588
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 1600
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1601
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->t(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    .line 1602
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1603
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    iget-object v1, v1, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c:Ljava/lang/String;

    const/16 v2, 0x7d1

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b(Ljava/lang/String;I)V

    .line 1606
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1568
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/fragment/aq;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 1611
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 1612
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)Z

    .line 1613
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->t(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    .line 1614
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1568
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/fragment/aq;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 1594
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1595
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/aq;->a:Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    .line 1596
    return-void
.end method
