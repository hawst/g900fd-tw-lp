.class public Lcom/sec/vip/amschaton/fragment/bn;
.super Ljava/lang/Object;
.source "AMSListFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1572
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/bn;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1570
    const/16 v0, 0x1389

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/bn;->d:I

    .line 1573
    iput-object p2, p0, Lcom/sec/vip/amschaton/fragment/bn;->c:Landroid/content/Context;

    .line 1574
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/bn;->b:Ljava/util/Map;

    .line 1575
    iput p3, p0, Lcom/sec/vip/amschaton/fragment/bn;->d:I

    .line 1576
    return-void
.end method


# virtual methods
.method public a(I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1579
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bn;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1580
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bn;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1598
    :cond_0
    :goto_0
    return-object v0

    .line 1583
    :cond_1
    const/4 v0, 0x0

    .line 1584
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/bn;->d:I

    packed-switch v1, :pswitch_data_0

    .line 1595
    :goto_1
    if-eqz v0, :cond_0

    .line 1596
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bn;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1586
    :pswitch_0
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bn;->c:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, v2}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1589
    :pswitch_1
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/sec/vip/amschaton/q;->b(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1592
    :pswitch_2
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/sec/vip/amschaton/q;->a(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1584
    nop

    :pswitch_data_0
    .packed-switch 0x1388
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(ILandroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 1602
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bn;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1603
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bn;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1606
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/fragment/bo;

    invoke-direct {v0, p0, p2}, Lcom/sec/vip/amschaton/fragment/bo;-><init>(Lcom/sec/vip/amschaton/fragment/bn;Landroid/widget/ImageView;)V

    .line 1613
    new-instance v1, Lcom/sec/vip/amschaton/fragment/bp;

    invoke-direct {v1, p0, p1, v0}, Lcom/sec/vip/amschaton/fragment/bp;-><init>(Lcom/sec/vip/amschaton/fragment/bn;ILandroid/os/Handler;)V

    .line 1621
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1622
    return-void
.end method
