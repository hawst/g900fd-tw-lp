.class public Lcom/sec/vip/amschaton/z;
.super Lcom/sec/vip/amschaton/u;
.source "AMSObjectStamp.java"


# instance fields
.field Z:F

.field aa:F

.field private ab:Landroid/graphics/Bitmap;

.field private ac:I

.field private ad:Lcom/sec/vip/amschaton/al;

.field private final ae:I

.field private final af:I

.field private ag:Z

.field private ah:F

.field private ai:F

.field private aj:F

.field private ak:F

.field private al:I

.field private am:Z


# direct methods
.method public constructor <init>(IIIILandroid/graphics/Bitmap;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 126
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/vip/amschaton/u;-><init>(IIIIII)V

    .line 42
    const/16 v0, 0x37

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    .line 43
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    .line 46
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ae:I

    .line 47
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/vip/amschaton/z;->af:I

    .line 48
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/z;->ag:Z

    .line 51
    iput v7, p0, Lcom/sec/vip/amschaton/z;->ah:F

    .line 52
    iput v7, p0, Lcom/sec/vip/amschaton/z;->ai:F

    .line 53
    iput v7, p0, Lcom/sec/vip/amschaton/z;->aj:F

    .line 54
    iput v7, p0, Lcom/sec/vip/amschaton/z;->ak:F

    .line 57
    iput v5, p0, Lcom/sec/vip/amschaton/z;->al:I

    .line 59
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/z;->am:Z

    .line 390
    iput v7, p0, Lcom/sec/vip/amschaton/z;->Z:F

    .line 391
    iput v7, p0, Lcom/sec/vip/amschaton/z;->aa:F

    .line 127
    iput-object p5, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    .line 128
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    .line 130
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 131
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setDither(Z)V

    .line 132
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 134
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    .line 135
    iput-boolean p6, p0, Lcom/sec/vip/amschaton/z;->am:Z

    .line 136
    return-void
.end method

.method public constructor <init>(IIIIZ)V
    .locals 7

    .prologue
    .line 76
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/vip/amschaton/u;-><init>(IIIIII)V

    .line 42
    const/16 v0, 0x37

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    .line 43
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    .line 46
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ae:I

    .line 47
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/vip/amschaton/z;->af:I

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/z;->ag:Z

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ah:F

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ai:F

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->aj:F

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ak:F

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->al:I

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/z;->am:Z

    .line 390
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->Z:F

    .line 391
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->aa:F

    .line 78
    const/4 v0, 0x0

    .line 79
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 80
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/vip/amschaton/al;->d(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    .line 81
    const/4 v0, 0x1

    .line 95
    :goto_0
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    .line 97
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 98
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 99
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 101
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/al;->h(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 102
    :cond_0
    const/16 v0, 0x6e

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    .line 106
    :goto_1
    iput-boolean p5, p0, Lcom/sec/vip/amschaton/z;->am:Z

    .line 107
    return-void

    .line 82
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/al;->i()I

    move-result v2

    add-int/2addr v1, v2

    if-ge p1, v1, :cond_2

    .line 83
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    iget v2, p0, Lcom/sec/vip/amschaton/z;->a:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    invoke-virtual {v3}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v3

    sub-int/2addr v2, v3

    const/4 v3, 0x7

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/al;->a(II)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 84
    :cond_2
    const/16 v1, 0x4e20

    if-lt p1, v1, :cond_3

    const/16 v1, 0x7530

    if-ge p1, v1, :cond_3

    .line 85
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    iget v2, p0, Lcom/sec/vip/amschaton/z;->a:I

    add-int/lit16 v2, v2, -0x4e20

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/al;->e(IZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 86
    :cond_3
    const/16 v1, 0x7530

    if-lt p1, v1, :cond_4

    const v1, 0x9c40

    if-ge p1, v1, :cond_4

    .line 87
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    iget v2, p0, Lcom/sec/vip/amschaton/z;->a:I

    add-int/lit16 v2, v2, -0x7530

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/al;->b(IZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 88
    :cond_4
    const v1, 0x9c40

    if-lt p1, v1, :cond_5

    .line 89
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    iget v2, p0, Lcom/sec/vip/amschaton/z;->a:I

    const v3, 0x9c40

    sub-int/2addr v2, v3

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/al;->a(IZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 92
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/al;->d(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    .line 93
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 104
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    goto :goto_1
.end method

.method public constructor <init>(IIIZ)V
    .locals 6

    .prologue
    .line 63
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;-><init>(IIIIZ)V

    .line 64
    return-void
.end method

.method private g(FF)Landroid/graphics/PointF;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1165
    .line 1167
    iget v0, p0, Lcom/sec/vip/amschaton/z;->E:F

    sub-float v0, p1, v0

    .line 1168
    iget v1, p0, Lcom/sec/vip/amschaton/z;->F:F

    sub-float v1, p2, v1

    .line 1169
    const/high16 v2, 0x41c00000    # 24.0f

    iget v3, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v2, v3

    .line 1171
    iget v3, p0, Lcom/sec/vip/amschaton/z;->al:I

    const/16 v4, 0x69

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/sec/vip/amschaton/z;->al:I

    const/16 v4, 0x6b

    if-ne v3, v4, :cond_7

    .line 1172
    :cond_0
    cmpg-float v1, v5, v0

    if-gtz v1, :cond_3

    cmpg-float v1, v0, v2

    if-gez v1, :cond_3

    .line 1173
    iget v0, p0, Lcom/sec/vip/amschaton/z;->E:F

    add-float p1, v0, v2

    .line 1181
    :cond_1
    :goto_0
    iget v0, p0, Lcom/sec/vip/amschaton/z;->E:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_6

    .line 1182
    iget v0, p0, Lcom/sec/vip/amschaton/z;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/z;->E:F

    sub-float v1, p1, v1

    add-float p2, v0, v1

    .line 1204
    :cond_2
    :goto_1
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0

    .line 1174
    :cond_3
    iget v1, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_4

    .line 1175
    iget v0, p0, Lcom/sec/vip/amschaton/z;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v1, v1

    add-float p1, v0, v1

    goto :goto_0

    .line 1176
    :cond_4
    neg-float v1, v2

    cmpg-float v1, v1, v0

    if-gez v1, :cond_5

    cmpg-float v1, v0, v5

    if-gez v1, :cond_5

    .line 1177
    iget v0, p0, Lcom/sec/vip/amschaton/z;->E:F

    sub-float p1, v0, v2

    goto :goto_0

    .line 1178
    :cond_5
    iget v1, p0, Lcom/sec/vip/amschaton/z;->ac:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    .line 1179
    iget v0, p0, Lcom/sec/vip/amschaton/z;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v1, v1

    sub-float p1, v0, v1

    goto :goto_0

    .line 1184
    :cond_6
    iget v0, p0, Lcom/sec/vip/amschaton/z;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/z;->E:F

    sub-float v1, p1, v1

    sub-float p2, v0, v1

    goto :goto_1

    .line 1187
    :cond_7
    iget v0, p0, Lcom/sec/vip/amschaton/z;->al:I

    const/16 v3, 0x6a

    if-eq v0, v3, :cond_8

    iget v0, p0, Lcom/sec/vip/amschaton/z;->al:I

    const/16 v3, 0x6c

    if-ne v0, v3, :cond_2

    .line 1188
    :cond_8
    cmpg-float v0, v5, v1

    if-gtz v0, :cond_a

    cmpg-float v0, v1, v2

    if-gez v0, :cond_a

    .line 1189
    iget v0, p0, Lcom/sec/vip/amschaton/z;->F:F

    add-float p2, v0, v2

    .line 1197
    :cond_9
    :goto_2
    iget v0, p0, Lcom/sec/vip/amschaton/z;->F:F

    cmpl-float v0, p2, v0

    if-lez v0, :cond_d

    .line 1198
    iget v0, p0, Lcom/sec/vip/amschaton/z;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/z;->F:F

    sub-float v1, p2, v1

    add-float p1, v0, v1

    goto :goto_1

    .line 1190
    :cond_a
    iget v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_b

    .line 1191
    iget v0, p0, Lcom/sec/vip/amschaton/z;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v1, v1

    add-float p2, v0, v1

    goto :goto_2

    .line 1192
    :cond_b
    neg-float v0, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_c

    cmpg-float v0, v1, v5

    if-gez v0, :cond_c

    .line 1193
    iget v0, p0, Lcom/sec/vip/amschaton/z;->F:F

    sub-float p2, v0, v2

    goto :goto_2

    .line 1194
    :cond_c
    iget v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_9

    .line 1195
    iget v0, p0, Lcom/sec/vip/amschaton/z;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v1, v1

    sub-float p2, v0, v1

    goto :goto_2

    .line 1200
    :cond_d
    iget v0, p0, Lcom/sec/vip/amschaton/z;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/z;->F:F

    sub-float v1, p2, v1

    sub-float p1, v0, v1

    goto/16 :goto_1
.end method


# virtual methods
.method public a(F)V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    .line 146
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 147
    iget v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    if-nez v0, :cond_0

    .line 148
    const/high16 v0, 0x425c0000    # 55.0f

    iget v1, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    add-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    .line 152
    :goto_0
    return-void

    .line 150
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    add-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    goto :goto_0
.end method

.method public a(FF)V
    .locals 0

    .prologue
    .line 221
    iput p1, p0, Lcom/sec/vip/amschaton/z;->E:F

    .line 222
    iput p2, p0, Lcom/sec/vip/amschaton/z;->F:F

    .line 223
    return-void
.end method

.method public a(FFFFI)Z
    .locals 7

    .prologue
    .line 292
    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/z;->a(FFFFIZ)Z

    move-result v0

    return v0
.end method

.method public a(FFFFIZ)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 298
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 299
    if-eqz p6, :cond_0

    .line 302
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 303
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->i:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 307
    :cond_0
    sub-float v1, p3, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 308
    sub-float v2, p4, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 309
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 310
    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 313
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/z;->a(Landroid/graphics/RectF;)Landroid/graphics/Point;

    move-result-object v4

    .line 315
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 316
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v0, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 317
    iget v0, p0, Lcom/sec/vip/amschaton/z;->e:I

    int-to-float v0, v0

    iget v1, v4, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, v4, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v3, v0, v1, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 319
    const/16 v0, 0x100

    if-ge p5, v0, :cond_2

    .line 320
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, p5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 323
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 325
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 329
    :cond_1
    iget v0, p0, Lcom/sec/vip/amschaton/z;->c:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v0, v1

    div-float/2addr v0, v5

    sub-float v1, p1, v0

    iget v0, p0, Lcom/sec/vip/amschaton/z;->c:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v0, v2

    div-float/2addr v0, v5

    sub-float v2, p2, v0

    iget v0, p0, Lcom/sec/vip/amschaton/z;->c:I

    int-to-float v0, v0

    iget v3, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v0, v3

    div-float/2addr v0, v5

    add-float v3, p3, v0

    iget v0, p0, Lcom/sec/vip/amschaton/z;->c:I

    int-to-float v0, v0

    iget v4, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v0, v4

    div-float/2addr v0, v5

    add-float v4, p4, v0

    iget v5, p0, Lcom/sec/vip/amschaton/z;->e:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;->b(FFFFI)V

    .line 339
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 334
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 337
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    iget v2, p0, Lcom/sec/vip/amschaton/z;->a:I

    iget-object v4, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    invoke-virtual {v4}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v4

    sub-int/2addr v2, v4

    add-int/lit16 v4, p5, -0x3e8

    invoke-virtual {v1, v2, v4}, Lcom/sec/vip/amschaton/al;->a(II)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public a(FFI)Z
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/vip/amschaton/z;->a(FFIZ)Z

    move-result v0

    return v0
.end method

.method public a(FFIZ)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 243
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/z;->am:Z

    if-eqz v0, :cond_0

    .line 244
    iget v3, p0, Lcom/sec/vip/amschaton/z;->E:F

    .line 245
    iget v4, p0, Lcom/sec/vip/amschaton/z;->F:F

    .line 247
    iget v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 260
    cmpl-float v0, v3, p1

    if-lez v0, :cond_4

    move v1, p1

    .line 265
    :goto_0
    cmpl-float v0, v4, p2

    if-lez v0, :cond_3

    move v2, p2

    :goto_1
    move-object v0, p0

    move v5, p3

    move v6, p4

    .line 270
    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/z;->a(FFFFIZ)Z

    move-result v0

    .line 287
    :goto_2
    return v0

    .line 272
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    float-to-int v2, p2

    iget v3, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v3, v3

    add-float/2addr v3, p1

    float-to-int v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v4, v4

    add-float/2addr v4, p2

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 273
    if-eqz p4, :cond_1

    .line 274
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v6, v6, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 276
    :cond_1
    const/16 v1, 0x100

    if-ge p3, v1, :cond_2

    .line 277
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, p3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 278
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, v5, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 279
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget v5, p0, Lcom/sec/vip/amschaton/z;->e:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;->b(FFFFI)V

    .line 287
    :goto_3
    const/4 v0, 0x1

    goto :goto_2

    .line 283
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 285
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v2

    iget v3, p0, Lcom/sec/vip/amschaton/z;->a:I

    iget-object v4, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    invoke-virtual {v4}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit16 v4, p3, -0x3e8

    invoke-virtual {v2, v3, v4}, Lcom/sec/vip/amschaton/al;->a(II)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, v5, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_3

    :cond_3
    move v2, v4

    move v4, p2

    goto :goto_1

    :cond_4
    move v1, v3

    move v3, p1

    goto :goto_0
.end method

.method public a(Landroid/graphics/RectF;I)Z
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/vip/amschaton/z;->a(Landroid/graphics/RectF;IZ)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/graphics/RectF;IZ)Z
    .locals 7

    .prologue
    .line 349
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/z;->am:Z

    if-eqz v0, :cond_0

    .line 350
    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    iget v3, p1, Landroid/graphics/RectF;->right:F

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    move-object v0, p0

    move v5, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/z;->a(FFFFIZ)Z

    move-result v0

    .line 352
    :goto_0
    return v0

    :cond_0
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0, v0, v1, p2, p3}, Lcom/sec/vip/amschaton/z;->a(FFIZ)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcom/sec/amsoma/structure/AMS_RECT;)Z
    .locals 12

    .prologue
    .line 164
    iget v0, p0, Lcom/sec/vip/amschaton/z;->a:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/sec/vip/amschaton/z;->a:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/z;->z:I

    const/4 v3, 0x0

    const/4 v4, 0x2

    iget v2, p0, Lcom/sec/vip/amschaton/z;->a:I

    int-to-byte v5, v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v9}, Lcom/sec/amsoma/AMSLibs;->VipAMS_EncodeNewStamp(ILcom/sec/amsoma/structure/AMS_RECT;BBB[IIII)Z

    move-result v0

    if-nez v0, :cond_2

    .line 166
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/z;->a()V

    .line 167
    const/4 v0, 0x0

    .line 188
    :goto_0
    return v0

    .line 170
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/z;->a:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->ad:Lcom/sec/vip/amschaton/al;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/al;->i()I

    move-result v2

    add-int/2addr v1, v2

    if-ge v0, v1, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/z;->z:I

    const/4 v3, 0x0

    const/4 v4, 0x2

    iget v2, p0, Lcom/sec/vip/amschaton/z;->a:I

    int-to-byte v5, v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v9}, Lcom/sec/amsoma/AMSLibs;->VipAMS_EncodeNewStamp(ILcom/sec/amsoma/structure/AMS_RECT;BBB[IIII)Z

    move-result v0

    if-nez v0, :cond_2

    .line 173
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/z;->a()V

    .line 174
    const/4 v0, 0x0

    goto :goto_0

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v1, v0, [I

    .line 179
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 180
    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->y:Lcom/sec/amsoma/AMSLibs;

    iget v3, p0, Lcom/sec/vip/amschaton/z;->z:I

    const/4 v5, 0x2

    const/4 v6, 0x2

    iget v0, p0, Lcom/sec/vip/amschaton/z;->a:I

    int-to-byte v7, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    const/16 v11, 0x20

    move-object v4, p1

    move-object v8, v1

    invoke-virtual/range {v2 .. v11}, Lcom/sec/amsoma/AMSLibs;->VipAMS_EncodeNewStamp(ILcom/sec/amsoma/structure/AMS_RECT;BBB[IIII)Z

    move-result v0

    if-nez v0, :cond_2

    .line 181
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/z;->a()V

    .line 182
    const/4 v0, 0x0

    goto :goto_0

    .line 186
    :cond_2
    new-instance v0, Landroid/graphics/RectF;

    iget-short v1, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-short v2, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-short v3, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-short v4, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/16 v1, 0xff

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/z;->a(Landroid/graphics/RectF;I)Z

    .line 188
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public b(Lcom/sec/amsoma/structure/AMS_RECT;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 200
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/z;->H:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-nez v0, :cond_1

    :cond_0
    move v0, v7

    .line 216
    :goto_0
    return v0

    .line 204
    :cond_1
    iget v0, p0, Lcom/sec/vip/amschaton/z;->e:I

    if-gez v0, :cond_2

    .line 206
    iget v0, p0, Lcom/sec/vip/amschaton/z;->e:I

    add-int/lit16 v0, v0, 0x168

    iput v0, p0, Lcom/sec/vip/amschaton/z;->e:I

    .line 209
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/z;->z:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v4

    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v5

    iget v6, p0, Lcom/sec/vip/amschaton/z;->e:I

    move-object v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/amsoma/AMSLibs;->VipAMS_ChangeSelectStamp(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;Lcom/sec/amsoma/structure/AMS_RECT;III)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v7

    .line 210
    goto :goto_0

    .line 212
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, p1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_TRect(Lcom/sec/amsoma/structure/AMS_RECT;)V

    .line 213
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    iget v1, p0, Lcom/sec/vip/amschaton/z;->e:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_nRotation(I)V

    .line 215
    new-instance v0, Landroid/graphics/RectF;

    iget-short v1, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-short v2, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-short v3, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-short v4, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/16 v1, 0xff

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/z;->a(Landroid/graphics/RectF;I)Z

    .line 216
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(FF)Z
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 366
    iget v2, p0, Lcom/sec/vip/amschaton/z;->B:I

    if-eq v2, v5, :cond_1

    .line 383
    :cond_0
    :goto_0
    return v0

    .line 369
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/z;->b()Landroid/graphics/RectF;

    move-result-object v2

    .line 372
    const/high16 v3, 0x41900000    # 18.0f

    iget v4, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v3, v4

    .line 373
    new-array v4, v5, [F

    aput p1, v4, v0

    aput p2, v4, v1

    .line 374
    new-instance v5, Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v6}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v6

    iget-short v6, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v6, v6

    sub-float/2addr v6, v3

    iget-object v7, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v7

    iget-short v7, v7, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v7, v7

    sub-float/2addr v7, v3

    iget-object v8, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v8

    iget-short v8, v8, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v8, v8

    add-float/2addr v8, v3

    iget-object v9, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v9

    iget-short v9, v9, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v9, v9

    add-float/2addr v3, v9

    invoke-direct {v5, v6, v7, v8, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 375
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v3

    .line 376
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 377
    invoke-virtual {v6}, Landroid/graphics/Matrix;->reset()V

    .line 378
    rsub-int/lit8 v3, v3, 0x5a

    rem-int/lit8 v3, v3, 0x5a

    int-to-float v3, v3

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    invoke-virtual {v6, v3, v7, v8}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 379
    invoke-virtual {v6, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 380
    aget v3, v4, v0

    aget v4, v4, v1

    invoke-virtual {v5, v3, v4}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    .line 381
    goto :goto_0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 387
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/z;->ag:Z

    .line 388
    return-void
.end method

.method public d(FF)Z
    .locals 13

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    .line 407
    const/4 v0, 0x0

    .line 651
    :goto_0
    return v0

    .line 413
    :cond_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/z;->am:Z

    if-eqz v0, :cond_12

    .line 414
    const/4 v0, 0x0

    .line 415
    iget v1, p0, Lcom/sec/vip/amschaton/z;->B:I

    packed-switch v1, :pswitch_data_0

    .line 601
    :goto_1
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 417
    :pswitch_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-nez v1, :cond_1

    .line 418
    const-string v0, "Why is selected object null???"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 420
    const/4 v0, 0x1

    goto :goto_0

    .line 423
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    if-eqz v1, :cond_2

    .line 425
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/z;->b()Landroid/graphics/RectF;

    move-result-object v1

    .line 427
    invoke-virtual {v1, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 429
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/z;->f:Z

    .line 439
    :goto_2
    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v1, v2

    .line 440
    const/high16 v2, 0x40c00000    # 6.0f

    iget v3, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v2, v3

    .line 442
    const/4 v3, 0x2

    new-array v3, v3, [F

    .line 443
    const/4 v4, 0x0

    aput p1, v3, v4

    .line 444
    const/4 v4, 0x1

    aput p2, v3, v4

    .line 446
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 447
    new-instance v5, Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v6}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v6

    iget-short v6, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v7

    iget-short v7, v7, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v8

    iget-short v8, v8, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v9

    iget-short v9, v9, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v9, v9

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 449
    iget v6, p0, Lcom/sec/vip/amschaton/z;->e:I

    neg-int v6, v6

    int-to-float v6, v6

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    invoke-virtual {v4, v6, v7, v8}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 451
    invoke-virtual {v4, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 452
    const/4 v4, 0x0

    aget v4, v3, v4

    .line 453
    const/4 v6, 0x1

    aget v3, v3, v6

    .line 457
    iget v6, v5, Landroid/graphics/RectF;->left:F

    float-to-int v6, v6

    .line 458
    iget v7, v5, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    .line 459
    iget v8, v5, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    .line 460
    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    .line 462
    int-to-float v9, v6

    sub-float/2addr v9, v1

    sub-float/2addr v9, v2

    const/high16 v10, 0x3f000000    # 0.5f

    add-float/2addr v9, v10

    float-to-int v9, v9

    .line 463
    int-to-float v10, v7

    sub-float/2addr v10, v1

    sub-float/2addr v10, v2

    const/high16 v11, 0x3f000000    # 0.5f

    add-float/2addr v10, v11

    float-to-int v10, v10

    .line 464
    int-to-float v11, v8

    add-float/2addr v11, v1

    add-float/2addr v11, v2

    const/high16 v12, 0x3f000000    # 0.5f

    add-float/2addr v11, v12

    float-to-int v11, v11

    .line 465
    int-to-float v12, v5

    add-float/2addr v12, v1

    add-float/2addr v2, v12

    const/high16 v12, 0x3f000000    # 0.5f

    add-float/2addr v2, v12

    float-to-int v2, v2

    .line 475
    int-to-float v12, v11

    sub-float/2addr v12, v4

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    cmpg-float v12, v12, v1

    if-gez v12, :cond_7

    int-to-float v12, v10

    sub-float/2addr v12, v3

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    cmpg-float v12, v12, v1

    if-gez v12, :cond_7

    .line 482
    const/16 v0, 0x65

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    .line 484
    const/4 v0, 0x1

    .line 533
    :cond_2
    :goto_3
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/z;->I:Z

    .line 534
    iget v1, p0, Lcom/sec/vip/amschaton/z;->B:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/sec/vip/amschaton/z;->B:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_e

    .line 536
    :cond_3
    iget v0, p0, Lcom/sec/vip/amschaton/z;->al:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    .line 538
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v1, v0

    .line 539
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    .line 540
    cmpl-float v2, v1, v0

    if-eqz v2, :cond_4

    move v0, v1

    .line 554
    :cond_4
    iget v2, p0, Lcom/sec/vip/amschaton/z;->al:I

    const/16 v3, 0x69

    if-ne v2, v3, :cond_b

    .line 555
    iget v0, p0, Lcom/sec/vip/amschaton/z;->E:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/z;->aj:F

    .line 556
    iget v0, p0, Lcom/sec/vip/amschaton/z;->ai:F

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ak:F

    .line 568
    :cond_5
    :goto_4
    iget v0, p0, Lcom/sec/vip/amschaton/z;->aj:F

    iget v1, p0, Lcom/sec/vip/amschaton/z;->ak:F

    const/16 v2, 0x80

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/vip/amschaton/z;->a(FFI)Z

    goto/16 :goto_1

    .line 433
    :cond_6
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/z;->f:Z

    .line 435
    iput p1, p0, Lcom/sec/vip/amschaton/z;->Z:F

    .line 436
    iput p2, p0, Lcom/sec/vip/amschaton/z;->aa:F

    goto/16 :goto_2

    .line 499
    :cond_7
    add-int v12, v9, v11

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    sub-float/2addr v12, v4

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    cmpg-float v12, v12, v1

    if-gez v12, :cond_8

    int-to-float v12, v10

    sub-float/2addr v12, v3

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    cmpg-float v12, v12, v1

    if-gez v12, :cond_8

    .line 501
    int-to-float v1, v6

    iput v1, p0, Lcom/sec/vip/amschaton/z;->E:F

    .line 502
    int-to-float v1, v5

    iput v1, p0, Lcom/sec/vip/amschaton/z;->F:F

    .line 503
    int-to-float v1, v8

    iput v1, p0, Lcom/sec/vip/amschaton/z;->ah:F

    .line 504
    int-to-float v1, v5

    iput v1, p0, Lcom/sec/vip/amschaton/z;->ai:F

    .line 505
    const/4 v1, 0x6

    iput v1, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 506
    const/16 v1, 0x6a

    iput v1, p0, Lcom/sec/vip/amschaton/z;->al:I

    goto/16 :goto_3

    .line 507
    :cond_8
    int-to-float v12, v11

    sub-float/2addr v12, v4

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    cmpg-float v12, v12, v1

    if-gez v12, :cond_9

    add-int v12, v10, v2

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    sub-float/2addr v12, v3

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    cmpg-float v12, v12, v1

    if-gez v12, :cond_9

    .line 509
    int-to-float v1, v6

    iput v1, p0, Lcom/sec/vip/amschaton/z;->E:F

    .line 510
    int-to-float v1, v7

    iput v1, p0, Lcom/sec/vip/amschaton/z;->F:F

    .line 511
    int-to-float v1, v6

    iput v1, p0, Lcom/sec/vip/amschaton/z;->ah:F

    .line 512
    int-to-float v1, v5

    iput v1, p0, Lcom/sec/vip/amschaton/z;->ai:F

    .line 513
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 514
    const/16 v1, 0x6b

    iput v1, p0, Lcom/sec/vip/amschaton/z;->al:I

    goto/16 :goto_3

    .line 515
    :cond_9
    add-int/2addr v11, v9

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    sub-float/2addr v11, v4

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    cmpg-float v11, v11, v1

    if-gez v11, :cond_a

    int-to-float v11, v2

    sub-float/2addr v11, v3

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    cmpg-float v11, v11, v1

    if-gez v11, :cond_a

    .line 517
    int-to-float v1, v6

    iput v1, p0, Lcom/sec/vip/amschaton/z;->E:F

    .line 518
    int-to-float v1, v7

    iput v1, p0, Lcom/sec/vip/amschaton/z;->F:F

    .line 519
    int-to-float v1, v8

    iput v1, p0, Lcom/sec/vip/amschaton/z;->ah:F

    .line 520
    int-to-float v1, v7

    iput v1, p0, Lcom/sec/vip/amschaton/z;->ai:F

    .line 521
    const/4 v1, 0x6

    iput v1, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 522
    const/16 v1, 0x6c

    iput v1, p0, Lcom/sec/vip/amschaton/z;->al:I

    goto/16 :goto_3

    .line 523
    :cond_a
    int-to-float v6, v9

    sub-float v4, v6, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v1

    if-gez v4, :cond_2

    add-int/2addr v2, v10

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v1, v2, v1

    if-gez v1, :cond_2

    .line 525
    int-to-float v1, v8

    iput v1, p0, Lcom/sec/vip/amschaton/z;->E:F

    .line 526
    int-to-float v1, v7

    iput v1, p0, Lcom/sec/vip/amschaton/z;->F:F

    .line 527
    int-to-float v1, v8

    iput v1, p0, Lcom/sec/vip/amschaton/z;->ah:F

    .line 528
    int-to-float v1, v5

    iput v1, p0, Lcom/sec/vip/amschaton/z;->ai:F

    .line 529
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 530
    const/16 v1, 0x69

    iput v1, p0, Lcom/sec/vip/amschaton/z;->al:I

    goto/16 :goto_3

    .line 557
    :cond_b
    iget v2, p0, Lcom/sec/vip/amschaton/z;->al:I

    const/16 v3, 0x6a

    if-ne v2, v3, :cond_c

    .line 558
    iget v1, p0, Lcom/sec/vip/amschaton/z;->ah:F

    iput v1, p0, Lcom/sec/vip/amschaton/z;->aj:F

    .line 559
    iget v1, p0, Lcom/sec/vip/amschaton/z;->ai:F

    sub-float v0, v1, v0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ak:F

    goto/16 :goto_4

    .line 560
    :cond_c
    iget v2, p0, Lcom/sec/vip/amschaton/z;->al:I

    const/16 v3, 0x6b

    if-ne v2, v3, :cond_d

    .line 561
    iget v0, p0, Lcom/sec/vip/amschaton/z;->E:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/z;->aj:F

    .line 562
    iget v0, p0, Lcom/sec/vip/amschaton/z;->ai:F

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ak:F

    goto/16 :goto_4

    .line 563
    :cond_d
    iget v1, p0, Lcom/sec/vip/amschaton/z;->al:I

    const/16 v2, 0x6c

    if-ne v1, v2, :cond_5

    .line 564
    iget v1, p0, Lcom/sec/vip/amschaton/z;->ah:F

    iput v1, p0, Lcom/sec/vip/amschaton/z;->aj:F

    .line 565
    iget v1, p0, Lcom/sec/vip/amschaton/z;->ai:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/z;->ak:F

    goto/16 :goto_4

    .line 571
    :cond_e
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    if-nez v1, :cond_f

    .line 572
    new-instance v1, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    iput-object v1, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 573
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->y:Lcom/sec/amsoma/AMSLibs;

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->getM_pSelectObjectData()I

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1, v2, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetSelectObjectData(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)V

    .line 574
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/z;->ag:Z

    .line 577
    :cond_f
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/z;->ag:Z

    if-eqz v1, :cond_10

    if-eqz v0, :cond_10

    .line 578
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    goto/16 :goto_1

    .line 582
    :cond_10
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    sub-float v0, p1, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->C:I

    .line 583
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v0, v0

    sub-float v0, p2, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->D:I

    .line 584
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 585
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v0

    const/16 v5, 0x80

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;->a(FFFFI)Z

    .line 586
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/z;->ag:Z

    goto/16 :goto_1

    .line 589
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/z;->G:Z

    if-nez v0, :cond_11

    .line 590
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/z;->G:Z

    .line 591
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 594
    :cond_11
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 596
    iget v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    .line 597
    sub-float v1, p1, v0

    sub-float v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/sec/vip/amschaton/z;->a(FF)V

    .line 598
    add-float v1, p1, v0

    add-float/2addr v0, p2

    const/16 v2, 0x80

    invoke-virtual {p0, v1, v0, v2}, Lcom/sec/vip/amschaton/z;->a(FFI)Z

    goto/16 :goto_1

    .line 604
    :cond_12
    iget v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    packed-switch v0, :pswitch_data_1

    .line 651
    :cond_13
    :goto_5
    :pswitch_3
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 606
    :pswitch_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-nez v0, :cond_14

    .line 607
    const-string v0, "Why is selected object null???"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 609
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 612
    :cond_14
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    if-nez v0, :cond_15

    .line 613
    new-instance v0, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 614
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->y:Lcom/sec/amsoma/AMSLibs;

    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->getM_pSelectObjectData()I

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetSelectObjectData(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)V

    .line 615
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/z;->ag:Z

    .line 619
    :cond_15
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/z;->ag:Z

    if-eqz v0, :cond_16

    .line 620
    const/high16 v0, 0x41400000    # 12.0f

    iget v1, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v0, v1

    .line 621
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v1, v1

    sub-float/2addr v1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    .line 622
    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v2, v2

    add-float/2addr v2, v0

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    .line 623
    sub-float/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v0

    if-gez v2, :cond_16

    sub-float/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v0, v1, v0

    if-gez v0, :cond_16

    .line 625
    const/16 v0, 0x65

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    .line 626
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    goto :goto_5

    .line 631
    :cond_16
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    cmpg-float v0, v0, p1

    if-gez v0, :cond_13

    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_13

    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v0, v0

    cmpg-float v0, v0, p2

    if-gez v0, :cond_13

    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_13

    .line 633
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    sub-float v0, p1, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->C:I

    .line 634
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v0, v0

    sub-float v0, p2, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/z;->D:I

    .line 635
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 636
    iget v0, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v1, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v1, v1

    sub-float v1, p2, v1

    const/16 v2, 0x80

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/vip/amschaton/z;->a(FFI)Z

    .line 638
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/z;->I:Z

    .line 639
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/z;->ag:Z

    goto/16 :goto_5

    .line 645
    :pswitch_5
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 646
    iget v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/z;->C:I

    .line 647
    iget v0, p0, Lcom/sec/vip/amschaton/z;->ac:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/z;->D:I

    .line 648
    iget v0, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v1, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v1, v1

    sub-float v1, p2, v1

    const/16 v2, 0xff

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/vip/amschaton/z;->a(FFI)Z

    goto/16 :goto_5

    .line 415
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 604
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1077
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1078
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1079
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/z;->ab:Landroid/graphics/Bitmap;

    .line 1081
    :cond_0
    return-void
.end method

.method public e(FF)Z
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/16 v5, 0x80

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 667
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v1, :cond_0

    .line 819
    :goto_0
    return v0

    .line 671
    :cond_0
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/z;->am:Z

    if-eqz v1, :cond_c

    .line 672
    iget v1, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v1, v1

    mul-float v4, v1, v7

    .line 674
    iget v1, p0, Lcom/sec/vip/amschaton/z;->B:I

    packed-switch v1, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    move v0, v6

    .line 792
    goto :goto_0

    .line 676
    :pswitch_1
    sub-float v1, p1, v4

    sub-float v2, p2, v4

    add-float v3, p1, v4

    add-float/2addr v4, p2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;->a(FFFFI)Z

    goto :goto_1

    .line 683
    :pswitch_2
    const/4 v1, 0x2

    new-array v1, v1, [F

    .line 684
    aput p1, v1, v0

    .line 685
    aput p2, v1, v6

    .line 687
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 688
    iget v3, p0, Lcom/sec/vip/amschaton/z;->e:I

    neg-int v3, v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/z;->X:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget-object v7, p0, Lcom/sec/vip/amschaton/z;->X:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    invoke-virtual {v2, v3, v4, v7}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 689
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 690
    aget v0, v1, v0

    aget v1, v1, v6

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/z;->g(FF)Landroid/graphics/PointF;

    move-result-object v0

    .line 691
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0, v1, v0, v5}, Lcom/sec/vip/amschaton/z;->a(FFI)Z

    .line 692
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/z;->H:Z

    if-eqz v0, :cond_2

    .line 693
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->I:Z

    .line 695
    :cond_2
    iget v0, p0, Lcom/sec/vip/amschaton/z;->al:I

    invoke-virtual {p0, v0, v6}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    goto :goto_1

    .line 699
    :pswitch_3
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/z;->f:Z

    if-eqz v1, :cond_7

    .line 700
    new-instance v4, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v4}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 701
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    sub-int/2addr v1, v2

    .line 702
    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    sub-int/2addr v2, v3

    .line 703
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/z;->c()Landroid/graphics/Point;

    move-result-object v3

    .line 704
    iget v7, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    sub-float/2addr v7, p2

    float-to-double v7, v7

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    sub-float/2addr v3, p1

    float-to-double v9, v3

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v7

    const-wide v9, 0x4056800000000000L    # 90.0

    sub-double/2addr v7, v9

    double-to-int v3, v7

    iput v3, p0, Lcom/sec/vip/amschaton/z;->e:I

    .line 706
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iput-short v3, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 707
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iput-short v3, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 711
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    iput v3, p0, Lcom/sec/vip/amschaton/z;->N:I

    .line 712
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    iput v3, p0, Lcom/sec/vip/amschaton/z;->O:I

    .line 714
    iget-short v3, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    if-gez v3, :cond_5

    .line 715
    iput-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 720
    :cond_3
    :goto_2
    iget-short v3, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    if-gez v3, :cond_6

    .line 721
    iput-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 727
    :cond_4
    :goto_3
    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 728
    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    add-int/2addr v0, v2

    int-to-short v0, v0

    iput-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 731
    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v0

    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v0

    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v0

    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;->a(FFFFI)Z

    .line 758
    :goto_4
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/z;->H:Z

    if-eqz v0, :cond_1

    .line 759
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->I:Z

    goto/16 :goto_1

    .line 717
    :cond_5
    iget-short v3, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v7, p0, Lcom/sec/vip/amschaton/z;->N:I

    sub-int/2addr v7, v1

    if-le v3, v7, :cond_3

    .line 718
    iget v3, p0, Lcom/sec/vip/amschaton/z;->N:I

    sub-int/2addr v3, v1

    int-to-short v3, v3

    iput-short v3, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    goto :goto_2

    .line 723
    :cond_6
    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v3, p0, Lcom/sec/vip/amschaton/z;->O:I

    sub-int/2addr v3, v2

    if-le v0, v3, :cond_4

    .line 724
    iget v0, p0, Lcom/sec/vip/amschaton/z;->O:I

    sub-int/2addr v0, v2

    int-to-short v0, v0

    iput-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    goto :goto_3

    .line 735
    :cond_7
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    sub-int v3, v0, v1

    .line 736
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    sub-int v4, v0, v1

    .line 738
    iget v0, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v0, v0

    sub-float v1, p1, v0

    .line 739
    iget v0, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v0, v0

    sub-float v0, p2, v0

    .line 740
    iget-object v7, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    move-result v7

    iput v7, p0, Lcom/sec/vip/amschaton/z;->N:I

    .line 741
    iget-object v7, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    move-result v7

    iput v7, p0, Lcom/sec/vip/amschaton/z;->O:I

    .line 742
    cmpg-float v7, v1, v2

    if-gez v7, :cond_9

    move v1, v2

    .line 748
    :cond_8
    :goto_5
    cmpg-float v7, v0, v2

    if-gez v7, :cond_a

    .line 754
    :goto_6
    int-to-float v0, v3

    add-float v3, v1, v0

    int-to-float v0, v4

    add-float v4, v2, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;->a(FFFFI)Z

    goto :goto_4

    .line 745
    :cond_9
    iget v7, p0, Lcom/sec/vip/amschaton/z;->N:I

    sub-int/2addr v7, v3

    int-to-float v7, v7

    cmpl-float v7, v1, v7

    if-lez v7, :cond_8

    .line 746
    iget v1, p0, Lcom/sec/vip/amschaton/z;->N:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    goto :goto_5

    .line 751
    :cond_a
    iget v2, p0, Lcom/sec/vip/amschaton/z;->O:I

    sub-int/2addr v2, v4

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_10

    .line 752
    iget v0, p0, Lcom/sec/vip/amschaton/z;->O:I

    sub-int/2addr v0, v4

    int-to-float v2, v0

    goto :goto_6

    .line 766
    :pswitch_4
    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v1, v2

    .line 767
    const/high16 v2, 0x40c00000    # 6.0f

    iget v3, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v2, v3

    .line 768
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v3, v3

    sub-float/2addr v3, v1

    sub-float/2addr v3, v2

    add-float/2addr v3, v7

    float-to-int v3, v3

    int-to-float v3, v3

    .line 769
    iget-object v4, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v4, v4

    add-float/2addr v4, v1

    add-float/2addr v2, v4

    add-float/2addr v2, v7

    float-to-int v2, v2

    int-to-float v2, v2

    .line 770
    const/4 v4, 0x2

    new-array v4, v4, [F

    .line 771
    aput p1, v4, v0

    .line 772
    aput p2, v4, v6

    .line 774
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 775
    new-instance v7, Landroid/graphics/RectF;

    iget-object v8, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v8

    iget-short v8, v8, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v9

    iget-short v9, v9, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v9, v9

    iget-object v10, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v10}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v10

    iget-short v10, v10, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v10, v10

    iget-object v11, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v11}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v11

    iget-short v11, v11, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v11, v11

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 777
    iget v8, p0, Lcom/sec/vip/amschaton/z;->e:I

    neg-int v8, v8

    int-to-float v8, v8

    invoke-virtual {v7}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    invoke-virtual {v7}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    invoke-virtual {v5, v8, v9, v7}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 778
    invoke-virtual {v5, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 780
    aget v5, v4, v0

    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v1

    if-gez v2, :cond_b

    aget v2, v4, v6

    sub-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v1, v2, v1

    if-gez v1, :cond_b

    move v1, v6

    :goto_7
    if-nez v1, :cond_1

    .line 781
    const/16 v1, 0x65

    invoke-virtual {p0, v1, v0}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    .line 782
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    goto/16 :goto_1

    :cond_b
    move v1, v0

    .line 780
    goto :goto_7

    .line 795
    :cond_c
    iget v1, p0, Lcom/sec/vip/amschaton/z;->B:I

    sparse-switch v1, :sswitch_data_0

    :cond_d
    :goto_8
    move v0, v6

    .line 819
    goto/16 :goto_0

    .line 798
    :sswitch_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/z;->H:Z

    if-eqz v0, :cond_e

    .line 799
    iget v0, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v1, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v1, v1

    sub-float v1, p2, v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/vip/amschaton/z;->a(FFI)Z

    .line 800
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->I:Z

    goto :goto_8

    .line 802
    :cond_e
    iget v0, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v1, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v1, v1

    sub-float v1, p2, v1

    const/16 v2, 0xff

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/vip/amschaton/z;->a(FFI)Z

    goto :goto_8

    .line 808
    :sswitch_1
    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/vip/amschaton/z;->d:F

    mul-float/2addr v1, v2

    .line 809
    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    sub-float/2addr v2, v1

    add-float/2addr v2, v7

    float-to-int v2, v2

    int-to-float v2, v2

    .line 810
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    add-float/2addr v3, v1

    add-float/2addr v3, v7

    float-to-int v3, v3

    int-to-float v3, v3

    .line 811
    sub-float/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v1

    if-gez v3, :cond_f

    sub-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v1, v2, v1

    if-gez v1, :cond_f

    move v1, v6

    :goto_9
    if-nez v1, :cond_d

    .line 813
    const/16 v1, 0x65

    invoke-virtual {p0, v1, v0}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    .line 814
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/z;->B:I

    goto :goto_8

    :cond_f
    move v1, v0

    .line 811
    goto :goto_9

    :cond_10
    move v2, v0

    goto/16 :goto_6

    .line 674
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 795
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x7 -> :sswitch_1
    .end sparse-switch
.end method

.method public f(FF)Z
    .locals 9

    .prologue
    const/16 v4, 0x65

    const/4 v8, 0x1

    const/16 v5, 0xff

    const/4 v3, 0x2

    const/4 v6, 0x0

    .line 835
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    move v0, v6

    .line 1071
    :goto_0
    return v0

    .line 841
    :cond_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/z;->am:Z

    if-eqz v0, :cond_12

    .line 842
    new-instance v0, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 843
    iget v1, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    .line 845
    iget v2, p0, Lcom/sec/vip/amschaton/z;->B:I

    packed-switch v2, :pswitch_data_0

    .line 1021
    :goto_1
    :pswitch_0
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->f:Z

    move v0, v6

    .line 1026
    goto :goto_0

    .line 847
    :pswitch_1
    iput v6, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 848
    sub-float v2, p1, v1

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 849
    sub-float v2, p2, v1

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 850
    add-float v2, p1, v1

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 851
    add-float/2addr v1, p2

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 852
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/z;->a(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    .line 853
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->f:Z

    goto :goto_0

    .line 857
    :pswitch_2
    iput v3, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 858
    iget-object v1, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    sub-int/2addr v1, v2

    .line 859
    iget-object v2, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    sub-int/2addr v2, v3

    .line 860
    iget-boolean v3, p0, Lcom/sec/vip/amschaton/z;->f:Z

    if-eqz v3, :cond_5

    .line 861
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/z;->c()Landroid/graphics/Point;

    move-result-object v3

    .line 862
    iget v4, v3, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    sub-float/2addr v4, p2

    float-to-double v7, v4

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    sub-float/2addr v3, p1

    float-to-double v3, v3

    invoke-static {v7, v8, v3, v4}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v3

    const-wide v7, 0x4056800000000000L    # 90.0

    sub-double/2addr v3, v7

    double-to-int v3, v3

    iput v3, p0, Lcom/sec/vip/amschaton/z;->e:I

    .line 864
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 865
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 869
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    iput v3, p0, Lcom/sec/vip/amschaton/z;->N:I

    .line 870
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    iput v3, p0, Lcom/sec/vip/amschaton/z;->O:I

    .line 872
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    if-gez v3, :cond_3

    .line 873
    iput-short v6, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 878
    :cond_1
    :goto_2
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    if-gez v3, :cond_4

    .line 879
    iput-short v6, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 885
    :cond_2
    :goto_3
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    add-int/2addr v1, v3

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 886
    iget-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    add-int/2addr v1, v2

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 917
    :goto_4
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/z;->I:Z

    if-nez v1, :cond_a

    .line 918
    iget-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;->a(FFFFI)Z

    goto/16 :goto_1

    .line 875
    :cond_3
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v4, p0, Lcom/sec/vip/amschaton/z;->N:I

    sub-int/2addr v4, v1

    if-le v3, v4, :cond_1

    .line 876
    iget v3, p0, Lcom/sec/vip/amschaton/z;->N:I

    sub-int/2addr v3, v1

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    goto :goto_2

    .line 881
    :cond_4
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v4, p0, Lcom/sec/vip/amschaton/z;->O:I

    sub-int/2addr v4, v2

    if-le v3, v4, :cond_2

    .line 882
    iget v3, p0, Lcom/sec/vip/amschaton/z;->O:I

    sub-int/2addr v3, v2

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    goto :goto_3

    .line 891
    :cond_5
    iget v3, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v3, v3

    sub-float v3, p1, v3

    float-to-int v3, v3

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 892
    iget v3, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v3, v3

    sub-float v3, p2, v3

    float-to-int v3, v3

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 896
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    iput v3, p0, Lcom/sec/vip/amschaton/z;->N:I

    .line 897
    iget-object v3, p0, Lcom/sec/vip/amschaton/z;->g:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    iput v3, p0, Lcom/sec/vip/amschaton/z;->O:I

    .line 899
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    if-gez v3, :cond_8

    .line 900
    iput-short v6, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 905
    :cond_6
    :goto_5
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    if-gez v3, :cond_9

    .line 906
    iput-short v6, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 912
    :cond_7
    :goto_6
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    add-int/2addr v1, v3

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 913
    iget-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    add-int/2addr v1, v2

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    goto :goto_4

    .line 902
    :cond_8
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v4, p0, Lcom/sec/vip/amschaton/z;->N:I

    sub-int/2addr v4, v1

    if-le v3, v4, :cond_6

    .line 903
    iget v3, p0, Lcom/sec/vip/amschaton/z;->N:I

    sub-int/2addr v3, v1

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    goto :goto_5

    .line 908
    :cond_9
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v4, p0, Lcom/sec/vip/amschaton/z;->O:I

    sub-int/2addr v4, v2

    if-le v3, v4, :cond_7

    .line 909
    iget v3, p0, Lcom/sec/vip/amschaton/z;->O:I

    sub-int/2addr v3, v2

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    goto :goto_6

    .line 921
    :cond_a
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->I:Z

    .line 922
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/z;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    .line 923
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->f:Z

    goto/16 :goto_0

    .line 929
    :pswitch_3
    const/16 v1, 0x66

    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    .line 930
    const/16 v1, 0x67

    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    .line 931
    const/16 v1, 0x68

    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    .line 932
    iput v3, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 933
    new-array v1, v3, [F

    .line 934
    aput p1, v1, v6

    .line 935
    aput p2, v1, v8

    .line 937
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 938
    iget v3, p0, Lcom/sec/vip/amschaton/z;->e:I

    neg-int v3, v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/z;->X:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget-object v7, p0, Lcom/sec/vip/amschaton/z;->X:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    invoke-virtual {v2, v3, v4, v7}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 939
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 940
    aget v2, v1, v6

    aget v1, v1, v8

    invoke-direct {p0, v2, v1}, Lcom/sec/vip/amschaton/z;->g(FF)Landroid/graphics/PointF;

    move-result-object v1

    .line 942
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/z;->I:Z

    if-nez v2, :cond_b

    .line 943
    iget v0, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/vip/amschaton/z;->a(FFI)Z

    goto/16 :goto_1

    .line 946
    :cond_b
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->I:Z

    .line 947
    iget v2, p0, Lcom/sec/vip/amschaton/z;->E:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 948
    iget v2, p0, Lcom/sec/vip/amschaton/z;->F:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 949
    iget v2, v1, Landroid/graphics/PointF;->x:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 950
    iget v2, v1, Landroid/graphics/PointF;->y:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 951
    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget v3, p0, Lcom/sec/vip/amschaton/z;->E:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_c

    .line 952
    iget v2, v1, Landroid/graphics/PointF;->x:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 953
    iget v2, p0, Lcom/sec/vip/amschaton/z;->E:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 955
    :cond_c
    iget v2, v1, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/sec/vip/amschaton/z;->F:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_d

    .line 956
    iget v1, v1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 957
    iget v1, p0, Lcom/sec/vip/amschaton/z;->F:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 959
    :cond_d
    iget-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 960
    iget-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 962
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    if-gez v3, :cond_10

    .line 963
    iput-short v6, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 968
    :cond_e
    :goto_7
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    if-gez v3, :cond_11

    .line 969
    iput-short v6, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 975
    :cond_f
    :goto_8
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    add-int/2addr v1, v3

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 976
    iget-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    add-int/2addr v1, v2

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 979
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/z;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    .line 980
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->f:Z

    goto/16 :goto_0

    .line 965
    :cond_10
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v4, p0, Lcom/sec/vip/amschaton/z;->N:I

    sub-int/2addr v4, v1

    if-le v3, v4, :cond_e

    .line 966
    iget v3, p0, Lcom/sec/vip/amschaton/z;->N:I

    sub-int/2addr v3, v1

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    goto :goto_7

    .line 971
    :cond_11
    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v4, p0, Lcom/sec/vip/amschaton/z;->O:I

    sub-int/2addr v4, v2

    if-le v3, v4, :cond_f

    .line 972
    iget v3, p0, Lcom/sec/vip/amschaton/z;->O:I

    sub-int/2addr v3, v2

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    goto :goto_8

    .line 1016
    :pswitch_4
    invoke-virtual {p0, v4, v6}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    .line 1017
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->S:Lcom/sec/vip/amschaton/v;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/v;->a()V

    goto/16 :goto_1

    .line 1029
    :cond_12
    new-instance v0, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 1030
    iget v1, p0, Lcom/sec/vip/amschaton/z;->B:I

    sparse-switch v1, :sswitch_data_0

    .line 1070
    :goto_9
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->f:Z

    move v0, v6

    .line 1071
    goto/16 :goto_0

    .line 1032
    :sswitch_0
    iput v6, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 1033
    iget v1, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v1, v1

    sub-float v1, p1, v1

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 1034
    iget v1, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v1, v1

    sub-float v1, p2, v1

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 1038
    iget v1, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v1, v1

    sub-float v1, p1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 1039
    iget v1, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v1, v1

    sub-float v1, p2, v1

    iget v2, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 1041
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/z;->a(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    .line 1042
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->f:Z

    goto/16 :goto_0

    .line 1045
    :sswitch_1
    iput v3, p0, Lcom/sec/vip/amschaton/z;->B:I

    .line 1046
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/z;->I:Z

    if-nez v1, :cond_13

    .line 1047
    iget v0, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v1, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v1, v1

    sub-float v1, p2, v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/vip/amschaton/z;->a(FFI)Z

    goto :goto_9

    .line 1050
    :cond_13
    iget v1, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v1, v1

    sub-float v1, p1, v1

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 1051
    iget v1, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v1, v1

    sub-float v1, p2, v1

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 1055
    iget v1, p0, Lcom/sec/vip/amschaton/z;->C:I

    int-to-float v1, v1

    sub-float v1, p1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 1056
    iget v1, p0, Lcom/sec/vip/amschaton/z;->D:I

    int-to-float v1, v1

    sub-float v1, p2, v1

    iget v2, p0, Lcom/sec/vip/amschaton/z;->ac:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 1058
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->I:Z

    .line 1059
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/z;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    .line 1060
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/z;->f:Z

    goto/16 :goto_0

    .line 1065
    :sswitch_2
    invoke-virtual {p0, v4, v6}, Lcom/sec/vip/amschaton/z;->a(IZ)V

    .line 1066
    iget-object v0, p0, Lcom/sec/vip/amschaton/z;->S:Lcom/sec/vip/amschaton/v;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/v;->a()V

    goto/16 :goto_9

    .line 845
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 1030
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x7 -> :sswitch_2
    .end sparse-switch
.end method
