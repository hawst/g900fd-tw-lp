.class public Lcom/sec/vip/amschaton/AMSFileListActivity;
.super Lcom/sec/vip/amschaton/AMSActivity;
.source "AMSFileListActivity.java"


# instance fields
.field private l:I

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;-><init>()V

    .line 46
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    .line 47
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->m:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->n:Z

    return-void
.end method

.method private a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 654
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd_HHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 655
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 656
    return-object v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 504
    iput p1, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    .line 505
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 512
    const-string v1, "AMS_ACTION"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 513
    const-class v1, Lcom/sec/vip/amschaton/BaseAMSActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 514
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 515
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 516
    const-string v1, "VIEWER_MODE"

    const/16 v2, 0x3e9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 517
    if-eqz p2, :cond_0

    .line 518
    const-string v1, "AMS_FILE_PATH"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 519
    const-string v1, "AMS_SAVE_FLAG"

    iget-boolean v2, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->m:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 522
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->startActivity(Landroid/content/Intent;)V

    .line 523
    return-void
.end method

.method private a(ILjava/lang/String;ZZZ)V
    .locals 3

    .prologue
    .line 425
    iput p1, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    .line 426
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 433
    const-class v1, Lcom/sec/vip/amschaton/BaseAMSActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 434
    const-string v1, "AMS_ACTION"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 435
    if-eqz p2, :cond_0

    .line 436
    const-string v1, "AMS_FILE_PATH"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 438
    :cond_0
    const-string v1, "AMS_SAVE_FLAG"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 439
    const-string v1, "AMS_EDIT_FLAG"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 440
    sget-boolean v1, Lcom/sec/vip/amschaton/AMSFileListActivity;->j:Z

    if-eqz v1, :cond_1

    .line 441
    const-string v1, "AMS_PEN_STATE_DEFAULT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 446
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->startActivity(Landroid/content/Intent;)V

    .line 447
    return-void

    .line 443
    :cond_1
    const-string v1, "AMS_PEN_STATE_DEFAULT"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 195
    if-nez p1, :cond_0

    .line 224
    :goto_0
    return-void

    .line 198
    :cond_0
    const-string v0, "ACTION"

    const/16 v1, 0x7d0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 201
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 203
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 206
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->c(Landroid/os/Bundle;)V

    goto :goto_0

    .line 209
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->d(Landroid/os/Bundle;)V

    goto :goto_0

    .line 212
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->e(Landroid/os/Bundle;)V

    goto :goto_0

    .line 215
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->f(Landroid/os/Bundle;)V

    goto :goto_0

    .line 218
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->g(Landroid/os/Bundle;)V

    goto :goto_0

    .line 221
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->h(Landroid/os/Bundle;)V

    goto :goto_0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(Ljava/lang/String;IZ)V
    .locals 3

    .prologue
    .line 479
    const/16 v0, 0x3ea

    iput v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    .line 480
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 487
    const-string v1, "AMS_ACTION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 489
    const-class v1, Lcom/sec/vip/amschaton/BaseAMSActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 492
    const-string v1, "AMS_DIRECT_PLAY"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 493
    if-eqz p1, :cond_0

    .line 494
    const-string v1, "AMS_FILE_PATH"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    :cond_0
    const/4 v1, -0x1

    if-le p2, v1, :cond_1

    .line 497
    const-string v1, "AMS_FILE_TYPE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 500
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->startActivity(Landroid/content/Intent;)V

    .line 501
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 5

    .prologue
    .line 616
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 618
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_2

    .line 621
    sget-boolean v0, Lcom/sec/vip/amschaton/AMSFileListActivity;->j:Z

    if-eqz v0, :cond_1

    .line 622
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSFileListActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "A"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string v4, ".jpg"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 627
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 630
    const v0, 0x7f0b00ec

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(I)V

    .line 631
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->finish()V

    .line 651
    :cond_0
    :goto_1
    return-void

    .line 624
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSFileListActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "A"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string v4, ".jpg"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 636
    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move-object v0, p2

    .line 640
    :cond_3
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[sendAMS] AMS_FILE_PATH : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 644
    const-string v2, "AMS_FILE_PATH"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 645
    const-string v0, "AMS_FILE_PATH_TO_MANAGE"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 646
    const-string v2, "AMS_WITH_TEXT"

    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 647
    const-string v0, "AMS_FILE_TYPE"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 648
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->setResult(ILandroid/content/Intent;)V

    .line 649
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->finish()V

    goto :goto_1

    .line 646
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    move-object v0, p1

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 669
    const/4 v0, 0x1

    .line 673
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 674
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v5, 0x0

    invoke-direct {v2, p2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 676
    const/16 v3, 0x400

    :try_start_2
    new-array v3, v3, [B

    .line 679
    :goto_0
    invoke-virtual {v4, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    .line 680
    if-gtz v5, :cond_2

    .line 685
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 695
    if-eqz v4, :cond_0

    .line 697
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8

    .line 703
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 705
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    .line 712
    :cond_1
    :goto_2
    return v0

    .line 683
    :cond_2
    const/4 v6, 0x0

    :try_start_5
    invoke-virtual {v2, v3, v6, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_b
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_0

    .line 686
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 687
    :goto_3
    :try_start_6
    const-string v4, "FileNotFoundException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 695
    if-eqz v3, :cond_3

    .line 697
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 703
    :cond_3
    :goto_4
    if-eqz v2, :cond_4

    .line 705
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :cond_4
    :goto_5
    move v0, v1

    .line 711
    goto :goto_2

    .line 690
    :catch_1
    move-exception v0

    move-object v2, v3

    move-object v4, v3

    .line 691
    :goto_6
    :try_start_9
    const-string v3, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 695
    if-eqz v4, :cond_5

    .line 697
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 703
    :cond_5
    :goto_7
    if-eqz v2, :cond_6

    .line 705
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    :cond_6
    :goto_8
    move v0, v1

    .line 711
    goto :goto_2

    .line 695
    :catchall_0
    move-exception v0

    move-object v2, v3

    move-object v4, v3

    :goto_9
    if-eqz v4, :cond_7

    .line 697
    :try_start_c
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2

    .line 703
    :cond_7
    :goto_a
    if-eqz v2, :cond_8

    .line 705
    :try_start_d
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3

    .line 695
    :cond_8
    :goto_b
    throw v0

    .line 698
    :catch_2
    move-exception v1

    .line 699
    const-string v3, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 706
    :catch_3
    move-exception v1

    .line 707
    const-string v2, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 698
    :catch_4
    move-exception v0

    .line 699
    const-string v3, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 706
    :catch_5
    move-exception v0

    .line 707
    const-string v2, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 698
    :catch_6
    move-exception v0

    .line 699
    const-string v3, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 706
    :catch_7
    move-exception v0

    .line 707
    const-string v2, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 698
    :catch_8
    move-exception v1

    .line 699
    const-string v3, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 706
    :catch_9
    move-exception v1

    .line 707
    const-string v2, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 695
    :catchall_1
    move-exception v0

    move-object v2, v3

    goto/16 :goto_9

    :catchall_2
    move-exception v0

    goto/16 :goto_9

    :catchall_3
    move-exception v0

    move-object v4, v3

    goto/16 :goto_9

    .line 690
    :catch_a
    move-exception v0

    move-object v2, v3

    goto/16 :goto_6

    :catch_b
    move-exception v0

    goto/16 :goto_6

    .line 686
    :catch_c
    move-exception v0

    move-object v2, v3

    goto/16 :goto_3

    :catch_d
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    goto/16 :goto_3
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 450
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    .line 451
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 466
    const-class v1, Lcom/sec/vip/amschaton/BaseAMSActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 472
    const-string v1, "AMS_ACTION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 473
    const-string v1, "AMS_TAB_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 475
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->startActivity(Landroid/content/Intent;)V

    .line 476
    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/16 v1, 0x3e8

    const/4 v3, 0x0

    .line 227
    iget v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    packed-switch v0, :pswitch_data_0

    .line 285
    const-string v0, "[doActionBack] invalid state"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :goto_0
    :pswitch_0
    return-void

    .line 229
    :pswitch_1
    sget-boolean v0, Lcom/sec/vip/amschaton/AMSFileListActivity;->j:Z

    if-eqz v0, :cond_0

    .line 230
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->finish()V

    goto :goto_0

    .line 232
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->b(I)V

    goto :goto_0

    .line 236
    :pswitch_2
    sget-boolean v0, Lcom/sec/vip/amschaton/AMSFileListActivity;->j:Z

    if-eqz v0, :cond_1

    .line 244
    const/4 v2, 0x0

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(ILjava/lang/String;ZZZ)V

    goto :goto_0

    .line 247
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->finish()V

    goto :goto_0

    .line 252
    :pswitch_3
    const-string v0, "AMS_FILE_TYPE"

    const/16 v2, 0x7d1

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 254
    const/16 v2, 0x7d0

    if-ne v0, v2, :cond_3

    .line 255
    const/16 v1, 0x3e9

    .line 259
    :cond_2
    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->b(I)V

    goto :goto_0

    .line 256
    :cond_3
    const/16 v2, 0x7d2

    if-ne v0, v2, :cond_2

    .line 257
    const/16 v1, 0x3ea

    goto :goto_1

    .line 263
    :pswitch_4
    const-string v0, "AMS_FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 264
    if-nez v6, :cond_4

    .line 265
    sget-object v6, Lcom/sec/vip/amschaton/AMSFileListActivity;->g:Ljava/lang/String;

    .line 267
    :cond_4
    iget-boolean v7, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->m:Z

    iget-boolean v8, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->n:Z

    move-object v4, p0

    move v5, v1

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(ILjava/lang/String;ZZZ)V

    goto :goto_0

    .line 271
    :pswitch_5
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->finish()V

    goto :goto_0

    .line 277
    :pswitch_6
    const-string v0, "AMS_FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 278
    if-nez v6, :cond_5

    .line 279
    sget-object v6, Lcom/sec/vip/amschaton/AMSFileListActivity;->g:Ljava/lang/String;

    .line 281
    :cond_5
    const/16 v5, 0x3ec

    iget-boolean v7, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->m:Z

    iget-boolean v8, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->n:Z

    move-object v4, p0

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(ILjava/lang/String;ZZZ)V

    goto :goto_0

    .line 227
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/16 v1, 0x3e8

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 291
    .line 292
    iget v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    packed-switch v0, :pswitch_data_0

    .line 314
    const-string v0, "[doActionCompose] invalid state"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    :goto_0
    return-void

    .line 295
    :pswitch_0
    const-string v0, "AMS_SAVE_FILE"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 296
    if-eqz v0, :cond_0

    .line 298
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->h()V

    .line 301
    :cond_0
    const-string v0, "AMS_FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 302
    const-string v0, "AMS_EDIT_FLAG"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->n:Z

    .line 303
    iget-boolean v4, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->n:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(ILjava/lang/String;ZZZ)V

    goto :goto_0

    .line 307
    :pswitch_1
    const-string v0, "AMS_FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 308
    const-string v0, "AMS_EDIT_FLAG"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->n:Z

    .line 309
    iget-boolean v4, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->n:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(ILjava/lang/String;ZZZ)V

    goto :goto_0

    .line 292
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 320
    iget v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    packed-switch v0, :pswitch_data_0

    .line 333
    const-string v0, "[doActionList] invalid state"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :goto_0
    return-void

    .line 323
    :pswitch_0
    const-string v0, "AMS_SAVE_FLAG"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->m:Z

    .line 326
    const-string v0, "AMS_TAB_ID"

    const/16 v1, 0x3e8

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 327
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->b(I)V

    goto :goto_0

    .line 320
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method

.method private e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 339
    iget v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    packed-switch v0, :pswitch_data_0

    .line 351
    const-string v0, "[doActionPlay] invalid state"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :goto_0
    return-void

    .line 342
    :pswitch_0
    const-string v0, "AMS_FILE_TYPE"

    const/16 v1, 0x7d1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 343
    const-string v1, "AMS_DIRECT_PLAY"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 344
    const-string v2, "AMS_FILE_PATH"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 345
    invoke-direct {p0, v2, v0, v1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    .line 339
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method

.method private f()V
    .locals 7

    .prologue
    .line 529
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 530
    :cond_0
    const-string v0, "[deleteTempFolder] External Storage Is Not Available or Writable!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    :cond_1
    return-void

    .line 533
    :cond_2
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSFileListActivity;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 534
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 535
    if-eqz v1, :cond_1

    .line 538
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[deleteTempFolder] fileList.length : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 540
    if-eqz v3, :cond_3

    .line 541
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/vip/amschaton/AMSFileListActivity;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v4

    .line 542
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/vip/amschaton/AMSFileListActivity;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private f(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 357
    iget v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    sparse-switch v0, :sswitch_data_0

    .line 381
    const-string v0, "[doActionPreview] invalid state"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :goto_0
    return-void

    .line 360
    :sswitch_0
    const-string v0, "AMS_SAVE_FLAG"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->m:Z

    .line 361
    const-string v0, "AMS_FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 362
    if-nez v0, :cond_0

    .line 363
    sget-object v0, Lcom/sec/vip/amschaton/AMSFileListActivity;->g:Ljava/lang/String;

    .line 365
    :cond_0
    const/16 v1, 0x3eb

    invoke-direct {p0, v1, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 369
    :sswitch_1
    const-string v0, "AMS_SAVE_FLAG"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->m:Z

    .line 370
    const-string v0, "AMS_FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 371
    if-nez v0, :cond_1

    .line 372
    sget-object v0, Lcom/sec/vip/amschaton/AMSFileListActivity;->g:Ljava/lang/String;

    .line 374
    :cond_1
    const/16 v1, 0x3ee

    invoke-direct {p0, v1, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 357
    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0x3ec -> :sswitch_1
    .end sparse-switch
.end method

.method private g()V
    .locals 13

    .prologue
    .line 551
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 552
    :cond_0
    const-string v0, "[deleteOldSentFile] External Storage Is Not Available or Writable!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    :cond_1
    return-void

    .line 555
    :cond_2
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSFileListActivity;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 556
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 557
    if-eqz v1, :cond_1

    .line 560
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[deleteOldSentFile] fileList.length : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 562
    const-wide/32 v4, 0x240c8400

    .line 563
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[deleteOldSentFile] currentTime : "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    array-length v6, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_1

    aget-object v7, v1, v0

    .line 565
    if-eqz v7, :cond_3

    .line 566
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/vip/amschaton/AMSFileListActivity;->d:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 567
    invoke-virtual {v8}, Ljava/io/File;->lastModified()J

    move-result-wide v9

    .line 568
    const-wide/16 v11, 0x0

    cmp-long v7, v9, v11

    if-gtz v7, :cond_4

    .line 569
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v7

    .line 571
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    :cond_3
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 574
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[deleteOldSentFile] lastModifiedTime : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v11}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    sub-long v9, v2, v9

    cmp-long v7, v9, v4

    if-lez v7, :cond_3

    .line 577
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v7

    .line 579
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private g(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 387
    iget v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    packed-switch v0, :pswitch_data_0

    .line 398
    const-string v0, "[doActionSend] invalid state"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :goto_0
    return-void

    .line 395
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/AMSFileListActivity;->i(Landroid/os/Bundle;)V

    goto :goto_0

    .line 387
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private h()V
    .locals 4

    .prologue
    .line 716
    const-string v0, "A"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v3, ""

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 717
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/vip/amschaton/AMSFileListActivity;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 718
    sget-object v1, Lcom/sec/vip/amschaton/AMSFileListActivity;->g:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 721
    const v0, 0x7f0b00ec

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(I)V

    .line 722
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->finish()V

    .line 725
    :cond_0
    return-void
.end method

.method private h(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->finish()V

    .line 422
    return-void
.end method

.method private i(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 586
    const-string v0, "AMS_FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 587
    const-string v1, "IMG_FILE_PATH"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 588
    const-string v2, "AMS_SEND_METHOD"

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 589
    const-string v3, "AMS_FILE_TYPE"

    const/16 v4, 0x7d1

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 604
    invoke-direct {p0, v0, v1, v3, v2}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 605
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 55
    const-string v0, "[onCreate]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->requestWindowFeature(I)Z

    .line 58
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/AMSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->setContentView(I)V

    .line 61
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 62
    :cond_0
    const-string v0, "External Storage Is Not Available or Writable!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const v0, 0x7f0b00ed

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(I)V

    .line 66
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->finish()V

    .line 144
    :goto_0
    return-void

    .line 70
    :cond_1
    if-eqz p1, :cond_2

    .line 71
    const-string v0, "[onCreate] savedInstanceState is not null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 76
    :cond_2
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->f()V

    .line 79
    sget-boolean v0, Lcom/sec/vip/amschaton/AMSFileListActivity;->j:Z

    if-nez v0, :cond_3

    .line 80
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->g()V

    .line 85
    :cond_3
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    .line 87
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 100
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 101
    if-eqz v1, :cond_5

    .line 102
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 103
    const-string v2, "AMS_START_STATE"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    .line 104
    iget v2, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    packed-switch v2, :pswitch_data_0

    .line 123
    :pswitch_0
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 124
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 125
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 139
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 140
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 107
    :pswitch_1
    const-class v1, Lcom/sec/vip/amschaton/BaseAMSActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_1

    .line 110
    :pswitch_2
    const-class v1, Lcom/sec/vip/amschaton/BaseAMSActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 111
    const-string v1, "AMS_ACTION"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 114
    :pswitch_3
    const-string v2, "AMS_FILE_PATH"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 115
    if-eqz v1, :cond_4

    .line 116
    const-string v2, "AMS_FILE_PATH"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    :cond_4
    const-string v1, "AMS_ACTION"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 119
    const-string v1, "VIEWER_MODE"

    const/16 v2, 0x3eb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 120
    const-class v1, Lcom/sec/vip/amschaton/BaseAMSActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_1

    .line 128
    :cond_5
    const/16 v1, 0x3e9

    iput v1, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    .line 130
    const-class v1, Lcom/sec/vip/amschaton/BaseAMSActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_1

    .line 142
    :cond_6
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->finish()V

    goto/16 :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 148
    const-string v0, "[onDestroy]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-super {p0}, Lcom/sec/vip/amschaton/AMSActivity;->onDestroy()V

    .line 165
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onNewIntent] mCurrentState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/AMSActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 172
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 173
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/AMSFileListActivity;->a(Landroid/os/Bundle;)V

    .line 174
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 178
    const-string v0, "[onRestoreInstanceState]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    if-eqz p1, :cond_0

    .line 180
    const-string v0, "SI_CURRENT_STATE"

    const/16 v1, 0x3e8

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    .line 183
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/AMSActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 184
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 188
    const-string v0, "[onSaveInstanceState]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v0, "SI_CURRENT_STATE"

    iget v1, p0, Lcom/sec/vip/amschaton/AMSFileListActivity;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 191
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/AMSActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 192
    return-void
.end method
