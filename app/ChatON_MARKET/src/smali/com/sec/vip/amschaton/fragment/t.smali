.class public Lcom/sec/vip/amschaton/fragment/t;
.super Landroid/widget/BaseAdapter;
.source "AMSBgSelectionFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

.field private b:[Z

.field private c:Landroid/os/Handler;

.field private d:Lcom/sec/vip/amschaton/fragment/n;

.field private e:Lcom/sec/vip/amschaton/fragment/n;

.field private final f:Landroid/view/View$OnClickListener;

.field private final g:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1162
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1148
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    .line 1149
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->c:Landroid/os/Handler;

    .line 1151
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->d:Lcom/sec/vip/amschaton/fragment/n;

    .line 1152
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->e:Lcom/sec/vip/amschaton/fragment/n;

    .line 1372
    new-instance v0, Lcom/sec/vip/amschaton/fragment/u;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/u;-><init>(Lcom/sec/vip/amschaton/fragment/t;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->f:Landroid/view/View$OnClickListener;

    .line 1390
    new-instance v0, Lcom/sec/vip/amschaton/fragment/v;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/v;-><init>(Lcom/sec/vip/amschaton/fragment/t;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->g:Landroid/view/View$OnClickListener;

    .line 1163
    invoke-static {p1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    .line 1164
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/t;->a(Z)V

    .line 1166
    new-instance v0, Lcom/sec/vip/amschaton/fragment/n;

    const/16 v1, 0x1388

    invoke-direct {v0, p1, p2, v1}, Lcom/sec/vip/amschaton/fragment/n;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->d:Lcom/sec/vip/amschaton/fragment/n;

    .line 1167
    new-instance v0, Lcom/sec/vip/amschaton/fragment/n;

    const/16 v1, 0x1389

    invoke-direct {v0, p1, p2, v1}, Lcom/sec/vip/amschaton/fragment/n;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->e:Lcom/sec/vip/amschaton/fragment/n;

    .line 1168
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/t;)[Z
    .locals 1

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    return-object v0
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/t;)V
    .locals 0

    .prologue
    .line 1145
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/t;->c()V

    return-void
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1403
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1420
    :goto_0
    return-void

    .line 1406
    :cond_0
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 1407
    const/16 v0, 0x7d0

    iput v0, v5, Landroid/os/Message;->what:I

    move v0, v1

    move v2, v3

    move v4, v1

    .line 1410
    :goto_1
    iget-object v6, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    array-length v6, v6

    if-ge v0, v6, :cond_2

    .line 1411
    iget-object v6, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    aget-boolean v6, v6, v0

    if-eqz v6, :cond_1

    move v4, v3

    .line 1410
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    .line 1414
    goto :goto_2

    .line 1417
    :cond_2
    if-eqz v4, :cond_3

    move v0, v3

    :goto_3
    iput v0, v5, Landroid/os/Message;->arg1:I

    .line 1418
    if-eqz v2, :cond_4

    :goto_4
    iput v3, v5, Landroid/os/Message;->arg2:I

    .line 1419
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->c:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1417
    goto :goto_3

    :cond_4
    move v3, v1

    .line 1418
    goto :goto_4
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 1342
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    if-nez v0, :cond_1

    .line 1351
    :cond_0
    :goto_0
    return-void

    .line 1345
    :cond_1
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 1348
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    aget-boolean v0, v0, p1

    .line 1349
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    aput-boolean v0, v1, p1

    .line 1350
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/t;->c()V

    goto :goto_0

    .line 1349
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1321
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/t;->c:Landroid/os/Handler;

    .line 1322
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    if-nez v0, :cond_0

    .line 1339
    :goto_0
    return-void

    .line 1335
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1336
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    aput-boolean p1, v1, v0

    .line 1335
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1338
    :cond_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/t;->c()V

    goto :goto_0
.end method

.method public a()[Z
    .locals 1

    .prologue
    .line 1328
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1359
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/t;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x3

    .line 1361
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1362
    const/16 v2, 0x7d1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1363
    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 1364
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->c:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1366
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/t;->notifyDataSetChanged()V

    .line 1367
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    const/16 v1, 0xbba

    if-ne v0, v1, :cond_0

    .line 1299
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, -0x1

    .line 1302
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x3

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1309
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 1314
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f020080

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 1190
    if-nez p2, :cond_0

    .line 1191
    new-instance v1, Lcom/sec/vip/amschaton/fragment/w;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/sec/vip/amschaton/fragment/w;-><init>(Lcom/sec/vip/amschaton/fragment/t;Lcom/sec/vip/amschaton/fragment/a;)V

    .line 1192
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1193
    const v2, 0x7f030015

    invoke-virtual {v0, v2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1195
    const v0, 0x7f070066

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v1, Lcom/sec/vip/amschaton/fragment/w;->a:Landroid/widget/RelativeLayout;

    .line 1196
    const v0, 0x7f07006b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    .line 1197
    const v0, 0x7f070067

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/sec/vip/amschaton/fragment/w;->c:Landroid/widget/CheckBox;

    .line 1198
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v1

    .line 1204
    :goto_0
    iget-object v0, v2, Lcom/sec/vip/amschaton/fragment/w;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1205
    iget-object v0, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1206
    iget-object v0, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1207
    iget-object v0, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1208
    const v0, 0x7f07006c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1209
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1210
    const v1, 0x7f07006d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1211
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1215
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v3}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v3

    const/16 v4, 0xbba

    if-ne v3, v4, :cond_4

    .line 1216
    if-nez p1, :cond_1

    .line 1218
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1219
    iget-object v0, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1292
    :goto_1
    return-object p2

    .line 1200
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/w;

    move-object v2, v0

    goto :goto_0

    .line 1221
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-ge p1, v0, :cond_2

    .line 1223
    add-int/lit8 v0, p1, -0x1

    .line 1224
    iget-object v1, v2, Lcom/sec/vip/amschaton/fragment/w;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1225
    iget-object v1, v2, Lcom/sec/vip/amschaton/fragment/w;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 1226
    iget-object v1, v2, Lcom/sec/vip/amschaton/fragment/w;->a:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/t;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1227
    iget-object v1, v2, Lcom/sec/vip/amschaton/fragment/w;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setId(I)V

    .line 1228
    iget-object v1, v2, Lcom/sec/vip/amschaton/fragment/w;->c:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/t;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1229
    iget-object v1, v2, Lcom/sec/vip/amschaton/fragment/w;->c:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/t;->b:[Z

    aget-boolean v3, v3, v0

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1230
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->d:Lcom/sec/vip/amschaton/fragment/n;

    iget-object v2, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0, v2, p1}, Lcom/sec/vip/amschaton/fragment/n;->a(ILandroid/widget/ImageView;I)V

    goto :goto_1

    .line 1232
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    if-ge p1, v0, :cond_3

    .line 1234
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    sub-int v0, p1, v0

    add-int/lit8 v0, v0, -0x1

    .line 1235
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->e:Lcom/sec/vip/amschaton/fragment/n;

    iget-object v2, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0, v2, p1}, Lcom/sec/vip/amschaton/fragment/n;->a(ILandroid/widget/ImageView;I)V

    goto :goto_1

    .line 1239
    :cond_3
    iget-object v0, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 1242
    :cond_4
    if-nez p1, :cond_5

    .line 1244
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1245
    iget-object v0, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1247
    :cond_5
    const/4 v0, 0x1

    if-ne p1, v0, :cond_6

    .line 1249
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1250
    iget-object v0, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 1252
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    if-ge p1, v0, :cond_7

    .line 1254
    add-int/lit8 v0, p1, -0x2

    .line 1255
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->d:Lcom/sec/vip/amschaton/fragment/n;

    iget-object v2, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0, v2, p1}, Lcom/sec/vip/amschaton/fragment/n;->a(ILandroid/widget/ImageView;I)V

    goto/16 :goto_1

    .line 1257
    :cond_7
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    if-ge p1, v0, :cond_8

    .line 1259
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/t;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    sub-int v0, p1, v0

    add-int/lit8 v0, v0, -0x2

    .line 1260
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/t;->e:Lcom/sec/vip/amschaton/fragment/n;

    iget-object v2, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0, v2, p1}, Lcom/sec/vip/amschaton/fragment/n;->a(ILandroid/widget/ImageView;I)V

    goto/16 :goto_1

    .line 1264
    :cond_8
    iget-object v0, v2, Lcom/sec/vip/amschaton/fragment/w;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1
.end method
