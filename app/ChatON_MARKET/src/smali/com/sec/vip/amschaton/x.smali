.class public Lcom/sec/vip/amschaton/x;
.super Lcom/sec/vip/amschaton/u;
.source "AMSObjectLine.java"


# instance fields
.field private final Z:I

.field private final aa:I

.field private ab:F

.field private ac:Z

.field private ad:Z

.field private ae:Landroid/graphics/Path;

.field private af:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/vip/amschaton/y;",
            ">;"
        }
    .end annotation
.end field

.field private ag:I

.field private ah:Z

.field private ai:F


# direct methods
.method public constructor <init>(III)V
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 91
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/u;-><init>(III)V

    .line 50
    iput v5, p0, Lcom/sec/vip/amschaton/x;->Z:I

    .line 51
    iput v6, p0, Lcom/sec/vip/amschaton/x;->aa:I

    .line 56
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    .line 57
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/x;->ad:Z

    .line 62
    iput v5, p0, Lcom/sec/vip/amschaton/x;->ag:I

    .line 64
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/x;->ah:Z

    .line 66
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/vip/amschaton/x;->ai:F

    .line 92
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    .line 93
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/x;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    int-to-float v1, p3

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 95
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 96
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setDither(Z)V

    .line 97
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 98
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 99
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 100
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    .line 102
    packed-switch p1, :pswitch_data_0

    .line 134
    :goto_0
    return-void

    .line 104
    :pswitch_0
    new-instance v0, Landroid/graphics/BlurMaskFilter;

    const/high16 v1, 0x40400000    # 3.0f

    sget-object v2, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v0, v1, v2}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    .line 105
    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 106
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    goto :goto_0

    .line 109
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 110
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    goto :goto_0

    .line 113
    :pswitch_2
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->b()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_0

    .line 115
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    .line 116
    iget v0, p0, Lcom/sec/vip/amschaton/x;->ai:F

    div-float/2addr v0, v3

    iput v0, p0, Lcom/sec/vip/amschaton/x;->ai:F

    .line 121
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x2

    new-array v2, v2, [F

    iget v3, p0, Lcom/sec/vip/amschaton/x;->c:I

    add-int/lit8 v3, v3, 0x1

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/x;->d:F

    mul-float/2addr v3, v4

    aput v3, v2, v5

    iget v3, p0, Lcom/sec/vip/amschaton/x;->ai:F

    iget v4, p0, Lcom/sec/vip/amschaton/x;->c:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/vip/amschaton/x;->d:F

    mul-float/2addr v3, v4

    aput v3, v2, v6

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 122
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    goto :goto_0

    .line 118
    :cond_1
    iget v1, p0, Lcom/sec/vip/amschaton/x;->ai:F

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    div-float/2addr v0, v3

    iput v0, p0, Lcom/sec/vip/amschaton/x;->ai:F

    goto :goto_1

    .line 125
    :pswitch_3
    new-instance v0, Landroid/graphics/EmbossMaskFilter;

    const/4 v1, 0x3

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const v2, 0x3ecccccd    # 0.4f

    const/high16 v3, 0x40c00000    # 6.0f

    const/high16 v4, 0x40600000    # 3.5f

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/EmbossMaskFilter;-><init>([FFFF)V

    .line 126
    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 127
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    goto :goto_0

    .line 130
    :pswitch_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 131
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    goto/16 :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch

    .line 125
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private d(F)V
    .locals 7

    .prologue
    const/high16 v6, 0x42c80000    # 100.0f

    .line 137
    const/high16 v0, 0x45800000    # 4096.0f

    div-float v0, p1, v0

    .line 138
    iget v1, p0, Lcom/sec/vip/amschaton/x;->ab:F

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v1, v2

    sget v2, Lcom/sec/vip/amschaton/x;->U:F

    sget v3, Lcom/sec/vip/amschaton/x;->U:F

    sub-float v3, v6, v3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    div-float/2addr v0, v6

    mul-float/2addr v0, v1

    .line 142
    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 143
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/x;->ab:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 147
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 237
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/x;->ag:I

    .line 238
    return-void
.end method

.method public a(F)V
    .locals 4

    .prologue
    .line 157
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 158
    iget v0, p0, Lcom/sec/vip/amschaton/x;->c:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/x;->ab:F

    .line 159
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/x;->ab:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 160
    return-void
.end method

.method public a(FF)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 174
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->g()F

    move-result v0

    const/4 v2, 0x0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    .line 176
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    if-eqz v0, :cond_0

    .line 177
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/x;->ad:Z

    .line 178
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->h()V

    .line 180
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->j()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/x;->d(F)V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 184
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 185
    iput p1, p0, Lcom/sec/vip/amschaton/x;->E:F

    .line 186
    iput p2, p0, Lcom/sec/vip/amschaton/x;->F:F

    .line 187
    iput v1, p0, Lcom/sec/vip/amschaton/x;->ag:I

    .line 188
    return-void

    :cond_1
    move v0, v1

    .line 174
    goto :goto_0
.end method

.method public a(FFFFI)Z
    .locals 6

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->g:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 328
    const/4 v0, 0x1

    return v0
.end method

.method public a(FFI)Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/vip/amschaton/x;->a(FFIZ)Z

    move-result v0

    return v0
.end method

.method public a(FFIF)Z
    .locals 6

    .prologue
    .line 242
    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/x;->a(FFIFZ)Z

    move-result v0

    return v0
.end method

.method public a(FFIFZ)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 247
    const/4 v0, 0x0

    cmpg-float v0, p4, v0

    if-gez v0, :cond_0

    .line 248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/x;->ad:Z

    .line 253
    :goto_0
    invoke-virtual {p0, p1, p2, p3, p5}, Lcom/sec/vip/amschaton/x;->a(FFIZ)Z

    .line 254
    return v1

    .line 250
    :cond_0
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/x;->ad:Z

    .line 251
    invoke-virtual {p0, p4}, Lcom/sec/vip/amschaton/x;->b(F)V

    goto :goto_0
.end method

.method public a(FFIZ)Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 276
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/x;->ad:Z

    if-eqz v0, :cond_0

    .line 281
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->j()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/x;->d(F)V

    .line 284
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/vip/amschaton/x;->E:F

    iget v2, p0, Lcom/sec/vip/amschaton/x;->F:F

    iget v3, p0, Lcom/sec/vip/amschaton/x;->E:F

    add-float/2addr v3, p1

    div-float/2addr v3, v5

    iget v4, p0, Lcom/sec/vip/amschaton/x;->F:F

    add-float/2addr v4, p2

    div-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 292
    :goto_0
    iput p1, p0, Lcom/sec/vip/amschaton/x;->E:F

    .line 293
    iput p2, p0, Lcom/sec/vip/amschaton/x;->F:F

    .line 294
    if-nez p3, :cond_1

    .line 307
    :goto_1
    return v9

    .line 288
    :cond_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/x;->l()V

    .line 289
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/vip/amschaton/x;->E:F

    iget v2, p0, Lcom/sec/vip/amschaton/x;->F:F

    iget v3, p0, Lcom/sec/vip/amschaton/x;->E:F

    add-float/2addr v3, p1

    div-float/2addr v3, v5

    iget v4, p0, Lcom/sec/vip/amschaton/x;->F:F

    add-float/2addr v4, p2

    div-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_0

    .line 297
    :cond_1
    iget v0, p0, Lcom/sec/vip/amschaton/x;->a:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    .line 298
    if-eqz p4, :cond_2

    .line 299
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v6, v6, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 301
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 303
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->j:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 304
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->k:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/vip/amschaton/x;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/x;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v7, v7, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/sec/vip/amschaton/x;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/x;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v7, v7, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1, v2, v3, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 305
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v6, v6, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public b(FF)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 200
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 203
    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 205
    iget v1, p0, Lcom/sec/vip/amschaton/x;->a:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 206
    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/x;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v7, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 208
    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 227
    :goto_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 228
    iput v6, p0, Lcom/sec/vip/amschaton/x;->ag:I

    .line 229
    return-void

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 216
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->j:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 222
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->k:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/vip/amschaton/x;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/x;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v6, v6, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/sec/vip/amschaton/x;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/x;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1, v2, v3, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 225
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v7, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 219
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->j:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/x;->ae:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/vip/amschaton/x;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 461
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/x;->ah:Z

    .line 462
    return-void
.end method

.method public d(FF)Z
    .locals 3

    .prologue
    .line 362
    float-to-int v0, p1

    int-to-short v0, v0

    int-to-float v0, v0

    float-to-int v1, p2

    int-to-short v1, v1

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/x;->a(FF)V

    .line 363
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    .line 364
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/vip/amschaton/y;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->g()F

    move-result v2

    invoke-direct {v1, p0, p1, p2, v2}, Lcom/sec/vip/amschaton/y;-><init>(Lcom/sec/vip/amschaton/x;FFF)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    const/4 v0, 0x1

    return v0
.end method

.method public e(FF)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 380
    iget v2, p0, Lcom/sec/vip/amschaton/x;->ag:I

    if-nez v2, :cond_0

    .line 394
    :goto_0
    return v0

    .line 389
    :cond_0
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    if-eqz v2, :cond_1

    .line 390
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->g()F

    move-result v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/x;->ad:Z

    .line 392
    :cond_1
    float-to-int v0, p1

    int-to-short v0, v0

    int-to-float v0, v0

    float-to-int v2, p2

    int-to-short v2, v2

    int-to-float v2, v2

    const/16 v3, 0xff

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/vip/amschaton/x;->a(FFI)Z

    .line 393
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/vip/amschaton/y;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->g()F

    move-result v3

    invoke-direct {v2, p0, p1, p2, v3}, Lcom/sec/vip/amschaton/y;-><init>(Lcom/sec/vip/amschaton/x;FFF)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 394
    goto :goto_0

    :cond_2
    move v0, v1

    .line 390
    goto :goto_1
.end method

.method public f(FF)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 409
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->y:Lcom/sec/amsoma/AMSLibs;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/vip/amschaton/x;->ag:I

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 455
    :goto_0
    return v0

    .line 414
    :cond_1
    iget v0, p0, Lcom/sec/vip/amschaton/x;->ag:I

    if-ne v0, v2, :cond_2

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/x;->ah:Z

    if-eqz v0, :cond_2

    .line 415
    add-float v0, p1, v4

    add-float v3, p2, v4

    invoke-virtual {p0, v0, v3}, Lcom/sec/vip/amschaton/x;->e(FF)Z

    .line 416
    add-float/2addr p1, v4

    .line 417
    add-float/2addr p2, v4

    .line 418
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/vip/amschaton/y;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->g()F

    move-result v4

    invoke-direct {v3, p0, p1, p2, v4}, Lcom/sec/vip/amschaton/y;-><init>(Lcom/sec/vip/amschaton/x;FFF)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    :cond_2
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/x;->ah:Z

    .line 422
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/y;

    iget v0, v0, Lcom/sec/vip/amschaton/y;->a:F

    .line 423
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/y;

    iget v0, v0, Lcom/sec/vip/amschaton/y;->b:F

    .line 424
    float-to-int v0, p1

    int-to-short v0, v0

    int-to-float v0, v0

    float-to-int v3, p2

    int-to-short v3, v3

    int-to-float v3, v3

    invoke-virtual {p0, v0, v3}, Lcom/sec/vip/amschaton/x;->b(FF)V

    .line 425
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/x;->ac:Z

    if-eqz v0, :cond_3

    .line 426
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->i()V

    .line 427
    invoke-direct {p0}, Lcom/sec/vip/amschaton/x;->l()V

    .line 429
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/vip/amschaton/y;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->g()F

    move-result v4

    invoke-direct {v3, p0, p1, p2, v4}, Lcom/sec/vip/amschaton/y;-><init>(Lcom/sec/vip/amschaton/x;FFF)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_4

    move v0, v1

    .line 431
    goto :goto_0

    .line 433
    :cond_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->y:Lcom/sec/amsoma/AMSLibs;

    iget v3, p0, Lcom/sec/vip/amschaton/x;->z:I

    invoke-virtual {v0, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_EncodeStrokeStart(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 434
    const-string v0, "VipAMS_EncodeStrokeStart failed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    iput v1, p0, Lcom/sec/vip/amschaton/x;->ag:I

    move v0, v1

    .line 436
    goto/16 :goto_0

    .line 438
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v1

    .line 439
    :goto_1
    if-ge v3, v4, :cond_6

    .line 440
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/y;

    iget v5, v0, Lcom/sec/vip/amschaton/y;->a:F

    .line 441
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/y;

    iget v6, v0, Lcom/sec/vip/amschaton/y;->b:F

    .line 442
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->af:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/y;

    iget v0, v0, Lcom/sec/vip/amschaton/y;->c:F

    .line 443
    iget-object v7, p0, Lcom/sec/vip/amschaton/x;->y:Lcom/sec/amsoma/AMSLibs;

    iget v8, p0, Lcom/sec/vip/amschaton/x;->z:I

    float-to-int v5, v5

    int-to-short v5, v5

    float-to-int v6, v6

    int-to-short v6, v6

    float-to-int v0, v0

    int-to-short v0, v0

    invoke-virtual {v7, v8, v5, v6, v0}, Lcom/sec/amsoma/AMSLibs;->VipAMS_EncodePointData(ISSS)Z

    move-result v0

    if-nez v0, :cond_7

    .line 444
    const-string v0, "VipAMS_EncodePointData failed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iput v1, p0, Lcom/sec/vip/amschaton/x;->ag:I

    .line 450
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/amschaton/x;->y:Lcom/sec/amsoma/AMSLibs;

    iget v3, p0, Lcom/sec/vip/amschaton/x;->z:I

    invoke-virtual {v0, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_EncodeStrokeEnd(I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 451
    const-string v0, "VipAMS_EncodeStrokeEnd failed"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/x;->a()V

    move v0, v1

    .line 453
    goto/16 :goto_0

    .line 439
    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_8
    move v0, v2

    .line 455
    goto/16 :goto_0
.end method
