.class public Lcom/sec/vip/amschaton/bl;
.super Ljava/lang/Object;
.source "TextInputLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/text/TextWatcher;

.field private b:Landroid/content/Context;

.field private c:Landroid/widget/TextView;

.field private d:Lcom/sec/vip/amschaton/AMSEditText;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Landroid/widget/Toast;

.field private h:Lcom/sec/vip/amschaton/bi;

.field private i:Lcom/sec/vip/amschaton/bp;

.field private j:Lcom/sec/vip/amschaton/bn;

.field private k:Lcom/sec/vip/amschaton/bo;


# direct methods
.method public constructor <init>(Landroid/widget/RelativeLayout;Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/sec/vip/amschaton/bl;->c:Landroid/widget/TextView;

    .line 40
    iput-object v1, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    .line 41
    iput-object v1, p0, Lcom/sec/vip/amschaton/bl;->e:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/bl;->f:I

    .line 43
    iput-object v1, p0, Lcom/sec/vip/amschaton/bl;->g:Landroid/widget/Toast;

    .line 45
    iput-object v1, p0, Lcom/sec/vip/amschaton/bl;->h:Lcom/sec/vip/amschaton/bi;

    .line 51
    iput-object v1, p0, Lcom/sec/vip/amschaton/bl;->i:Lcom/sec/vip/amschaton/bp;

    .line 53
    iput-object v1, p0, Lcom/sec/vip/amschaton/bl;->j:Lcom/sec/vip/amschaton/bn;

    .line 54
    iput-object v1, p0, Lcom/sec/vip/amschaton/bl;->k:Lcom/sec/vip/amschaton/bo;

    .line 59
    new-instance v0, Lcom/sec/vip/amschaton/bm;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/bm;-><init>(Lcom/sec/vip/amschaton/bl;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/bl;->a:Landroid/text/TextWatcher;

    .line 158
    iput-object p2, p0, Lcom/sec/vip/amschaton/bl;->b:Landroid/content/Context;

    .line 160
    const v0, 0x7f07023d

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/AMSEditText;

    iput-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    .line 161
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/bl;->a(Landroid/widget/RelativeLayout;Landroid/content/Context;I)V

    .line 162
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/bl;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/vip/amschaton/bl;->g:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/bl;)Lcom/sec/vip/amschaton/bn;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->j:Lcom/sec/vip/amschaton/bn;

    return-object v0
.end method

.method private a(Landroid/widget/RelativeLayout;Landroid/content/Context;I)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 171
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    const v1, 0x10000006

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSEditText;->setImeOptions(I)V

    .line 175
    const v0, 0x7f07023e

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/bl;->c:Landroid/widget/TextView;

    .line 176
    if-lez p3, :cond_0

    .line 177
    iput p3, p0, Lcom/sec/vip/amschaton/bl;->f:I

    .line 178
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bl;->a:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 179
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->c:Landroid/widget/TextView;

    const-string v1, "(%d/%d)"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    invoke-virtual {v3}, Lcom/sec/vip/amschaton/AMSEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/sec/vip/amschaton/bl;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSEditText;->requestFocus()Z

    .line 185
    new-instance v0, Lcom/sec/vip/amschaton/bo;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bl;->e:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/vip/amschaton/bo;-><init>(Lcom/sec/vip/amschaton/bl;Ljava/lang/String;Lcom/sec/vip/amschaton/bm;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/bl;->k:Lcom/sec/vip/amschaton/bo;

    .line 186
    new-instance v0, Lcom/sec/vip/amschaton/bn;

    iget v1, p0, Lcom/sec/vip/amschaton/bl;->f:I

    const-string v2, "KSC5601"

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/vip/amschaton/bn;-><init>(Lcom/sec/vip/amschaton/bl;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/bl;->j:Lcom/sec/vip/amschaton/bn;

    .line 187
    new-array v0, v6, [Landroid/text/InputFilter;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bl;->k:Lcom/sec/vip/amschaton/bo;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/sec/vip/amschaton/bl;->j:Lcom/sec/vip/amschaton/bn;

    aput-object v1, v0, v5

    .line 188
    iget-object v1, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/AMSEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 189
    return-void
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/bl;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/vip/amschaton/bl;->f:I

    return v0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/bl;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 195
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->i:Lcom/sec/vip/amschaton/bp;

    if-nez v0, :cond_0

    .line 211
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 200
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    move v0, v1

    .line 202
    :goto_1
    if-ge v0, v4, :cond_3

    .line 203
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x20

    if-eq v5, v6, :cond_1

    move v0, v1

    .line 208
    :goto_2
    iget-object v3, p0, Lcom/sec/vip/amschaton/bl;->i:Lcom/sec/vip/amschaton/bp;

    if-lez v4, :cond_2

    if-nez v0, :cond_2

    :goto_3
    invoke-interface {v3, v2}, Lcom/sec/vip/amschaton/bp;->a(Z)V

    goto :goto_0

    .line 202
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v2, v1

    .line 208
    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/bl;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/vip/amschaton/bl;->d()V

    return-void
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/bl;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->g:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/bl;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/sec/vip/amschaton/bi;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/vip/amschaton/bl;->h:Lcom/sec/vip/amschaton/bi;

    .line 230
    return-void
.end method

.method public a(Lcom/sec/vip/amschaton/bp;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/vip/amschaton/bl;->i:Lcom/sec/vip/amschaton/bp;

    .line 234
    invoke-direct {p0}, Lcom/sec/vip/amschaton/bl;->d()V

    .line 235
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/AMSEditText;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/AMSEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSEditText;->setSelection(I)V

    .line 222
    return-void
.end method

.method public b()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSEditText;->setText(Ljava/lang/CharSequence;)V

    .line 280
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 247
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->d:Lcom/sec/vip/amschaton/AMSEditText;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->h:Lcom/sec/vip/amschaton/bi;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/vip/amschaton/bl;->h:Lcom/sec/vip/amschaton/bi;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/bi;->b()Z

    goto :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x7f070072
        :pswitch_0
    .end packed-switch
.end method
