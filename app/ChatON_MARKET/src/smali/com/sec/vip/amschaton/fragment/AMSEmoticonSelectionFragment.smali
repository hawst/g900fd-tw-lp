.class public Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;
.super Lcom/sec/vip/amschaton/fragment/AMSFragment;
.source "AMSEmoticonSelectionFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/GridView;

.field private b:Lcom/sec/vip/amschaton/fragment/at;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->a:Landroid/widget/GridView;

    .line 25
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->b:Lcom/sec/vip/amschaton/fragment/at;

    .line 87
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->a:Landroid/widget/GridView;

    if-nez v0, :cond_0

    .line 85
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->b:Lcom/sec/vip/amschaton/fragment/at;

    if-eqz v0, :cond_1

    .line 81
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->b:Lcom/sec/vip/amschaton/fragment/at;

    .line 83
    :cond_1
    new-instance v0, Lcom/sec/vip/amschaton/fragment/at;

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/at;-><init>(Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;Lcom/sec/vip/amschaton/fragment/as;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->b:Lcom/sec/vip/amschaton/fragment/at;

    .line 84
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->a:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->b:Lcom/sec/vip/amschaton/fragment/at;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 70
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 71
    const-string v1, "AMS_EMOTICON_INDEX"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 72
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/av;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/fragment/av;->c()V

    .line 74
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 30
    const v0, 0x7f03005a

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 32
    const v0, 0x7f07025d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->a:Landroid/widget/GridView;

    .line 33
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->a:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0902ec

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setColumnWidth(I)V

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->a:Landroid/widget/GridView;

    const v2, 0x7f020073

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setSelector(I)V

    .line 37
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->a()V

    .line 38
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->a:Landroid/widget/GridView;

    new-instance v2, Lcom/sec/vip/amschaton/fragment/as;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/as;-><init>(Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 51
    return-object v1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onDestroy()V

    .line 67
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onPause()V

    .line 57
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onResume()V

    .line 62
    return-void
.end method
