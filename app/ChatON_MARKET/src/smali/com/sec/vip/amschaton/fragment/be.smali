.class Lcom/sec/vip/amschaton/fragment/be;
.super Ljava/lang/Object;
.source "AMSListFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/sec/vip/amschaton/fragment/AMSListFragment;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 599
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/be;->c:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    iput p2, p0, Lcom/sec/vip/amschaton/fragment/be;->a:I

    iput-object p3, p0, Lcom/sec/vip/amschaton/fragment/be;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 602
    .line 603
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/be;->a:I

    const/16 v2, 0x7d2

    if-ne v0, v2, :cond_2

    .line 604
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/be;->c:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/be;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 612
    :goto_0
    if-eqz v0, :cond_0

    .line 613
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/be;->c:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/be;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Ljava/lang/String;)V

    .line 614
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/be;->c:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    const v3, 0x7f0b00f6

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(I)V

    .line 615
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/be;->c:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v2, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)V

    .line 617
    :cond_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/be;->c:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 618
    if-nez v0, :cond_1

    .line 619
    const-string v0, "Selected item was not deleted!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    :cond_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 622
    return-void

    .line 605
    :cond_2
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/be;->a:I

    const/16 v2, 0x7d1

    if-ne v0, v2, :cond_3

    .line 607
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/be;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/q;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 609
    :cond_3
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/be;->a:I

    const/16 v2, 0x7d0

    if-ne v0, v2, :cond_4

    .line 610
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/be;->c:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/be;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/sec/vip/amschaton/q;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method
