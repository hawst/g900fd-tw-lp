.class public Lcom/sec/vip/amschaton/fragment/cq;
.super Ljava/lang/Object;
.source "AMSStampSelectionFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V
    .locals 1

    .prologue
    .line 1480
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/cq;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1481
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cq;->b:Ljava/util/Map;

    .line 1482
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1485
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cq;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1486
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cq;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1516
    :cond_0
    :goto_0
    return-object v0

    .line 1489
    :cond_1
    const/4 v0, 0x0

    .line 1490
    const-string v1, "d"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1492
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1493
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lcom/sec/vip/amschaton/al;->a(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1513
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 1514
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cq;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1495
    :cond_3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1496
    if-ltz v1, :cond_2

    .line 1498
    const/16 v2, 0x4e20

    if-ge v1, v2, :cond_4

    .line 1501
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Lcom/sec/vip/amschaton/al;->d(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1502
    :cond_4
    const/16 v2, 0x7530

    if-ge v1, v2, :cond_5

    .line 1504
    add-int/lit16 v0, v1, -0x4e20

    .line 1505
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lcom/sec/vip/amschaton/al;->e(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1506
    :cond_5
    const v2, 0x9c40

    if-ge v1, v2, :cond_2

    .line 1508
    add-int/lit16 v0, v1, -0x7530

    .line 1509
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lcom/sec/vip/amschaton/al;->b(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 1519
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cq;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cq;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1523
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/fragment/cr;

    invoke-direct {v0, p0, p2}, Lcom/sec/vip/amschaton/fragment/cr;-><init>(Lcom/sec/vip/amschaton/fragment/cq;Landroid/widget/ImageView;)V

    .line 1530
    new-instance v1, Lcom/sec/vip/amschaton/fragment/cs;

    invoke-direct {v1, p0, p1, v0}, Lcom/sec/vip/amschaton/fragment/cs;-><init>(Lcom/sec/vip/amschaton/fragment/cq;Ljava/lang/String;Landroid/os/Handler;)V

    .line 1538
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1539
    return-void
.end method
