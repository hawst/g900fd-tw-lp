.class public Lcom/sec/vip/amschaton/fragment/db;
.super Landroid/widget/CursorAdapter;
.source "AMSStampSelectionFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

.field private b:Lcom/sec/vip/amschaton/fragment/cq;

.field private c:[Z

.field private d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1361
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/db;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    .line 1362
    invoke-direct {p0, p2, p3}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 1356
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->b:Lcom/sec/vip/amschaton/fragment/cq;

    .line 1358
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    .line 1363
    new-instance v0, Lcom/sec/vip/amschaton/fragment/cq;

    invoke-direct {v0, p1}, Lcom/sec/vip/amschaton/fragment/cq;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->b:Lcom/sec/vip/amschaton/fragment/cq;

    .line 1364
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    .line 1365
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/db;->a(Z)V

    .line 1366
    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1445
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->d:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1464
    :goto_0
    return-void

    .line 1448
    :cond_0
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 1449
    const/16 v0, 0x3e8

    iput v0, v5, Landroid/os/Message;->what:I

    .line 1452
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    if-eqz v0, :cond_2

    move v0, v1

    move v2, v3

    move v4, v1

    .line 1453
    :goto_1
    iget-object v6, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    array-length v6, v6

    if-ge v0, v6, :cond_3

    .line 1454
    iget-object v6, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    aget-boolean v6, v6, v0

    if-eqz v6, :cond_1

    move v4, v3

    .line 1453
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    .line 1457
    goto :goto_2

    :cond_2
    move v2, v3

    move v4, v1

    .line 1461
    :cond_3
    if-eqz v4, :cond_4

    move v0, v3

    :goto_3
    iput v0, v5, Landroid/os/Message;->arg1:I

    .line 1462
    if-eqz v2, :cond_5

    :goto_4
    iput v3, v5, Landroid/os/Message;->arg2:I

    .line 1463
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->d:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1461
    goto :goto_3

    :cond_5
    move v3, v1

    .line 1462
    goto :goto_4
.end method


# virtual methods
.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1468
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/db;->d:Landroid/os/Handler;

    .line 1469
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 1374
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    if-eqz v0, :cond_0

    .line 1375
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1376
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    aput-boolean p1, v1, v0

    .line 1375
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1379
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/db;->notifyDataSetChanged()V

    .line 1380
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/db;->b()V

    .line 1381
    return-void
.end method

.method public a()[Z
    .locals 1

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    return-object v0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 1385
    const v0, 0x7f07006e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1386
    const v1, 0x7f070066

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 1387
    const v2, 0x7f070067

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 1388
    const v3, 0x7f070070

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 1390
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1391
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 1392
    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1394
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v0

    const/16 v4, 0x7d2

    if-ne v0, v4, :cond_0

    .line 1395
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1397
    invoke-virtual {v2, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1398
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    aget-boolean v0, v0, v4

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1399
    new-instance v0, Lcom/sec/vip/amschaton/fragment/dc;

    invoke-direct {v0, p0, v2}, Lcom/sec/vip/amschaton/fragment/dc;-><init>(Lcom/sec/vip/amschaton/fragment/db;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1413
    :goto_0
    const-string v0, "ams_index"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1414
    const v1, 0x9c40

    if-lt v0, v1, :cond_1

    .line 1415
    const-string v0, "ams_path"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1416
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/db;->b:Lcom/sec/vip/amschaton/fragment/cq;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "d"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lcom/sec/vip/amschaton/fragment/cq;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 1420
    :goto_1
    return-void

    .line 1409
    :cond_0
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1410
    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 1418
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/db;->b:Lcom/sec/vip/amschaton/fragment/cq;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lcom/sec/vip/amschaton/fragment/cq;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1424
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/db;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1425
    const v1, 0x7f030016

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1426
    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 1433
    instance-of v0, p1, Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 1434
    check-cast p1, Landroid/widget/CheckBox;

    .line 1435
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1436
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/db;->c:[Z

    aput-boolean p2, v1, v0

    .line 1438
    :cond_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/db;->b()V

    .line 1440
    return-void
.end method
