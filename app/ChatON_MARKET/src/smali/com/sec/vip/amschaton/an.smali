.class public Lcom/sec/vip/amschaton/an;
.super Lcom/sec/vip/amschaton/aw;
.source "AMSToolPenPopup.java"


# instance fields
.field private g:I

.field private h:[Landroid/widget/ImageView;

.field private i:I

.field private j:Landroid/widget/SeekBar;

.field private k:Landroid/widget/ImageView;

.field private l:I

.field private m:[Landroid/widget/ImageView;

.field private n:Landroid/widget/ImageView;

.field private o:[I

.field private p:[I

.field private q:Landroid/view/View;

.field private r:Landroid/view/View$OnClickListener;

.field private s:Landroid/view/View$OnClickListener;

.field private t:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/view/View;Z)V
    .locals 3

    .prologue
    const/16 v2, 0xf

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1, p2, p4}, Lcom/sec/vip/amschaton/aw;-><init>(Landroid/content/Context;IZ)V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/an;->g:I

    .line 35
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/vip/amschaton/an;->i:I

    .line 37
    iput-object v1, p0, Lcom/sec/vip/amschaton/an;->j:Landroid/widget/SeekBar;

    .line 38
    iput-object v1, p0, Lcom/sec/vip/amschaton/an;->k:Landroid/widget/ImageView;

    .line 42
    const/16 v0, -0x100

    iput v0, p0, Lcom/sec/vip/amschaton/an;->l:I

    .line 44
    iput-object v1, p0, Lcom/sec/vip/amschaton/an;->m:[Landroid/widget/ImageView;

    .line 45
    iput-object v1, p0, Lcom/sec/vip/amschaton/an;->n:Landroid/widget/ImageView;

    .line 47
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->o:[I

    .line 51
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->p:[I

    .line 57
    iput-object v1, p0, Lcom/sec/vip/amschaton/an;->q:Landroid/view/View;

    .line 170
    new-instance v0, Lcom/sec/vip/amschaton/as;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/as;-><init>(Lcom/sec/vip/amschaton/an;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->r:Landroid/view/View$OnClickListener;

    .line 314
    new-instance v0, Lcom/sec/vip/amschaton/at;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/at;-><init>(Lcom/sec/vip/amschaton/an;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->s:Landroid/view/View$OnClickListener;

    .line 354
    new-instance v0, Lcom/sec/vip/amschaton/av;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/av;-><init>(Lcom/sec/vip/amschaton/an;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->t:Landroid/view/View$OnClickListener;

    .line 62
    invoke-direct {p0, p3}, Lcom/sec/vip/amschaton/an;->a(Landroid/view/View;)V

    .line 63
    return-void

    .line 47
    nop

    :array_0
    .array-data 4
        0x7f07021f
        0x7f070220
        0x7f070221
        0x7f070222
        0x7f070223
        0x7f070224
        0x7f070225
        0x7f070226
        0x7f070227
        0x7f070228
        0x7f070229
        0x7f07022a
        0x7f07022b
        0x7f07022c
        0x7f07022d
    .end array-data

    .line 51
    :array_1
    .array-data 4
        0x7f080028
        0x7f080029
        0x7f08002a
        0x7f08002b
        0x7f08002c
        0x7f08002d
        0x7f08002e
        0x7f08002f
        0x7f080030
        0x7f080031
        0x7f080032
        0x7f080033
        0x7f080034
        0x7f080035
        0x7f080036
    .end array-data
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/an;I)I
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/an;->f(I)I

    move-result v0

    return v0
.end method

.method private a(II)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/high16 v5, 0x3f000000    # 0.5f

    .line 222
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 223
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 224
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 225
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 226
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 228
    iget-object v1, p0, Lcom/sec/vip/amschaton/an;->k:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    .line 229
    iget-object v2, p0, Lcom/sec/vip/amschaton/an;->k:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    .line 230
    iget-object v3, p0, Lcom/sec/vip/amschaton/an;->c:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    .line 231
    iget-object v3, p0, Lcom/sec/vip/amschaton/an;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 232
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/vip/amschaton/an;->c:Landroid/graphics/Bitmap;

    .line 234
    :cond_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/vip/amschaton/an;->c:Landroid/graphics/Bitmap;

    .line 235
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/vip/amschaton/an;->c:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 239
    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 240
    int-to-float v4, v1

    mul-float/2addr v4, v5

    int-to-float v2, v2

    mul-float/2addr v2, v5

    int-to-float v1, v1

    const v5, 0x3d449ba6    # 0.048f

    mul-float/2addr v1, v5

    int-to-float v5, p1

    mul-float/2addr v1, v5

    const/high16 v5, 0x40000000    # 2.0f

    sub-float/2addr v1, v5

    invoke-virtual {v3, v4, v2, v1, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 242
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->k:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/an;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 245
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->j:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 246
    const v1, 0x102000d

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ClipDrawable;

    .line 247
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p2, v1}, Landroid/graphics/drawable/ClipDrawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 249
    return-void
.end method

.method private a(IZ)V
    .locals 1

    .prologue
    .line 154
    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-lt p1, v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->h:[Landroid/widget/ImageView;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v7, 0xf

    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 67
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->f()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f070274

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/vip/amschaton/ao;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/ao;-><init>(Lcom/sec/vip/amschaton/an;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    new-array v0, v6, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->h:[Landroid/widget/ImageView;

    .line 80
    iget-object v2, p0, Lcom/sec/vip/amschaton/an;->h:[Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->f()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070275

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v2, v1

    .line 81
    iget-object v2, p0, Lcom/sec/vip/amschaton/an;->h:[Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->f()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070276

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v2, v5

    .line 82
    iget-object v2, p0, Lcom/sec/vip/amschaton/an;->h:[Landroid/widget/ImageView;

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->f()Landroid/view/View;

    move-result-object v0

    const v4, 0x7f070277

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v2, v3

    .line 83
    iget-object v2, p0, Lcom/sec/vip/amschaton/an;->h:[Landroid/widget/ImageView;

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->f()Landroid/view/View;

    move-result-object v0

    const v4, 0x7f070278

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v2, v3

    move v0, v1

    .line 84
    :goto_0
    if-ge v0, v6, :cond_0

    .line 85
    iget-object v2, p0, Lcom/sec/vip/amschaton/an;->h:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/vip/amschaton/an;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/an;->g:I

    invoke-direct {p0, v0, v5}, Lcom/sec/vip/amschaton/an;->a(IZ)V

    .line 89
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->f()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f07027a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->k:Landroid/widget/ImageView;

    .line 90
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->f()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f070279

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->j:Landroid/widget/SeekBar;

    .line 91
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->j:Landroid/widget/SeekBar;

    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 92
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->j:Landroid/widget/SeekBar;

    new-instance v2, Lcom/sec/vip/amschaton/aq;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/aq;-><init>(Lcom/sec/vip/amschaton/an;)V

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 116
    iget v0, p0, Lcom/sec/vip/amschaton/an;->i:I

    invoke-direct {p0, v0, v5}, Lcom/sec/vip/amschaton/an;->b(IZ)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->f()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f07027b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->q:Landroid/view/View;

    .line 119
    new-array v0, v7, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->m:[Landroid/widget/ImageView;

    .line 121
    :goto_1
    if-ge v1, v7, :cond_1

    .line 123
    iget-object v2, p0, Lcom/sec/vip/amschaton/an;->m:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->q:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/vip/amschaton/an;->o:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v2, v1

    .line 124
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->m:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/an;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->q:Landroid/view/View;

    const v1, 0x7f070289

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->n:Landroid/widget/ImageView;

    .line 128
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->n:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/an;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget v0, p0, Lcom/sec/vip/amschaton/an;->l:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/an;->f(I)I

    move-result v0

    invoke-direct {p0, v0, v5}, Lcom/sec/vip/amschaton/an;->c(IZ)V

    .line 131
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/an;->b(Landroid/view/View;)V

    .line 132
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/an;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/vip/amschaton/an;->j()V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/an;IZ)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/an;->c(IZ)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 284
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    .line 285
    iget-object v1, p0, Lcom/sec/vip/amschaton/an;->m:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 284
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 288
    return-void
.end method

.method private b(IZ)V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->j:Landroid/widget/SeekBar;

    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/an;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 191
    iget v0, p0, Lcom/sec/vip/amschaton/an;->l:I

    invoke-direct {p0, p1, v0}, Lcom/sec/vip/amschaton/an;->a(II)V

    .line 192
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lcom/sec/vip/amschaton/ar;

    invoke-direct {v0, p0, p1}, Lcom/sec/vip/amschaton/ar;-><init>(Lcom/sec/vip/amschaton/an;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 144
    return-void
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/an;)[Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->h:[Landroid/widget/ImageView;

    return-object v0
.end method

.method private c(IZ)V
    .locals 4

    .prologue
    const/16 v3, 0xf

    const/4 v1, 0x0

    .line 270
    if-lt p1, v3, :cond_0

    .line 281
    :goto_0
    return-void

    .line 273
    :cond_0
    if-gez p1, :cond_2

    move v0, v1

    .line 274
    :goto_1
    if-ge v0, v3, :cond_1

    .line 275
    iget-object v2, p0, Lcom/sec/vip/amschaton/an;->m:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 274
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->m:[Landroid/widget/ImageView;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/an;)[Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->m:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/an;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/vip/amschaton/an;->l:I

    return v0
.end method

.method private e(I)I
    .locals 2

    .prologue
    .line 211
    add-int/lit8 v0, p1, -0x1

    .line 212
    if-ltz v0, :cond_0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_1

    .line 214
    :cond_0
    const/4 v0, 0x4

    .line 216
    :cond_1
    return v0
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/an;)[I
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->p:[I

    return-object v0
.end method

.method private f(I)I
    .locals 3

    .prologue
    .line 306
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xf

    if-ge v0, v1, :cond_1

    .line 307
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->e()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/an;->p:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 311
    :goto_1
    return v0

    .line 306
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 311
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private j()V
    .locals 0

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->dismiss()V

    .line 148
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/vip/amschaton/an;->g:I

    return v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 161
    iget v0, p0, Lcom/sec/vip/amschaton/an;->g:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/an;->a(IZ)V

    .line 162
    iput p1, p0, Lcom/sec/vip/amschaton/an;->g:I

    .line 163
    iget v0, p0, Lcom/sec/vip/amschaton/an;->g:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/an;->a(IZ)V

    .line 164
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/sec/vip/amschaton/an;->i:I

    return v0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 195
    iget v0, p0, Lcom/sec/vip/amschaton/an;->i:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/an;->b(IZ)V

    .line 196
    iput p1, p0, Lcom/sec/vip/amschaton/an;->i:I

    .line 197
    iget v0, p0, Lcom/sec/vip/amschaton/an;->i:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/an;->b(IZ)V

    .line 198
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 302
    iget v0, p0, Lcom/sec/vip/amschaton/an;->l:I

    return v0
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 295
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/an;->a(Z)V

    .line 296
    iput p1, p0, Lcom/sec/vip/amschaton/an;->l:I

    .line 297
    iget v0, p0, Lcom/sec/vip/amschaton/an;->l:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/an;->f(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/an;->c(IZ)V

    .line 298
    iget v0, p0, Lcom/sec/vip/amschaton/an;->i:I

    iget v1, p0, Lcom/sec/vip/amschaton/an;->l:I

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/an;->a(II)V

    .line 299
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->d:Lcom/sec/vip/amschaton/i;

    new-instance v1, Lcom/sec/vip/amschaton/au;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/au;-><init>(Lcom/sec/vip/amschaton/an;)V

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/i;->a(Lcom/sec/vip/amschaton/bi;)V

    .line 352
    return-void
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 329
    new-instance v0, Lcom/sec/vip/amschaton/i;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->e()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/an;->d:Lcom/sec/vip/amschaton/i;

    .line 330
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->d:Lcom/sec/vip/amschaton/i;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/i;->a(I)V

    .line 331
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/an;->d()V

    .line 332
    iget-object v0, p0, Lcom/sec/vip/amschaton/an;->d:Lcom/sec/vip/amschaton/i;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/i;->d()V

    .line 333
    return-void
.end method
