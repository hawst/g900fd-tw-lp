.class public Lcom/sec/vip/amschaton/aa;
.super Lcom/sec/vip/amschaton/u;
.source "AMSObjectText.java"


# instance fields
.field private Z:Lcom/sec/vip/amschaton/ab;

.field private aa:Landroid/graphics/Bitmap;

.field private ab:I

.field private ac:I

.field private ad:[I

.field private ae:Z

.field private af:I

.field private ag:Z

.field private ah:F

.field private ai:I

.field private aj:I

.field private ak:Landroid/graphics/Bitmap;

.field private al:I

.field private am:I


# direct methods
.method public constructor <init>(IIIZII)V
    .locals 6

    .prologue
    .line 99
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/amschaton/u;-><init>(IIIII)V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/aa;->Z:Lcom/sec/vip/amschaton/ab;

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    .line 41
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/vip/amschaton/aa;->ad:[I

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->ae:Z

    .line 55
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->af:I

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->ag:Z

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    .line 80
    const/16 v0, 0x30

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ai:I

    .line 81
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->aj:I

    .line 100
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    .line 101
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 103
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 104
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 105
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 106
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 107
    if-gez p3, :cond_1

    .line 108
    const/4 v0, 0x0

    .line 110
    :goto_0
    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .line 111
    const/4 v0, 0x4

    .line 113
    :cond_0
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->c:I

    .line 117
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->ad:[I

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->c:I

    aget v1, v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 118
    iput-boolean p4, p0, Lcom/sec/vip/amschaton/aa;->ag:Z

    .line 119
    return-void

    :cond_1
    move v0, p3

    goto :goto_0

    .line 41
    nop

    :array_0
    .array-data 4
        0xf
        0x12
        0x14
        0x17
        0x19
    .end array-data
.end method

.method private a(FFFFLjava/lang/String;IZZ)Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 2277
    const-string v0, "Don\'t use it !!!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2278
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2279
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2284
    if-eqz p7, :cond_0

    .line 2285
    invoke-virtual {p0, v0, p5}, Lcom/sec/vip/amschaton/aa;->a(Landroid/graphics/RectF;Ljava/lang/String;)Z

    .line 2289
    :cond_0
    if-eqz p8, :cond_1

    .line 2290
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->i:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2293
    :cond_1
    add-float v1, p1, p3

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    div-float/2addr v1, v6

    .line 2294
    add-float v2, p1, p3

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    div-float/2addr v2, v6

    .line 2295
    add-float v3, p2, p4

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v6

    .line 2296
    add-float v4, p2, p4

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    div-float/2addr v4, v6

    .line 2297
    iget-boolean v5, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    if-eqz v5, :cond_2

    .line 2298
    iget v5, p0, Lcom/sec/vip/amschaton/aa;->af:I

    packed-switch v5, :pswitch_data_0

    .line 2309
    :pswitch_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, p1, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2310
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v1, v1

    add-float/2addr v1, p1

    invoke-virtual {v0, p1, v3, v1, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2327
    :goto_0
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/sec/vip/amschaton/aa;->a(FFFF)V

    .line 2351
    const/4 v0, 0x1

    return v0

    .line 2302
    :pswitch_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v5, v5

    sub-float v5, p3, v5

    iget-object v6, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, v5, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2303
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v1, v1

    sub-float v1, p3, v1

    invoke-virtual {v0, v1, v3, p3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 2313
    :pswitch_2
    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v5, v5

    sub-float v5, p4, v5

    iget-object v6, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v3, v4, v1, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2314
    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v3, v3

    sub-float v3, p4, v3

    invoke-virtual {v0, v1, v3, v2, p4}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 2317
    :pswitch_3
    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v3, v4, v1, p2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2318
    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v3, v3

    add-float/2addr v3, p2

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 2322
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, p1, p2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2323
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v1, v1

    add-float/2addr v1, p1

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v2, v2

    add-float/2addr v2, p2

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 2298
    nop

    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a(F)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    .line 129
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 130
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->ad:[I

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->c:I

    aget v1, v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-double v1, v1

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    add-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 132
    const/high16 v0, 0x41700000    # 15.0f

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    .line 139
    const/high16 v0, 0x41a00000    # 20.0f

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v0, v1

    add-float/2addr v0, v5

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ai:I

    .line 140
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v0, v1

    add-float/2addr v0, v5

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->aj:I

    .line 141
    return-void
.end method

.method public a(FF)V
    .locals 1

    .prologue
    .line 1523
    float-to-int v0, p1

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    .line 1524
    float-to-int v0, p2

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    .line 1525
    return-void
.end method

.method public a(Lcom/sec/vip/amschaton/ab;)V
    .locals 0

    .prologue
    .line 2362
    iput-object p1, p0, Lcom/sec/vip/amschaton/aa;->Z:Lcom/sec/vip/amschaton/ab;

    .line 2363
    return-void
.end method

.method public a(FFFFLjava/lang/String;I)Z
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 2258
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/sec/vip/amschaton/aa;->a(FFFFLjava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public a(FFFFLjava/lang/String;IZ)Z
    .locals 9

    .prologue
    .line 2263
    const/4 v7, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/sec/vip/amschaton/aa;->a(FFFFLjava/lang/String;IZZ)Z

    move-result v0

    return v0
.end method

.method public a(FFLjava/lang/String;I)Z
    .locals 10

    .prologue
    const/16 v9, 0x6a

    const/16 v8, 0x69

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 2101
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, p4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2102
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->B:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_7

    .line 2103
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v0, v0, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2105
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 2106
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/aa;->ak:Landroid/graphics/Bitmap;

    .line 2110
    :cond_0
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    sub-float v2, p1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-direct {v1, v0, v0, v2, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0, v1, p3}, Lcom/sec/vip/amschaton/aa;->a(Landroid/graphics/RectF;Ljava/lang/String;)Z

    .line 2112
    new-instance v1, Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 2115
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_4

    .line 2116
    new-instance v0, Landroid/graphics/RectF;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v6, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2121
    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2122
    const-string v0, "AMSObjectText item resize_x trying to move out of canvas bounds"

    const-string v1, "AMSObjectText"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2123
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->ak:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 2124
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->ak:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    .line 2125
    const-string v0, "AMSObjectText setting bitmap to keep item within canvas boundary"

    const-string v1, "AMSObjectText"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2127
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    .line 2128
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    .line 2131
    :cond_2
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_5

    .line 2132
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->al:I

    if-ne v0, v8, :cond_3

    .line 2133
    const-string v0, "interchanging mPivotX value from left to right for resisizing item from right side"

    const-string v1, "AMSObjectText"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mPivotX value before:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->E:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AMSObjectText"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2135
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    .line 2136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mPivotX value after interchange:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->E:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AMSObjectText"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2148
    :cond_3
    :goto_1
    const-string v0, "AMSObjectText drawing item on canvas"

    const-string v1, "AMSObjectText"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2150
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_6

    .line 2152
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2153
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/aa;->a(FFFF)V

    .line 2154
    const/16 v0, 0x6b

    invoke-virtual {p0, v0, v7}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    .line 2155
    const/16 v0, 0x6b

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->al:I

    .line 2252
    :goto_2
    return v7

    .line 2119
    :cond_4
    new-instance v0, Landroid/graphics/RectF;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v6, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto/16 :goto_0

    .line 2141
    :cond_5
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->al:I

    const/16 v1, 0x6b

    if-ne v0, v1, :cond_3

    .line 2142
    const-string v0, "interchanging mPivotX value from right to left for resisizing item from left side"

    const-string v1, "AMSObjectText"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mPivotX value before:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->E:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AMSObjectText"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2144
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    .line 2145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mPivotX value after interchange:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->E:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AMSObjectText"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2157
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2158
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/aa;->a(FFFF)V

    .line 2159
    invoke-virtual {p0, v8, v7}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    .line 2160
    iput v8, p0, Lcom/sec/vip/amschaton/aa;->al:I

    goto/16 :goto_2

    .line 2163
    :cond_7
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->B:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_f

    .line 2164
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v0, v0, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2166
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_8

    .line 2167
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/aa;->ak:Landroid/graphics/Bitmap;

    .line 2170
    :cond_8
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    sub-float v2, p2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-direct {v1, v0, v0, v0, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0, v1, p3}, Lcom/sec/vip/amschaton/aa;->a(Landroid/graphics/RectF;Ljava/lang/String;)Z

    .line 2172
    new-instance v1, Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 2175
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    cmpg-float v0, v0, p2

    if-gez v0, :cond_c

    .line 2176
    new-instance v0, Landroid/graphics/RectF;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget-object v6, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2181
    :goto_3
    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2182
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->ak:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_9

    .line 2183
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->ak:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    .line 2185
    :cond_9
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    .line 2186
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    .line 2189
    :cond_a
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    cmpg-float v0, v0, p2

    if-gez v0, :cond_d

    .line 2190
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->am:I

    if-ne v0, v9, :cond_b

    .line 2191
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    .line 2200
    :cond_b
    :goto_4
    const-string v0, "AMSObjectText drawing item on canvas"

    const-string v1, "AMSObjectText"

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2202
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    cmpg-float v0, v0, p2

    if-gez v0, :cond_e

    .line 2203
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2204
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/aa;->a(FFFF)V

    .line 2205
    const/16 v0, 0x6c

    invoke-virtual {p0, v0, v7}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    .line 2206
    const/16 v0, 0x6c

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->am:I

    goto/16 :goto_2

    .line 2179
    :cond_c
    new-instance v0, Landroid/graphics/RectF;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v6, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto/16 :goto_3

    .line 2195
    :cond_d
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->am:I

    const/16 v1, 0x6c

    if-ne v0, v1, :cond_b

    .line 2196
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    goto/16 :goto_4

    .line 2208
    :cond_e
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2209
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->F:F

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/aa;->a(FFFF)V

    .line 2210
    invoke-virtual {p0, v9, v7}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    .line 2211
    iput v9, p0, Lcom/sec/vip/amschaton/aa;->am:I

    goto/16 :goto_2

    .line 2214
    :cond_f
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->B:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_10

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->B:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_16

    .line 2216
    :cond_10
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    if-nez v1, :cond_11

    .line 2217
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-direct {v1, v0, v0, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0, v1, p3}, Lcom/sec/vip/amschaton/aa;->a(Landroid/graphics/RectF;Ljava/lang/String;)Z

    .line 2222
    :cond_11
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 2223
    cmpg-float v2, p1, v0

    if-gez v2, :cond_14

    .line 2224
    const-string v2, "AMSObjectText item moving trying to escape from canvas bounds from left side -- stopping it on left boundary x = 0"

    const-string v3, "AMSObjectText"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move p1, v0

    .line 2232
    :cond_12
    :goto_5
    cmpg-float v2, p2, v0

    if-gez v2, :cond_15

    .line 2233
    const-string v1, "AMSObjectText item moving trying to escape from canvas bounds from top side -- stopping it on top boundary y = 0"

    const-string v2, "AMSObjectText"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move p2, v0

    .line 2241
    :cond_13
    :goto_6
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v0, v0, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2243
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, p1, p2, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2244
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v0, v0

    add-float/2addr v0, p1

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v1, v1

    add-float/2addr v1, p2

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/vip/amschaton/aa;->a(FFFF)V

    goto/16 :goto_2

    .line 2227
    :cond_14
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    cmpl-float v2, p1, v2

    if-lez v2, :cond_12

    .line 2228
    const-string v2, "AMSObjectText item moving trying to escape from canvas bounds from right side -- stopping it on right boundary"

    const-string v3, "AMSObjectText"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2229
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sub-float p1, v2, v3

    .line 2230
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "x after normlising :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AMSObjectText"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 2236
    :cond_15
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    cmpl-float v2, p2, v2

    if-lez v2, :cond_13

    .line 2237
    const-string v2, "AMSObjectText item moving trying to escape from canvas bounds from bottom side -- stopping it on bottom boundary"

    const-string v3, "AMSObjectText"

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2238
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    sub-float p2, v1, v2

    .line 2239
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Y after normlising :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AMSObjectText"

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 2247
    :cond_16
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v0, v0, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2248
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, -0x40800000    # -1.0f

    invoke-direct {v1, v0, v0, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0, v1, p3}, Lcom/sec/vip/amschaton/aa;->a(Landroid/graphics/RectF;Ljava/lang/String;)Z

    .line 2249
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, p1, p2, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2250
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v0, v0

    add-float/2addr v0, p1

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v1, v1

    add-float/2addr v1, p2

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/vip/amschaton/aa;->a(FFFF)V

    goto/16 :goto_2
.end method

.method public a(III)Z
    .locals 10

    .prologue
    const/4 v0, 0x5

    const/4 v9, 0x0

    .line 261
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/u;->a(III)Z

    .line 263
    if-gez p3, :cond_9

    move v1, v9

    .line 266
    :goto_0
    if-le v1, v0, :cond_8

    .line 274
    :goto_1
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->c:I

    .line 278
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->ad:[I

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->c:I

    aget v1, v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 354
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 355
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_cText()[C

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/aa;->a(Landroid/graphics/RectF;Ljava/lang/String;)Z

    .line 357
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_cText()[C

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/aa;->b(Ljava/lang/String;)Z

    .line 360
    :cond_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->H:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-nez v0, :cond_2

    :cond_1
    move v0, v9

    .line 395
    :goto_2
    return v0

    .line 363
    :cond_2
    new-instance v3, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v3}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 364
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 365
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 366
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 367
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 369
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    if-gez v0, :cond_5

    .line 370
    iput-short v9, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 375
    :cond_3
    :goto_3
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    if-gez v0, :cond_6

    .line 376
    iput-short v9, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 382
    :cond_4
    :goto_4
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 383
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 386
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->z:I

    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetCurObjectColor(IIII)Z

    .line 387
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->z:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->c:I

    int-to-byte v4, v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_cText()[C

    move-result-object v5

    iget-object v6, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v6}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_nTextLength()I

    move-result v6

    int-to-short v6, v6

    iget-object v7, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_cFontFaceName()[C

    move-result-object v7

    iget-object v8, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_nFontFaceLength()I

    move-result v8

    int-to-short v8, v8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/amsoma/AMSLibs;->VipAMS_ChangeSelectText(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;Lcom/sec/amsoma/structure/AMS_RECT;B[CS[CS)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v9

    .line 389
    goto/16 :goto_2

    .line 372
    :cond_5
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->N:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_3

    .line 373
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->N:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    goto :goto_3

    .line 378
    :cond_6
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->O:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_4

    .line 379
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->O:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    goto/16 :goto_4

    .line 391
    :cond_7
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_TRect(Lcom/sec/amsoma/structure/AMS_RECT;)V

    .line 392
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    .line 394
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/aa;->a(FFLjava/lang/String;I)Z

    .line 395
    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_8
    move v0, v1

    goto/16 :goto_1

    :cond_9
    move v1, p3

    goto/16 :goto_0
.end method

.method public a(Landroid/graphics/RectF;Ljava/lang/String;)Z
    .locals 12

    .prologue
    .line 1836
    new-instance v5, Landroid/text/TextPaint;

    invoke-direct {v5}, Landroid/text/TextPaint;-><init>()V

    .line 1838
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1839
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setDither(Z)V

    .line 1840
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->ad:[I

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->c:I

    aget v0, v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v0, v1

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1841
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->b:I

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1842
    const/4 v4, 0x0

    .line 1843
    const-string v2, ""

    .line 1844
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1845
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    .line 1846
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    .line 1847
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    .line 1850
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_4

    .line 1851
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v0

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_3

    .line 1852
    const/4 v0, 0x1

    .line 1859
    :goto_0
    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    if-le v0, v3, :cond_0

    .line 1860
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    .line 1862
    :cond_0
    invoke-virtual {v5, p2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v5}, Landroid/text/TextPaint;->getTextSize()F

    move-result v7

    mul-float/2addr v3, v7

    int-to-float v7, v0

    div-float/2addr v3, v7

    iget-object v7, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    move-result v7

    int-to-float v7, v7

    cmpl-float v3, v3, v7

    if-lez v3, :cond_1

    .line 1863
    invoke-virtual {v5, p2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {v5}, Landroid/text/TextPaint;->getTextSize()F

    move-result v3

    mul-float/2addr v0, v3

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    float-to-double v7, v0

    const-wide/high16 v9, 0x3fe0000000000000L    # 0.5

    add-double/2addr v7, v9

    double-to-int v0, v7

    .line 1865
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "## maxBoundWidth : "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1867
    const/4 v3, 0x0

    move v11, v3

    move v3, v4

    move v4, v11

    :goto_1
    if-ge v4, v6, :cond_6

    .line 1868
    invoke-virtual {p2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 1870
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1871
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1872
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1873
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v8

    const/high16 v9, 0x3f000000    # 0.5f

    add-float/2addr v8, v9

    float-to-int v8, v8

    .line 1875
    iget v9, p0, Lcom/sec/vip/amschaton/aa;->aj:I

    add-int/2addr v9, v0

    if-le v8, v9, :cond_5

    if-eqz v4, :cond_5

    .line 1877
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1879
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1880
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1881
    add-int/lit8 v3, v3, 0x1

    .line 1867
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1854
    :cond_3
    invoke-virtual {v5, p2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {v5}, Landroid/text/TextPaint;->getTextSize()F

    move-result v3

    mul-float/2addr v0, v3

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v0, v3

    float-to-double v7, v0

    const-wide/high16 v9, 0x3fe0000000000000L    # 0.5

    add-double/2addr v7, v9

    double-to-int v0, v7

    goto/16 :goto_0

    .line 1857
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v0, v3

    float-to-int v0, v0

    goto/16 :goto_0

    .line 1883
    :cond_5
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1884
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v7

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v7, v8

    float-to-int v7, v7

    .line 1885
    iget v8, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    if-ge v8, v7, :cond_2

    .line 1886
    iput v7, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    goto :goto_2

    .line 1904
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1905
    add-int/lit8 v2, v3, 0x1

    .line 1906
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 1907
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    if-ge v1, v0, :cond_7

    .line 1908
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    .line 1912
    :cond_7
    const/4 v0, 0x0

    .line 1913
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ai:I

    if-ge v1, v3, :cond_8

    .line 1914
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->ai:I

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    .line 1915
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ai:I

    iput v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    .line 1918
    :cond_8
    invoke-virtual {v5}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    float-to-double v6, v1

    int-to-double v1, v2

    const-wide v8, 0x3fd999999999999aL    # 0.4

    add-double/2addr v1, v8

    mul-double/2addr v1, v6

    double-to-int v1, v1

    iput v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    .line 1919
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_9

    .line 1920
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1921
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    .line 1923
    :cond_9
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    .line 1924
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1926
    const/4 v1, 0x0

    .line 1927
    const/4 v2, 0x1

    .line 1929
    :goto_3
    const-string v6, "\n"

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v6

    .line 1930
    const/4 v7, -0x1

    if-ne v6, v7, :cond_a

    .line 1931
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Landroid/text/TextPaint;->getTextSize()F

    move-result v4

    int-to-float v2, v2

    mul-float/2addr v2, v4

    invoke-virtual {v3, v1, v0, v2, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1938
    const/4 v0, 0x1

    return v0

    .line 1934
    :cond_a
    invoke-virtual {v4, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Landroid/text/TextPaint;->getTextSize()F

    move-result v7

    int-to-float v8, v2

    mul-float/2addr v7, v8

    invoke-virtual {v3, v1, v0, v7, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1935
    add-int/lit8 v1, v6, 0x1

    .line 1936
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public a(Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 406
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->H:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-nez v0, :cond_1

    :cond_0
    move v0, v9

    .line 510
    :goto_0
    return v0

    .line 466
    :cond_1
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 467
    invoke-virtual {p0, v0, p1}, Lcom/sec/vip/amschaton/aa;->a(Landroid/graphics/RectF;Ljava/lang/String;)Z

    .line 470
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 471
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/aa;->b(Ljava/lang/String;)Z

    .line 474
    :cond_2
    new-instance v3, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v3}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 475
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 476
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 477
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 478
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 480
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    if-gez v0, :cond_5

    .line 481
    iput-short v9, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 486
    :cond_3
    :goto_1
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    if-gez v0, :cond_6

    .line 487
    iput-short v9, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 493
    :cond_4
    :goto_2
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 494
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 496
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    iget-short v1, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/aa;->a(FF)V

    .line 499
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->z:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->c:I

    int-to-byte v4, v4

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    int-to-short v6, v6

    iget-object v7, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_cFontFaceName()[C

    move-result-object v7

    iget-object v8, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_nFontFaceLength()I

    move-result v8

    int-to-short v8, v8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/amsoma/AMSLibs;->VipAMS_ChangeSelectText(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;Lcom/sec/amsoma/structure/AMS_RECT;B[CS[CS)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v9

    .line 500
    goto/16 :goto_0

    .line 483
    :cond_5
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->N:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_3

    .line 484
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->N:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    goto :goto_1

    .line 489
    :cond_6
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->O:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_4

    .line 490
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->O:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    goto :goto_2

    .line 502
    :cond_7
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_TRect(Lcom/sec/amsoma/structure/AMS_RECT;)V

    .line 503
    new-instance v0, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;-><init>()V

    .line 504
    invoke-virtual {v0, p1}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->setM_strText(Ljava/lang/String;)V

    .line 505
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->setM_nTextLength(I)V

    .line 506
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strFontFaceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->setM_strFontFaceName(Ljava/lang/String;)V

    .line 507
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_nFontFaceLength()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->setM_nFontFaceLength(I)V

    .line 508
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1, v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_TText(Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;)V

    .line 509
    const/16 v0, 0xff

    invoke-virtual {p0, p1, v0}, Lcom/sec/vip/amschaton/aa;->a(Ljava/lang/String;I)Z

    .line 510
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 2

    .prologue
    .line 2042
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->F:F

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/sec/vip/amschaton/aa;->a(FFLjava/lang/String;I)Z

    .line 2043
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/String;II)Z
    .locals 8

    .prologue
    const/4 v0, 0x5

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v7, 0x0

    .line 177
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v1, :cond_0

    .line 238
    :goto_0
    return v7

    .line 180
    :cond_0
    iput p2, p0, Lcom/sec/vip/amschaton/aa;->b:I

    .line 181
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->b:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 183
    if-gez p3, :cond_7

    move v1, v7

    .line 186
    :goto_1
    if-le v1, v0, :cond_6

    .line 189
    :goto_2
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->c:I

    .line 193
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->l:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->ad:[I

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->c:I

    aget v1, v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 195
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/aa;->b(Ljava/lang/String;)Z

    .line 196
    const-string v6, ""

    .line 197
    new-instance v2, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v2}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 199
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v1, v1

    div-float/2addr v1, v4

    sub-float/2addr v0, v1

    add-float/2addr v0, v3

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 200
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v1, v1

    div-float/2addr v1, v4

    sub-float/2addr v0, v1

    add-float/2addr v0, v3

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 201
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v1, v1

    div-float/2addr v1, v4

    add-float/2addr v0, v1

    add-float/2addr v0, v3

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 202
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v1, v1

    div-float/2addr v1, v4

    add-float/2addr v0, v1

    add-float/2addr v0, v3

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 209
    iget-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    if-gez v0, :cond_4

    .line 210
    iput-short v7, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 215
    :cond_1
    :goto_3
    iget-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    if-gez v0, :cond_5

    .line 216
    iput-short v7, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 222
    :cond_2
    :goto_4
    iget-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 223
    iget-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 225
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->z:I

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->c:I

    int-to-byte v3, v3

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    int-to-short v5, v5

    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/amsoma/AMSLibs;->VipAMS_EncodeNewText(ILcom/sec/amsoma/structure/AMS_RECT;B[CS[CS)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 235
    const/16 v0, 0xff

    invoke-virtual {p0, p1, v0}, Lcom/sec/vip/amschaton/aa;->a(Ljava/lang/String;I)Z

    .line 238
    :cond_3
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 212
    :cond_4
    iget-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->N:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v1, v3

    if-le v0, v1, :cond_1

    .line 213
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->N:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    goto :goto_3

    .line 218
    :cond_5
    iget-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->O:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v1, v3

    if-le v0, v1, :cond_2

    .line 219
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->O:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    goto :goto_4

    :cond_6
    move v0, v1

    goto/16 :goto_2

    :cond_7
    move v1, p3

    goto/16 :goto_1
.end method

.method public a(Ljava/lang/String;IIFF)Z
    .locals 1

    .prologue
    .line 243
    iput p4, p0, Lcom/sec/vip/amschaton/aa;->E:F

    .line 244
    iput p5, p0, Lcom/sec/vip/amschaton/aa;->F:F

    .line 245
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/aa;->a(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public a([I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2376
    array-length v2, p1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 2381
    :goto_0
    return v0

    .line 2379
    :cond_0
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    float-to-int v2, v2

    aput v2, p1, v0

    .line 2380
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    float-to-int v0, v0

    aput v0, p1, v1

    move v0, v1

    .line 2381
    goto :goto_0
.end method

.method public b(Lcom/sec/amsoma/structure/AMS_RECT;)Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 591
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->H:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-nez v0, :cond_1

    :cond_0
    move v0, v9

    .line 602
    :goto_0
    return v0

    .line 594
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->z:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->c:I

    int-to-byte v4, v3

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_cText()[C

    move-result-object v5

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_nTextLength()I

    move-result v3

    int-to-short v6, v3

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_cFontFaceName()[C

    move-result-object v7

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_nFontFaceLength()I

    move-result v3

    int-to-short v8, v3

    move-object v3, p1

    invoke-virtual/range {v0 .. v8}, Lcom/sec/amsoma/AMSLibs;->VipAMS_ChangeSelectText(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;Lcom/sec/amsoma/structure/AMS_RECT;B[CS[CS)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v9

    .line 596
    goto :goto_0

    .line 598
    :cond_2
    new-instance v0, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 599
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->getM_pSelectObjectData()I

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetSelectObjectData(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)V

    .line 600
    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    iget-short v1, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/aa;->a(FFLjava/lang/String;I)Z

    .line 601
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, p1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_TRect(Lcom/sec/amsoma/structure/AMS_RECT;)V

    .line 602
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 13

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v12, 0x3f000000    # 0.5f

    .line 1618
    new-instance v6, Landroid/text/TextPaint;

    invoke-direct {v6}, Landroid/text/TextPaint;-><init>()V

    .line 1620
    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1621
    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setDither(Z)V

    .line 1622
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->ad:[I

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->c:I

    aget v0, v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v0, v1

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1623
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->b:I

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1625
    const-string v1, ""

    .line 1626
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1627
    iput v3, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    .line 1628
    iput v3, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    .line 1629
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->N:I

    const/high16 v5, 0x42200000    # 40.0f

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v5, v7

    add-float/2addr v5, v12

    float-to-int v5, v5

    sub-int v7, v2, v5

    .line 1631
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    move v5, v3

    move v2, v3

    .line 1632
    :goto_0
    if-ge v5, v8, :cond_2

    .line 1633
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v9

    .line 1635
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1636
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1637
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1638
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v10

    add-float/2addr v10, v12

    float-to-int v10, v10

    .line 1640
    iget v11, p0, Lcom/sec/vip/amschaton/aa;->aj:I

    add-int/2addr v11, v7

    if-le v10, v11, :cond_1

    .line 1641
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1643
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1644
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1645
    add-int/lit8 v2, v2, 0x1

    .line 1632
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1647
    :cond_1
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1648
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    add-float/2addr v9, v12

    float-to-int v9, v9

    .line 1649
    iget v10, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    if-ge v10, v9, :cond_0

    .line 1650
    iput v9, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    goto :goto_1

    .line 1668
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1669
    add-int/lit8 v1, v2, 0x1

    .line 1670
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    add-float/2addr v0, v12

    float-to-int v0, v0

    .line 1671
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    if-ge v2, v0, :cond_3

    .line 1672
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    .line 1676
    :cond_3
    const/4 v0, 0x0

    .line 1677
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->ai:I

    if-ge v2, v7, :cond_4

    .line 1678
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->ai:I

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    add-float/2addr v0, v12

    .line 1679
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ai:I

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    .line 1682
    :cond_4
    invoke-virtual {v6}, Landroid/text/TextPaint;->getTextSize()F

    move-result v2

    float-to-double v7, v2

    int-to-double v1, v1

    const-wide v9, 0x3fd999999999999aL    # 0.4

    add-double/2addr v1, v9

    mul-double/2addr v1, v7

    double-to-int v1, v1

    iput v1, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    .line 1683
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_5

    .line 1684
    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1685
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    .line 1687
    :cond_5
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    .line 1688
    new-instance v7, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-direct {v7, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move v1, v3

    move v2, v4

    .line 1693
    :goto_2
    const-string v3, "\n"

    invoke-virtual {v5, v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    .line 1694
    const/4 v8, -0x1

    if-ne v3, v8, :cond_6

    .line 1695
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v5, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Landroid/text/TextPaint;->getTextSize()F

    move-result v3

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-virtual {v7, v1, v0, v2, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1702
    return v4

    .line 1698
    :cond_6
    invoke-virtual {v5, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Landroid/text/TextPaint;->getTextSize()F

    move-result v8

    int-to-float v9, v2

    mul-float/2addr v8, v9

    invoke-virtual {v7, v1, v0, v8, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1699
    add-int/lit8 v1, v3, 0x1

    .line 1700
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public c(FF)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 154
    iget v1, p0, Lcom/sec/vip/amschaton/aa;->B:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 162
    :cond_0
    :goto_0
    return v0

    .line 157
    :cond_1
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->R:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v3, v4

    mul-float/2addr v3, v8

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    add-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->R:F

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v8

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    add-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->R:F

    iget v6, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v5, v6

    mul-float/2addr v5, v8

    iget v6, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    add-float/2addr v5, v6

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v5

    iget-short v5, v5, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/vip/amschaton/aa;->R:F

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v6, v7

    mul-float/2addr v6, v8

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    add-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 159
    invoke-virtual {v1, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public d(FF)Z
    .locals 11

    .prologue
    const/4 v10, 0x6

    const/4 v9, 0x5

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 653
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v2, :cond_0

    .line 910
    :goto_0
    return v0

    .line 830
    :cond_0
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->B:I

    packed-switch v2, :pswitch_data_0

    :goto_1
    move v0, v1

    .line 910
    goto :goto_0

    .line 832
    :pswitch_0
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-nez v2, :cond_1

    .line 833
    const-string v2, "Why is selected object null???"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->B:I

    move v0, v1

    .line 835
    goto :goto_0

    .line 838
    :cond_1
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    if-nez v2, :cond_2

    .line 839
    new-instance v2, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    iput-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 840
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->getM_pSelectObjectData()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2, v3, v4}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetSelectObjectData(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)V

    .line 841
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->ae:Z

    .line 844
    :cond_2
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/aa;->ae:Z

    if-eqz v2, :cond_7

    .line 846
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 847
    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 848
    iget v4, p0, Lcom/sec/vip/amschaton/aa;->R:F

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v8

    float-to-int v4, v4

    sub-int/2addr v2, v4

    .line 849
    iget v4, p0, Lcom/sec/vip/amschaton/aa;->R:F

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v8

    float-to-int v4, v4

    add-int/2addr v3, v4

    .line 852
    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 853
    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v5

    iget-short v5, v5, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 854
    iget v6, p0, Lcom/sec/vip/amschaton/aa;->R:F

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v6, v7

    mul-float/2addr v6, v8

    float-to-int v6, v6

    sub-int/2addr v4, v6

    .line 855
    iget v6, p0, Lcom/sec/vip/amschaton/aa;->R:F

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v6, v7

    mul-float/2addr v6, v8

    float-to-int v6, v6

    add-int/2addr v5, v6

    .line 857
    iget-boolean v6, p0, Lcom/sec/vip/amschaton/aa;->ag:Z

    if-eqz v6, :cond_3

    add-int v6, v2, v5

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v6, p1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_3

    int-to-float v6, v4

    sub-float/2addr v6, p2

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_3

    .line 859
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    .line 860
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    .line 861
    iput v10, p0, Lcom/sec/vip/amschaton/aa;->B:I

    .line 862
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->am:I

    .line 863
    const/16 v0, 0x6a

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    goto/16 :goto_1

    .line 865
    :cond_3
    iget-boolean v6, p0, Lcom/sec/vip/amschaton/aa;->ag:Z

    if-eqz v6, :cond_4

    int-to-float v6, v5

    sub-float/2addr v6, p1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_4

    add-int v6, v4, v3

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v6, p2

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_4

    .line 867
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    .line 868
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    .line 869
    iput v9, p0, Lcom/sec/vip/amschaton/aa;->B:I

    .line 870
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->al:I

    .line 871
    const/16 v0, 0x6b

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    goto/16 :goto_1

    .line 873
    :cond_4
    iget-boolean v6, p0, Lcom/sec/vip/amschaton/aa;->ag:Z

    if-eqz v6, :cond_5

    add-int v6, v2, v5

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v6, p1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_5

    int-to-float v6, v3

    sub-float/2addr v6, p2

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_5

    .line 875
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    .line 876
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    .line 877
    iput v10, p0, Lcom/sec/vip/amschaton/aa;->B:I

    .line 878
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->am:I

    .line 879
    const/16 v0, 0x6c

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    goto/16 :goto_1

    .line 881
    :cond_5
    iget-boolean v6, p0, Lcom/sec/vip/amschaton/aa;->ag:Z

    if-eqz v6, :cond_6

    int-to-float v2, v2

    sub-float/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v6, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v2, v2, v6

    if-gez v2, :cond_6

    add-int v2, v4, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_6

    .line 883
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    .line 884
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    .line 885
    iput v9, p0, Lcom/sec/vip/amschaton/aa;->B:I

    .line 886
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->al:I

    .line 887
    const/16 v0, 0x69

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    goto/16 :goto_1

    .line 891
    :cond_6
    int-to-float v2, v5

    sub-float/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_7

    int-to-float v2, v4

    sub-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_7

    .line 893
    const/16 v0, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    .line 894
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->B:I

    goto/16 :goto_1

    .line 899
    :cond_7
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v2, v2

    sub-float v2, p1, v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->C:I

    .line 900
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    sub-float v2, p2, v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->D:I

    .line 901
    const/4 v2, 0x3

    iput v2, p0, Lcom/sec/vip/amschaton/aa;->B:I

    .line 902
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v3, v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/vip/amschaton/aa;->a(FF)V

    .line 903
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_cText()[C

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([C)V

    .line 904
    const/16 v3, 0x80

    invoke-virtual {p0, v2, v3}, Lcom/sec/vip/amschaton/aa;->a(Ljava/lang/String;I)Z

    .line 905
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    .line 906
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/aa;->a(FF)V

    .line 907
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/aa;->ae:Z

    goto/16 :goto_1

    .line 830
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public e(FF)Z
    .locals 7

    .prologue
    const/16 v4, 0x80

    const/high16 v3, 0x41200000    # 10.0f

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 925
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v2, :cond_0

    .line 1123
    :goto_0
    return v0

    .line 1089
    :cond_0
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->B:I

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    move v0, v1

    .line 1123
    goto :goto_0

    .line 1091
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/sec/vip/amschaton/aa;->E:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_2

    iget v0, p0, Lcom/sec/vip/amschaton/aa;->F:F

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_3

    .line 1092
    :cond_2
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    .line 1094
    :cond_3
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->C:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v2, p0, Lcom/sec/vip/amschaton/aa;->D:I

    int-to-float v2, v2

    sub-float v2, p2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/sec/vip/amschaton/aa;->a(FFLjava/lang/String;I)Z

    goto :goto_1

    .line 1099
    :pswitch_2
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 1100
    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 1101
    iget v4, p0, Lcom/sec/vip/amschaton/aa;->R:F

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v6

    float-to-int v4, v4

    sub-int/2addr v2, v4

    .line 1102
    iget v4, p0, Lcom/sec/vip/amschaton/aa;->R:F

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->d:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v6

    float-to-int v4, v4

    add-int/2addr v3, v4

    .line 1103
    int-to-float v3, v3

    sub-float/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    int-to-float v2, v2

    sub-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sec/vip/amschaton/aa;->ah:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_4

    move v2, v1

    :goto_2
    if-nez v2, :cond_1

    .line 1105
    const/16 v2, 0x65

    invoke-virtual {p0, v2, v0}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    .line 1106
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/aa;->B:I

    goto :goto_1

    :cond_4
    move v2, v0

    .line 1103
    goto :goto_2

    .line 1113
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->ag:Z

    if-eqz v0, :cond_1

    .line 1116
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, v4}, Lcom/sec/vip/amschaton/aa;->a(FFLjava/lang/String;I)Z

    .line 1117
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->H:Z

    if-eqz v0, :cond_1

    .line 1118
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    goto/16 :goto_1

    .line 1089
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public f(FF)Z
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/16 v5, 0xff

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1138
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v2, :cond_1

    .line 1510
    :cond_0
    :goto_0
    return v0

    .line 1382
    :cond_1
    new-instance v3, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v3}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 1383
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->B:I

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v1

    .line 1510
    goto :goto_0

    .line 1385
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/aa;->G:Z

    if-nez v2, :cond_2

    .line 1386
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/aa;->G:Z

    move v0, v1

    .line 1387
    goto :goto_0

    .line 1389
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/aa;->a(FF)V

    .line 1390
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->Z:Lcom/sec/vip/amschaton/ab;

    if-eqz v2, :cond_0

    .line 1393
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->Z:Lcom/sec/vip/amschaton/ab;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/sec/vip/amschaton/ab;->a(Ljava/lang/String;)Z

    goto :goto_1

    .line 1396
    :pswitch_2
    iput v4, p0, Lcom/sec/vip/amschaton/aa;->B:I

    .line 1398
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    sub-int/2addr v2, v4

    .line 1399
    iget-object v4, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v5

    iget-short v5, v5, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    sub-int/2addr v4, v5

    .line 1401
    iget v5, p0, Lcom/sec/vip/amschaton/aa;->N:I

    if-eqz v5, :cond_3

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->O:I

    if-nez v5, :cond_4

    .line 1402
    :cond_3
    const-string v5, "mCanvas width or height not initialised initializing them -- "

    const-string v6, "AMSObjectText"

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1403
    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/sec/vip/amschaton/aa;->N:I

    .line 1404
    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->g:Landroid/graphics/Canvas;

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/sec/vip/amschaton/aa;->O:I

    .line 1407
    :cond_4
    iget v5, p0, Lcom/sec/vip/amschaton/aa;->C:I

    int-to-float v5, v5

    sub-float v5, p1, v5

    float-to-int v5, v5

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 1408
    iget v5, p0, Lcom/sec/vip/amschaton/aa;->D:I

    int-to-float v5, v5

    sub-float v5, p2, v5

    float-to-int v5, v5

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 1409
    int-to-float v5, v2

    add-float/2addr v5, p1

    iget v6, p0, Lcom/sec/vip/amschaton/aa;->C:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    float-to-int v5, v5

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 1410
    int-to-float v5, v4

    add-float/2addr v5, p2

    iget v6, p0, Lcom/sec/vip/amschaton/aa;->D:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    float-to-int v5, v5

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 1413
    const-string v5, "AMSObjectText item saving the state after move complete"

    const-string v6, "AMSObjectText"

    invoke-static {v5, v6}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1414
    iget-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    if-gez v5, :cond_7

    .line 1415
    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 1416
    iget-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    add-int/2addr v2, v5

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 1422
    :cond_5
    :goto_2
    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    if-gez v2, :cond_8

    .line 1423
    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 1424
    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    add-int/2addr v2, v4

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 1431
    :cond_6
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AMSObjectText item saving state rec bounds are ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "AMSObjectText"

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/aa;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v2

    .line 1434
    iget-boolean v4, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    if-eqz v4, :cond_9

    .line 1435
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    move v0, v2

    .line 1436
    goto/16 :goto_0

    .line 1418
    :cond_7
    iget-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v6, p0, Lcom/sec/vip/amschaton/aa;->N:I

    iget-object v7, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int/2addr v6, v7

    if-le v5, v6, :cond_5

    .line 1419
    iget v5, p0, Lcom/sec/vip/amschaton/aa;->N:I

    iget-object v6, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 1420
    iget-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    add-int/2addr v2, v5

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    goto :goto_2

    .line 1426
    :cond_8
    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v5, p0, Lcom/sec/vip/amschaton/aa;->O:I

    iget-object v6, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    if-le v2, v5, :cond_6

    .line 1427
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->O:I

    iget-object v5, p0, Lcom/sec/vip/amschaton/aa;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sub-int/2addr v2, v5

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 1428
    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    add-int/2addr v2, v4

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    goto/16 :goto_3

    .line 1438
    :cond_9
    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v2, v2

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v3, v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/vip/amschaton/aa;->a(FF)V

    .line 1439
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/aa;->G:Z

    if-nez v2, :cond_a

    .line 1440
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/aa;->G:Z

    move v0, v1

    .line 1441
    goto/16 :goto_0

    .line 1443
    :cond_a
    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->Z:Lcom/sec/vip/amschaton/ab;

    if-eqz v2, :cond_0

    .line 1446
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->Z:Lcom/sec/vip/amschaton/ab;

    iget-object v2, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/sec/vip/amschaton/ab;->a(Ljava/lang/String;)Z

    goto/16 :goto_1

    .line 1451
    :pswitch_3
    const/16 v2, 0x65

    invoke-virtual {p0, v2, v0}, Lcom/sec/vip/amschaton/aa;->b(IZ)V

    .line 1452
    iget-object v0, p0, Lcom/sec/vip/amschaton/aa;->S:Lcom/sec/vip/amschaton/v;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/v;->a()V

    goto/16 :goto_1

    .line 1456
    :pswitch_4
    iput v4, p0, Lcom/sec/vip/amschaton/aa;->B:I

    .line 1457
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->al:I

    .line 1458
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 1459
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v2, v4

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 1460
    float-to-int v2, p1

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 1461
    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    add-int/2addr v2, v4

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 1462
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    cmpg-float v2, p1, v2

    if-gez v2, :cond_b

    .line 1463
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v4, v4

    sub-float/2addr v2, v4

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 1464
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 1469
    :goto_4
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    if-nez v2, :cond_c

    .line 1470
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3, v5}, Lcom/sec/vip/amschaton/aa;->a(FFLjava/lang/String;I)Z

    goto/16 :goto_1

    .line 1466
    :cond_b
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 1467
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    goto :goto_4

    .line 1473
    :cond_c
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    .line 1474
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AMSObjectText item saving state after resizex bounds are ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "AMSObjectText"

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1475
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/aa;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 1476
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/aa;->a()V

    goto/16 :goto_0

    .line 1479
    :cond_d
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3, v5}, Lcom/sec/vip/amschaton/aa;->a(FFLjava/lang/String;I)Z

    goto/16 :goto_1

    .line 1482
    :pswitch_5
    iput v4, p0, Lcom/sec/vip/amschaton/aa;->B:I

    .line 1483
    iput v0, p0, Lcom/sec/vip/amschaton/aa;->am:I

    .line 1484
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->E:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v2, v4

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 1485
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 1486
    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    add-int/2addr v2, v4

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 1487
    float-to-int v2, p2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 1488
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AMSObjectText item saving state after resizeY bounds are ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v4, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "AMSObjectText"

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1489
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    cmpg-float v2, p2, v2

    if-gez v2, :cond_e

    .line 1490
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v4, v4

    sub-float/2addr v2, v4

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 1491
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 1496
    :goto_5
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    if-nez v2, :cond_f

    .line 1497
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3, v5}, Lcom/sec/vip/amschaton/aa;->a(FFLjava/lang/String;I)Z

    goto/16 :goto_1

    .line 1493
    :cond_e
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 1494
    iget v2, p0, Lcom/sec/vip/amschaton/aa;->F:F

    iget v4, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    goto :goto_5

    .line 1500
    :cond_f
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/aa;->I:Z

    .line 1501
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/aa;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 1502
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/aa;->a()V

    goto/16 :goto_0

    .line 1505
    :cond_10
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    iget-short v2, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/aa;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3, v5}, Lcom/sec/vip/amschaton/aa;->a(FFLjava/lang/String;I)Z

    goto/16 :goto_1

    .line 1383
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method

.method public l()I
    .locals 1

    .prologue
    .line 2020
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->ab:I

    return v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 2029
    iget v0, p0, Lcom/sec/vip/amschaton/aa;->ac:I

    return v0
.end method
