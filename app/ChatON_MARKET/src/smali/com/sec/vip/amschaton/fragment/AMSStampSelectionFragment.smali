.class public Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;
.super Lcom/sec/vip/amschaton/fragment/AMSFragment;
.source "AMSStampSelectionFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private A:Landroid/widget/AdapterView$OnItemClickListener;

.field private B:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private final C:Landroid/view/View$OnClickListener;

.field private D:Landroid/os/Handler;

.field private E:Landroid/os/Handler;

.field private a:I

.field private b:Landroid/widget/GridView;

.field private c:Lcom/sec/vip/amschaton/fragment/cw;

.field private m:I

.field private n:Landroid/widget/CheckedTextView;

.field private o:Z

.field private p:Lcom/sec/chaton/widget/m;

.field private q:I

.field private r:J

.field private s:Landroid/widget/LinearLayout;

.field private t:Landroid/view/View;

.field private u:Landroid/view/View;

.field private v:I

.field private w:Lcom/sec/vip/amschaton/fragment/db;

.field private x:Lcom/sec/vip/amschaton/a/e;

.field private y:Landroid/view/View;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;-><init>()V

    .line 97
    iput v3, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    .line 99
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    .line 100
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    .line 102
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m:I

    .line 105
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n:Landroid/widget/CheckedTextView;

    .line 109
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->o:Z

    .line 112
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p:Lcom/sec/chaton/widget/m;

    .line 115
    iput v3, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    .line 116
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->r:J

    .line 118
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->s:Landroid/widget/LinearLayout;

    .line 119
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->t:Landroid/view/View;

    .line 120
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->u:Landroid/view/View;

    .line 121
    const/16 v0, 0x1389

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    .line 123
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    .line 124
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    .line 127
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->z:Z

    .line 514
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ck;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/ck;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->A:Landroid/widget/AdapterView$OnItemClickListener;

    .line 547
    new-instance v0, Lcom/sec/vip/amschaton/fragment/cl;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/cl;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->B:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 933
    new-instance v0, Lcom/sec/vip/amschaton/fragment/cb;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/cb;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->C:Landroid/view/View$OnClickListener;

    .line 990
    new-instance v0, Lcom/sec/vip/amschaton/fragment/cc;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/cc;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->D:Landroid/os/Handler;

    .line 1287
    new-instance v0, Lcom/sec/vip/amschaton/fragment/cg;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/cg;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->E:Landroid/os/Handler;

    .line 1805
    return-void
.end method

.method private a(Landroid/content/Intent;Landroid/os/Bundle;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 866
    .line 867
    const-string v0, "exceed_size"

    const/4 v2, 0x1

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 868
    const-string v0, "[getBitmapFromExtras] exceed_size is true!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    const-string v0, "temp_file_path"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 871
    if-nez v2, :cond_0

    move-object v0, v1

    .line 890
    :goto_0
    return-object v0

    .line 874
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tmpFilePath : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 876
    if-nez v0, :cond_1

    move-object v0, v1

    .line 877
    goto :goto_0

    .line 881
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 882
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 883
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 889
    :cond_2
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[getBitmapFromExtras] bmp width and height : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 886
    :cond_3
    const-string v0, "[getBitmapFromExtras] exceed_size is false!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    const-string v0, "data"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method private a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 894
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd_HHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 895
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 896
    return-object v0
.end method

.method private a()V
    .locals 8

    .prologue
    const v2, 0x7f0d0014

    const v7, 0x7f0b0121

    const v6, 0x7f0b00fd

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 573
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 575
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/vip/amschaton/fragment/cn;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/cn;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/vip/amschaton/fragment/cm;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/cm;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 631
    :cond_0
    :goto_0
    return-void

    .line 601
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    .line 602
    array-length v1, v0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 605
    new-array v1, v5, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aget-object v3, v0, v4

    aput-object v3, v1, v2

    aget-object v0, v0, v5

    aput-object v0, v1, v4

    .line 606
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/vip/amschaton/fragment/cp;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/cp;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/vip/amschaton/fragment/co;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/co;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 1099
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fe

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b03f6

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fc

    new-instance v2, Lcom/sec/vip/amschaton/fragment/cf;

    invoke-direct {v2, p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/cf;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;II)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fd

    new-instance v2, Lcom/sec/vip/amschaton/fragment/ce;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/ce;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 1115
    return-void
.end method

.method private a(IJ)V
    .locals 4

    .prologue
    const v1, 0x9c40

    .line 823
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    if-nez v0, :cond_1

    .line 836
    :cond_0
    :goto_0
    return-void

    .line 831
    :cond_1
    const-string v0, ""

    .line 832
    if-lt p1, v1, :cond_2

    .line 833
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/al;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 835
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/vip/amschaton/a/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 459
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[STP]"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string v4, ".png"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 460
    invoke-direct {p0, p1, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    .line 461
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h()V

    .line 463
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    .line 464
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h(I)V

    .line 465
    return-void
.end method

.method private a(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    .line 901
    sget-object v0, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v0

    .line 902
    invoke-static {p1}, Lcom/sec/chaton/util/cp;->b(Landroid/view/MenuItem;)V

    .line 903
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p1, v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;Z)V

    .line 921
    return-void

    .line 903
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 471
    const v0, 0x7f070265

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n:Landroid/widget/CheckedTextView;

    .line 472
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n:Landroid/widget/CheckedTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 473
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n:Landroid/widget/CheckedTextView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 475
    const v0, 0x7f07026d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    .line 476
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    const v1, 0x7f020073

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelector(I)V

    .line 477
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/vip/amschaton/fragment/cj;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/cj;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 491
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 492
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Z)V

    .line 493
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->k()V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Z)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Z)V

    return-void
.end method

.method private a(Ljava/io/FileOutputStream;)V
    .locals 1

    .prologue
    .line 740
    if-eqz p1, :cond_0

    .line 741
    :try_start_0
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    :cond_0
    :goto_0
    return-void

    .line 743
    :catch_0
    move-exception v0

    .line 744
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 496
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    if-nez v0, :cond_1

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 500
    :cond_1
    if-eqz p1, :cond_2

    .line 501
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->A:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 502
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    const/16 v1, 0x1388

    if-eq v0, v1, :cond_0

    .line 503
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->B:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 504
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->registerForContextMenu(Landroid/view/View;)V

    goto :goto_0

    .line 507
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 508
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 509
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->unregisterForContextMenu(Landroid/view/View;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 712
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;II)Z
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(II)Z

    move-result v0

    return v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 640
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/amschaton/AMSEmoticonSelectionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 641
    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 642
    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 634
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/amschaton/AMSImageEditorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 635
    const-string v1, "IMAGE_SELECTOR"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 636
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 637
    return-void
.end method

.method private b(IJ)V
    .locals 3

    .prologue
    .line 1161
    if-gez p1, :cond_0

    .line 1170
    :goto_0
    return-void

    .line 1164
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/a/c;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/a/c;-><init>(Landroid/content/Context;)V

    .line 1165
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/c;->b()Lcom/sec/vip/amschaton/a/c;

    .line 1166
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    add-int/lit16 v2, p1, -0x7530

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/al;->b(I)I

    move-result v1

    .line 1167
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/a/c;->a(Ljava/lang/String;Ljava/lang/String;)J

    .line 1168
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/c;->a()V

    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, 0x42dc0000    # 110.0f

    const v2, 0x7f07014b

    .line 1921
    const v0, 0x7f07026e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    .line 1922
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v3}, Lcom/sec/chaton/util/an;->b(F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 1923
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v3}, Lcom/sec/chaton/util/an;->b(F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 1924
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1925
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b0132

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1927
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1928
    new-instance v1, Lcom/sec/vip/amschaton/fragment/ch;

    invoke-direct {v1, p0, v0}, Lcom/sec/vip/amschaton/fragment/ch;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1935
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setEmptyView(Landroid/view/View;)V

    .line 1936
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 1937
    return-void
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f()V

    return-void
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->d(I)V

    return-void
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Z)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c(Z)V

    return-void
.end method

.method private b(Ljava/io/FileOutputStream;)V
    .locals 1

    .prologue
    .line 756
    if-eqz p1, :cond_0

    .line 757
    :try_start_0
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 762
    :cond_0
    :goto_0
    return-void

    .line 759
    :catch_0
    move-exception v0

    .line 760
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 925
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->s:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 931
    :cond_0
    :goto_0
    return-void

    .line 928
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 929
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n:Landroid/widget/CheckedTextView;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 930
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->s:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 929
    goto :goto_1

    :cond_3
    move v2, v1

    .line 930
    goto :goto_2
.end method

.method private b(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1117
    if-gez p1, :cond_0

    .line 1157
    :goto_0
    return v0

    .line 1120
    :cond_0
    const/4 v1, 0x0

    .line 1122
    packed-switch p2, :pswitch_data_0

    move p1, v0

    move-object v0, v1

    .line 1146
    :goto_1
    if-eqz v0, :cond_1

    .line 1147
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(I)V

    .line 1149
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1150
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1151
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1154
    :cond_1
    const v0, 0x7f0b00f6

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(I)V

    .line 1155
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h()V

    .line 1157
    const/4 v0, 0x1

    goto :goto_0

    .line 1124
    :pswitch_0
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/vip/amschaton/al;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 1125
    const v1, 0x9c40

    add-int/2addr p1, v1

    .line 1126
    goto :goto_1

    .line 1128
    :pswitch_1
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/al;->e(I)I

    move-result v0

    .line 1129
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/vip/amschaton/al;->c(I)I

    move-result v0

    .line 1130
    add-int/lit16 v0, v0, 0x7530

    .line 1131
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(I)V

    .line 1132
    add-int/lit16 v2, p1, 0x7530

    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(I)V

    move p1, v0

    move-object v0, v1

    .line 1133
    goto :goto_1

    .line 1135
    :pswitch_2
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/al;->f(I)Ljava/lang/String;

    move-result-object v0

    .line 1136
    add-int/lit16 p1, p1, 0x4e20

    .line 1137
    goto :goto_1

    .line 1141
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(I)V

    move-object v0, v1

    .line 1142
    goto :goto_1

    .line 1122
    :pswitch_data_0
    .packed-switch 0xfa0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(Landroid/graphics/Bitmap;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 774
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 775
    const-string v1, "[saveBitmapToPNG] Wrong extension!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    :goto_0
    return v0

    .line 778
    :cond_0
    if-nez p1, :cond_1

    .line 779
    const-string v1, "[saveBitmapToPNG] Bitmap is null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 782
    :cond_1
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 784
    const/4 v4, 0x0

    .line 786
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 787
    :try_start_1
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {p1, v2, v4, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 794
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Ljava/io/FileOutputStream;)V

    .line 795
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Ljava/io/FileOutputStream;)V

    move v2, v1

    .line 798
    :goto_1
    if-nez v2, :cond_2

    .line 799
    const-string v1, "[saveBitmapToPNG] Fail to save file!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 788
    :catch_0
    move-exception v2

    move-object v3, v4

    .line 789
    :goto_2
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 790
    const-string v2, "[resizedJpegCopy] IO Exception!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[resizedJpegCopy] Out File: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Ljava/io/FileOutputStream;)V

    .line 795
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Ljava/io/FileOutputStream;)V

    move v2, v0

    .line 796
    goto :goto_1

    .line 794
    :catchall_0
    move-exception v0

    move-object v3, v4

    :goto_3
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Ljava/io/FileOutputStream;)V

    .line 795
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Ljava/io/FileOutputStream;)V

    .line 794
    throw v0

    .line 802
    :cond_2
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 803
    const-string v1, "[saveBitmapToPNG] File does not exist!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 807
    goto/16 :goto_0

    .line 794
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 788
    :catch_1
    move-exception v2

    goto :goto_2
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    return v0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)I
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    return p1
.end method

.method private c(I)V
    .locals 3

    .prologue
    const v1, 0x9c40

    .line 650
    const/16 v0, 0x4e20

    if-ge p1, v0, :cond_3

    .line 652
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/al;->i(I)I

    move-result v0

    .line 653
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 654
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    .line 669
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 670
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h(I)V

    .line 672
    :cond_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f()V

    .line 673
    return-void

    .line 655
    :cond_2
    if-ltz v0, :cond_0

    .line 656
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v1

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    goto :goto_0

    .line 658
    :cond_3
    const/16 v0, 0x7530

    if-ge p1, v0, :cond_4

    .line 660
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    add-int/lit16 v0, v0, -0x4e20

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    goto :goto_0

    .line 661
    :cond_4
    if-ge p1, v1, :cond_5

    .line 663
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    add-int/lit16 v1, p1, -0x7530

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/al;->b(I)I

    move-result v0

    .line 664
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v1

    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/vip/amschaton/al;->d(I)I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    goto :goto_0

    .line 667
    :cond_5
    sub-int v0, p1, v1

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    goto :goto_0
.end method

.method private c(Z)V
    .locals 0

    .prologue
    .line 950
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->d(Z)V

    .line 951
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f()V

    .line 952
    return-void
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Z)Z
    .locals 0

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->o:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)I
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    return p1
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)Lcom/sec/vip/amschaton/a/e;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    return-object v0
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 811
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(IJ)V

    .line 813
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 814
    const-string v1, "AMS_STAMP_INDEX"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 815
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 816
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/av;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/fragment/av;->c()V

    .line 817
    return-void
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 1339
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_1

    .line 1340
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    if-eqz v0, :cond_0

    .line 1341
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/fragment/db;->a(Z)V

    .line 1350
    :cond_0
    :goto_0
    return-void

    .line 1346
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    if-eqz v0, :cond_0

    .line 1349
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/fragment/cw;->a(Z)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Z)Z
    .locals 0

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->z:Z

    return p1
.end method

.method private e()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 681
    .line 682
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 684
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    const v1, 0x9c40

    add-int/2addr v0, v1

    .line 699
    :cond_0
    :goto_0
    return v0

    .line 685
    :cond_1
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v3

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_2

    .line 687
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/al;->e(I)I

    move-result v0

    .line 688
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/al;->c(I)I

    move-result v0

    add-int/lit16 v0, v0, 0x7530

    .line 689
    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n()I

    move-result v3

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_3

    .line 691
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit16 v0, v0, 0x4e20

    goto :goto_0

    .line 692
    :cond_3
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->o()I

    move-result v3

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_0

    .line 694
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/al;->g(I)I

    move-result v1

    .line 695
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m:I

    return v0
.end method

.method private e(I)V
    .locals 0

    .prologue
    .line 839
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m:I

    .line 840
    return-void
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(I)V

    return-void
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v0

    return v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 720
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_1

    .line 721
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    if-eqz v0, :cond_0

    .line 722
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/db;->notifyDataSetChanged()V

    .line 730
    :cond_0
    :goto_0
    return-void

    .line 727
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    if-eqz v0, :cond_0

    .line 728
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/cw;->b()V

    goto :goto_0
.end method

.method private f(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1009
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    .line 1010
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    packed-switch v0, :pswitch_data_0

    .line 1026
    :goto_0
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->j(I)V

    .line 1027
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g()V

    .line 1028
    return-void

    .line 1012
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 1013
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1015
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i()V

    goto :goto_0

    .line 1018
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 1019
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1021
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->j()V

    goto :goto_0

    .line 1010
    :pswitch_data_0
    .packed-switch 0x1388
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(I)V

    return-void
.end method

.method static synthetic g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n()I

    move-result v0

    return v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 843
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m:I

    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_0

    .line 863
    :goto_0
    return-void

    .line 847
    :cond_0
    const/4 v0, 0x0

    .line 848
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    const/16 v2, 0x1388

    if-ne v1, v2, :cond_2

    .line 849
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    if-eqz v1, :cond_1

    .line 850
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/db;->getCount()I

    move-result v0

    .line 857
    :cond_1
    :goto_1
    if-lez v0, :cond_3

    .line 858
    const/16 v0, 0x7d1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(I)V

    .line 862
    :goto_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 854
    :cond_2
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    .line 860
    :cond_3
    const/16 v0, 0x7d0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(I)V

    goto :goto_2
.end method

.method private g(I)V
    .locals 2

    .prologue
    .line 1031
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    if-nez v0, :cond_1

    .line 1035
    :cond_0
    :goto_0
    return-void

    .line 1034
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/a/e;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h(I)V

    return-void
.end method

.method static synthetic h(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->o()I

    move-result v0

    return v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 955
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->o:Z

    if-eqz v0, :cond_0

    .line 960
    :goto_0
    return-void

    .line 958
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->o:Z

    .line 959
    new-instance v0, Lcom/sec/vip/amschaton/fragment/da;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/da;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Lcom/sec/vip/amschaton/fragment/ca;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/da;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private h(I)V
    .locals 2

    .prologue
    .line 1078
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/vip/amschaton/fragment/cd;

    invoke-direct {v1, p0, p1}, Lcom/sec/vip/amschaton/fragment/cd;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1086
    return-void
.end method

.method static synthetic i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v0

    return v0
.end method

.method private i()V
    .locals 7

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    if-nez v0, :cond_1

    .line 1075
    :cond_0
    :goto_0
    return-void

    .line 1041
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/e;->c()Landroid/database/Cursor;

    move-result-object v1

    .line 1042
    if-eqz v1, :cond_0

    .line 1046
    const/4 v2, 0x0

    .line 1047
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    move v6, v0

    move v0, v2

    move v2, v6

    .line 1048
    :goto_1
    if-eqz v2, :cond_3

    .line 1049
    const-string v2, "ams_index"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1050
    const v3, 0x9c40

    if-lt v2, v3, :cond_2

    .line 1051
    const-string v3, "ams_path"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1052
    if-eqz v3, :cond_2

    .line 1053
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/sec/vip/amschaton/al;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1054
    if-eqz v3, :cond_2

    .line 1055
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1056
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1057
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(I)V

    .line 1058
    const/4 v0, 0x1

    .line 1063
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    goto :goto_1

    .line 1065
    :cond_3
    if-eqz v0, :cond_5

    .line 1066
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/e;->c()Landroid/database/Cursor;

    move-result-object v0

    .line 1069
    :goto_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    if-eqz v1, :cond_4

    .line 1070
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    .line 1072
    :cond_4
    new-instance v1, Lcom/sec/vip/amschaton/fragment/db;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/vip/amschaton/fragment/db;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    .line 1073
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1074
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/db;->a(Landroid/os/Handler;)V

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method private i(I)V
    .locals 3

    .prologue
    .line 1173
    if-gez p1, :cond_0

    .line 1182
    :goto_0
    return-void

    .line 1176
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/a/c;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/a/c;-><init>(Landroid/content/Context;)V

    .line 1177
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/c;->b()Lcom/sec/vip/amschaton/a/c;

    .line 1178
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    add-int/lit16 v2, p1, -0x7530

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/al;->e(I)I

    move-result v1

    .line 1179
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/a/c;->b(Ljava/lang/String;)Z

    .line 1180
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/c;->a()V

    goto :goto_0
.end method

.method static synthetic j(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e()I

    move-result v0

    return v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    if-eqz v0, :cond_0

    .line 1090
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    .line 1092
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/fragment/cw;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/cw;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    .line 1093
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/cw;->a(Landroid/os/Handler;)V

    .line 1094
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1095
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f()V

    .line 1096
    return-void
.end method

.method private j(I)V
    .locals 4

    .prologue
    const v3, 0x7f07014d

    const v2, 0x7f07014c

    const v1, 0x7f07014b

    .line 1939
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1956
    :goto_0
    return-void

    .line 1942
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1950
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0203d6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1951
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b03f5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1952
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1944
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02034a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1945
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b03f8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1946
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->y:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1942
    :pswitch_data_0
    .packed-switch 0x1388
        :pswitch_0
    .end packed-switch
.end method

.method private k()V
    .locals 8

    .prologue
    const v7, 0x9c40

    const/4 v1, 0x0

    .line 1188
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->l()[Z

    move-result-object v4

    .line 1189
    if-nez v4, :cond_1

    .line 1282
    :cond_0
    :goto_0
    return-void

    .line 1192
    :cond_1
    array-length v5, v4

    .line 1194
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    const/16 v2, 0x1388

    if-ne v0, v2, :cond_b

    .line 1195
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    if-eqz v0, :cond_0

    .line 1198
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/e;->c()Landroid/database/Cursor;

    move-result-object v3

    .line 1199
    if-eqz v3, :cond_2

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v0, v5, :cond_3

    .line 1200
    :cond_2
    if-eqz v3, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1201
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    move v2, v1

    .line 1206
    :goto_1
    if-ge v2, v5, :cond_9

    .line 1207
    aget-boolean v0, v4, v2

    if-eqz v0, :cond_4

    .line 1208
    invoke-interface {v3, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1212
    const-string v0, "ams_index"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1213
    const/16 v0, 0x4e20

    if-ge v1, v0, :cond_5

    .line 1216
    const/16 v0, 0xfa3

    .line 1231
    :goto_2
    if-gez v1, :cond_8

    .line 1232
    const-string v0, "Index is -1!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    :cond_4
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1217
    :cond_5
    const/16 v0, 0x7530

    if-ge v1, v0, :cond_6

    .line 1219
    add-int/lit16 v1, v1, -0x4e20

    .line 1220
    const/16 v0, 0xfa2

    goto :goto_2

    .line 1221
    :cond_6
    if-ge v1, v7, :cond_7

    .line 1223
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    add-int/lit16 v1, v1, -0x7530

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/al;->b(I)I

    move-result v0

    .line 1224
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/al;->d(I)I

    move-result v1

    .line 1225
    const/16 v0, 0xfa1

    .line 1226
    goto :goto_2

    .line 1228
    :cond_7
    sub-int/2addr v1, v7

    .line 1229
    const/16 v0, 0xfa0

    goto :goto_2

    .line 1235
    :cond_8
    invoke-direct {p0, v1, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(II)Z

    goto :goto_3

    .line 1240
    :cond_9
    if-eqz v3, :cond_a

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_a

    .line 1241
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1278
    :cond_a
    if-lez v5, :cond_0

    .line 1279
    const v0, 0x7f0b00f6

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(I)V

    .line 1280
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h()V

    goto/16 :goto_0

    :cond_b
    move v3, v1

    .line 1248
    :goto_4
    if-ge v3, v5, :cond_a

    .line 1249
    aget-boolean v0, v4, v3

    if-eqz v0, :cond_c

    .line 1250
    const/4 v0, 0x0

    .line 1252
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    if-ge v3, v2, :cond_d

    .line 1253
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/vip/amschaton/al;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 1254
    add-int v0, v3, v7

    .line 1266
    :goto_5
    if-nez v2, :cond_f

    .line 1248
    :cond_c
    :goto_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 1255
    :cond_d
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v6

    add-int/2addr v2, v6

    if-ge v3, v2, :cond_e

    .line 1256
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    sub-int v2, v3, v2

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/al;->e(I)I

    move-result v0

    .line 1257
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/vip/amschaton/al;->c(I)I

    move-result v0

    .line 1258
    add-int/lit16 v0, v0, 0x7530

    .line 1259
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(I)V

    .line 1260
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v0

    sub-int v0, v3, v0

    add-int/lit16 v0, v0, 0x7530

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(I)V

    goto :goto_6

    .line 1262
    :cond_e
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v6

    add-int/2addr v2, v6

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n()I

    move-result v6

    add-int/2addr v2, v6

    if-ge v3, v2, :cond_10

    .line 1263
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    sub-int v2, v3, v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v6

    sub-int/2addr v2, v6

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/al;->f(I)Ljava/lang/String;

    move-result-object v2

    .line 1264
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v0

    sub-int v0, v3, v0

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v6

    sub-int/2addr v0, v6

    add-int/lit16 v0, v0, 0x4e20

    goto :goto_5

    .line 1269
    :cond_f
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(I)V

    .line 1271
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1272
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1273
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_6

    :cond_10
    move-object v2, v0

    move v0, v1

    goto :goto_5
.end method

.method static synthetic k(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a()V

    return-void
.end method

.method static synthetic l(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    return-object v0
.end method

.method private l()[Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1323
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    const/16 v2, 0x1388

    if-ne v1, v2, :cond_2

    .line 1324
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    if-nez v1, :cond_1

    .line 1334
    :cond_0
    :goto_0
    return-object v0

    .line 1328
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->w:Lcom/sec/vip/amschaton/fragment/db;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/db;->a()[Z

    move-result-object v0

    goto :goto_0

    .line 1331
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    if-eqz v1, :cond_0

    .line 1334
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c:Lcom/sec/vip/amschaton/fragment/cw;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/cw;->a()[Z

    move-result-object v0

    goto :goto_0
.end method

.method private m()I
    .locals 1

    .prologue
    .line 1544
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/al;->d()I

    move-result v0

    return v0
.end method

.method static synthetic m(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b()V

    return-void
.end method

.method private n()I
    .locals 1

    .prologue
    .line 1548
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/al;->h()I

    move-result v0

    return v0
.end method

.method static synthetic n(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method private o()I
    .locals 1

    .prologue
    .line 1552
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/al;->g()I

    move-result v0

    return v0
.end method

.method static synthetic o(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q()V

    return-void
.end method

.method private p()I
    .locals 1

    .prologue
    .line 1556
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/al;->f()I

    move-result v0

    return v0
.end method

.method static synthetic p(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->D:Landroid/os/Handler;

    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 1871
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p:Lcom/sec/chaton/widget/m;

    if-eqz v0, :cond_0

    .line 1872
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p:Lcom/sec/chaton/widget/m;

    .line 1874
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p:Lcom/sec/chaton/widget/m;

    .line 1875
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p:Lcom/sec/chaton/widget/m;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/m;->setProgressStyle(I)V

    .line 1876
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p:Lcom/sec/chaton/widget/m;

    const v1, 0x7f0b00ef

    invoke-virtual {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/m;->setMessage(Ljava/lang/CharSequence;)V

    .line 1877
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p:Lcom/sec/chaton/widget/m;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/m;->show()V

    .line 1878
    return-void
.end method

.method static synthetic q(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->r()V

    return-void
.end method

.method static synthetic r(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a:I

    return v0
.end method

.method private r()V
    .locals 1

    .prologue
    .line 1881
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p:Lcom/sec/chaton/widget/m;

    if-eqz v0, :cond_0

    .line 1882
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p:Lcom/sec/chaton/widget/m;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/m;->dismiss()V

    .line 1883
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p:Lcom/sec/chaton/widget/m;

    .line 1885
    :cond_0
    return-void
.end method


# virtual methods
.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1906
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m:I

    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_0

    .line 1908
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Z)V

    .line 1909
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->d(Z)V

    .line 1910
    const/16 v0, 0x7d1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(I)V

    .line 1911
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->z:Z

    .line 1912
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f()V

    .line 1913
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Z)V

    .line 1914
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1918
    :goto_0
    return-void

    .line 1916
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/av;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/fragment/av;->c()V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 404
    if-eq p2, v4, :cond_0

    .line 456
    :goto_0
    return-void

    .line 409
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 455
    :cond_1
    :goto_1
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 411
    :pswitch_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 412
    if-eqz v0, :cond_1

    .line 416
    invoke-direct {p0, p3, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Landroid/content/Intent;Landroid/os/Bundle;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 417
    if-eqz v0, :cond_1

    .line 420
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Landroid/graphics/Bitmap;)V

    .line 421
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 424
    :pswitch_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 425
    if-eqz v0, :cond_1

    .line 428
    const-string v1, "AMS_EMOTICON_INDEX"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 429
    add-int/lit16 v1, v1, 0x7530

    .line 430
    const-string v2, "AMS_STAMP_INDEX"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 432
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(IJ)V

    .line 433
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(IJ)V

    .line 434
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/al;->c()V

    .line 436
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 437
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 438
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 439
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/av;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/fragment/av;->c()V

    goto :goto_1

    .line 442
    :pswitch_2
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 443
    if-eqz v0, :cond_1

    .line 446
    const-string v1, "AMS_STAMP_INDEX"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 447
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(IJ)V

    .line 449
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 450
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 451
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 452
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/av;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/fragment/av;->c()V

    goto/16 :goto_1

    .line 409
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1889
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1903
    :cond_0
    :goto_0
    return-void

    .line 1892
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1893
    const/16 v0, 0x1388

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(I)V

    goto :goto_0

    .line 1897
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1898
    const/16 v0, 0x1389

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(I)V

    goto :goto_0

    .line 1889
    nop

    :pswitch_data_0
    .packed-switch 0x7f070267
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    const v7, 0x9c40

    const/16 v3, 0xfa2

    const/16 v2, 0xfa1

    const/16 v1, 0xfa0

    const/4 v0, 0x1

    .line 326
    iget v4, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    const/16 v5, 0x1388

    if-ne v4, v5, :cond_6

    .line 327
    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    if-nez v4, :cond_1

    .line 399
    :cond_0
    :goto_0
    return v0

    .line 330
    :cond_1
    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    iget-wide v5, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->r:J

    invoke-virtual {v4, v5, v6}, Lcom/sec/vip/amschaton/a/e;->b(J)Landroid/database/Cursor;

    move-result-object v4

    .line 331
    if-eqz v4, :cond_0

    .line 336
    const-string v5, "ams_index"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 337
    const/16 v5, 0x4e20

    if-ge v4, v5, :cond_2

    .line 340
    const/16 v3, 0xfa3

    move v1, v4

    .line 355
    :goto_1
    if-gez v1, :cond_5

    .line 356
    const-string v1, "Index is -1!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 341
    :cond_2
    const/16 v5, 0x7530

    if-ge v4, v5, :cond_3

    .line 343
    add-int/lit16 v1, v4, -0x4e20

    .line 344
    goto :goto_1

    .line 345
    :cond_3
    if-ge v4, v7, :cond_4

    .line 347
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    add-int/lit16 v3, v4, -0x7530

    invoke-virtual {v1, v3}, Lcom/sec/vip/amschaton/al;->b(I)I

    move-result v1

    .line 348
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/vip/amschaton/al;->d(I)I

    move-result v1

    move v3, v2

    .line 350
    goto :goto_1

    .line 352
    :cond_4
    sub-int v2, v4, v7

    move v3, v1

    move v1, v2

    .line 353
    goto :goto_1

    .line 359
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 367
    :goto_2
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 361
    :pswitch_0
    invoke-direct {p0, v1, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(II)V

    goto :goto_2

    .line 372
    :cond_6
    iget v4, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    if-lez v4, :cond_7

    iget v4, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    if-ge v4, v5, :cond_7

    .line 374
    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    add-int/lit8 v2, v2, -0x1

    .line 387
    :goto_3
    if-gez v2, :cond_9

    .line 388
    const-string v1, "Index is -1!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 376
    :cond_7
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v4

    if-le v1, v4, :cond_8

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v4

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    if-ge v1, v4, :cond_8

    .line 378
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v3

    sub-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    move v8, v2

    move v2, v1

    move v1, v8

    .line 379
    goto :goto_3

    .line 380
    :cond_8
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v4

    add-int/2addr v2, v4

    if-le v1, v2, :cond_0

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v4

    add-int/2addr v2, v4

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->n()I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_0

    .line 382
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->q:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->p()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    move v1, v3

    .line 383
    goto :goto_3

    .line 391
    :cond_9
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 399
    :goto_4
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto/16 :goto_0

    .line 393
    :pswitch_1
    invoke-direct {p0, v2, v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(II)V

    goto :goto_4

    .line 359
    nop

    :pswitch_data_0
    .packed-switch 0x7f070588
        :pswitch_0
    .end packed-switch

    .line 391
    :pswitch_data_1
    .packed-switch 0x7f070588
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 132
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 133
    new-instance v0, Lcom/sec/vip/amschaton/a/e;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/a/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    .line 134
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/e;->b()Lcom/sec/vip/amschaton/a/e;

    .line 136
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreate(Landroid/os/Bundle;)V

    .line 137
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b:Landroid/widget/GridView;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    const v0, 0x7f0b0121

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 315
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 316
    const v1, 0x7f0f000d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 317
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_0

    .line 318
    const v0, 0x7f070588

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 321
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 322
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5

    .prologue
    const v2, 0x7f0705a6

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 186
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m:I

    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_1

    .line 187
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00fe

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 188
    const v0, 0x7f0f0021

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 190
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->z:Z

    if-nez v0, :cond_0

    .line 191
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 215
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 216
    return-void

    .line 194
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 198
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0121

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 199
    const v0, 0x7f0f000e

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 200
    const v0, 0x7f070589

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 202
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->m:I

    const/16 v2, 0x7d0

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->v:I

    const/16 v2, 0x1388

    if-ne v1, v2, :cond_3

    .line 203
    :cond_2
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 204
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 212
    :goto_1
    const v0, 0x7f07058a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 213
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Landroid/view/MenuItem;)V

    goto :goto_0

    .line 207
    :cond_3
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 208
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 148
    const v0, 0x7f03005d

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 150
    const v0, 0x7f070269

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 151
    const v0, 0x7f07026c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 154
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    .line 157
    const-string v3, "AMS_STAMP_INDEX"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 162
    :goto_0
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Landroid/view/View;)V

    .line 163
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c(I)V

    .line 165
    const v0, 0x7f070266

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->s:Landroid/widget/LinearLayout;

    .line 166
    const v0, 0x7f070267

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->t:Landroid/view/View;

    .line 167
    const v0, 0x7f070268

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->u:Landroid/view/View;

    .line 168
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->t:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->u:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    const/16 v0, 0x1389

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(I)V

    .line 173
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Landroid/view/View;)V

    .line 181
    return-object v2

    .line 159
    :cond_0
    const-string v0, "[onCreate] bundle is NULL!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 294
    const-string v0, "[onDestroy]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->x:Lcom/sec/vip/amschaton/a/e;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/e;->a()V

    .line 299
    :cond_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->r()V

    .line 301
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onDestroy()V

    .line 302
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 220
    .line 221
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 222
    const v2, 0x7f0705a5

    if-ne v1, v2, :cond_2

    .line 223
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c()V

    move v0, v3

    .line 283
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 284
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 287
    :cond_1
    return v0

    .line 225
    :cond_2
    const v2, 0x7f070589

    if-ne v1, v2, :cond_3

    .line 226
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Z)V

    .line 227
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Z)V

    .line 228
    const/16 v0, 0x7d2

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(I)V

    .line 233
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f()V

    .line 234
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    move v0, v3

    .line 235
    goto :goto_0

    .line 236
    :cond_3
    const v2, 0x7f0705a6

    if-ne v1, v2, :cond_7

    .line 237
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->a(Z)V

    .line 240
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b03f6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 241
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->l()[Z

    move-result-object v4

    .line 242
    if-eqz v4, :cond_5

    move v1, v0

    .line 243
    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_6

    .line 244
    aget-boolean v5, v4, v0

    if-ne v5, v3, :cond_4

    .line 245
    add-int/lit8 v1, v1, 0x1

    .line 243
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v1, v0

    .line 249
    :cond_6
    if-le v1, v3, :cond_8

    .line 250
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b023d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 254
    :goto_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b00fe

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fc

    new-instance v2, Lcom/sec/vip/amschaton/fragment/ci;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/ci;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fd

    new-instance v2, Lcom/sec/vip/amschaton/fragment/ca;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/ca;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 275
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    move v0, v3

    .line 277
    goto/16 :goto_0

    :cond_7
    const v2, 0x7f07058a

    if-ne v1, v2, :cond_0

    .line 278
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/downloads/ActivityAmsItemDownloads;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 279
    const-string v1, "amsType"

    sget-object v2, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 280
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->startActivity(Landroid/content/Intent;)V

    move v0, v3

    .line 281
    goto/16 :goto_0

    :cond_8
    move-object v0, v2

    goto :goto_2
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 306
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h()V

    .line 307
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onResume()V

    .line 308
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 142
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onStart()V

    .line 143
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0121

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 144
    return-void
.end method
