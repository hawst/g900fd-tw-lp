.class public Lcom/sec/vip/amschaton/AMSStampSelectionActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "AMSStampSelectionActivity.java"

# interfaces
.implements Lcom/sec/vip/amschaton/fragment/av;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;-><init>()V

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSStampSelectionActivity;->finish()V

    .line 61
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSStampSelectionActivity;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/AMSFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->c()V

    .line 66
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSStampSelectionActivity;->supportInvalidateOptionsMenu()V

    .line 67
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 28
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 31
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 35
    const v0, 0x7f09015d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 36
    const v2, 0x7f09015e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 38
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 39
    const/4 v0, -0x1

    .line 42
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSStampSelectionActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/view/Window;->setLayout(II)V

    .line 45
    :cond_1
    return-void
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 50
    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSStampSelectionActivity;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/AMSFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->c()V

    .line 52
    const/4 v0, 0x1

    .line 54
    :cond_0
    return v0
.end method
