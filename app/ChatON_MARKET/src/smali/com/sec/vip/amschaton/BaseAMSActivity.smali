.class public Lcom/sec/vip/amschaton/BaseAMSActivity;
.super Lcom/sec/chaton/base/BaseSinglePaneActivity;
.source "BaseAMSActivity.java"

# interfaces
.implements Lcom/sec/vip/amschaton/fragment/av;


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f070013

    const-string v2, "com.sec.chaton:single_pane_fragment"

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 125
    return-void
.end method

.method private c(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 26
    .line 29
    if-eqz p1, :cond_3

    .line 30
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 32
    :goto_0
    if-nez v1, :cond_1

    .line 33
    iput v4, p0, Lcom/sec/vip/amschaton/BaseAMSActivity;->a:I

    .line 34
    new-instance v0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;-><init>()V

    .line 81
    :goto_1
    :pswitch_0
    if-eqz v0, :cond_0

    .line 82
    invoke-static {p1}, Lcom/sec/vip/amschaton/BaseAMSActivity;->b(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 84
    :cond_0
    return-object v0

    .line 37
    :cond_1
    const-string v2, "AMS_ACTION"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 38
    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 39
    iget v1, p0, Lcom/sec/vip/amschaton/BaseAMSActivity;->a:I

    packed-switch v1, :pswitch_data_0

    .line 52
    :pswitch_1
    iput v4, p0, Lcom/sec/vip/amschaton/BaseAMSActivity;->a:I

    .line 53
    new-instance v0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;-><init>()V

    goto :goto_1

    .line 43
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->finish()V

    goto :goto_1

    .line 47
    :pswitch_3
    new-instance v0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;-><init>()V

    goto :goto_1

    .line 57
    :cond_2
    iput v1, p0, Lcom/sec/vip/amschaton/BaseAMSActivity;->a:I

    .line 58
    iget v1, p0, Lcom/sec/vip/amschaton/BaseAMSActivity;->a:I

    packed-switch v1, :pswitch_data_1

    .line 76
    iput v4, p0, Lcom/sec/vip/amschaton/BaseAMSActivity;->a:I

    .line 77
    new-instance v0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;-><init>()V

    goto :goto_1

    .line 60
    :pswitch_4
    new-instance v0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;-><init>()V

    goto :goto_1

    .line 63
    :pswitch_5
    new-instance v0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;-><init>()V

    goto :goto_1

    .line 67
    :pswitch_6
    new-instance v0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;-><init>()V

    goto :goto_1

    .line 73
    :pswitch_7
    new-instance v0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;-><init>()V

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 58
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 90
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->c(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 95
    return-object v0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 146
    const-string v0, "AMS_ACTION"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 147
    if-le v0, v1, :cond_1

    .line 148
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/BaseAMSActivity;->c(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 150
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->finish()V

    .line 179
    return-void
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/vip/amschaton/BaseAMSActivity;->a:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 135
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/vip/amschaton/AMSFileListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 136
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 137
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 138
    const-string v1, "ACTION"

    const/16 v2, 0x7d6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 139
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->startActivity(Landroid/content/Intent;)V

    .line 141
    :cond_0
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->finish()V

    .line 142
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/AMSFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->c()V

    .line 173
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->supportInvalidateOptionsMenu()V

    .line 174
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 101
    invoke-super {p0}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onResume()V

    .line 104
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 108
    const v0, 0x7f09015d

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 109
    const v0, 0x7f09015e

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 111
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 117
    :goto_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/view/Window;->setLayout(II)V

    .line 120
    :cond_0
    return-void

    :cond_1
    move v0, v1

    move v1, v2

    .line 114
    goto :goto_0
.end method

.method public onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 161
    invoke-super {p0, p1}, Lcom/sec/chaton/base/BaseSinglePaneActivity;->onSupportOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 162
    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 163
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->b()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/AMSFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->c()V

    .line 164
    const/4 v0, 0x1

    .line 166
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/BaseAMSActivity;->supportInvalidateOptionsMenu()V

    .line 167
    return v0
.end method
