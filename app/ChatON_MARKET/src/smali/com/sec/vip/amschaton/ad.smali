.class public Lcom/sec/vip/amschaton/ad;
.super Ljava/lang/Object;
.source "AMSSendDialog.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/view/View;

.field private c:Lcom/sec/common/a/d;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/CheckBox;

.field private f:Landroid/widget/TextView;

.field private g:Z

.field private h:Lcom/sec/vip/amschaton/ak;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v2, p0, Lcom/sec/vip/amschaton/ad;->d:Landroid/widget/LinearLayout;

    .line 32
    iput-object v2, p0, Lcom/sec/vip/amschaton/ad;->e:Landroid/widget/CheckBox;

    .line 33
    iput-object v2, p0, Lcom/sec/vip/amschaton/ad;->f:Landroid/widget/TextView;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/ad;->g:Z

    .line 41
    iput-object v2, p0, Lcom/sec/vip/amschaton/ad;->h:Lcom/sec/vip/amschaton/ak;

    .line 52
    iput-object p1, p0, Lcom/sec/vip/amschaton/ad;->a:Landroid/content/Context;

    .line 54
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 55
    const v1, 0x7f030062

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ad;->b:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->b:Landroid/view/View;

    const v1, 0x7f070285

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/ad;->d:Landroid/widget/LinearLayout;

    .line 58
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->b:Landroid/view/View;

    const v1, 0x7f070286

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/vip/amschaton/ad;->e:Landroid/widget/CheckBox;

    .line 59
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->b:Landroid/view/View;

    const v1, 0x7f070287

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/ad;->f:Landroid/widget/TextView;

    .line 62
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ad;->c()V

    .line 63
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->e:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/vip/amschaton/ad;->g:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 66
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->e:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/vip/amschaton/ae;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/ae;-><init>(Lcom/sec/vip/amschaton/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->e:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/vip/amschaton/af;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/af;-><init>(Lcom/sec/vip/amschaton/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->f:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/vip/amschaton/ag;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/ag;-><init>(Lcom/sec/vip/amschaton/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->f:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/vip/amschaton/ah;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/ah;-><init>(Lcom/sec/vip/amschaton/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 115
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/ad;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->e:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/ad;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/ad;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/ad;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ad;->g:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/ad;)Lcom/sec/vip/amschaton/ak;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->h:Lcom/sec/vip/amschaton/ak;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->a:Landroid/content/Context;

    const-string v1, "AMSPref"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 191
    const-string v1, "AMS_SEND_DIALOG_WITH_MESSAGE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/ad;->g:Z

    .line 192
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->a:Landroid/content/Context;

    const-string v1, "AMSPref"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 196
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AMS_SEND_DIALOG_WITH_MESSAGE"

    iget-boolean v2, p0, Lcom/sec/vip/amschaton/ad;->g:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 197
    return-void
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/ad;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ad;->d()V

    return-void
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/ad;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->c:Lcom/sec/common/a/d;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 119
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/ad;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00eb

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/vip/amschaton/ai;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/ai;-><init>(Lcom/sec/vip/amschaton/ad;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ad;->c:Lcom/sec/common/a/d;

    .line 131
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->c:Lcom/sec/common/a/d;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/common/a/d;->setCanceledOnTouchOutside(Z)V

    .line 132
    return-void
.end method

.method public a(Lcom/sec/vip/amschaton/ak;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/vip/amschaton/ad;->h:Lcom/sec/vip/amschaton/ak;

    .line 147
    return-void
.end method

.method public a(IIZ)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 150
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->d:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    move v0, v2

    .line 185
    :goto_0
    return v0

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 154
    const v1, 0x7f030063

    iget-object v3, p0, Lcom/sec/vip/amschaton/ad;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 155
    const v1, 0x7f070288

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 156
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 157
    invoke-virtual {v3, p1}, Landroid/view/View;->setId(I)V

    .line 159
    const v1, 0x7f0202dc

    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 160
    new-instance v1, Lcom/sec/vip/amschaton/aj;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/aj;-><init>(Lcom/sec/vip/amschaton/ad;)V

    invoke-virtual {v3, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v1, p0, Lcom/sec/vip/amschaton/ad;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 172
    if-eqz p3, :cond_1

    .line 175
    const v1, 0x7f030083

    iget-object v3, p0, Lcom/sec/vip/amschaton/ad;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 176
    iget-object v1, p0, Lcom/sec/vip/amschaton/ad;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 185
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->c:Lcom/sec/common/a/d;

    if-nez v0, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/ad;->a()V

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/ad;->c:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 139
    return-void
.end method
