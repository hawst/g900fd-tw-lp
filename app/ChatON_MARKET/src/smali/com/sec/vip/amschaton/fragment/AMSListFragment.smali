.class public Lcom/sec/vip/amschaton/fragment/AMSListFragment;
.super Lcom/sec/vip/amschaton/fragment/AMSFragment;
.source "AMSListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private A:Z

.field private B:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private final C:Landroid/view/View$OnClickListener;

.field private D:Landroid/os/Handler;

.field private E:Landroid/widget/AdapterView$OnItemClickListener;

.field private F:Landroid/os/Handler;

.field private a:I

.field private b:I

.field private c:J

.field private m:Landroid/widget/GridView;

.field private n:Lcom/sec/vip/amschaton/fragment/bt;

.field private o:Landroid/widget/CheckedTextView;

.field private p:Z

.field private q:Lcom/sec/chaton/widget/m;

.field private r:Landroid/widget/LinearLayout;

.field private s:Landroid/view/View;

.field private t:Landroid/view/View;

.field private u:I

.field private v:Lcom/sec/vip/amschaton/fragment/br;

.field private w:Lcom/sec/vip/amschaton/a/f;

.field private x:Landroid/view/View;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 62
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;-><init>()V

    .line 76
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a:I

    .line 79
    iput v3, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    .line 80
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c:J

    .line 82
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    .line 83
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    .line 87
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->o:Landroid/widget/CheckedTextView;

    .line 90
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->p:Z

    .line 93
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    .line 96
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->r:Landroid/widget/LinearLayout;

    .line 97
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->s:Landroid/view/View;

    .line 98
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->t:Landroid/view/View;

    .line 99
    const/16 v0, 0xfa1

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    .line 101
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    .line 102
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    .line 103
    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    .line 104
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->y:Z

    .line 106
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->z:Z

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->A:Z

    .line 632
    new-instance v0, Lcom/sec/vip/amschaton/fragment/bf;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/bf;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->B:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 660
    new-instance v0, Lcom/sec/vip/amschaton/fragment/bg;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/bg;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->C:Landroid/view/View$OnClickListener;

    .line 717
    new-instance v0, Lcom/sec/vip/amschaton/fragment/bh;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/bh;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->D:Landroid/os/Handler;

    .line 987
    new-instance v0, Lcom/sec/vip/amschaton/fragment/bj;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/bj;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->E:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1427
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ax;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/ax;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->F:Landroid/os/Handler;

    .line 1628
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;J)J
    .locals 0

    .prologue
    .line 62
    iput-wide p1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c:J

    return-wide p1
.end method

.method private a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 599
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fe

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b03f6

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fc

    new-instance v2, Lcom/sec/vip/amschaton/fragment/be;

    invoke-direct {v2, p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/be;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;ILjava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fd

    new-instance v2, Lcom/sec/vip/amschaton/fragment/bd;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/bd;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 630
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 762
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    .line 763
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    packed-switch v0, :pswitch_data_0

    .line 779
    :goto_0
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(I)V

    .line 780
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->g()V

    .line 781
    return-void

    .line 765
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 766
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 768
    invoke-direct {p0, p2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->f(Z)V

    goto :goto_0

    .line 771
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 772
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 774
    invoke-direct {p0, p2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->g(Z)V

    goto :goto_0

    .line 763
    :pswitch_data_0
    .packed-switch 0xfa0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    .line 739
    sget-object v0, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v0

    .line 740
    invoke-static {p1}, Lcom/sec/chaton/util/cp;->b(Landroid/view/MenuItem;)V

    .line 742
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p1, v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;Z)V

    .line 759
    return-void

    .line 742
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, 0x42dc0000    # 110.0f

    const v2, 0x7f07014b

    .line 854
    const v0, 0x7f07026b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    .line 855
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v3}, Lcom/sec/chaton/util/an;->b(F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 856
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v3}, Lcom/sec/chaton/util/an;->b(F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 857
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 858
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b030f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 860
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 861
    new-instance v1, Lcom/sec/vip/amschaton/fragment/bi;

    invoke-direct {v1, p0, v0}, Lcom/sec/vip/amschaton/fragment/bi;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 868
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setEmptyView(Landroid/view/View;)V

    .line 869
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 870
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b()V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;I)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;IZ)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(IZ)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 822
    if-nez p1, :cond_0

    .line 831
    :goto_0
    return-void

    .line 830
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/a/f;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 459
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->o:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->r:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->z:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->z:Z

    .line 464
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->o:Landroid/widget/CheckedTextView;

    iget-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->z:Z

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 466
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->o:Landroid/widget/CheckedTextView;

    if-eqz p1, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 468
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->r:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_4

    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 463
    goto :goto_1

    :cond_3
    move v0, v2

    .line 466
    goto :goto_2

    :cond_4
    move v2, v1

    .line 468
    goto :goto_3
.end method

.method private a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 244
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->k()[Z

    move-result-object v2

    .line 245
    if-eqz v2, :cond_0

    .line 246
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-boolean v4, v2, v1

    .line 247
    if-eqz v4, :cond_1

    .line 248
    const/4 v0, 0x1

    .line 252
    :cond_0
    return v0

    .line 246
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    return v0
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSListFragment;I)I
    .locals 0

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    return p1
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 500
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 501
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 504
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->k()[Z

    move-result-object v5

    .line 505
    if-nez v5, :cond_1

    .line 595
    :cond_0
    :goto_0
    return-void

    .line 509
    :cond_1
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    const/16 v2, 0xfa0

    if-ne v0, v2, :cond_9

    .line 510
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/f;->c()Landroid/database/Cursor;

    move-result-object v2

    .line 512
    array-length v0, v5

    .line 514
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-eq v6, v0, :cond_3

    .line 515
    :cond_2
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 521
    :goto_1
    array-length v6, v5

    if-ge v0, v6, :cond_5

    .line 522
    aget-boolean v6, v5, v0

    if-eqz v6, :cond_4

    .line 523
    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 524
    const-string v6, "ams_path"

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 525
    const-string v7, "ams_type"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 526
    packed-switch v7, :pswitch_data_0

    .line 521
    :cond_4
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 528
    :pswitch_0
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 531
    :pswitch_1
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 542
    :cond_5
    if-eqz v2, :cond_6

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_6

    .line 543
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 580
    :cond_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 581
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 582
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    .line 584
    :cond_7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 585
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 586
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/vip/amschaton/q;->a([Ljava/lang/String;)Z

    .line 593
    :cond_8
    const v0, 0x7f0b00f6

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(I)V

    .line 594
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Z)V

    goto/16 :goto_0

    .line 549
    :cond_9
    array-length v6, v5

    move v2, v1

    .line 550
    :goto_3
    if-ge v2, v6, :cond_6

    .line 551
    aget-boolean v0, v5, v2

    if-eqz v0, :cond_b

    .line 553
    const/4 v0, 0x0

    .line 554
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h()I

    move-result v7

    .line 555
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->i()I

    move-result v8

    .line 557
    if-ge v2, v7, :cond_c

    .line 558
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v0, v7, v2}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 559
    if-eqz v0, :cond_a

    .line 560
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    :cond_a
    :goto_4
    if-nez v0, :cond_d

    .line 550
    :cond_b
    :goto_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 562
    :cond_c
    add-int/2addr v8, v7

    if-ge v2, v8, :cond_a

    .line 563
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    sub-int v7, v2, v7

    invoke-virtual {v0, v7}, Lcom/sec/vip/amschaton/q;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 564
    if-eqz v0, :cond_a

    .line 565
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 576
    :cond_d
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Ljava/lang/String;)V

    goto :goto_5

    .line 526
    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private b(I)V
    .locals 4

    .prologue
    const v3, 0x7f07014d

    const v2, 0x7f07014c

    const v1, 0x7f07014b

    .line 834
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    if-nez v0, :cond_0

    .line 851
    :goto_0
    return-void

    .line 837
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 845
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020352

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 846
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b030e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 847
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 839
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02034a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 840
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b0131

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 841
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 837
    :pswitch_data_0
    .packed-switch 0xfa0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    if-nez v0, :cond_1

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 475
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 476
    if-eqz p1, :cond_0

    .line 477
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->E:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    return-object v0
.end method

.method private c(I)V
    .locals 0

    .prologue
    .line 938
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a:I

    .line 939
    return-void
.end method

.method private c(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1471
    new-instance v0, Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1472
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Z)V

    .line 1473
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Ljava/lang/String;I)V

    .line 1488
    :goto_0
    return-void

    .line 1475
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0125

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fc

    new-instance v2, Lcom/sec/vip/amschaton/fragment/az;

    invoke-direct {v2, p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/az;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fd

    new-instance v2, Lcom/sec/vip/amschaton/fragment/ay;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/ay;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 483
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    const/16 v1, 0xfa0

    if-ne v0, v1, :cond_1

    .line 484
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/fragment/br;->a(Z)V

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 490
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/fragment/bt;->a(Z)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)Z
    .locals 0

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->A:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a:I

    return v0
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->d(Z)V

    return-void
.end method

.method private d(Ljava/lang/String;I)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1491
    new-instance v0, Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1492
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Z)V

    move-object v0, p0

    move-object v1, p1

    move v4, v2

    move v5, v3

    .line 1493
    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Ljava/lang/String;ZZZZ)V

    .line 1508
    :goto_0
    return-void

    .line 1495
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0125

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fc

    new-instance v2, Lcom/sec/vip/amschaton/fragment/bb;

    invoke-direct {v2, p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/bb;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fd

    new-instance v2, Lcom/sec/vip/amschaton/fragment/ba;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/ba;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private d(Z)V
    .locals 0

    .prologue
    .line 695
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(Z)V

    .line 696
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->l()V

    .line 697
    return-void
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h()I

    move-result v0

    return v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 874
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    if-eqz v0, :cond_0

    .line 875
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    .line 877
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    .line 878
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/m;->setProgressStyle(I)V

    .line 879
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    const v1, 0x7f0b00ef

    invoke-virtual {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/m;->setMessage(Ljava/lang/CharSequence;)V

    .line 880
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/m;->show()V

    .line 881
    return-void
.end method

.method private e(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 703
    if-nez p1, :cond_1

    .line 704
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->D:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 712
    :cond_0
    :goto_0
    return-void

    .line 707
    :cond_1
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->p:Z

    if-nez v0, :cond_0

    .line 710
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->p:Z

    .line 711
    new-instance v0, Lcom/sec/vip/amschaton/fragment/bq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/bq;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Lcom/sec/vip/amschaton/fragment/aw;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/bq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)Z
    .locals 0

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->z:Z

    return p1
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->i()I

    move-result v0

    return v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 884
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/m;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 885
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/m;->dismiss()V

    .line 887
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->q:Lcom/sec/chaton/widget/m;

    .line 888
    return-void
.end method

.method private f(Z)V
    .locals 5

    .prologue
    .line 784
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    if-nez v0, :cond_1

    .line 818
    :cond_0
    :goto_0
    return-void

    .line 787
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_5

    .line 788
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/f;->c()Landroid/database/Cursor;

    move-result-object v1

    .line 789
    if-eqz v1, :cond_0

    .line 794
    const/4 v2, 0x0

    .line 795
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    move v4, v0

    move v0, v2

    move v2, v4

    .line 796
    :goto_1
    if-eqz v2, :cond_4

    .line 797
    const-string v2, "ams_path"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 798
    if-eqz v2, :cond_3

    .line 799
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 800
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 801
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Ljava/lang/String;)V

    .line 802
    const/4 v0, 0x1

    .line 805
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    goto :goto_1

    .line 807
    :cond_4
    if-eqz v0, :cond_6

    .line 808
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/f;->c()Landroid/database/Cursor;

    move-result-object v0

    .line 813
    :goto_2
    new-instance v1, Lcom/sec/vip/amschaton/fragment/br;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/vip/amschaton/fragment/br;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    .line 816
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 817
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->F:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/br;->a(Landroid/os/Handler;)V

    goto :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)Z
    .locals 0

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->p:Z

    return p1
.end method

.method static synthetic g(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->j()I

    move-result v0

    return v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 909
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a:I

    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_1

    .line 935
    :cond_0
    :goto_0
    return-void

    .line 913
    :cond_1
    const/4 v0, 0x0

    .line 914
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    const/16 v2, 0xfa0

    if-ne v1, v2, :cond_3

    .line 915
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    if-eqz v1, :cond_2

    .line 916
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/br;->getCount()I

    move-result v0

    .line 923
    :cond_2
    :goto_1
    if-lez v0, :cond_4

    .line 924
    const/16 v0, 0x7d1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(I)V

    .line 932
    :goto_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 933
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 920
    :cond_3
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->i()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    .line 928
    :cond_4
    const/16 v0, 0x7d0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(I)V

    goto :goto_2
.end method

.method static synthetic g(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Z)V

    return-void
.end method

.method private g(Z)V
    .locals 2

    .prologue
    .line 894
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    if-nez v0, :cond_0

    .line 906
    :goto_0
    return-void

    .line 897
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_2

    .line 899
    :cond_1
    new-instance v0, Lcom/sec/vip/amschaton/fragment/bt;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/bt;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    .line 902
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->F:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/bt;->a(Landroid/os/Handler;)V

    .line 903
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 905
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->l()V

    goto :goto_0
.end method

.method private h()I
    .locals 1

    .prologue
    .line 942
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/q;->c()I

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->o:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)Z
    .locals 0

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->y:Z

    return p1
.end method

.method private i()I
    .locals 1

    .prologue
    .line 946
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/q;->e()I

    move-result v0

    return v0
.end method

.method static synthetic i(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)Lcom/sec/vip/amschaton/a/f;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    return-object v0
.end method

.method private j()I
    .locals 1

    .prologue
    .line 950
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/q;->d()I

    move-result v0

    return v0
.end method

.method static synthetic j(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e()V

    return-void
.end method

.method static synthetic k(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->D:Landroid/os/Handler;

    return-object v0
.end method

.method private k()[Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 955
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    const/16 v2, 0xfa0

    if-ne v1, v2, :cond_2

    .line 956
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    if-nez v1, :cond_1

    .line 967
    :cond_0
    :goto_0
    return-object v0

    .line 960
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/br;->a()[Z

    move-result-object v0

    goto :goto_0

    .line 964
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    if-eqz v1, :cond_0

    .line 967
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/bt;->a()[Z

    move-result-object v0

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 972
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    const/16 v1, 0xfa0

    if-ne v0, v1, :cond_1

    .line 973
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    if-eqz v0, :cond_0

    .line 974
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->v:Lcom/sec/vip/amschaton/fragment/br;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/br;->notifyDataSetChanged()V

    .line 984
    :cond_0
    :goto_0
    return-void

    .line 980
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    if-eqz v0, :cond_0

    .line 983
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->n:Lcom/sec/vip/amschaton/fragment/bt;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/bt;->b()V

    goto :goto_0
.end method

.method static synthetic l(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->f()V

    return-void
.end method


# virtual methods
.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 334
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a:I

    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_0

    .line 336
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Z)V

    .line 338
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(Z)V

    .line 339
    const/16 v0, 0x7d1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(I)V

    .line 340
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->l()V

    .line 342
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Z)V

    .line 343
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->y:Z

    .line 345
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 349
    :goto_0
    return-void

    .line 347
    :cond_0
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->c()V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 127
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->setRetainInstance(Z)V

    .line 129
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 353
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 354
    const/16 v0, 0xc9

    if-ne p1, v0, :cond_0

    .line 355
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Z)V

    .line 359
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 679
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 692
    :cond_0
    :goto_0
    return-void

    .line 682
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->s:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eq v0, v1, :cond_0

    .line 683
    const/16 v0, 0xfa0

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(IZ)V

    goto :goto_0

    .line 687
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eq v0, v1, :cond_0

    .line 688
    const/16 v0, 0xfa1

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(IZ)V

    goto :goto_0

    .line 679
    nop

    :pswitch_data_0
    .packed-switch 0x7f070267
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 379
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    const/16 v2, 0xfa0

    if-ne v1, v2, :cond_3

    .line 380
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    if-nez v1, :cond_1

    .line 454
    :cond_0
    :goto_0
    return v0

    .line 383
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    iget-wide v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/a/f;->b(J)Landroid/database/Cursor;

    move-result-object v1

    .line 384
    if-eqz v1, :cond_0

    .line 387
    const-string v2, "ams_path"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 388
    const-string v3, "ams_type"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 389
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 391
    if-nez v2, :cond_2

    .line 392
    const-string v1, "amsFile is NULL!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 395
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 413
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 397
    :pswitch_0
    invoke-direct {p0, v2, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(Ljava/lang/String;I)V

    goto :goto_1

    .line 400
    :pswitch_1
    invoke-virtual {p0, v2, v4, v1, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_1

    .line 403
    :pswitch_2
    invoke-direct {p0, v2, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->d(Ljava/lang/String;I)V

    goto :goto_1

    .line 406
    :pswitch_3
    invoke-direct {p0, v1, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 418
    :cond_3
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    if-lez v1, :cond_4

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_4

    .line 419
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 420
    const/16 v1, 0x7d2

    .line 430
    :goto_2
    if-nez v2, :cond_6

    .line 431
    const-string v1, "amsFile is NULL!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 421
    :cond_4
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h()I

    move-result v2

    if-le v1, v2, :cond_5

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->i()I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_5

    .line 422
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/q;->b(I)Ljava/lang/String;

    move-result-object v2

    .line 423
    const/16 v1, 0x7d1

    goto :goto_2

    .line 424
    :cond_5
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->i()I

    move-result v3

    add-int/2addr v2, v3

    if-le v1, v2, :cond_0

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->i()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->j()I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_0

    .line 425
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b:I

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->i()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/q;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 426
    const/16 v1, 0x7d0

    goto :goto_2

    .line 434
    :cond_6
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    .line 454
    :goto_3
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto/16 :goto_0

    .line 437
    :pswitch_4
    invoke-direct {p0, v2, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(Ljava/lang/String;I)V

    goto :goto_3

    .line 441
    :pswitch_5
    invoke-virtual {p0, v2, v4, v1, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_3

    .line 444
    :pswitch_6
    invoke-direct {p0, v2, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->d(Ljava/lang/String;I)V

    goto :goto_3

    .line 447
    :pswitch_7
    invoke-direct {p0, v1, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(ILjava/lang/String;)V

    goto :goto_3

    .line 395
    :pswitch_data_0
    .packed-switch 0x7f070585
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 434
    :pswitch_data_1
    .packed-switch 0x7f070585
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 110
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreate(Landroid/os/Bundle;)V

    .line 112
    invoke-static {p0, v2}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 114
    new-instance v0, Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/a/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    .line 115
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/f;->b()Lcom/sec/vip/amschaton/a/f;

    .line 118
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Z)V

    .line 119
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 365
    const v0, 0x7f0b00ea

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 367
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 368
    const v1, 0x7f0f000c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 369
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    const/16 v1, 0xfa0

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->A:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 371
    :cond_0
    const v0, 0x7f070588

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 374
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 375
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5

    .prologue
    const v2, 0x7f0705a6

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 195
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a:I

    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_1

    .line 196
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00fe

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 197
    const v0, 0x7f0f0021

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 199
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->y:Z

    if-nez v0, :cond_0

    .line 200
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 226
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 227
    return-void

    .line 203
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 207
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0051

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 208
    const v0, 0x7f0f000e

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 210
    const v0, 0x7f070589

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 212
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a:I

    const/16 v2, 0x7d0

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    const/16 v2, 0xfa0

    if-ne v1, v2, :cond_3

    .line 213
    :cond_2
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 214
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 222
    :goto_1
    const v0, 0x7f07058a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 223
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Landroid/view/MenuItem;)V

    goto :goto_0

    .line 217
    :cond_3
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 218
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 141
    const v0, 0x7f03005d

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 145
    const v0, 0x7f070265

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->o:Landroid/widget/CheckedTextView;

    .line 147
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->o:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->o:Landroid/widget/CheckedTextView;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    const v0, 0x7f070269

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 155
    const v0, 0x7f07026c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 157
    const v0, 0x7f07026a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    .line 159
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 160
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->m:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->B:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 161
    invoke-direct {p0, v4}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Z)V

    .line 163
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Landroid/view/View;)V

    .line 165
    const v0, 0x7f070266

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->r:Landroid/widget/LinearLayout;

    .line 166
    const v0, 0x7f070267

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->s:Landroid/view/View;

    .line 167
    const v0, 0x7f070268

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->t:Landroid/view/View;

    .line 168
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->s:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->t:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a:I

    const/16 v2, 0x7d2

    if-ne v0, v2, :cond_0

    .line 173
    invoke-direct {p0, v4}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Z)V

    .line 175
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->u:I

    invoke-direct {p0, v0, v3}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(IZ)V

    .line 177
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 184
    const-string v0, "[onDestroy]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->w:Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/f;->a()V

    .line 188
    :cond_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->f()V

    .line 189
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onDestroy()V

    .line 190
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 257
    .line 258
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 259
    const v2, 0x7f0705a5

    if-ne v1, v2, :cond_2

    .line 260
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c()V

    move v0, v3

    .line 321
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 322
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 325
    :cond_1
    return v0

    .line 262
    :cond_2
    const v2, 0x7f070589

    if-ne v1, v2, :cond_3

    .line 263
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Z)V

    .line 265
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Z)V

    .line 266
    const/16 v0, 0x7d2

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(I)V

    .line 267
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->l()V

    .line 268
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    move v0, v3

    .line 269
    goto :goto_0

    .line 270
    :cond_3
    const v2, 0x7f0705a6

    if-ne v1, v2, :cond_7

    .line 271
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Z)V

    .line 279
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b03f6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 280
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->k()[Z

    move-result-object v4

    .line 281
    if-eqz v4, :cond_5

    move v1, v0

    .line 282
    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_6

    .line 283
    aget-boolean v5, v4, v0

    if-ne v5, v3, :cond_4

    .line 284
    add-int/lit8 v1, v1, 0x1

    .line 282
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v1, v0

    .line 289
    :cond_6
    if-le v1, v3, :cond_8

    .line 290
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b023d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 294
    :goto_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b00fe

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fc

    new-instance v2, Lcom/sec/vip/amschaton/fragment/bc;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/bc;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fd

    new-instance v2, Lcom/sec/vip/amschaton/fragment/aw;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/aw;-><init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 314
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    move v0, v3

    .line 316
    goto/16 :goto_0

    :cond_7
    const v2, 0x7f07058a

    if-ne v1, v2, :cond_0

    .line 317
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/chaton/settings/downloads/ActivityAmsItemDownloads;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 318
    const-string v2, "amsType"

    sget-object v3, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 319
    const/16 v2, 0xc9

    invoke-virtual {p0, v1, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_8
    move-object v0, v2

    goto :goto_2
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    .prologue
    const v2, 0x7f0705a6

    .line 232
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a:I

    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_0

    .line 233
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 234
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 240
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 241
    return-void

    .line 237
    :cond_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 133
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onStart()V

    .line 135
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0051

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 136
    return-void
.end method
