.class public Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;
.super Lcom/sec/vip/amschaton/fragment/AMSFragment;
.source "AMSComposerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/vip/amschaton/ab;
.implements Lcom/sec/vip/amschaton/n;
.implements Lcom/sec/vip/amschaton/p;


# static fields
.field public static a:Z

.field public static b:Z


# instance fields
.field private A:Lcom/sec/vip/amschaton/aw;

.field private B:Lcom/sec/vip/amschaton/AMSDrawManager;

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:[I

.field private H:[I

.field private I:I

.field private J:I

.field private K:Z

.field private L:Z

.field private M:Lcom/sec/vip/amschaton/bi;

.field private N:Landroid/widget/RelativeLayout;

.field private O:Lcom/sec/vip/amschaton/bl;

.field private P:Landroid/widget/RelativeLayout;

.field private Q:I

.field private R:Z

.field private S:I

.field private T:I

.field private U:Ljava/lang/String;

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aa:Landroid/widget/RelativeLayout;

.field private ab:I

.field private ac:I

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Z

.field private ah:Z

.field private ai:Z

.field private aj:Landroid/util/DisplayMetrics;

.field private ak:Landroid/app/ProgressDialog;

.field private al:Lcom/sec/vip/amschaton/fragment/aq;

.field private am:Lcom/sec/vip/amschaton/bp;

.field private an:Landroid/graphics/Bitmap;

.field private ao:Landroid/graphics/Bitmap;

.field private ap:Landroid/os/Handler;

.field c:Ljava/lang/String;

.field private m:Landroid/graphics/Bitmap;

.field private n:I

.field private o:F

.field private p:Landroid/widget/ImageButton;

.field private q:Landroid/widget/ImageButton;

.field private r:Landroid/widget/ImageButton;

.field private s:Landroid/widget/ImageButton;

.field private t:Landroid/widget/ImageView;

.field private u:Landroid/widget/ImageView;

.field private v:Landroid/widget/ImageView;

.field private w:Landroid/widget/ImageView;

.field private x:Landroid/graphics/Bitmap;

.field private y:Landroid/graphics/Bitmap;

.field private z:Landroid/view/MenuItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 150
    sput-boolean v0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a:Z

    .line 157
    sput-boolean v0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;-><init>()V

    .line 95
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->m:Landroid/graphics/Bitmap;

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n:I

    .line 98
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->o:F

    .line 118
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    .line 119
    const/16 v0, 0x7d2

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    .line 128
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    .line 129
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    .line 135
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    .line 139
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->R:Z

    .line 142
    iput v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->S:I

    .line 143
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    .line 146
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->V:Z

    .line 152
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Z:Z

    .line 162
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ag:Z

    .line 164
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ah:Z

    .line 166
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    .line 170
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ak:Landroid/app/ProgressDialog;

    .line 171
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c:Ljava/lang/String;

    .line 172
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->al:Lcom/sec/vip/amschaton/fragment/aq;

    .line 174
    new-instance v0, Lcom/sec/vip/amschaton/fragment/aa;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/aa;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->am:Lcom/sec/vip/amschaton/bp;

    .line 2549
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ah;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/ah;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ap:Landroid/os/Handler;

    .line 2564
    return-void
.end method

.method private A()Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/high16 v1, 0x42a00000    # 80.0f

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    .line 1999
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->o:F

    mul-float/2addr v0, v1

    float-to-int v7, v0

    .line 2000
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->o:F

    mul-float/2addr v0, v1

    float-to-int v8, v0

    .line 2001
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 2002
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2003
    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 2005
    new-instance v0, Lcom/sec/vip/amschaton/aa;

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    iget v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ab:I

    iget v6, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ac:I

    invoke-direct/range {v0 .. v6}, Lcom/sec/vip/amschaton/aa;-><init>(IIIZII)V

    .line 2006
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->o:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/aa;->a(F)V

    .line 2007
    const-string v1, "Aa"

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/aa;->b(Ljava/lang/String;)Z

    .line 2008
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/aa;->l()I

    move-result v1

    .line 2009
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/aa;->m()I

    move-result v2

    .line 2011
    invoke-virtual {v0, v9}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;)V

    .line 2012
    sub-int v1, v7, v1

    int-to-float v1, v1

    mul-float/2addr v1, v10

    sub-int v2, v8, v2

    int-to-float v2, v2

    mul-float/2addr v2, v10

    const-string v3, "Aa"

    const/16 v4, 0xff

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/u;->a(FFLjava/lang/String;I)Z

    .line 2013
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/aa;->e()V

    .line 2015
    return-object v9
.end method

.method private B()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2213
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2214
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "AMSPref"

    invoke-virtual {v0, v1, v4}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2215
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    .line 2216
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AMS_BG_SET_FLAG"

    iget-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2217
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->x()V

    .line 2249
    :goto_0
    return-void

    .line 2221
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 2222
    const v2, 0x7f030012

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v3, 0x7f070063

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2223
    const v0, 0x7f070064

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2224
    const v2, 0x7f0b00f0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2226
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v2, 0x7f0b011f

    invoke-virtual {v0, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0101

    new-instance v2, Lcom/sec/vip/amschaton/fragment/ag;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/ag;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0128

    new-instance v2, Lcom/sec/vip/amschaton/fragment/af;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/af;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private C()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x3

    .line 2302
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "AMSPref"

    invoke-virtual {v0, v1, v4}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2304
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_TOOL_TYPE"

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2305
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_PEN_STYLE"

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2307
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_PEN_LINE_COLOR"

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    aget v3, v3, v5

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2308
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_PEN_BLUR_COLOR"

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    aget v3, v3, v6

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2309
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_CPEN_MARKER_COLOR"

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    aget v3, v3, v7

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2310
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_PEN_DOT_COLOR"

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    aget v3, v3, v4

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2312
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_PEN_LINE"

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    aget v3, v3, v5

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2313
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_PEN_BLUR_SIZE"

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    aget v3, v3, v6

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2314
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_PEN_MARKER_SIZE"

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    aget v3, v3, v7

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2315
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_PEN_DOT_SIZE"

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    aget v3, v3, v4

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2316
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_PEN_ERASER_SIZE"

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    const/4 v4, 0x4

    aget v3, v3, v4

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2318
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_TEXT_SIZE"

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2319
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_TEXT_COLOR"

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2321
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "AMS_STAMP_STYLE"

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2322
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AMS_BG_SET_FLAG"

    iget-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2323
    return-void
.end method

.method private D()V
    .locals 1

    .prologue
    .line 2493
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_0

    .line 2494
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->v()V

    .line 2496
    :cond_0
    return-void
.end method

.method private E()V
    .locals 4

    .prologue
    .line 2533
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ah:Z

    if-eqz v0, :cond_0

    .line 2538
    :goto_0
    return-void

    .line 2537
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ar;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/ar;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Lcom/sec/vip/amschaton/fragment/aa;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Boolean;

    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/ar;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private F()V
    .locals 4

    .prologue
    .line 2542
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ah:Z

    if-eqz v0, :cond_0

    .line 2547
    :goto_0
    return-void

    .line 2546
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ar;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/ar;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Lcom/sec/vip/amschaton/fragment/aa;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Boolean;

    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/ar;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2020
    .line 2022
    const-string v0, "exceed_size"

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2024
    const-string v0, "temp_file_path"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2026
    if-nez v2, :cond_1

    move-object v0, v1

    .line 2050
    :cond_0
    :goto_0
    return-object v0

    .line 2031
    :cond_1
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2033
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2035
    goto :goto_0

    .line 2038
    :cond_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2040
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2042
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 2047
    :cond_3
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private a(IIII)Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1869
    if-eq p1, v7, :cond_0

    .line 1944
    :goto_0
    return-object v0

    .line 1879
    :cond_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->an:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 1881
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->an:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1882
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->an:Landroid/graphics/Bitmap;

    .line 1885
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ao:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 1887
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ao:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1888
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ao:Landroid/graphics/Bitmap;

    .line 1894
    :cond_2
    packed-switch p2, :pswitch_data_0

    .line 1898
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02008d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1899
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020095

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1915
    :goto_1
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 1916
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 1918
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v6, v6, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1920
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->an:Landroid/graphics/Bitmap;

    .line 1921
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ao:Landroid/graphics/Bitmap;

    .line 1923
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->an:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1924
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ao:Landroid/graphics/Bitmap;

    invoke-direct {v3, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1926
    invoke-virtual {v2, p3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1927
    invoke-virtual {v3, p3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1929
    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1930
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1931
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1932
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1934
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->an:Landroid/graphics/Bitmap;

    invoke-direct {v1, v0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1935
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->an:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(I)V

    .line 1936
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ao:Landroid/graphics/Bitmap;

    invoke-direct {v2, v0, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1937
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ao:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(I)V

    .line 1939
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 1940
    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v0, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1941
    new-array v1, v7, [I

    const v3, 0x10100a7

    aput v3, v1, v6

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1942
    new-array v1, v7, [I

    const v3, 0x10100a1

    aput v3, v1, v6

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1902
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02008e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1903
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020096

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_1

    .line 1906
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02008f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1907
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020097

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_1

    .line 1910
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020090

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1911
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020098

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_1

    .line 1894
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 1940
    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
    .end array-data
.end method

.method private a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2055
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd_HHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2056
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2057
    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    const v3, 0x7f07023f

    const v2, 0x7f07022f

    .line 506
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 508
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 510
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    .line 511
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 512
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 531
    :goto_0
    return-void

    .line 518
    :cond_0
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    .line 520
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 521
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 523
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    .line 524
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 525
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1332
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    .line 1333
    iput p2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    .line 1335
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->e()Lcom/sec/vip/amschaton/u;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1337
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 1344
    :goto_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    .line 1345
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(I)V

    .line 1347
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b(II)V

    .line 1348
    return-void

    .line 1341
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    invoke-virtual {v0, v4, v1, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(III)Z

    goto :goto_0
.end method

.method private a(III)V
    .locals 6

    .prologue
    .line 1320
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    .line 1321
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aput p2, v0, v1

    .line 1322
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aput p3, v0, v1

    .line 1324
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    iget v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    iget v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aget v4, v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 1326
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    .line 1327
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(I)V

    .line 1328
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1555
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    .line 1556
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->m:Landroid/graphics/Bitmap;

    .line 1557
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, p1, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Landroid/graphics/Bitmap;Z)Z

    .line 1558
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 1559
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    .line 1560
    return-void
.end method

.method private a(Landroid/widget/EditText;Z)V
    .locals 3

    .prologue
    .line 2062
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2064
    if-eqz p2, :cond_0

    .line 2066
    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2072
    :goto_0
    return-void

    .line 2070
    :cond_0
    invoke-virtual {p1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;I)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;II)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;III)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(III)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private a(Lcom/sec/vip/amschaton/i;)V
    .locals 1

    .prologue
    .line 1358
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n()Lcom/sec/vip/amschaton/an;

    move-result-object v0

    .line 1359
    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/an;->a(Lcom/sec/vip/amschaton/i;)V

    .line 1360
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/an;->d()V

    .line 1361
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    .line 1362
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1812
    const/16 v0, 0x7d2

    .line 1814
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1816
    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1818
    const/16 v0, 0x7d0

    .line 1825
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 1826
    return-void

    .line 1820
    :cond_1
    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1822
    const/16 v0, 0x7d1

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->K:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->K:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;I)I
    .locals 0

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    return p1
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Lcom/sec/vip/amschaton/bi;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->M:Lcom/sec/vip/amschaton/bi;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 935
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a:Z

    .line 937
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->t()Z

    move-result v0

    if-nez v0, :cond_1

    .line 938
    :cond_0
    invoke-static {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    .line 962
    :goto_0
    return-void

    .line 940
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00ea

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00f0

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0101

    new-instance v2, Lcom/sec/vip/amschaton/fragment/ak;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/ak;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0128

    new-instance v2, Lcom/sec/vip/amschaton/fragment/aj;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/aj;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 189
    const/16 v0, 0xbb8

    if-ne p1, v0, :cond_3

    .line 192
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->U:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 213
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->s()Z

    move-result v0

    if-nez v0, :cond_2

    .line 214
    const v0, 0x7f0b00fa

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(I)V

    goto :goto_0

    .line 199
    :pswitch_0
    const v0, 0x7f0b00f7

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(I)V

    .line 200
    const-string v0, "Load Error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :pswitch_1
    const v0, 0x7f0b00f9

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(I)V

    goto :goto_0

    .line 206
    :pswitch_2
    const v0, 0x7f0b00f8

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(I)V

    goto :goto_0

    .line 209
    :pswitch_3
    const v0, 0x7f0b0149

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(I)V

    goto :goto_0

    .line 218
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    .line 219
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->setPlayMode(Z)V

    .line 220
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    .line 221
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->h()V

    .line 223
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->V:Z

    goto :goto_0

    .line 225
    :cond_3
    const/16 v0, 0xbb9

    if-ne p1, v0, :cond_0

    .line 227
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(I)V

    goto :goto_0

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private b(II)V
    .locals 1

    .prologue
    .line 2500
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v0, :cond_1

    .line 2509
    :cond_0
    :goto_0
    return-void

    .line 2504
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2508
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, p1, p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(II)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(Z)V

    return-void
.end method

.method private b(Lcom/sec/vip/amschaton/i;)V
    .locals 1

    .prologue
    .line 1428
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->p()Lcom/sec/vip/amschaton/az;

    move-result-object v0

    .line 1429
    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/az;->a(Lcom/sec/vip/amschaton/i;)V

    .line 1430
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/az;->c()V

    .line 1431
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    .line 1432
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const v6, 0x7f07023f

    const v5, 0x7f07022f

    const/16 v4, 0x8

    const/4 v3, 0x0

    const v2, 0x7f070071

    .line 2114
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->N:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 2116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->N:Landroid/widget/RelativeLayout;

    .line 2119
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 2120
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 2122
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2123
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2130
    :goto_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->N:Landroid/widget/RelativeLayout;

    .line 2136
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->N:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/vip/amschaton/fragment/ad;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/ad;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2142
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    if-eqz v0, :cond_1

    .line 2144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    .line 2147
    :cond_1
    new-instance v0, Lcom/sec/vip/amschaton/bl;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/16 v3, 0x80

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/vip/amschaton/bl;-><init>(Landroid/widget/RelativeLayout;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    .line 2149
    if-eqz p1, :cond_2

    .line 2151
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/bl;->a(Ljava/lang/String;)V

    .line 2154
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->am:Lcom/sec/vip/amschaton/bp;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/bl;->a(Lcom/sec/vip/amschaton/bp;)V

    .line 2156
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ae;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/ae;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->M:Lcom/sec/vip/amschaton/bi;

    .line 2186
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->M:Lcom/sec/vip/amschaton/bi;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/bl;->a(Lcom/sec/vip/amschaton/bi;)V

    .line 2187
    return-void

    .line 2126
    :cond_3
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2127
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1288
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    .line 1290
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    if-nez v1, :cond_0

    .line 1292
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(I)V

    .line 1316
    :goto_0
    return v0

    .line 1296
    :cond_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/aw;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1298
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(I)V

    goto :goto_0

    .line 1302
    :cond_1
    if-eqz p1, :cond_2

    .line 1304
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/sec/vip/amschaton/fragment/al;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/al;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1316
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1312
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/aw;->dismiss()V

    .line 1313
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(I)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;I)I
    .locals 0

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->F:I

    return p1
.end method

.method private c(Z)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1733
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/vip/amschaton/AMSActivity;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "A"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-string v5, ".jpg"

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1735
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1737
    const v1, 0x7f0b00ec

    invoke-virtual {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(I)V

    .line 1738
    iput-boolean v7, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Y:Z

    .line 1761
    :goto_0
    return-object v0

    .line 1743
    :cond_0
    iput-boolean v8, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Y:Z

    .line 1744
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1746
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/32 v5, 0x100000

    cmp-long v3, v3, v5

    if-ltz v3, :cond_1

    if-nez p1, :cond_2

    :cond_1
    move-object v0, v1

    .line 1748
    goto :goto_0

    .line 1751
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1752
    const-string v1, "%s\n(%d/%d)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00fb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v3, v8

    const/4 v2, 0x2

    const/high16 v4, 0x100000

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1754
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    const v3, 0x7f0b0100

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b00fc

    new-instance v3, Lcom/sec/vip/amschaton/fragment/ac;

    invoke-direct {v3, p0}, Lcom/sec/vip/amschaton/fragment/ac;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private c(I)V
    .locals 0

    .prologue
    .line 1062
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->d(I)V

    .line 1063
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(I)V

    .line 1064
    return-void
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->c()V

    return-void
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->k(Z)V

    return-void
.end method

.method private c(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2597
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v1, :cond_1

    .line 2606
    :cond_0
    :goto_0
    return v0

    .line 2600
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aj:Landroid/util/DisplayMetrics;

    invoke-virtual {v1, p1, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Ljava/lang/String;Landroid/util/DisplayMetrics;)Z

    move-result v1

    .line 2602
    if-eqz v1, :cond_0

    .line 2606
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0x3e9

    invoke-virtual {v0, v1, p1, v2}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;I)I
    .locals 0

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    return p1
.end method

.method private d(I)V
    .locals 6

    .prologue
    const/4 v0, 0x4

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1068
    new-array v2, v0, [I

    fill-array-data v2, :array_0

    .line 1069
    new-array v3, v0, [Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->q:Landroid/widget/ImageButton;

    aput-object v0, v3, v1

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->r:Landroid/widget/ImageButton;

    aput-object v0, v3, v5

    const/4 v0, 0x2

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s:Landroid/widget/ImageButton;

    aput-object v4, v3, v0

    const/4 v0, 0x3

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->p:Landroid/widget/ImageButton;

    aput-object v4, v3, v0

    move v0, v1

    .line 1071
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 1073
    aget v4, v2, v0

    if-ne v4, p1, :cond_0

    .line 1075
    aget-object v4, v3, v0

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1071
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1079
    :cond_0
    aget-object v4, v3, v0

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_1

    .line 1082
    :cond_1
    return-void

    .line 1068
    nop

    :array_0
    .array-data 4
        0x7d2
        0x7d3
        0x7d5
        0x7d1
    .end array-data
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->X:Z

    return v0
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Y:Z

    return p1
.end method

.method private d(Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1830
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v1, :cond_0

    .line 1832
    const-string v1, "[startAMSPreview] mAMSComposer is NULL!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1863
    :goto_0
    return v0

    .line 1837
    :cond_0
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->t()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1839
    const-string v1, "[startAMSPreview] Empty AMS file!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1844
    :cond_1
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/sec/vip/amschaton/AMSActivity;->g:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1846
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1848
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1852
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    sget-object v2, Lcom/sec/vip/amschaton/AMSActivity;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aj:Landroid/util/DisplayMetrics;

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Ljava/lang/String;Landroid/util/DisplayMetrics;Z)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1854
    const-string v1, "[startAMSPreview] Fail to save AMS file!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1859
    :cond_3
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->L:Z

    if-nez v1, :cond_4

    .line 1860
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v1, v0, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 1863
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;I)I
    .locals 0

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    return p1
.end method

.method private e()V
    .locals 4

    .prologue
    .line 966
    const-string v0, "A"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v3, ""

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 967
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/vip/amschaton/AMSActivity;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 969
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 971
    const v0, 0x7f0b00ec

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(I)V

    .line 976
    :goto_0
    return-void

    .line 975
    :cond_0
    const v0, 0x7f0b00f4

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(I)V

    goto :goto_0
.end method

.method private e(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1221
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    .line 1222
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    .line 1224
    packed-switch p1, :pswitch_data_0

    .line 1227
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    iget v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    iget v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aget v4, v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 1237
    :goto_0
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(I)V

    .line 1238
    const/16 v0, 0xbb9

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b(I)V

    .line 1239
    return-void

    .line 1230
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    goto :goto_0

    .line 1233
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x3

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-virtual {v0, v1, v2, v4, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    goto :goto_0

    .line 1224
    nop

    :pswitch_data_0
    .packed-switch 0x7d3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->c()V

    return-void
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 2077
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->N:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    if-nez v0, :cond_1

    .line 2102
    :cond_0
    :goto_0
    return-void

    .line 2082
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bl;->b()Landroid/widget/EditText;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/widget/EditText;Z)V

    .line 2083
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->L:Z

    .line 2085
    if-eqz p1, :cond_2

    .line 2087
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->N:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2088
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->K:Z

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(Z)V

    .line 2089
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0120

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 2101
    :goto_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 2092
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->N:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2093
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->X:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 2094
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0102

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    goto :goto_1

    .line 2097
    :cond_3
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b03f7

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    goto :goto_1
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)Z
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->d(Z)Z

    move-result v0

    return v0
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 993
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->k(Z)V

    .line 995
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_0

    .line 997
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, v1, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 1000
    :cond_0
    const/16 v0, 0x7d2

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    .line 1001
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(I)V

    .line 1003
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    if-eq v0, v1, :cond_2

    .line 1005
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_1

    .line 1007
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    iget v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    iget v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aget v4, v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 1010
    :cond_1
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    .line 1011
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(I)V

    .line 1012
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    .line 1020
    :goto_0
    return-void

    .line 1017
    :cond_2
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    .line 1019
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->m()V

    goto :goto_0
.end method

.method private f(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2476
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->h(Z)V

    .line 2478
    packed-switch p1, :pswitch_data_0

    .line 2489
    :goto_0
    :pswitch_0
    return-void

    .line 2481
    :pswitch_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->i(Z)V

    goto :goto_0

    .line 2484
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->j(Z)V

    goto :goto_0

    .line 2478
    nop

    :pswitch_data_0
    .packed-switch 0x7d3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e()V

    return-void
.end method

.method private f(Z)V
    .locals 1

    .prologue
    .line 2106
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->z:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 2108
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->z:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2110
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ad:Z

    return p1
.end method

.method static synthetic g(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 5

    .prologue
    const/16 v1, 0x7d3

    const/4 v4, 0x0

    .line 1024
    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    .line 1025
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(I)V

    .line 1027
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    if-eq v0, v1, :cond_0

    .line 1029
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, v4, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 1030
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 1031
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    .line 1032
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(I)V

    .line 1033
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    .line 1034
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->k(Z)V

    .line 1041
    :goto_0
    return-void

    .line 1039
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    .line 1040
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->o()V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->c()V

    return-void
.end method

.method private g(Z)V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/high16 v4, -0x1000000

    .line 2253
    if-eqz p1, :cond_0

    .line 2255
    const/16 v0, 0x7d2

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    .line 2256
    iput v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    .line 2257
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->F:I

    .line 2259
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    aput v4, v0, v5

    .line 2260
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    const/4 v1, 0x1

    aput v4, v0, v1

    .line 2261
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    aput v4, v0, v6

    .line 2262
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    aput v4, v0, v7

    .line 2264
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    aput v8, v0, v5

    .line 2265
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    const/4 v1, 0x1

    aput v8, v0, v1

    .line 2266
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    aput v8, v0, v6

    .line 2267
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    aput v8, v0, v7

    .line 2268
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    const/4 v1, 0x4

    aput v8, v0, v1

    .line 2270
    iput v7, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    .line 2271
    iput v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    .line 2272
    iput v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    .line 2298
    :goto_0
    return-void

    .line 2276
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "AMSPref"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2277
    const-string v1, "AMS_TOOL_TYPE"

    const/16 v2, 0x7d0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    .line 2278
    const-string v1, "AMS_PEN_STYLE"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    .line 2279
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->F:I

    .line 2281
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    const-string v2, "AMS_PEN_LINE_COLOR"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v5

    .line 2282
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    const/4 v2, 0x1

    const-string v3, "AMS_PEN_BLUR_COLOR"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    aput v3, v1, v2

    .line 2283
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    const-string v2, "AMS_CPEN_MARKER_COLOR"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v7

    .line 2284
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    const-string v2, "AMS_PEN_DOT_COLOR"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v6

    .line 2286
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    const-string v2, "AMS_PEN_LINE"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v5

    .line 2287
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    const/4 v2, 0x1

    const-string v3, "AMS_PEN_BLUR_SIZE"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    aput v3, v1, v2

    .line 2288
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    const-string v2, "AMS_PEN_MARKER_SIZE"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v7

    .line 2289
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    const-string v2, "AMS_PEN_DOT_SIZE"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v6

    .line 2290
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    const/4 v2, 0x4

    const-string v3, "AMS_PEN_ERASER_SIZE"

    invoke-interface {v0, v3, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    aput v3, v1, v2

    .line 2292
    const-string v1, "AMS_TEXT_SIZE"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    .line 2293
    const-string v1, "AMS_TEXT_COLOR"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    .line 2295
    const-string v1, "AMS_STAMP_STYLE"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    .line 2296
    const-string v1, "AMS_BG_SET_FLAG"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    goto/16 :goto_0
.end method

.method static synthetic h(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Lcom/sec/vip/amschaton/aw;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_0

    .line 1046
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->t:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->q()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1047
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->u:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->r()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1048
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->v:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->t()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1049
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->t:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1051
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->w:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1056
    :goto_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1058
    :cond_0
    return-void

    .line 1054
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->w:Landroid/widget/ImageView;

    iget-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->t()Z

    move-result v2

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e(Z)V

    return-void
.end method

.method private h(Z)V
    .locals 6

    .prologue
    .line 2327
    const v0, 0x7f02000d

    .line 2329
    if-eqz p1, :cond_0

    .line 2331
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2374
    :goto_0
    return-void

    .line 2336
    :cond_0
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ag:Z

    if-eqz v1, :cond_2

    .line 2338
    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    iget v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    iget v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aget v4, v4, v5

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(IIII)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2340
    if-nez v1, :cond_1

    .line 2342
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2349
    :goto_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    goto :goto_0

    .line 2346
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 2355
    :cond_2
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    packed-switch v0, :pswitch_data_0

    .line 2359
    const v0, 0x7f02008d

    .line 2372
    :goto_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 2362
    :pswitch_0
    const v0, 0x7f02008e

    .line 2363
    goto :goto_2

    .line 2365
    :pswitch_1
    const v0, 0x7f02008f

    .line 2366
    goto :goto_2

    .line 2368
    :pswitch_2
    const v0, 0x7f020090

    goto :goto_2

    .line 2355
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic i(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    return v0
.end method

.method private i(Z)V
    .locals 2

    .prologue
    .line 2378
    const v0, 0x7f020047

    .line 2380
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->x:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 2382
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->x:Landroid/graphics/Bitmap;

    .line 2385
    :cond_0
    if-eqz p1, :cond_1

    .line 2387
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->r:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2404
    :goto_0
    return-void

    .line 2392
    :cond_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->x:Landroid/graphics/Bitmap;

    .line 2394
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->x:Landroid/graphics/Bitmap;

    if-nez v1, :cond_2

    .line 2396
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->r:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2403
    :goto_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    goto :goto_0

    .line 2400
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->r:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->x:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method private i()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1086
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    .line 1087
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v3

    .line 1088
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090145

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1089
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090148

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 1091
    if-ge v0, v3, :cond_1

    :goto_0
    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ab:I

    .line 1092
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[createComposer] Composer Size = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ab:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ab:I

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ac:I

    .line 1095
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_0

    .line 1097
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->u()V

    .line 1098
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    .line 1101
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ab:I

    iget v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ac:I

    invoke-direct {v0, v3, v4, v5}, Lcom/sec/vip/amschaton/AMSDrawManager;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    .line 1103
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1105
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->d()V

    move v0, v1

    .line 1128
    :goto_1
    return v0

    .line 1091
    :cond_1
    sub-int v0, v3, v4

    mul-int/lit8 v3, v5, 0x2

    sub-int/2addr v0, v3

    goto :goto_0

    .line 1109
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEnableZoom(Z)V

    .line 1110
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->setPlayMode(Z)V

    .line 1111
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->setOnTextInputRequestListener(Lcom/sec/vip/amschaton/ab;)V

    .line 1112
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->setOnObjectListener(Lcom/sec/vip/amschaton/n;)V

    .line 1113
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->setOnTouchTextArea(Lcom/sec/vip/amschaton/p;)V

    .line 1114
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    const/4 v4, 0x4

    aget v3, v3, v4

    invoke-virtual {v0, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEraserSize(I)V

    .line 1116
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->V:Z

    if-nez v0, :cond_3

    .line 1118
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s()V

    .line 1121
    :cond_3
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v3, "AMSPref"

    invoke-virtual {v0, v3, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1122
    const-string v3, "AMS_BG_SET_FLAG"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1123
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    .line 1124
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AMS_BG_SET_FLAG"

    iget-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1126
    :cond_4
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->j()V

    move v0, v2

    .line 1128
    goto :goto_1
.end method

.method static synthetic i(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    return p1
.end method

.method static synthetic j(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    return v0
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1133
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aa:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1135
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aa:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aa:Landroid/widget/RelativeLayout;

    .line 1139
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aa:Landroid/widget/RelativeLayout;

    .line 1141
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1142
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1143
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aa:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1144
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aa:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1145
    return-void
.end method

.method private j(Z)V
    .locals 8

    .prologue
    const v7, 0x9c40

    const/16 v6, 0xf0

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 2408
    const v2, 0x7f020045

    .line 2410
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 2411
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 2414
    :goto_0
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    .line 2416
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 2417
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    .line 2420
    :cond_0
    if-eqz p1, :cond_1

    .line 2422
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 2423
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2472
    :goto_1
    return-void

    .line 2428
    :cond_1
    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 2430
    if-le v0, v6, :cond_2

    .line 2431
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-virtual {v0, v3, v5}, Lcom/sec/vip/amschaton/al;->d(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    .line 2461
    :goto_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    if-nez v0, :cond_9

    .line 2463
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 2464
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2471
    :goto_3
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    goto :goto_1

    .line 2433
    :cond_2
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-virtual {v0, v3, v1}, Lcom/sec/vip/amschaton/al;->d(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 2436
    :cond_3
    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    const/16 v4, 0x7530

    if-ge v3, v4, :cond_5

    .line 2438
    if-le v0, v6, :cond_4

    .line 2439
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    add-int/lit16 v3, v3, -0x4e20

    invoke-virtual {v0, v3, v5}, Lcom/sec/vip/amschaton/al;->e(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 2441
    :cond_4
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    add-int/lit16 v3, v3, -0x4e20

    invoke-virtual {v0, v3, v1}, Lcom/sec/vip/amschaton/al;->e(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 2444
    :cond_5
    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    if-ge v3, v7, :cond_7

    .line 2446
    if-le v0, v6, :cond_6

    .line 2447
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    add-int/lit16 v3, v3, -0x7530

    invoke-virtual {v0, v3, v5}, Lcom/sec/vip/amschaton/al;->b(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 2449
    :cond_6
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    add-int/lit16 v3, v3, -0x7530

    invoke-virtual {v0, v3, v1}, Lcom/sec/vip/amschaton/al;->b(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 2454
    :cond_7
    if-le v0, v6, :cond_8

    .line 2455
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    sub-int/2addr v3, v7

    invoke-virtual {v0, v3, v5}, Lcom/sec/vip/amschaton/al;->a(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 2457
    :cond_8
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    sub-int/2addr v3, v7

    invoke-virtual {v0, v3, v1}, Lcom/sec/vip/amschaton/al;->a(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 2468
    :cond_9
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_3

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method static synthetic j(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Z)Z
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ah:Z

    return p1
.end method

.method static synthetic k(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->F:I

    return v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1153
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    const v1, 0x7f07023c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1155
    if-eqz v0, :cond_0

    .line 1157
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1158
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aa:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1161
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    const v1, 0x7f070231

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->p:Landroid/widget/ImageButton;

    .line 1162
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    const v1, 0x7f070232

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->q:Landroid/widget/ImageButton;

    .line 1163
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    const v1, 0x7f070234

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->r:Landroid/widget/ImageButton;

    .line 1164
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    const v1, 0x7f070233

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s:Landroid/widget/ImageButton;

    .line 1166
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    const v1, 0x7f070238

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->t:Landroid/widget/ImageView;

    .line 1167
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    const v1, 0x7f070239

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->u:Landroid/widget/ImageView;

    .line 1168
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    const v1, 0x7f070237

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->v:Landroid/widget/ImageView;

    .line 1169
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    const v1, 0x7f07023a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->w:Landroid/widget/ImageView;

    .line 1171
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->p:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1172
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1173
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1174
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1176
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1177
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1178
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1179
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1209
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->V:Z

    if-nez v0, :cond_1

    .line 1210
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->setPlayMode(Z)V

    .line 1211
    const/16 v0, 0xbb8

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b(I)V

    .line 1214
    :cond_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->h()V

    .line 1215
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c(I)V

    .line 1216
    const/16 v0, 0xbb9

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b(I)V

    .line 1217
    return-void
.end method

.method private k(Z)V
    .locals 1

    .prologue
    .line 2513
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v0, :cond_1

    .line 2524
    :cond_0
    :goto_0
    return-void

    .line 2517
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->A()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2518
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->y()V

    .line 2521
    :cond_2
    if-eqz p1, :cond_0

    .line 2522
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->x()V

    goto :goto_0
.end method

.method private l()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1243
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    if-nez v0, :cond_1

    .line 1284
    :cond_0
    :goto_0
    return-void

    .line 1248
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/aw;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1254
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/aw;->g()Lcom/sec/vip/amschaton/i;

    move-result-object v2

    .line 1256
    if-eqz v2, :cond_4

    .line 1258
    invoke-virtual {v2}, Lcom/sec/vip/amschaton/i;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1260
    const/4 v0, 0x1

    .line 1264
    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b(Z)Z

    .line 1266
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1275
    :pswitch_0
    if-eqz v0, :cond_3

    .line 1276
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Lcom/sec/vip/amschaton/i;)V

    goto :goto_0

    .line 1268
    :pswitch_1
    if-eqz v0, :cond_2

    .line 1269
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b(Lcom/sec/vip/amschaton/i;)V

    goto :goto_0

    .line 1271
    :cond_2
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->o()V

    goto :goto_0

    .line 1278
    :cond_3
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->m()V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 1266
    :pswitch_data_0
    .packed-switch 0x7d2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic l(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)[I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    return-object v0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 1352
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n()Lcom/sec/vip/amschaton/an;

    move-result-object v0

    .line 1353
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    .line 1354
    return-void
.end method

.method static synthetic m(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)[I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    return-object v0
.end method

.method private n()Lcom/sec/vip/amschaton/an;
    .locals 5

    .prologue
    .line 1365
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Z:Z

    .line 1366
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1367
    const/4 v0, 0x1

    .line 1369
    :cond_0
    new-instance v1, Lcom/sec/vip/amschaton/an;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f03005f

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->q:Landroid/widget/ImageButton;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/sec/vip/amschaton/an;-><init>(Landroid/content/Context;ILandroid/view/View;Z)V

    .line 1370
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/an;->a(I)V

    .line 1371
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aget v0, v0, v2

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/an;->b(I)V

    .line 1372
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E:I

    aget v0, v0, v2

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/an;->c(I)V

    .line 1374
    new-instance v0, Lcom/sec/vip/amschaton/fragment/am;

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/am;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Lcom/sec/vip/amschaton/an;)V

    .line 1406
    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/an;->a(Lcom/sec/vip/amschaton/bi;)V

    .line 1408
    new-instance v0, Lcom/sec/vip/amschaton/fragment/an;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/an;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/an;->a(Lcom/sec/vip/amschaton/ay;)V

    .line 1417
    return-object v1
.end method

.method static synthetic n(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C()V

    return-void
.end method

.method static synthetic o(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    return v0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 1422
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->p()Lcom/sec/vip/amschaton/az;

    move-result-object v0

    .line 1423
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    .line 1424
    return-void
.end method

.method static synthetic p(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    return v0
.end method

.method private p()Lcom/sec/vip/amschaton/az;
    .locals 5

    .prologue
    .line 1435
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Z:Z

    .line 1436
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1437
    const/4 v0, 0x1

    .line 1439
    :cond_0
    new-instance v1, Lcom/sec/vip/amschaton/az;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f030060

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->r:Landroid/widget/ImageButton;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/sec/vip/amschaton/az;-><init>(Landroid/content/Context;ILandroid/view/View;Z)V

    .line 1440
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->J:I

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/az;->a(I)V

    .line 1441
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->I:I

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/az;->b(I)V

    .line 1442
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ao;

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/ao;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Lcom/sec/vip/amschaton/az;)V

    .line 1461
    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/az;->a(Lcom/sec/vip/amschaton/bi;)V

    .line 1463
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ap;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/ap;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/az;->a(Lcom/sec/vip/amschaton/ay;)V

    .line 1472
    return-object v1
.end method

.method private q()V
    .locals 3

    .prologue
    .line 1477
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->k(Z)V

    .line 1479
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_0

    .line 1481
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/amschaton/AMSBgSelectionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1482
    const-string v1, "BACKGROUND_COLOR"

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1483
    const-string v1, "BACKGROUND_INDEX"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->S:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1484
    const-string v1, "AMS_FRAME"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1485
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1487
    :cond_0
    return-void
.end method

.method static synthetic q(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    return v0
.end method

.method static synthetic r(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->U:Ljava/lang/String;

    return-object v0
.end method

.method private r()V
    .locals 4

    .prologue
    const/16 v1, 0x7d5

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 1491
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->k(Z)V

    .line 1493
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_0

    .line 1495
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, v2, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 1498
    :cond_0
    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    .line 1500
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->d(I)V

    .line 1502
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    if-eq v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->R:Z

    if-nez v0, :cond_2

    .line 1504
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_1

    .line 1506
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-virtual {v0, v3, v1, v2, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 1509
    :cond_1
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    .line 1510
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(I)V

    .line 1511
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    .line 1512
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C()V

    .line 1513
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ae:Z

    .line 1536
    :goto_0
    return-void

    .line 1518
    :cond_2
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->R:Z

    if-eqz v0, :cond_4

    .line 1520
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->R:Z

    .line 1522
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_3

    .line 1524
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-virtual {v0, v3, v1, v2, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 1527
    :cond_3
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D:I

    .line 1528
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    .line 1531
    :cond_4
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C()V

    .line 1533
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/amschaton/AMSStampSelectionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1534
    const-string v1, "AMS_STAMP_INDEX"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1535
    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private s()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/16 v3, 0xff

    .line 1540
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v0, :cond_0

    .line 1552
    :goto_0
    return-void

    .line 1544
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "AMSPref"

    invoke-virtual {v0, v1, v4}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1545
    const-string v1, "AMS_BG_SET_FLAG"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1546
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    .line 1547
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AMS_BG_SET_FLAG"

    iget-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1549
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, v3, v3, v3, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    .line 1550
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    .line 1551
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    goto :goto_0
.end method

.method static synthetic s(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->u()V

    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 1564
    new-instance v0, Lcom/sec/vip/amschaton/fragment/aq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/aq;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;Lcom/sec/vip/amschaton/fragment/aa;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->al:Lcom/sec/vip/amschaton/fragment/aq;

    .line 1565
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->al:Lcom/sec/vip/amschaton/fragment/aq;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/aq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1566
    return-void
.end method

.method static synthetic t(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->v()V

    return-void
.end method

.method static synthetic u(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private u()V
    .locals 4

    .prologue
    .line 1619
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1627
    :goto_0
    return-void

    .line 1622
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ak:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 1623
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ak:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 1625
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ak:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method static synthetic v(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Lcom/sec/vip/amschaton/bl;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    return-object v0
.end method

.method private v()V
    .locals 1

    .prologue
    .line 1630
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1636
    :cond_0
    :goto_0
    return-void

    .line 1633
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ak:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 1634
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ak:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method

.method static synthetic w(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Lcom/sec/vip/amschaton/AMSDrawManager;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    return-object v0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 1640
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/al;->d()I

    move-result v0

    if-lez v0, :cond_0

    .line 1642
    const v0, 0x9c40

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    .line 1652
    :goto_0
    return-void

    .line 1644
    :cond_0
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/al;->h()I

    move-result v0

    if-lez v0, :cond_1

    .line 1646
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    goto :goto_0

    .line 1650
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    goto :goto_0
.end method

.method private x()V
    .locals 2

    .prologue
    .line 1656
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v0, :cond_0

    .line 1676
    :goto_0
    return-void

    .line 1661
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    .line 1663
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->i()Z

    .line 1665
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->P:Landroid/widget/RelativeLayout;

    const v1, 0x7f07023c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1667
    if-eqz v0, :cond_1

    .line 1669
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1672
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aa:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1674
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e(I)V

    .line 1675
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->h()V

    goto :goto_0
.end method

.method static synthetic x(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    return v0
.end method

.method private y()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1680
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v0, :cond_0

    .line 1729
    :goto_0
    return-void

    .line 1685
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->t()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1687
    const v0, 0x7f0b00f1

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(I)V

    goto :goto_0

    .line 1691
    :cond_1
    new-instance v0, Lcom/sec/vip/amschaton/ad;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/ad;-><init>(Landroid/content/Context;)V

    .line 1692
    const/4 v1, 0x0

    const v2, 0x7f0b0129

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/vip/amschaton/ad;->a(IIZ)Z

    .line 1693
    const v1, 0x7f0b012a

    invoke-virtual {v0, v3, v1, v3}, Lcom/sec/vip/amschaton/ad;->a(IIZ)Z

    .line 1694
    const/4 v1, 0x2

    const v2, 0x7f0b0130

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/vip/amschaton/ad;->a(IIZ)Z

    .line 1695
    new-instance v1, Lcom/sec/vip/amschaton/fragment/ab;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/ab;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/ad;->a(Lcom/sec/vip/amschaton/ak;)V

    .line 1728
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/ad;->b()V

    goto :goto_0
.end method

.method static synthetic y(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->x()V

    return-void
.end method

.method static synthetic z(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ap:Landroid/os/Handler;

    return-object v0
.end method

.method private z()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/16 v6, 0x1e0

    .line 1766
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/vip/amschaton/AMSActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "I"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-string v5, ".jpg"

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1768
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1770
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->f()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1772
    if-eqz v2, :cond_2

    .line 1774
    const/4 v3, 0x1

    invoke-static {v2, v6, v6, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1779
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1780
    :try_start_1
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x50

    invoke-virtual {v2, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1781
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1782
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 1783
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1790
    if-eqz v3, :cond_0

    .line 1792
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 1807
    :goto_0
    return-object v0

    .line 1785
    :catch_0
    move-exception v2

    move-object v3, v1

    .line 1786
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1790
    if-eqz v3, :cond_0

    .line 1792
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 1794
    :catch_1
    move-exception v0

    .line 1795
    :goto_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1805
    :cond_0
    :goto_3
    const v0, 0x7f0b00ec

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(I)V

    move-object v0, v1

    .line 1807
    goto :goto_0

    .line 1787
    :catch_2
    move-exception v2

    move-object v3, v1

    .line 1788
    :goto_4
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1790
    if-eqz v3, :cond_0

    .line 1792
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 1794
    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    .line 1795
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1790
    :cond_1
    throw v1

    .line 1801
    :cond_2
    const-string v0, "Bitmap main is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1790
    :catchall_0
    move-exception v2

    move-object v3, v1

    move-object v1, v2

    :goto_5
    if-eqz v3, :cond_1

    .line 1792
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 1794
    :catch_5
    move-exception v0

    goto :goto_2

    .line 1790
    :catchall_1
    move-exception v1

    goto :goto_5

    .line 1787
    :catch_6
    move-exception v2

    goto :goto_4

    .line 1785
    :catch_7
    move-exception v2

    goto :goto_1
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 2529
    return-void
.end method

.method public a(IIIII)Z
    .locals 1

    .prologue
    .line 2200
    sparse-switch p1, :sswitch_data_0

    .line 2208
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2203
    :sswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    .line 2204
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->h()V

    goto :goto_0

    .line 2200
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2192
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b(Ljava/lang/String;)V

    .line 2193
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e(Z)V

    .line 2194
    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 690
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->L:Z

    if-eqz v0, :cond_1

    .line 692
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e(Z)V

    .line 694
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    if-eqz v0, :cond_0

    .line 696
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bl;->c()V

    .line 702
    :cond_0
    :goto_0
    return-void

    .line 700
    :cond_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b()V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/16 v7, 0x3e8

    const/4 v5, 0x5

    const/high16 v6, 0x43f00000    # 480.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 291
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 293
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 295
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a()V

    .line 296
    if-eqz p1, :cond_4

    .line 298
    const-string v0, "SI_SAVE_TEMP_AMS"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    const-string v0, "SI_SAVE_FLAG"

    iget-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    .line 301
    const-string v0, "SI_EDIT_FLAG"

    iget-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->X:Z

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->X:Z

    .line 302
    sget-object v0, Lcom/sec/vip/amschaton/AMSActivity;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->U:Ljava/lang/String;

    .line 303
    const/16 v0, 0xbb8

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b(I)V

    .line 306
    :cond_0
    const-string v0, "SI_CURRENT_TOOL_MODE"

    const/16 v3, 0x7d2

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    move v0, v1

    .line 323
    :goto_0
    sput-boolean v1, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b:Z

    .line 324
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Y:Z

    .line 325
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ah:Z

    .line 326
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->U:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->U:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_1
    move v3, v2

    :goto_1
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->V:Z

    .line 327
    new-array v3, v5, [I

    iput-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->G:[I

    .line 328
    new-array v3, v5, [I

    iput-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->H:[I

    .line 329
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->g(Z)V

    .line 330
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C()V

    .line 332
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    .line 334
    iget v5, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v2, :cond_7

    .line 336
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Z:Z

    .line 337
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    iput v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->o:F

    .line 345
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->i()Z

    move-result v3

    if-nez v3, :cond_8

    .line 399
    :cond_3
    :goto_3
    return-void

    .line 309
    :cond_4
    if-eqz v4, :cond_5

    .line 311
    const-string v0, "AMS_FILE_PATH"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->U:Ljava/lang/String;

    .line 312
    const-string v0, "AMS_SAVE_FLAG"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    .line 313
    const-string v0, "AMS_EDIT_FLAG"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->X:Z

    .line 314
    const-string v0, "AMS_PEN_STATE_DEFAULT"

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 318
    :cond_5
    const-string v0, "[onCreate] bundle is NULL!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->U:Ljava/lang/String;

    move v0, v2

    goto :goto_0

    :cond_6
    move v3, v1

    .line 326
    goto :goto_1

    .line 339
    :cond_7
    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_2

    .line 341
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Z:Z

    .line 342
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    iput v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->o:F

    goto :goto_2

    .line 350
    :cond_8
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->k()V

    .line 352
    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e(I)V

    .line 354
    if-eqz v0, :cond_9

    .line 356
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->w()V

    .line 359
    :cond_9
    if-eqz v4, :cond_a

    .line 360
    const-string v0, "BACKGROUND_INDEX"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->S:I

    .line 361
    const-string v0, "AMS_FRAME"

    invoke-virtual {v4, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    .line 362
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    const/16 v3, 0x3e9

    if-ne v0, v3, :cond_a

    .line 363
    invoke-direct {p0, v4}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/os/Bundle;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 364
    if-eqz v0, :cond_3

    .line 367
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/graphics/Bitmap;)V

    .line 368
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 369
    new-instance v0, Ljava/io/File;

    sget-object v3, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->k:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 370
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 371
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 376
    :cond_a
    if-eqz p1, :cond_3

    const-string v0, "restore_background"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 377
    const-string v0, "BgBitmapParcel"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->m:Landroid/graphics/Bitmap;

    .line 378
    const-string v0, "bgColor"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n:I

    .line 379
    const-string v0, "BgType"

    invoke-virtual {p1, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    .line 380
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_b

    .line 382
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n:I

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    .line 383
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n:I

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    .line 384
    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n:I

    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    .line 386
    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v4, v0, v1, v3, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    .line 387
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    .line 389
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    goto/16 :goto_3

    .line 391
    :cond_b
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 393
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->m:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/graphics/Bitmap;)V

    goto/16 :goto_3
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const v2, 0x9c40

    const/16 v6, 0x3e8

    const/4 v3, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 706
    .line 708
    sparse-switch p1, :sswitch_data_0

    .line 877
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->h()V

    .line 878
    return-void

    .line 711
    :sswitch_0
    if-ne p2, v3, :cond_0

    .line 716
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 718
    if-eqz v0, :cond_0

    .line 723
    const-string v1, "BACKGROUND_INDEX"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->S:I

    .line 724
    const-string v1, "AMS_FRAME"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    .line 726
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    .line 727
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    .line 729
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->m:Landroid/graphics/Bitmap;

    .line 730
    iput v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n:I

    .line 733
    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    if-ne v2, v6, :cond_1

    .line 735
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->s()V

    goto :goto_0

    .line 737
    :cond_1
    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    const/16 v3, 0x3ec

    if-ne v2, v3, :cond_2

    .line 739
    const-string v1, "BACKGROUND_COLOR"

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 740
    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n:I

    .line 741
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    .line 742
    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 743
    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 744
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ai:Z

    .line 745
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v3, v1, v2, v0, v5}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    .line 746
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    .line 748
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->D()V

    goto :goto_0

    .line 750
    :cond_2
    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    const/16 v3, 0x3e9

    if-ne v2, v3, :cond_3

    .line 752
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/os/Bundle;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 754
    if-eqz v0, :cond_0

    .line 759
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/graphics/Bitmap;)V

    .line 762
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->h:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 764
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 766
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 769
    :cond_3
    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    const/16 v3, 0x3eb

    if-ne v2, v3, :cond_4

    .line 771
    const-string v1, "BG_SKIN_INDEX"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 772
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v5}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/res/AssetManager;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 774
    if-eqz v0, :cond_0

    .line 776
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 779
    :cond_4
    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    const/16 v3, 0x3ea

    if-ne v2, v3, :cond_5

    .line 781
    const-string v1, "BG_SKIN_INDEX"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 782
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v5}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/Context;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 784
    if-eqz v0, :cond_0

    .line 786
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 790
    :cond_5
    iput v6, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    .line 791
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    goto/16 :goto_0

    .line 797
    :sswitch_1
    if-eq p2, v3, :cond_a

    .line 799
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->e()Lcom/sec/vip/amschaton/u;

    move-result-object v0

    if-nez v0, :cond_0

    .line 801
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v1

    if-le v0, v1, :cond_6

    .line 803
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    add-int/lit16 v1, v1, -0x4e20

    invoke-virtual {v0, v1, v4}, Lcom/sec/vip/amschaton/al;->e(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 804
    if-nez v0, :cond_9

    .line 806
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    add-int/lit16 v1, v1, -0x7530

    invoke-virtual {v0, v1, v4}, Lcom/sec/vip/amschaton/al;->b(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 807
    if-nez v0, :cond_8

    .line 809
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1, v4}, Lcom/sec/vip/amschaton/al;->a(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 810
    if-nez v0, :cond_7

    .line 811
    iput v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    .line 827
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x3

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-virtual {v0, v1, v2, v4, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 828
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(I)V

    goto/16 :goto_0

    .line 814
    :cond_7
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 818
    :cond_8
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 822
    :cond_9
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 834
    :cond_a
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 836
    if-eqz v0, :cond_0

    .line 841
    const-string v1, "AMS_STAMP_INDEX"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    .line 843
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    if-ltz v0, :cond_b

    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v1

    if-le v0, v1, :cond_c

    .line 847
    :cond_b
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    const/16 v1, 0x7530

    if-ge v0, v1, :cond_d

    .line 849
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    add-int/lit16 v1, v1, -0x4e20

    invoke-virtual {v0, v1, v4}, Lcom/sec/vip/amschaton/al;->e(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 860
    :goto_2
    if-nez v0, :cond_f

    .line 862
    iput v4, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    .line 871
    :cond_c
    :goto_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x3

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    invoke-virtual {v0, v1, v2, v4, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 872
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(I)V

    .line 873
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C()V

    goto/16 :goto_0

    .line 851
    :cond_d
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    if-ge v0, v2, :cond_e

    .line 853
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    add-int/lit16 v1, v1, -0x7530

    invoke-virtual {v0, v1, v4}, Lcom/sec/vip/amschaton/al;->b(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2

    .line 857
    :cond_e
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Q:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1, v4}, Lcom/sec/vip/amschaton/al;->a(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2

    .line 866
    :cond_f
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_3

    .line 708
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xb -> :sswitch_1
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 883
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e(Z)V

    .line 885
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 932
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 887
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    if-eqz v0, :cond_1

    .line 888
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/aw;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 892
    :cond_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f()V

    goto :goto_0

    .line 895
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    if-eqz v0, :cond_2

    .line 896
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->A:Lcom/sec/vip/amschaton/aw;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/aw;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 900
    :cond_2
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->g()V

    goto :goto_0

    .line 903
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ae:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->af:Z

    if-nez v0, :cond_0

    .line 906
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ae:Z

    .line 907
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->r()V

    goto :goto_0

    .line 910
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->E()V

    goto :goto_0

    .line 913
    :pswitch_5
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->F()V

    goto :goto_0

    .line 916
    :pswitch_6
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ad:Z

    if-nez v0, :cond_0

    .line 917
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ad:Z

    .line 918
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->t()V

    goto :goto_0

    .line 922
    :pswitch_7
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B()V

    goto :goto_0

    .line 925
    :pswitch_8
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ae:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->af:Z

    if-nez v0, :cond_0

    .line 928
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->af:Z

    .line 929
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->q()V

    goto :goto_0

    .line 885
    nop

    :pswitch_data_0
    .packed-switch 0x7f070231
        :pswitch_8
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 434
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 436
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a()V

    .line 438
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v3, :cond_6

    .line 440
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Z:Z

    .line 449
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 451
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    move v1, v0

    .line 456
    :goto_1
    const-string v0, ""

    .line 457
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->L:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    if-eqz v2, :cond_1

    .line 459
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f07023d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 460
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    .line 461
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    .line 462
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bl;->b()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 465
    :cond_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->j()V

    .line 466
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->k()V

    .line 468
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v2, :cond_2

    .line 470
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->w()V

    .line 473
    :cond_2
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->l()V

    .line 475
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->L:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    if-eqz v2, :cond_3

    .line 478
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b(Ljava/lang/String;)V

    .line 479
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e(Z)V

    .line 486
    :cond_3
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->R:Z

    if-nez v0, :cond_4

    .line 488
    const/16 v0, 0x7d5

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->f(I)V

    .line 491
    :cond_4
    if-lez v1, :cond_5

    .line 493
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 495
    if-eqz v0, :cond_5

    .line 497
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 500
    :cond_5
    return-void

    .line 442
    :cond_6
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 444
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Z:Z

    goto/16 :goto_0

    :cond_7
    move v1, v0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 276
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreate(Landroid/os/Bundle;)V

    .line 277
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aj:Landroid/util/DisplayMetrics;

    .line 278
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->aj:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 279
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 280
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const v1, 0x7f0705a6

    .line 625
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->L:Z

    if-eqz v0, :cond_0

    .line 627
    const v0, 0x7f0f0023

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 640
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 641
    return-void

    .line 631
    :cond_0
    const v0, 0x7f0f0021

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 632
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 633
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 636
    :cond_1
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 285
    const v0, 0x7f030057

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 594
    sget-boolean v0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->Y:Z

    if-nez v0, :cond_0

    .line 596
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e()V

    .line 599
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->x:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 601
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->x:Landroid/graphics/Bitmap;

    .line 604
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 606
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 607
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y:Landroid/graphics/Bitmap;

    .line 610
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_3

    .line 612
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->u()V

    .line 613
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    .line 616
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ad:Z

    .line 617
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->b:Z

    .line 619
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onDestroy()V

    .line 620
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 662
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 684
    :goto_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->h()V

    .line 685
    return v4

    .line 664
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->L:Z

    if-eqz v0, :cond_1

    .line 665
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    if-eqz v0, :cond_0

    .line 666
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bl;->b()Landroid/widget/EditText;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/widget/EditText;Z)V

    .line 669
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/vip/amschaton/fragment/ai;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/ai;-><init>(Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 677
    :cond_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->y()V

    goto :goto_0

    .line 681
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->c()V

    goto :goto_0

    .line 662
    nop

    :pswitch_data_0
    .packed-switch 0x7f0705a5
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 537
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->L:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bl;->b()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/widget/EditText;Z)V

    .line 541
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->al:Lcom/sec/vip/amschaton/fragment/aq;

    if-eqz v0, :cond_1

    .line 542
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->al:Lcom/sec/vip/amschaton/fragment/aq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/aq;->cancel(Z)Z

    .line 544
    :cond_1
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onPause()V

    .line 545
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 646
    const v0, 0x7f0705a6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->z:Landroid/view/MenuItem;

    .line 648
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->L:Z

    if-eqz v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bl;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 650
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->z:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 657
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 658
    return-void

    .line 653
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->z:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 415
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onResume()V

    .line 417
    sput-boolean v2, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a:Z

    .line 419
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->L:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->O:Lcom/sec/vip/amschaton/bl;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bl;->b()Landroid/widget/EditText;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a(Landroid/widget/EditText;Z)V

    .line 422
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e(Z)V

    .line 427
    :goto_0
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->ae:Z

    .line 428
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->af:Z

    .line 429
    return-void

    .line 424
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->e(Z)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 550
    .line 552
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->B:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->t()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 554
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->d(Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 556
    const-string v0, "[onSaveInstanceState] Cannot save temp AMS file!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 560
    :cond_0
    const-string v2, "SI_SAVE_FLAG"

    iget-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->W:Z

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 561
    const-string v2, "SI_EDIT_FLAG"

    iget-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->X:Z

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 584
    :goto_0
    const-string v2, "SI_SAVE_TEMP_AMS"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 585
    const-string v0, "SI_CURRENT_TOOL_MODE"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->C:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 586
    sput-boolean v1, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->a:Z

    .line 588
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 589
    return-void

    .line 565
    :cond_1
    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    const/16 v3, 0x3e8

    if-eq v2, v3, :cond_3

    .line 567
    const-string v2, "restore_background"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568
    const-string v0, "BgType"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 570
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->T:I

    const/16 v2, 0x3ec

    if-ne v0, v2, :cond_2

    .line 571
    const-string v0, "bgColor"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->n:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move v0, v1

    goto :goto_0

    .line 573
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 574
    const-string v0, "BgBitmapParcel"

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->m:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 581
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 404
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onStart()V

    .line 405
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->X:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 406
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0102

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 411
    :goto_0
    return-void

    .line 409
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSComposerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b03f7

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    goto :goto_0
.end method
