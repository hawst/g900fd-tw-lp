.class public Lcom/sec/vip/amschaton/u;
.super Ljava/lang/Object;
.source "AMSObject.java"


# static fields
.field public static U:F

.field public static V:I


# instance fields
.field protected A:Lcom/sec/amsoma/structure/AMS_UI_DATA;

.field protected B:I

.field protected C:I

.field protected D:I

.field protected E:F

.field protected F:F

.field protected G:Z

.field protected H:Z

.field protected I:Z

.field protected J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

.field protected K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

.field protected L:Landroid/graphics/Rect;

.field protected M:Landroid/graphics/drawable/Drawable;

.field protected N:I

.field protected O:I

.field protected final P:F

.field protected final Q:I

.field protected R:F

.field protected S:Lcom/sec/vip/amschaton/v;

.field protected final T:F

.field protected final W:F

.field protected X:Landroid/graphics/Point;

.field Y:F

.field private Z:F

.field protected a:I

.field private aa:F

.field private ab:[F

.field private ac:I

.field private ad:Landroid/graphics/Paint;

.field protected b:I

.field protected c:I

.field protected d:F

.field protected e:I

.field protected f:Z

.field protected g:Landroid/graphics/Canvas;

.field protected h:Landroid/graphics/Bitmap;

.field protected i:Landroid/graphics/Bitmap;

.field protected j:Landroid/graphics/Canvas;

.field protected k:Landroid/graphics/Bitmap;

.field protected l:Landroid/graphics/Paint;

.field protected m:Landroid/graphics/drawable/Drawable;

.field protected n:Landroid/graphics/drawable/Drawable;

.field protected o:Landroid/graphics/drawable/Drawable;

.field protected p:Landroid/graphics/drawable/Drawable;

.field protected q:Landroid/graphics/drawable/Drawable;

.field protected r:Landroid/graphics/drawable/Drawable;

.field protected s:Landroid/graphics/drawable/Drawable;

.field protected t:Landroid/graphics/drawable/Drawable;

.field protected u:Landroid/graphics/drawable/Drawable;

.field protected v:Landroid/graphics/Paint;

.field protected w:Landroid/graphics/DashPathEffect;

.field protected x:Landroid/graphics/DashPathEffect;

.field protected y:Lcom/sec/amsoma/AMSLibs;

.field protected z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    const/high16 v0, 0x41a00000    # 20.0f

    sput v0, Lcom/sec/vip/amschaton/u;->U:F

    .line 149
    const/16 v0, 0x1e

    sput v0, Lcom/sec/vip/amschaton/u;->V:I

    return-void
.end method

.method public constructor <init>(III)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 169
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/vip/amschaton/u;-><init>(IIIIII)V

    .line 170
    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 7

    .prologue
    .line 173
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/vip/amschaton/u;-><init>(IIIIII)V

    .line 174
    return-void
.end method

.method public constructor <init>(IIIIII)V
    .locals 5

    .prologue
    const/high16 v4, 0x41480000    # 12.5f

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/vip/amschaton/u;->d:F

    .line 85
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    .line 86
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->h:Landroid/graphics/Bitmap;

    .line 87
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->i:Landroid/graphics/Bitmap;

    .line 88
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->j:Landroid/graphics/Canvas;

    .line 89
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->k:Landroid/graphics/Bitmap;

    .line 90
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->l:Landroid/graphics/Paint;

    .line 91
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->m:Landroid/graphics/drawable/Drawable;

    .line 92
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->n:Landroid/graphics/drawable/Drawable;

    .line 93
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->o:Landroid/graphics/drawable/Drawable;

    .line 94
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->p:Landroid/graphics/drawable/Drawable;

    .line 95
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->q:Landroid/graphics/drawable/Drawable;

    .line 97
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->r:Landroid/graphics/drawable/Drawable;

    .line 98
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->s:Landroid/graphics/drawable/Drawable;

    .line 99
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->t:Landroid/graphics/drawable/Drawable;

    .line 100
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->u:Landroid/graphics/drawable/Drawable;

    .line 102
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    .line 103
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->w:Landroid/graphics/DashPathEffect;

    .line 104
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->x:Landroid/graphics/DashPathEffect;

    .line 106
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->y:Lcom/sec/amsoma/AMSLibs;

    .line 107
    iput v2, p0, Lcom/sec/vip/amschaton/u;->z:I

    .line 108
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->A:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    .line 109
    iput v2, p0, Lcom/sec/vip/amschaton/u;->B:I

    .line 110
    iput v2, p0, Lcom/sec/vip/amschaton/u;->C:I

    .line 111
    iput v2, p0, Lcom/sec/vip/amschaton/u;->D:I

    .line 112
    iput v3, p0, Lcom/sec/vip/amschaton/u;->E:F

    .line 113
    iput v3, p0, Lcom/sec/vip/amschaton/u;->F:F

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/u;->G:Z

    .line 115
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/u;->H:Z

    .line 116
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/u;->I:Z

    .line 117
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    .line 118
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 120
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->L:Landroid/graphics/Rect;

    .line 121
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->M:Landroid/graphics/drawable/Drawable;

    .line 123
    iput v2, p0, Lcom/sec/vip/amschaton/u;->N:I

    .line 124
    iput v2, p0, Lcom/sec/vip/amschaton/u;->O:I

    .line 126
    iput v4, p0, Lcom/sec/vip/amschaton/u;->P:F

    .line 127
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/vip/amschaton/u;->Q:I

    .line 128
    iput v4, p0, Lcom/sec/vip/amschaton/u;->R:F

    .line 135
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->S:Lcom/sec/vip/amschaton/v;

    .line 142
    const/high16 v0, 0x45800000    # 4096.0f

    iput v0, p0, Lcom/sec/vip/amschaton/u;->T:F

    .line 151
    iput v3, p0, Lcom/sec/vip/amschaton/u;->Z:F

    .line 152
    iput v3, p0, Lcom/sec/vip/amschaton/u;->aa:F

    .line 153
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->ab:[F

    .line 154
    iput v2, p0, Lcom/sec/vip/amschaton/u;->ac:I

    .line 156
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/vip/amschaton/u;->W:F

    .line 736
    iput-object v1, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    .line 738
    iput v3, p0, Lcom/sec/vip/amschaton/u;->Y:F

    .line 177
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/vip/amschaton/u;->a(IIII)V

    .line 178
    iput p5, p0, Lcom/sec/vip/amschaton/u;->N:I

    .line 179
    iput p6, p0, Lcom/sec/vip/amschaton/u;->O:I

    .line 180
    return-void
.end method

.method private a(IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 183
    iput p2, p0, Lcom/sec/vip/amschaton/u;->b:I

    .line 184
    iput p1, p0, Lcom/sec/vip/amschaton/u;->a:I

    .line 185
    iput p3, p0, Lcom/sec/vip/amschaton/u;->c:I

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    .line 187
    iput p4, p0, Lcom/sec/vip/amschaton/u;->e:I

    .line 189
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->w:Landroid/graphics/DashPathEffect;

    .line 190
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v4, [F

    fill-array-data v1, :array_1

    const/high16 v2, -0x40800000    # -1.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->x:Landroid/graphics/DashPathEffect;

    .line 191
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    .line 192
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 193
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 194
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setDither(Z)V

    .line 195
    return-void

    .line 189
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data

    .line 190
    :array_1
    .array-data 4
        0x40400000    # 3.0f
        0x40e00000    # 7.0f
    .end array-data
.end method


# virtual methods
.method protected a(Landroid/graphics/RectF;)Landroid/graphics/Point;
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 772
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    div-float/2addr v0, v3

    .line 773
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    .line 774
    new-instance v2, Landroid/graphics/Point;

    float-to-int v0, v0

    float-to-int v1, v1

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 383
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->i:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 384
    return-void
.end method

.method public a(F)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x40a00000    # 5.0f

    .line 255
    iput p1, p0, Lcom/sec/vip/amschaton/u;->d:F

    .line 256
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v6, [F

    iget v2, p0, Lcom/sec/vip/amschaton/u;->d:F

    mul-float/2addr v2, v3

    aput v2, v1, v4

    iget v2, p0, Lcom/sec/vip/amschaton/u;->d:F

    mul-float/2addr v2, v3

    aput v2, v1, v5

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->w:Landroid/graphics/DashPathEffect;

    .line 257
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v6, [F

    const/high16 v2, 0x40400000    # 3.0f

    iget v3, p0, Lcom/sec/vip/amschaton/u;->d:F

    mul-float/2addr v2, v3

    aput v2, v1, v4

    const/high16 v2, 0x40e00000    # 7.0f

    iget v3, p0, Lcom/sec/vip/amschaton/u;->d:F

    mul-float/2addr v2, v3

    aput v2, v1, v5

    iget v2, p0, Lcom/sec/vip/amschaton/u;->d:F

    neg-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->x:Landroid/graphics/DashPathEffect;

    .line 258
    return-void
.end method

.method public a(FF)V
    .locals 0

    .prologue
    .line 364
    return-void
.end method

.method protected a(FFFF)V
    .locals 11

    .prologue
    .line 635
    iget v0, p0, Lcom/sec/vip/amschaton/u;->B:I

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/sec/vip/amschaton/u;->B:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_9

    .line 636
    iget v1, p0, Lcom/sec/vip/amschaton/u;->d:F

    .line 638
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    .line 644
    const/high16 v0, 0x41480000    # 12.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    neg-int v2, v0

    .line 645
    const/high16 v0, 0x41480000    # 12.5f

    mul-float/2addr v0, v1

    float-to-int v3, v0

    .line 646
    const/high16 v0, 0x41480000    # 12.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    neg-int v4, v0

    .line 647
    const/high16 v0, 0x41480000    # 12.5f

    mul-float/2addr v0, v1

    float-to-int v5, v0

    .line 648
    iget v0, p0, Lcom/sec/vip/amschaton/u;->R:F

    mul-float/2addr v0, v1

    .line 649
    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->u:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_0

    .line 650
    iget v0, p0, Lcom/sec/vip/amschaton/u;->R:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    .line 654
    :cond_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->m:Landroid/graphics/drawable/Drawable;

    sub-float v6, p1, v0

    float-to-int v6, v6

    sub-float v7, p2, v0

    float-to-int v7, v7

    add-float v8, p3, v0

    float-to-int v8, v8

    add-float v9, p4, v0

    float-to-int v9, v9

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 655
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->m:Landroid/graphics/drawable/Drawable;

    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    invoke-virtual {v1, v6}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 659
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 661
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->q:Landroid/graphics/drawable/Drawable;

    int-to-float v6, v2

    add-float/2addr v6, p3

    add-float/2addr v6, v0

    float-to-int v6, v6

    int-to-float v7, v4

    add-float/2addr v7, p2

    sub-float/2addr v7, v0

    float-to-int v7, v7

    int-to-float v8, v3

    add-float/2addr v8, p3

    add-float/2addr v8, v0

    float-to-int v8, v8

    int-to-float v9, v5

    add-float/2addr v9, p2

    sub-float/2addr v9, v0

    float-to-int v9, v9

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 662
    const/16 v1, 0x65

    const/4 v6, 0x0

    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 667
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 669
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->n:Landroid/graphics/drawable/Drawable;

    int-to-float v6, v2

    add-float/2addr v6, p1

    sub-float/2addr v6, v0

    float-to-int v6, v6

    int-to-float v7, v4

    add-float/2addr v7, p2

    sub-float/2addr v7, v0

    float-to-int v7, v7

    int-to-float v8, v3

    add-float/2addr v8, p1

    sub-float/2addr v8, v0

    float-to-int v8, v8

    int-to-float v9, v5

    add-float/2addr v9, p2

    sub-float/2addr v9, v0

    float-to-int v9, v9

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 670
    const/16 v1, 0x66

    const/4 v6, 0x0

    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 673
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 675
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->o:Landroid/graphics/drawable/Drawable;

    int-to-float v6, v2

    add-float/2addr v6, p3

    add-float/2addr v6, v0

    float-to-int v6, v6

    int-to-float v7, v4

    add-float/2addr v7, p4

    add-float/2addr v7, v0

    float-to-int v7, v7

    int-to-float v8, v3

    add-float/2addr v8, p3

    add-float/2addr v8, v0

    float-to-int v8, v8

    int-to-float v9, v5

    add-float/2addr v9, p4

    add-float/2addr v9, v0

    float-to-int v9, v9

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 676
    const/16 v1, 0x67

    const/4 v6, 0x0

    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 679
    :cond_3
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_4

    .line 681
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->p:Landroid/graphics/drawable/Drawable;

    int-to-float v6, v2

    add-float/2addr v6, p1

    sub-float/2addr v6, v0

    float-to-int v6, v6

    int-to-float v7, v4

    add-float/2addr v7, p4

    add-float/2addr v7, v0

    float-to-int v7, v7

    int-to-float v8, v3

    add-float/2addr v8, p1

    sub-float/2addr v8, v0

    float-to-int v8, v8

    int-to-float v9, v5

    add-float/2addr v9, p4

    add-float/2addr v9, v0

    float-to-int v9, v9

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 682
    const/16 v1, 0x68

    const/4 v6, 0x0

    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 687
    :cond_4
    add-float v1, p2, p4

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v1, v6

    .line 688
    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_5

    .line 691
    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->r:Landroid/graphics/drawable/Drawable;

    int-to-float v7, v2

    add-float/2addr v7, p1

    sub-float/2addr v7, v0

    float-to-int v7, v7

    int-to-float v8, v4

    add-float/2addr v8, v1

    float-to-int v8, v8

    int-to-float v9, v3

    add-float/2addr v9, p1

    sub-float/2addr v9, v0

    float-to-int v9, v9

    int-to-float v10, v5

    add-float/2addr v10, v1

    float-to-int v10, v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 692
    const/16 v6, 0x69

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 694
    :cond_5
    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_6

    .line 697
    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->t:Landroid/graphics/drawable/Drawable;

    int-to-float v7, v2

    add-float/2addr v7, p3

    add-float/2addr v7, v0

    float-to-int v7, v7

    int-to-float v8, v4

    add-float/2addr v8, v1

    float-to-int v8, v8

    int-to-float v9, v3

    add-float/2addr v9, p3

    add-float/2addr v9, v0

    float-to-int v9, v9

    int-to-float v10, v5

    add-float/2addr v1, v10

    float-to-int v1, v1

    invoke-virtual {v6, v7, v8, v9, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 698
    const/16 v1, 0x6b

    const/4 v6, 0x0

    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 702
    :cond_6
    add-float v1, p1, p3

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v1, v6

    .line 703
    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_7

    .line 706
    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->s:Landroid/graphics/drawable/Drawable;

    int-to-float v7, v2

    add-float/2addr v7, v1

    float-to-int v7, v7

    int-to-float v8, v4

    add-float/2addr v8, p2

    sub-float/2addr v8, v0

    float-to-int v8, v8

    int-to-float v9, v3

    add-float/2addr v9, v1

    float-to-int v9, v9

    int-to-float v10, v5

    add-float/2addr v10, p2

    sub-float/2addr v10, v0

    float-to-int v10, v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 707
    const/16 v6, 0x6a

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 709
    :cond_7
    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->u:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_8

    .line 712
    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->u:Landroid/graphics/drawable/Drawable;

    int-to-float v2, v2

    add-float/2addr v2, v1

    float-to-int v2, v2

    int-to-float v4, v4

    add-float/2addr v4, p4

    add-float/2addr v4, v0

    float-to-int v4, v4

    int-to-float v3, v3

    add-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v5

    add-float/2addr v3, p4

    add-float/2addr v0, v3

    float-to-int v0, v0

    invoke-virtual {v6, v2, v4, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 713
    const/16 v0, 0x6c

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 716
    :cond_8
    instance-of v0, p0, Lcom/sec/vip/amschaton/z;

    if-eqz v0, :cond_9

    .line 720
    const/4 v1, 0x0

    float-to-int v0, p1

    int-to-float v2, v0

    float-to-int v0, p2

    int-to-float v3, v0

    float-to-int v0, p3

    int-to-float v4, v0

    float-to-int v0, p4

    int-to-float v5, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/u;->a(ZFFFF)V

    .line 734
    :cond_9
    :goto_0
    return-void

    .line 724
    :cond_a
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->w:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 725
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    mul-float/2addr v2, v1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 726
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 727
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, p1, p2, p3, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 728
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->x:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 729
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    const/high16 v2, 0x40400000    # 3.0f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 730
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 731
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, p1, p2, p3, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 500
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/vip/amschaton/u;->R:F

    .line 501
    return-void
.end method

.method protected a(IZ)V
    .locals 5

    .prologue
    .line 822
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 824
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    iget v1, p0, Lcom/sec/vip/amschaton/u;->e:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->X:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->X:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 825
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 826
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    iget v1, p0, Lcom/sec/vip/amschaton/u;->e:I

    neg-int v1, v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->X:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->X:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 827
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 245
    invoke-virtual {p0, p1, p1}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 246
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 231
    iput-object p1, p0, Lcom/sec/vip/amschaton/u;->h:Landroid/graphics/Bitmap;

    .line 232
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->h:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    .line 233
    iput-object p2, p0, Lcom/sec/vip/amschaton/u;->i:Landroid/graphics/Bitmap;

    .line 234
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->i:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->j:Landroid/graphics/Canvas;

    .line 235
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->j:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->h:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 236
    return-void
.end method

.method public a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 970
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->L:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 971
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->L:Landroid/graphics/Rect;

    .line 973
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->L:Landroid/graphics/Rect;

    .line 974
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/sec/vip/amschaton/u;->m:Landroid/graphics/drawable/Drawable;

    .line 480
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 483
    iput-object p1, p0, Lcom/sec/vip/amschaton/u;->n:Landroid/graphics/drawable/Drawable;

    .line 484
    iput-object p2, p0, Lcom/sec/vip/amschaton/u;->o:Landroid/graphics/drawable/Drawable;

    .line 485
    iput-object p3, p0, Lcom/sec/vip/amschaton/u;->p:Landroid/graphics/drawable/Drawable;

    .line 486
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 489
    iput-object p1, p0, Lcom/sec/vip/amschaton/u;->r:Landroid/graphics/drawable/Drawable;

    .line 490
    iput-object p2, p0, Lcom/sec/vip/amschaton/u;->s:Landroid/graphics/drawable/Drawable;

    .line 491
    iput-object p3, p0, Lcom/sec/vip/amschaton/u;->t:Landroid/graphics/drawable/Drawable;

    .line 492
    iput-object p4, p0, Lcom/sec/vip/amschaton/u;->u:Landroid/graphics/drawable/Drawable;

    .line 493
    return-void
.end method

.method public a(Lcom/sec/amsoma/AMSLibs;ILcom/sec/amsoma/structure/AMS_UI_DATA;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/vip/amschaton/u;->y:Lcom/sec/amsoma/AMSLibs;

    .line 218
    iput p2, p0, Lcom/sec/vip/amschaton/u;->z:I

    .line 219
    iput-object p3, p0, Lcom/sec/vip/amschaton/u;->A:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    .line 220
    return-void
.end method

.method public a(Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;)V
    .locals 3

    .prologue
    .line 292
    iput-object p1, p0, Lcom/sec/vip/amschaton/u;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    .line 293
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->y:Lcom/sec/amsoma/AMSLibs;

    if-eqz v0, :cond_0

    .line 294
    new-instance v0, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 295
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->y:Lcom/sec/amsoma/AMSLibs;

    invoke-virtual {p1}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->getM_pSelectObjectData()I

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetSelectObjectData(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)V

    .line 299
    :goto_0
    return-void

    .line 297
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    goto :goto_0
.end method

.method public a(Lcom/sec/vip/amschaton/v;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/vip/amschaton/u;->S:Lcom/sec/vip/amschaton/v;

    .line 139
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 267
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/u;->H:Z

    .line 268
    if-eqz p1, :cond_0

    .line 269
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/u;->B:I

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/u;->B:I

    goto :goto_0
.end method

.method protected a(ZFFFF)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 785
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 786
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    .line 788
    :cond_0
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/sec/vip/amschaton/u;->b(FFFF)Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->X:Landroid/graphics/Point;

    .line 789
    const/high16 v0, 0x42200000    # 40.0f

    .line 790
    sub-float v2, p3, p5

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 793
    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->save()I

    .line 794
    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/vip/amschaton/u;->X:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/u;->X:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    sub-float v2, v5, v2

    invoke-virtual {v3, v4, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 797
    iget v2, p0, Lcom/sec/vip/amschaton/u;->d:F

    mul-float/2addr v0, v2

    neg-float v4, v0

    .line 799
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 800
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 801
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->w:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 802
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    move v2, v1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 804
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    const/16 v2, -0x100

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 805
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 808
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 809
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 810
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 811
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 812
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 814
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 815
    const v2, 0x7f0203f7

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 816
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    .line 817
    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    sub-float/2addr v1, v2

    sub-float v2, v4, v2

    iget-object v4, p0, Lcom/sec/vip/amschaton/u;->ad:Landroid/graphics/Paint;

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 818
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 819
    return-void
.end method

.method public a(FFFFI)Z
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x0

    return v0
.end method

.method public a(FFFFIZ)Z
    .locals 1

    .prologue
    .line 471
    const/4 v0, 0x0

    return v0
.end method

.method public a(FFFFLjava/lang/String;I)Z
    .locals 1

    .prologue
    .line 463
    const/4 v0, 0x0

    return v0
.end method

.method public a(FFFFLjava/lang/String;IZ)Z
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x0

    return v0
.end method

.method public a(FFI)Z
    .locals 1

    .prologue
    .line 424
    const/4 v0, 0x0

    return v0
.end method

.method public a(FFIF)Z
    .locals 1

    .prologue
    .line 428
    const/4 v0, 0x0

    return v0
.end method

.method public a(FFIFZ)Z
    .locals 1

    .prologue
    .line 432
    const/4 v0, 0x0

    return v0
.end method

.method public a(FFIZ)Z
    .locals 1

    .prologue
    .line 467
    const/4 v0, 0x0

    return v0
.end method

.method public a(FFLjava/lang/String;I)Z
    .locals 1

    .prologue
    .line 459
    const/4 v0, 0x0

    return v0
.end method

.method public a(III)Z
    .locals 1

    .prologue
    .line 336
    iput p1, p0, Lcom/sec/vip/amschaton/u;->a:I

    .line 337
    iput p2, p0, Lcom/sec/vip/amschaton/u;->b:I

    .line 338
    iput p3, p0, Lcom/sec/vip/amschaton/u;->c:I

    .line 339
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/graphics/RectF;I)Z
    .locals 1

    .prologue
    .line 436
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/graphics/RectF;IZ)Z
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/sec/amsoma/structure/AMS_RECT;)Z
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x0

    return v0
.end method

.method protected b(FFFF)Landroid/graphics/Point;
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 778
    add-float v0, p3, p1

    div-float/2addr v0, v2

    .line 779
    add-float v1, p4, p2

    div-float/2addr v1, v2

    .line 780
    new-instance v2, Landroid/graphics/Point;

    float-to-int v0, v0

    float-to-int v1, v1

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2
.end method

.method protected b()Landroid/graphics/RectF;
    .locals 9

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 740
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/u;->c()Landroid/graphics/Point;

    move-result-object v1

    .line 741
    const/high16 v2, 0x42200000    # 40.0f

    .line 742
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 743
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 744
    iput-boolean v7, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 746
    iget v4, p0, Lcom/sec/vip/amschaton/u;->Y:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    .line 747
    const v4, 0x7f0203f7

    invoke-static {v0, v4, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 748
    const v0, 0x3fa66666    # 1.3f

    iget v3, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    div-float/2addr v0, v6

    iput v0, p0, Lcom/sec/vip/amschaton/u;->Y:F

    .line 750
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/u;->R:F

    iget v3, p0, Lcom/sec/vip/amschaton/u;->d:F

    mul-float/2addr v0, v3

    .line 751
    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/vip/amschaton/u;->u:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_1

    .line 752
    iget v0, p0, Lcom/sec/vip/amschaton/u;->R:F

    iget v3, p0, Lcom/sec/vip/amschaton/u;->d:F

    mul-float/2addr v0, v3

    mul-float/2addr v0, v6

    .line 754
    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [F

    .line 755
    iget v4, v1, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    aput v4, v3, v8

    .line 756
    iget v4, v1, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v5

    iget-short v5, v5, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    iget-object v6, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v6}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v6

    iget-short v6, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v0, v5

    iget v5, p0, Lcom/sec/vip/amschaton/u;->d:F

    mul-float/2addr v2, v5

    add-float/2addr v0, v2

    sub-float v0, v4, v0

    aput v0, v3, v7

    .line 757
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 758
    iget v2, p0, Lcom/sec/vip/amschaton/u;->e:I

    int-to-float v2, v2

    iget v4, v1, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {v0, v2, v4, v1}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 760
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 762
    new-instance v0, Landroid/graphics/RectF;

    aget v1, v3, v8

    iget v2, p0, Lcom/sec/vip/amschaton/u;->Y:F

    sub-float/2addr v1, v2

    aget v2, v3, v7

    iget v4, p0, Lcom/sec/vip/amschaton/u;->Y:F

    sub-float/2addr v2, v4

    aget v4, v3, v8

    iget v5, p0, Lcom/sec/vip/amschaton/u;->Y:F

    add-float/2addr v4, v5

    aget v3, v3, v7

    iget v5, p0, Lcom/sec/vip/amschaton/u;->Y:F

    add-float/2addr v3, v5

    invoke-direct {v0, v1, v2, v4, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public b(F)V
    .locals 1

    .prologue
    .line 979
    iput p1, p0, Lcom/sec/vip/amschaton/u;->Z:F

    .line 980
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 984
    :goto_0
    return-void

    .line 983
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/u;->Z:F

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/u;->c(F)V

    goto :goto_0
.end method

.method public b(FF)V
    .locals 0

    .prologue
    .line 376
    return-void
.end method

.method protected b(FFFFI)V
    .locals 20

    .prologue
    .line 508
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/vip/amschaton/u;->B:I

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/vip/amschaton/u;->B:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_a

    .line 509
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/vip/amschaton/u;->d:F

    .line 511
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/u;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_b

    .line 517
    const/high16 v4, 0x41480000    # 12.5f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    neg-int v6, v4

    .line 518
    const/high16 v4, 0x41480000    # 12.5f

    mul-float/2addr v4, v5

    float-to-int v7, v4

    .line 519
    const/high16 v4, 0x41480000    # 12.5f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    neg-int v8, v4

    .line 520
    const/high16 v4, 0x41480000    # 12.5f

    mul-float/2addr v4, v5

    float-to-int v9, v4

    .line 521
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/vip/amschaton/u;->R:F

    mul-float/2addr v4, v5

    .line 522
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/vip/amschaton/u;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/vip/amschaton/u;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/vip/amschaton/u;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/vip/amschaton/u;->u:Landroid/graphics/drawable/Drawable;

    if-eqz v10, :cond_0

    .line 523
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/vip/amschaton/u;->R:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    .line 527
    :cond_0
    sub-float v10, p1, v4

    .line 528
    sub-float v11, p2, v4

    .line 529
    add-float v12, p3, v4

    .line 530
    add-float v13, p4, v4

    .line 531
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v10, v11, v12, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 534
    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    .line 535
    invoke-virtual {v5, v14}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 537
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/vip/amschaton/u;->e:I

    int-to-float v15, v15

    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v5, v15, v0, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 538
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v14}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 539
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->m:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    invoke-virtual {v5, v15}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 544
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_1

    .line 546
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->q:Landroid/graphics/drawable/Drawable;

    int-to-float v15, v6

    add-float v15, v15, p3

    add-float/2addr v15, v4

    float-to-int v15, v15

    int-to-float v0, v8

    move/from16 v16, v0

    add-float v16, v16, p2

    sub-float v16, v16, v4

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    int-to-float v0, v7

    move/from16 v17, v0

    add-float v17, v17, p3

    add-float v17, v17, v4

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    int-to-float v0, v9

    move/from16 v18, v0

    add-float v18, v18, p2

    sub-float v18, v18, v4

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v5, v15, v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 547
    const/16 v5, 0x65

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v15}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 552
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_2

    .line 554
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->n:Landroid/graphics/drawable/Drawable;

    int-to-float v15, v6

    add-float v15, v15, p1

    sub-float/2addr v15, v4

    float-to-int v15, v15

    int-to-float v0, v8

    move/from16 v16, v0

    add-float v16, v16, p2

    sub-float v16, v16, v4

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    int-to-float v0, v7

    move/from16 v17, v0

    add-float v17, v17, p1

    sub-float v17, v17, v4

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    int-to-float v0, v9

    move/from16 v18, v0

    add-float v18, v18, p2

    sub-float v18, v18, v4

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v5, v15, v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 555
    const/16 v5, 0x66

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v15}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 558
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_3

    .line 560
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->o:Landroid/graphics/drawable/Drawable;

    int-to-float v15, v6

    add-float v15, v15, p3

    add-float/2addr v15, v4

    float-to-int v15, v15

    int-to-float v0, v8

    move/from16 v16, v0

    add-float v16, v16, p4

    add-float v16, v16, v4

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    int-to-float v0, v7

    move/from16 v17, v0

    add-float v17, v17, p3

    add-float v17, v17, v4

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    int-to-float v0, v9

    move/from16 v18, v0

    add-float v18, v18, p4

    add-float v18, v18, v4

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v5, v15, v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 561
    const/16 v5, 0x67

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v15}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 564
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_4

    .line 566
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/u;->p:Landroid/graphics/drawable/Drawable;

    int-to-float v15, v6

    add-float v15, v15, p1

    sub-float/2addr v15, v4

    float-to-int v15, v15

    int-to-float v0, v8

    move/from16 v16, v0

    add-float v16, v16, p4

    add-float v16, v16, v4

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    int-to-float v0, v7

    move/from16 v17, v0

    add-float v17, v17, p1

    sub-float v17, v17, v4

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    int-to-float v0, v9

    move/from16 v18, v0

    add-float v18, v18, p4

    add-float v18, v18, v4

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v5, v15, v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 567
    const/16 v5, 0x68

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v15}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 572
    :cond_4
    add-float v5, p2, p4

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v5, v15

    .line 573
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/vip/amschaton/u;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v15, :cond_5

    .line 576
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/vip/amschaton/u;->r:Landroid/graphics/drawable/Drawable;

    int-to-float v0, v6

    move/from16 v16, v0

    add-float v16, v16, p1

    sub-float v16, v16, v4

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    int-to-float v0, v8

    move/from16 v17, v0

    add-float v17, v17, v5

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    int-to-float v0, v7

    move/from16 v18, v0

    add-float v18, v18, p1

    sub-float v18, v18, v4

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    int-to-float v0, v9

    move/from16 v19, v0

    add-float v19, v19, v5

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v15 .. v19}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 577
    const/16 v15, 0x69

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 579
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/vip/amschaton/u;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v15, :cond_6

    .line 582
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/vip/amschaton/u;->t:Landroid/graphics/drawable/Drawable;

    int-to-float v0, v6

    move/from16 v16, v0

    add-float v16, v16, p3

    add-float v16, v16, v4

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    int-to-float v0, v8

    move/from16 v17, v0

    add-float v17, v17, v5

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    int-to-float v0, v7

    move/from16 v18, v0

    add-float v18, v18, p3

    add-float v18, v18, v4

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    int-to-float v0, v9

    move/from16 v19, v0

    add-float v5, v5, v19

    float-to-int v5, v5

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v15, v0, v1, v2, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 583
    const/16 v5, 0x6b

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v15}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 587
    :cond_6
    add-float v5, p1, p3

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v5, v15

    .line 588
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/vip/amschaton/u;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v15, :cond_7

    .line 591
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/vip/amschaton/u;->s:Landroid/graphics/drawable/Drawable;

    int-to-float v0, v6

    move/from16 v16, v0

    add-float v16, v16, v5

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    int-to-float v0, v8

    move/from16 v17, v0

    add-float v17, v17, p2

    sub-float v17, v17, v4

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    int-to-float v0, v7

    move/from16 v18, v0

    add-float v18, v18, v5

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    int-to-float v0, v9

    move/from16 v19, v0

    add-float v19, v19, p2

    sub-float v19, v19, v4

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v15 .. v19}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 592
    const/16 v15, 0x6a

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 594
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/vip/amschaton/u;->u:Landroid/graphics/drawable/Drawable;

    if-eqz v15, :cond_8

    .line 597
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/vip/amschaton/u;->u:Landroid/graphics/drawable/Drawable;

    int-to-float v6, v6

    add-float/2addr v6, v5

    float-to-int v6, v6

    int-to-float v8, v8

    add-float v8, v8, p4

    add-float/2addr v8, v4

    float-to-int v8, v8

    int-to-float v7, v7

    add-float/2addr v5, v7

    float-to-int v5, v5

    int-to-float v7, v9

    add-float v7, v7, p4

    add-float/2addr v4, v7

    float-to-int v4, v4

    invoke-virtual {v15, v6, v8, v5, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 598
    const/16 v4, 0x6c

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/sec/vip/amschaton/u;->b(IZ)V

    .line 601
    :cond_8
    move-object/from16 v0, p0

    instance-of v4, v0, Lcom/sec/vip/amschaton/z;

    if-eqz v4, :cond_9

    .line 605
    const/4 v5, 0x0

    float-to-int v4, v10

    int-to-float v6, v4

    float-to-int v4, v11

    int-to-float v7, v4

    float-to-int v4, v12

    int-to-float v8, v4

    float-to-int v4, v13

    int-to-float v9, v4

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v9}, Lcom/sec/vip/amschaton/u;->a(ZFFFF)V

    .line 607
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/vip/amschaton/u;->e:I

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v4, v5, v6, v7}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 621
    :cond_a
    :goto_0
    return-void

    .line 610
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/vip/amschaton/u;->w:Landroid/graphics/DashPathEffect;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 611
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    const/high16 v6, 0x40a00000    # 5.0f

    mul-float/2addr v6, v5

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 612
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    const/high16 v6, -0x1000000

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 613
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    new-instance v6, Landroid/graphics/RectF;

    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v6, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 614
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/vip/amschaton/u;->x:Landroid/graphics/DashPathEffect;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 615
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    const/high16 v6, 0x40400000    # 3.0f

    mul-float/2addr v5, v6

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 616
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 617
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    new-instance v5, Landroid/graphics/RectF;

    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/vip/amschaton/u;->v:Landroid/graphics/Paint;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected b(IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 831
    .line 832
    packed-switch p1, :pswitch_data_0

    .line 858
    const/4 v0, 0x0

    .line 861
    :goto_0
    if-nez v0, :cond_0

    .line 873
    :goto_1
    return-void

    .line 834
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->q:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 837
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->n:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 840
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->o:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 843
    :pswitch_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->p:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 846
    :pswitch_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->r:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 849
    :pswitch_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->s:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 852
    :pswitch_6
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 855
    :pswitch_7
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->u:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 864
    :cond_0
    if-eqz p2, :cond_1

    .line 865
    new-array v1, v3, [I

    const v2, 0x10100a7

    aput v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 870
    :goto_2
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 871
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 872
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 867
    :cond_1
    new-array v1, v3, [I

    const v2, -0x10100a7

    aput v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_2

    .line 832
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public b(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/sec/vip/amschaton/u;->k:Landroid/graphics/Bitmap;

    .line 309
    return-void
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 496
    iput-object p1, p0, Lcom/sec/vip/amschaton/u;->q:Landroid/graphics/drawable/Drawable;

    .line 497
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 282
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/u;->G:Z

    .line 283
    return-void
.end method

.method public b(Lcom/sec/amsoma/structure/AMS_RECT;)Z
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x0

    return v0
.end method

.method protected c()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 766
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 767
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 768
    new-instance v2, Landroid/graphics/Point;

    float-to-int v0, v0

    float-to-int v1, v1

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2
.end method

.method protected c(F)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1002
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->ab:[F

    if-eqz v1, :cond_3

    .line 1003
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->ab:[F

    iget v2, p0, Lcom/sec/vip/amschaton/u;->ac:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/vip/amschaton/u;->ac:I

    aput p1, v1, v2

    .line 1004
    iget v1, p0, Lcom/sec/vip/amschaton/u;->ac:I

    sget v2, Lcom/sec/vip/amschaton/u;->V:I

    rem-int/2addr v1, v2

    if-nez v1, :cond_0

    .line 1005
    iput v0, p0, Lcom/sec/vip/amschaton/u;->ac:I

    .line 1007
    :cond_0
    const/4 v1, 0x0

    .line 1008
    :goto_0
    sget v2, Lcom/sec/vip/amschaton/u;->V:I

    if-ge v0, v2, :cond_2

    .line 1009
    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->ab:[F

    if-nez v2, :cond_1

    .line 1010
    iget v2, p0, Lcom/sec/vip/amschaton/u;->Z:F

    add-float/2addr v1, v2

    .line 1008
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1012
    :cond_1
    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->ab:[F

    aget v2, v2, v0

    add-float/2addr v1, v2

    goto :goto_1

    .line 1015
    :cond_2
    sget v0, Lcom/sec/vip/amschaton/u;->V:I

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/sec/vip/amschaton/u;->aa:F

    .line 1032
    :cond_3
    return-void
.end method

.method public c(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 504
    iput-object p1, p0, Lcom/sec/vip/amschaton/u;->M:Landroid/graphics/drawable/Drawable;

    .line 505
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 1041
    return-void
.end method

.method public c(FF)Z
    .locals 1

    .prologue
    .line 886
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 937
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v1, :cond_1

    .line 950
    :cond_0
    :goto_0
    return v0

    .line 940
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-eqz v1, :cond_0

    .line 943
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/u;->H:Z

    if-eqz v1, :cond_0

    .line 946
    iput v0, p0, Lcom/sec/vip/amschaton/u;->B:I

    .line 947
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/u;->z:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/u;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v0, v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_DeleteSelectObject(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;)Z

    move-result v0

    .line 948
    iput-object v3, p0, Lcom/sec/vip/amschaton/u;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    .line 949
    iput-object v3, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    goto :goto_0
.end method

.method public d(FF)Z
    .locals 1

    .prologue
    .line 900
    const/4 v0, 0x0

    return v0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 958
    return-void
.end method

.method public e(FF)Z
    .locals 1

    .prologue
    .line 914
    const/4 v0, 0x0

    return v0
.end method

.method public f()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 966
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->L:Landroid/graphics/Rect;

    return-object v0
.end method

.method public f(FF)Z
    .locals 1

    .prologue
    .line 928
    const/4 v0, 0x0

    return v0
.end method

.method public g()F
    .locals 1

    .prologue
    .line 987
    iget v0, p0, Lcom/sec/vip/amschaton/u;->Z:F

    return v0
.end method

.method protected h()V
    .locals 3

    .prologue
    .line 991
    sget v0, Lcom/sec/vip/amschaton/u;->V:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->ab:[F

    .line 992
    const/4 v0, 0x0

    :goto_0
    sget v1, Lcom/sec/vip/amschaton/u;->V:I

    if-ge v0, v1, :cond_0

    .line 993
    iget-object v1, p0, Lcom/sec/vip/amschaton/u;->ab:[F

    const/4 v2, 0x0

    aput v2, v1, v0

    .line 992
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 995
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 998
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/u;->ab:[F

    .line 999
    return-void
.end method

.method protected j()F
    .locals 1

    .prologue
    .line 1035
    iget v0, p0, Lcom/sec/vip/amschaton/u;->aa:F

    return v0
.end method

.method public k()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    if-eqz v0, :cond_0

    .line 1045
    iget-object v0, p0, Lcom/sec/vip/amschaton/u;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    .line 1046
    new-instance v0, Landroid/graphics/Rect;

    iget-short v2, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget-short v3, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget-short v4, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    invoke-direct {v0, v2, v3, v4, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1048
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
