.class public Lcom/sec/vip/amschaton/i;
.super Ljava/lang/Object;
.source "AMSColorPickerDialog.java"

# interfaces
.implements Lcom/sec/vip/amschaton/bh;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/view/View;

.field private c:Lcom/sec/common/a/d;

.field private d:Lcom/sec/vip/amschaton/ColorPickerView;

.field private e:I

.field private f:I

.field private g:I

.field private h:Lcom/sec/vip/amschaton/bi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v2, p0, Lcom/sec/vip/amschaton/i;->d:Lcom/sec/vip/amschaton/ColorPickerView;

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/vip/amschaton/i;->e:I

    .line 25
    iput v1, p0, Lcom/sec/vip/amschaton/i;->f:I

    .line 26
    iput v1, p0, Lcom/sec/vip/amschaton/i;->g:I

    .line 28
    iput-object v2, p0, Lcom/sec/vip/amschaton/i;->h:Lcom/sec/vip/amschaton/bi;

    .line 31
    iput-object p1, p0, Lcom/sec/vip/amschaton/i;->a:Landroid/content/Context;

    .line 33
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 34
    const v1, 0x7f030056

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/i;->b:Landroid/view/View;

    .line 36
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09014c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/i;->f:I

    .line 37
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09014d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/i;->g:I

    .line 39
    invoke-direct {p0}, Lcom/sec/vip/amschaton/i;->f()V

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/i;)Lcom/sec/vip/amschaton/bi;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->h:Lcom/sec/vip/amschaton/bi;

    return-object v0
.end method

.method private f()V
    .locals 5

    .prologue
    .line 69
    new-instance v0, Lcom/sec/vip/amschaton/ColorPickerView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/i;->a:Landroid/content/Context;

    iget v2, p0, Lcom/sec/vip/amschaton/i;->f:I

    iget v3, p0, Lcom/sec/vip/amschaton/i;->g:I

    const/4 v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/ColorPickerView;-><init>(Landroid/content/Context;III)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/i;->d:Lcom/sec/vip/amschaton/ColorPickerView;

    .line 70
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->d:Lcom/sec/vip/amschaton/ColorPickerView;

    invoke-virtual {v0, p0}, Lcom/sec/vip/amschaton/ColorPickerView;->setColorChangedListener(Lcom/sec/vip/amschaton/bh;)V

    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->d:Lcom/sec/vip/amschaton/ColorPickerView;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/ColorPickerView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 76
    :cond_0
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 77
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/sec/vip/amschaton/i;->f:I

    iget v3, p0, Lcom/sec/vip/amschaton/i;->g:I

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 78
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->d:Lcom/sec/vip/amschaton/ColorPickerView;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->b:Landroid/view/View;

    const v2, 0x7f07022e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 82
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 83
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 44
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/i;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b012e

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    new-instance v2, Lcom/sec/vip/amschaton/k;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/k;-><init>(Lcom/sec/vip/amschaton/i;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/vip/amschaton/j;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/j;-><init>(Lcom/sec/vip/amschaton/i;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/i;->c:Lcom/sec/common/a/d;

    .line 66
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 86
    iput p1, p0, Lcom/sec/vip/amschaton/i;->e:I

    .line 87
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->d:Lcom/sec/vip/amschaton/ColorPickerView;

    iget v1, p0, Lcom/sec/vip/amschaton/i;->e:I

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/ColorPickerView;->setColor(I)V

    .line 88
    return-void
.end method

.method public a(Lcom/sec/vip/amschaton/bi;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/vip/amschaton/i;->h:Lcom/sec/vip/amschaton/bi;

    .line 112
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/sec/vip/amschaton/i;->e:I

    return v0
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 116
    iput p1, p0, Lcom/sec/vip/amschaton/i;->e:I

    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->c:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->isShowing()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->c:Lcom/sec/common/a/d;

    if-nez v0, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/i;->a()V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->c:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 104
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/vip/amschaton/i;->c:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 108
    return-void
.end method
