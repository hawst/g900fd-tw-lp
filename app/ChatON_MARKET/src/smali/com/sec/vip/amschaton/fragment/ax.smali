.class Lcom/sec/vip/amschaton/fragment/ax;
.super Landroid/os/Handler;
.source "AMSListFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V
    .locals 0

    .prologue
    .line 1427
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/ax;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1430
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1466
    :cond_0
    :goto_0
    return-void

    .line 1432
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ax;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    const/16 v3, 0x7d2

    if-ne v0, v3, :cond_0

    .line 1437
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v1, :cond_1

    .line 1438
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ax;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)Z

    .line 1444
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ax;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1446
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ax;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1447
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ax;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)Landroid/widget/CheckedTextView;

    move-result-object v3

    iget v0, p1, Landroid/os/Message;->arg2:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 1448
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ax;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    iget v3, p1, Landroid/os/Message;->arg2:I

    if-ne v3, v1, :cond_3

    :goto_3
    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)Z

    goto :goto_0

    .line 1441
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ax;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)Z

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1447
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1448
    goto :goto_3

    .line 1454
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1455
    if-lez v0, :cond_0

    .line 1456
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ax;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    const/16 v1, 0x7d1

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 1430
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
