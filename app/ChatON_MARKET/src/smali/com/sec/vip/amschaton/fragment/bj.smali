.class Lcom/sec/vip/amschaton/fragment/bj;
.super Ljava/lang/Object;
.source "AMSListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V
    .locals 0

    .prologue
    .line 987
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/16 v0, 0x7d2

    const/4 v2, 0x0

    .line 990
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    const/16 v3, 0xfa0

    if-ne v1, v3, :cond_2

    .line 991
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)Lcom/sec/vip/amschaton/a/f;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1040
    :cond_0
    :goto_0
    return-void

    .line 994
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)Lcom/sec/vip/amschaton/a/f;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Lcom/sec/vip/amschaton/a/f;->b(J)Landroid/database/Cursor;

    move-result-object v0

    .line 995
    if-eqz v0, :cond_0

    .line 998
    const-string v1, "ams_path"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 999
    const-string v2, "ams_type"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1000
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1002
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v2, v1, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Ljava/lang/String;I)V

    goto :goto_0

    .line 1006
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 1012
    if-nez p3, :cond_3

    .line 1014
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)V

    .line 1015
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    const/4 v1, 0x0

    const/4 v5, 0x1

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Ljava/lang/String;ZZZZ)V

    goto :goto_0

    .line 1017
    :cond_3
    if-lez p3, :cond_4

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-ge p3, v1, :cond_4

    .line 1018
    add-int/lit8 v1, p3, -0x1

    .line 1019
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-virtual {v3}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 1034
    :goto_1
    if-nez v1, :cond_6

    .line 1035
    const-string v0, "amsFile is NULL!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1021
    :cond_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    if-le p3, v0, :cond_5

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    if-ge p3, v0, :cond_5

    .line 1024
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    sub-int v0, p3, v0

    add-int/lit8 v0, v0, -0x1

    .line 1025
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/q;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 1026
    const/16 v0, 0x7d1

    goto :goto_1

    .line 1027
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    add-int/2addr v0, v1

    if-le p3, v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    if-ge p3, v0, :cond_0

    .line 1028
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    sub-int v0, p3, v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 1029
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/q;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 1030
    const/16 v0, 0x7d0

    goto :goto_1

    .line 1039
    :cond_6
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/bj;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v2, v1, v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method
