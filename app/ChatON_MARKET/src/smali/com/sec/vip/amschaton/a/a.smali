.class public Lcom/sec/vip/amschaton/a/a;
.super Ljava/lang/Object;
.source "AMSDatabase.java"


# instance fields
.field protected final a:Landroid/content/Context;

.field protected b:Lcom/sec/vip/amschaton/a/b;

.field protected c:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/sec/vip/amschaton/a/a;->a:Landroid/content/Context;

    .line 34
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/sec/vip/amschaton/a/a;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/sec/vip/amschaton/a/b;

    iget-object v1, p0, Lcom/sec/vip/amschaton/a/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/vip/amschaton/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/a/a;->b:Lcom/sec/vip/amschaton/a/b;

    .line 38
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/a;->b:Lcom/sec/vip/amschaton/a/b;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/a/a;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 40
    return-object p0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/a;->b:Lcom/sec/vip/amschaton/a/b;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/a;->b:Lcom/sec/vip/amschaton/a/b;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/b;->close()V

    .line 46
    iput-object v1, p0, Lcom/sec/vip/amschaton/a/a;->b:Lcom/sec/vip/amschaton/a/b;

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/a;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 50
    iput-object v1, p0, Lcom/sec/vip/amschaton/a/a;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 52
    :cond_1
    return-void
.end method
