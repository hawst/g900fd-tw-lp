.class Lcom/sec/vip/amschaton/bv;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ZoomableImageView.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/ZoomableImageView;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 902
    iput-object p1, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 905
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-boolean v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    if-eqz v0, :cond_0

    .line 922
    :goto_0
    return v4

    .line 909
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    .line 910
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iput-boolean v4, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    .line 911
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->r:F

    .line 912
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->s:F

    .line 914
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    iget-object v1, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->E:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3fb999999999999aL    # 0.1

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 915
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->E:F

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    .line 919
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->u:F

    .line 920
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->b(Lcom/sec/vip/amschaton/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/vip/amschaton/ZoomableImageView;->c(Lcom/sec/vip/amschaton/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 921
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->b(Lcom/sec/vip/amschaton/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/vip/amschaton/ZoomableImageView;->c(Lcom/sec/vip/amschaton/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 917
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bv;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    goto :goto_1
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 932
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 927
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method
