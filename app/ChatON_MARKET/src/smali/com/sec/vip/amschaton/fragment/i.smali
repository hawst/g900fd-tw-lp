.class Lcom/sec/vip/amschaton/fragment/i;
.super Ljava/lang/Object;
.source "AMSBgSelectionFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 475
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_1

    .line 476
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0, p4, p5}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;J)J

    .line 477
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Landroid/widget/GridView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->openContextMenu(Landroid/view/View;)V

    .line 493
    :cond_0
    :goto_0
    return v2

    .line 481
    :cond_1
    if-eqz p3, :cond_0

    if-eq p3, v2, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    if-lt p3, v0, :cond_2

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    if-lt p3, v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    if-eq p3, v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    const/16 v1, 0xbb9

    if-ne v0, v1, :cond_0

    .line 491
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0, p3}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I

    .line 492
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/i;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Landroid/widget/GridView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->openContextMenu(Landroid/view/View;)V

    goto :goto_0
.end method
