.class public Lcom/sec/vip/amschaton/fragment/cw;
.super Landroid/widget/BaseAdapter;
.source "AMSStampSelectionFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

.field private b:[Z

.field private c:Landroid/os/Handler;

.field private d:Lcom/sec/vip/amschaton/fragment/ct;

.field private e:Lcom/sec/vip/amschaton/fragment/ct;

.field private f:Lcom/sec/vip/amschaton/fragment/ct;

.field private g:Lcom/sec/vip/amschaton/fragment/ct;

.field private final h:Landroid/view/View$OnClickListener;

.field private final i:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1576
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1565
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    .line 1566
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->c:Landroid/os/Handler;

    .line 1568
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->d:Lcom/sec/vip/amschaton/fragment/ct;

    .line 1569
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->e:Lcom/sec/vip/amschaton/fragment/ct;

    .line 1570
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->f:Lcom/sec/vip/amschaton/fragment/ct;

    .line 1571
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->g:Lcom/sec/vip/amschaton/fragment/ct;

    .line 1740
    new-instance v0, Lcom/sec/vip/amschaton/fragment/cx;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/cx;-><init>(Lcom/sec/vip/amschaton/fragment/cw;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->h:Landroid/view/View$OnClickListener;

    .line 1758
    new-instance v0, Lcom/sec/vip/amschaton/fragment/cy;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/cy;-><init>(Lcom/sec/vip/amschaton/fragment/cw;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->i:Landroid/view/View$OnClickListener;

    .line 1577
    invoke-static {p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v0

    invoke-static {p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {p1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    .line 1578
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/cw;->a(Z)V

    .line 1580
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ct;

    const/16 v1, 0x1388

    invoke-direct {v0, p1, p2, v1}, Lcom/sec/vip/amschaton/fragment/ct;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->d:Lcom/sec/vip/amschaton/fragment/ct;

    .line 1581
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ct;

    const/16 v1, 0x1389

    invoke-direct {v0, p1, p2, v1}, Lcom/sec/vip/amschaton/fragment/ct;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->e:Lcom/sec/vip/amschaton/fragment/ct;

    .line 1582
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ct;

    const/16 v1, 0x138a

    invoke-direct {v0, p1, p2, v1}, Lcom/sec/vip/amschaton/fragment/ct;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->f:Lcom/sec/vip/amschaton/fragment/ct;

    .line 1583
    new-instance v0, Lcom/sec/vip/amschaton/fragment/ct;

    const/16 v1, 0x138b

    invoke-direct {v0, p1, p2, v1}, Lcom/sec/vip/amschaton/fragment/ct;-><init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->g:Lcom/sec/vip/amschaton/fragment/ct;

    .line 1584
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/cw;)[Z
    .locals 1

    .prologue
    .line 1564
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    return-object v0
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/cw;)V
    .locals 0

    .prologue
    .line 1564
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/cw;->c()V

    return-void
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1771
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1788
    :goto_0
    return-void

    .line 1774
    :cond_0
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 1775
    const/16 v0, 0x3e8

    iput v0, v5, Landroid/os/Message;->what:I

    move v0, v1

    move v2, v3

    move v4, v1

    .line 1778
    :goto_1
    iget-object v6, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    array-length v6, v6

    if-ge v0, v6, :cond_2

    .line 1779
    iget-object v6, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    aget-boolean v6, v6, v0

    if-eqz v6, :cond_1

    move v4, v3

    .line 1778
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    .line 1782
    goto :goto_2

    .line 1785
    :cond_2
    if-eqz v4, :cond_3

    move v0, v3

    :goto_3
    iput v0, v5, Landroid/os/Message;->arg1:I

    .line 1786
    if-eqz v2, :cond_4

    :goto_4
    iput v3, v5, Landroid/os/Message;->arg2:I

    .line 1787
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->c:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1785
    goto :goto_3

    :cond_4
    move v3, v1

    .line 1786
    goto :goto_4
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 1712
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    if-nez v0, :cond_1

    .line 1721
    :cond_0
    :goto_0
    return-void

    .line 1715
    :cond_1
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 1718
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    aget-boolean v0, v0, p1

    .line 1719
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    aput-boolean v0, v1, p1

    .line 1720
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/cw;->c()V

    goto :goto_0

    .line 1719
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1688
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/cw;->c:Landroid/os/Handler;

    .line 1689
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 1702
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    if-nez v0, :cond_0

    .line 1709
    :goto_0
    return-void

    .line 1705
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1706
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    aput-boolean p1, v1, v0

    .line 1705
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1708
    :cond_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/cw;->c()V

    goto :goto_0
.end method

.method public a()[Z
    .locals 1

    .prologue
    .line 1695
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 1727
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1728
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/cw;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 1729
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1730
    const/16 v2, 0x3e9

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1731
    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 1732
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->c:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1734
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/cw;->notifyDataSetChanged()V

    .line 1735
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 1588
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v0

    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_0

    .line 1589
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1591
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1596
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 1601
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/16 v6, 0x7d2

    const/4 v3, 0x0

    .line 1607
    if-nez p2, :cond_3

    .line 1608
    new-instance v1, Lcom/sec/vip/amschaton/fragment/cz;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/sec/vip/amschaton/fragment/cz;-><init>(Lcom/sec/vip/amschaton/fragment/cw;Lcom/sec/vip/amschaton/fragment/ca;)V

    .line 1609
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1610
    const v4, 0x7f030016

    invoke-virtual {v0, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1611
    const v0, 0x7f07006e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v1, Lcom/sec/vip/amschaton/fragment/cz;->a:Landroid/widget/RelativeLayout;

    .line 1612
    const v0, 0x7f07006f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v1, Lcom/sec/vip/amschaton/fragment/cz;->b:Landroid/widget/RelativeLayout;

    .line 1613
    const v0, 0x7f070066

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v1, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    .line 1614
    const v0, 0x7f070070

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/vip/amschaton/fragment/cz;->d:Landroid/widget/ImageView;

    .line 1615
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1616
    iget-object v0, v1, Lcom/sec/vip/amschaton/fragment/cz;->d:Landroid/widget/ImageView;

    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1618
    :cond_0
    const v0, 0x7f070067

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/sec/vip/amschaton/fragment/cz;->e:Landroid/widget/CheckBox;

    .line 1619
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 1623
    :goto_0
    iget-object v1, v0, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1624
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    if-ne v1, v6, :cond_1

    .line 1625
    add-int/lit8 p1, p1, 0x1

    .line 1627
    :cond_1
    if-nez p1, :cond_5

    .line 1629
    iget-object v4, v0, Lcom/sec/vip/amschaton/fragment/cz;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    if-ne v1, v6, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1630
    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/cz;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1681
    :cond_2
    :goto_2
    return-object p2

    .line 1621
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/cz;

    goto :goto_0

    :cond_4
    move v1, v3

    .line 1629
    goto :goto_1

    .line 1632
    :cond_5
    iget-object v1, v0, Lcom/sec/vip/amschaton/fragment/cz;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1633
    iget-object v1, v0, Lcom/sec/vip/amschaton/fragment/cz;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1634
    add-int/lit8 v1, p1, -0x1

    .line 1635
    if-ltz v1, :cond_2

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    if-ge v1, v2, :cond_2

    .line 1636
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 1638
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->d:Lcom/sec/vip/amschaton/fragment/ct;

    iget-object v4, v0, Lcom/sec/vip/amschaton/fragment/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v2, v1, v4}, Lcom/sec/vip/amschaton/fragment/ct;->a(ILandroid/widget/ImageView;)V

    .line 1640
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    if-ne v2, v6, :cond_6

    .line 1641
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1642
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 1643
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1644
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->e:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setId(I)V

    .line 1645
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->e:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1646
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->e:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    aget-boolean v4, v4, v1

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1677
    :cond_6
    :goto_3
    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/cz;->b:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->r(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    if-ne v2, v1, :cond_7

    const/4 v3, 0x1

    :cond_7
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    goto :goto_2

    .line 1648
    :cond_8
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    if-lt v1, v2, :cond_9

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    if-ge v1, v2, :cond_9

    .line 1650
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->g:Lcom/sec/vip/amschaton/fragment/ct;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    sub-int v4, v1, v4

    iget-object v5, v0, Lcom/sec/vip/amschaton/fragment/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v2, v4, v5}, Lcom/sec/vip/amschaton/fragment/ct;->a(ILandroid/widget/ImageView;)V

    .line 1652
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    if-ne v2, v6, :cond_6

    .line 1653
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1654
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 1655
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1656
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->e:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setId(I)V

    .line 1657
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->e:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1658
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->e:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    aget-boolean v4, v4, v1

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_3

    .line 1660
    :cond_9
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    if-lt v1, v2, :cond_a

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    if-ge v1, v2, :cond_a

    .line 1662
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->e:Lcom/sec/vip/amschaton/fragment/ct;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    sub-int v4, v1, v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v5}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, v0, Lcom/sec/vip/amschaton/fragment/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v2, v4, v5}, Lcom/sec/vip/amschaton/fragment/ct;->a(ILandroid/widget/ImageView;)V

    .line 1664
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    if-ne v2, v6, :cond_6

    .line 1665
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1666
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 1667
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->c:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1668
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->e:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setId(I)V

    .line 1669
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->e:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1670
    iget-object v2, v0, Lcom/sec/vip/amschaton/fragment/cz;->e:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->b:[Z

    aget-boolean v4, v4, v1

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_3

    .line 1672
    :cond_a
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    if-lt v1, v2, :cond_6

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v2}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v2

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    add-int/2addr v2, v4

    if-ge v1, v2, :cond_6

    .line 1674
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/cw;->f:Lcom/sec/vip/amschaton/fragment/ct;

    iget-object v4, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v4}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v4

    sub-int v4, v1, v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v5}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/vip/amschaton/fragment/cw;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v5}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, v0, Lcom/sec/vip/amschaton/fragment/cz;->d:Landroid/widget/ImageView;

    invoke-virtual {v2, v4, v5}, Lcom/sec/vip/amschaton/fragment/ct;->a(ILandroid/widget/ImageView;)V

    goto/16 :goto_3
.end method
