.class public Lcom/sec/vip/amschaton/al;
.super Ljava/lang/Object;
.source "AMSStampManager.java"


# static fields
.field private static final e:Lcom/sec/vip/amschaton/al;

.field private static l:[I


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private f:Landroid/content/Context;

.field private g:I

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private n:I

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private p:I

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private r:I

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/sec/vip/amschaton/al;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/al;-><init>()V

    sput-object v0, Lcom/sec/vip/amschaton/al;->e:Lcom/sec/vip/amschaton/al;

    .line 85
    const/16 v0, 0x24

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/vip/amschaton/al;->l:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3
        0x6
        0x1d
        0x1e
        0x1f
        0x21
        0x22
        0x24
        0x28
        0x29
        0x37
        0x38
        0x39
        0x3d
        0x3f
        0x41
        0x4b
        0x4c
        0x4d
        0x4e
        0x51
        0x52
        0x54
        0x5a
        0x60
        0x64
        0x75
        0x9d
        0xa0
        0xa3
        0xad
        0xae
        0xbd
        0xbe
        0xc7
    .end array-data
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const-string v0, "stamp_tr_hp"

    iput-object v0, p0, Lcom/sec/vip/amschaton/al;->a:Ljava/lang/String;

    .line 61
    const-string v0, "flashcon_ani_hp"

    iput-object v0, p0, Lcom/sec/vip/amschaton/al;->b:Ljava/lang/String;

    .line 72
    iput-object v2, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    .line 74
    iput v1, p0, Lcom/sec/vip/amschaton/al;->g:I

    .line 76
    iput-object v2, p0, Lcom/sec/vip/amschaton/al;->h:Ljava/util/ArrayList;

    .line 79
    iput v1, p0, Lcom/sec/vip/amschaton/al;->i:I

    .line 81
    iput-object v2, p0, Lcom/sec/vip/amschaton/al;->j:Ljava/util/ArrayList;

    .line 82
    iput v1, p0, Lcom/sec/vip/amschaton/al;->k:I

    .line 89
    iput-object v2, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    .line 90
    iput v1, p0, Lcom/sec/vip/amschaton/al;->n:I

    .line 91
    iput-object v2, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    .line 92
    iput v1, p0, Lcom/sec/vip/amschaton/al;->p:I

    .line 95
    iput-object v2, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    .line 96
    iput v1, p0, Lcom/sec/vip/amschaton/al;->r:I

    .line 97
    iput v1, p0, Lcom/sec/vip/amschaton/al;->s:I

    .line 103
    return-void
.end method

.method public static a()Lcom/sec/vip/amschaton/al;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/sec/vip/amschaton/al;->e:Lcom/sec/vip/amschaton/al;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->d:Ljava/lang/String;

    return-object v0
.end method

.method private a([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 789
    new-instance v0, Lcom/sec/vip/amschaton/am;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/am;-><init>(Lcom/sec/vip/amschaton/al;)V

    invoke-static {p1, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 804
    return-void
.end method


# virtual methods
.method public a(II)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 766
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->j:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 767
    const/4 v0, 0x0

    .line 769
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->j:Ljava/util/ArrayList;

    mul-int/lit8 v1, p1, 0x8

    add-int/2addr v1, p2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public a(IZ)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 481
    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 494
    :cond_0
    :goto_0
    return-object v0

    .line 484
    :cond_1
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 487
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    invoke-virtual {p0, v0, p1}, Lcom/sec/vip/amschaton/al;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 488
    if-eqz p2, :cond_2

    .line 489
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 491
    :cond_2
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 492
    const/4 v2, 0x2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 493
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 494
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    sget-object v1, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-static {v0, v1, p1}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 470
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 471
    if-eqz p2, :cond_0

    .line 472
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 477
    :goto_0
    return-object v0

    .line 474
    :cond_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 475
    const/4 v2, 0x2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 476
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 477
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 441
    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 448
    :cond_0
    :goto_0
    return-object v0

    .line 444
    :cond_1
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 447
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 457
    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 465
    :cond_0
    :goto_0
    return-object v0

    .line 460
    :cond_1
    if-ltz p2, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 463
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 464
    sget-object v1, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-static {p1, v1, v0}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 465
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 452
    sget-object v0, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-static {p1, v0, p2}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 453
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 120
    if-nez p1, :cond_0

    .line 142
    :goto_0
    return-void

    .line 128
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/AMS/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/al;->c:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "userstamp/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/al;->d:Ljava/lang/String;

    .line 132
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 133
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 137
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/al;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 138
    const-string v0, "[loadDownloadStamp] false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->d:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/vip/amschaton/al;->a(Landroid/content/Context;Landroid/content/res/AssetManager;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Landroid/content/res/AssetManager;)V
    .locals 1

    .prologue
    .line 172
    .line 174
    :try_start_0
    const-string v0, "stamp_tr_hp"

    invoke-virtual {p2, v0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 175
    array-length v0, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-gtz v0, :cond_0

    .line 201
    :goto_0
    return-void

    .line 178
    :catch_0
    move-exception v0

    .line 179
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 185
    :cond_0
    sget-object v0, Lcom/sec/vip/amschaton/al;->l:[I

    array-length v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/al;->g:I

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/content/res/AssetManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    .line 157
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/al;->a(Landroid/content/Context;Landroid/content/res/AssetManager;)V

    .line 158
    invoke-virtual {p0, p3}, Lcom/sec/vip/amschaton/al;->a(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/al;->b()V

    .line 161
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 252
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 253
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 254
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 255
    const-string v0, "[loadUserStamp] There is no directory!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_0
    return-void

    .line 258
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 259
    if-eqz v1, :cond_0

    .line 263
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/al;->a([Ljava/lang/String;)V

    .line 264
    array-length v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/al;->i:I

    .line 266
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/al;->h:Ljava/util/ArrayList;

    .line 267
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 271
    iget v0, p0, Lcom/sec/vip/amschaton/al;->i:I

    if-lez v0, :cond_0

    .line 272
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/sec/vip/amschaton/al;->i:I

    if-ge v0, v2, :cond_0

    .line 273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 274
    iget-object v3, p0, Lcom/sec/vip/amschaton/al;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public b(I)I
    .locals 2

    .prologue
    const v0, 0x7f0201e9

    .line 518
    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 524
    :cond_0
    :goto_0
    return v0

    .line 521
    :cond_1
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 524
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public b(IZ)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 551
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/al;->e()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 552
    :cond_0
    const/4 v0, 0x0

    .line 575
    :cond_1
    :goto_0
    return-object v0

    .line 554
    :cond_2
    if-eqz p2, :cond_3

    .line 557
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 558
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "srcBmp: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    const/16 v0, 0x48

    .line 560
    invoke-static {v1, v0, v0, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 561
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "retBmp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    if-eqz v1, :cond_1

    .line 564
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 565
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 572
    :cond_3
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 573
    iput v3, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 574
    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 575
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v2, v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 284
    invoke-static {}, Lcom/sec/chaton/multimedia/emoticon/h;->a()Ljava/util/Map;

    move-result-object v1

    .line 286
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/al;->n:I

    .line 288
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    .line 289
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 290
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 291
    iget-object v3, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 294
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/al;->c()V

    .line 295
    return-void
.end method

.method public b(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 334
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 335
    if-nez v0, :cond_0

    move v0, v6

    .line 429
    :goto_0
    return v0

    .line 347
    :cond_0
    :try_start_0
    sget-object v1, Lcom/sec/chaton/e/ar;->c:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "install DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 348
    if-nez v0, :cond_2

    .line 374
    if-eqz v0, :cond_1

    .line 375
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v6

    .line 349
    goto :goto_0

    .line 352
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 353
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    .line 355
    :cond_3
    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 357
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .line 358
    :goto_1
    if-eqz v1, :cond_7

    .line 359
    const-string v1, "item_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 360
    sget-object v2, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-static {p1, v2, v1}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 361
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 362
    iget-object v2, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    goto :goto_1

    .line 366
    :catch_0
    move-exception v0

    move-object v0, v7

    .line 367
    :goto_2
    :try_start_2
    const-string v1, "SQLiteException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 374
    if-eqz v0, :cond_5

    .line 375
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_5
    move v0, v6

    .line 372
    goto :goto_0

    .line 374
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v7, :cond_6

    .line 375
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 374
    :cond_6
    throw v0

    :cond_7
    if-eqz v0, :cond_8

    .line 375
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 380
    :cond_8
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/al;->r:I

    .line 403
    sget-object v0, Lcom/sec/chaton/d/e;->c:Lcom/sec/chaton/d/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/al;->s:I

    .line 404
    iget v0, p0, Lcom/sec/vip/amschaton/al;->s:I

    if-gez v0, :cond_9

    .line 405
    iput v6, p0, Lcom/sec/vip/amschaton/al;->s:I

    .line 429
    :cond_9
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 374
    :catchall_1
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_3

    .line 366
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method public c(I)I
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 528
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 538
    :goto_0
    return v2

    .line 532
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 533
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_1

    :goto_2
    move v2, v1

    .line 538
    goto :goto_0

    .line 532
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public c(IZ)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 613
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/al;->f()I

    move-result v0

    if-lt p1, v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 634
    :cond_1
    :goto_0
    return-object v0

    .line 616
    :cond_2
    if-eqz p2, :cond_3

    .line 617
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 621
    if-eqz v2, :cond_4

    .line 622
    const/16 v0, 0x48

    .line 623
    invoke-static {v2, v0, v0, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 624
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 625
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 631
    :cond_3
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 632
    iput v3, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 633
    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 634
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v2, v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 298
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 299
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    .line 301
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    .line 302
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 303
    iput v2, p0, Lcom/sec/vip/amschaton/al;->p:I

    .line 304
    new-instance v3, Lcom/sec/vip/amschaton/a/c;

    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    invoke-direct {v3, v0}, Lcom/sec/vip/amschaton/a/c;-><init>(Landroid/content/Context;)V

    .line 305
    invoke-virtual {v3}, Lcom/sec/vip/amschaton/a/c;->b()Lcom/sec/vip/amschaton/a/c;

    .line 306
    invoke-virtual {v3}, Lcom/sec/vip/amschaton/a/c;->c()Landroid/database/Cursor;

    move-result-object v4

    .line 307
    if-eqz v4, :cond_4

    .line 308
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 309
    :goto_0
    if-eqz v0, :cond_3

    .line 310
    const-string v0, "ams_index"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move v1, v2

    .line 312
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 313
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v5, v0, :cond_1

    .line 314
    const/4 v0, 0x1

    .line 318
    :goto_2
    if-eqz v0, :cond_2

    .line 319
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    :goto_3
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    goto :goto_0

    .line 312
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 321
    :cond_2
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/vip/amschaton/a/c;->b(Ljava/lang/String;)Z

    goto :goto_3

    .line 325
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/al;->p:I

    .line 326
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 328
    :cond_4
    invoke-virtual {v3}, Lcom/sec/vip/amschaton/a/c;->a()V

    .line 330
    return-void

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public d()I
    .locals 1

    .prologue
    .line 433
    iget v0, p0, Lcom/sec/vip/amschaton/al;->r:I

    return v0
.end method

.method public d(I)I
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 589
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 599
    :goto_0
    return v2

    .line 593
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 594
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_1

    :goto_2
    move v2, v1

    .line 599
    goto :goto_0

    .line 593
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public d(IZ)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const v4, 0x7f020314

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 659
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/al;->h(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 660
    if-eqz p2, :cond_0

    .line 661
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 682
    :goto_0
    return-object v0

    .line 663
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 664
    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 665
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 666
    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v4, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 671
    :cond_1
    if-eqz p2, :cond_2

    .line 673
    :try_start_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "stamp_tr_hp/stamp_%03d.png"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    add-int/lit8 v6, p1, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 675
    :cond_2
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 676
    const/4 v2, 0x2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 677
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 678
    iget-object v2, p0, Lcom/sec/vip/amschaton/al;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "stamp_tr_hp/stamp_%03d.png"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    add-int/lit8 v7, p1, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 680
    :catch_0
    move-exception v1

    .line 681
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 585
    iget v0, p0, Lcom/sec/vip/amschaton/al;->n:I

    return v0
.end method

.method public e(I)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 603
    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 609
    :cond_0
    :goto_0
    return v0

    .line 606
    :cond_1
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 609
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public e(IZ)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 709
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/al;->h()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 710
    :cond_0
    const/4 v0, 0x0

    .line 722
    :goto_0
    return-object v0

    .line 716
    :cond_1
    if-eqz p2, :cond_2

    .line 717
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 719
    :cond_2
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 720
    const/4 v0, 0x2

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 721
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 722
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 639
    iget v0, p0, Lcom/sec/vip/amschaton/al;->p:I

    return v0
.end method

.method public f(I)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 747
    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->h:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 753
    :cond_0
    :goto_0
    return-object v0

    .line 750
    :cond_1
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/al;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 753
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 693
    iget v0, p0, Lcom/sec/vip/amschaton/al;->g:I

    return v0
.end method

.method public g(I)I
    .locals 1

    .prologue
    .line 861
    if-ltz p1, :cond_0

    sget-object v0, Lcom/sec/vip/amschaton/al;->l:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 862
    :cond_0
    const/4 v0, -0x1

    .line 864
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/sec/vip/amschaton/al;->l:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 732
    iget v0, p0, Lcom/sec/vip/amschaton/al;->i:I

    return v0
.end method

.method public h(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 868
    sget-object v2, Lcom/sec/vip/amschaton/al;->l:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 869
    if-ne p1, v4, :cond_1

    .line 870
    const/4 v0, 0x1

    .line 873
    :cond_0
    return v0

    .line 868
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 778
    iget v0, p0, Lcom/sec/vip/amschaton/al;->k:I

    return v0
.end method

.method public i(I)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 877
    .line 878
    sget-object v3, Lcom/sec/vip/amschaton/al;->l:[I

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget v2, v3, v1

    .line 879
    if-ne p1, v2, :cond_0

    .line 884
    :goto_1
    return v0

    .line 882
    :cond_0
    add-int/lit8 v2, v0, 0x1

    .line 878
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 884
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lcom/sec/vip/amschaton/al;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 852
    const/4 v0, 0x1

    .line 857
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 888
    const/16 v0, 0xd3

    return v0
.end method
