.class Lcom/sec/vip/amschaton/fragment/x;
.super Landroid/os/AsyncTask;
.source "AMSBgSelectionFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;


# direct methods
.method private constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 0

    .prologue
    .line 775
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/x;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Lcom/sec/vip/amschaton/fragment/a;)V
    .locals 0

    .prologue
    .line 775
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/x;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .prologue
    .line 785
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/x;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 786
    const-string v0, "AMS file loading has failed!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 796
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/x;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v0

    const/16 v1, 0xbba

    if-eq v0, v1, :cond_0

    .line 798
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/x;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/x;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)V

    .line 802
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/x;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Z)Z

    .line 804
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/x;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    .line 805
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 806
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 775
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/fragment/x;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 775
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/fragment/x;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 778
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/x;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    .line 779
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 780
    return-void
.end method
