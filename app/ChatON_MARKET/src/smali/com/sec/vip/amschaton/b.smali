.class public Lcom/sec/vip/amschaton/b;
.super Ljava/lang/Object;
.source "AMSBackgroundManager.java"


# static fields
.field private static final c:Lcom/sec/vip/amschaton/b;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/vip/amschaton/c;",
            ">;"
        }
    .end annotation
.end field

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/sec/vip/amschaton/b;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/b;-><init>()V

    sput-object v0, Lcom/sec/vip/amschaton/b;->c:Lcom/sec/vip/amschaton/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-string v0, "bg_org_filename_list.txt"

    iput-object v0, p0, Lcom/sec/vip/amschaton/b;->b:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/b;->d:Ljava/util/ArrayList;

    .line 54
    iput v1, p0, Lcom/sec/vip/amschaton/b;->e:I

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    .line 57
    iput v1, p0, Lcom/sec/vip/amschaton/b;->g:I

    .line 58
    iput v1, p0, Lcom/sec/vip/amschaton/b;->h:I

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/b;->i:Ljava/util/ArrayList;

    .line 61
    iput v1, p0, Lcom/sec/vip/amschaton/b;->j:I

    .line 74
    return-void
.end method

.method public static a()Lcom/sec/vip/amschaton/b;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/sec/vip/amschaton/b;->c:Lcom/sec/vip/amschaton/b;

    return-object v0
.end method

.method private a(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 585
    .line 588
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 589
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 590
    :try_start_2
    invoke-direct {p0, v3, v1}, Lcom/sec/vip/amschaton/b;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 594
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/b;->a(Ljava/io/InputStream;)V

    .line 595
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/b;->a(Ljava/io/OutputStream;)V

    .line 596
    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/b;->b(Ljava/io/OutputStream;)V

    .line 598
    return-void

    .line 591
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 592
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 594
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/b;->a(Ljava/io/InputStream;)V

    .line 595
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/b;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 594
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/b;->a(Ljava/io/InputStream;)V

    .line 595
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/b;->a(Ljava/io/OutputStream;)V

    .line 596
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/b;->b(Ljava/io/OutputStream;)V

    .line 594
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_2

    :catchall_3
    move-exception v0

    goto :goto_2

    .line 591
    :catch_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v2, v3

    goto :goto_1
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 640
    if-eqz p1, :cond_0

    .line 641
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    :cond_0
    :goto_0
    return-void

    .line 643
    :catch_0
    move-exception v0

    .line 644
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 657
    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 660
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 661
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 666
    return-void

    .line 664
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method private a(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 608
    if-eqz p1, :cond_0

    .line 609
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 614
    :cond_0
    :goto_0
    return-void

    .line 611
    :catch_0
    move-exception v0

    .line 612
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Landroid/content/res/AssetManager;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 521
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 522
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 524
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 525
    if-nez v3, :cond_1

    .line 551
    :cond_0
    :goto_0
    return v1

    .line 531
    :cond_1
    :try_start_0
    const-string v0, "bg_org_hp_list"

    invoke-virtual {p1, v0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 532
    array-length v0, v4

    if-lez v0, :cond_0

    array-length v0, v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-gt v0, v2, :cond_0

    move v0, v1

    .line 541
    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_4

    .line 542
    aget-object v5, v4, v1

    aget-object v6, v3, v0

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v2

    .line 547
    :goto_2
    if-nez v0, :cond_2

    .line 548
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bg_org_hp_list/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v3, v4, v1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v1, v4, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v1, v2

    .line 551
    goto :goto_0

    .line 535
    :catch_0
    move-exception v0

    .line 536
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 541
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private b(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 624
    if-eqz p1, :cond_0

    .line 625
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 630
    :cond_0
    :goto_0
    return-void

    .line 627
    :catch_0
    move-exception v0

    .line 628
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;IZ)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 446
    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 459
    :cond_0
    :goto_0
    return-object v0

    .line 449
    :cond_1
    if-ltz p2, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 452
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 453
    if-eqz p3, :cond_2

    .line 454
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 456
    :cond_2
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 457
    const/4 v2, 0x2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 458
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 459
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 434
    sget-object v0, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-static {p1, v0, p2}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 435
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 436
    if-eqz p3, :cond_0

    .line 437
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 442
    :goto_0
    return-object v0

    .line 439
    :cond_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 440
    const/4 v2, 0x2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 441
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 442
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/res/AssetManager;IZ)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 492
    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 509
    :goto_0
    return-object v0

    .line 497
    :cond_1
    if-eqz p3, :cond_2

    .line 498
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bg_org_hp/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/vip/amschaton/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 500
    :cond_2
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 501
    const/4 v0, 0x2

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 502
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bg_org_hp/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/vip/amschaton/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 505
    :catch_0
    move-exception v0

    .line 506
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 507
    goto :goto_0
.end method

.method public a(I)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 406
    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-object v0

    .line 409
    :cond_1
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 412
    iget-object v0, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 422
    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 430
    :cond_0
    :goto_0
    return-object v0

    .line 425
    :cond_1
    if-ltz p2, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 428
    iget-object v0, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 429
    sget-object v1, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-static {p1, v1, v0}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 430
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 417
    sget-object v0, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-static {p1, v0, p2}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 418
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 90
    if-nez p1, :cond_0

    move v0, v1

    .line 118
    :goto_0
    return v0

    .line 98
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/AMS/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/b;->a:Ljava/lang/String;

    .line 101
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/vip/amschaton/b;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 105
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/b;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 106
    const-string v0, "[loadDownloadBackground] false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/b;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 111
    const-string v0, "[loadChatSkinBackgrond] false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/res/AssetManager;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 115
    goto :goto_0

    .line 98
    :cond_3
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 118
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Landroid/content/res/AssetManager;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 132
    if-nez p1, :cond_0

    .line 145
    :goto_0
    return v0

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v1}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/res/AssetManager;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 137
    const-string v1, "[copyAssetBgListToLocalFolder] false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/b;->b(Landroid/content/res/AssetManager;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 141
    const-string v1, "[loadBackgroundInList] false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 145
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lcom/sec/vip/amschaton/b;->g:I

    return v0
.end method

.method public b(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 176
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 177
    if-nez v0, :cond_0

    move v0, v6

    .line 253
    :goto_0
    return v0

    .line 183
    :cond_0
    :try_start_0
    sget-object v1, Lcom/sec/chaton/e/ar;->f:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "install"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "install DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 193
    if-eqz v0, :cond_1

    .line 194
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v7

    .line 198
    :cond_1
    if-nez v0, :cond_4

    move v0, v6

    .line 199
    goto :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 185
    :try_start_1
    const-string v0, "SQLiteException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 193
    if-eqz v7, :cond_2

    .line 194
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v6

    .line 191
    goto :goto_0

    .line 193
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 194
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 193
    :cond_3
    throw v0

    .line 202
    :cond_4
    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->i:Ljava/util/ArrayList;

    if-nez v1, :cond_5

    .line 203
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/vip/amschaton/b;->i:Ljava/util/ArrayList;

    .line 205
    :cond_5
    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 207
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .line 208
    :goto_1
    if-eqz v1, :cond_9

    .line 209
    const-string v1, "item_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 210
    invoke-static {p1, v1}, Lcom/sec/chaton/settings/downloads/cd;->e(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 211
    if-nez v2, :cond_6

    .line 212
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    goto :goto_1

    .line 215
    :cond_6
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 216
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/common/util/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 217
    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 218
    const/4 v4, 0x3

    aget-object v3, v3, v4

    const-string v4, "ma"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 219
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    goto :goto_1

    .line 222
    :cond_7
    iget-object v3, p0, Lcom/sec/vip/amschaton/b;->i:Ljava/util/ArrayList;

    new-instance v4, Lcom/sec/vip/amschaton/c;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, p0, v1, v2}, Lcom/sec/vip/amschaton/c;-><init>(Lcom/sec/vip/amschaton/b;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    :cond_8
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    goto :goto_1

    .line 226
    :cond_9
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 229
    iget-object v0, p0, Lcom/sec/vip/amschaton/b;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/b;->j:I

    .line 253
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public b(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 463
    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    :cond_0
    move v0, v1

    .line 472
    :goto_0
    return v0

    .line 466
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 468
    :try_start_0
    sget-object v2, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-static {p1, v2, v0}, Lcom/sec/chaton/settings/downloads/q;->c(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 472
    const/4 v0, 0x1

    goto :goto_0

    .line 469
    :catch_0
    move-exception v0

    move v0, v1

    .line 470
    goto :goto_0
.end method

.method public b(Landroid/content/res/AssetManager;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 149
    .line 151
    :try_start_0
    const-string v1, "bg_org_hp"

    invoke-virtual {p1, v1}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 152
    array-length v2, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-gtz v2, :cond_0

    .line 171
    :goto_0
    return v0

    .line 155
    :catch_0
    move-exception v1

    .line 156
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 160
    :cond_0
    iget-object v2, p0, Lcom/sec/vip/amschaton/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 161
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 166
    iget-object v2, p0, Lcom/sec/vip/amschaton/b;->d:Ljava/util/ArrayList;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/b;->e:I

    .line 171
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 479
    iget v0, p0, Lcom/sec/vip/amschaton/b;->e:I

    return v0
.end method

.method public c(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 297
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 298
    if-nez v0, :cond_0

    move v0, v6

    .line 394
    :goto_0
    return v0

    .line 310
    :cond_0
    :try_start_0
    sget-object v1, Lcom/sec/chaton/e/ar;->b:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "install DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 311
    if-nez v0, :cond_2

    .line 339
    if-eqz v0, :cond_1

    .line 340
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v6

    .line 312
    goto :goto_0

    .line 315
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 316
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    .line 318
    :cond_3
    iget-object v1, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 320
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .line 321
    :goto_1
    if-eqz v1, :cond_7

    .line 322
    const-string v1, "item_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 323
    sget-object v2, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-static {p1, v2, v1}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 324
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 325
    iget-object v2, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    goto :goto_1

    .line 329
    :catch_0
    move-exception v0

    move-object v0, v7

    .line 330
    :goto_2
    :try_start_2
    const-string v1, "SQLiteException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 339
    if-eqz v7, :cond_5

    .line 340
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    move v0, v6

    .line 337
    goto :goto_0

    .line 339
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v7, :cond_6

    .line 340
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 339
    :cond_6
    throw v0

    :cond_7
    if-eqz v0, :cond_8

    .line 340
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 345
    :cond_8
    iget-object v0, p0, Lcom/sec/vip/amschaton/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/b;->g:I

    .line 368
    sget-object v0, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/b;->h:I

    .line 369
    iget v0, p0, Lcom/sec/vip/amschaton/b;->h:I

    if-gez v0, :cond_9

    .line 370
    iput v6, p0, Lcom/sec/vip/amschaton/b;->h:I

    .line 394
    :cond_9
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 339
    :catchall_1
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_3

    .line 329
    :catch_1
    move-exception v1

    goto :goto_2
.end method
