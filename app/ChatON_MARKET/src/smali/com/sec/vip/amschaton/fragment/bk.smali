.class public Lcom/sec/vip/amschaton/fragment/bk;
.super Ljava/lang/Object;
.source "AMSListFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V
    .locals 1

    .prologue
    .line 1517
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/bk;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1518
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/bk;->b:Ljava/util/Map;

    .line 1519
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 1522
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bk;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1523
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bk;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1533
    :cond_0
    :goto_0
    return-object v0

    .line 1526
    :cond_1
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1527
    const/4 v1, 0x2

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1528
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 1529
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1530
    if-eqz v0, :cond_0

    .line 1531
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bk;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 1537
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bk;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1538
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bk;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1541
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/fragment/bl;

    invoke-direct {v0, p0, p2}, Lcom/sec/vip/amschaton/fragment/bl;-><init>(Lcom/sec/vip/amschaton/fragment/bk;Landroid/widget/ImageView;)V

    .line 1548
    new-instance v1, Lcom/sec/vip/amschaton/fragment/bm;

    invoke-direct {v1, p0, p1, v0}, Lcom/sec/vip/amschaton/fragment/bm;-><init>(Lcom/sec/vip/amschaton/fragment/bk;Ljava/lang/String;Landroid/os/Handler;)V

    .line 1556
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1557
    return-void
.end method
