.class public Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;
.super Lcom/sec/vip/amschaton/fragment/AMSFragment;
.source "AMSBgSelectionFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/vip/amschaton/bi;


# instance fields
.field private A:Landroid/widget/CheckedTextView;

.field private B:Landroid/view/View;

.field private C:Z

.field private final D:Landroid/view/View$OnClickListener;

.field private E:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private F:Landroid/widget/AdapterView$OnItemClickListener;

.field private G:Landroid/os/Handler;

.field a:Lcom/sec/vip/amschaton/d;

.field private b:Landroid/widget/GridView;

.field private c:Landroid/widget/LinearLayout;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Lcom/sec/vip/amschaton/fragment/y;

.field private p:Lcom/sec/vip/amschaton/a/d;

.field private q:Lcom/sec/vip/amschaton/fragment/t;

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:Z

.field private x:Lcom/sec/chaton/widget/m;

.field private y:I

.field private z:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;-><init>()V

    .line 62
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    .line 63
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c:Landroid/widget/LinearLayout;

    .line 64
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->m:Landroid/view/View;

    .line 65
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n:Landroid/view/View;

    .line 67
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    .line 68
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    .line 70
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    .line 71
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->r:I

    .line 72
    iput v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->s:I

    .line 73
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->t:I

    .line 74
    const/16 v0, 0x1389

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    .line 99
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->v:I

    .line 100
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->w:Z

    .line 101
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->x:Lcom/sec/chaton/widget/m;

    .line 103
    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    .line 104
    iput v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->y:I

    .line 105
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->z:J

    .line 110
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->C:Z

    .line 401
    new-instance v0, Lcom/sec/vip/amschaton/fragment/h;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/h;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->D:Landroid/view/View$OnClickListener;

    .line 472
    new-instance v0, Lcom/sec/vip/amschaton/fragment/i;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/i;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->E:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 497
    new-instance v0, Lcom/sec/vip/amschaton/fragment/j;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/j;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->F:Landroid/widget/AdapterView$OnItemClickListener;

    .line 668
    new-instance v0, Lcom/sec/vip/amschaton/fragment/e;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/e;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->G:Landroid/os/Handler;

    .line 1436
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;J)J
    .locals 0

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->z:J

    return-wide p1
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 633
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fe

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b03f6

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(I)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fc

    new-instance v2, Lcom/sec/vip/amschaton/fragment/d;

    invoke-direct {v2, p0, p2, p1}, Lcom/sec/vip/amschaton/fragment/d;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;II)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fd

    new-instance v2, Lcom/sec/vip/amschaton/fragment/c;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/c;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 662
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 419
    if-nez p1, :cond_0

    .line 420
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/av;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/fragment/av;->c()V

    .line 430
    :goto_0
    return-void

    .line 423
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 424
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 425
    const-string v1, "AMS_FRAME"

    const/16 v2, 0x3e9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 426
    const-string v1, "BACKGROUND_INDEX"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->s:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 428
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 429
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/av;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/fragment/av;->c()V

    goto :goto_0
.end method

.method private a(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    .line 825
    sget-object v0, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v0

    .line 826
    invoke-static {p1}, Lcom/sec/chaton/util/cp;->b(Landroid/view/MenuItem;)V

    .line 828
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p1, v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;Z)V

    .line 839
    return-void

    .line 828
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, 0x42dc0000    # 110.0f

    const v2, 0x7f07014b

    .line 187
    const v0, 0x7f07026b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    .line 188
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v3}, Lcom/sec/chaton/util/an;->b(F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v3}, Lcom/sec/chaton/util/an;->b(F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 190
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 191
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    const v1, 0x7f07014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b0132

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 193
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 194
    new-instance v1, Lcom/sec/vip/amschaton/fragment/a;

    invoke-direct {v1, p0, v0}, Lcom/sec/vip/amschaton/fragment/a;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setEmptyView(Landroid/view/View;)V

    .line 202
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 203
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p()V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Z)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 0

    .prologue
    .line 433
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b(Z)V

    .line 434
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g()V

    .line 435
    return-void
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I
    .locals 0

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->y:I

    return p1
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->A:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method private b(I)V
    .locals 4

    .prologue
    const v3, 0x7f07014d

    const v2, 0x7f07014c

    const v1, 0x7f07014b

    .line 167
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    if-nez v0, :cond_0

    .line 184
    :goto_0
    return-void

    .line 170
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 178
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020352

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b03f4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 180
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 172
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02034a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 173
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b0132

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0x1388
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Z)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 439
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_1

    .line 440
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/fragment/y;->a(Z)V

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 447
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/fragment/t;->a(Z)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    return v0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I
    .locals 0

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->r:I

    return p1
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 608
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/amschaton/AMSImageEditorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 609
    const-string v1, "IMAGE_SELECTOR"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 610
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 611
    return-void
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    if-nez v0, :cond_1

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 458
    if-eqz p1, :cond_0

    .line 459
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->F:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->C:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)I
    .locals 0

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->s:I

    return p1
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    return-object v0
.end method

.method private d(I)V
    .locals 0

    .prologue
    .line 665
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->v:I

    .line 666
    return-void
.end method

.method private d(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 464
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->A:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 470
    :cond_0
    :goto_0
    return-void

    .line 467
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->A:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 468
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->A:Landroid/widget/CheckedTextView;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 469
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 468
    goto :goto_1

    :cond_3
    move v2, v1

    .line 469
    goto :goto_2
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->w:Z

    return p1
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n()I

    move-result v0

    return v0
.end method

.method private e()V
    .locals 6

    .prologue
    const v2, 0x7f0d0013

    const v5, 0x7f0b012b

    const v4, 0x7f0b00fd

    const/4 v3, 0x1

    .line 559
    invoke-static {}, Lcom/sec/chaton/util/am;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 561
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/vip/amschaton/fragment/l;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/l;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    invoke-virtual {v0, v2, v1}, Lcom/sec/common/a/a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/vip/amschaton/fragment/k;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/k;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    invoke-virtual {v0, v4, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 605
    :cond_0
    :goto_0
    return-void

    .line 583
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    .line 584
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 587
    new-array v1, v3, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aget-object v0, v0, v3

    aput-object v0, v1, v2

    .line 588
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v2, Lcom/sec/vip/amschaton/fragment/b;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/b;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    new-instance v1, Lcom/sec/vip/amschaton/fragment/m;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/m;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    invoke-virtual {v0, v4, v1}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private e(I)V
    .locals 3

    .prologue
    .line 721
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 722
    const-string v1, "BACKGROUND_INDEX"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->s:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 723
    const-string v1, "AMS_FRAME"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 725
    packed-switch p1, :pswitch_data_0

    .line 740
    :goto_0
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 741
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 742
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 743
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/av;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/fragment/av;->c()V

    .line 744
    return-void

    .line 729
    :pswitch_1
    const-string v1, "BG_SKIN_INDEX"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->s:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 730
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->s:I

    add-int/lit16 v1, v1, 0x2710

    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(I)V

    goto :goto_0

    .line 733
    :pswitch_2
    const-string v1, "BG_SKIN_INDEX"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->s:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 734
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->s:I

    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f(I)V

    goto :goto_0

    .line 737
    :pswitch_3
    const-string v1, "BACKGROUND_COLOR"

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->t:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 725
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(I)V

    return-void
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o()I

    move-result v0

    return v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 614
    const/4 v1, 0x0

    .line 615
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->t:I

    .line 616
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    if-eqz v2, :cond_0

    .line 617
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/d;->e()Z

    move-result v1

    .line 618
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/d;->f()I

    move-result v0

    .line 619
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/d;->d()V

    .line 620
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    .line 622
    :cond_0
    new-instance v2, Lcom/sec/vip/amschaton/d;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/vip/amschaton/d;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    .line 623
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->t:I

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/d;->a(I)V

    .line 624
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    invoke-virtual {v2, p0}, Lcom/sec/vip/amschaton/d;->a(Lcom/sec/vip/amschaton/bi;)V

    .line 625
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/d;->c()V

    .line 626
    if-eqz v1, :cond_1

    .line 627
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/d;->b(I)V

    .line 629
    :cond_1
    return-void
.end method

.method private f(I)V
    .locals 5

    .prologue
    .line 747
    if-gez p1, :cond_0

    .line 755
    :goto_0
    return-void

    .line 750
    :cond_0
    const-string v0, ""

    .line 751
    const/16 v1, 0x2710

    if-lt p1, v1, :cond_1

    .line 752
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v0

    add-int/lit16 v1, p1, -0x2710

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/b;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 754
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/vip/amschaton/a/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(I)V

    return-void
.end method

.method static synthetic g(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->v:I

    return v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 705
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_1

    .line 706
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    if-eqz v0, :cond_0

    .line 707
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/y;->notifyDataSetChanged()V

    .line 717
    :cond_0
    :goto_0
    return-void

    .line 713
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    if-eqz v0, :cond_0

    .line 716
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/t;->b()V

    goto :goto_0
.end method

.method private g(I)V
    .locals 2

    .prologue
    .line 766
    if-gez p1, :cond_0

    .line 770
    :goto_0
    return-void

    .line 769
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/a/d;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(I)V

    return-void
.end method

.method static synthetic h(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)Lcom/sec/vip/amschaton/a/d;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 758
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->w:Z

    if-eqz v0, :cond_0

    .line 763
    :goto_0
    return-void

    .line 761
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->w:Z

    .line 762
    new-instance v0, Lcom/sec/vip/amschaton/fragment/x;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/x;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Lcom/sec/vip/amschaton/fragment/a;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/x;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private h(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 842
    iput p1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    .line 843
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    packed-switch v0, :pswitch_data_0

    .line 857
    :goto_0
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b(I)V

    .line 858
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->j()V

    .line 859
    return-void

    .line 845
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 846
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 847
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->k()V

    goto :goto_0

    .line 850
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 851
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 852
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->i()V

    goto :goto_0

    .line 843
    :pswitch_data_0
    .packed-switch 0x1388
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic h(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->h(I)V

    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 862
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    if-eqz v0, :cond_0

    .line 863
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    .line 865
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/fragment/t;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/vip/amschaton/fragment/t;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    .line 866
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->G:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/t;->a(Landroid/os/Handler;)V

    .line 867
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 868
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g()V

    .line 869
    return-void
.end method

.method static synthetic i(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g()V

    return-void
.end method

.method static synthetic j(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->r:I

    return v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 872
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->v:I

    const/16 v1, 0xbba

    if-ne v0, v1, :cond_1

    .line 894
    :cond_0
    :goto_0
    return-void

    .line 875
    :cond_1
    const/4 v0, 0x0

    .line 877
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    const/16 v2, 0x1388

    if-ne v1, v2, :cond_3

    .line 878
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    if-eqz v1, :cond_2

    .line 879
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/y;->getCount()I

    move-result v0

    .line 886
    :cond_2
    :goto_1
    if-lez v0, :cond_4

    .line 887
    const/16 v0, 0xbb9

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(I)V

    .line 891
    :goto_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 892
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 883
    :cond_3
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n()I

    move-result v0

    goto :goto_1

    .line 889
    :cond_4
    const/16 v0, 0xbb8

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(I)V

    goto :goto_2
.end method

.method private k()V
    .locals 7

    .prologue
    .line 898
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    if-nez v0, :cond_1

    .line 936
    :cond_0
    :goto_0
    return-void

    .line 902
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/d;->c()Landroid/database/Cursor;

    move-result-object v1

    .line 903
    if-eqz v1, :cond_0

    .line 907
    const/4 v2, 0x0

    .line 908
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    move v6, v0

    move v0, v2

    move v2, v6

    .line 909
    :goto_1
    if-eqz v2, :cond_3

    .line 910
    const-string v2, "ams_index"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 911
    const/16 v3, 0x2710

    if-lt v2, v3, :cond_2

    .line 912
    const-string v3, "ams_path"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 913
    if-eqz v3, :cond_2

    .line 914
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 915
    if-eqz v3, :cond_2

    .line 916
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 917
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 918
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(I)V

    .line 919
    const/4 v0, 0x1

    .line 924
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    goto :goto_1

    .line 926
    :cond_3
    if-eqz v0, :cond_5

    .line 927
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/d;->c()Landroid/database/Cursor;

    move-result-object v0

    .line 930
    :goto_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    if-eqz v1, :cond_4

    .line 931
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    .line 933
    :cond_4
    new-instance v1, Lcom/sec/vip/amschaton/fragment/y;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/vip/amschaton/fragment/y;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    .line 934
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 935
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->G:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/fragment/y;->a(Landroid/os/Handler;)V

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method static synthetic k(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e()V

    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->x:Lcom/sec/chaton/widget/m;

    if-eqz v0, :cond_0

    .line 1118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->x:Lcom/sec/chaton/widget/m;

    .line 1120
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/widget/m;->a(Landroid/content/Context;)Lcom/sec/chaton/widget/m;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->x:Lcom/sec/chaton/widget/m;

    .line 1121
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->x:Lcom/sec/chaton/widget/m;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/m;->setProgressStyle(I)V

    .line 1122
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->x:Lcom/sec/chaton/widget/m;

    const v1, 0x7f0b00ef

    invoke-virtual {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/chaton/widget/m;->setMessage(Ljava/lang/CharSequence;)V

    .line 1123
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->x:Lcom/sec/chaton/widget/m;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/m;->show()V

    .line 1124
    return-void
.end method

.method static synthetic l(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->f()V

    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 1127
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->x:Lcom/sec/chaton/widget/m;

    if-eqz v0, :cond_0

    .line 1128
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->x:Lcom/sec/chaton/widget/m;

    invoke-virtual {v0}, Lcom/sec/chaton/widget/m;->dismiss()V

    .line 1129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->x:Lcom/sec/chaton/widget/m;

    .line 1131
    :cond_0
    return-void
.end method

.method static synthetic m(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->h()V

    return-void
.end method

.method private n()I
    .locals 1

    .prologue
    .line 1134
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/b;->b()I

    move-result v0

    return v0
.end method

.method static synthetic n(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->l()V

    return-void
.end method

.method private o()I
    .locals 1

    .prologue
    .line 1138
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/b;->c()I

    move-result v0

    return v0
.end method

.method static synthetic o(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->m()V

    return-void
.end method

.method private p()V
    .locals 8

    .prologue
    const/16 v2, 0x3ea

    const/4 v0, 0x0

    .line 1507
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q()[Z

    move-result-object v4

    .line 1508
    if-nez v4, :cond_1

    .line 1589
    :cond_0
    :goto_0
    return-void

    .line 1511
    :cond_1
    array-length v5, v4

    .line 1513
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    const/16 v3, 0x1388

    if-ne v1, v3, :cond_a

    .line 1514
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    if-eqz v1, :cond_0

    .line 1517
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/a/d;->c()Landroid/database/Cursor;

    move-result-object v6

    .line 1518
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eq v1, v5, :cond_3

    .line 1519
    :cond_2
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1520
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    move v3, v0

    .line 1525
    :goto_1
    if-ge v3, v5, :cond_8

    .line 1526
    aget-boolean v0, v4, v3

    if-eqz v0, :cond_5

    .line 1528
    invoke-interface {v6, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1532
    const-string v0, "ams_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1533
    const/16 v1, 0x2710

    if-ge v0, v1, :cond_6

    .line 1534
    const/16 v1, 0x3eb

    .line 1539
    :goto_2
    if-ne v1, v2, :cond_4

    .line 1540
    add-int/lit16 v0, v0, -0x2710

    .line 1542
    :cond_4
    if-gez v0, :cond_7

    .line 1543
    const-string v0, "Wrong index"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1525
    :cond_5
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_6
    move v1, v2

    .line 1536
    goto :goto_2

    .line 1547
    :cond_7
    packed-switch v1, :pswitch_data_0

    goto :goto_3

    .line 1549
    :pswitch_0
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v1, v7, v0}, Lcom/sec/vip/amschaton/b;->b(Landroid/content/Context;I)Z

    .line 1550
    add-int/lit16 v0, v0, 0x2710

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(I)V

    goto :goto_3

    .line 1553
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(I)V

    goto :goto_3

    .line 1563
    :cond_8
    if-eqz v6, :cond_9

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1564
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1585
    :cond_9
    if-lez v5, :cond_0

    .line 1586
    const v0, 0x7f0b00f6

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a(I)V

    .line 1587
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->h()V

    goto/16 :goto_0

    .line 1571
    :cond_a
    :goto_4
    if-ge v0, v5, :cond_9

    .line 1572
    aget-boolean v1, v4, v0

    if-eqz v1, :cond_b

    .line 1573
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n()I

    move-result v1

    .line 1575
    if-ge v0, v1, :cond_b

    .line 1576
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/vip/amschaton/b;->b(Landroid/content/Context;I)Z

    .line 1577
    add-int/lit16 v1, v0, 0x2710

    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g(I)V

    .line 1571
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1547
    :pswitch_data_0
    .packed-switch 0x3ea
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private q()[Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1592
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    const/16 v2, 0x1388

    if-ne v1, v2, :cond_2

    .line 1593
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    if-nez v1, :cond_1

    .line 1605
    :cond_0
    :goto_0
    return-object v0

    .line 1597
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o:Lcom/sec/vip/amschaton/fragment/y;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/y;->a()[Z

    move-result-object v0

    goto :goto_0

    .line 1602
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    if-eqz v1, :cond_0

    .line 1605
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q:Lcom/sec/vip/amschaton/fragment/t;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/t;->a()[Z

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 1638
    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1628
    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->s:I

    .line 1629
    const/16 v0, 0x3ec

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->r:I

    .line 1630
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a:Lcom/sec/vip/amschaton/d;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/d;->b()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->t:I

    .line 1631
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g()V

    .line 1632
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->r:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->e(I)V

    .line 1633
    return v1
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1678
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->v:I

    const/16 v1, 0xbba

    if-ne v0, v1, :cond_0

    .line 1680
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Z)V

    .line 1682
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b(Z)V

    .line 1683
    const/16 v0, 0xbb9

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(I)V

    .line 1684
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->C:Z

    .line 1685
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g()V

    .line 1686
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Z)V

    .line 1687
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 1691
    :goto_0
    return-void

    .line 1689
    :cond_0
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->c()V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 316
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 324
    :goto_0
    return-void

    .line 319
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 321
    :pswitch_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 319
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1611
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1624
    :cond_0
    :goto_0
    return-void

    .line 1614
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1615
    const/16 v0, 0x1388

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->h(I)V

    goto :goto_0

    .line 1619
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1620
    const/16 v0, 0x1389

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->h(I)V

    goto :goto_0

    .line 1611
    nop

    :pswitch_data_0
    .packed-switch 0x7f070267
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/16 v1, 0x3eb

    const/16 v0, 0x3ea

    const/4 v2, 0x1

    .line 345
    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    const/16 v4, 0x1388

    if-ne v3, v4, :cond_4

    .line 346
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    if-nez v3, :cond_0

    move v0, v2

    .line 398
    :goto_0
    return v0

    .line 349
    :cond_0
    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    iget-wide v4, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->z:J

    invoke-virtual {v3, v4, v5}, Lcom/sec/vip/amschaton/a/d;->b(J)Landroid/database/Cursor;

    move-result-object v3

    .line 350
    if-nez v3, :cond_1

    move v0, v2

    .line 351
    goto :goto_0

    .line 354
    :cond_1
    const-string v4, "ams_index"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 355
    const/16 v4, 0x2710

    if-ge v3, v4, :cond_2

    .line 361
    :goto_1
    if-ne v1, v0, :cond_8

    .line 362
    add-int/lit16 v0, v3, -0x2710

    .line 364
    :goto_2
    if-gez v0, :cond_3

    .line 365
    const-string v0, "Wrong index"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 366
    goto :goto_0

    :cond_2
    move v1, v0

    .line 358
    goto :goto_1

    .line 368
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 376
    :goto_3
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 370
    :pswitch_0
    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a(II)V

    goto :goto_3

    .line 379
    :cond_4
    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->y:I

    add-int/lit8 v3, v2, -0x2

    .line 380
    const/16 v2, 0x3e8

    .line 381
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n()I

    move-result v4

    if-ge v3, v4, :cond_5

    .line 389
    :goto_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 398
    :goto_5
    :pswitch_1
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 383
    :cond_5
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o()I

    move-result v4

    add-int/2addr v0, v4

    if-ge v3, v0, :cond_6

    move v0, v1

    .line 384
    goto :goto_4

    .line 385
    :cond_6
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->o()I

    move-result v1

    add-int/2addr v0, v1

    if-ne v3, v0, :cond_7

    .line 386
    const/16 v0, 0x3ec

    goto :goto_4

    .line 395
    :pswitch_2
    invoke-direct {p0, v3, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a(II)V

    goto :goto_5

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    move v0, v3

    goto :goto_2

    .line 368
    nop

    :pswitch_data_0
    .packed-switch 0x7f070588
        :pswitch_0
    .end packed-switch

    .line 389
    :pswitch_data_1
    .packed-switch 0x7f070585
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 115
    new-instance v0, Lcom/sec/vip/amschaton/a/d;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/a/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    .line 116
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/d;->b()Lcom/sec/vip/amschaton/a/d;

    .line 118
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreate(Landroid/os/Bundle;)V

    .line 120
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 328
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    const v0, 0x7f0b012b

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 331
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 332
    const v1, 0x7f0f000c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 333
    const v0, 0x7f070586

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 334
    const v0, 0x7f070585

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 335
    const v0, 0x7f070587

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 336
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_0

    .line 337
    const v0, 0x7f070588

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 340
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 341
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5

    .prologue
    const v2, 0x7f0705a6

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1643
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->v:I

    const/16 v1, 0xbba

    if-ne v0, v1, :cond_1

    .line 1644
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b00fe

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 1645
    const v0, 0x7f0f0021

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1647
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->C:Z

    if-nez v0, :cond_0

    .line 1648
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1673
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1674
    return-void

    .line 1651
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 1655
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b012b

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 1656
    const v0, 0x7f0f000e

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1657
    const v0, 0x7f070589

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1659
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->v:I

    const/16 v2, 0xbb8

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->u:I

    const/16 v2, 0x1388

    if-ne v1, v2, :cond_3

    .line 1660
    :cond_2
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1661
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 1669
    :goto_1
    const v0, 0x7f07058a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1670
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a(Landroid/view/MenuItem;)V

    goto :goto_0

    .line 1664
    :cond_3
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1665
    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 124
    const v0, 0x7f03005d

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 126
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 128
    const-string v2, "AMS_FRAME"

    const/16 v3, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->r:I

    .line 129
    const-string v2, "BACKGROUND_INDEX"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->s:I

    .line 130
    const-string v2, "BACKGROUND_COLOR"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->t:I

    .line 133
    const v0, 0x7f070265

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->A:Landroid/widget/CheckedTextView;

    .line 135
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->A:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->A:Landroid/widget/CheckedTextView;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->D:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    const v0, 0x7f070269

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 139
    const v0, 0x7f07026c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 141
    const v0, 0x7f07026a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    .line 143
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 144
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->b:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->E:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 145
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Z)V

    .line 147
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->a(Landroid/view/View;)V

    .line 149
    const v0, 0x7f070266

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c:Landroid/widget/LinearLayout;

    .line 150
    const v0, 0x7f070267

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->m:Landroid/view/View;

    .line 151
    const v0, 0x7f070268

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n:Landroid/view/View;

    .line 152
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->m:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->n:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    const/16 v0, 0x1389

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->h(I)V

    .line 157
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 281
    const-string v0, "[onDestroy]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->p:Lcom/sec/vip/amschaton/a/d;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/d;->a()V

    .line 285
    :cond_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->m()V

    .line 286
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onDestroy()V

    .line 287
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 208
    .line 209
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 210
    const v2, 0x7f0705a5

    if-ne v1, v2, :cond_2

    .line 211
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c()V

    move v0, v3

    .line 271
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 272
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 275
    :cond_1
    return v0

    .line 213
    :cond_2
    const v2, 0x7f070589

    if-ne v1, v2, :cond_3

    .line 214
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Z)V

    .line 216
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(Z)V

    .line 217
    const/16 v0, 0xbba

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->d(I)V

    .line 218
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->g()V

    .line 219
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    move v0, v3

    .line 220
    goto :goto_0

    .line 221
    :cond_3
    const v2, 0x7f0705a6

    if-ne v1, v2, :cond_7

    .line 222
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->c(Z)V

    .line 225
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b03f6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 226
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->q()[Z

    move-result-object v4

    .line 227
    if-eqz v4, :cond_5

    move v1, v0

    .line 228
    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_6

    .line 229
    aget-boolean v5, v4, v0

    if-ne v5, v3, :cond_4

    .line 230
    add-int/lit8 v1, v1, 0x1

    .line 228
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v1, v0

    .line 234
    :cond_6
    if-le v1, v3, :cond_8

    .line 235
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b023d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 239
    :goto_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b00fe

    invoke-virtual {v1, v2}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fc

    new-instance v2, Lcom/sec/vip/amschaton/fragment/g;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/g;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b00fd

    new-instance v2, Lcom/sec/vip/amschaton/fragment/f;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/f;-><init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 263
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    move v0, v3

    .line 265
    goto/16 :goto_0

    :cond_7
    const v2, 0x7f07058a

    if-ne v1, v2, :cond_0

    .line 266
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/chaton/settings/downloads/ActivityAmsItemDownloads;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 267
    const-string v1, "amsType"

    sget-object v2, Lcom/sec/chaton/d/e;->b:Lcom/sec/chaton/d/e;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 268
    const/16 v1, 0xc9

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v3

    .line 269
    goto/16 :goto_0

    :cond_8
    move-object v0, v2

    goto :goto_2
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 291
    const-string v0, "[onPause]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onPause()V

    .line 294
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 298
    const-string v0, "[onResume]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->h()V

    .line 300
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onResume()V

    .line 302
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 162
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onStart()V

    .line 164
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b012b

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 165
    return-void
.end method
