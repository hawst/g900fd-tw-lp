.class public Lcom/sec/vip/amschaton/w;
.super Lcom/sec/vip/amschaton/u;
.source "AMSObjectDrawings.java"


# instance fields
.field private Z:Landroid/graphics/Paint;

.field private aa:F

.field private ab:F

.field private final ac:I

.field private ad:Z

.field private ae:Z


# direct methods
.method public constructor <init>(IIIZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 75
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/u;-><init>(III)V

    .line 56
    iput v0, p0, Lcom/sec/vip/amschaton/w;->aa:F

    .line 57
    iput v0, p0, Lcom/sec/vip/amschaton/w;->ab:F

    .line 59
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/vip/amschaton/w;->ac:I

    .line 60
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/w;->ad:Z

    .line 62
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/w;->ae:Z

    .line 76
    iput-boolean p4, p0, Lcom/sec/vip/amschaton/w;->ae:Z

    .line 77
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/w;->ae:Z

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    .line 79
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/w;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 80
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    int-to-float v1, p3

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 81
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 82
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 83
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 84
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 85
    packed-switch p1, :pswitch_data_0

    .line 99
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 90
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    goto :goto_0

    .line 96
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    goto :goto_0

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(F)V
    .locals 5

    .prologue
    .line 112
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 113
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/w;->c:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-double v1, v1

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    add-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 116
    :cond_0
    return-void
.end method

.method public a(FF)V
    .locals 0

    .prologue
    .line 212
    iput p1, p0, Lcom/sec/vip/amschaton/w;->E:F

    .line 213
    iput p2, p0, Lcom/sec/vip/amschaton/w;->F:F

    .line 214
    return-void
.end method

.method public a(FFFFI)Z
    .locals 7

    .prologue
    .line 260
    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/w;->a(FFFFIZ)Z

    move-result v0

    return v0
.end method

.method public a(FFFFIZ)Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 280
    if-eqz p6, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->i:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 283
    :cond_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/w;->ae:Z

    if-eqz v0, :cond_2

    .line 284
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 285
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 286
    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    invoke-virtual {v2, p5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 287
    iget v2, p0, Lcom/sec/vip/amschaton/w;->a:I

    packed-switch v2, :pswitch_data_0

    .line 314
    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 316
    :goto_0
    iget v0, p0, Lcom/sec/vip/amschaton/w;->c:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v0, v1

    div-float/2addr v0, v5

    sub-float v0, p1, v0

    iget v1, p0, Lcom/sec/vip/amschaton/w;->c:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v1, v2

    div-float/2addr v1, v5

    sub-float v1, p2, v1

    iget v2, p0, Lcom/sec/vip/amschaton/w;->c:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v2, v3

    div-float/2addr v2, v5

    add-float/2addr v2, p3

    iget v3, p0, Lcom/sec/vip/amschaton/w;->c:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float/2addr v3, p4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/w;->a(FFFF)V

    .line 325
    :cond_1
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 290
    :pswitch_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 294
    :pswitch_1
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    invoke-virtual {v1, v2, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 295
    invoke-virtual {v1, p1, p4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 296
    invoke-virtual {v1, p3, p4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 297
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    invoke-virtual {v1, v0, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 298
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 302
    :pswitch_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->g:Landroid/graphics/Canvas;

    sub-float v2, p3, p1

    div-float/2addr v2, v5

    sub-float v3, p4, p2

    div-float/2addr v3, v5

    iget-object v4, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 306
    :pswitch_3
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    invoke-virtual {v1, v2, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 307
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {v1, p1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 308
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    invoke-virtual {v1, v2, p4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 309
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {v1, p3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    invoke-virtual {v1, v0, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 311
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 319
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->M:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 320
    new-instance v0, Landroid/graphics/Rect;

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {p4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 321
    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 322
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->M:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 287
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public a(FFI)Z
    .locals 7

    .prologue
    .line 229
    iget v3, p0, Lcom/sec/vip/amschaton/w;->E:F

    .line 230
    iget v4, p0, Lcom/sec/vip/amschaton/w;->F:F

    .line 232
    iget v0, p0, Lcom/sec/vip/amschaton/w;->B:I

    packed-switch v0, :pswitch_data_0

    move v0, p2

    move v1, p1

    .line 245
    :goto_0
    cmpl-float v2, v3, v1

    if-lez v2, :cond_1

    .line 250
    :goto_1
    cmpl-float v2, v4, v0

    if-lez v2, :cond_0

    move v2, v0

    :goto_2
    move-object v0, p0

    move v5, p3

    .line 255
    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/w;->a(FFFFI)Z

    move-result v0

    return v0

    .line 235
    :pswitch_0
    iget p2, p0, Lcom/sec/vip/amschaton/w;->ab:F

    move v0, p2

    move v1, p1

    .line 236
    goto :goto_0

    .line 238
    :pswitch_1
    iget p1, p0, Lcom/sec/vip/amschaton/w;->aa:F

    move v0, p2

    move v1, p1

    .line 240
    goto :goto_0

    :cond_0
    move v2, v4

    move v4, v0

    goto :goto_2

    :cond_1
    move v6, v1

    move v1, v3

    move v3, v6

    goto :goto_1

    .line 232
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(III)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 149
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/u;->a(III)Z

    .line 150
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    move v0, v6

    .line 178
    :goto_0
    return v0

    .line 153
    :cond_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/w;->H:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/w;->z:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    iget-object v3, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget v4, p0, Lcom/sec/vip/amschaton/w;->a:I

    int-to-byte v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/amsoma/AMSLibs;->VipAMS_ChangeSelectDiagram(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;Lcom/sec/amsoma/structure/AMS_RECT;B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    const/4 v0, 0x0

    goto :goto_0

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, p1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_cStyle(I)V

    .line 158
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/w;->c:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v1, v2

    float-to-double v1, v1

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    add-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 160
    packed-switch p1, :pswitch_data_0

    .line 174
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 176
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v0

    const/16 v5, 0xff

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/w;->a(FFFFI)Z

    :cond_2
    move v0, v6

    .line 178
    goto :goto_0

    .line 165
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    goto :goto_1

    .line 171
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->Z:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    goto :goto_1

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/sec/amsoma/structure/AMS_RECT;)Z
    .locals 6

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->y:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/w;->z:I

    iget v2, p0, Lcom/sec/vip/amschaton/w;->a:I

    int-to-byte v2, v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_EncodeNewDiagram(ILcom/sec/amsoma/structure/AMS_RECT;B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "errocode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->y:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/w;->z:I

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/w;->a()V

    .line 130
    const/4 v0, 0x0

    .line 133
    :goto_0
    return v0

    .line 132
    :cond_0
    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v0

    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v0

    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v0

    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v0

    const/16 v5, 0xff

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/w;->a(FFFFI)Z

    .line 133
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lcom/sec/amsoma/structure/AMS_RECT;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 190
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/w;->H:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-nez v1, :cond_1

    .line 198
    :cond_0
    :goto_0
    return v0

    .line 193
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->y:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/w;->z:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/w;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    iget-object v4, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v4

    int-to-byte v4, v4

    invoke-virtual {v1, v2, v3, p1, v4}, Lcom/sec/amsoma/AMSLibs;->VipAMS_ChangeSelectDiagram(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;Lcom/sec/amsoma/structure/AMS_RECT;B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, p1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_TRect(Lcom/sec/amsoma/structure/AMS_RECT;)V

    .line 197
    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v0

    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v0

    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v0

    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v0

    const/16 v5, 0xff

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/w;->a(FFFFI)Z

    .line 198
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(FF)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/high16 v7, 0x41200000    # 10.0f

    .line 339
    iget v1, p0, Lcom/sec/vip/amschaton/w;->B:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 346
    :cond_0
    :goto_0
    return v0

    .line 342
    :cond_1
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v7

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v4, v7

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v5

    iget-short v5, v5, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 343
    invoke-virtual {v1, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 344
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public d(FF)Z
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v6, 0x1

    const/high16 v8, 0x41200000    # 10.0f

    .line 362
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    move v6, v1

    .line 469
    :goto_0
    return v6

    .line 366
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/w;->B:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 456
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/w;->G:Z

    if-nez v0, :cond_e

    .line 458
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/w;->G:Z

    goto :goto_0

    .line 368
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    if-nez v0, :cond_1

    .line 369
    const-string v0, "Why is selected object null???"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    iput v1, p0, Lcom/sec/vip/amschaton/w;->B:I

    goto :goto_0

    .line 374
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    if-eqz v0, :cond_a

    .line 375
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 376
    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 377
    iget-object v3, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 378
    iget-object v4, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 379
    int-to-float v5, v0

    sub-float/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_3

    int-to-float v5, v2

    sub-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_3

    .line 381
    int-to-float v0, v3

    iput v0, p0, Lcom/sec/vip/amschaton/w;->E:F

    .line 382
    int-to-float v0, v4

    iput v0, p0, Lcom/sec/vip/amschaton/w;->F:F

    .line 383
    iput v9, p0, Lcom/sec/vip/amschaton/w;->B:I

    move v0, v1

    .line 433
    :goto_1
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/w;->I:Z

    .line 434
    iget v2, p0, Lcom/sec/vip/amschaton/w;->B:I

    if-eq v2, v9, :cond_2

    iget v2, p0, Lcom/sec/vip/amschaton/w;->B:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/sec/vip/amschaton/w;->B:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_b

    .line 435
    :cond_2
    const/16 v0, 0x80

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/vip/amschaton/w;->a(FFI)Z

    goto :goto_0

    .line 384
    :cond_3
    int-to-float v5, v3

    sub-float/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_4

    int-to-float v5, v2

    sub-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_4

    .line 391
    const/16 v0, 0x65

    invoke-virtual {p0, v0, v6}, Lcom/sec/vip/amschaton/w;->b(IZ)V

    move v0, v6

    .line 392
    goto :goto_1

    .line 393
    :cond_4
    int-to-float v5, v3

    sub-float/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_5

    int-to-float v5, v4

    sub-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_5

    .line 395
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/w;->E:F

    .line 396
    int-to-float v0, v2

    iput v0, p0, Lcom/sec/vip/amschaton/w;->F:F

    .line 397
    iput v9, p0, Lcom/sec/vip/amschaton/w;->B:I

    move v0, v1

    goto :goto_1

    .line 398
    :cond_5
    int-to-float v5, v0

    sub-float/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_6

    int-to-float v5, v4

    sub-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_6

    .line 400
    int-to-float v0, v3

    iput v0, p0, Lcom/sec/vip/amschaton/w;->E:F

    .line 401
    int-to-float v0, v2

    iput v0, p0, Lcom/sec/vip/amschaton/w;->F:F

    .line 402
    iput v9, p0, Lcom/sec/vip/amschaton/w;->B:I

    move v0, v1

    goto :goto_1

    .line 403
    :cond_6
    add-int v5, v0, v3

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_7

    int-to-float v5, v2

    sub-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_7

    .line 405
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/w;->E:F

    .line 406
    int-to-float v0, v4

    iput v0, p0, Lcom/sec/vip/amschaton/w;->F:F

    .line 407
    int-to-float v0, v3

    iput v0, p0, Lcom/sec/vip/amschaton/w;->aa:F

    .line 408
    int-to-float v0, v4

    iput v0, p0, Lcom/sec/vip/amschaton/w;->ab:F

    .line 409
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/vip/amschaton/w;->B:I

    move v0, v1

    goto/16 :goto_1

    .line 410
    :cond_7
    int-to-float v5, v3

    sub-float/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_8

    add-int v5, v2, v4

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_8

    .line 412
    int-to-float v3, v0

    iput v3, p0, Lcom/sec/vip/amschaton/w;->E:F

    .line 413
    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/w;->F:F

    .line 414
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/w;->aa:F

    .line 415
    int-to-float v0, v4

    iput v0, p0, Lcom/sec/vip/amschaton/w;->ab:F

    .line 416
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/vip/amschaton/w;->B:I

    move v0, v1

    goto/16 :goto_1

    .line 417
    :cond_8
    add-int v5, v0, v3

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_9

    int-to-float v5, v4

    sub-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v7, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v7, v8

    cmpg-float v5, v5, v7

    if-gez v5, :cond_9

    .line 419
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/w;->E:F

    .line 420
    int-to-float v0, v2

    iput v0, p0, Lcom/sec/vip/amschaton/w;->F:F

    .line 421
    int-to-float v0, v3

    iput v0, p0, Lcom/sec/vip/amschaton/w;->aa:F

    .line 422
    int-to-float v0, v2

    iput v0, p0, Lcom/sec/vip/amschaton/w;->ab:F

    .line 423
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/vip/amschaton/w;->B:I

    move v0, v1

    goto/16 :goto_1

    .line 424
    :cond_9
    int-to-float v0, v0

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v5, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v5, v8

    cmpg-float v0, v0, v5

    if-gez v0, :cond_a

    add-int v0, v2, v4

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v5, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v5, v8

    cmpg-float v0, v0, v5

    if-gez v0, :cond_a

    .line 426
    int-to-float v0, v3

    iput v0, p0, Lcom/sec/vip/amschaton/w;->E:F

    .line 427
    int-to-float v0, v2

    iput v0, p0, Lcom/sec/vip/amschaton/w;->F:F

    .line 428
    int-to-float v0, v3

    iput v0, p0, Lcom/sec/vip/amschaton/w;->aa:F

    .line 429
    int-to-float v0, v4

    iput v0, p0, Lcom/sec/vip/amschaton/w;->ab:F

    .line 430
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/vip/amschaton/w;->B:I

    :cond_a
    move v0, v1

    goto/16 :goto_1

    .line 438
    :cond_b
    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    if-nez v2, :cond_c

    .line 439
    new-instance v2, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    iput-object v2, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 440
    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->y:Lcom/sec/amsoma/AMSLibs;

    iget-object v3, p0, Lcom/sec/vip/amschaton/w;->J:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->getM_pSelectObjectData()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2, v3, v4}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetSelectObjectData(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)V

    .line 441
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/w;->ad:Z

    .line 444
    :cond_c
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/w;->ad:Z

    if-eqz v1, :cond_d

    if-eqz v0, :cond_d

    .line 445
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/vip/amschaton/w;->B:I

    goto/16 :goto_0

    .line 449
    :cond_d
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    sub-float v0, p1, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/w;->C:I

    .line 450
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v0, v0

    sub-float v0, p2, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/w;->D:I

    .line 451
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/vip/amschaton/w;->B:I

    .line 452
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v0

    const/16 v5, 0x80

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/w;->a(FFFFI)Z

    .line 453
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/w;->ad:Z

    goto/16 :goto_0

    .line 462
    :cond_e
    iput v6, p0, Lcom/sec/vip/amschaton/w;->B:I

    .line 463
    sub-float v0, p1, v3

    sub-float v1, p2, v3

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/w;->a(FF)V

    .line 464
    const/high16 v0, 0x41700000    # 15.0f

    iget v1, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    const/high16 v1, 0x41700000    # 15.0f

    iget v2, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v1, v2

    add-float/2addr v1, p2

    const/16 v2, 0xff

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/vip/amschaton/w;->a(FFI)Z

    .line 465
    sub-float v0, p1, v3

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/w;->E:F

    .line 466
    sub-float v0, p2, v3

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/w;->F:F

    goto/16 :goto_0

    .line 366
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public e(FF)Z
    .locals 8

    .prologue
    const/high16 v7, -0x3e900000    # -15.0f

    const/4 v0, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x41700000    # 15.0f

    .line 485
    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v1, :cond_0

    .line 533
    :goto_0
    return v0

    .line 488
    :cond_0
    iget v1, p0, Lcom/sec/vip/amschaton/w;->B:I

    packed-switch v1, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    move v0, v6

    .line 533
    goto :goto_0

    .line 493
    :pswitch_1
    iget v0, p0, Lcom/sec/vip/amschaton/w;->E:F

    sub-float v0, p1, v0

    .line 494
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    sub-float v1, p2, v1

    .line 495
    cmpg-float v2, v5, v0

    if-gtz v2, :cond_2

    iget v2, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v2, v4

    cmpg-float v2, v0, v2

    if-gez v2, :cond_2

    .line 496
    iget v2, p0, Lcom/sec/vip/amschaton/w;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v4

    add-float p1, v2, v3

    .line 498
    :cond_2
    iget v2, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v2, v7

    cmpg-float v2, v2, v0

    if-gez v2, :cond_3

    cmpg-float v0, v0, v5

    if-gez v0, :cond_3

    .line 499
    iget v0, p0, Lcom/sec/vip/amschaton/w;->E:F

    iget v2, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v2, v4

    sub-float p1, v0, v2

    .line 501
    :cond_3
    cmpg-float v0, v5, v1

    if-gtz v0, :cond_4

    iget v0, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v0, v4

    cmpg-float v0, v1, v0

    if-gez v0, :cond_4

    .line 502
    iget v0, p0, Lcom/sec/vip/amschaton/w;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v2, v4

    add-float p2, v0, v2

    .line 504
    :cond_4
    iget v0, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v0, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    cmpg-float v0, v1, v5

    if-gez v0, :cond_5

    .line 505
    iget v0, p0, Lcom/sec/vip/amschaton/w;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v1, v4

    sub-float p2, v0, v1

    .line 507
    :cond_5
    const/16 v0, 0x80

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/vip/amschaton/w;->a(FFI)Z

    .line 508
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/w;->H:Z

    if-eqz v0, :cond_1

    .line 509
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/w;->I:Z

    goto :goto_1

    .line 513
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    sub-int/2addr v0, v1

    .line 514
    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    sub-int v4, v1, v2

    .line 515
    iget v1, p0, Lcom/sec/vip/amschaton/w;->C:I

    int-to-float v1, v1

    sub-float v1, p1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/w;->D:I

    int-to-float v2, v2

    sub-float v2, p2, v2

    int-to-float v0, v0

    add-float/2addr v0, p1

    iget v3, p0, Lcom/sec/vip/amschaton/w;->C:I

    int-to-float v3, v3

    sub-float v3, v0, v3

    int-to-float v0, v4

    add-float/2addr v0, p2

    iget v4, p0, Lcom/sec/vip/amschaton/w;->D:I

    int-to-float v4, v4

    sub-float v4, v0, v4

    const/16 v5, 0x80

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/w;->a(FFFFI)Z

    .line 516
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/w;->H:Z

    if-eqz v0, :cond_1

    .line 517
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/w;->I:Z

    goto/16 :goto_1

    .line 523
    :pswitch_3
    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v1, v1

    .line 524
    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v2, v2

    .line 525
    sub-float/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x41200000    # 10.0f

    iget v4, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_6

    sub-float/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x41200000    # 10.0f

    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_6

    move v1, v6

    :goto_2
    if-nez v1, :cond_1

    .line 527
    const/16 v1, 0x65

    invoke-virtual {p0, v1, v0}, Lcom/sec/vip/amschaton/w;->b(IZ)V

    .line 528
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/w;->B:I

    goto/16 :goto_1

    :cond_6
    move v1, v0

    .line 525
    goto :goto_2

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public f(FF)Z
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/high16 v8, -0x3e900000    # -15.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v5, 0x41700000    # 15.0f

    .line 549
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->y:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    move v0, v6

    .line 670
    :goto_0
    return v0

    .line 552
    :cond_0
    new-instance v0, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 553
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    sub-float v1, p1, v1

    .line 554
    iget v2, p0, Lcom/sec/vip/amschaton/w;->F:F

    sub-float v2, p2, v2

    .line 555
    iget v3, p0, Lcom/sec/vip/amschaton/w;->B:I

    packed-switch v3, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v6

    .line 670
    goto :goto_0

    .line 557
    :pswitch_1
    cmpg-float v3, v7, v1

    if-gtz v3, :cond_1

    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v5

    cmpg-float v3, v1, v3

    if-gez v3, :cond_1

    .line 558
    iget v3, p0, Lcom/sec/vip/amschaton/w;->E:F

    iget v4, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v4, v5

    add-float p1, v3, v4

    .line 560
    :cond_1
    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v8

    cmpg-float v3, v3, v1

    if-gez v3, :cond_2

    cmpg-float v1, v1, v7

    if-gez v1, :cond_2

    .line 561
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v5

    sub-float p1, v1, v3

    .line 563
    :cond_2
    cmpg-float v1, v7, v2

    if-gtz v1, :cond_3

    iget v1, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v1, v5

    cmpg-float v1, v2, v1

    if-gez v1, :cond_3

    .line 564
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v5

    add-float p2, v1, v3

    .line 566
    :cond_3
    iget v1, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v1, v8

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    cmpg-float v1, v2, v7

    if-gez v1, :cond_4

    .line 567
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v2, v5

    sub-float p2, v1, v2

    .line 569
    :cond_4
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 570
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 571
    float-to-int v1, p1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 572
    float-to-int v1, p2

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 573
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    cmpg-float v1, p1, v1

    if-gez v1, :cond_5

    .line 574
    float-to-int v1, p1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 575
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 577
    :cond_5
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    cmpg-float v1, p2, v1

    if-gez v1, :cond_6

    .line 578
    float-to-int v1, p2

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 579
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 581
    :cond_6
    iput v6, p0, Lcom/sec/vip/amschaton/w;->B:I

    .line 582
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/w;->a(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    goto/16 :goto_0

    .line 585
    :pswitch_2
    iput v4, p0, Lcom/sec/vip/amschaton/w;->B:I

    .line 586
    iget-object v1, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    sub-int/2addr v1, v2

    .line 587
    iget-object v2, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    iget-object v3, p0, Lcom/sec/vip/amschaton/w;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    sub-int/2addr v2, v3

    .line 588
    iget v3, p0, Lcom/sec/vip/amschaton/w;->C:I

    int-to-float v3, v3

    sub-float v3, p1, v3

    float-to-int v3, v3

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 589
    iget v3, p0, Lcom/sec/vip/amschaton/w;->D:I

    int-to-float v3, v3

    sub-float v3, p2, v3

    float-to-int v3, v3

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 590
    int-to-float v1, v1

    add-float/2addr v1, p1

    iget v3, p0, Lcom/sec/vip/amschaton/w;->C:I

    int-to-float v3, v3

    sub-float/2addr v1, v3

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 591
    int-to-float v1, v2

    add-float/2addr v1, p2

    iget v2, p0, Lcom/sec/vip/amschaton/w;->D:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 592
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/w;->I:Z

    if-nez v1, :cond_7

    .line 593
    iget-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v0

    const/16 v5, 0xff

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/w;->a(FFFFI)Z

    goto/16 :goto_1

    .line 596
    :cond_7
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/w;->I:Z

    .line 597
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/w;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    goto/16 :goto_0

    .line 600
    :pswitch_3
    iput v4, p0, Lcom/sec/vip/amschaton/w;->B:I

    .line 601
    cmpg-float v3, v7, v1

    if-gtz v3, :cond_8

    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v5

    cmpg-float v3, v1, v3

    if-gez v3, :cond_8

    .line 602
    iget v3, p0, Lcom/sec/vip/amschaton/w;->E:F

    iget v4, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v4, v5

    add-float p1, v3, v4

    .line 604
    :cond_8
    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v8

    cmpg-float v3, v3, v1

    if-gez v3, :cond_9

    cmpg-float v1, v1, v7

    if-gez v1, :cond_9

    .line 605
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v5

    sub-float p1, v1, v3

    .line 607
    :cond_9
    cmpg-float v1, v7, v2

    if-gtz v1, :cond_a

    iget v1, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v1, v5

    cmpg-float v1, v2, v1

    if-gez v1, :cond_a

    .line 608
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    iget v3, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v3, v5

    add-float p2, v1, v3

    .line 610
    :cond_a
    iget v1, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v1, v8

    cmpg-float v1, v1, v2

    if-gez v1, :cond_b

    cmpg-float v1, v2, v7

    if-gez v1, :cond_b

    .line 611
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/w;->d:F

    mul-float/2addr v2, v5

    sub-float p2, v1, v2

    .line 613
    :cond_b
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/w;->I:Z

    if-nez v1, :cond_c

    .line 614
    const/16 v0, 0xff

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/vip/amschaton/w;->a(FFI)Z

    goto/16 :goto_1

    .line 617
    :cond_c
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/w;->I:Z

    .line 618
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 619
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 620
    float-to-int v1, p1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 621
    float-to-int v1, p2

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 622
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    cmpg-float v1, p1, v1

    if-gez v1, :cond_d

    .line 623
    float-to-int v1, p1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 624
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 626
    :cond_d
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    cmpg-float v1, p2, v1

    if-gez v1, :cond_e

    .line 627
    float-to-int v1, p2

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 628
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 630
    :cond_e
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/w;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    goto/16 :goto_0

    .line 632
    :pswitch_4
    iput v4, p0, Lcom/sec/vip/amschaton/w;->B:I

    .line 633
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/w;->I:Z

    if-nez v1, :cond_f

    .line 634
    const/16 v0, 0xff

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/vip/amschaton/w;->a(FFI)Z

    goto/16 :goto_1

    .line 637
    :cond_f
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/w;->I:Z

    .line 638
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 639
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 640
    float-to-int v1, p1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 641
    iget v1, p0, Lcom/sec/vip/amschaton/w;->ab:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 642
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    cmpg-float v1, p1, v1

    if-gez v1, :cond_10

    .line 643
    float-to-int v1, p1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 644
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 646
    :cond_10
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/w;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    goto/16 :goto_0

    .line 648
    :pswitch_5
    iput v4, p0, Lcom/sec/vip/amschaton/w;->B:I

    .line 649
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/w;->I:Z

    if-nez v1, :cond_11

    .line 650
    const/16 v0, 0xff

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/vip/amschaton/w;->a(FFI)Z

    goto/16 :goto_1

    .line 653
    :cond_11
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/w;->I:Z

    .line 654
    iget v1, p0, Lcom/sec/vip/amschaton/w;->E:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 655
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 656
    iget v1, p0, Lcom/sec/vip/amschaton/w;->aa:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 657
    float-to-int v1, p2

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 658
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    cmpg-float v1, p2, v1

    if-gez v1, :cond_12

    .line 659
    float-to-int v1, p2

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 660
    iget v1, p0, Lcom/sec/vip/amschaton/w;->F:F

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 662
    :cond_12
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/w;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    goto/16 :goto_0

    .line 666
    :pswitch_6
    const/16 v0, 0x65

    invoke-virtual {p0, v0, v6}, Lcom/sec/vip/amschaton/w;->b(IZ)V

    .line 667
    iget-object v0, p0, Lcom/sec/vip/amschaton/w;->S:Lcom/sec/vip/amschaton/v;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/v;->a()V

    goto/16 :goto_1

    .line 555
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
