.class Lcom/sec/vip/amschaton/bt;
.super Ljava/lang/Object;
.source "ZoomableImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/ZoomableImageView;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 799
    iput-object p1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x2

    const/high16 v4, 0x40a00000    # 5.0f

    const v3, 0x3e99999a    # 0.3f

    const/4 v2, 0x0

    .line 804
    iget-object v0, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    .line 805
    iget-object v0, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iput-boolean v2, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    .line 806
    iget-object v0, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->b(Lcom/sec/vip/amschaton/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/vip/amschaton/ZoomableImageView;->a(Lcom/sec/vip/amschaton/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 808
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 809
    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 811
    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    aget v2, v0, v2

    iput v2, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 812
    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    aget v2, v0, v5

    iput v2, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 813
    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    aget v0, v0, v6

    iput v0, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 816
    iget-object v0, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    sub-float/2addr v0, v1

    .line 817
    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    sub-float/2addr v1, v2

    .line 819
    iget-object v2, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 837
    :goto_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->invalidate()V

    .line 838
    return-void

    .line 821
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    .line 822
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 823
    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 825
    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    aget v2, v0, v2

    iput v2, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 826
    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    aget v2, v0, v5

    iput v2, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 827
    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    aget v0, v0, v6

    iput v0, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 830
    iget-object v0, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    .line 831
    iget-object v1, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v3

    .line 833
    iget-object v2, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 834
    iget-object v0, p0, Lcom/sec/vip/amschaton/bt;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->b(Lcom/sec/vip/amschaton/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x19

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
