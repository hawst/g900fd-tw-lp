.class public Lcom/sec/vip/amschaton/AMSDrawManager;
.super Lcom/sec/vip/amschaton/ZoomableImageView;
.source "AMSDrawManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;


# instance fields
.field private I:Landroid/content/Context;

.field private J:Lcom/sec/amsoma/AMSLibs;

.field private K:I

.field private L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

.field private M:I

.field private N:I

.field private O:I

.field private P:I

.field private Q:I

.field private R:I

.field private S:I

.field private T:F

.field private U:Lcom/sec/vip/amschaton/u;

.field private V:Lcom/sec/vip/amschaton/u;

.field private W:Landroid/graphics/Bitmap;

.field private Z:Landroid/graphics/Bitmap;

.field private aA:F

.field private aB:F

.field private aC:F

.field private aD:I

.field private aE:Lcom/sec/vip/amschaton/ac;

.field private aF:Z

.field private aG:Z

.field private aH:Landroid/graphics/PointF;

.field private aI:Landroid/graphics/Bitmap;

.field private aJ:Z

.field private aK:I

.field private aL:Lcom/sec/vip/amschaton/n;

.field private aM:Lcom/sec/vip/amschaton/v;

.field private final aN:I

.field private aO:Lcom/sec/vip/amschaton/o;

.field private aa:Landroid/graphics/Bitmap;

.field private ab:Z

.field private ac:I

.field private ad:Z

.field private ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

.field private final af:I

.field private final ag:I

.field private ah:I

.field private ai:Lcom/sec/vip/amschaton/u;

.field private aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

.field private ak:I

.field private al:Z

.field private am:Z

.field private an:I

.field private ao:I

.field private ap:Z

.field private aq:I

.field private ar:Z

.field private as:I

.field private at:[F

.field private au:[F

.field private av:F

.field private aw:Lcom/sec/vip/amschaton/ab;

.field private ax:Lcom/sec/vip/amschaton/p;

.field private ay:Z

.field private az:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 204
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/AMS/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSDrawManager;->a:Ljava/lang/String;

    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "template/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSDrawManager;->b:Ljava/lang/String;

    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "temp/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSDrawManager;->c:Ljava/lang/String;

    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "amsbasicfiles/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSDrawManager;->d:Ljava/lang/String;

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "amsuserfiles/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSDrawManager;->e:Ljava/lang/String;

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "amssentfiles/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/vip/amschaton/AMSDrawManager;->f:Ljava/lang/String;

    return-void

    .line 204
    :cond_0
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 285
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/ZoomableImageView;-><init>(Landroid/content/Context;)V

    .line 71
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    .line 73
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    .line 74
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 75
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    .line 76
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    .line 77
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    .line 78
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ab:Z

    .line 81
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    .line 82
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    .line 85
    const/16 v0, 0x20

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->af:I

    .line 86
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ag:I

    .line 87
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ah:I

    .line 88
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    .line 89
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 90
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    .line 91
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    .line 92
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->am:Z

    .line 93
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->an:I

    .line 94
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    .line 95
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    .line 101
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aq:I

    .line 102
    iput-boolean v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ar:Z

    .line 108
    const/high16 v0, 0x42000000    # 32.0f

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->av:F

    .line 113
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aw:Lcom/sec/vip/amschaton/ab;

    .line 114
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ax:Lcom/sec/vip/amschaton/p;

    .line 123
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ay:Z

    .line 124
    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->az:F

    .line 125
    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aA:F

    .line 128
    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aB:F

    .line 129
    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aC:F

    .line 137
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aD:I

    .line 140
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    .line 141
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aF:Z

    .line 142
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aG:Z

    .line 143
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v3, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aH:Landroid/graphics/PointF;

    .line 144
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aI:Landroid/graphics/Bitmap;

    .line 147
    iput-boolean v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    .line 183
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    .line 187
    new-instance v0, Lcom/sec/vip/amschaton/l;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/l;-><init>(Lcom/sec/vip/amschaton/AMSDrawManager;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aM:Lcom/sec/vip/amschaton/v;

    .line 226
    const/16 v0, 0xf0

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aN:I

    .line 3037
    new-instance v0, Lcom/sec/vip/amschaton/m;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/m;-><init>(Lcom/sec/vip/amschaton/AMSDrawManager;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aO:Lcom/sec/vip/amschaton/o;

    .line 286
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 255
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/ZoomableImageView;-><init>(Landroid/content/Context;)V

    .line 71
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    .line 73
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    .line 74
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 75
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    .line 76
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    .line 77
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    .line 78
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ab:Z

    .line 81
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    .line 82
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    .line 85
    const/16 v0, 0x20

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->af:I

    .line 86
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ag:I

    .line 87
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ah:I

    .line 88
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    .line 89
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 90
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    .line 91
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    .line 92
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->am:Z

    .line 93
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->an:I

    .line 94
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    .line 95
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    .line 101
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aq:I

    .line 102
    iput-boolean v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ar:Z

    .line 108
    const/high16 v0, 0x42000000    # 32.0f

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->av:F

    .line 113
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aw:Lcom/sec/vip/amschaton/ab;

    .line 114
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ax:Lcom/sec/vip/amschaton/p;

    .line 123
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ay:Z

    .line 124
    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->az:F

    .line 125
    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aA:F

    .line 128
    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aB:F

    .line 129
    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aC:F

    .line 137
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aD:I

    .line 140
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    .line 141
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aF:Z

    .line 142
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aG:Z

    .line 143
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v3, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aH:Landroid/graphics/PointF;

    .line 144
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aI:Landroid/graphics/Bitmap;

    .line 147
    iput-boolean v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    .line 183
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    .line 187
    new-instance v0, Lcom/sec/vip/amschaton/l;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/l;-><init>(Lcom/sec/vip/amschaton/AMSDrawManager;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aM:Lcom/sec/vip/amschaton/v;

    .line 226
    const/16 v0, 0xf0

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aN:I

    .line 3037
    new-instance v0, Lcom/sec/vip/amschaton/m;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/m;-><init>(Lcom/sec/vip/amschaton/AMSDrawManager;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aO:Lcom/sec/vip/amschaton/o;

    .line 256
    iput-object p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    .line 257
    iput v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    .line 258
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    .line 259
    int-to-float v0, p2

    const/high16 v1, 0x43700000    # 240.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    .line 260
    iput v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    .line 261
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    .line 262
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    .line 263
    iput p2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    .line 264
    iput p3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    .line 267
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    .line 268
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    .line 270
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 271
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 272
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 273
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 274
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 276
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/vip/amschaton/AMSDrawManager;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 279
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090144

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->av:F

    .line 282
    return-void
.end method

.method private E()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 493
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-eqz v2, :cond_0

    .line 494
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v2, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_Close(I)V

    .line 495
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    .line 497
    :cond_0
    new-instance v2, Lcom/sec/amsoma/AMSLibs;

    invoke-direct {v2}, Lcom/sec/amsoma/AMSLibs;-><init>()V

    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    .line 498
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    invoke-virtual {v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_Init()V

    .line 500
    sget-object v2, Lcom/sec/vip/amschaton/AMSDrawManager;->c:Ljava/lang/String;

    .line 501
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v3, v4, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetTempFilePath([BI)Z

    .line 505
    new-instance v2, Lcom/sec/amsoma/structure/AMS_UI_DATA;

    invoke-direct {v2}, Lcom/sec/amsoma/structure/AMS_UI_DATA;-><init>()V

    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    .line 506
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_UI_DATA;->init()V

    .line 507
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    int-to-short v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    int-to-short v4, v4

    invoke-virtual {v2, v0, v0, v3, v4}, Lcom/sec/amsoma/structure/AMS_UI_DATA;->setRect(SSSS)V

    .line 510
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iput v3, v2, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rgbPen:I

    .line 511
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    iput v3, v2, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rgbBack:I

    .line 512
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    int-to-byte v3, v3

    iput-byte v3, v2, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_cObjectSize:B

    .line 513
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iput-byte v1, v2, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_eObjectType:B

    .line 514
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iput-byte v0, v2, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_cObjectStyle:B

    .line 516
    new-instance v2, Lcom/sec/amsoma/structure/AMS_OPTION;

    invoke-direct {v2}, Lcom/sec/amsoma/structure/AMS_OPTION;-><init>()V

    .line 517
    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OPTION;->init()V

    .line 518
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    invoke-virtual {v3, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_Open(Lcom/sec/amsoma/structure/AMS_OPTION;)I

    move-result v2

    iput v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    .line 520
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[AMS Library Version] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v3, v4}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetLibVersion(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    new-instance v2, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    .line 523
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget-byte v3, v3, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_cObjectSize:B

    invoke-virtual {v2, v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_cSize(B)V

    .line 524
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget-byte v3, v3, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_cObjectStyle:B

    invoke-virtual {v2, v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_cStyle(I)V

    .line 525
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v3, v3, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rgbPen:I

    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_nColorR(I)V

    .line 526
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v3, v3, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rgbPen:I

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_nColorG(I)V

    .line 527
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v3, v3, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rgbPen:I

    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_nColorB(I)V

    .line 529
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    iget-object v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget-object v5, v5, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rectRegion:Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-virtual {v3, v4, v5, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_TotalNewEncoding(ILcom/sec/amsoma/structure/AMS_RECT;Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 530
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v1

    .line 531
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error Code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private F()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1579
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1580
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 1581
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v6, v6, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1585
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1583
    :cond_0
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method

.method private G()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1594
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetBackgroundStyle(I)I

    move-result v0

    .line 1595
    packed-switch v0, :pswitch_data_0

    .line 1621
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 1597
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetBackgroundColorR(I)I

    move-result v0

    .line 1598
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetBackgroundColorG(I)I

    move-result v1

    .line 1599
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v2, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetBackgroundColorB(I)I

    move-result v2

    .line 1600
    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    .line 1601
    invoke-virtual {p0, v0, v1, v2, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 1605
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetBackgroundIndex(I)B

    move-result v0

    .line 1607
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(I)V

    goto :goto_0

    .line 1612
    :pswitch_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetCurrentBackgroundBuf(I)[I

    move-result-object v0

    .line 1613
    sget v1, Lcom/sec/amsoma/AMSLibs;->g_nCurrentBackgroundWidth:I

    sget v2, Lcom/sec/amsoma/AMSLibs;->g_nCurrentBackgroundHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1614
    invoke-virtual {p0, v0, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Landroid/graphics/Bitmap;Z)Z

    .line 1616
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 1595
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private H()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2306
    .line 2307
    new-instance v3, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    .line 2309
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    .line 2318
    :goto_0
    return v1

    .line 2312
    :cond_0
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aK:I

    move v0, v1

    move v2, v1

    .line 2314
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v4, v5, v0, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetNextDrawingData(IILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I

    move-result v0

    .line 2315
    :goto_2
    if-nez v0, :cond_3

    .line 2316
    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v4, v5}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetNextFrame(I)Z

    move-result v4

    if-nez v4, :cond_2

    move v1, v2

    .line 2318
    goto :goto_0

    .line 2320
    :cond_2
    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v4, v5, v0, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetNextDrawingData(IILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I

    move-result v0

    goto :goto_2

    .line 2322
    :cond_3
    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aK:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aK:I

    .line 2323
    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_eType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 2325
    :pswitch_0
    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_nPointNum()I

    move-result v4

    add-int/2addr v2, v4

    .line 2326
    iget-boolean v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 2327
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    goto :goto_1

    .line 2331
    :pswitch_1
    add-int/lit8 v2, v2, 0x20

    .line 2332
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    goto :goto_1

    .line 2335
    :pswitch_2
    add-int/lit8 v2, v2, 0x20

    .line 2336
    iget-boolean v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v4

    if-nez v4, :cond_1

    .line 2337
    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cAdditionalID()I

    move-result v4

    .line 2338
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/vip/amschaton/al;->h(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2339
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    goto :goto_1

    .line 2344
    :pswitch_3
    add-int/lit8 v2, v2, 0x20

    goto :goto_1

    .line 2323
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private I()V
    .locals 1

    .prologue
    .line 2576
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    if-nez v0, :cond_0

    .line 2578
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->invalidate()V

    .line 2590
    :goto_0
    return-void

    .line 2580
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/u;->f()Landroid/graphics/Rect;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2582
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->invalidate()V

    goto :goto_0

    .line 2587
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/u;->f()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method private J()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 2597
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->as:I

    .line 2598
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->at:[F

    .line 2599
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->au:[F

    .line 2600
    return-void

    .line 2598
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 2599
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private K()Z
    .locals 2

    .prologue
    .line 2757
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetBackgroundStyle(I)I

    move-result v0

    .line 2758
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2759
    const/4 v0, 0x1

    .line 2761
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/AMSDrawManager;)Lcom/sec/vip/amschaton/ac;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    return-object v0
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/16 v5, 0xe6

    const/16 v4, 0xcf

    const/16 v1, 0x8

    const/4 v3, 0x0

    const/16 v2, 0xff

    .line 2791
    if-ltz p1, :cond_0

    const/16 v0, 0xb

    if-le p1, v0, :cond_1

    .line 2832
    :cond_0
    :goto_0
    return-void

    .line 2794
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2796
    :pswitch_0
    invoke-virtual {p0, v2, v2, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2799
    :pswitch_1
    invoke-virtual {p0, v2, v2, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2802
    :pswitch_2
    invoke-virtual {p0, v2, v2, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2805
    :pswitch_3
    const/16 v0, 0xf9

    const/16 v1, 0xbb

    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2808
    :pswitch_4
    const/16 v0, 0xf4

    invoke-virtual {p0, v4, v5, v0, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2811
    :pswitch_5
    const/16 v0, 0xf3

    const/16 v1, 0xeb

    invoke-virtual {p0, v0, v4, v1, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2814
    :pswitch_6
    const/16 v0, 0xfc

    const/16 v1, 0xf4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2817
    :pswitch_7
    const/16 v0, 0xfd

    const/16 v1, 0xee

    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2820
    :pswitch_8
    const/16 v0, 0xf7

    invoke-virtual {p0, v5, v0, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2823
    :pswitch_9
    const/16 v0, 0xfa

    const/16 v1, 0xd0

    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2826
    :pswitch_a
    const/16 v0, 0xe5

    const/16 v1, 0xce

    const/16 v2, 0x9d

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2829
    :pswitch_b
    const/16 v0, 0x31

    invoke-virtual {p0, v1, v0, v1, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    goto :goto_0

    .line 2794
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private a(Lcom/sec/amsoma/structure/AMS_RECT;)V
    .locals 7

    .prologue
    .line 2593
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    new-instance v1, Landroid/graphics/Rect;

    iget-short v2, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->av:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget-short v3, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->av:F

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iget-short v4, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->av:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    iget-short v5, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->av:F

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Rect;)V

    .line 2594
    return-void
.end method

.method private a(FFLandroid/view/MotionEvent;)Z
    .locals 18

    .prologue
    .line 1281
    invoke-virtual/range {p0 .. p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1282
    const/4 v2, 0x1

    .line 1496
    :goto_0
    return v2

    .line 1299
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->e(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1300
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ab:Z

    .line 1301
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 1302
    const/4 v2, 0x1

    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ac:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(IIII)Z

    .line 1303
    new-instance v2, Lcom/sec/vip/amschaton/x;

    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ac:I

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/vip/amschaton/x;-><init>(III)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    .line 1304
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/amsoma/AMSLibs;ILcom/sec/amsoma/structure/AMS_UI_DATA;)V

    .line 1305
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3, v4}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 1306
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 1307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 1308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/sec/vip/amschaton/u;->d(FF)Z

    move-result v2

    goto :goto_0

    .line 1312
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-eqz v2, :cond_2

    .line 1313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/sec/vip/amschaton/u;->c(FF)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/sec/vip/amschaton/u;->d(FF)Z

    move-result v2

    goto/16 :goto_0

    .line 1319
    :cond_2
    new-instance v6, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-direct {v6}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;-><init>()V

    .line 1320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    move/from16 v0, p1

    float-to-int v4, v0

    move/from16 v0, p2

    float-to-int v5, v0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    invoke-virtual/range {v2 .. v7}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SelectObject(IIILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;I)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1322
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aK:I

    const/16 v3, 0x94

    if-le v2, v3, :cond_3

    .line 1323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    const v3, 0x7f0b00fa

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1324
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1326
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aK:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aK:I

    .line 1328
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    .line 1330
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1332
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3, v4}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 1333
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    .line 1334
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 1335
    if-eqz v2, :cond_5

    .line 1336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->b(Z)V

    .line 1338
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    if-eqz v2, :cond_6

    .line 1339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface/range {v2 .. v7}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    .line 1345
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    if-nez v2, :cond_8

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->f(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getPressure()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/high16 v4, 0x45800000    # 4096.0f

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->b(F)V

    .line 1356
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    .line 1357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ax:Lcom/sec/vip/amschaton/p;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/sec/vip/amschaton/p;->a(Z)V

    .line 1360
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/sec/vip/amschaton/u;->d(FF)Z

    move-result v2

    goto/16 :goto_0

    .line 1352
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->b(F)V

    goto :goto_1

    .line 1363
    :cond_9
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 1364
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    .line 1365
    new-instance v8, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    .line 1366
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->getM_pSelectObjectData()I

    move-result v3

    invoke-virtual {v2, v3, v8}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetSelectObjectData(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)V

    .line 1367
    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_eType()I

    move-result v4

    .line 1369
    const/4 v2, 0x3

    if-ne v4, v2, :cond_a

    .line 1370
    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cAdditionalID()I

    move-result v2

    move v9, v2

    .line 1374
    :goto_2
    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v10

    .line 1375
    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v5

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v11

    .line 1376
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v9, v11, v10}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(IIII)Z

    move-result v2

    if-nez v2, :cond_b

    .line 1377
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1372
    :cond_a
    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v2

    move v9, v2

    goto :goto_2

    .line 1380
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    if-eqz v2, :cond_c

    .line 1381
    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_eType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_e

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_e

    .line 1382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    const/4 v3, 0x1

    const/4 v4, 0x3

    const/4 v5, -0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-interface/range {v2 .. v7}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    .line 1392
    :cond_c
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;)Z

    .line 1395
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020076

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    .line 1396
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020074

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1397
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020074

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1398
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020075

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1399
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020078

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 1400
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020077

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    .line 1401
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020078

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v15

    .line 1402
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020077

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v16

    .line 1403
    const/16 v17, 0x5

    .line 1408
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-eqz v5, :cond_d

    .line 1409
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v5}, Lcom/sec/vip/amschaton/u;->e()V

    .line 1410
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 1412
    :cond_d
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    packed-switch v5, :pswitch_data_0

    .line 1479
    new-instance v5, Lcom/sec/vip/amschaton/z;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct {v5, v6, v11, v10, v7}, Lcom/sec/vip/amschaton/z;-><init>(IIIZ)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 1480
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0201a5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1481
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v6, v5}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1482
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v5, v2, v3, v4}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1484
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v2, v12}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1485
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aM:Lcom/sec/vip/amschaton/v;

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/vip/amschaton/v;)V

    .line 1486
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/sec/vip/amschaton/u;->a(I)V

    .line 1489
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/amsoma/AMSLibs;ILcom/sec/amsoma/structure/AMS_UI_DATA;)V

    .line 1490
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;)V

    .line 1491
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 1492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 1493
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->a(Z)V

    .line 1495
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3, v4}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 1496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/sec/vip/amschaton/u;->d(FF)Z

    move-result v2

    goto/16 :goto_0

    .line 1384
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    const/4 v3, 0x1

    move v5, v9

    move v6, v11

    move v7, v10

    invoke-interface/range {v2 .. v7}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    goto/16 :goto_3

    .line 1414
    :pswitch_0
    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v2

    if-nez v2, :cond_10

    .line 1416
    new-instance v2, Lcom/sec/vip/amschaton/z;

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v6

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    move v3, v9

    move v4, v11

    move v5, v10

    invoke-direct/range {v2 .. v7}, Lcom/sec/vip/amschaton/z;-><init>(IIIIZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 1427
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020299

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1429
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v3, v2}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1430
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    if-eqz v2, :cond_f

    .line 1432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, v16

    invoke-virtual {v2, v13, v14, v15, v0}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1435
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v2, v12}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1436
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aM:Lcom/sec/vip/amschaton/v;

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/vip/amschaton/v;)V

    .line 1437
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/sec/vip/amschaton/u;->a(I)V

    goto/16 :goto_4

    .line 1418
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v2, v3, v8}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetObjectImageBuf(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)[I

    move-result-object v2

    .line 1419
    sget v3, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageWidth:I

    sget v4, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1421
    new-instance v2, Lcom/sec/vip/amschaton/z;

    const/4 v3, -0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-virtual {v8}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v6

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    invoke-direct/range {v2 .. v8}, Lcom/sec/vip/amschaton/z;-><init>(IIIILandroid/graphics/Bitmap;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    goto :goto_5

    .line 1440
    :pswitch_1
    new-instance v5, Lcom/sec/vip/amschaton/w;

    const/4 v6, 0x0

    invoke-direct {v5, v9, v11, v10, v6}, Lcom/sec/vip/amschaton/w;-><init>(IIIZ)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 1445
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020299

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1446
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020314

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/vip/amschaton/u;->c(Landroid/graphics/drawable/Drawable;)V

    .line 1448
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v6, v5}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1449
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v5, v2, v3, v4}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1451
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v2, v12}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1452
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aM:Lcom/sec/vip/amschaton/v;

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/vip/amschaton/v;)V

    .line 1453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/sec/vip/amschaton/u;->a(I)V

    goto/16 :goto_4

    .line 1456
    :pswitch_2
    new-instance v2, Lcom/sec/vip/amschaton/aa;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    move v3, v9

    move v4, v11

    move v5, v10

    invoke-direct/range {v2 .. v8}, Lcom/sec/vip/amschaton/aa;-><init>(IIIZII)V

    .line 1457
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aw:Lcom/sec/vip/amschaton/ab;

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/aa;->a(Lcom/sec/vip/amschaton/ab;)V

    .line 1458
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/aa;->b(Z)V

    .line 1459
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 1464
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020299

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1467
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v3, v2}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1469
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, v16

    invoke-virtual {v2, v13, v14, v15, v0}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1471
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v2, v12}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1472
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aM:Lcom/sec/vip/amschaton/v;

    invoke-virtual {v2, v3}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/vip/amschaton/v;)V

    .line 1473
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/sec/vip/amschaton/u;->a(I)V

    .line 1475
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ax:Lcom/sec/vip/amschaton/p;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/sec/vip/amschaton/p;->a(Z)V

    goto/16 :goto_4

    .line 1412
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(FFZ)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1534
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->c(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    move v1, v0

    .line 1561
    :cond_0
    :goto_0
    return v1

    .line 1538
    :cond_1
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    if-nez v2, :cond_4

    .line 1540
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0, p3}, Lcom/sec/vip/amschaton/u;->c(Z)V

    .line 1541
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0, p1, p2}, Lcom/sec/vip/amschaton/u;->f(FF)Z

    move-result v6

    .line 1542
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    if-eqz v0, :cond_2

    .line 1543
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-interface/range {v0 .. v5}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    .line 1545
    :cond_2
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ab:Z

    if-eqz v0, :cond_3

    .line 1546
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ab:Z

    .line 1547
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    :cond_3
    move v1, v6

    .line 1549
    goto :goto_0

    .line 1551
    :cond_4
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-eqz v2, :cond_0

    .line 1552
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    if-eqz v2, :cond_5

    .line 1553
    const/4 v2, 0x2

    new-array v5, v2, [I

    .line 1554
    invoke-virtual {p0, v5}, Lcom/sec/vip/amschaton/AMSDrawManager;->getLocationOnScreen([I)V

    .line 1555
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    const/4 v3, 0x4

    aget v4, v5, v1

    int-to-float v4, v4

    add-float/2addr v4, p1

    float-to-int v4, v4

    aget v0, v5, v0

    int-to-float v0, v0

    add-float/2addr v0, p2

    float-to-int v5, v0

    move v6, v1

    move v7, v1

    invoke-interface/range {v2 .. v7}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    .line 1558
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0, p3}, Lcom/sec/vip/amschaton/u;->c(Z)V

    .line 1559
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0, p1, p2}, Lcom/sec/vip/amschaton/u;->f(FF)Z

    move-result v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/AMSDrawManager;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aG:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/AMSDrawManager;)Lcom/sec/vip/amschaton/ab;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aw:Lcom/sec/vip/amschaton/ab;

    return-object v0
.end method

.method private b(FFLandroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1502
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    move v4, v3

    .line 1528
    :cond_0
    :goto_0
    return v4

    .line 1506
    :cond_1
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    if-nez v0, :cond_3

    .line 1508
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    if-ne v0, v3, :cond_2

    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    if-nez v0, :cond_2

    invoke-direct {p0, p3}, Lcom/sec/vip/amschaton/AMSDrawManager;->f(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1513
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getPressure()F

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/high16 v2, 0x45800000    # 4096.0f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(F)V

    .line 1517
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0, p1, p2}, Lcom/sec/vip/amschaton/u;->e(FF)Z

    move-result v4

    goto :goto_0

    .line 1515
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(F)V

    goto :goto_1

    .line 1519
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_0

    .line 1520
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    if-eqz v0, :cond_4

    .line 1522
    const/4 v0, 0x2

    new-array v5, v0, [I

    .line 1523
    invoke-virtual {p0, v5}, Lcom/sec/vip/amschaton/AMSDrawManager;->getLocationOnScreen([I)V

    .line 1524
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    const/4 v1, 0x3

    aget v2, v5, v4

    int-to-float v2, v2

    add-float/2addr v2, p1

    float-to-int v2, v2

    aget v3, v5, v3

    int-to-float v3, v3

    add-float/2addr v3, p2

    float-to-int v3, v3

    move v5, v4

    invoke-interface/range {v0 .. v5}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    .line 1526
    :cond_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0, p1, p2}, Lcom/sec/vip/amschaton/u;->e(FF)Z

    move-result v4

    goto :goto_0
.end method

.method private b(II)Z
    .locals 17

    .prologue
    .line 1809
    new-instance v5, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-direct {v5}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;-><init>()V

    .line 1810
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    move/from16 v3, p1

    move/from16 v4, p2

    invoke-virtual/range {v1 .. v6}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SelectObject(IIILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1811
    const/4 v1, 0x0

    .line 1916
    :goto_0
    return v1

    .line 1813
    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 1814
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    .line 1815
    new-instance v7, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    .line 1816
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->getM_pSelectObjectData()I

    move-result v2

    invoke-virtual {v1, v2, v7}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetSelectObjectData(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)V

    .line 1817
    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_eType()I

    move-result v3

    .line 1819
    const/4 v1, 0x3

    if-ne v3, v1, :cond_1

    .line 1820
    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cAdditionalID()I

    move-result v1

    move v8, v1

    .line 1824
    :goto_1
    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v9

    .line 1825
    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v1

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v2

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v1, v2, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v10

    .line 1826
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v8, v10, v9}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(IIII)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1827
    const/4 v1, 0x0

    goto :goto_0

    .line 1822
    :cond_1
    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 1830
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    if-eqz v1, :cond_3

    .line 1831
    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_eType()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 1832
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    const/4 v2, 0x1

    const/4 v3, 0x3

    const/4 v4, -0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-interface/range {v1 .. v6}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    .line 1838
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;)Z

    .line 1841
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020076

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 1842
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020074

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 1843
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020074

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 1844
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020075

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 1845
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020078

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    .line 1846
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 1847
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020078

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    .line 1848
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v15

    .line 1849
    const/16 v16, 0x5

    .line 1850
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-eqz v1, :cond_4

    .line 1851
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/u;->e()V

    .line 1852
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 1855
    :cond_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    packed-switch v1, :pswitch_data_0

    .line 1899
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/amsoma/AMSLibs;ILcom/sec/amsoma/structure/AMS_UI_DATA;)V

    .line 1900
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;)V

    .line 1901
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 1902
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 1903
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/u;->a(Z)V

    .line 1905
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 1911
    new-instance v7, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    .line 1912
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->getM_pSelectObjectData()I

    move-result v2

    invoke-virtual {v1, v2, v7}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetSelectObjectData(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)V

    .line 1913
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_8

    .line 1914
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v3, v3

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v4, v4

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v5

    iget-short v5, v5, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v5, v5

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v6

    iget-short v6, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v6, v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/16 v3, 0xff

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    move-result v1

    goto/16 :goto_0

    .line 1834
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    const/4 v2, 0x1

    move v4, v8

    move v5, v10

    move v6, v9

    invoke-interface/range {v1 .. v6}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    goto/16 :goto_2

    .line 1857
    :pswitch_0
    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    if-nez v1, :cond_7

    .line 1859
    new-instance v1, Lcom/sec/vip/amschaton/z;

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    move v2, v8

    move v3, v10

    move v4, v9

    invoke-direct/range {v1 .. v6}, Lcom/sec/vip/amschaton/z;-><init>(IIIIZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 1866
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/u;->b(Z)V

    .line 1867
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020299

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1869
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v2, v1}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1870
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    if-eqz v1, :cond_6

    .line 1872
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1, v12, v13, v14, v15}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1875
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1, v11}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1876
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aM:Lcom/sec/vip/amschaton/v;

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/vip/amschaton/v;)V

    .line 1877
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/u;->a(I)V

    .line 1879
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    check-cast v1, Lcom/sec/vip/amschaton/z;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/z;->d(Z)V

    goto/16 :goto_3

    .line 1861
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2, v7}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetObjectImageBuf(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)[I

    move-result-object v1

    .line 1862
    sget v2, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageWidth:I

    sget v3, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1864
    new-instance v1, Lcom/sec/vip/amschaton/z;

    const/4 v2, -0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v5

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    invoke-direct/range {v1 .. v7}, Lcom/sec/vip/amschaton/z;-><init>(IIIILandroid/graphics/Bitmap;Z)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    goto/16 :goto_4

    .line 1883
    :pswitch_1
    new-instance v1, Lcom/sec/vip/amschaton/aa;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    move v2, v8

    move v3, v10

    move v4, v9

    invoke-direct/range {v1 .. v7}, Lcom/sec/vip/amschaton/aa;-><init>(IIIZII)V

    .line 1884
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aw:Lcom/sec/vip/amschaton/ab;

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/aa;->a(Lcom/sec/vip/amschaton/ab;)V

    .line 1885
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/aa;->b(Z)V

    .line 1886
    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 1887
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020299

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1889
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v2, v1}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1891
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1, v12, v13, v14, v15}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1893
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1, v11}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1894
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->aM:Lcom/sec/vip/amschaton/v;

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/vip/amschaton/v;)V

    .line 1895
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    move/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/u;->a(I)V

    goto/16 :goto_3

    .line 1916
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v2

    iget-short v2, v2, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v2, v2

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v3

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v3, v3

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v4, v4

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v5

    iget-short v5, v5, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v5, v5

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_cText()[C

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([C)V

    const/16 v7, 0xff

    invoke-virtual/range {v1 .. v7}, Lcom/sec/vip/amschaton/u;->a(FFFFLjava/lang/String;I)Z

    move-result v1

    goto/16 :goto_0

    .line 1855
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private b(IIII)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 554
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    int-to-byte v3, p1

    invoke-virtual {v1, v2, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetCurObjectType(IB)Z

    move-result v1

    if-nez v1, :cond_0

    .line 555
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v1

    .line 556
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Error Code ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Line : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    :goto_0
    return v0

    .line 559
    :cond_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    int-to-byte v3, p2

    invoke-virtual {v1, v2, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetCurObjectStyle(IB)Z

    move-result v1

    if-nez v1, :cond_1

    .line 560
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v1

    .line 561
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Error Code ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Line : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 564
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    int-to-byte v3, p4

    invoke-virtual {v1, v2, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetCurObjectSize(IB)Z

    move-result v1

    if-nez v1, :cond_2

    .line 565
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v1

    .line 566
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Error Code ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Line : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 569
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-static {p3}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {p3}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {p3}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetCurObjectColor(IIII)Z

    move-result v1

    if-nez v1, :cond_3

    .line 570
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v1

    .line 571
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Error Code ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Line : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 574
    :cond_3
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/AMSDrawManager;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aH:Landroid/graphics/PointF;

    return-object v0
.end method

.method private d(FF)Landroid/graphics/PointF;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 407
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->B()F

    move-result v0

    .line 408
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->D()Landroid/graphics/PointF;

    move-result-object v1

    .line 409
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v2

    if-nez v2, :cond_0

    iget v2, v1, Landroid/graphics/PointF;->x:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 410
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 414
    :goto_0
    return-object v0

    .line 412
    :cond_0
    iget v2, v1, Landroid/graphics/PointF;->x:F

    sub-float v2, p1, v2

    div-float/2addr v2, v0

    .line 413
    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float v1, p2, v1

    div-float/2addr v1, v0

    .line 414
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0
.end method

.method private e(FF)Lcom/sec/amsoma/structure/AMS_RECT;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 2603
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->at:[F

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->as:I

    aput p1, v0, v1

    .line 2604
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->au:[F

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->as:I

    aput p2, v0, v1

    .line 2605
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->as:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->as:I

    .line 2606
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->as:I

    if-le v0, v5, :cond_0

    .line 2607
    iput v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->as:I

    .line 2611
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->at:[F

    aget v0, v0, v4

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->at:[F

    aget v1, v1, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->at:[F

    aget v1, v1, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 2612
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->at:[F

    aget v1, v1, v4

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->at:[F

    aget v2, v2, v6

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->at:[F

    aget v2, v2, v5

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 2613
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->au:[F

    aget v2, v2, v4

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->au:[F

    aget v3, v3, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->au:[F

    aget v3, v3, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 2614
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->au:[F

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->au:[F

    aget v4, v4, v6

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->au:[F

    aget v4, v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 2616
    new-instance v4, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v4}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 2617
    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 2618
    float-to-int v1, v3

    int-to-short v1, v1

    iput-short v1, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 2619
    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 2620
    float-to-int v0, v2

    int-to-short v0, v0

    iput-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 2622
    return-object v4
.end method

.method private e(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2843
    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 2846
    const-string v3, "2.3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2848
    const/16 v2, 0x400

    .line 2849
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v3

    and-int/2addr v2, v3

    if-eqz v2, :cond_2

    .line 2864
    :cond_0
    :goto_0
    return v0

    .line 2852
    :cond_1
    const-string v3, "3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2854
    const/high16 v2, 0x4000000

    .line 2855
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v3

    and-int/2addr v2, v3

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2858
    :cond_3
    const-string v3, "4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2860
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    goto :goto_0
.end method

.method private f(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2868
    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 2871
    const-string v3, "2.3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2873
    const/16 v2, 0x200

    .line 2874
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v3

    and-int/2addr v2, v3

    if-eqz v2, :cond_2

    .line 2889
    :cond_0
    :goto_0
    return v0

    .line 2877
    :cond_1
    const-string v3, "3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2879
    const/high16 v2, 0x2000000

    .line 2880
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v3

    and-int/2addr v2, v3

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2883
    :cond_3
    const-string v3, "4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2885
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    goto :goto_0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 2987
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    if-nez v0, :cond_0

    .line 2988
    const/4 v0, 0x0

    .line 2990
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v0, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2246
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v3, :cond_0

    .line 2247
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    .line 2279
    :goto_0
    return v0

    .line 2250
    :cond_0
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v3, v4}, Lcom/sec/amsoma/AMSLibs;->VipAMS_ClearEncoding(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2251
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    goto :goto_0

    .line 2255
    :cond_1
    new-array v3, v1, [Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;

    .line 2256
    new-instance v4, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;

    invoke-direct {v4}, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;-><init>()V

    aput-object v4, v3, v2

    .line 2257
    aget-object v4, v3, v2

    invoke-virtual {v4, v2}, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->setM_eExportType(I)V

    .line 2258
    aget-object v4, v3, v2

    invoke-virtual {v4, p1}, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->setM_strFileName(Ljava/lang/String;)V

    .line 2259
    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    iget-object v6, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget-object v6, v6, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rectRegion:Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-virtual {v4, v5, v3, v6}, Lcom/sec/amsoma/AMSLibs;->VipAMS_TotalLoadEncodingFile(I[Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;Lcom/sec/amsoma/structure/AMS_RECT;)I

    move-result v3

    if-nez v3, :cond_2

    .line 2260
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v3, v4}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v3

    .line 2261
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    .line 2262
    packed-switch v3, :pswitch_data_0

    .line 2270
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VipAMS_TotalLoadEncodingFile Error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 2264
    goto :goto_0

    .line 2266
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 2268
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 2273
    :cond_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->d()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->p()I

    move-result v0

    if-gt v0, v1, :cond_3

    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->K()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2274
    :cond_3
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    .line 2278
    :goto_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->H()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->an:I

    move v0, v2

    .line 2279
    goto :goto_0

    .line 2276
    :cond_4
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    goto :goto_1

    .line 2262
    nop

    :pswitch_data_0
    .packed-switch 0x2d
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 2954
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    if-nez v0, :cond_0

    .line 2966
    :goto_0
    return-void

    .line 2957
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    if-eq p1, v0, :cond_1

    .line 2958
    iget p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    .line 2960
    :cond_1
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    if-eq p2, v0, :cond_2

    .line 2961
    iget p2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    .line 2963
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/ac;->a()V

    .line 2964
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/vip/amschaton/ac;->a(III)Z

    .line 2965
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->v()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 2564
    if-eqz p1, :cond_0

    .line 2565
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->v()V

    .line 2570
    :goto_0
    return-void

    .line 2568
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->invalidate()V

    goto :goto_0
.end method

.method public a()Z
    .locals 6

    .prologue
    const v5, 0x7f0b0149

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 451
    :try_start_0
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 452
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 453
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    .line 455
    :cond_0
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 461
    :try_start_1
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 462
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 463
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    .line 465
    :cond_1
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    .line 473
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->setDefaultScale(I)V

    .line 474
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 476
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->C()V

    .line 477
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->E()Z

    .line 478
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v3

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-virtual {p0, v2, v3, v4, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIIZ)Z

    move-result v2

    if-nez v2, :cond_2

    .line 483
    :goto_0
    return v0

    .line 456
    :catch_0
    move-exception v1

    .line 457
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-static {v1, v5, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 466
    :catch_1
    move-exception v1

    .line 467
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-static {v1, v5, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 481
    :cond_2
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 482
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->F()Z

    move v0, v1

    .line 483
    goto :goto_0
.end method

.method public a(FF)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2994
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    if-nez v0, :cond_0

    move v0, v6

    .line 3008
    :goto_0
    return v0

    .line 2997
    :cond_0
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aF:Z

    .line 2998
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v0, p1, p2}, Lcom/sec/vip/amschaton/ac;->c(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2999
    iput-boolean v7, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aF:Z

    .line 3000
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v0, p1, p2}, Lcom/sec/vip/amschaton/ac;->d(FF)Z

    move-result v0

    goto :goto_0

    .line 3003
    :cond_1
    new-instance v4, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    invoke-direct {v4}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;-><init>()V

    .line 3004
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    float-to-int v2, p1

    float-to-int v3, p2

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SelectObject(IIILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3005
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->y()V

    move v0, v6

    .line 3006
    goto :goto_0

    :cond_2
    move v0, v7

    .line 3008
    goto :goto_0
.end method

.method public a(III)Z
    .locals 2

    .prologue
    .line 1070
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_0

    .line 1071
    iput p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    .line 1072
    iput p2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    .line 1073
    iput p3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    .line 1074
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(IIII)Z

    .line 1075
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    invoke-virtual {v0, v1, p2, p3}, Lcom/sec/vip/amschaton/u;->a(III)Z

    move-result v0

    .line 1077
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(IIII)Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 594
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    iput v2, v1, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rgbBack:I

    .line 595
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(IIII)Z

    move-result v1

    if-nez v1, :cond_1

    move v4, v0

    .line 634
    :cond_0
    :goto_0
    return v4

    .line 598
    :cond_1
    iput p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    .line 599
    iput p2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    .line 600
    iput p3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    .line 601
    iput p4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    .line 603
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    if-eqz v1, :cond_2

    .line 604
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/u;->e()V

    .line 605
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    .line 607
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 628
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/vip/amschaton/u;->a(Lcom/sec/amsoma/AMSLibs;ILcom/sec/amsoma/structure/AMS_UI_DATA;)V

    .line 630
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 631
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 632
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(F)V

    goto :goto_0

    .line 609
    :pswitch_0
    new-instance v0, Lcom/sec/vip/amschaton/x;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/vip/amschaton/x;-><init>(III)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    .line 610
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 611
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ac:I

    goto :goto_1

    .line 616
    :pswitch_1
    new-instance v0, Lcom/sec/vip/amschaton/z;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    iget-boolean v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/sec/vip/amschaton/z;-><init>(IIIZ)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    goto :goto_1

    .line 619
    :pswitch_2
    new-instance v1, Lcom/sec/vip/amschaton/w;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-direct {v1, v2, v3, v5, v0}, Lcom/sec/vip/amschaton/w;-><init>(IIIZ)V

    iput-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    .line 620
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020314

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->c(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 623
    :pswitch_3
    new-instance v0, Lcom/sec/vip/amschaton/aa;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    iget v6, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    invoke-direct/range {v0 .. v6}, Lcom/sec/vip/amschaton/aa;-><init>(IIIZII)V

    .line 624
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aw:Lcom/sec/vip/amschaton/ab;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/aa;->a(Lcom/sec/vip/amschaton/ab;)V

    .line 625
    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    goto/16 :goto_1

    .line 607
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(IIIZ)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 698
    if-eqz p4, :cond_0

    .line 699
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2, p1, p2, p3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetBackgroundColor(IIII)Z

    move-result v1

    if-nez v1, :cond_0

    .line 700
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VipAMS_SetBackgroundColor Error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v2, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :goto_0
    return v0

    .line 704
    :cond_0
    invoke-static {p1, p2, p3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    .line 705
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 706
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 707
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    .line 710
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 716
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 717
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 718
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_2

    .line 719
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 721
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_3

    .line 722
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 724
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 711
    :catch_0
    move-exception v1

    .line 712
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    const v2, 0x7f0b0149

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;Z)Z
    .locals 14

    .prologue
    .line 771
    if-eqz p2, :cond_0

    .line 772
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 773
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 774
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .line 775
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    move v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 777
    iget-object v8, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v9, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    const/16 v13, 0x18

    move-object v10, v1

    move v11, v3

    move v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetBackgroundRawImage(I[IIII)Z

    move-result v0

    if-nez v0, :cond_0

    .line 778
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VipAMS_SetBackgroundRawImage1 Error = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    const/4 v0, 0x0

    .line 799
    :goto_0
    return v0

    .line 782
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 783
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 784
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    .line 786
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    .line 787
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 788
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 789
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 790
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 791
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 792
    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, p1, v2, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 793
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_2

    .line 794
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 796
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_3

    .line 797
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 799
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1091
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 1092
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v2, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->d(FF)Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 1093
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 1094
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 1095
    const/16 v4, 0x65

    iput v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aD:I

    .line 1098
    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    if-ne v4, v3, :cond_1

    .line 1099
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ay:Z

    .line 1100
    cmpg-float v4, v2, v0

    if-ltz v4, :cond_0

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    int-to-float v0, v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_6

    .line 1101
    :cond_0
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ay:Z

    move v0, v3

    .line 1119
    :goto_0
    return v0

    .line 1105
    :cond_1
    cmpg-float v3, v2, v0

    if-gez v3, :cond_2

    move v2, v0

    .line 1108
    :cond_2
    cmpg-float v3, v1, v0

    if-gez v3, :cond_5

    .line 1111
    :goto_1
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    int-to-float v1, v1

    cmpl-float v1, v2, v1

    if-lez v1, :cond_4

    .line 1112
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    .line 1114
    :goto_2
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_3

    .line 1115
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    move v5, v0

    move v0, v1

    move v1, v5

    .line 1119
    :goto_3
    invoke-direct {p0, v0, v1, p1}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(FFLandroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_3
    move v5, v0

    move v0, v1

    move v1, v5

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_3
.end method

.method public a(Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;)Z
    .locals 10

    .prologue
    .line 1641
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->G()Z

    .line 1642
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->F()Z

    .line 1643
    new-instance v9, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    .line 1644
    const/4 v1, 0x0

    .line 1645
    const/4 v0, 0x0

    .line 1647
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v2, v3, v0, v9}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetNextDrawingData(IILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I

    move-result v0

    .line 1648
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aK:I

    move v8, v0

    move-object v0, v1

    .line 1649
    :goto_0
    if-eqz v8, :cond_9

    .line 1650
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aK:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aK:I

    .line 1651
    if-eqz p1, :cond_0

    .line 1652
    invoke-virtual {p1}, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->getM_pSelectObjectNode()I

    move-result v1

    if-ne v8, v1, :cond_0

    .line 1653
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2, v8, v9}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetNextDrawingData(IILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I

    move-result v1

    move v8, v1

    .line 1654
    goto :goto_0

    .line 1657
    :cond_0
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_eType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1740
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2, v8, v9}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetNextDrawingData(IILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I

    move-result v1

    move v8, v1

    goto :goto_0

    .line 1659
    :pswitch_0
    new-instance v0, Lcom/sec/vip/amschaton/x;

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/vip/amschaton/x;-><init>(III)V

    .line 1660
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_4

    .line 1661
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;)V

    .line 1665
    :goto_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 1666
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 1670
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_nPointNum()I

    move-result v7

    .line 1671
    const/4 v1, 0x0

    move v6, v1

    :goto_3
    if-ge v6, v7, :cond_1

    .line 1672
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_pnAMSPointX(I)S

    move-result v1

    int-to-float v1, v1

    .line 1673
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_pnAMSPointY(I)S

    move-result v2

    int-to-float v2, v2

    .line 1675
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_bPressureFlag()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1676
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_pnAMSPointPressure(I)S

    move-result v3

    int-to-float v4, v3

    .line 1680
    :goto_4
    if-nez v6, :cond_6

    .line 1681
    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/u;->a(FF)V

    .line 1688
    :cond_2
    :goto_5
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_nPointNum()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v6, v3, :cond_3

    .line 1689
    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/u;->b(FF)V

    .line 1671
    :cond_3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_3

    .line 1663
    :cond_4
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 1678
    :cond_5
    const/high16 v4, -0x40800000    # -1.0f

    goto :goto_4

    .line 1682
    :cond_6
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_nPointNum()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v6, v3, :cond_2

    .line 1686
    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/u;->a(FFIFZ)Z

    goto :goto_5

    .line 1694
    :pswitch_1
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v0

    if-nez v0, :cond_7

    .line 1696
    new-instance v0, Lcom/sec/vip/amschaton/z;

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cAdditionalID()I

    move-result v1

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v3

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v4

    iget-boolean v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;-><init>(IIIIZ)V

    .line 1707
    :goto_6
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v1

    .line 1708
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;)V

    .line 1709
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 1710
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 1714
    new-instance v2, Landroid/graphics/RectF;

    iget-short v3, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v3, v3

    iget-short v4, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v4, v4

    iget-short v5, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v5, v5

    iget-short v1, v1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v1, v1

    invoke-direct {v2, v3, v4, v5, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/16 v1, 0xff

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;IZ)Z

    goto/16 :goto_1

    .line 1697
    :cond_7
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    .line 1698
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1, v9}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetObjectImageBuf(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)[I

    move-result-object v0

    .line 1699
    sget v1, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageWidth:I

    sget v2, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1701
    new-instance v0, Lcom/sec/vip/amschaton/z;

    const/4 v1, -0x1

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v3

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v4

    iget-boolean v6, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    invoke-direct/range {v0 .. v6}, Lcom/sec/vip/amschaton/z;-><init>(IIIILandroid/graphics/Bitmap;Z)V

    goto :goto_6

    .line 1704
    :cond_8
    new-instance v0, Lcom/sec/vip/amschaton/z;

    const/4 v1, 0x0

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v3

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v4

    iget-boolean v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;-><init>(IIIIZ)V

    goto/16 :goto_6

    .line 1717
    :pswitch_2
    new-instance v0, Lcom/sec/vip/amschaton/w;

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/w;-><init>(IIIZ)V

    .line 1718
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    .line 1719
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020314

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->c(Landroid/graphics/drawable/Drawable;)V

    .line 1720
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;)V

    .line 1721
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 1722
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 1725
    iget-short v1, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-short v2, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-short v3, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    const/16 v5, 0xff

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/u;->a(FFFFIZ)Z

    goto/16 :goto_1

    .line 1728
    :pswitch_3
    new-instance v0, Lcom/sec/vip/amschaton/aa;

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v3

    const/4 v4, 0x1

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    iget v6, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    invoke-direct/range {v0 .. v6}, Lcom/sec/vip/amschaton/aa;-><init>(IIIZII)V

    .line 1729
    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v4

    .line 1730
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;)V

    .line 1731
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 1732
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 1736
    iget-short v1, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-short v2, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-short v3, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-short v4, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    invoke-virtual {v9}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0xff

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/vip/amschaton/u;->a(FFFFLjava/lang/String;IZ)Z

    goto/16 :goto_1

    .line 1743
    :cond_9
    if-eqz v0, :cond_a

    .line 1744
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/u;->e()V

    .line 1748
    :cond_a
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v0

    .line 1749
    if-eqz v0, :cond_b

    .line 1750
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Error Code ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Line : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1751
    const/4 v0, 0x0

    .line 1753
    :goto_7
    return v0

    :cond_b
    const/4 v0, 0x1

    goto :goto_7

    .line 1657
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/lang/String;II)Z
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1768
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    if-eq v0, v5, :cond_0

    move v0, v8

    .line 1805
    :goto_0
    return v0

    .line 1772
    :cond_0
    iput p2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    .line 1773
    iput p3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    .line 1774
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    if-eqz v0, :cond_2

    .line 1775
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    check-cast v0, Lcom/sec/vip/amschaton/aa;

    .line 1776
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/vip/amschaton/aa;->a(III)Z

    .line 1777
    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/aa;->a(Ljava/lang/String;)Z

    .line 1801
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    if-eqz v0, :cond_1

    .line 1802
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    move v1, v8

    move v2, v8

    move v3, v8

    move v4, v8

    move v5, v8

    invoke-interface/range {v0 .. v5}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    .line 1804
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->v()V

    move v0, v9

    .line 1805
    goto :goto_0

    .line 1779
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    check-cast v0, Lcom/sec/vip/amschaton/aa;

    .line 1780
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(IIII)Z

    .line 1782
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    div-int/lit8 v1, v1, 0x2

    .line 1783
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    div-int/lit8 v1, v1, 0x2

    .line 1784
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aG:Z

    if-eqz v1, :cond_3

    .line 1785
    iput-boolean v8, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aG:Z

    .line 1787
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->y()V

    .line 1788
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aH:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-int v7, v1

    .line 1789
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aH:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    float-to-int v6, v1

    .line 1790
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    int-to-float v4, v7

    int-to-float v5, v6

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/aa;->a(Ljava/lang/String;IIFF)Z

    move v0, v6

    move v1, v7

    .line 1799
    :goto_2
    invoke-direct {p0, v1, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(II)Z

    goto :goto_1

    .line 1792
    :cond_3
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/vip/amschaton/aa;->a(Ljava/lang/String;II)Z

    .line 1793
    new-array v2, v5, [I

    .line 1794
    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/aa;->a([I)Z

    .line 1795
    aget v1, v2, v8

    .line 1796
    aget v0, v2, v9

    goto :goto_2
.end method

.method public a(Ljava/lang/String;Landroid/util/DisplayMetrics;)Z
    .locals 1

    .prologue
    .line 2196
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Ljava/lang/String;Landroid/util/DisplayMetrics;Z)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Landroid/util/DisplayMetrics;Z)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 2207
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    .line 2235
    :goto_0
    return v2

    .line 2210
    :cond_0
    iget v0, p2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, p2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2211
    const/16 v1, 0x1e0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 2214
    mul-int v0, v3, v3

    new-array v1, v0, [I

    .line 2215
    if-eqz p3, :cond_1

    .line 2216
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->o()V

    .line 2217
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    .line 2218
    invoke-virtual {p0, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Z)V

    .line 2220
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-static {v0, v3, v3, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    move v4, v2

    move v5, v2

    move v6, v3

    move v7, v3

    .line 2221
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 2222
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v4, v1, v3, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetAMSImageBuf(I[III)I

    .line 2225
    new-array v0, v8, [Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;

    .line 2226
    new-instance v1, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;

    invoke-direct {v1}, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;-><init>()V

    aput-object v1, v0, v2

    .line 2227
    aget-object v1, v0, v2

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->setM_eExportType(I)V

    .line 2228
    aget-object v1, v0, v2

    invoke-virtual {v1, p1}, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->setM_strFileName(Ljava/lang/String;)V

    .line 2230
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v3, v0}, Lcom/sec/amsoma/AMSLibs;->VipAMS_TotalSaveEncoding(I[Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;)I

    move-result v0

    if-nez v0, :cond_2

    .line 2231
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetErrorCode(I)I

    move-result v0

    .line 2232
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VipAMS_TotalSaveEncoding Error = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v2, v8

    .line 2235
    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 960
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->R:I

    return v0
.end method

.method public b(FF)Z
    .locals 1

    .prologue
    .line 3012
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    if-nez v0, :cond_0

    .line 3013
    const/4 v0, 0x0

    .line 3018
    :goto_0
    return v0

    .line 3015
    :cond_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aF:Z

    if-eqz v0, :cond_1

    .line 3016
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v0, p1, p2}, Lcom/sec/vip/amschaton/ac;->e(FF)Z

    move-result v0

    goto :goto_0

    .line 3018
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1142
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 1143
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v2, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->d(FF)Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 1144
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 1145
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 1146
    const/16 v4, 0x66

    iput v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aD:I

    .line 1148
    iput v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aB:F

    .line 1149
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aC:F

    .line 1153
    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    if-ne v4, v3, :cond_8

    .line 1154
    cmpg-float v4, v2, v0

    if-ltz v4, :cond_0

    cmpg-float v4, v1, v0

    if-ltz v4, :cond_0

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    int-to-float v4, v4

    cmpl-float v4, v2, v4

    if-gtz v4, :cond_0

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    int-to-float v4, v4

    cmpl-float v4, v1, v4

    if-lez v4, :cond_4

    .line 1156
    :cond_0
    iput v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->az:F

    .line 1157
    iput v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aA:F

    .line 1159
    iget-boolean v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ay:Z

    if-nez v4, :cond_3

    .line 1160
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ay:Z

    .line 1161
    cmpg-float v3, v2, v0

    if-gez v3, :cond_1

    move v2, v0

    .line 1164
    :cond_1
    cmpg-float v3, v1, v0

    if-gez v3, :cond_10

    .line 1167
    :goto_0
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    int-to-float v1, v1

    cmpl-float v1, v2, v1

    if-lez v1, :cond_f

    .line 1168
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    .line 1170
    :goto_1
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_2

    .line 1171
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    .line 1173
    :cond_2
    invoke-direct {p0, v1, v0, p1}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(FFLandroid/view/MotionEvent;)Z

    .line 1174
    invoke-direct {p0, v1, v0, v5}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(FFZ)Z

    move-result v0

    .line 1212
    :goto_2
    return v0

    :cond_3
    move v0, v3

    .line 1176
    goto :goto_2

    .line 1178
    :cond_4
    iget-boolean v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ay:Z

    if-eqz v3, :cond_e

    .line 1179
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ay:Z

    .line 1180
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->az:F

    .line 1181
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aA:F

    .line 1182
    cmpg-float v3, v2, v0

    if-gez v3, :cond_5

    move v2, v0

    .line 1185
    :cond_5
    cmpg-float v3, v1, v0

    if-gez v3, :cond_d

    .line 1188
    :goto_3
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    int-to-float v1, v1

    cmpl-float v1, v2, v1

    if-lez v1, :cond_c

    .line 1189
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    .line 1191
    :goto_4
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_6

    .line 1192
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    .line 1194
    :cond_6
    invoke-direct {p0, v1, v0, p1}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(FFLandroid/view/MotionEvent;)Z

    .line 1212
    :cond_7
    :goto_5
    invoke-direct {p0, v1, v0, p1}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(FFLandroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_2

    .line 1198
    :cond_8
    cmpg-float v3, v2, v0

    if-gez v3, :cond_9

    move v2, v0

    .line 1201
    :cond_9
    cmpg-float v3, v1, v0

    if-gez v3, :cond_b

    .line 1204
    :goto_6
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    int-to-float v1, v1

    cmpl-float v1, v2, v1

    if-lez v1, :cond_a

    .line 1205
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    .line 1207
    :goto_7
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_7

    .line 1208
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    goto :goto_5

    :cond_a
    move v1, v2

    goto :goto_7

    :cond_b
    move v0, v1

    goto :goto_6

    :cond_c
    move v1, v2

    goto :goto_4

    :cond_d
    move v0, v1

    goto :goto_3

    :cond_e
    move v0, v1

    move v1, v2

    goto :goto_5

    :cond_f
    move v1, v2

    goto :goto_1

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/16 v0, 0x64

    .line 2626
    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    .line 2627
    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    .line 2628
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->E()Z

    .line 2629
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Ljava/lang/String;)I

    .line 2630
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->s()Z

    move-result v0

    .line 2631
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-eqz v1, :cond_0

    .line 2632
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_Close(I)V

    .line 2633
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    .line 2635
    :cond_0
    return v0
.end method

.method public c()I
    .locals 2

    .prologue
    .line 969
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetBGAudioStyle(I)I

    move-result v0

    return v0
.end method

.method public c(FF)Z
    .locals 2

    .prologue
    .line 3022
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    if-nez v0, :cond_0

    .line 3023
    const/4 v0, 0x0

    .line 3030
    :goto_0
    return v0

    .line 3025
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aH:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 3026
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aF:Z

    if-eqz v0, :cond_1

    .line 3027
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v0, p1, p2}, Lcom/sec/vip/amschaton/ac;->f(FF)Z

    move-result v0

    goto :goto_0

    .line 3029
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aO:Lcom/sec/vip/amschaton/o;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1, p2}, Lcom/sec/vip/amschaton/o;->a(Ljava/lang/String;FF)Z

    .line 3030
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1243
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1244
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v1, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->d(FF)Landroid/graphics/PointF;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 1245
    iget v1, v0, Landroid/graphics/PointF;->x:F

    .line 1246
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 1247
    const/16 v4, 0x67

    iput v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aD:I

    .line 1249
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v4, v4, 0xff

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 1250
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aB:F

    .line 1251
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aC:F

    .line 1256
    :cond_0
    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    if-ne v4, v3, :cond_2

    .line 1257
    cmpg-float v4, v1, v2

    if-ltz v4, :cond_1

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_1

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_1

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_6

    :cond_1
    move v0, v3

    .line 1275
    :goto_0
    return v0

    .line 1261
    :cond_2
    cmpg-float v4, v1, v2

    if-gez v4, :cond_3

    move v1, v2

    .line 1264
    :cond_3
    cmpg-float v4, v0, v2

    if-gez v4, :cond_4

    move v0, v2

    .line 1267
    :cond_4
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_5

    .line 1268
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    .line 1270
    :cond_5
    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_6

    .line 1271
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    .line 1275
    :cond_6
    invoke-direct {p0, v1, v0, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(FFZ)Z

    move-result v0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 1005
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->c()I

    move-result v0

    .line 1006
    packed-switch v0, :pswitch_data_0

    .line 1013
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1010
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1006
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public e()Lcom/sec/vip/amschaton/u;
    .locals 1

    .prologue
    .line 1055
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    return-object v0
.end method

.method public f()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1570
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 1630
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;)Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1925
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetCurrentFrame(IB)Z

    .line 1926
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->G()Z

    .line 1927
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->F()Z

    .line 1928
    new-instance v0, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 1929
    iput v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ah:I

    .line 1930
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    .line 1931
    iput v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    .line 1932
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->am:Z

    .line 1933
    iput v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    .line 1934
    const/4 v0, 0x1

    return v0
.end method

.method public i()Z
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v9, 0x1

    const/16 v12, 0x20

    const/16 v11, 0xff

    const/4 v10, 0x0

    .line 1943
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->am:Z

    if-eqz v0, :cond_0

    move v0, v9

    .line 2119
    :goto_0
    return v0

    .line 1946
    :cond_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    if-nez v0, :cond_3

    .line 1947
    iput v10, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    .line 1948
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ah:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetNextDrawingData(IILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ah:I

    .line 1949
    :goto_1
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ah:I

    if-nez v0, :cond_2

    .line 1950
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetNextFrame(I)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v10

    .line 1951
    goto :goto_0

    .line 1954
    :cond_1
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->F()Z

    .line 1955
    invoke-virtual {p0, v10}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Z)V

    .line 1956
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ah:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetNextDrawingData(IILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ah:I

    goto :goto_1

    .line 1958
    :cond_2
    iput-boolean v9, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    .line 1962
    :cond_3
    iput-boolean v9, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->am:Z

    .line 1963
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_4

    .line 1964
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/u;->e()V

    .line 1965
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    .line 1967
    :cond_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_eType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2117
    :goto_2
    iput-boolean v10, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->am:Z

    move v0, v9

    .line 2119
    goto :goto_0

    .line 1969
    :pswitch_0
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-nez v0, :cond_5

    .line 1970
    new-instance v0, Lcom/sec/vip/amschaton/x;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/vip/amschaton/x;-><init>(III)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    .line 1971
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 1972
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 1973
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 1975
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_pnAMSPointX(I)S

    move-result v0

    int-to-float v1, v0

    .line 1976
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v0

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    invoke-virtual {v0, v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_pnAMSPointY(I)S

    move-result v0

    int-to-float v2, v0

    .line 1978
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_bPressureFlag()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1979
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v0

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    invoke-virtual {v0, v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_pnAMSPointPressure(I)S

    move-result v0

    int-to-float v0, v0

    .line 1983
    :goto_3
    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-nez v3, :cond_9

    .line 1984
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/u;->a(FF)V

    .line 1985
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->J()V

    .line 1990
    :cond_6
    :goto_4
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_nPointNum()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_7

    .line 1991
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/u;->b(FF)V

    .line 1992
    iput-boolean v10, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    .line 1994
    :cond_7
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    .line 1995
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    .line 1996
    invoke-direct {p0, v1, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->e(FF)Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Lcom/sec/amsoma/structure/AMS_RECT;)V

    goto/16 :goto_2

    .line 1981
    :cond_8
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_3

    .line 1986
    :cond_9
    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TPoint()Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->getM_nPointNum()I

    move-result v4

    if-ge v3, v4, :cond_6

    .line 1988
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v3, v1, v2, v11, v0}, Lcom/sec/vip/amschaton/u;->a(FFIF)Z

    goto :goto_4

    .line 1999
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cAdditionalID()I

    move-result v1

    .line 2000
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-nez v0, :cond_a

    .line 2001
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v0

    if-nez v0, :cond_b

    .line 2003
    new-instance v0, Lcom/sec/vip/amschaton/z;

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v4

    iget-boolean v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/amschaton/z;-><init>(IIIIZ)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    .line 2014
    :goto_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2, v3}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 2015
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 2016
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 2018
    :cond_a
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v0

    .line 2019
    new-instance v2, Landroid/graphics/RectF;

    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v3, v3

    iget-short v4, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v4, v4

    iget-short v5, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v5, v5

    iget-short v6, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v6, v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2020
    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    .line 2021
    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    .line 2022
    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v3

    if-nez v3, :cond_12

    .line 2024
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v3

    if-ge v1, v3, :cond_e

    .line 2025
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-ge v1, v12, :cond_d

    .line 2027
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    .line 2074
    :goto_6
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Lcom/sec/amsoma/structure/AMS_RECT;)V

    goto/16 :goto_2

    .line 2004
    :cond_b
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v0

    if-ne v0, v13, :cond_c

    .line 2005
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0, v2, v3}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetObjectImageBuf(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)[I

    move-result-object v0

    .line 2006
    sget v2, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageWidth:I

    sget v3, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 2008
    new-instance v2, Lcom/sec/vip/amschaton/z;

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v0

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v5

    invoke-static {v0, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v5

    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v6

    iget-boolean v8, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    invoke-direct/range {v2 .. v8}, Lcom/sec/vip/amschaton/z;-><init>(IIIILandroid/graphics/Bitmap;Z)V

    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    goto/16 :goto_5

    .line 2011
    :cond_c
    new-instance v2, Lcom/sec/vip/amschaton/z;

    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v0

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v0, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v5

    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nRotation()I

    move-result v6

    iget-boolean v7, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aJ:Z

    move v3, v10

    invoke-direct/range {v2 .. v7}, Lcom/sec/vip/amschaton/z;-><init>(IIIIZ)V

    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    goto/16 :goto_5

    .line 2030
    :cond_d
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1, v2, v11}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    .line 2031
    iput-boolean v10, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    goto/16 :goto_6

    .line 2034
    :cond_e
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/vip/amschaton/al;->k()I

    move-result v3

    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/vip/amschaton/al;->i()I

    move-result v4

    add-int/2addr v3, v4

    if-ge v1, v3, :cond_10

    .line 2035
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-ge v1, v12, :cond_f

    .line 2037
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v3, v3, 0x2

    add-int/lit16 v3, v3, 0x3e8

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    goto/16 :goto_6

    .line 2040
    :cond_f
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1, v2, v11}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    .line 2041
    iput-boolean v10, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    goto/16 :goto_6

    .line 2045
    :cond_10
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-ge v1, v12, :cond_11

    .line 2047
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    goto/16 :goto_6

    .line 2050
    :cond_11
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1, v2, v11}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    .line 2051
    iput-boolean v10, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    goto/16 :goto_6

    .line 2054
    :cond_12
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    if-ne v1, v13, :cond_14

    .line 2055
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-ge v1, v12, :cond_13

    .line 2057
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    goto/16 :goto_6

    .line 2060
    :cond_13
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1, v2, v11}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    .line 2061
    iput-boolean v10, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    goto/16 :goto_6

    .line 2065
    :cond_14
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-ge v1, v12, :cond_15

    .line 2067
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    goto/16 :goto_6

    .line 2070
    :cond_15
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1, v2, v11}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/RectF;I)Z

    .line 2071
    iput-boolean v10, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    goto/16 :goto_6

    .line 2077
    :pswitch_2
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-nez v0, :cond_16

    .line 2078
    new-instance v0, Lcom/sec/vip/amschaton/w;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v3

    invoke-direct {v0, v1, v2, v3, v10}, Lcom/sec/vip/amschaton/w;-><init>(IIIZ)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    .line 2079
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020314

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->c(Landroid/graphics/drawable/Drawable;)V

    .line 2080
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 2081
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 2082
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 2084
    :cond_16
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v6

    .line 2085
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    .line 2086
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    .line 2087
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-ge v0, v12, :cond_17

    .line 2088
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-short v1, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-short v2, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-short v3, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-short v4, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget v7, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    mul-float/2addr v5, v7

    float-to-int v5, v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/u;->a(FFFFI)Z

    .line 2093
    :goto_7
    invoke-direct {p0, v6}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Lcom/sec/amsoma/structure/AMS_RECT;)V

    goto/16 :goto_2

    .line 2090
    :cond_17
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-short v1, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-short v2, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-short v3, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-short v4, v6, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    move v5, v11

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/u;->a(FFFFI)Z

    .line 2091
    iput-boolean v10, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    goto :goto_7

    .line 2096
    :pswitch_3
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-nez v0, :cond_18

    .line 2097
    new-instance v0, Lcom/sec/vip/amschaton/aa;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cStyle()I

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v2}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorR()I

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorG()I

    move-result v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v4}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_nColorB()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v3}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_cSize()B

    move-result v3

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    iget v6, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    move v4, v9

    invoke-direct/range {v0 .. v6}, Lcom/sec/vip/amschaton/aa;-><init>(IIIZII)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    .line 2098
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/u;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 2099
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->b(Landroid/graphics/Bitmap;)V

    .line 2100
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 2102
    :cond_18
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TRect()Lcom/sec/amsoma/structure/AMS_RECT;

    move-result-object v7

    .line 2103
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    .line 2104
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    .line 2105
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    if-ge v0, v12, :cond_19

    .line 2108
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-short v1, v7, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-short v2, v7, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-short v3, v7, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-short v4, v7, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    iget v8, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ak:I

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    mul-float/2addr v6, v8

    float-to-int v6, v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/u;->a(FFFFLjava/lang/String;I)Z

    .line 2114
    :goto_8
    invoke-direct {p0, v7}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Lcom/sec/amsoma/structure/AMS_RECT;)V

    goto/16 :goto_2

    .line 2111
    :cond_19
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    iget-short v1, v7, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v1

    iget-short v2, v7, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v2

    iget-short v3, v7, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v3

    iget-short v4, v7, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v5

    move v6, v11

    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/u;->a(FFFFLjava/lang/String;I)Z

    .line 2112
    iput-boolean v10, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->al:Z

    goto :goto_8

    .line 1967
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public j()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2128
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-nez v0, :cond_1

    .line 2139
    :cond_0
    :goto_0
    return v1

    .line 2131
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/u;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2134
    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 2135
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    if-eqz v0, :cond_2

    .line 2136
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-interface/range {v0 .. v5}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    .line 2138
    :cond_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->v()V

    move v1, v6

    .line 2139
    goto :goto_0
.end method

.method public k()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2149
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    if-eqz v0, :cond_1

    .line 2150
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->y()V

    .line 2165
    :cond_0
    :goto_0
    return v1

    .line 2154
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-eqz v0, :cond_0

    .line 2157
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_Undo(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 2160
    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 2161
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    if-eqz v0, :cond_2

    .line 2162
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-interface/range {v0 .. v5}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    .line 2164
    :cond_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->v()V

    move v1, v6

    .line 2165
    goto :goto_0
.end method

.method public l()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2174
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_1

    .line 2185
    :cond_0
    :goto_0
    return v1

    .line 2177
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_Redo(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 2180
    invoke-virtual {p0, v1, v6}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    .line 2181
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    if-eqz v0, :cond_2

    .line 2182
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-interface/range {v0 .. v5}, Lcom/sec/vip/amschaton/n;->a(IIIII)Z

    .line 2184
    :cond_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->v()V

    move v1, v6

    .line 2185
    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 2288
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->an:I

    return v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 2297
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ao:I

    return v0
.end method

.method public o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2423
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    .line 2432
    :goto_0
    return-void

    .line 2427
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetNextFrame(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2431
    invoke-virtual {p0, v2, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 313
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_0

    .line 386
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    if-eqz v1, :cond_2

    .line 387
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/u;->f()Landroid/graphics/Rect;

    move-result-object v1

    if-nez v1, :cond_8

    .line 388
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->invalidate()V

    .line 394
    :cond_2
    :goto_1
    return v0

    .line 315
    :pswitch_1
    iput v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aq:I

    .line 319
    invoke-virtual {p0, p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 322
    :pswitch_2
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ar:Z

    if-eqz v1, :cond_0

    .line 323
    iput v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aq:I

    .line 324
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    if-ne v1, v0, :cond_4

    .line 326
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    .line 329
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_3

    .line 330
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/u;->a()V

    .line 331
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    iput v2, v0, Lcom/sec/vip/amschaton/u;->B:I

    .line 352
    :cond_3
    :goto_2
    invoke-virtual {p0, p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->d(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    .line 337
    :cond_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    if-nez v0, :cond_3

    .line 338
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    if-nez v0, :cond_5

    .line 339
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_3

    .line 340
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/u;->a()V

    .line 341
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    iput v2, v0, Lcom/sec/vip/amschaton/u;->B:I

    goto :goto_2

    .line 344
    :cond_5
    invoke-virtual {p0, p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->c(Landroid/view/MotionEvent;)Z

    goto :goto_2

    .line 356
    :pswitch_3
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aq:I

    if-ne v1, v0, :cond_6

    iget-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ar:Z

    if-eqz v1, :cond_6

    .line 357
    invoke-virtual {p0, p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->d(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    .line 362
    :cond_6
    invoke-virtual {p0, p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 365
    :pswitch_4
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ar:Z

    if-eqz v1, :cond_0

    .line 366
    invoke-virtual {p0, p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->d(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    .line 372
    :pswitch_5
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aq:I

    if-nez v1, :cond_7

    .line 376
    invoke-virtual {p0, p2}, Lcom/sec/vip/amschaton/AMSDrawManager;->c(Landroid/view/MotionEvent;)Z

    .line 378
    :cond_7
    iput v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aq:I

    goto :goto_0

    .line 390
    :cond_8
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/u;->f()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_1

    .line 313
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public p()I
    .locals 2

    .prologue
    .line 2454
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    .line 2455
    const/4 v0, -0x1

    .line 2457
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetLastFrame(I)I

    move-result v0

    goto :goto_0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 2478
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    .line 2479
    const/4 v0, 0x0

    .line 2481
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_IsEnableUndo(I)Z

    move-result v0

    goto :goto_0
.end method

.method public r()Z
    .locals 2

    .prologue
    .line 2490
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    .line 2491
    const/4 v0, 0x0

    .line 2493
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_IsEnableRedo(I)Z

    move-result v0

    goto :goto_0
.end method

.method public s()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2502
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v1, :cond_1

    .line 2505
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_IsEditEnable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ap:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAMSLib(Lcom/sec/amsoma/AMSLibs;ILcom/sec/amsoma/structure/AMS_UI_DATA;)V
    .locals 0

    .prologue
    .line 891
    iput-object p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    .line 892
    iput p2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    .line 893
    iput-object p3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    .line 894
    return-void
.end method

.method public setBitmapForAMS(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 423
    if-nez p1, :cond_1

    .line 424
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 432
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 427
    if-eqz v0, :cond_0

    .line 430
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setEditMode(ZZ)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 864
    const/4 v0, 0x0

    .line 865
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    .line 866
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    if-nez v1, :cond_1

    .line 867
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-eqz v1, :cond_0

    .line 868
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 869
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    .line 870
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    .line 871
    const/4 v0, 0x1

    .line 873
    :cond_0
    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->P:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v4, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(IIII)Z

    .line 875
    :cond_1
    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 876
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    .line 878
    :cond_2
    return-void
.end method

.method public setEnableZoom(Z)V
    .locals 0

    .prologue
    .line 441
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ar:Z

    .line 442
    return-void
.end method

.method public setEraserSize(I)V
    .locals 0

    .prologue
    .line 646
    iput p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ac:I

    .line 647
    return-void
.end method

.method public setOnObjectListener(Lcom/sec/vip/amschaton/n;)V
    .locals 0

    .prologue
    .line 903
    iput-object p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aL:Lcom/sec/vip/amschaton/n;

    .line 904
    return-void
.end method

.method public setOnTextInputRequestListener(Lcom/sec/vip/amschaton/ab;)V
    .locals 0

    .prologue
    .line 913
    iput-object p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aw:Lcom/sec/vip/amschaton/ab;

    .line 914
    return-void
.end method

.method public setOnTouchTextArea(Lcom/sec/vip/amschaton/p;)V
    .locals 0

    .prologue
    .line 2839
    iput-object p1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ax:Lcom/sec/vip/amschaton/p;

    .line 2840
    return-void
.end method

.method public setPage(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2441
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    .line 2446
    :goto_0
    return-void

    .line 2444
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    int-to-byte v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/sec/amsoma/AMSLibs;->VipAMS_SetCurrentFrame(IB)Z

    .line 2445
    invoke-virtual {p0, v3, v3}, Lcom/sec/vip/amschaton/AMSDrawManager;->setEditMode(ZZ)V

    goto :goto_0
.end method

.method public setPlayMode(Z)V
    .locals 1

    .prologue
    .line 847
    if-eqz p1, :cond_0

    .line 848
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 852
    :goto_0
    return-void

    .line 850
    :cond_0
    invoke-virtual {p0, p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method public t()Z
    .locals 2

    .prologue
    .line 2514
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-nez v0, :cond_0

    .line 2515
    const/4 v0, 0x0

    .line 2517
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_IsAnimationEnable(I)Z

    move-result v0

    goto :goto_0
.end method

.method public u()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2524
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    .line 2527
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    if-eqz v0, :cond_0

    .line 2528
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->K:I

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/AMSLibs;->VipAMS_Close(I)V

    .line 2529
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->J:Lcom/sec/amsoma/AMSLibs;

    .line 2533
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 2534
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2535
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    .line 2537
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 2538
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2539
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    .line 2541
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 2542
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2543
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    .line 2547
    :cond_3
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->L:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    .line 2548
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->U:Lcom/sec/vip/amschaton/u;

    .line 2549
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    .line 2550
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ae:Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;

    .line 2551
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ai:Lcom/sec/vip/amschaton/u;

    .line 2552
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aj:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    .line 2553
    iput-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aw:Lcom/sec/vip/amschaton/ab;

    .line 2556
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 2557
    return-void
.end method

.method public v()V
    .locals 0

    .prologue
    .line 2560
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->I()V

    .line 2561
    return-void
.end method

.method public w()V
    .locals 2

    .prologue
    .line 2895
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aD:I

    packed-switch v0, :pswitch_data_0

    .line 2911
    :cond_0
    :goto_0
    return-void

    .line 2898
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    .line 2899
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2900
    :cond_1
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ad:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    if-eqz v0, :cond_0

    .line 2901
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->V:Lcom/sec/vip/amschaton/u;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/u;->k()Landroid/graphics/Rect;

    move-result-object v0

    .line 2902
    if-eqz v0, :cond_0

    .line 2903
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    .line 2904
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    .line 2905
    invoke-direct {p0, v1, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->b(II)Z

    goto :goto_0

    .line 2895
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public x()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 2916
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Z:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aI:Landroid/graphics/Bitmap;

    .line 2919
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 2920
    const/4 v8, 0x5

    .line 2921
    new-instance v0, Lcom/sec/vip/amschaton/ac;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    iget v6, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    invoke-direct/range {v0 .. v6}, Lcom/sec/vip/amschaton/ac;-><init>(IIIZII)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    .line 2922
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aO:Lcom/sec/vip/amschaton/o;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/ac;->a(Lcom/sec/vip/amschaton/o;)V

    .line 2923
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v0, v9}, Lcom/sec/vip/amschaton/ac;->b(Z)V

    .line 2924
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020299

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2925
    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/ac;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2927
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v0, v7}, Lcom/sec/vip/amschaton/ac;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2928
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aM:Lcom/sec/vip/amschaton/v;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/ac;->a(Lcom/sec/vip/amschaton/v;)V

    .line 2929
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v0, v8}, Lcom/sec/vip/amschaton/ac;->a(I)V

    .line 2931
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->ax:Lcom/sec/vip/amschaton/p;

    invoke-interface {v0, v9}, Lcom/sec/vip/amschaton/p;->a(Z)V

    .line 2933
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/ac;->b(Landroid/graphics/Bitmap;)V

    .line 2934
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    iget v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->T:F

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/ac;->a(F)V

    .line 2935
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v0, v9}, Lcom/sec/vip/amschaton/ac;->a(Z)V

    .line 2937
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    iget-object v1, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->W:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aI:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/sec/vip/amschaton/ac;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 2939
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->O:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 2951
    :goto_0
    return-void

    .line 2942
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->I:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b011e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2944
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->M:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v4, v0

    .line 2945
    iget v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->N:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v5, v0

    .line 2946
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    iget v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->Q:I

    iget v3, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->S:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/ac;->a(Ljava/lang/String;IIFF)Z

    .line 2948
    new-instance v0, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    iget-object v2, v2, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    invoke-direct {v0, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 2949
    iget-object v2, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    iget v5, v0, Landroid/graphics/RectF;->right:F

    iget v6, v0, Landroid/graphics/RectF;->bottom:F

    const/16 v8, 0xff

    move-object v7, v1

    invoke-virtual/range {v2 .. v8}, Lcom/sec/vip/amschaton/ac;->a(FFFFLjava/lang/String;I)Z

    .line 2950
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->v()V

    goto :goto_0
.end method

.method public y()V
    .locals 1

    .prologue
    .line 2969
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    if-nez v0, :cond_0

    .line 2970
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->z()V

    .line 2977
    :goto_0
    return-void

    .line 2973
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/ac;->a()V

    .line 2974
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->v()V

    .line 2975
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aE:Lcom/sec/vip/amschaton/ac;

    .line 2976
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSDrawManager;->z()V

    goto :goto_0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 2980
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aI:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 2981
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aI:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2983
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/AMSDrawManager;->aI:Landroid/graphics/Bitmap;

    .line 2984
    return-void
.end method
