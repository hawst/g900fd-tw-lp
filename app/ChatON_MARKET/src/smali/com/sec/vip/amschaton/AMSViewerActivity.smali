.class public Lcom/sec/vip/amschaton/AMSViewerActivity;
.super Lcom/sec/vip/amschaton/AMSActivity;
.source "AMSViewerActivity.java"


# instance fields
.field protected l:Lcom/sec/vip/amschaton/bq;

.field protected m:Landroid/widget/LinearLayout;

.field protected n:I

.field protected o:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSActivity;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/sec/vip/amschaton/AMSViewerActivity;->l:Lcom/sec/vip/amschaton/bq;

    .line 32
    iput-object v1, p0, Lcom/sec/vip/amschaton/AMSViewerActivity;->m:Landroid/widget/LinearLayout;

    .line 33
    iput v0, p0, Lcom/sec/vip/amschaton/AMSViewerActivity;->n:I

    .line 34
    iput-boolean v0, p0, Lcom/sec/vip/amschaton/AMSViewerActivity;->o:Z

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 38
    const-string v0, "[onCreate]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/AMSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "[onDestroy]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSViewerActivity;->l:Lcom/sec/vip/amschaton/bq;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/q;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/al;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSViewerActivity;->l:Lcom/sec/vip/amschaton/bq;

    iget-boolean v1, p0, Lcom/sec/vip/amschaton/AMSViewerActivity;->o:Z

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/bq;->b(Z)V

    .line 47
    iget-object v0, p0, Lcom/sec/vip/amschaton/AMSViewerActivity;->l:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->d()V

    .line 49
    :cond_0
    invoke-super {p0}, Lcom/sec/vip/amschaton/AMSActivity;->onDestroy()V

    .line 50
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 54
    const-string v0, "[onResume]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSViewerActivity;->a()V

    .line 56
    invoke-super {p0}, Lcom/sec/vip/amschaton/AMSActivity;->onResume()V

    .line 57
    return-void
.end method
