.class public Lcom/sec/vip/amschaton/a/f;
.super Lcom/sec/vip/amschaton/a/a;
.source "AMSRecentTemplateDatabase.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/a/a;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method private d(Ljava/lang/String;)J
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    const-wide/16 v8, -0x1

    .line 151
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    move-wide v0, v8

    .line 172
    :goto_0
    return-wide v0

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    move-wide v0, v8

    .line 155
    goto :goto_0

    .line 157
    :cond_1
    const-string v3, "ams_path=?"

    .line 158
    new-array v4, v10, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 159
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ams_recent_template"

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 160
    if-nez v0, :cond_2

    move-wide v0, v8

    .line 161
    goto :goto_0

    .line 164
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eq v1, v10, :cond_3

    move-wide v0, v8

    .line 165
    goto :goto_0

    .line 167
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_4

    move-wide v0, v8

    .line 168
    goto :goto_0

    .line 170
    :cond_4
    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 172
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 29
    iget-object v2, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v2, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-wide v0

    .line 32
    :cond_1
    iget-object v2, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v2

    if-nez v2, :cond_0

    .line 37
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 38
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/a/f;->c(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 44
    if-eqz v3, :cond_0

    .line 47
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 48
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    .line 49
    :goto_1
    if-eqz v2, :cond_2

    .line 50
    const-string v2, "_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 51
    invoke-virtual {p0, v4, v5}, Lcom/sec/vip/amschaton/a/f;->a(J)Z

    .line 52
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    goto :goto_1

    .line 55
    :cond_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 59
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/a/f;->c()Landroid/database/Cursor;

    move-result-object v2

    .line 60
    if-eqz v2, :cond_0

    .line 63
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 64
    const/16 v1, 0x28

    if-lt v0, v1, :cond_3

    .line 65
    const/16 v0, 0x27

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    .line 66
    :goto_2
    if-eqz v0, :cond_3

    .line 67
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 68
    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/a/f;->a(J)Z

    .line 69
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    goto :goto_2

    .line 74
    :cond_3
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 75
    const-string v1, "ams_path"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v1, "ams_index"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v1, "ams_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v1, "ams_date"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "ams_recent_template"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0}, Lcom/sec/vip/amschaton/a/a;->a()V

    .line 26
    return-void
.end method

.method public a(J)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 83
    iget-object v2, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v2, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v1

    .line 86
    :cond_1
    iget-object v2, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v2

    if-nez v2, :cond_0

    .line 89
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Delete called value__"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v2, "_id=?"

    .line 92
    new-array v3, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 94
    iget-object v4, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "ams_recent_template"

    invoke-virtual {v4, v5, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public b(J)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 117
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-object v5

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "ams_path"

    aput-object v0, v2, v6

    const-string v0, "ams_type"

    aput-object v0, v2, v4

    const/4 v0, 0x2

    const-string v1, "ams_date"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 124
    const-string v3, "_id=?"

    .line 125
    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 126
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ams_recent_template"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 127
    if-eqz v5, :cond_0

    .line 128
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    goto :goto_0
.end method

.method public b()Lcom/sec/vip/amschaton/a/f;
    .locals 1

    .prologue
    .line 19
    const-string v0, "ams_recent_template"

    invoke-super {p0, v0}, Lcom/sec/vip/amschaton/a/a;->a(Ljava/lang/String;)Lcom/sec/vip/amschaton/a/a;

    .line 20
    return-object p0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/a/f;->d(Ljava/lang/String;)J

    move-result-wide v0

    .line 99
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 102
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/a/f;->a(J)Z

    move-result v0

    goto :goto_0
.end method

.method public c()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 106
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_1

    .line 113
    :cond_0
    :goto_0
    return-object v3

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "ams_path"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "ams_type"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "ams_date"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 113
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ams_recent_template"

    const-string v7, "ams_date DESC"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 134
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-object v5

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "ams_path"

    aput-object v0, v2, v6

    const-string v0, "ams_type"

    aput-object v0, v2, v4

    const/4 v0, 0x2

    const-string v1, "ams_date"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 141
    const-string v3, "ams_path=?"

    .line 142
    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    .line 143
    iget-object v0, p0, Lcom/sec/vip/amschaton/a/f;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ams_recent_template"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 144
    if-eqz v5, :cond_0

    .line 145
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    goto :goto_0
.end method
