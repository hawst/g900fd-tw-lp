.class public Lcom/sec/vip/amschaton/fragment/ct;
.super Ljava/lang/Object;
.source "AMSStampSelectionFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1814
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/ct;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1812
    const/16 v0, 0x138a

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/ct;->c:I

    .line 1815
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/ct;->b:Ljava/util/Map;

    .line 1816
    iput p3, p0, Lcom/sec/vip/amschaton/fragment/ct;->c:I

    .line 1817
    return-void
.end method


# virtual methods
.method public a(I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1820
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ct;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1821
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ct;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1843
    :cond_0
    :goto_0
    return-object v0

    .line 1824
    :cond_1
    const/4 v0, 0x0

    .line 1825
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/ct;->c:I

    packed-switch v1, :pswitch_data_0

    .line 1840
    :goto_1
    if-eqz v0, :cond_0

    .line 1841
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/ct;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1827
    :pswitch_0
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/sec/vip/amschaton/al;->a(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1830
    :pswitch_1
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/sec/vip/amschaton/al;->e(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1833
    :pswitch_2
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/al;->g(I)I

    move-result v0

    .line 1834
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Lcom/sec/vip/amschaton/al;->d(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1837
    :pswitch_3
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/sec/vip/amschaton/al;->c(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1825
    :pswitch_data_0
    .packed-switch 0x1388
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(ILandroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 1847
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ct;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ct;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1851
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/fragment/cu;

    invoke-direct {v0, p0, p2}, Lcom/sec/vip/amschaton/fragment/cu;-><init>(Lcom/sec/vip/amschaton/fragment/ct;Landroid/widget/ImageView;)V

    .line 1858
    new-instance v1, Lcom/sec/vip/amschaton/fragment/cv;

    invoke-direct {v1, p0, p1, v0}, Lcom/sec/vip/amschaton/fragment/cv;-><init>(Lcom/sec/vip/amschaton/fragment/ct;ILandroid/os/Handler;)V

    .line 1866
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1867
    return-void
.end method
