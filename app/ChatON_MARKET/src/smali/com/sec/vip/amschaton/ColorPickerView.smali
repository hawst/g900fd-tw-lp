.class public Lcom/sec/vip/amschaton/ColorPickerView;
.super Landroid/view/View;
.source "ColorPickerView.java"


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:Lcom/sec/vip/amschaton/bh;

.field private a:[I

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/Bitmap;

.field private j:Landroid/graphics/Canvas;

.field private k:Landroid/graphics/Rect;

.field private l:I

.field private m:[F

.field private n:[I

.field private final o:I

.field private final p:I

.field private final q:I

.field private r:I

.field private s:F

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;III)V
    .locals 6

    .prologue
    const/16 v5, 0x1f

    const/16 v4, 0xa

    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 115
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 53
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->k:Landroid/graphics/Rect;

    .line 54
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->l:I

    .line 55
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    .line 56
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    .line 59
    iput v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->o:I

    .line 60
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->p:I

    .line 61
    iput v5, p0, Lcom/sec/vip/amschaton/ColorPickerView;->q:I

    .line 63
    iput v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->r:I

    .line 65
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->s:F

    .line 66
    iput v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->t:I

    .line 67
    iput v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->u:I

    .line 69
    const/16 v0, 0x23

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->v:I

    .line 70
    iput v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->w:I

    .line 71
    const/16 v0, 0xa0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->x:I

    .line 72
    const/16 v0, 0xa0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->y:I

    .line 73
    const/16 v0, 0x96

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    .line 74
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->A:I

    .line 75
    iput v5, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    .line 76
    iput v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->C:I

    .line 78
    const/16 v0, 0x186

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->D:I

    .line 79
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->E:I

    .line 80
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->F:I

    .line 81
    const/16 v0, 0x28

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->G:I

    .line 82
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    .line 83
    const/16 v0, 0x12

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->I:I

    .line 84
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->J:I

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->K:Lcom/sec/vip/amschaton/bh;

    .line 116
    invoke-virtual {p0, p2, p3}, Lcom/sec/vip/amschaton/ColorPickerView;->setWindowSize(II)V

    .line 117
    invoke-virtual {p0, p4}, Lcom/sec/vip/amschaton/ColorPickerView;->setColor(I)V

    .line 118
    return-void
.end method

.method private a(I)I
    .locals 4

    .prologue
    .line 176
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->s:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 177
    if-nez v0, :cond_0

    .line 178
    const/4 v0, 0x1

    .line 180
    :cond_0
    return v0
.end method

.method private a(IIF)I
    .locals 1

    .prologue
    .line 523
    sub-int v0, p2, p1

    int-to-float v0, v0

    mul-float/2addr v0, p3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method private a([IF)I
    .locals 7

    .prologue
    .line 537
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    .line 538
    const/4 v0, 0x0

    aget v0, p1, v0

    .line 556
    :goto_0
    return v0

    .line 540
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_1

    .line 541
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget v0, p1, v0

    goto :goto_0

    .line 544
    :cond_1
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, p2

    .line 545
    float-to-int v1, v0

    .line 546
    int-to-float v2, v1

    sub-float/2addr v0, v2

    .line 549
    aget v2, p1, v1

    .line 550
    add-int/lit8 v1, v1, 0x1

    aget v1, p1, v1

    .line 551
    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-direct {p0, v3, v4, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(IIF)I

    move-result v3

    .line 552
    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v4

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v5

    invoke-direct {p0, v4, v5, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(IIF)I

    move-result v4

    .line 553
    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v5

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v6

    invoke-direct {p0, v5, v6, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(IIF)I

    move-result v5

    .line 554
    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    invoke-direct {p0, v2, v1, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(IIF)I

    move-result v0

    .line 556
    invoke-static {v3, v4, v5, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    goto :goto_0
.end method

.method private a()V
    .locals 6

    .prologue
    .line 199
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->v:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->v:I

    .line 200
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->w:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->w:I

    .line 201
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->x:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->x:I

    .line 202
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->y:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->y:I

    .line 203
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    .line 204
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->A:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->A:I

    .line 205
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    .line 206
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->C:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->C:I

    .line 208
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->D:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->D:I

    .line 209
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->E:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->E:I

    .line 210
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->F:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->F:I

    .line 211
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->G:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->G:I

    .line 212
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    .line 213
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->I:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->I:I

    .line 214
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->J:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(I)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->J:I

    .line 216
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->k:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->D:I

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->w:I

    iget v3, p0, Lcom/sec/vip/amschaton/ColorPickerView;->D:I

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->F:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->G:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->w:I

    iget v5, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 218
    return-void
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 379
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->h:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 380
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, p2, -0x3

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->G:I

    add-int/lit8 v3, p2, 0x3

    invoke-direct {v0, v4, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 381
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->h:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 382
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, p2, -0x1

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->G:I

    add-int/lit8 v3, p2, 0x1

    invoke-direct {v0, v4, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 383
    return-void
.end method

.method private a(Landroid/graphics/Canvas;II)V
    .locals 5

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->g:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 367
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->F:I

    sub-int v1, p2, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->F:I

    sub-int v2, p3, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/vip/amschaton/ColorPickerView;->F:I

    add-int/2addr v3, p2

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->F:I

    add-int/2addr v4, p3

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 368
    return-void
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 224
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->a:[I

    .line 228
    new-instance v0, Landroid/graphics/SweepGradient;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->a:[I

    invoke-direct {v0, v1, v1, v2, v3}, Landroid/graphics/SweepGradient;-><init>(FF[I[F)V

    .line 229
    new-instance v7, Landroid/graphics/SweepGradient;

    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->a:[I

    invoke-direct {v7, v1, v1, v0, v3}, Landroid/graphics/SweepGradient;-><init>(FF[I[F)V

    .line 230
    new-instance v0, Landroid/graphics/RadialGradient;

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->x:I

    int-to-float v3, v2

    const/high16 v5, -0x1000000

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFFIILandroid/graphics/Shader$TileMode;)V

    .line 231
    new-instance v1, Landroid/graphics/ComposeShader;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v7, v0, v2}, Landroid/graphics/ComposeShader;-><init>(Landroid/graphics/Shader;Landroid/graphics/Shader;Landroid/graphics/PorterDuff$Mode;)V

    .line 232
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->b:Landroid/graphics/Paint;

    .line 233
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 234
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 235
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setDither(Z)V

    .line 236
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->c:Landroid/graphics/Paint;

    .line 237
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 240
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->c:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->A:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 241
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setDither(Z)V

    .line 244
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->d:Landroid/graphics/Paint;

    .line 245
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 246
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 247
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setDither(Z)V

    .line 250
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->e:Landroid/graphics/Paint;

    .line 251
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->e:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 252
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 253
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->e:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->C:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 254
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setDither(Z)V

    .line 256
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->f:Landroid/graphics/Paint;

    .line 257
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 258
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 259
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->f:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->E:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 260
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setDither(Z)V

    .line 262
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->G:I

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->i:Landroid/graphics/Bitmap;

    .line 263
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->i:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->j:Landroid/graphics/Canvas;

    .line 267
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->g:Landroid/graphics/Paint;

    .line 268
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 269
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->g:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->I:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 272
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->h:Landroid/graphics/Paint;

    .line 273
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 274
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->h:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->J:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 276
    return-void

    .line 224
    nop

    :array_0
    .array-data 4
        -0x10000
        -0xff01
        -0xffff01
        -0xff0001
        -0xff0100
        -0x100
        -0x10000
    .end array-data
.end method

.method private b(I)Z
    .locals 2

    .prologue
    .line 191
    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, p1, 0xff

    if-ne v0, v1, :cond_0

    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, p1, 0xff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 284
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ColorPickerView;->d()V

    .line 286
    const-wide v0, 0x401921fb54442d18L    # 6.283185307179586

    iget-object v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    aget v2, v2, v7

    float-to-double v2, v2

    const-wide v4, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    div-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    .line 287
    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    aget v1, v1, v8

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 288
    iget-object v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    float-to-double v5, v1

    mul-double/2addr v3, v5

    double-to-int v3, v3

    aput v3, v2, v7

    .line 289
    iget-object v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v3

    float-to-double v0, v1

    mul-double/2addr v0, v3

    double-to-int v0, v0

    aput v0, v2, v8

    .line 291
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    aget v2, v2, v9

    iget v3, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sub-int/2addr v1, v2

    aput v1, v0, v9

    .line 292
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 298
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 299
    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    aget v1, v1, v3

    aput v1, v0, v3

    .line 300
    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    aget v1, v1, v4

    aput v1, v0, v4

    .line 301
    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, v0, v2

    .line 302
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    .line 304
    new-array v1, v2, [I

    .line 305
    aput v0, v1, v3

    .line 306
    const/high16 v0, -0x1000000

    aput v0, v1, v4

    .line 307
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 308
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setDither(Z)V

    .line 309
    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setLevel(I)Z

    .line 310
    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->G:I

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 311
    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->j:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 312
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 323
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 324
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->t:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->u:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 326
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->v:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->w:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 329
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->x:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->y:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 331
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    neg-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 333
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    neg-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 335
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    neg-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 336
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    neg-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 338
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/vip/amschaton/ColorPickerView;->a(Landroid/graphics/Canvas;II)V

    .line 339
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->x:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->y:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 342
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->v:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->w:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 346
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->k:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->k:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 347
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->i:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v5, v5, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 348
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v5, v5, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 350
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a(Landroid/graphics/Canvas;I)V

    .line 352
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 353
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v6, -0x1

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v12, 0x2

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 394
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->t:I

    int-to-float v2, v2

    sub-float v7, v0, v2

    .line 395
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->u:I

    int-to-float v2, v2

    sub-float v8, v0, v2

    .line 400
    float-to-int v0, v8

    .line 401
    if-gez v0, :cond_0

    move v0, v1

    .line 404
    :cond_0
    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    if-le v0, v2, :cond_1

    .line 405
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    .line 410
    :cond_1
    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->v:I

    int-to-float v2, v2

    sub-float v2, v7, v2

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->x:I

    int-to-float v4, v4

    sub-float v9, v2, v4

    .line 411
    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->w:I

    int-to-float v2, v2

    sub-float v2, v8, v2

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->y:I

    int-to-float v4, v4

    sub-float v10, v2, v4

    .line 414
    mul-float v2, v9, v9

    mul-float v4, v10, v10

    add-float/2addr v2, v4

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v5, v4

    .line 421
    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->F:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    int-to-float v2, v2

    cmpg-float v2, v5, v2

    if-gtz v2, :cond_5

    move v2, v3

    .line 428
    :goto_0
    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    int-to-float v4, v4

    cmpg-float v4, v5, v4

    if-gtz v4, :cond_6

    move v4, v3

    .line 431
    :goto_1
    iget v11, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    int-to-float v11, v11

    cmpl-float v11, v5, v11

    if-lez v11, :cond_2

    .line 432
    iget v5, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    int-to-float v5, v5

    .line 436
    :cond_2
    iget-object v11, p0, Lcom/sec/vip/amschaton/ColorPickerView;->k:Landroid/graphics/Rect;

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-virtual {v11, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    .line 438
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 503
    :cond_3
    :goto_2
    :pswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->K:Lcom/sec/vip/amschaton/bh;

    if-eqz v0, :cond_4

    .line 504
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->K:Lcom/sec/vip/amschaton/bh;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    invoke-static {v1}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/vip/amschaton/bh;->b(I)Z

    .line 505
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->d:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    invoke-static {v1}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 506
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    neg-int v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    neg-int v2, v2

    iget v4, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    iget v5, p0, Lcom/sec/vip/amschaton/ColorPickerView;->B:I

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->invalidate(Landroid/graphics/Rect;)V

    .line 508
    :cond_4
    return v3

    :cond_5
    move v2, v1

    .line 421
    goto :goto_0

    :cond_6
    move v4, v1

    .line 428
    goto :goto_1

    .line 440
    :pswitch_1
    iput v6, p0, Lcom/sec/vip/amschaton/ColorPickerView;->r:I

    .line 442
    if-eqz v2, :cond_7

    .line 443
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->r:I

    goto :goto_2

    .line 444
    :cond_7
    if-eqz v7, :cond_3

    .line 445
    const/16 v0, 0x1f

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->r:I

    goto :goto_2

    .line 450
    :pswitch_2
    iget v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->r:I

    const/16 v7, 0x1e

    if-ne v2, v7, :cond_a

    .line 454
    float-to-double v7, v10

    float-to-double v9, v9

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v7

    double-to-float v2, v7

    .line 455
    float-to-double v7, v2

    const-wide v9, 0x401921fb54442d18L    # 6.283185307179586

    div-double/2addr v7, v9

    double-to-float v0, v7

    .line 456
    const/4 v7, 0x0

    cmpg-float v7, v0, v7

    if-gez v7, :cond_8

    .line 457
    add-float/2addr v0, v13

    .line 460
    :cond_8
    iget-object v7, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    float-to-double v10, v5

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    aput v8, v7, v1

    .line 461
    iget-object v7, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    float-to-double v10, v5

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v2, v8

    aput v2, v7, v3

    .line 466
    if-nez v4, :cond_b

    .line 467
    iget-object v2, p0, Lcom/sec/vip/amschaton/ColorPickerView;->a:[I

    invoke-direct {p0, v2, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->a([IF)I

    move-result v0

    .line 470
    :goto_3
    const/4 v2, 0x3

    new-array v2, v2, [F

    .line 471
    invoke-static {v0, v2}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 472
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    aget v6, v2, v1

    aput v6, v0, v1

    .line 474
    if-eqz v4, :cond_9

    .line 475
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    aget v1, v2, v3

    aput v1, v0, v3

    .line 480
    :goto_4
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ColorPickerView;->d()V

    .line 483
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/ColorPickerView;->invalidate()V

    goto/16 :goto_2

    .line 477
    :cond_9
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->z:I

    int-to-float v1, v1

    div-float v1, v5, v1

    aput v1, v0, v3

    goto :goto_4

    .line 487
    :cond_a
    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->r:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_3

    .line 489
    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    aget v1, v1, v12

    if-eq v1, v0, :cond_3

    .line 490
    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->n:[I

    aput v0, v1, v12

    .line 491
    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->H:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    sub-float v0, v13, v0

    .line 493
    iget-object v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    aput v0, v1, v12

    .line 498
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/ColorPickerView;->invalidate()V

    goto/16 :goto_2

    :cond_b
    move v0, v6

    goto :goto_3

    .line 438
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setColor(I)V
    .locals 3

    .prologue
    .line 150
    iput p1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->l:I

    .line 151
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 152
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/ColorPickerView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->m:[F

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 155
    :cond_0
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ColorPickerView;->c()V

    .line 156
    return-void
.end method

.method public setColorChangedListener(Lcom/sec/vip/amschaton/bh;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->K:Lcom/sec/vip/amschaton/bh;

    .line 166
    return-void
.end method

.method public setWindowSize(II)V
    .locals 3

    .prologue
    .line 129
    int-to-float v0, p1

    const/high16 v1, 0x43f00000    # 480.0f

    div-float/2addr v0, v1

    .line 130
    int-to-float v1, p2

    const/high16 v2, 0x43aa0000    # 340.0f

    div-float/2addr v1, v2

    .line 131
    cmpg-float v2, v0, v1

    if-gez v2, :cond_0

    .line 132
    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->s:F

    .line 136
    :goto_0
    div-int/lit8 v0, p1, 0x2

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->s:F

    const/high16 v2, 0x43700000    # 240.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->t:I

    .line 137
    div-int/lit8 v0, p2, 0x2

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->s:F

    const/high16 v2, 0x432a0000    # 170.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->u:I

    .line 138
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ColorPickerView;->a()V

    .line 139
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ColorPickerView;->b()V

    .line 140
    iget v0, p0, Lcom/sec/vip/amschaton/ColorPickerView;->l:I

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/ColorPickerView;->setColor(I)V

    .line 141
    return-void

    .line 134
    :cond_0
    iput v1, p0, Lcom/sec/vip/amschaton/ColorPickerView;->s:F

    goto :goto_0
.end method
