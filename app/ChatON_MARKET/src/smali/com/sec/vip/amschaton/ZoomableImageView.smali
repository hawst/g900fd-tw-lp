.class public Lcom/sec/vip/amschaton/ZoomableImageView;
.super Landroid/view/View;
.source "ZoomableImageView.java"


# instance fields
.field A:Landroid/graphics/PointF;

.field B:Landroid/graphics/PointF;

.field C:Z

.field D:F

.field E:F

.field F:F

.field G:F

.field H:F

.field private I:Ljava/lang/Runnable;

.field private J:Ljava/lang/Runnable;

.field private a:Landroid/graphics/Bitmap;

.field private b:I

.field private c:I

.field private d:Landroid/os/Handler;

.field private e:Landroid/view/GestureDetector;

.field private f:I

.field g:Landroid/graphics/Paint;

.field h:Landroid/graphics/Matrix;

.field i:Landroid/graphics/Matrix;

.field j:Landroid/graphics/PointF;

.field k:F

.field l:F

.field m:F

.field n:I

.field o:F

.field p:F

.field q:F

.field r:F

.field s:F

.field t:F

.field u:F

.field v:F

.field w:Z

.field x:F

.field y:F

.field z:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 126
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    .line 45
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    .line 46
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->i:Landroid/graphics/Matrix;

    .line 48
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->j:Landroid/graphics/PointF;

    .line 50
    iput v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 51
    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 52
    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 58
    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->n:I

    .line 70
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->v:F

    .line 71
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    .line 73
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->x:F

    .line 76
    iput v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->y:F

    .line 77
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->z:Landroid/graphics/PointF;

    .line 79
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->A:Landroid/graphics/PointF;

    .line 80
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->B:Landroid/graphics/PointF;

    .line 81
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->C:Z

    .line 83
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->d:Landroid/os/Handler;

    .line 86
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->E:F

    .line 88
    const/high16 v0, 0x41c80000    # 25.0f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->F:F

    .line 89
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->G:F

    .line 799
    new-instance v0, Lcom/sec/vip/amschaton/bt;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/bt;-><init>(Lcom/sec/vip/amschaton/ZoomableImageView;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->I:Ljava/lang/Runnable;

    .line 844
    new-instance v0, Lcom/sec/vip/amschaton/bu;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/bu;-><init>(Lcom/sec/vip/amschaton/ZoomableImageView;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->J:Ljava/lang/Runnable;

    .line 127
    invoke-virtual {p0, v4}, Lcom/sec/vip/amschaton/ZoomableImageView;->setFocusable(Z)V

    .line 128
    invoke-virtual {p0, v4}, Lcom/sec/vip/amschaton/ZoomableImageView;->setFocusableInTouchMode(Z)V

    .line 130
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->H:F

    .line 132
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->a()V

    .line 133
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/sec/vip/amschaton/bv;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/bv;-><init>(Lcom/sec/vip/amschaton/ZoomableImageView;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->e:Landroid/view/GestureDetector;

    .line 134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 145
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    .line 45
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    .line 46
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->i:Landroid/graphics/Matrix;

    .line 48
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->j:Landroid/graphics/PointF;

    .line 50
    iput v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 51
    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 52
    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 58
    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->n:I

    .line 70
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->v:F

    .line 71
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    .line 73
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->x:F

    .line 76
    iput v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->y:F

    .line 77
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->z:Landroid/graphics/PointF;

    .line 79
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->A:Landroid/graphics/PointF;

    .line 80
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->B:Landroid/graphics/PointF;

    .line 81
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->C:Z

    .line 83
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->d:Landroid/os/Handler;

    .line 86
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->E:F

    .line 88
    const/high16 v0, 0x41c80000    # 25.0f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->F:F

    .line 89
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->G:F

    .line 799
    new-instance v0, Lcom/sec/vip/amschaton/bt;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/bt;-><init>(Lcom/sec/vip/amschaton/ZoomableImageView;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->I:Ljava/lang/Runnable;

    .line 844
    new-instance v0, Lcom/sec/vip/amschaton/bu;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/bu;-><init>(Lcom/sec/vip/amschaton/ZoomableImageView;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->J:Ljava/lang/Runnable;

    .line 147
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->H:F

    .line 148
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->a()V

    .line 149
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/sec/vip/amschaton/bv;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/bv;-><init>(Lcom/sec/vip/amschaton/ZoomableImageView;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->e:Landroid/view/GestureDetector;

    .line 151
    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->f:I

    .line 152
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)F
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 563
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    .line 564
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v1, v2

    .line 565
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/ZoomableImageView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->I:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 158
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->g:Landroid/graphics/Paint;

    .line 160
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 161
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 162
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 163
    return-void
.end method

.method private a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 577
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    .line 578
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    add-float/2addr v1, v2

    .line 579
    div-float/2addr v0, v3

    div-float/2addr v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 580
    return-void
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/ZoomableImageView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 288
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    const/16 v2, 0x9

    new-array v2, v2, [F

    .line 293
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 295
    aget v3, v2, v0

    iput v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 297
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 298
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v3, v4

    .line 299
    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    .line 300
    iget v5, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    .line 301
    iget-object v6, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v6, v3, v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 302
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->invalidate()V

    .line 305
    :cond_2
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 306
    aget v3, v2, v0

    iput v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 307
    const/4 v3, 0x2

    aget v3, v2, v3

    iput v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 308
    const/4 v3, 0x5

    aget v2, v2, v3

    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 310
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 311
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    iget-object v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    sub-int/2addr v3, v4

    .line 316
    if-gez v2, :cond_8

    .line 317
    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    cmpl-float v4, v4, v7

    if-lez v4, :cond_7

    .line 318
    iput v7, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    move v2, v1

    .line 329
    :goto_1
    if-gez v3, :cond_a

    .line 330
    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    cmpl-float v4, v4, v7

    if-lez v4, :cond_9

    .line 331
    iput v7, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    move v0, v1

    .line 342
    :cond_3
    :goto_2
    if-nez v2, :cond_4

    if-eqz v0, :cond_0

    .line 343
    :cond_4
    if-nez v0, :cond_5

    .line 344
    iget v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    .line 346
    :cond_5
    if-nez v2, :cond_6

    .line 347
    iget v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    .line 351
    :cond_6
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    .line 353
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->I:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 354
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->I:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 320
    :cond_7
    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    int-to-float v5, v2

    cmpg-float v4, v4, v5

    if-gez v4, :cond_b

    .line 321
    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    move v2, v1

    .line 322
    goto :goto_1

    .line 325
    :cond_8
    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    move v2, v1

    .line 326
    goto :goto_1

    .line 333
    :cond_9
    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    int-to-float v5, v3

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 334
    int-to-float v0, v3

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    move v0, v1

    .line 335
    goto :goto_2

    .line 338
    :cond_a
    div-int/lit8 v0, v3, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    move v0, v1

    .line 339
    goto :goto_2

    :cond_b
    move v2, v0

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/ZoomableImageView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->J:Ljava/lang/Runnable;

    return-object v0
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v0, 0x1

    .line 359
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 363
    :cond_1
    const/16 v2, 0x9

    new-array v2, v2, [F

    .line 364
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 366
    aget v3, v2, v1

    iput v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 368
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 369
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v3, v4

    .line 370
    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    .line 371
    iget v5, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    .line 372
    iget-object v6, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v6, v3, v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 373
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->invalidate()V

    .line 376
    :cond_2
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 377
    aget v3, v2, v1

    iput v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 378
    const/4 v3, 0x2

    aget v3, v2, v3

    iput v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 379
    const/4 v3, 0x5

    aget v2, v2, v3

    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 381
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 382
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    iget-object v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    sub-int/2addr v3, v4

    .line 387
    if-gez v2, :cond_7

    .line 388
    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    cmpl-float v4, v4, v7

    if-lez v4, :cond_6

    .line 389
    iput v7, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    move v2, v0

    .line 400
    :goto_1
    if-gez v3, :cond_9

    .line 401
    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    cmpl-float v4, v4, v7

    if-lez v4, :cond_8

    .line 402
    iput v7, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    .line 413
    :goto_2
    if-nez v2, :cond_3

    if-eqz v0, :cond_0

    .line 414
    :cond_3
    if-nez v0, :cond_4

    .line 415
    iget v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    .line 417
    :cond_4
    if-nez v2, :cond_5

    .line 418
    iget v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    .line 420
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_0

    .line 391
    :cond_6
    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    int-to-float v5, v2

    cmpg-float v4, v4, v5

    if-gez v4, :cond_b

    .line 392
    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    move v2, v0

    .line 393
    goto :goto_1

    .line 396
    :cond_7
    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->o:F

    move v2, v0

    .line 397
    goto :goto_1

    .line 404
    :cond_8
    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    int-to-float v5, v3

    cmpg-float v4, v4, v5

    if-gez v4, :cond_a

    .line 405
    int-to-float v1, v3

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    goto :goto_2

    .line 409
    :cond_9
    div-int/lit8 v1, v3, 0x2

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->p:F

    goto :goto_2

    :cond_a
    move v0, v1

    goto :goto_2

    :cond_b
    move v2, v1

    goto :goto_1
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->b()V

    return-void
.end method


# virtual methods
.method public B()F
    .locals 1

    .prologue
    .line 588
    iget v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    return v0
.end method

.method public C()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 595
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    .line 596
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->i:Landroid/graphics/Matrix;

    .line 598
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 599
    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 600
    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 601
    return-void
.end method

.method public D()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 609
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public d(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    const/4 v9, 0x5

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 437
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->e:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 552
    :cond_0
    :goto_0
    return v7

    .line 441
    :cond_1
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    if-nez v0, :cond_0

    .line 446
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 447
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_0

    .line 551
    :cond_2
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->invalidate()V

    goto :goto_0

    .line 451
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->i:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 452
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->j:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 453
    iput v7, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->n:I

    goto :goto_1

    .line 458
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/ZoomableImageView;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->y:F

    .line 459
    iget v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->y:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_3

    .line 460
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->i:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 461
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->z:Landroid/graphics/PointF;

    invoke-direct {p0, v0, p1}, Lcom/sec/vip/amschaton/ZoomableImageView;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 462
    iput v8, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->n:I

    .line 464
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->B:Landroid/graphics/PointF;

    invoke-direct {p0, v0, p1}, Lcom/sec/vip/amschaton/ZoomableImageView;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 473
    :pswitch_3
    iput v6, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->n:I

    .line 475
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 476
    aget v1, v0, v8

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 477
    aget v1, v0, v9

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 478
    aget v0, v0, v6

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 482
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->b()V

    goto :goto_1

    .line 490
    :pswitch_4
    iput-boolean v6, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->C:Z

    .line 492
    iget v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->n:I

    if-ne v1, v7, :cond_4

    iget-boolean v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    if-nez v1, :cond_4

    .line 493
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->i:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 494
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->j:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    .line 495
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->j:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    .line 497
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 499
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 500
    aget v1, v0, v8

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 501
    aget v1, v0, v9

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 502
    aget v0, v0, v6

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    goto/16 :goto_1

    .line 503
    :cond_4
    iget v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->n:I

    if-ne v1, v8, :cond_2

    iget-boolean v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    if-nez v1, :cond_2

    .line 504
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/ZoomableImageView;->a(Landroid/view/MotionEvent;)F

    move-result v1

    .line 505
    cmpl-float v2, v1, v2

    if-lez v2, :cond_5

    .line 506
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->i:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 507
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->y:F

    div-float/2addr v1, v2

    .line 508
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 509
    aget v2, v0, v6

    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 511
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    mul-float/2addr v2, v1

    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_7

    .line 512
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->z:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->z:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 525
    :goto_2
    iput-boolean v7, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->C:Z

    .line 529
    :cond_5
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->C:Z

    if-nez v1, :cond_6

    .line 530
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->i:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 532
    :cond_6
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->A:Landroid/graphics/PointF;

    invoke-direct {p0, v1, p1}, Lcom/sec/vip/amschaton/ZoomableImageView;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 533
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->A:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->B:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    .line 534
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->A:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->B:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    .line 536
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 538
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 539
    aget v1, v0, v8

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 540
    aget v1, v0, v9

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 541
    aget v0, v0, v6

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 544
    invoke-direct {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->c()V

    goto/16 :goto_1

    .line 513
    :cond_7
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    mul-float/2addr v2, v1

    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->E:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_8

    .line 514
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v2, v3

    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->E:F

    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->z:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->z:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_2

    .line 516
    :cond_8
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->z:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->z:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_2

    .line 447
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 282
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 167
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 170
    iput p1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    .line 171
    iput p2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    .line 173
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 175
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 181
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->f:I

    if-nez v2, :cond_4

    .line 217
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    iget v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    if-le v2, v4, :cond_2

    .line 218
    if-lt v1, v3, :cond_1

    .line 219
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    int-to-float v2, v2

    int-to-float v1, v1

    div-float/2addr v2, v1

    .line 220
    int-to-float v1, v3

    mul-float/2addr v1, v2

    .line 221
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    float-to-int v1, v1

    sub-int v1, v3, v1

    div-int/lit8 v1, v1, 0x2

    .line 223
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 224
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    int-to-float v4, v1

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 252
    :goto_0
    int-to-float v1, v1

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 253
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 255
    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 256
    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    .line 273
    :goto_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->invalidate()V

    .line 275
    :cond_0
    return-void

    .line 226
    :cond_1
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    int-to-float v2, v2

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 227
    int-to-float v1, v1

    mul-float/2addr v1, v2

    .line 228
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    float-to-int v1, v1

    sub-int v1, v3, v1

    div-int/lit8 v1, v1, 0x2

    .line 230
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 231
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    int-to-float v4, v1

    invoke-virtual {v3, v5, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v6, v1

    move v1, v0

    move v0, v6

    .line 232
    goto :goto_0

    .line 234
    :cond_2
    if-lt v1, v3, :cond_3

    .line 235
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    int-to-float v2, v2

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 236
    int-to-float v1, v1

    mul-float/2addr v1, v2

    .line 237
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    float-to-int v1, v1

    sub-int v1, v3, v1

    div-int/lit8 v1, v1, 0x2

    .line 239
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 240
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    int-to-float v4, v1

    invoke-virtual {v3, v5, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v6, v1

    move v1, v0

    move v0, v6

    .line 241
    goto :goto_0

    .line 242
    :cond_3
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    int-to-float v2, v2

    int-to-float v1, v1

    div-float/2addr v2, v1

    .line 243
    int-to-float v1, v3

    mul-float/2addr v1, v2

    .line 244
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    float-to-int v1, v1

    sub-int v1, v3, v1

    div-int/lit8 v1, v1, 0x2

    .line 246
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 247
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    int-to-float v4, v1

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 258
    :cond_4
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    if-le v3, v2, :cond_5

    .line 259
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    sub-int v1, v2, v1

    div-int/lit8 v1, v1, 0x2

    .line 260
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    int-to-float v3, v1

    invoke-virtual {v2, v5, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v6, v1

    move v1, v0

    move v0, v6

    .line 266
    :goto_2
    int-to-float v1, v1

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 267
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 269
    iput v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 270
    iput v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    goto :goto_1

    .line 262
    :cond_5
    iget v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 263
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    int-to-float v3, v1

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 7

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 717
    if-eqz p1, :cond_5

    .line 718
    iput-object p1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    .line 720
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    .line 721
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    .line 723
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 724
    iget-object v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 730
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 732
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->f:I

    if-nez v2, :cond_1

    .line 733
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    if-le v0, v2, :cond_0

    .line 734
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float/2addr v2, v0

    .line 735
    int-to-float v0, v3

    mul-float/2addr v0, v2

    .line 736
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    float-to-int v0, v0

    sub-int v0, v3, v0

    div-int/lit8 v0, v0, 0x2

    .line 738
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 739
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    int-to-float v4, v0

    invoke-virtual {v3, v5, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 749
    :goto_0
    int-to-float v1, v1

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 750
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 752
    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 753
    iput v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    .line 781
    :goto_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/ZoomableImageView;->invalidate()V

    .line 785
    :goto_2
    return-void

    .line 741
    :cond_0
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    int-to-float v2, v2

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 742
    int-to-float v0, v0

    mul-float/2addr v0, v2

    .line 743
    iget v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    float-to-int v0, v0

    sub-int v0, v3, v0

    div-int/lit8 v0, v0, 0x2

    .line 745
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 746
    iget-object v3, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    int-to-float v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_0

    .line 755
    :cond_1
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    if-le v0, v2, :cond_3

    .line 757
    iget v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    if-le v3, v0, :cond_2

    move v0, v1

    .line 763
    :goto_3
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    int-to-float v3, v0

    invoke-virtual {v2, v5, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 774
    :goto_4
    int-to-float v1, v1

    iput v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->l:F

    .line 775
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->m:F

    .line 777
    iput v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 778
    iput v4, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->D:F

    goto :goto_1

    .line 760
    :cond_2
    iget v0, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    goto :goto_3

    .line 765
    :cond_3
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->b:I

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    .line 766
    iget v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    if-le v3, v2, :cond_4

    .line 771
    :goto_5
    iget-object v2, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    int-to-float v3, v0

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_4

    .line 769
    :cond_4
    iget v1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->c:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    goto :goto_5

    .line 783
    :cond_5
    const-string v0, "bitmap is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public setDefaultScale(I)V
    .locals 0

    .prologue
    .line 116
    iput p1, p0, Lcom/sec/vip/amschaton/ZoomableImageView;->f:I

    .line 117
    return-void
.end method
