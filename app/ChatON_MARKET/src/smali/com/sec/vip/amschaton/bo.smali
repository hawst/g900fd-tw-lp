.class Lcom/sec/vip/amschaton/bo;
.super Ljava/lang/Object;
.source "TextInputLayout.java"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/bl;

.field private b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/sec/vip/amschaton/bl;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/vip/amschaton/bo;->a:Lcom/sec/vip/amschaton/bl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/bo;->b:Ljava/lang/String;

    .line 354
    iput-object p2, p0, Lcom/sec/vip/amschaton/bo;->b:Ljava/lang/String;

    .line 355
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/vip/amschaton/bl;Ljava/lang/String;Lcom/sec/vip/amschaton/bm;)V
    .locals 0

    .prologue
    .line 349
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/bo;-><init>(Lcom/sec/vip/amschaton/bl;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 359
    .line 360
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 361
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 362
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    move v3, v2

    move v0, v2

    .line 365
    :goto_0
    if-ge v3, v6, :cond_0

    .line 366
    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 367
    const/16 v8, 0xa

    if-eq v7, v8, :cond_2

    .line 368
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 373
    :goto_1
    iget-object v7, p0, Lcom/sec/vip/amschaton/bo;->a:Lcom/sec/vip/amschaton/bl;

    invoke-static {v7}, Lcom/sec/vip/amschaton/bl;->b(Lcom/sec/vip/amschaton/bl;)I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    if-le v3, v7, :cond_3

    .line 378
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 379
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, p2

    .line 382
    iget-object v5, p0, Lcom/sec/vip/amschaton/bo;->b:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 383
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    invoke-virtual {v4, v2, v5}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 384
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    .line 385
    :goto_2
    if-ge v2, v5, :cond_1

    .line 386
    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 387
    iget-object v7, p0, Lcom/sec/vip/amschaton/bo;->b:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_4

    .line 388
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 393
    :goto_3
    iget-object v6, p0, Lcom/sec/vip/amschaton/bo;->a:Lcom/sec/vip/amschaton/bl;

    invoke-static {v6}, Lcom/sec/vip/amschaton/bl;->b(Lcom/sec/vip/amschaton/bl;)I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    if-le v2, v6, :cond_5

    .line 399
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 400
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, p2

    .line 402
    if-eqz v0, :cond_6

    move-object v0, v1

    .line 405
    :goto_4
    return-object v0

    :cond_2
    move v0, v1

    .line 370
    goto :goto_1

    .line 365
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 390
    goto :goto_3

    .line 385
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 405
    :cond_6
    const/4 v0, 0x0

    goto :goto_4
.end method
