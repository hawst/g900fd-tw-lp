.class public abstract Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;
.super Lcom/sec/vip/amschaton/fragment/AMSFragment;
.source "AMSViewerFragment.java"


# instance fields
.field protected A:Landroid/widget/ImageView;

.field protected B:Landroid/view/View;

.field protected C:Landroid/view/View;

.field protected D:Landroid/view/View;

.field protected E:Landroid/view/View;

.field protected F:Landroid/widget/RelativeLayout;

.field protected G:Landroid/widget/LinearLayout;

.field protected H:Landroid/widget/LinearLayout;

.field protected I:Landroid/widget/LinearLayout;

.field protected J:Landroid/widget/RelativeLayout;

.field protected K:Landroid/widget/TextView;

.field protected L:Landroid/widget/ImageView;

.field protected M:Z

.field protected N:Z

.field protected O:Z

.field protected P:Landroid/widget/RelativeLayout;

.field protected Q:Landroid/view/View$OnClickListener;

.field protected R:Landroid/view/View$OnClickListener;

.field private S:Z

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Landroid/os/Handler;

.field private Y:Lcom/sec/vip/amschaton/bs;

.field private Z:Ljava/lang/Runnable;

.field protected a:I

.field protected b:Lcom/sec/vip/amschaton/bq;

.field protected c:Landroid/widget/LinearLayout;

.field protected m:Ljava/lang/String;

.field protected n:I

.field protected o:I

.field protected p:Z

.field protected q:Z

.field protected r:J

.field protected s:Z

.field protected t:J

.field protected u:Z

.field protected v:Z

.field protected w:Z

.field protected x:Ljava/lang/String;

.field protected y:Ljava/text/DateFormat;

.field protected z:Landroid/widget/SeekBar;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSFragment;-><init>()V

    .line 77
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    .line 78
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    .line 95
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->S:Z

    .line 97
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->T:Z

    .line 100
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->z:Landroid/widget/SeekBar;

    .line 107
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->F:Landroid/widget/RelativeLayout;

    .line 108
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->G:Landroid/widget/LinearLayout;

    .line 109
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->H:Landroid/widget/LinearLayout;

    .line 110
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->I:Landroid/widget/LinearLayout;

    .line 111
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->J:Landroid/widget/RelativeLayout;

    .line 112
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->K:Landroid/widget/TextView;

    .line 113
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->L:Landroid/widget/ImageView;

    .line 115
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->U:Z

    .line 116
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->M:Z

    .line 118
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->N:Z

    .line 119
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->V:Z

    .line 120
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->W:Z

    .line 122
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->O:Z

    .line 124
    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->P:Landroid/widget/RelativeLayout;

    .line 126
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->X:Landroid/os/Handler;

    .line 675
    new-instance v0, Lcom/sec/vip/amschaton/fragment/dk;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/dk;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->Y:Lcom/sec/vip/amschaton/bs;

    .line 823
    new-instance v0, Lcom/sec/vip/amschaton/fragment/dr;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/dr;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->Q:Landroid/view/View$OnClickListener;

    .line 900
    new-instance v0, Lcom/sec/vip/amschaton/fragment/de;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/de;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->Z:Ljava/lang/Runnable;

    .line 909
    new-instance v0, Lcom/sec/vip/amschaton/fragment/df;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/df;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->R:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1238
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd_HHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1239
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1240
    return-object v0
.end method

.method private a(Lcom/sec/vip/amschaton/bj;)V
    .locals 3

    .prologue
    .line 1279
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Lcom/sec/vip/amschaton/bj;->b()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1280
    invoke-virtual {p1}, Lcom/sec/vip/amschaton/bj;->b()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/sec/vip/amschaton/fragment/dj;

    invoke-direct {v1, p0, p1}, Lcom/sec/vip/amschaton/fragment/dj;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Lcom/sec/vip/amschaton/bj;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1288
    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->g(Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->V:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1100
    const/4 v0, 0x1

    .line 1104
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1105
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v5, 0x0

    invoke-direct {v2, p2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1107
    const/16 v3, 0x400

    :try_start_2
    new-array v3, v3, [B

    .line 1110
    :goto_0
    invoke-virtual {v4, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    .line 1111
    if-gtz v5, :cond_2

    .line 1116
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1126
    if-eqz v4, :cond_0

    .line 1128
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8

    .line 1134
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 1136
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    .line 1145
    :cond_1
    :goto_2
    const v1, 0x7f0b00f4

    invoke-virtual {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(I)V

    .line 1146
    return v0

    .line 1114
    :cond_2
    const/4 v6, 0x0

    :try_start_5
    invoke-virtual {v2, v3, v6, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_b
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_0

    .line 1117
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 1118
    :goto_3
    :try_start_6
    const-string v4, "FileNotFoundException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 1126
    if-eqz v3, :cond_3

    .line 1128
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 1134
    :cond_3
    :goto_4
    if-eqz v2, :cond_4

    .line 1136
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :cond_4
    :goto_5
    move v0, v1

    .line 1142
    goto :goto_2

    .line 1121
    :catch_1
    move-exception v0

    move-object v2, v3

    move-object v4, v3

    .line 1122
    :goto_6
    :try_start_9
    const-string v3, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 1126
    if-eqz v4, :cond_5

    .line 1128
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 1134
    :cond_5
    :goto_7
    if-eqz v2, :cond_6

    .line 1136
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    :cond_6
    :goto_8
    move v0, v1

    .line 1142
    goto :goto_2

    .line 1126
    :catchall_0
    move-exception v0

    move-object v2, v3

    move-object v4, v3

    :goto_9
    if-eqz v4, :cond_7

    .line 1128
    :try_start_c
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2

    .line 1134
    :cond_7
    :goto_a
    if-eqz v2, :cond_8

    .line 1136
    :try_start_d
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3

    .line 1126
    :cond_8
    :goto_b
    throw v0

    .line 1129
    :catch_2
    move-exception v1

    .line 1130
    const-string v3, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 1137
    :catch_3
    move-exception v1

    .line 1138
    const-string v2, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 1129
    :catch_4
    move-exception v0

    .line 1130
    const-string v3, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1137
    :catch_5
    move-exception v0

    .line 1138
    const-string v2, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1129
    :catch_6
    move-exception v0

    .line 1130
    const-string v3, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 1137
    :catch_7
    move-exception v0

    .line 1138
    const-string v2, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 1129
    :catch_8
    move-exception v1

    .line 1130
    const-string v3, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 1137
    :catch_9
    move-exception v1

    .line 1138
    const-string v2, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 1126
    :catchall_1
    move-exception v0

    move-object v2, v3

    goto/16 :goto_9

    :catchall_2
    move-exception v0

    goto/16 :goto_9

    :catchall_3
    move-exception v0

    move-object v4, v3

    goto/16 :goto_9

    .line 1121
    :catch_a
    move-exception v0

    move-object v2, v3

    goto/16 :goto_6

    :catch_b
    move-exception v0

    goto/16 :goto_6

    .line 1117
    :catch_c
    move-exception v0

    move-object v2, v3

    goto/16 :goto_3

    :catch_d
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    goto/16 :goto_3
.end method

.method private b(I)V
    .locals 0

    .prologue
    .line 660
    return-void
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->e(Z)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 275
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-nez v0, :cond_1

    .line 276
    const-string v0, "mViewer is null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0, p1, v3}, Lcom/sec/vip/amschaton/bq;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->M:Z

    .line 282
    instance-of v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;

    if-eqz v0, :cond_2

    .line 283
    const v0, 0x7f070126

    invoke-virtual {p0, v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(IZ)V

    .line 285
    :cond_2
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->M:Z

    if-eqz v0, :cond_4

    .line 286
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/AMSDrawManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->setBitmapForAMS(Ljava/lang/String;)V

    .line 291
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->f(Z)V

    .line 293
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->z:Landroid/widget/SeekBar;

    sget v1, Lcom/sec/vip/amschaton/bq;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 294
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->z:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 296
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->p:Z

    if-eqz v0, :cond_3

    .line 297
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->k()V

    .line 298
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b(Z)V

    .line 299
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->N:Z

    if-nez v0, :cond_0

    .line 300
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Z)V

    goto :goto_0

    .line 304
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/bq;->a(Z)V

    .line 306
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a()V

    .line 307
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->e(Z)V

    .line 309
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->N:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->V:Z

    if-nez v0, :cond_0

    .line 310
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->d(Z)V

    goto :goto_0

    .line 314
    :cond_4
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->f(Z)V

    .line 315
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/AMSDrawManager;->setBitmapForAMS(Ljava/lang/String;)V

    .line 319
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->l()V

    .line 321
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->N:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->V:Z

    if-nez v0, :cond_0

    .line 322
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->d(Z)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->W:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->j()V

    return-void
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->d(Z)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1043
    if-nez p1, :cond_0

    .line 1051
    :goto_0
    return-void

    .line 1046
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/a/f;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/a/f;-><init>(Landroid/content/Context;)V

    .line 1047
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/f;->b()Lcom/sec/vip/amschaton/a/f;

    .line 1048
    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/a/f;->b(Ljava/lang/String;)Z

    .line 1049
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/a/f;->a()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->k()V

    return-void
.end method

.method private d(Z)V
    .locals 0

    .prologue
    .line 414
    return-void
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)Z
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->W:Z

    return p1
.end method

.method private d(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1292
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-nez v0, :cond_0

    move v0, v1

    .line 1302
    :goto_0
    return v0

    .line 1295
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/AMSDrawManager;

    .line 1296
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1297
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1298
    invoke-virtual {v0, p1, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Ljava/lang/String;Landroid/util/DisplayMetrics;)Z

    move-result v0

    .line 1299
    if-nez v0, :cond_1

    move v0, v1

    .line 1300
    goto :goto_0

    .line 1302
    :cond_1
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0x3e9

    invoke-virtual {v0, v1, p1, v2}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->X:Landroid/os/Handler;

    return-object v0
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 487
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->P:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 489
    return-void
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)Z
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->U:Z

    return p1
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->Z:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->i(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f(Z)V
    .locals 2

    .prologue
    .line 522
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->Q:Landroid/view/View$OnClickListener;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 523
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->g(Z)V

    .line 524
    return-void

    .line 522
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g(Z)V
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 530
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-nez v0, :cond_0

    .line 534
    const-string v0, "mViewer is null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    :goto_0
    return-void

    .line 537
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->Y:Lcom/sec/vip/amschaton/bs;

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/bq;->a(Lcom/sec/vip/amschaton/bs;)V

    .line 540
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    new-instance v1, Lcom/sec/vip/amschaton/fragment/dd;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/dd;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/bq;->a(Lcom/sec/vip/amschaton/br;)V

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->o()V

    return-void
.end method

.method private h(Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 595
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 596
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v0

    .line 597
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    .line 606
    if-ge v0, v1, :cond_0

    :goto_0
    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a:I

    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[createViewer] mViewerSize = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    new-instance v0, Lcom/sec/vip/amschaton/bq;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/bq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    .line 612
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a:I

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/bq;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 613
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    move v0, v2

    .line 619
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 606
    goto :goto_0

    .line 616
    :cond_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 617
    goto :goto_1

    .line 619
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private i(Z)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1157
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    .line 1159
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1163
    const v1, 0x7f0b00ec

    invoke-virtual {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(I)V

    .line 1179
    :goto_0
    return-object v0

    .line 1166
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1167
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/32 v5, 0x100000

    cmp-long v3, v3, v5

    if-ltz v3, :cond_1

    if-nez p1, :cond_2

    :cond_1
    move-object v0, v1

    .line 1169
    goto :goto_0

    .line 1171
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1172
    const-string v1, "%s\n(%d/%d)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b00fb

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x2

    const/high16 v4, 0x100000

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1173
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v2

    const v3, 0x7f0b0100

    invoke-virtual {v2, v3}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/common/a/a;->b(Ljava/lang/CharSequence;)Lcom/sec/common/a/a;

    move-result-object v1

    const v2, 0x7f0b00fc

    new-instance v3, Lcom/sec/vip/amschaton/fragment/di;

    invoke-direct {v3, p0}, Lcom/sec/vip/amschaton/fragment/di;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/common/a/d;->show()V

    goto :goto_0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 665
    return-void
.end method

.method static synthetic i(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->p()V

    return-void
.end method

.method private j()V
    .locals 0

    .prologue
    .line 670
    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    const v4, 0x7f07005f

    .line 699
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-nez v0, :cond_0

    .line 700
    const-string v0, "mViewer is null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    :goto_0
    return-void

    .line 705
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->s()Z

    move-result v0

    .line 706
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->U:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 707
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->W:Z

    .line 708
    new-instance v1, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0c0102

    invoke-direct {v1, v0, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 709
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 712
    const v0, 0x7f030010

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(I)V

    .line 713
    const v0, 0x7f07005e

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 715
    new-instance v2, Lcom/sec/vip/amschaton/fragment/dl;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/dl;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 720
    new-instance v2, Lcom/sec/vip/amschaton/fragment/dm;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/dm;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 727
    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/vip/amschaton/fragment/dn;

    invoke-direct {v3, p0}, Lcom/sec/vip/amschaton/fragment/dn;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 733
    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/sec/vip/amschaton/fragment/do;

    invoke-direct {v3, p0, v0}, Lcom/sec/vip/amschaton/fragment/do;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 763
    const v0, 0x7f070060

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 764
    new-instance v2, Lcom/sec/vip/amschaton/fragment/dp;

    invoke-direct {v2, p0, v1}, Lcom/sec/vip/amschaton/fragment/dp;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Landroid/app/Dialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 771
    new-instance v0, Lcom/sec/vip/amschaton/fragment/dq;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/fragment/dq;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 780
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 782
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->b()V

    .line 784
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a()V

    goto/16 :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 807
    instance-of v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;

    if-eqz v0, :cond_0

    .line 808
    const v0, 0x7f070126

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(IZ)V

    .line 809
    const v0, 0x7f070283

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(IZ)V

    .line 810
    const v0, 0x7f070284

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(IZ)V

    .line 815
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 816
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->B:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020054

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 817
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->P:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 818
    return-void
.end method

.method private m()Z
    .locals 5

    .prologue
    .line 1059
    new-instance v0, Lcom/sec/vip/amschaton/bj;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0c0102

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/vip/amschaton/bj;-><init>(Landroid/content/Context;II)V

    .line 1060
    const-string v1, "A"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string v4, ""

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1061
    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/bj;->a(Ljava/lang/String;)V

    .line 1062
    const-string v1, "\\/:*?\"<>|"

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/bj;->b(Ljava/lang/String;)V

    .line 1063
    new-instance v1, Lcom/sec/vip/amschaton/fragment/dh;

    invoke-direct {v1, p0, v0}, Lcom/sec/vip/amschaton/fragment/dh;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;Lcom/sec/vip/amschaton/bj;)V

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/bj;->a(Lcom/sec/vip/amschaton/bi;)V

    .line 1085
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bj;->show()V

    .line 1086
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Lcom/sec/vip/amschaton/bj;)V

    .line 1087
    const/4 v0, 0x1

    return v0
.end method

.method private n()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v7, 0x1e0

    .line 1186
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->h()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/AMSDrawManager;

    .line 1189
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/vip/amschaton/AMSActivity;->d:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "I"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-string v6, ".jpg"

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1191
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1192
    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1193
    if-eqz v0, :cond_2

    .line 1194
    const/4 v3, 0x1

    invoke-static {v0, v7, v7, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1197
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1198
    :try_start_1
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x50

    invoke-virtual {v0, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1199
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1200
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 1201
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1207
    if-eqz v3, :cond_0

    .line 1209
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    :goto_0
    move-object v0, v1

    .line 1223
    :goto_1
    return-object v0

    .line 1202
    :catch_0
    move-exception v0

    move-object v3, v2

    .line 1203
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1207
    if-eqz v3, :cond_0

    .line 1209
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 1211
    :catch_1
    move-exception v0

    .line 1212
    :goto_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1222
    :cond_0
    :goto_4
    const v0, 0x7f0b00ec

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(I)V

    move-object v0, v2

    .line 1223
    goto :goto_1

    .line 1204
    :catch_2
    move-exception v0

    move-object v3, v2

    .line 1205
    :goto_5
    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1207
    if-eqz v3, :cond_0

    .line 1209
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 1211
    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v1

    .line 1212
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1207
    :cond_1
    throw v0

    .line 1217
    :cond_2
    const-string v0, "Bitmap main is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1207
    :catchall_0
    move-exception v0

    move-object v3, v2

    :goto_6
    if-eqz v3, :cond_1

    .line 1209
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 1211
    :catch_5
    move-exception v0

    goto :goto_3

    .line 1207
    :catchall_1
    move-exception v0

    goto :goto_6

    .line 1204
    :catch_6
    move-exception v0

    goto :goto_5

    .line 1202
    :catch_7
    move-exception v0

    goto :goto_2
.end method

.method private o()V
    .locals 4

    .prologue
    .line 1246
    const-string v0, "A"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v3, ""

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1247
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/vip/amschaton/AMSActivity;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1249
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1252
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1253
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1254
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1257
    :cond_0
    const v0, 0x7f0b00ec

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(I)V

    .line 1261
    :goto_0
    return-void

    .line 1260
    :cond_1
    const v0, 0x7f0b00f4

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(I)V

    goto :goto_0
.end method

.method private p()V
    .locals 3

    .prologue
    .line 1264
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/vip/amschaton/AMSFileListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1265
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1266
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1267
    const-string v1, "ACTION"

    const/16 v2, 0x7d6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1268
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->startActivity(Landroid/content/Intent;)V

    .line 1269
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1270
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-nez v0, :cond_1

    .line 332
    const-string v0, "mViewer is null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->g()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 350
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->B:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020063

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 361
    :goto_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 362
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    goto :goto_0

    .line 357
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->B:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020054

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method protected a(IZ)V
    .locals 1

    .prologue
    .line 247
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->N:Z

    if-eqz v0, :cond_0

    .line 248
    sparse-switch p1, :sswitch_data_0

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 250
    :sswitch_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->C:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 253
    :sswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->D:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 256
    :sswitch_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->E:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 248
    :sswitch_data_0
    .sparse-switch
        0x7f070126 -> :sswitch_1
        0x7f070283 -> :sswitch_0
        0x7f070284 -> :sswitch_2
    .end sparse-switch
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 795
    const/16 v0, 0x7d2

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->o:I

    .line 796
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 797
    sget-object v0, Lcom/sec/vip/amschaton/AMSActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 798
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->o:I

    .line 803
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->o:I

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 804
    return-void

    .line 799
    :cond_1
    sget-object v0, Lcom/sec/vip/amschaton/AMSActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 800
    const/16 v0, 0x7d1

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->o:I

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 0

    .prologue
    .line 459
    return-void
.end method

.method protected abstract a(Landroid/view/View;)Z
.end method

.method protected a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 926
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c(Ljava/lang/String;)V

    .line 934
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/q;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 936
    const/4 v0, 0x1

    .line 939
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Z)V
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->P:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 519
    return-void
.end method

.method protected b()Z
    .locals 4

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 627
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    .line 629
    :cond_0
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    .line 630
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a:I

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a:I

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 633
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-nez v0, :cond_1

    .line 634
    const/4 v0, 0x0

    .line 639
    :goto_0
    return v0

    .line 638
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->c:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/bq;->h()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 639
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected c(Z)V
    .locals 2

    .prologue
    .line 569
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->z:Landroid/widget/SeekBar;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 570
    return-void

    .line 569
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected e()V
    .locals 2

    .prologue
    .line 686
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-nez v0, :cond_0

    .line 687
    const-string v0, "mViewer is null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    :goto_0
    return-void

    .line 690
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->c()V

    .line 692
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a()V

    goto :goto_0
.end method

.method protected f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 946
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/vip/amschaton/AMSActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 948
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "targetPath     : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTargetFilePath: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 952
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 953
    const v0, 0x7f0b00f5

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(I)V

    .line 965
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :cond_1
    :goto_1
    return-object v0

    .line 954
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 955
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m()Z

    goto :goto_0

    .line 957
    :cond_3
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 958
    if-eqz v1, :cond_0

    .line 959
    sget-boolean v2, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v2, :cond_1

    .line 960
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Saved     : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected g()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 972
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-nez v0, :cond_0

    .line 973
    const-string v0, "mViewer is null!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    :goto_0
    return-void

    .line 976
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 977
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->S:Z

    goto :goto_0

    .line 980
    :cond_1
    const/4 v0, 0x0

    .line 982
    instance-of v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;

    if-eqz v1, :cond_3

    .line 988
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0, v4}, Lcom/sec/vip/amschaton/bq;->a(Z)V

    .line 989
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    .line 1033
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    .line 1034
    iput-boolean v5, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->S:Z

    goto :goto_0

    .line 990
    :cond_3
    instance-of v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;

    if-eqz v1, :cond_2

    .line 992
    new-instance v1, Lcom/sec/vip/amschaton/ad;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/vip/amschaton/ad;-><init>(Landroid/content/Context;)V

    .line 993
    const v2, 0x7f0b0129

    invoke-virtual {v1, v5, v2, v4}, Lcom/sec/vip/amschaton/ad;->a(IIZ)Z

    .line 994
    const v2, 0x7f0b012a

    invoke-virtual {v1, v4, v2, v4}, Lcom/sec/vip/amschaton/ad;->a(IIZ)Z

    .line 995
    const/4 v2, 0x2

    const v3, 0x7f0b0130

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/vip/amschaton/ad;->a(IIZ)Z

    .line 996
    new-instance v2, Lcom/sec/vip/amschaton/fragment/dg;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/dg;-><init>(Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/vip/amschaton/ad;->a(Lcom/sec/vip/amschaton/ak;)V

    .line 1030
    invoke-virtual {v1}, Lcom/sec/vip/amschaton/ad;->b()V

    goto :goto_1

    .line 1038
    :cond_4
    const-string v1, ""

    invoke-virtual {p0, v0, v1, v4}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 136
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreate(Landroid/os/Bundle;)V

    .line 138
    const-string v0, "[onCreate]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSFragment;->onCreate(Landroid/os/Bundle;)V

    .line 144
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 146
    const-string v1, "AMS_FILE_PATH"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    .line 147
    const-string v1, "VIEWER_MODE"

    const/16 v2, 0x3ea

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->n:I

    .line 148
    const-string v1, "AMS_FILE_TYPE"

    const/16 v2, 0x7d1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->o:I

    .line 150
    const-string v1, "AMS_DIRECT_PLAY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->p:Z

    .line 152
    const-string v1, "AMS_SAVE_FLAG"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->q:Z

    .line 153
    const-string v1, "AMS_DOWNLOAD_PREVIEW"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->s:Z

    .line 155
    const-string v1, "AMS_FILE_SIZE"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->r:J

    .line 156
    const-string v1, "AMS_EXPIRATION_DATE"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->t:J

    .line 157
    const-string v1, "AMS_IS_FAILED_ITEM"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->u:Z

    .line 159
    const-string v1, "AMS_FROM_DOWNLOADS"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->w:Z

    .line 160
    const-string v1, "AMS_ITEM_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->x:Ljava/lang/String;

    .line 166
    :goto_0
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->v:Z

    .line 170
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->y:Ljava/text/DateFormat;

    .line 201
    return-void

    .line 162
    :cond_0
    const-string v0, "[onCreate] bundle is NULL!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 205
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 208
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->N:Z

    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->h(Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    const/4 v0, 0x0

    .line 243
    :goto_0
    return-object v0

    .line 214
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->a(Landroid/view/View;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->T:Z

    .line 217
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->h()V

    .line 220
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->n:I

    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b(I)V

    .line 222
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->i()V

    .line 241
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->m:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
