.class public Lcom/sec/vip/amschaton/q;
.super Ljava/lang/Object;
.source "AMSFileManager.java"


# static fields
.field private static final e:Lcom/sec/vip/amschaton/q;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:I

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/sec/vip/amschaton/q;

    invoke-direct {v0}, Lcom/sec/vip/amschaton/q;-><init>()V

    sput-object v0, Lcom/sec/vip/amschaton/q;->e:Lcom/sec/vip/amschaton/q;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-string v0, "basic_ams_filename_list.txt"

    iput-object v0, p0, Lcom/sec/vip/amschaton/q;->d:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    .line 55
    iput v1, p0, Lcom/sec/vip/amschaton/q;->g:I

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    .line 57
    iput v1, p0, Lcom/sec/vip/amschaton/q;->i:I

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    .line 60
    iput v1, p0, Lcom/sec/vip/amschaton/q;->k:I

    .line 61
    iput v1, p0, Lcom/sec/vip/amschaton/q;->l:I

    .line 64
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/q;->m:Z

    .line 69
    return-void
.end method

.method public static a()Lcom/sec/vip/amschaton/q;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sec/vip/amschaton/q;->e:Lcom/sec/vip/amschaton/q;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/q;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->b:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 704
    if-eqz p1, :cond_0

    .line 705
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 710
    :cond_0
    :goto_0
    return-void

    .line 707
    :catch_0
    move-exception v0

    .line 708
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 721
    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 724
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 725
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 730
    return-void

    .line 728
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method private a(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 672
    if-eqz p1, :cond_0

    .line 673
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 678
    :cond_0
    :goto_0
    return-void

    .line 675
    :catch_0
    move-exception v0

    .line 676
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Landroid/content/res/AssetManager;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 535
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 536
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 538
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 539
    if-nez v3, :cond_1

    .line 565
    :cond_0
    :goto_0
    return v1

    .line 545
    :cond_1
    :try_start_0
    const-string v0, "basic_ams_file_list"

    invoke-virtual {p1, v0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 546
    array-length v0, v4

    if-lez v0, :cond_0

    array-length v0, v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-gt v0, v2, :cond_0

    move v0, v1

    .line 555
    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_4

    .line 556
    aget-object v5, v4, v1

    aget-object v6, v3, v0

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v2

    .line 561
    :goto_2
    if-nez v0, :cond_2

    .line 562
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "basic_ams_file_list/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v3, v4, v1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v1, v4, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/vip/amschaton/q;->b(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v1, v2

    .line 565
    goto :goto_0

    .line 549
    :catch_0
    move-exception v0

    .line 550
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 555
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private b(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 649
    .line 652
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 653
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 654
    :try_start_2
    invoke-direct {p0, v3, v1}, Lcom/sec/vip/amschaton/q;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 658
    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/q;->a(Ljava/io/InputStream;)V

    .line 659
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/q;->a(Ljava/io/OutputStream;)V

    .line 660
    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/q;->b(Ljava/io/OutputStream;)V

    .line 662
    return-void

    .line 655
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 656
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 658
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/q;->a(Ljava/io/InputStream;)V

    .line 659
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/q;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 658
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-direct {p0, v2}, Lcom/sec/vip/amschaton/q;->a(Ljava/io/InputStream;)V

    .line 659
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/q;->a(Ljava/io/OutputStream;)V

    .line 660
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/q;->b(Ljava/io/OutputStream;)V

    .line 658
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_2

    :catchall_3
    move-exception v0

    goto :goto_2

    .line 655
    :catch_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v2, v3

    goto :goto_1
.end method

.method private b(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 688
    if-eqz p1, :cond_0

    .line 689
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 694
    :cond_0
    :goto_0
    return-void

    .line 691
    :catch_0
    move-exception v0

    .line 692
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private b([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 918
    new-instance v0, Lcom/sec/vip/amschaton/r;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/r;-><init>(Lcom/sec/vip/amschaton/q;)V

    invoke-static {p1, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 933
    return-void
.end method

.method private b(Landroid/content/res/AssetManager;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 577
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 578
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 580
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    .line 581
    if-nez v4, :cond_1

    .line 635
    :cond_0
    :goto_0
    return v1

    .line 587
    :cond_1
    :try_start_0
    const-string v0, "basic_ams_files"

    invoke-virtual {p1, v0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 588
    array-length v0, v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-lez v0, :cond_0

    move v0, v1

    .line 599
    :goto_1
    array-length v3, v4

    if-ge v0, v3, :cond_9

    .line 600
    aget-object v3, v4, v0

    const-string v6, "1"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    .line 605
    :goto_2
    aget-object v3, v5, v1

    const-string v6, "ics_sample"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    move v0, v1

    .line 607
    :goto_3
    array-length v3, v4

    if-ge v0, v3, :cond_3

    .line 608
    new-instance v3, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v4, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 607
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 591
    :catch_0
    move-exception v0

    .line 592
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 599
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 611
    :cond_3
    :goto_4
    array-length v0, v5

    if-ge v1, v0, :cond_7

    .line 612
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[copyAssetBasicFileToLocalFolder1] "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v3, v5, v1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "basic_ams_files/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v3, v5, v1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v5, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v0, v3}, Lcom/sec/vip/amschaton/q;->b(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    move v0, v1

    .line 618
    :goto_5
    array-length v3, v5

    if-ge v0, v3, :cond_7

    move v3, v1

    .line 620
    :goto_6
    array-length v6, v4

    if-ge v3, v6, :cond_8

    .line 621
    aget-object v6, v5, v0

    aget-object v7, v4, v3

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v3, v2

    .line 626
    :goto_7
    if-nez v3, :cond_5

    .line 627
    aget-object v3, v5, v0

    invoke-direct {p0, v3}, Lcom/sec/vip/amschaton/q;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 628
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[copyAssetBasicFileToLocalFolder2] "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v6, v5, v0

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "basic_ams_files/"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v6, v5, v0

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v5, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v3, v6}, Lcom/sec/vip/amschaton/q;->b(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 620
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_7
    move v1, v2

    .line 635
    goto/16 :goto_0

    :cond_8
    move v3, v1

    goto :goto_7

    :cond_9
    move v0, v1

    goto/16 :goto_2
.end method

.method private b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 499
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 500
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 502
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 503
    if-nez v1, :cond_0

    .line 523
    :goto_0
    return v0

    .line 509
    :cond_0
    iget-object v2, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 510
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    .line 512
    :cond_1
    iget-object v2, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 514
    array-length v2, v1

    iput v2, p0, Lcom/sec/vip/amschaton/q;->g:I

    .line 515
    :goto_1
    iget v2, p0, Lcom/sec/vip/amschaton/q;->g:I

    if-ge v0, v2, :cond_3

    .line 516
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 517
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 515
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 520
    :cond_2
    iget-object v3, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 523
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 882
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 883
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 884
    iput-object p1, p0, Lcom/sec/vip/amschaton/q;->b:Ljava/lang/String;

    .line 886
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 887
    if-nez v1, :cond_0

    .line 907
    :goto_0
    return v0

    .line 891
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/vip/amschaton/q;->b([Ljava/lang/String;)V

    .line 893
    iget-object v2, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 894
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    .line 896
    :cond_1
    iget-object v2, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 898
    array-length v2, v1

    iput v2, p0, Lcom/sec/vip/amschaton/q;->i:I

    .line 899
    :goto_1
    iget v2, p0, Lcom/sec/vip/amschaton/q;->i:I

    if-ge v0, v2, :cond_3

    .line 900
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 901
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 899
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 904
    :cond_2
    iget-object v3, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 907
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1036
    new-instance v3, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/vip/amschaton/q;->d:Ljava/lang/String;

    invoke-direct {v3, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1038
    const/4 v2, 0x0

    .line 1040
    :try_start_0
    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1041
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1043
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1044
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 1048
    :catch_0
    move-exception v2

    .line 1052
    :goto_1
    if-eqz v0, :cond_0

    .line 1053
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1064
    :cond_0
    :goto_2
    return v1

    .line 1046
    :cond_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1052
    if-eqz v0, :cond_2

    .line 1053
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_3
    move v0, v1

    .line 1059
    :goto_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1060
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1061
    const/4 v1, 0x1

    goto :goto_2

    .line 1051
    :catchall_0
    move-exception v0

    .line 1052
    :goto_5
    if-eqz v2, :cond_3

    .line 1053
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 1051
    :cond_3
    :goto_6
    throw v0

    .line 1059
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1055
    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v0

    goto :goto_3

    .line 1051
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    .line 1048
    :catch_4
    move-exception v0

    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public a(IZ)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 427
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 428
    :cond_0
    const/4 v0, 0x0

    .line 436
    :goto_0
    return-object v0

    .line 430
    :cond_1
    if-eqz p2, :cond_2

    .line 431
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 433
    :cond_2
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 434
    const/4 v0, 0x2

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 435
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 436
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;IZ)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 324
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 337
    :cond_0
    :goto_0
    return-object v0

    .line 327
    :cond_1
    if-ltz p2, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 330
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 331
    if-eqz p3, :cond_2

    .line 332
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 334
    :cond_2
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 335
    const/4 v2, 0x2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 336
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 337
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 411
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 412
    :cond_0
    const/4 v0, 0x0

    .line 414
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 312
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-object v0

    .line 315
    :cond_1
    if-ltz p2, :cond_0

    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 318
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 319
    sget-object v1, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-static {p1, v1, v0}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 134
    if-nez p1, :cond_0

    move v0, v1

    .line 165
    :goto_0
    return v0

    .line 142
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/chaton/util/ck;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/AMS/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/q;->a:Ljava/lang/String;

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/vip/amschaton/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "amsbasicfiles/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/q;->c:Ljava/lang/String;

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/vip/amschaton/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "amsuserfiles/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/q;->b:Ljava/lang/String;

    .line 147
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/vip/amschaton/q;->a:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 148
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/vip/amschaton/q;->c:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 149
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/vip/amschaton/q;->b:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 153
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/q;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    const-string v0, "[loadDownloadAMSFiles] false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/vip/amschaton/q;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/vip/amschaton/q;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v3, v4}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 158
    goto/16 :goto_0

    .line 142
    :cond_2
    invoke-static {}, Lcom/sec/common/CommonApplication;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 162
    :cond_3
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/q;->m:Z

    move v0, v2

    .line 165
    goto/16 :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1068
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1069
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1088
    :goto_0
    return v0

    .line 1072
    :cond_0
    const/16 v1, 0x3e8

    if-ne p3, v1, :cond_2

    .line 1073
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1074
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1075
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/q;->k:I

    .line 1088
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1077
    :cond_2
    const/16 v1, 0x3e9

    if-ne p3, v1, :cond_3

    .line 1078
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1079
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1080
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/q;->i:I

    goto :goto_1

    .line 1082
    :cond_3
    const/16 v1, 0x3ea

    if-ne p3, v1, :cond_1

    .line 1083
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1084
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1085
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/q;->g:I

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 363
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 394
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 367
    :goto_1
    iget-object v2, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_5

    .line 368
    invoke-virtual {p0, p1, v0}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 369
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 374
    :goto_2
    if-ltz v0, :cond_4

    iget-object v2, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 375
    iget-object v2, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 377
    if-eqz p3, :cond_2

    .line 379
    :try_start_0
    sget-object v2, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-static {p1, v2, v0}, Lcom/sec/chaton/settings/downloads/q;->c(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 387
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/q;->k:I

    .line 394
    const/4 v1, 0x1

    goto :goto_0

    .line 367
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 389
    :cond_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 390
    iput v1, p0, Lcom/sec/vip/amschaton/q;->k:I

    goto :goto_0

    .line 380
    :catch_0
    move-exception v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public a(Landroid/content/Context;[Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;[Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Context;[Ljava/lang/String;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 345
    if-nez p2, :cond_0

    .line 355
    :goto_0
    return v0

    :cond_0
    move v1, v0

    .line 349
    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 350
    aget-object v1, p2, v0

    invoke-virtual {p0, p1, v1}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 351
    if-nez v1, :cond_2

    :cond_1
    move v0, v1

    .line 355
    goto :goto_0

    .line 349
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 179
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 199
    :cond_0
    :goto_0
    return v0

    .line 183
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v1}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/res/AssetManager;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 184
    const-string v1, "[copyAssetSampleFileListToLocalFolder] false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/q;->b(Landroid/content/res/AssetManager;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 188
    const-string v1, "[copyAssetBasicFileToLocalFolder] false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_3
    invoke-direct {p0, p2}, Lcom/sec/vip/amschaton/q;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 192
    const-string v1, "[loadLocalBasicAMSFiles] false"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_4
    invoke-direct {p0, p3}, Lcom/sec/vip/amschaton/q;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 789
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 790
    :cond_0
    const/4 v0, 0x0

    .line 800
    :goto_0
    return v0

    .line 792
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 793
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/q;->i:I

    .line 795
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 796
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 797
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 800
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a([Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 775
    if-nez p1, :cond_0

    .line 785
    :goto_0
    return v0

    :cond_0
    move v1, v0

    .line 779
    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 780
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/sec/vip/amschaton/q;->a(Ljava/lang/String;)Z

    move-result v1

    .line 781
    if-nez v1, :cond_2

    :cond_1
    move v0, v1

    .line 785
    goto :goto_0

    .line 779
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public b(IZ)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 762
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 763
    :cond_0
    const/4 v0, 0x0

    .line 771
    :goto_0
    return-object v0

    .line 765
    :cond_1
    if-eqz p2, :cond_2

    .line 766
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 768
    :cond_2
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 769
    const/4 v0, 0x2

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 770
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 771
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 746
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 747
    :cond_0
    const/4 v0, 0x0

    .line 749
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 85
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/q;->m:Z

    if-eqz v0, :cond_4

    .line 86
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 122
    :goto_0
    return v0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    .line 94
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 95
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 96
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 98
    const-string v0, "AMSFileManager - path name is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 99
    goto :goto_0

    .line 102
    :cond_2
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AMSFileManager - sample file path("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") is not exist"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 105
    goto :goto_0

    .line 108
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AMSFileManager - sample file path("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") is exist"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_4
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/q;->m:Z

    goto/16 :goto_0

    .line 112
    :cond_5
    const-string v0, "AMSFileManager - mBasicAMSFileList size is zero"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 113
    goto/16 :goto_0

    .line 117
    :cond_6
    const-string v0, "AMSFileManager - mBasicAMSFileList is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 118
    goto/16 :goto_0
.end method

.method public b(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 204
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 205
    if-nez v0, :cond_0

    move v0, v6

    .line 300
    :goto_0
    return v0

    .line 217
    :cond_0
    :try_start_0
    sget-object v1, Lcom/sec/chaton/e/ar;->d:Lcom/sec/chaton/e/ar;

    invoke-static {v1}, Lcom/sec/chaton/e/aq;->a(Lcom/sec/chaton/e/ar;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "install DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 218
    if-nez v0, :cond_2

    .line 244
    if-eqz v0, :cond_1

    .line 245
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v6

    .line 219
    goto :goto_0

    .line 222
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 223
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    .line 225
    :cond_3
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 227
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .line 228
    :goto_1
    if-eqz v1, :cond_7

    .line 229
    const-string v1, "item_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 230
    sget-object v2, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-static {p1, v2, v1}, Lcom/sec/chaton/settings/downloads/q;->b(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 231
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 232
    iget-object v2, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    goto :goto_1

    .line 236
    :catch_0
    move-exception v0

    move-object v0, v7

    .line 237
    :goto_2
    :try_start_2
    const-string v1, "SQLiteException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 244
    if-eqz v0, :cond_5

    .line 245
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_5
    move v0, v6

    .line 242
    goto :goto_0

    .line 244
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v7, :cond_6

    .line 245
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 244
    :cond_6
    throw v0

    :cond_7
    if-eqz v0, :cond_8

    .line 245
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 251
    :cond_8
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/q;->k:I

    .line 274
    sget-object v0, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/q;->a(Lcom/sec/chaton/d/e;)I

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/q;->l:I

    .line 275
    iget v0, p0, Lcom/sec/vip/amschaton/q;->l:I

    if-gez v0, :cond_9

    .line 276
    iput v6, p0, Lcom/sec/vip/amschaton/q;->l:I

    .line 300
    :cond_9
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 244
    :catchall_1
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_3

    .line 236
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method public b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 994
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    if-nez p2, :cond_1

    .line 1032
    :cond_0
    :goto_0
    return v0

    .line 997
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 998
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 999
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1001
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1002
    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lcom/sec/vip/amschaton/q;->g:I

    .line 1004
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/vip/amschaton/q;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 1005
    if-eqz v3, :cond_0

    .line 1008
    const/4 v2, 0x0

    .line 1010
    :try_start_0
    new-instance v4, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/vip/amschaton/q;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/vip/amschaton/q;->d:Ljava/lang/String;

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z

    .line 1012
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1013
    :try_start_1
    new-instance v4, Ljava/io/OutputStreamWriter;

    invoke-direct {v4, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    move v2, v0

    .line 1014
    :goto_1
    array-length v5, v3

    if-ge v2, v5, :cond_4

    .line 1015
    aget-object v5, v3, v2

    invoke-virtual {v4, v5}, Ljava/io/OutputStreamWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1016
    array-length v5, v3

    add-int/lit8 v5, v5, -0x1

    if-ge v2, v5, :cond_3

    .line 1017
    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/io/OutputStreamWriter;->append(C)Ljava/io/Writer;

    .line 1014
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1020
    :cond_4
    invoke-virtual {v4}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1024
    if-eqz v1, :cond_5

    .line 1026
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1032
    :cond_5
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1024
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_6

    .line 1026
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1024
    :cond_6
    :goto_4
    throw v0

    .line 1021
    :catch_0
    move-exception v1

    move-object v1, v2

    .line 1024
    :goto_5
    if-eqz v1, :cond_0

    .line 1026
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 1027
    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_2

    .line 1024
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1021
    :catch_4
    move-exception v2

    goto :goto_5
.end method

.method public c()I
    .locals 1

    .prologue
    .line 304
    iget v0, p0, Lcom/sec/vip/amschaton/q;->k:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 401
    iget v0, p0, Lcom/sec/vip/amschaton/q;->g:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 736
    iget v0, p0, Lcom/sec/vip/amschaton/q;->i:I

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 973
    iget-object v0, p0, Lcom/sec/vip/amschaton/q;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 974
    const/4 v0, 0x1

    .line 976
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
