.class public Lcom/sec/vip/amschaton/ac;
.super Lcom/sec/vip/amschaton/u;
.source "AMSObjectTextGuide.java"


# instance fields
.field public Z:Ljava/lang/String;

.field public aa:Landroid/graphics/Rect;

.field private ab:Lcom/sec/vip/amschaton/o;

.field private ac:Landroid/graphics/Bitmap;

.field private ad:I

.field private ae:I

.field private af:[I

.field private final ag:I

.field private final ah:I

.field private ai:F

.field private aj:F

.field private ak:I

.field private al:Z

.field private am:F

.field private an:I

.field private ao:F

.field private ap:F

.field private aq:F

.field private ar:F

.field private as:F


# direct methods
.method public constructor <init>(IIIZII)V
    .locals 6

    .prologue
    .line 70
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/sec/vip/amschaton/u;-><init>(IIIII)V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->ab:Lcom/sec/vip/amschaton/o;

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    .line 36
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->af:[I

    .line 38
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ag:I

    .line 39
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ah:I

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->aj:F

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->al:Z

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->am:F

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->an:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ao:F

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ap:F

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->aq:F

    .line 51
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    .line 52
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ar:F

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->as:F

    .line 71
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    .line 72
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 74
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 75
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 76
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 77
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 78
    if-gez p3, :cond_1

    .line 79
    const/4 v0, 0x0

    .line 81
    :goto_0
    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .line 82
    const/4 v0, 0x4

    .line 84
    :cond_0
    iput v0, p0, Lcom/sec/vip/amschaton/ac;->c:I

    .line 85
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->c:I

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->an:I

    .line 86
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->af:[I

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->c:I

    aget v1, v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 87
    iput-boolean p4, p0, Lcom/sec/vip/amschaton/ac;->al:Z

    .line 88
    return-void

    :cond_1
    move v0, p3

    goto :goto_0

    .line 36
    :array_0
    .array-data 4
        0xf
        0x12
        0x14
        0x17
        0x19
    .end array-data
.end method

.method private b(Ljava/lang/String;)F
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1100
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    .line 1102
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1103
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setDither(Z)V

    .line 1104
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->af:[I

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->c:I

    aget v1, v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1105
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->b:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1106
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method private b(FFFFLjava/lang/String;IZ)Z
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 1206
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1207
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1209
    if-eqz p7, :cond_0

    .line 1210
    invoke-virtual {p0, v0, p5}, Lcom/sec/vip/amschaton/ac;->a(Landroid/graphics/RectF;Ljava/lang/String;)Z

    .line 1213
    :cond_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->i:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1215
    add-float v1, p1, p3

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    div-float/2addr v1, v5

    .line 1216
    add-float v2, p1, p3

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    div-float/2addr v2, v5

    .line 1217
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    packed-switch v3, :pswitch_data_0

    .line 1224
    :pswitch_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, p1, p2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1225
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v1, v1

    add-float/2addr v1, p1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    add-float/2addr v2, p2

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1247
    :goto_0
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/sec/vip/amschaton/ac;->a(FFFF)V

    .line 1249
    const/4 v0, 0x1

    return v0

    .line 1219
    :pswitch_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v3, v3

    sub-float v3, p3, v3

    iget-object v4, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, v3, p2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1220
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v1, v1

    sub-float v1, p3, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    add-float/2addr v2, p2

    invoke-virtual {v0, v1, p2, p3, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 1228
    :pswitch_2
    iget-boolean v3, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    if-eqz v3, :cond_1

    .line 1229
    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v3, v4, v1, p2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1230
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v3, v3

    sub-float v3, p4, v3

    invoke-virtual {v0, v1, v3, v2, p4}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 1232
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, p1, p2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1233
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v1, v1

    add-float/2addr v1, p1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    add-float/2addr v2, p2

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 1237
    :pswitch_3
    iget-boolean v3, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    if-eqz v3, :cond_2

    .line 1238
    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v3, v4, v1, p2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1239
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v3, v3

    add-float/2addr v3, p2

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 1241
    :cond_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, p1, p2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1242
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v1, v1

    add-float/2addr v1, p1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    add-float/2addr v2, p2

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 1217
    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a(F)V
    .locals 5

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/u;->a(F)V

    .line 99
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->af:[I

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->c:I

    aget v1, v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-double v1, v1

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    add-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 100
    return-void
.end method

.method public a(FF)V
    .locals 1

    .prologue
    .line 911
    float-to-int v0, p1

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 912
    float-to-int v0, p2

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    .line 913
    return-void
.end method

.method public a(Lcom/sec/vip/amschaton/o;)V
    .locals 0

    .prologue
    .line 1253
    iput-object p1, p0, Lcom/sec/vip/amschaton/ac;->ab:Lcom/sec/vip/amschaton/o;

    .line 1254
    return-void
.end method

.method public a(FFFFLjava/lang/String;I)Z
    .locals 8

    .prologue
    .line 1201
    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/sec/vip/amschaton/ac;->b(FFFFLjava/lang/String;IZ)Z

    move-result v0

    return v0
.end method

.method public a(FFLjava/lang/String;I)Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1156
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->al:Z

    if-eqz v0, :cond_0

    .line 1157
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 1158
    iget v4, p0, Lcom/sec/vip/amschaton/ac;->F:F

    .line 1160
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    packed-switch v0, :pswitch_data_0

    move v0, p2

    move v1, p1

    .line 1173
    :goto_0
    cmpl-float v2, v3, v1

    if-lez v2, :cond_3

    .line 1178
    :goto_1
    cmpl-float v2, v4, v0

    if-lez v2, :cond_2

    move v2, v0

    :goto_2
    move-object v0, p0

    move-object v5, p3

    move v6, p4

    .line 1183
    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/ac;->a(FFFFLjava/lang/String;I)Z

    move-result v0

    .line 1194
    :goto_3
    return v0

    .line 1163
    :pswitch_0
    iget p2, p0, Lcom/sec/vip/amschaton/ac;->aj:F

    move v0, p2

    move v1, p1

    .line 1164
    goto :goto_0

    .line 1166
    :pswitch_1
    iget p1, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    move v0, p2

    move v1, p1

    .line 1168
    goto :goto_0

    .line 1186
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1187
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 1188
    invoke-virtual {p0, p3}, Lcom/sec/vip/amschaton/ac;->a(Ljava/lang/String;)Z

    .line 1190
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    float-to-int v2, p2

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v3, v3

    add-float/2addr v3, p1

    float-to-int v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v4, v4

    add-float/2addr v4, p2

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1191
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->i:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v5, v5, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1192
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, p1, p2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1193
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/sec/vip/amschaton/ac;->a(FFFF)V

    .line 1194
    const/4 v0, 0x1

    goto :goto_3

    :cond_2
    move v2, v4

    move v4, v0

    goto :goto_2

    :cond_3
    move v7, v1

    move v1, v3

    move v3, v7

    goto :goto_1

    .line 1160
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(III)Z
    .locals 8

    .prologue
    const/16 v6, 0xff

    const/4 v0, 0x5

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 180
    invoke-super {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/u;->a(III)Z

    .line 182
    if-gez p3, :cond_6

    move v1, v2

    .line 185
    :goto_0
    if-le v1, v0, :cond_5

    .line 188
    :goto_1
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->an:I

    if-eq v1, v0, :cond_4

    move v1, v7

    .line 191
    :goto_2
    iput v0, p0, Lcom/sec/vip/amschaton/ac;->c:I

    .line 192
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->c:I

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->an:I

    .line 193
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->af:[I

    iget v4, p0, Lcom/sec/vip/amschaton/ac;->c:I

    aget v3, v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 195
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->al:Z

    if-eqz v0, :cond_3

    .line 197
    new-instance v0, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    invoke-direct {v0, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 198
    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    invoke-virtual {p0, v0, v3}, Lcom/sec/vip/amschaton/ac;->a(Landroid/graphics/RectF;Ljava/lang/String;)Z

    .line 199
    iget-boolean v3, p0, Lcom/sec/vip/amschaton/ac;->H:Z

    if-nez v3, :cond_1

    .line 236
    :cond_0
    :goto_3
    return v2

    .line 202
    :cond_1
    new-instance v4, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v4}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 203
    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 204
    iget v2, v0, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 205
    iget v2, v0, Landroid/graphics/RectF;->right:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 206
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 207
    if-eqz v1, :cond_2

    .line 208
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v2

    div-float/2addr v1, v5

    .line 209
    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v2

    div-float/2addr v0, v5

    .line 210
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float v2, v1, v2

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 211
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float v2, v0, v2

    float-to-int v2, v2

    int-to-short v2, v2

    iput-short v2, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 212
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 213
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 215
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget-short v1, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget-short v2, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget-short v3, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-short v5, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 216
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 217
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    .line 218
    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v0

    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v0

    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v0

    iget-short v0, v4, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/ac;->a(FFFFLjava/lang/String;I)Z

    :goto_4
    move v2, v7

    .line 236
    goto :goto_3

    .line 220
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/ac;->a(Ljava/lang/String;)Z

    .line 221
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->H:Z

    if-eqz v0, :cond_0

    .line 224
    new-instance v0, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 225
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v5

    .line 226
    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v2, v5

    .line 227
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float v3, v1, v3

    float-to-int v3, v3

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 228
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float v3, v2, v3

    float-to-int v3, v3

    int-to-short v3, v3

    iput-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 229
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v1, v3

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 230
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v1, v2

    float-to-int v1, v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 231
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget-short v2, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget-short v3, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget-short v4, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-short v0, v0, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 232
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 233
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    .line 234
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v6}, Lcom/sec/vip/amschaton/ac;->a(FFLjava/lang/String;I)Z

    goto :goto_4

    :cond_4
    move v1, v2

    goto/16 :goto_2

    :cond_5
    move v0, v1

    goto/16 :goto_1

    :cond_6
    move v1, p3

    goto/16 :goto_0
.end method

.method public a(Landroid/graphics/RectF;Ljava/lang/String;)Z
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1002
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7}, Landroid/text/TextPaint;-><init>()V

    .line 1004
    invoke-virtual {v7, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1005
    invoke-virtual {v7, v4}, Landroid/text/TextPaint;->setDither(Z)V

    .line 1006
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->af:[I

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->c:I

    aget v0, v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v0, v1

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1007
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->b:I

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1009
    const-string v1, ""

    .line 1010
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1011
    iput v5, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    .line 1012
    iput v5, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    .line 1013
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v8

    .line 1014
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v9, v2

    move v6, v5

    move v2, v5

    move v3, v5

    .line 1015
    :goto_0
    if-ge v6, v8, :cond_7

    .line 1016
    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v10

    const/16 v11, 0x80

    if-ge v10, v11, :cond_1

    .line 1017
    add-int/lit8 v3, v3, 0x1

    .line 1022
    :goto_1
    if-lez v3, :cond_6

    .line 1023
    rem-int/lit8 v10, v3, 0x2

    if-nez v10, :cond_3

    .line 1025
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v10

    float-to-int v10, v10

    .line 1026
    iget v11, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    if-ge v11, v10, :cond_0

    .line 1027
    iput v10, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    .line 1029
    :cond_0
    if-le v10, v9, :cond_2

    .line 1030
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1032
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1033
    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1035
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    .line 1015
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1019
    :cond_1
    add-int/lit8 v3, v3, 0x2

    goto :goto_1

    .line 1037
    :cond_2
    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1041
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v10

    float-to-int v10, v10

    .line 1042
    iget v11, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    if-ge v11, v10, :cond_4

    .line 1043
    iput v10, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    .line 1045
    :cond_4
    if-le v10, v9, :cond_5

    .line 1046
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1048
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1049
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1050
    const/4 v3, 0x2

    .line 1051
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1053
    :cond_5
    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1057
    :cond_6
    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1060
    :cond_7
    if-lez v3, :cond_8

    .line 1061
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1062
    add-int/lit8 v2, v2, 0x1

    .line 1063
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    .line 1064
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    if-ge v3, v0, :cond_8

    .line 1065
    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    .line 1068
    :cond_8
    invoke-virtual {v7}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    float-to-double v8, v0

    int-to-double v2, v2

    const-wide v10, 0x3fd999999999999aL    # 0.4

    add-double/2addr v2, v10

    mul-double/2addr v2, v8

    double-to-int v0, v2

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    .line 1069
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_9

    .line 1070
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1071
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    .line 1073
    :cond_9
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    .line 1074
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move v0, v5

    move v2, v4

    .line 1080
    :goto_3
    const-string v5, "\n"

    invoke-virtual {v1, v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    .line 1081
    const/4 v6, -0x1

    if-ne v5, v6, :cond_a

    .line 1082
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-virtual {v3, v0, v12, v1, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1089
    return v4

    .line 1085
    :cond_a
    invoke-virtual {v1, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Landroid/text/TextPaint;->getTextSize()F

    move-result v6

    int-to-float v8, v2

    mul-float/2addr v6, v8

    invoke-virtual {v3, v0, v12, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1086
    add-int/lit8 v0, v5, 0x1

    .line 1087
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public a(Ljava/lang/String;)Z
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 923
    new-instance v6, Landroid/text/TextPaint;

    invoke-direct {v6}, Landroid/text/TextPaint;-><init>()V

    .line 925
    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 926
    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setDither(Z)V

    .line 927
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->af:[I

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->c:I

    aget v0, v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v0, v1

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 928
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->b:I

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 930
    const-string v1, ""

    .line 931
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 932
    iput v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    .line 933
    iput v3, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    .line 934
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->N:I

    const/high16 v5, 0x42200000    # 40.0f

    iget v7, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v5, v7

    const/high16 v7, 0x3f000000    # 0.5f

    add-float/2addr v5, v7

    float-to-int v5, v5

    sub-int v7, v2, v5

    .line 935
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    move v5, v3

    move v2, v3

    .line 936
    :goto_0
    if-ge v5, v8, :cond_2

    .line 956
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v9

    .line 957
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v10

    float-to-int v10, v10

    .line 958
    iget v11, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    if-ge v11, v10, :cond_0

    .line 959
    iput v10, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    .line 961
    :cond_0
    if-le v10, v7, :cond_1

    .line 962
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 964
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 965
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 966
    add-int/lit8 v2, v2, 0x1

    .line 936
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 968
    :cond_1
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 971
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 972
    add-int/lit8 v1, v2, 0x1

    .line 973
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    .line 974
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    if-ge v2, v0, :cond_3

    .line 975
    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    .line 978
    :cond_3
    invoke-virtual {v6}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    float-to-double v7, v0

    int-to-double v0, v1

    const-wide v9, 0x3fd999999999999aL    # 0.4

    add-double/2addr v0, v9

    mul-double/2addr v0, v7

    double-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    .line 979
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 980
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 981
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    .line 983
    :cond_4
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    .line 984
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ac:Landroid/graphics/Bitmap;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move v0, v3

    move v1, v4

    .line 989
    :goto_2
    const-string v3, "\n"

    invoke-virtual {v5, v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    .line 990
    const/4 v7, -0x1

    if-ne v3, v7, :cond_5

    .line 991
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v5, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Landroid/text/TextPaint;->getTextSize()F

    move-result v3

    int-to-float v1, v1

    mul-float/2addr v1, v3

    invoke-virtual {v2, v0, v12, v1, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 998
    return v4

    .line 994
    :cond_5
    invoke-virtual {v5, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Landroid/text/TextPaint;->getTextSize()F

    move-result v7

    int-to-float v8, v1

    mul-float/2addr v7, v8

    invoke-virtual {v2, v0, v12, v7, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 995
    add-int/lit8 v0, v3, 0x1

    .line 996
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 2

    .prologue
    .line 1137
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->F:F

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/sec/vip/amschaton/ac;->a(FFLjava/lang/String;I)Z

    .line 1138
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/String;II)Z
    .locals 7

    .prologue
    const/16 v6, 0xff

    const/4 v0, 0x5

    .line 137
    iput p2, p0, Lcom/sec/vip/amschaton/ac;->b:I

    .line 138
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->b:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 140
    if-gez p3, :cond_2

    .line 141
    const/4 v1, 0x0

    .line 143
    :goto_0
    if-le v1, v0, :cond_1

    .line 146
    :goto_1
    iput v0, p0, Lcom/sec/vip/amschaton/ac;->c:I

    .line 147
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->c:I

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->an:I

    .line 148
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->l:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->af:[I

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->c:I

    aget v1, v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 150
    invoke-virtual {p0, p1}, Lcom/sec/vip/amschaton/ac;->a(Ljava/lang/String;)Z

    .line 151
    iput-object p1, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    .line 152
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->F:F

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v4, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, p0, Lcom/sec/vip/amschaton/ac;->F:F

    iget v5, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 153
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->al:Z

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    move-object v0, p0

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/ac;->a(FFFFLjava/lang/String;I)Z

    .line 158
    :goto_2
    const/4 v0, 0x1

    return v0

    .line 156
    :cond_0
    invoke-virtual {p0, p1, v6}, Lcom/sec/vip/amschaton/ac;->a(Ljava/lang/String;I)Z

    goto :goto_2

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, p3

    goto :goto_0
.end method

.method public a(Ljava/lang/String;IIFF)Z
    .locals 1

    .prologue
    .line 162
    iput p4, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 163
    iput p5, p0, Lcom/sec/vip/amschaton/ac;->F:F

    .line 164
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/vip/amschaton/ac;->a(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/sec/amsoma/structure/AMS_RECT;)Z
    .locals 7

    .prologue
    const/16 v6, 0xff

    .line 285
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->H:Z

    if-nez v0, :cond_0

    .line 286
    const/4 v0, 0x0

    .line 296
    :goto_0
    return v0

    .line 289
    :cond_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->al:Z

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget-short v1, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget-short v2, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget-short v3, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-short v4, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 291
    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v1, v0

    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v2, v0

    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    int-to-float v3, v0

    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/ac;->a(FFFFLjava/lang/String;I)Z

    .line 296
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget-short v1, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget-short v2, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget-short v3, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    iget-short v4, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 294
    iget-short v0, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    iget-short v1, p1, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v6}, Lcom/sec/vip/amschaton/ac;->a(FFLjava/lang/String;I)Z

    goto :goto_1
.end method

.method public c(FF)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 113
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->B:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v0

    .line 116
    :cond_1
    const/high16 v1, 0x41700000    # 15.0f

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v1, v2

    .line 117
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    sub-float/2addr v3, v1

    iget-object v4, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    sub-float/2addr v4, v1

    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    add-float/2addr v5, v1

    const/high16 v6, 0x41400000    # 12.0f

    iget v7, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    add-float/2addr v1, v6

    invoke-direct {v2, v3, v4, v5, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 119
    invoke-virtual {v2, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public d(FF)Z
    .locals 12

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->al:Z

    if-eqz v0, :cond_8

    .line 313
    const/4 v0, 0x0

    .line 314
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->B:I

    packed-switch v1, :pswitch_data_0

    .line 427
    :goto_0
    const/4 v0, 0x1

    .line 452
    :goto_1
    return v0

    .line 318
    :pswitch_0
    iput p1, p0, Lcom/sec/vip/amschaton/ac;->ar:F

    .line 319
    iput p2, p0, Lcom/sec/vip/amschaton/ac;->as:F

    .line 323
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 324
    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    .line 325
    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    .line 326
    iget-object v4, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    .line 329
    iget v5, p0, Lcom/sec/vip/amschaton/ac;->R:F

    iget v6, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    .line 330
    sub-int v6, v1, v5

    .line 331
    sub-int v7, v2, v5

    .line 332
    add-int v8, v3, v5

    const/high16 v9, 0x41400000    # 12.0f

    iget v10, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v9, v10

    float-to-int v9, v9

    add-int/2addr v8, v9

    .line 333
    add-int/2addr v5, v4

    .line 343
    int-to-float v9, v8

    sub-float/2addr v9, p1

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x41700000    # 15.0f

    iget v11, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gez v9, :cond_2

    int-to-float v9, v7

    sub-float/2addr v9, p2

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x41700000    # 15.0f

    iget v11, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gez v9, :cond_2

    .line 349
    const/16 v0, 0x65

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/ac;->b(IZ)V

    .line 350
    const/4 v0, 0x1

    .line 405
    :cond_0
    :goto_2
    iput p2, p0, Lcom/sec/vip/amschaton/ac;->am:F

    .line 406
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    .line 407
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->B:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->B:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_6

    .line 409
    :cond_1
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/ac;->b(IZ)V

    .line 410
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 411
    const/16 v1, 0x80

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/vip/amschaton/ac;->a(FFLjava/lang/String;I)Z

    .line 412
    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/ac;->b(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->aq:F

    goto :goto_0

    .line 365
    :cond_2
    add-int v9, v6, v8

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    sub-float/2addr v9, p1

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x41700000    # 15.0f

    iget v11, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gez v9, :cond_3

    int-to-float v9, v7

    sub-float/2addr v9, p2

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x41700000    # 15.0f

    iget v11, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gez v9, :cond_3

    .line 367
    int-to-float v1, v1

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 368
    int-to-float v1, v4

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->F:F

    .line 369
    int-to-float v1, v3

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    .line 370
    int-to-float v1, v4

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->aj:F

    .line 371
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ao:F

    .line 372
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ap:F

    .line 373
    const/4 v1, 0x6

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->B:I

    .line 374
    const/16 v1, 0x6a

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    goto :goto_2

    .line 375
    :cond_3
    int-to-float v9, v8

    sub-float/2addr v9, p1

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x41700000    # 15.0f

    iget v11, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gez v9, :cond_4

    add-int v9, v7, v5

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    sub-float/2addr v9, p2

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x41700000    # 15.0f

    iget v11, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gez v9, :cond_4

    .line 377
    int-to-float v3, v1

    iput v3, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 378
    int-to-float v2, v2

    iput v2, p0, Lcom/sec/vip/amschaton/ac;->F:F

    .line 379
    int-to-float v1, v1

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    .line 380
    int-to-float v1, v4

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->aj:F

    .line 381
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->B:I

    .line 383
    const/16 v1, 0x6b

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    goto/16 :goto_2

    .line 384
    :cond_4
    add-int/2addr v8, v6

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float/2addr v8, p1

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    const/high16 v9, 0x41700000    # 15.0f

    iget v10, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v9, v10

    cmpg-float v8, v8, v9

    if-gez v8, :cond_5

    int-to-float v8, v5

    sub-float/2addr v8, p2

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    const/high16 v9, 0x41700000    # 15.0f

    iget v10, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v9, v10

    cmpg-float v8, v8, v9

    if-gez v8, :cond_5

    .line 386
    int-to-float v1, v1

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 387
    int-to-float v1, v2

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->F:F

    .line 388
    int-to-float v1, v3

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    .line 389
    int-to-float v1, v2

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->aj:F

    .line 390
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ao:F

    .line 391
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ap:F

    .line 392
    const/4 v1, 0x6

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->B:I

    .line 393
    const/16 v1, 0x6c

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    goto/16 :goto_2

    .line 394
    :cond_5
    int-to-float v1, v6

    sub-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v6, 0x41700000    # 15.0f

    iget v8, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v6, v8

    cmpg-float v1, v1, v6

    if-gez v1, :cond_0

    add-int v1, v7, v5

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v5, 0x41700000    # 15.0f

    iget v6, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v5, v6

    cmpg-float v1, v1, v5

    if-gez v1, :cond_0

    .line 396
    int-to-float v1, v3

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 397
    int-to-float v1, v2

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->F:F

    .line 398
    int-to-float v1, v3

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    .line 399
    int-to-float v1, v4

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->aj:F

    .line 400
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->B:I

    .line 402
    const/16 v1, 0x69

    iput v1, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    goto/16 :goto_2

    .line 416
    :cond_6
    if-eqz v0, :cond_7

    .line 417
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    goto/16 :goto_0

    .line 421
    :cond_7
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->C:I

    .line 422
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    sub-float v0, p2, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->D:I

    .line 423
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    .line 424
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    const/16 v6, 0x80

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/vip/amschaton/ac;->a(FFFFLjava/lang/String;I)Z

    goto/16 :goto_0

    .line 430
    :cond_8
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    packed-switch v0, :pswitch_data_1

    .line 452
    :goto_3
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 433
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 434
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    .line 435
    int-to-float v1, v1

    sub-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x41700000    # 15.0f

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_9

    int-to-float v0, v0

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x41700000    # 15.0f

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_9

    .line 436
    const/16 v0, 0x65

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/ac;->b(IZ)V

    .line 437
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    goto :goto_3

    .line 442
    :cond_9
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->C:I

    .line 443
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    sub-float v0, p2, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->D:I

    .line 444
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    .line 445
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/ac;->a(FF)V

    .line 446
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 447
    const/16 v1, 0x80

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/ac;->a(Ljava/lang/String;I)Z

    .line 448
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    .line 449
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/ac;->a(FF)V

    goto :goto_3

    .line 314
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    .line 430
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
    .end packed-switch
.end method

.method public e(FF)Z
    .locals 9

    .prologue
    const/16 v6, 0x80

    const/high16 v4, 0x41700000    # 15.0f

    const/4 v7, 0x0

    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 467
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->al:Z

    if-eqz v0, :cond_11

    .line 468
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    packed-switch v0, :pswitch_data_0

    .line 597
    :cond_0
    :goto_0
    :pswitch_0
    return v8

    .line 472
    :pswitch_1
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    sub-float v0, p1, v0

    .line 473
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->F:F

    sub-float v2, p2, v2

    .line 474
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v3, v3

    cmpl-float v3, v0, v3

    if-lez v3, :cond_1

    .line 475
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v4, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v4, v4

    add-float p1, v3, v4

    .line 477
    :cond_1
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    neg-int v3, v3

    int-to-float v3, v3

    cmpl-float v0, v3, v0

    if-lez v0, :cond_2

    .line 478
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v3, v3

    sub-float p1, v0, v3

    .line 480
    :cond_2
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v0, v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_3

    cmpg-float v0, v1, v2

    if-gtz v0, :cond_4

    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v0, v0

    cmpg-float v0, v2, v0

    if-gez v0, :cond_4

    .line 481
    :cond_3
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v3, v3

    add-float p2, v0, v3

    .line 483
    :cond_4
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_5

    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_6

    cmpg-float v0, v2, v1

    if-gez v0, :cond_6

    .line 484
    :cond_5
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v1, v1

    sub-float p2, v0, v1

    .line 487
    :cond_6
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v6}, Lcom/sec/vip/amschaton/ac;->a(FFLjava/lang/String;I)Z

    .line 488
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->H:Z

    if-eqz v0, :cond_7

    .line 489
    iput-boolean v8, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    .line 491
    :cond_7
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    invoke-virtual {p0, v0, v8}, Lcom/sec/vip/amschaton/ac;->b(IZ)V

    goto :goto_0

    .line 494
    :pswitch_2
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    .line 495
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->am:F

    sub-float v2, p2, v2

    const v3, 0x3f19999a    # 0.6f

    mul-float/2addr v2, v3

    .line 497
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    const/16 v4, 0x6a

    if-ne v3, v4, :cond_f

    .line 498
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 499
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    add-float/2addr v0, v2

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    .line 500
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v0, v0

    .line 506
    :cond_8
    :goto_1
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_9

    .line 507
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    .line 508
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iput v3, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    .line 509
    iput v2, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 512
    :cond_9
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->E:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->aq:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_a

    .line 513
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ao:F

    iput v2, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 514
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ap:F

    iput v2, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    .line 516
    :cond_a
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iput v2, p0, Lcom/sec/vip/amschaton/ac;->ao:F

    .line 517
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    iput v2, p0, Lcom/sec/vip/amschaton/ac;->ap:F

    .line 518
    iput p2, p0, Lcom/sec/vip/amschaton/ac;->am:F

    .line 520
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->F:F

    sub-float v2, p2, v2

    .line 521
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v3, v3

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_c

    cmpg-float v3, v1, v2

    if-gtz v3, :cond_b

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-ltz v3, :cond_c

    :cond_b
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v3, v3

    int-to-float v3, v3

    cmpl-float v3, v3, v2

    if-gtz v3, :cond_c

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v3, v3

    int-to-float v3, v3

    cmpg-float v3, v3, v2

    if-gez v3, :cond_d

    cmpg-float v1, v2, v1

    if-gez v1, :cond_d

    .line 522
    :cond_c
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->F:F

    int-to-float v0, v0

    add-float p2, v1, v0

    .line 525
    :cond_d
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v6}, Lcom/sec/vip/amschaton/ac;->a(FFLjava/lang/String;I)Z

    .line 526
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->H:Z

    if-eqz v0, :cond_e

    .line 527
    iput-boolean v8, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    .line 529
    :cond_e
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    invoke-virtual {p0, v0, v8}, Lcom/sec/vip/amschaton/ac;->b(IZ)V

    goto/16 :goto_0

    .line 501
    :cond_f
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    const/16 v4, 0x6c

    if-ne v3, v4, :cond_8

    .line 502
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->E:F

    add-float/2addr v3, v2

    iput v3, p0, Lcom/sec/vip/amschaton/ac;->E:F

    .line 503
    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    sub-float v2, v3, v2

    iput v2, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    goto/16 :goto_1

    .line 532
    :pswitch_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    .line 533
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v4, v1, v2

    .line 535
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->C:I

    int-to-float v1, v1

    sub-float v1, p1, v1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->D:I

    int-to-float v2, v2

    sub-float v2, p2, v2

    int-to-float v0, v0

    add-float/2addr v0, p1

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->C:I

    int-to-float v3, v3

    sub-float v3, v0, v3

    int-to-float v0, v4

    add-float/2addr v0, p2

    iget v4, p0, Lcom/sec/vip/amschaton/ac;->D:I

    int-to-float v4, v4

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/vip/amschaton/ac;->b(FFFFLjava/lang/String;IZ)Z

    .line 536
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->H:Z

    if-eqz v0, :cond_0

    .line 537
    iput-boolean v8, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    goto/16 :goto_0

    .line 543
    :pswitch_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 544
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    .line 545
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->R:F

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 546
    sub-int/2addr v0, v2

    .line 547
    add-int/2addr v1, v2

    const/high16 v2, 0x41400000    # 12.0f

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    .line 548
    int-to-float v1, v1

    sub-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v2, v4

    cmpg-float v1, v1, v2

    if-gez v1, :cond_10

    int-to-float v0, v0

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v1, v4

    cmpg-float v0, v0, v1

    if-gez v0, :cond_10

    move v0, v8

    :goto_2
    if-nez v0, :cond_0

    .line 549
    const/16 v0, 0x65

    invoke-virtual {p0, v0, v7}, Lcom/sec/vip/amschaton/ac;->b(IZ)V

    .line 550
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    goto/16 :goto_0

    :cond_10
    move v0, v7

    .line 548
    goto :goto_2

    .line 558
    :cond_11
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 560
    :sswitch_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    if-nez v0, :cond_13

    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v2, 0x41200000    # 10.0f

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_12

    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v2, 0x41200000    # 10.0f

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_13

    .line 561
    :cond_12
    iput-boolean v8, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    .line 564
    :cond_13
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int v3, v0, v2

    .line 565
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v4, v0, v2

    .line 566
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->C:I

    int-to-float v0, v0

    sub-float v2, p1, v0

    .line 567
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->D:I

    int-to-float v0, v0

    sub-float v0, p2, v0

    .line 568
    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/sec/vip/amschaton/ac;->N:I

    .line 569
    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/sec/vip/amschaton/ac;->O:I

    .line 570
    cmpg-float v5, v2, v1

    if-gez v5, :cond_16

    move v2, v1

    .line 576
    :cond_14
    :goto_3
    cmpg-float v3, v0, v1

    if-gez v3, :cond_17

    move v0, v1

    .line 582
    :cond_15
    :goto_4
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v1, v6}, Lcom/sec/vip/amschaton/ac;->a(FFLjava/lang/String;I)Z

    goto/16 :goto_0

    .line 573
    :cond_16
    iget v5, p0, Lcom/sec/vip/amschaton/ac;->N:I

    sub-int/2addr v5, v3

    int-to-float v5, v5

    cmpl-float v5, v2, v5

    if-lez v5, :cond_14

    .line 574
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->N:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    goto :goto_3

    .line 579
    :cond_17
    iget v1, p0, Lcom/sec/vip/amschaton/ac;->O:I

    sub-int/2addr v1, v4

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_15

    .line 580
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->O:I

    sub-int/2addr v0, v4

    int-to-float v0, v0

    goto :goto_4

    .line 588
    :sswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    .line 589
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    .line 590
    sub-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v2, v4

    cmpg-float v1, v1, v2

    if-gez v1, :cond_18

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/sec/vip/amschaton/ac;->d:F

    mul-float/2addr v1, v4

    cmpg-float v0, v0, v1

    if-gez v0, :cond_18

    move v0, v8

    :goto_5
    if-nez v0, :cond_0

    .line 591
    const/16 v0, 0x65

    invoke-virtual {p0, v0, v7}, Lcom/sec/vip/amschaton/ac;->b(IZ)V

    .line 592
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    goto/16 :goto_0

    :cond_18
    move v0, v7

    .line 590
    goto :goto_5

    .line 468
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 558
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x7 -> :sswitch_1
    .end sparse-switch
.end method

.method public f(FF)Z
    .locals 9

    .prologue
    const/high16 v8, 0x40800000    # 4.0f

    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 612
    .line 615
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->al:Z

    if-eqz v0, :cond_21

    .line 616
    new-instance v3, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v3}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 617
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    sub-float v0, p1, v0

    .line 618
    iget v4, p0, Lcom/sec/vip/amschaton/ac;->F:F

    sub-float v4, p2, v4

    .line 619
    iget v5, p0, Lcom/sec/vip/amschaton/ac;->B:I

    packed-switch v5, :pswitch_data_0

    :goto_0
    :pswitch_0
    move v0, v1

    .line 898
    :goto_1
    return v0

    .line 626
    :pswitch_1
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/ac;->a(FF)V

    .line 632
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ab:Lcom/sec/vip/amschaton/o;

    if-nez v0, :cond_0

    move v0, v1

    .line 633
    goto :goto_1

    .line 635
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 636
    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    .line 637
    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->ab:Lcom/sec/vip/amschaton/o;

    invoke-interface {v3, v7, v0, v2}, Lcom/sec/vip/amschaton/o;->a(Ljava/lang/String;FF)Z

    goto :goto_0

    .line 640
    :pswitch_2
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 641
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v5, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v5, v5

    add-float p1, v2, v5

    .line 643
    :cond_1
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    neg-int v2, v2

    int-to-float v2, v2

    cmpl-float v0, v2, v0

    if-lez v0, :cond_2

    .line 644
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v2, v2

    sub-float p1, v0, v2

    .line 646
    :cond_2
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v0, v0

    cmpl-float v0, v4, v0

    if-gtz v0, :cond_3

    cmpg-float v0, v6, v4

    if-gtz v0, :cond_4

    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v0, v0

    cmpg-float v0, v4, v0

    if-gez v0, :cond_4

    .line 647
    :cond_3
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    add-float p2, v0, v2

    .line 649
    :cond_4
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-gtz v0, :cond_5

    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_6

    cmpg-float v0, v4, v6

    if-gez v0, :cond_6

    .line 650
    :cond_5
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    sub-float p2, v0, v2

    .line 652
    :cond_6
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 653
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 654
    float-to-int v0, p1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 655
    float-to-int v0, p2

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 656
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_7

    .line 657
    float-to-int v0, p1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 658
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 660
    :cond_7
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_8

    .line 661
    float-to-int v0, p2

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 662
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 664
    :cond_8
    iput v1, p0, Lcom/sec/vip/amschaton/ac;->B:I

    .line 665
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/ac;->a(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    goto/16 :goto_1

    .line 668
    :pswitch_3
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    .line 670
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v4

    .line 671
    iget-object v4, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    .line 672
    iget v5, p0, Lcom/sec/vip/amschaton/ac;->C:I

    int-to-float v5, v5

    sub-float v5, p1, v5

    float-to-int v5, v5

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 673
    iget v5, p0, Lcom/sec/vip/amschaton/ac;->D:I

    int-to-float v5, v5

    sub-float v5, p2, v5

    float-to-int v5, v5

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 674
    int-to-float v0, v0

    add-float/2addr v0, p1

    iget v5, p0, Lcom/sec/vip/amschaton/ac;->C:I

    int-to-float v5, v5

    sub-float/2addr v0, v5

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 675
    int-to-float v0, v4

    add-float/2addr v0, p2

    iget v4, p0, Lcom/sec/vip/amschaton/ac;->D:I

    int-to-float v4, v4

    sub-float/2addr v0, v4

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 676
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/ac;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    .line 678
    iget-boolean v3, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    if-eqz v3, :cond_a

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ar:F

    sub-float/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v8

    if-gtz v3, :cond_9

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->as:F

    sub-float/2addr v3, p2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v8

    if-lez v3, :cond_a

    .line 679
    :cond_9
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    goto/16 :goto_1

    .line 682
    :cond_a
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->C:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->D:I

    int-to-float v3, v3

    sub-float v3, p2, v3

    invoke-virtual {p0, v0, v3}, Lcom/sec/vip/amschaton/ac;->a(FF)V

    .line 683
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->G:Z

    if-nez v0, :cond_b

    .line 684
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/ac;->G:Z

    move v0, v2

    .line 685
    goto/16 :goto_1

    .line 692
    :cond_b
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ab:Lcom/sec/vip/amschaton/o;

    if-nez v0, :cond_c

    move v0, v1

    .line 693
    goto/16 :goto_1

    .line 695
    :cond_c
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 696
    iget-object v2, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    .line 697
    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->ab:Lcom/sec/vip/amschaton/o;

    invoke-interface {v3, v7, v0, v2}, Lcom/sec/vip/amschaton/o;->a(Ljava/lang/String;FF)Z

    goto/16 :goto_0

    .line 740
    :pswitch_4
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_d

    .line 741
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v5, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v5, v5

    add-float p1, v2, v5

    .line 743
    :cond_d
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    neg-int v2, v2

    int-to-float v2, v2

    cmpl-float v0, v2, v0

    if-lez v0, :cond_e

    .line 744
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v2, v2

    sub-float p1, v0, v2

    .line 746
    :cond_e
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v0, v0

    cmpl-float v0, v4, v0

    if-gtz v0, :cond_f

    cmpg-float v0, v6, v4

    if-gtz v0, :cond_10

    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v0, v0

    cmpg-float v0, v4, v0

    if-gez v0, :cond_10

    .line 747
    :cond_f
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    add-float p2, v0, v2

    .line 749
    :cond_10
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-gtz v0, :cond_11

    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_12

    cmpg-float v0, v4, v6

    if-gez v0, :cond_12

    .line 750
    :cond_11
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    sub-float p2, v0, v2

    .line 752
    :cond_12
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    if-nez v0, :cond_13

    .line 753
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->K:Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->getM_TText()Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_TEXT_INFO;->getM_strText()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0xff

    invoke-virtual {p0, p1, p2, v0, v2}, Lcom/sec/vip/amschaton/ac;->a(FFLjava/lang/String;I)Z

    goto/16 :goto_0

    .line 758
    :cond_13
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    .line 759
    iput p2, p0, Lcom/sec/vip/amschaton/ac;->aj:F

    .line 760
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 761
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 762
    float-to-int v0, p1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 763
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->aj:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 764
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_14

    .line 765
    float-to-int v0, p1

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 766
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 768
    :cond_14
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/ac;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    goto/16 :goto_1

    .line 772
    :pswitch_5
    iget-boolean v2, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    if-eqz v2, :cond_1a

    .line 773
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    .line 774
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ak:I

    const/16 v5, 0x6a

    if-ne v2, v5, :cond_15

    .line 775
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v0, v0

    .line 777
    :cond_15
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    cmpl-float v2, v4, v2

    if-gtz v2, :cond_17

    cmpg-float v2, v6, v4

    if-gtz v2, :cond_16

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    cmpg-float v2, v4, v2

    if-ltz v2, :cond_17

    :cond_16
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v2, v2

    int-to-float v2, v2

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_17

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v2, v2

    int-to-float v2, v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_18

    cmpg-float v2, v4, v6

    if-gez v2, :cond_18

    .line 778
    :cond_17
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->F:F

    int-to-float v0, v0

    add-float p2, v2, v0

    .line 802
    :cond_18
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    .line 804
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 805
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 806
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ai:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 807
    float-to-int v0, p2

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 808
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_19

    .line 809
    float-to-int v0, p2

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 810
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    float-to-int v0, v0

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 812
    :cond_19
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/ac;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    goto/16 :goto_1

    .line 781
    :cond_1a
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1b

    .line 782
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v3, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v3, v3

    add-float p1, v2, v3

    .line 784
    :cond_1b
    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    neg-int v2, v2

    int-to-float v2, v2

    cmpl-float v0, v2, v0

    if-lez v0, :cond_1c

    .line 785
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->E:F

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ad:I

    int-to-float v2, v2

    sub-float p1, v0, v2

    .line 787
    :cond_1c
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v0, v0

    cmpl-float v0, v4, v0

    if-gtz v0, :cond_1d

    cmpg-float v0, v6, v4

    if-gtz v0, :cond_1e

    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v0, v0

    cmpg-float v0, v4, v0

    if-gez v0, :cond_1e

    .line 788
    :cond_1d
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    add-float p2, v0, v2

    .line 790
    :cond_1e
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-gtz v0, :cond_1f

    iget v0, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_20

    cmpg-float v0, v4, v6

    if-gez v0, :cond_20

    .line 791
    :cond_1f
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->F:F

    iget v2, p0, Lcom/sec/vip/amschaton/ac;->ae:I

    int-to-float v2, v2

    sub-float p2, v0, v2

    .line 793
    :cond_20
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->Z:Ljava/lang/String;

    const/16 v2, 0xff

    invoke-virtual {p0, p1, p2, v0, v2}, Lcom/sec/vip/amschaton/ac;->a(FFLjava/lang/String;I)Z

    goto/16 :goto_0

    .line 815
    :pswitch_6
    const/16 v0, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/ac;->b(IZ)V

    .line 816
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->S:Lcom/sec/vip/amschaton/v;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/v;->a()V

    goto/16 :goto_0

    .line 822
    :cond_21
    new-instance v3, Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-direct {v3}, Lcom/sec/amsoma/structure/AMS_RECT;-><init>()V

    .line 823
    iget v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    sparse-switch v0, :sswitch_data_0

    :goto_2
    move v0, v2

    .line 898
    goto/16 :goto_1

    .line 825
    :sswitch_0
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->G:Z

    if-nez v0, :cond_22

    .line 826
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/ac;->G:Z

    move v0, v2

    .line 827
    goto/16 :goto_1

    .line 829
    :cond_22
    invoke-virtual {p0, p1, p2}, Lcom/sec/vip/amschaton/ac;->a(FF)V

    .line 834
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ab:Lcom/sec/vip/amschaton/o;

    if-nez v0, :cond_23

    move v0, v1

    .line 835
    goto/16 :goto_1

    .line 837
    :cond_23
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 838
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 839
    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->ab:Lcom/sec/vip/amschaton/o;

    invoke-interface {v3, v7, v0, v1}, Lcom/sec/vip/amschaton/o;->a(Ljava/lang/String;FF)Z

    goto :goto_2

    .line 842
    :sswitch_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/ac;->B:I

    .line 844
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v4

    .line 845
    iget-object v4, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    .line 846
    iget v5, p0, Lcom/sec/vip/amschaton/ac;->C:I

    int-to-float v5, v5

    sub-float v5, p1, v5

    float-to-int v5, v5

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 847
    iget v5, p0, Lcom/sec/vip/amschaton/ac;->D:I

    int-to-float v5, v5

    sub-float v5, p2, v5

    float-to-int v5, v5

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 848
    int-to-float v5, v0

    add-float/2addr v5, p1

    iget v6, p0, Lcom/sec/vip/amschaton/ac;->C:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    float-to-int v5, v5

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 849
    int-to-float v5, v4

    add-float/2addr v5, p2

    iget v6, p0, Lcom/sec/vip/amschaton/ac;->D:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    float-to-int v5, v5

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 851
    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/sec/vip/amschaton/ac;->N:I

    .line 852
    iget-object v5, p0, Lcom/sec/vip/amschaton/ac;->g:Landroid/graphics/Canvas;

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/sec/vip/amschaton/ac;->O:I

    .line 854
    iget-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    if-gez v5, :cond_26

    .line 855
    iput-short v1, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    .line 860
    :cond_24
    :goto_3
    iget-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    if-gez v5, :cond_27

    .line 861
    iput-short v1, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    .line 867
    :cond_25
    :goto_4
    iget-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    add-int/2addr v0, v5

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nRight:S

    .line 868
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    add-int/2addr v0, v4

    int-to-short v0, v0

    iput-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nBottom:S

    .line 870
    invoke-virtual {p0, v3}, Lcom/sec/vip/amschaton/ac;->b(Lcom/sec/amsoma/structure/AMS_RECT;)Z

    move-result v0

    .line 871
    iget-boolean v4, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    if-eqz v4, :cond_28

    .line 872
    iput-boolean v1, p0, Lcom/sec/vip/amschaton/ac;->I:Z

    goto/16 :goto_1

    .line 857
    :cond_26
    iget-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    iget v6, p0, Lcom/sec/vip/amschaton/ac;->N:I

    sub-int/2addr v6, v0

    if-le v5, v6, :cond_24

    .line 858
    iget v5, p0, Lcom/sec/vip/amschaton/ac;->N:I

    sub-int/2addr v5, v0

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    goto :goto_3

    .line 863
    :cond_27
    iget-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    iget v6, p0, Lcom/sec/vip/amschaton/ac;->O:I

    sub-int/2addr v6, v4

    if-le v5, v6, :cond_25

    .line 864
    iget v5, p0, Lcom/sec/vip/amschaton/ac;->O:I

    sub-int/2addr v5, v4

    int-to-short v5, v5

    iput-short v5, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    goto :goto_4

    .line 875
    :cond_28
    iget-short v0, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nLeft:S

    int-to-float v0, v0

    iget-short v3, v3, Lcom/sec/amsoma/structure/AMS_RECT;->nTop:S

    int-to-float v3, v3

    invoke-virtual {p0, v0, v3}, Lcom/sec/vip/amschaton/ac;->a(FF)V

    .line 876
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/ac;->G:Z

    if-nez v0, :cond_29

    .line 877
    iput-boolean v2, p0, Lcom/sec/vip/amschaton/ac;->G:Z

    move v0, v2

    .line 878
    goto/16 :goto_1

    .line 885
    :cond_29
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->ab:Lcom/sec/vip/amschaton/o;

    if-nez v0, :cond_2a

    move v0, v1

    .line 886
    goto/16 :goto_1

    .line 888
    :cond_2a
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 889
    iget-object v1, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->aa:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 890
    iget-object v3, p0, Lcom/sec/vip/amschaton/ac;->ab:Lcom/sec/vip/amschaton/o;

    invoke-interface {v3, v7, v0, v1}, Lcom/sec/vip/amschaton/o;->a(Ljava/lang/String;FF)Z

    goto/16 :goto_2

    .line 894
    :sswitch_2
    const/16 v0, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/sec/vip/amschaton/ac;->b(IZ)V

    .line 895
    iget-object v0, p0, Lcom/sec/vip/amschaton/ac;->S:Lcom/sec/vip/amschaton/v;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/v;->a()V

    goto/16 :goto_2

    .line 619
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 823
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3 -> :sswitch_1
        0x7 -> :sswitch_2
    .end sparse-switch
.end method
