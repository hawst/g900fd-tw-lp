.class public Lcom/sec/vip/amschaton/fragment/at;
.super Landroid/widget/BaseAdapter;
.source "AMSEmoticonSelectionFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;


# direct methods
.method private constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/at;->a:Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 91
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;Lcom/sec/vip/amschaton/fragment/as;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/fragment/at;-><init>(Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/al;->e()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 105
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 111
    if-nez p2, :cond_2

    .line 112
    new-instance v1, Lcom/sec/vip/amschaton/fragment/au;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/sec/vip/amschaton/fragment/au;-><init>(Lcom/sec/vip/amschaton/fragment/at;Lcom/sec/vip/amschaton/fragment/as;)V

    .line 113
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/at;->a:Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSEmoticonSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 114
    const v2, 0x7f030014

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 115
    const v0, 0x7f070068

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/sec/vip/amschaton/fragment/au;->a:Landroid/widget/LinearLayout;

    .line 116
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, v1, Lcom/sec/vip/amschaton/fragment/au;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 119
    :cond_0
    const v0, 0x7f070069

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/sec/vip/amschaton/fragment/au;->b:Landroid/widget/ImageView;

    .line 120
    invoke-static {}, Lcom/sec/chaton/global/GlobalApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, v1, Lcom/sec/vip/amschaton/fragment/au;->b:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 123
    :cond_1
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 128
    :goto_0
    invoke-static {}, Lcom/sec/vip/amschaton/al;->a()Lcom/sec/vip/amschaton/al;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/vip/amschaton/al;->b(I)I

    move-result v1

    .line 129
    iget-object v0, v0, Lcom/sec/vip/amschaton/fragment/au;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 131
    return-object p2

    .line 125
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/vip/amschaton/fragment/au;

    goto :goto_0
.end method
