.class Lcom/sec/vip/amschaton/fragment/ck;
.super Ljava/lang/Object;
.source "AMSStampSelectionFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v0

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_2

    .line 518
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)Lcom/sec/vip/amschaton/a/e;

    move-result-object v0

    if-nez v0, :cond_1

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 521
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)Lcom/sec/vip/amschaton/a/e;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Lcom/sec/vip/amschaton/a/e;->b(J)Landroid/database/Cursor;

    move-result-object v0

    .line 522
    if-eqz v0, :cond_0

    .line 525
    const-string v1, "ams_index"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 526
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 527
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1, v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)V

    goto :goto_0

    .line 531
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v0

    const/16 v1, 0x7d2

    if-eq v0, v1, :cond_0

    .line 534
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->h(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->i(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    add-int/2addr v0, v1

    .line 535
    add-int/lit8 v1, p3, -0x1

    .line 536
    if-ltz v1, :cond_4

    if-ge v1, v0, :cond_4

    .line 537
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)I

    .line 538
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->j(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;I)V

    .line 543
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->b(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    goto :goto_0

    .line 539
    :cond_4
    const/4 v0, -0x1

    if-ne v1, v0, :cond_3

    .line 541
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/ck;->a:Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;->k(Lcom/sec/vip/amschaton/fragment/AMSStampSelectionFragment;)V

    goto :goto_1
.end method
