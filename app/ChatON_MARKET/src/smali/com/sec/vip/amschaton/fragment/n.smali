.class public Lcom/sec/vip/amschaton/fragment/n;
.super Ljava/lang/Object;
.source "AMSBgSelectionFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1444
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/n;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1442
    const/16 v0, 0x1389

    iput v0, p0, Lcom/sec/vip/amschaton/fragment/n;->d:I

    .line 1445
    iput-object p2, p0, Lcom/sec/vip/amschaton/fragment/n;->c:Landroid/content/Context;

    .line 1446
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/n;->b:Ljava/util/Map;

    .line 1447
    iput p3, p0, Lcom/sec/vip/amschaton/fragment/n;->d:I

    .line 1448
    return-void
.end method


# virtual methods
.method public a(I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1451
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/n;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1452
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/n;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1467
    :cond_0
    :goto_0
    return-object v0

    .line 1455
    :cond_1
    const/4 v0, 0x0

    .line 1456
    iget v1, p0, Lcom/sec/vip/amschaton/fragment/n;->d:I

    packed-switch v1, :pswitch_data_0

    .line 1464
    :goto_1
    if-eqz v0, :cond_0

    .line 1465
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/n;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1458
    :pswitch_0
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/n;->c:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, v2}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/Context;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1461
    :pswitch_1
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/n;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1, v2}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/res/AssetManager;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 1456
    :pswitch_data_0
    .packed-switch 0x1388
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(ILandroid/widget/ImageView;I)V
    .locals 2

    .prologue
    .line 1471
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/n;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1472
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/n;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1501
    :goto_0
    return-void

    .line 1476
    :cond_0
    if-eqz p2, :cond_1

    .line 1477
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1479
    :cond_1
    new-instance v0, Lcom/sec/vip/amschaton/fragment/o;

    invoke-direct {v0, p0, p2, p3}, Lcom/sec/vip/amschaton/fragment/o;-><init>(Lcom/sec/vip/amschaton/fragment/n;Landroid/widget/ImageView;I)V

    .line 1492
    new-instance v1, Lcom/sec/vip/amschaton/fragment/p;

    invoke-direct {v1, p0, p1, v0}, Lcom/sec/vip/amschaton/fragment/p;-><init>(Lcom/sec/vip/amschaton/fragment/n;ILandroid/os/Handler;)V

    .line 1500
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
