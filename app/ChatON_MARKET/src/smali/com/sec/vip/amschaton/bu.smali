.class Lcom/sec/vip/amschaton/bu;
.super Ljava/lang/Object;
.source "ZoomableImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/ZoomableImageView;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 844
    iput-object p1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 847
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v0, v1

    .line 849
    sub-float v1, v0, v5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v1, v1

    const-wide v3, 0x3fa999999999999aL    # 0.05

    cmpl-double v1, v1, v3

    if-lez v1, :cond_3

    .line 850
    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    .line 851
    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 852
    sub-float/2addr v0, v5

    .line 853
    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    const v2, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v2

    add-float/2addr v0, v5

    iput v0, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    .line 855
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 857
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 858
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 859
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iput v5, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    .line 872
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_2

    .line 873
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    iget-object v3, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v3, v3, Lcom/sec/vip/amschaton/ZoomableImageView;->r:F

    iget-object v4, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v4, v4, Lcom/sec/vip/amschaton/ZoomableImageView;->s:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 874
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->b(Lcom/sec/vip/amschaton/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/vip/amschaton/ZoomableImageView;->c(Lcom/sec/vip/amschaton/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 875
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->invalidate()V

    .line 894
    :goto_1
    return-void

    .line 862
    :cond_1
    sub-float v0, v5, v0

    .line 863
    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    sub-float v0, v5, v0

    iput v0, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    .line 864
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 866
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 867
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 868
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iput v5, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    goto :goto_0

    .line 877
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iput-boolean v6, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    .line 878
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iput v5, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    .line 879
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    iget-object v3, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v3, v3, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v3, v3, Lcom/sec/vip/amschaton/ZoomableImageView;->r:F

    iget-object v4, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v4, v4, Lcom/sec/vip/amschaton/ZoomableImageView;->s:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 880
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 881
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->b(Lcom/sec/vip/amschaton/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/vip/amschaton/ZoomableImageView;->c(Lcom/sec/vip/amschaton/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 882
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->invalidate()V

    .line 883
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->d(Lcom/sec/vip/amschaton/ZoomableImageView;)V

    goto :goto_1

    .line 886
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iput-boolean v6, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->w:Z

    .line 887
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iput v5, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->t:F

    .line 888
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v0, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    iget-object v2, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v2, v2, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    iget-object v3, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v3, v3, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v3, v3, Lcom/sec/vip/amschaton/ZoomableImageView;->r:F

    iget-object v4, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v4, v4, Lcom/sec/vip/amschaton/ZoomableImageView;->s:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 889
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    iget v1, v1, Lcom/sec/vip/amschaton/ZoomableImageView;->q:F

    iput v1, v0, Lcom/sec/vip/amschaton/ZoomableImageView;->k:F

    .line 890
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->b(Lcom/sec/vip/amschaton/ZoomableImageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v1}, Lcom/sec/vip/amschaton/ZoomableImageView;->c(Lcom/sec/vip/amschaton/ZoomableImageView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 891
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->invalidate()V

    .line 892
    iget-object v0, p0, Lcom/sec/vip/amschaton/bu;->a:Lcom/sec/vip/amschaton/ZoomableImageView;

    invoke-static {v0}, Lcom/sec/vip/amschaton/ZoomableImageView;->d(Lcom/sec/vip/amschaton/ZoomableImageView;)V

    goto/16 :goto_1
.end method
