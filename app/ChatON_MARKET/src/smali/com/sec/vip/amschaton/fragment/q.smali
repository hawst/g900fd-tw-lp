.class public Lcom/sec/vip/amschaton/fragment/q;
.super Ljava/lang/Object;
.source "AMSBgSelectionFragment.java"


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;)V
    .locals 1

    .prologue
    .line 1059
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/q;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1060
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/q;->b:Ljava/util/Map;

    .line 1061
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1064
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1065
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1089
    :cond_0
    :goto_0
    return-object v0

    .line 1068
    :cond_1
    const/4 v0, 0x0

    .line 1069
    const-string v1, "d"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1071
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1072
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/q;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1086
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 1087
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/q;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1074
    :cond_3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1075
    const/16 v2, 0x2710

    if-ge v1, v2, :cond_2

    .line 1077
    invoke-static {}, Lcom/sec/vip/amschaton/b;->a()Lcom/sec/vip/amschaton/b;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/q;->a:Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;

    invoke-virtual {v2}, Lcom/sec/vip/amschaton/fragment/AMSBgSelectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-virtual {v0, v2, v1, v3}, Lcom/sec/vip/amschaton/b;->a(Landroid/content/res/AssetManager;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1094
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1097
    :cond_0
    new-instance v0, Lcom/sec/vip/amschaton/fragment/r;

    invoke-direct {v0, p0, p2}, Lcom/sec/vip/amschaton/fragment/r;-><init>(Lcom/sec/vip/amschaton/fragment/q;Landroid/widget/ImageView;)V

    .line 1104
    new-instance v1, Lcom/sec/vip/amschaton/fragment/s;

    invoke-direct {v1, p0, p1, v0}, Lcom/sec/vip/amschaton/fragment/s;-><init>(Lcom/sec/vip/amschaton/fragment/q;Ljava/lang/String;Landroid/os/Handler;)V

    .line 1112
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1113
    return-void
.end method
