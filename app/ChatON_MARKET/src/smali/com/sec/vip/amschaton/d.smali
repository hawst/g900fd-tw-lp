.class public Lcom/sec/vip/amschaton/d;
.super Ljava/lang/Object;
.source "AMSBgColorSelectionDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field protected a:Lcom/sec/vip/amschaton/i;

.field private b:Landroid/content/Context;

.field private c:Landroid/view/View;

.field private d:Lcom/sec/common/a/d;

.field private e:I

.field private f:[Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:[I

.field private i:[I

.field private j:Lcom/sec/vip/amschaton/bi;

.field private k:Landroid/view/View$OnClickListener;

.field private l:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/16 v4, 0xe

    const/4 v2, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v2, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    .line 25
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/vip/amschaton/d;->e:I

    .line 27
    iput-object v2, p0, Lcom/sec/vip/amschaton/d;->f:[Landroid/widget/ImageView;

    .line 28
    iput-object v2, p0, Lcom/sec/vip/amschaton/d;->g:Landroid/widget/ImageView;

    .line 30
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/vip/amschaton/d;->h:[I

    .line 36
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/vip/amschaton/d;->i:[I

    .line 44
    iput-object v2, p0, Lcom/sec/vip/amschaton/d;->j:Lcom/sec/vip/amschaton/bi;

    .line 46
    new-instance v0, Lcom/sec/vip/amschaton/e;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/e;-><init>(Lcom/sec/vip/amschaton/d;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/d;->k:Landroid/view/View$OnClickListener;

    .line 63
    new-instance v0, Lcom/sec/vip/amschaton/f;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/f;-><init>(Lcom/sec/vip/amschaton/d;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/d;->l:Landroid/view/View$OnClickListener;

    .line 71
    iput-object p1, p0, Lcom/sec/vip/amschaton/d;->b:Landroid/content/Context;

    .line 73
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 74
    const v1, 0x7f030055

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/d;->c:Landroid/view/View;

    .line 76
    new-array v0, v4, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/d;->f:[Landroid/widget/ImageView;

    .line 77
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 78
    iget-object v2, p0, Lcom/sec/vip/amschaton/d;->f:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->c:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/vip/amschaton/d;->h:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v2, v1

    .line 79
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->f:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/d;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->c:Landroid/view/View;

    const v1, 0x7f07022d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/d;->g:Landroid/widget/ImageView;

    .line 82
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/d;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget v0, p0, Lcom/sec/vip/amschaton/d;->e:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/d;->c(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/d;->a(IZ)V

    .line 84
    return-void

    .line 30
    nop

    :array_0
    .array-data 4
        0x7f07021f
        0x7f070220
        0x7f070221
        0x7f070222
        0x7f070223
        0x7f070224
        0x7f070225
        0x7f070226
        0x7f070227
        0x7f070228
        0x7f070229
        0x7f07022a
        0x7f07022b
        0x7f07022c
    .end array-data

    .line 36
    :array_1
    .array-data 4
        0x7f080028
        0x7f080029
        0x7f08002a
        0x7f08002b
        0x7f08002c
        0x7f08002d
        0x7f08002e
        0x7f08002f
        0x7f080030
        0x7f080032
        0x7f080033
        0x7f080034
        0x7f080035
        0x7f080036
    .end array-data
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/d;I)I
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/d;->c(I)I

    move-result v0

    return v0
.end method

.method private a(IZ)V
    .locals 4

    .prologue
    const/16 v3, 0xe

    const/4 v1, 0x0

    .line 125
    if-lt p1, v3, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    if-gez p1, :cond_3

    move v0, v1

    .line 129
    :goto_1
    if-ge v0, v3, :cond_2

    .line 130
    iget-object v2, p0, Lcom/sec/vip/amschaton/d;->f:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 129
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 137
    :cond_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->f:[Landroid/widget/ImageView;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/d;IZ)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/d;->a(IZ)V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/d;)[Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->f:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/d;)I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/sec/vip/amschaton/d;->e:I

    return v0
.end method

.method private c(I)I
    .locals 3

    .prologue
    .line 141
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xe

    if-ge v0, v1, :cond_1

    .line 142
    iget-object v1, p0, Lcom/sec/vip/amschaton/d;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/d;->i:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 146
    :goto_1
    return v0

    .line 141
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/d;)[I
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->i:[I

    return-object v0
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/d;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/d;)Lcom/sec/vip/amschaton/bi;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->j:Lcom/sec/vip/amschaton/bi;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/vip/amschaton/d;)Lcom/sec/common/a/d;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->d:Lcom/sec/common/a/d;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/common/a/a;->a(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 88
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(Z)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b012e

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->a(I)Lcom/sec/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/d;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/common/a/a;->b(Landroid/view/View;)Lcom/sec/common/a/a;

    move-result-object v0

    const v1, 0x7f0b0039

    invoke-virtual {v0, v1, p0}, Lcom/sec/common/a/a;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->a()Lcom/sec/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/d;->d:Lcom/sec/common/a/d;

    .line 94
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/vip/amschaton/d;->e:I

    .line 98
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/d;->a(IZ)V

    .line 99
    iget v0, p0, Lcom/sec/vip/amschaton/d;->e:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/d;->c(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/d;->a(IZ)V

    .line 100
    return-void
.end method

.method public a(Lcom/sec/vip/amschaton/bi;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/vip/amschaton/d;->j:Lcom/sec/vip/amschaton/bi;

    .line 122
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/sec/vip/amschaton/d;->e:I

    return v0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 164
    new-instance v0, Lcom/sec/vip/amschaton/i;

    iget-object v1, p0, Lcom/sec/vip/amschaton/d;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    .line 165
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/i;->a(I)V

    .line 166
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    new-instance v1, Lcom/sec/vip/amschaton/g;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/g;-><init>(Lcom/sec/vip/amschaton/d;)V

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/i;->a(Lcom/sec/vip/amschaton/bi;)V

    .line 188
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/i;->d()V

    .line 189
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->d:Lcom/sec/common/a/d;

    if-nez v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/d;->a()V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->d:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->show()V

    .line 111
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/i;->e()V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->d:Lcom/sec/common/a/d;

    invoke-interface {v0}, Lcom/sec/common/a/d;->dismiss()V

    .line 118
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/i;->c()Z

    move-result v0

    .line 153
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->a:Lcom/sec/vip/amschaton/i;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/i;->b()I

    move-result v0

    .line 160
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/d;->e:I

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->j:Lcom/sec/vip/amschaton/bi;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/vip/amschaton/d;->j:Lcom/sec/vip/amschaton/bi;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/bi;->a()Z

    .line 196
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 197
    return-void
.end method
