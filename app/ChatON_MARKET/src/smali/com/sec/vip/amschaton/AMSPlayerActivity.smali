.class public Lcom/sec/vip/amschaton/AMSPlayerActivity;
.super Lcom/sec/vip/amschaton/AMSViewerActivity;
.source "AMSPlayerActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/vip/amschaton/AMSViewerActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 78
    if-eq p2, v2, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSPlayerActivity;->finish()V

    .line 100
    :goto_0
    return-void

    .line 83
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 97
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSPlayerActivity;->finish()V

    goto :goto_0

    .line 86
    :pswitch_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 87
    if-nez v0, :cond_1

    .line 88
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSPlayerActivity;->finish()V

    goto :goto_0

    .line 91
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 92
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 93
    invoke-virtual {p0, v2, v1}, Lcom/sec/vip/amschaton/AMSPlayerActivity;->setResult(ILandroid/content/Intent;)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSPlayerActivity;->finish()V

    goto :goto_0

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 23
    const-string v0, "[onCreate]"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/AMSViewerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 41
    if-nez v0, :cond_0

    .line 42
    const-string v0, "[onCreate] bundle is NULL!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/AMSPlayerActivity;->finish()V

    .line 74
    :goto_0
    return-void

    .line 64
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/vip/amschaton/BaseAMSActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 70
    const-string v0, "AMS_ACTION"

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 73
    const/16 v0, 0x3e8

    invoke-virtual {p0, v1, v0}, Lcom/sec/vip/amschaton/AMSPlayerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
