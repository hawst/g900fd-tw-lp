.class public Lcom/sec/vip/amschaton/bq;
.super Ljava/lang/Object;
.source "Viewer.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static a:I


# instance fields
.field private b:I

.field private c:Z

.field private d:Landroid/content/Context;

.field private e:I

.field private f:I

.field private g:I

.field private h:Landroid/os/Handler;

.field private i:Lcom/sec/vip/amschaton/AMSDrawManager;

.field private j:Landroid/media/MediaPlayer;

.field private k:Z

.field private l:Lcom/sec/vip/amschaton/bs;

.field private m:I

.field private n:Landroid/os/PowerManager$WakeLock;

.field private o:Lcom/sec/vip/amschaton/br;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/16 v0, 0xfa

    sput v0, Lcom/sec/vip/amschaton/bq;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/16 v6, 0xfa

    const/16 v5, 0xa

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput v5, p0, Lcom/sec/vip/amschaton/bq;->b:I

    .line 51
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/bq;->c:Z

    .line 54
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/vip/amschaton/bq;->e:I

    .line 55
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/vip/amschaton/bq;->f:I

    .line 58
    iput v3, p0, Lcom/sec/vip/amschaton/bq;->g:I

    .line 61
    iput-object v1, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    .line 68
    iput-object v1, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    .line 70
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/bq;->k:Z

    .line 76
    iput-object v1, p0, Lcom/sec/vip/amschaton/bq;->l:Lcom/sec/vip/amschaton/bs;

    .line 77
    iput v3, p0, Lcom/sec/vip/amschaton/bq;->m:I

    .line 93
    iput-object v1, p0, Lcom/sec/vip/amschaton/bq;->o:Lcom/sec/vip/amschaton/br;

    .line 102
    iput-object p1, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    .line 105
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 106
    const/16 v1, 0x1a

    const-string v2, "[AMS] Don\'t Sleep!"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/bq;->n:Landroid/os/PowerManager$WakeLock;

    .line 108
    sget-boolean v0, Lcom/sec/vip/amschaton/AMSActivity;->j:Z

    if-eqz v0, :cond_0

    .line 109
    sput v6, Lcom/sec/vip/amschaton/bq;->a:I

    .line 110
    iput v4, p0, Lcom/sec/vip/amschaton/bq;->b:I

    .line 111
    iput-boolean v4, p0, Lcom/sec/vip/amschaton/bq;->c:Z

    .line 117
    :goto_0
    return-void

    .line 113
    :cond_0
    sput v6, Lcom/sec/vip/amschaton/bq;->a:I

    .line 114
    iput v5, p0, Lcom/sec/vip/amschaton/bq;->b:I

    .line 115
    iput-boolean v3, p0, Lcom/sec/vip/amschaton/bq;->c:Z

    goto :goto_0
.end method

.method private i()V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->o:Lcom/sec/vip/amschaton/br;

    if-eqz v0, :cond_1

    .line 359
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->o:Lcom/sec/vip/amschaton/br;

    invoke-interface {v0}, Lcom/sec/vip/amschaton/br;->a()V

    .line 361
    :cond_1
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v0, :cond_0

    .line 413
    const/4 v0, 0x0

    .line 415
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->n()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->m()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    sget v1, Lcom/sec/vip/amschaton/bq;->a:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public a(Lcom/sec/vip/amschaton/br;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/sec/vip/amschaton/bq;->o:Lcom/sec/vip/amschaton/br;

    .line 352
    return-void
.end method

.method public a(Lcom/sec/vip/amschaton/bs;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/vip/amschaton/bq;->l:Lcom/sec/vip/amschaton/bs;

    .line 126
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 523
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 525
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 529
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/bq;->k:Z

    if-eqz v0, :cond_2

    .line 530
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 531
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 533
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 536
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v0, :cond_3

    .line 537
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    const v1, 0x7f0b015e

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 538
    const-string v0, "mAMSViewer is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :goto_0
    return-void

    .line 541
    :cond_3
    iget v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    .line 542
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->o()V

    .line 543
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->g()Z

    .line 545
    :cond_4
    if-eqz p1, :cond_5

    .line 546
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0, v2}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Z)V

    .line 548
    :cond_5
    iput v2, p0, Lcom/sec/vip/amschaton/bq;->g:I

    .line 549
    invoke-direct {p0}, Lcom/sec/vip/amschaton/bq;->j()V

    goto :goto_0
.end method

.method public a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 153
    iput p1, p0, Lcom/sec/vip/amschaton/bq;->e:I

    .line 154
    iput p1, p0, Lcom/sec/vip/amschaton/bq;->f:I

    .line 155
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v1, :cond_0

    .line 156
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->u()V

    .line 157
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    .line 159
    :cond_0
    new-instance v1, Lcom/sec/vip/amschaton/AMSDrawManager;

    iget-object v2, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    iget v3, p0, Lcom/sec/vip/amschaton/bq;->e:I

    iget v4, p0, Lcom/sec/vip/amschaton/bq;->f:I

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/vip/amschaton/AMSDrawManager;-><init>(Landroid/content/Context;II)V

    iput-object v1, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    .line 160
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 161
    const/4 v0, 0x0

    .line 164
    :goto_0
    return v0

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v1, v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->setPlayMode(Z)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 197
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v1, :cond_0

    .line 198
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    const v2, 0x7f0b015e

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 199
    const-string v1, "mAMSViewer is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :goto_0
    return v0

    .line 202
    :cond_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v1, p1}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Ljava/lang/String;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 214
    if-eqz p2, :cond_1

    .line 215
    invoke-direct {p0}, Lcom/sec/vip/amschaton/bq;->i()V

    .line 217
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 204
    :pswitch_0
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    const v2, 0x7f0b00f7

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 205
    const-string v1, "Load Error"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :pswitch_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    const v2, 0x7f0b00f9

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 211
    :pswitch_2
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    const v2, 0x7f0b0149

    invoke-static {v1, v2, v0}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x3

    .line 422
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 423
    iget v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    packed-switch v0, :pswitch_data_0

    .line 471
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "play() - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/bq;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    :goto_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/vip/amschaton/h;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/vip/amschaton/h;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/vip/amschaton/h;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 476
    :cond_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/bq;->e()V

    .line 480
    :cond_1
    :goto_1
    return-void

    .line 425
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-nez v0, :cond_2

    .line 426
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->d:Landroid/content/Context;

    const v1, 0x7f0b015e

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/widget/ai;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 427
    const-string v0, "mAMSViewer is null"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 430
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 434
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 436
    :cond_3
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    .line 437
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/vip/amschaton/bq;->b:I

    int-to-long v1, v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 439
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_4

    .line 440
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 441
    iput v4, p0, Lcom/sec/vip/amschaton/bq;->g:I

    goto :goto_0

    .line 443
    :cond_4
    iput v3, p0, Lcom/sec/vip/amschaton/bq;->g:I

    goto :goto_0

    .line 447
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    if-eqz v0, :cond_5

    .line 448
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/vip/amschaton/bq;->b:I

    int-to-long v1, v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 451
    :cond_5
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_6

    .line 452
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 454
    :cond_6
    iput v4, p0, Lcom/sec/vip/amschaton/bq;->g:I

    goto/16 :goto_0

    .line 458
    :pswitch_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    if-eqz v0, :cond_7

    .line 459
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/vip/amschaton/bq;->b:I

    int-to-long v1, v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 461
    :cond_7
    iput v3, p0, Lcom/sec/vip/amschaton/bq;->g:I

    goto/16 :goto_0

    .line 465
    :pswitch_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_8

    .line 466
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 468
    :cond_8
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    goto/16 :goto_0

    .line 478
    :cond_9
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/bq;->f()V

    goto/16 :goto_1

    .line 423
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 556
    if-eqz p1, :cond_0

    .line 557
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/bq;->a(Z)V

    .line 559
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 560
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 561
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    .line 563
    :cond_1
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 489
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    packed-switch v0, :pswitch_data_0

    .line 512
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pause() - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/bq;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    :goto_0
    return-void

    .line 492
    :pswitch_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 497
    :cond_1
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    goto :goto_0

    .line 500
    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    goto :goto_0

    .line 504
    :pswitch_3
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 505
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 506
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 509
    :cond_2
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    goto :goto_0

    .line 489
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public d()V
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->u()V

    .line 571
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    .line 573
    :cond_0
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 579
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 582
    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 588
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 591
    :cond_0
    return-void
.end method

.method public g()I
    .locals 1

    .prologue
    .line 599
    iget v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    return v0
.end method

.method public h()Landroid/view/View;
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    return-object v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 368
    iget v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/AMSDrawManager;->i()Z

    move-result v0

    .line 375
    if-nez v0, :cond_2

    .line 377
    iget v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    sparse-switch v0, :sswitch_data_0

    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AMS done.. but - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/vip/amschaton/bq;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->l:Lcom/sec/vip/amschaton/bs;

    if-eqz v0, :cond_4

    .line 393
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/bq;->a()I

    move-result v0

    .line 394
    iget v1, p0, Lcom/sec/vip/amschaton/bq;->m:I

    if-le v0, v1, :cond_3

    .line 395
    iget-object v1, p0, Lcom/sec/vip/amschaton/bq;->l:Lcom/sec/vip/amschaton/bs;

    invoke-interface {v1, v0}, Lcom/sec/vip/amschaton/bs;->a(I)V

    .line 397
    :cond_3
    iput v0, p0, Lcom/sec/vip/amschaton/bq;->m:I

    .line 399
    :cond_4
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->i:Lcom/sec/vip/amschaton/AMSDrawManager;

    iget-boolean v1, p0, Lcom/sec/vip/amschaton/bq;->c:Z

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/AMSDrawManager;->a(Z)V

    .line 400
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/sec/vip/amschaton/bq;->h:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/vip/amschaton/bq;->b:I

    int-to-long v1, v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 380
    :sswitch_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    goto :goto_1

    .line 384
    :sswitch_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/bq;->g:I

    .line 385
    invoke-direct {p0}, Lcom/sec/vip/amschaton/bq;->j()V

    goto :goto_1

    .line 377
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x7 -> :sswitch_0
    .end sparse-switch
.end method
