.class Lcom/sec/vip/amschaton/fragment/bf;
.super Ljava/lang/Object;
.source "AMSListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;


# direct methods
.method constructor <init>(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)V
    .locals 0

    .prologue
    .line 632
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 635
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)Z

    .line 637
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    const/16 v1, 0xfa0

    if-ne v0, v1, :cond_1

    .line 638
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0, p4, p5}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->a(Lcom/sec/vip/amschaton/fragment/AMSListFragment;J)J

    .line 639
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)Landroid/widget/GridView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->openContextMenu(Landroid/view/View;)V

    .line 656
    :cond_0
    :goto_0
    return v2

    .line 643
    :cond_1
    if-eqz p3, :cond_0

    .line 646
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->d(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    const/16 v1, 0x7d2

    if-eq v0, v1, :cond_0

    .line 650
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    if-lt p3, v0, :cond_2

    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->e(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->f(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->g(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    if-ge p3, v0, :cond_2

    .line 652
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0, v2}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSListFragment;Z)Z

    .line 654
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v0, p3}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->b(Lcom/sec/vip/amschaton/fragment/AMSListFragment;I)I

    .line 655
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/bf;->a:Lcom/sec/vip/amschaton/fragment/AMSListFragment;

    invoke-static {v1}, Lcom/sec/vip/amschaton/fragment/AMSListFragment;->c(Lcom/sec/vip/amschaton/fragment/AMSListFragment;)Landroid/widget/GridView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->openContextMenu(Landroid/view/View;)V

    goto :goto_0
.end method
