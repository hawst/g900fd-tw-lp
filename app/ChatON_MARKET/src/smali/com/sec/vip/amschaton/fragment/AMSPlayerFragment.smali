.class public Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;
.super Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;
.source "AMSPlayerFragment.java"


# instance fields
.field private S:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 72
    const v0, 0x7f03005e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;)Z
    .locals 8

    .prologue
    const/16 v7, 0xd

    const/4 v6, 0x1

    const/4 v4, -0x2

    const/4 v5, 0x0

    .line 80
    const v0, 0x7f07023c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 81
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->a:I

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->a:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 82
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "## mViewerSize = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 84
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 88
    const v1, 0x7f070273

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->F:Landroid/widget/RelativeLayout;

    .line 89
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->F:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->Q:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->A:Landroid/widget/ImageView;

    .line 94
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->A:Landroid/widget/ImageView;

    const v2, 0x7f02012b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 95
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 96
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 97
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->A:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 98
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->A:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->Q:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    const v1, 0x7f070271

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->B:Landroid/view/View;

    .line 101
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->B:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->Q:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->P:Landroid/widget/RelativeLayout;

    .line 105
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->P:Landroid/widget/RelativeLayout;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->a:I

    iget v4, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->a:I

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 106
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->P:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->A:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 107
    invoke-virtual {p0, v5}, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->b(Z)V

    .line 108
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->P:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 111
    const v0, 0x7f070270

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->G:Landroid/widget/LinearLayout;

    .line 112
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->N:Z

    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p0, v5}, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->a(Z)V

    .line 116
    :cond_0
    const v0, 0x7f070272

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->z:Landroid/widget/SeekBar;

    .line 117
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->z:Landroid/widget/SeekBar;

    new-instance v1, Lcom/sec/vip/amschaton/fragment/bx;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/bx;-><init>(Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 123
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->z:Landroid/widget/SeekBar;

    sget v1, Lcom/sec/vip/amschaton/bq;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->z:Landroid/widget/SeekBar;

    invoke-virtual {v0, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 125
    invoke-virtual {p0, v6}, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->c(Z)V

    .line 127
    return v6
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->onAttach(Landroid/app/Activity;)V

    .line 60
    iput-object p1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->S:Landroid/app/Activity;

    .line 61
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 47
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 48
    invoke-super {p0, p1}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->onCreate(Landroid/os/Bundle;)V

    .line 50
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->w:Z

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0225

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 53
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5

    .prologue
    const v4, 0x7f07058c

    const v3, 0x7f070589

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 137
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->w:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->s:Z

    if-nez v0, :cond_2

    .line 138
    const v0, 0x7f0f000e

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 139
    const v0, 0x7f07058a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 140
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->g()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 144
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 150
    :goto_0
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    .line 168
    :cond_0
    :goto_1
    invoke-super {p0, p1, p2}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 169
    return-void

    .line 148
    :cond_1
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 153
    :cond_2
    const v0, 0x7f0f0031

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 154
    const v0, 0x7f0705b2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 155
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->b:Lcom/sec/vip/amschaton/bq;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->b:Lcom/sec/vip/amschaton/bq;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/bq;->g()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    .line 159
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 165
    :goto_2
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/chaton/util/cp;->a(Landroid/view/MenuItem;)V

    goto :goto_1

    .line 163
    :cond_3
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->onDetach()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->S:Landroid/app/Activity;

    .line 69
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 173
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 216
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 179
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->f()Ljava/lang/String;

    move-result-object v0

    .line 180
    sget-boolean v1, Lcom/sec/chaton/util/y;->b:Z

    if-eqz v1, :cond_1

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "filePath     : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    :cond_1
    iget-boolean v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->w:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->x:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 185
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/d/e;->d:Lcom/sec/chaton/d/e;

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->x:Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2, v3, v4}, Lcom/sec/chaton/settings/downloads/q;->a(Landroid/content/Context;Lcom/sec/chaton/d/e;Ljava/lang/String;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->c()V

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    .line 187
    sget-boolean v1, Lcom/sec/chaton/util/y;->e:Z

    if-eqz v1, :cond_2

    .line 188
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 196
    :pswitch_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;->S:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/chaton/settings/downloads/av;->d(Landroid/content/Context;)Lcom/sec/common/a/a;

    move-result-object v0

    .line 198
    const v1, 0x7f0b0037

    new-instance v2, Lcom/sec/vip/amschaton/fragment/by;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/fragment/by;-><init>(Lcom/sec/vip/amschaton/fragment/AMSPlayerFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/common/a/a;->d(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sec/common/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/common/a/a;->b()Lcom/sec/common/a/d;

    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x7f070589
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
