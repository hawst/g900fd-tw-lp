.class public Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;
.super Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;
.source "AMSPreviewFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 29
    const v0, 0x7f030061

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;)Z
    .locals 8

    .prologue
    const/16 v7, 0xd

    const/4 v6, 0x1

    const/4 v4, -0x2

    const/4 v5, 0x0

    .line 122
    const v0, 0x7f07023c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 123
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a:I

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 124
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "## mViewerSize = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 126
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 130
    const v1, 0x7f070273

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->F:Landroid/widget/RelativeLayout;

    .line 131
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->F:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->Q:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->A:Landroid/widget/ImageView;

    .line 136
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->A:Landroid/widget/ImageView;

    const v2, 0x7f02012b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 137
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 138
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 139
    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->A:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 140
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->A:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->Q:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    const v1, 0x7f070271

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->B:Landroid/view/View;

    .line 143
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->B:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->Q:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->P:Landroid/widget/RelativeLayout;

    .line 147
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->P:Landroid/widget/RelativeLayout;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    iget v3, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a:I

    iget v4, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a:I

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 148
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->P:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->A:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 149
    invoke-virtual {p0, v5}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->b(Z)V

    .line 150
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->P:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 153
    const v0, 0x7f070283

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->C:Landroid/view/View;

    .line 154
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->o:I

    const/16 v1, 0x7d0

    if-ne v0, v1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->C:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 158
    :cond_0
    const v0, 0x7f070126

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->D:Landroid/view/View;

    .line 159
    const v0, 0x7f070284

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->E:Landroid/view/View;

    .line 160
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->C:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->D:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->E:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    const v0, 0x7f070244

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->H:Landroid/widget/LinearLayout;

    .line 192
    const v0, 0x7f070270

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->G:Landroid/widget/LinearLayout;

    .line 193
    iget-boolean v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->N:Z

    if-nez v0, :cond_1

    .line 194
    invoke-virtual {p0, v5}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a(Z)V

    .line 197
    :cond_1
    const v0, 0x7f070272

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->z:Landroid/widget/SeekBar;

    .line 198
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->z:Landroid/widget/SeekBar;

    new-instance v1, Lcom/sec/vip/amschaton/fragment/bz;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/fragment/bz;-><init>(Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 204
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->z:Landroid/widget/SeekBar;

    sget v1, Lcom/sec/vip/amschaton/bq;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 205
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->z:Landroid/widget/SeekBar;

    invoke-virtual {v0, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 206
    invoke-virtual {p0, v6}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->c(Z)V

    .line 208
    return v6
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 101
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "AMS_ACTION"

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 102
    if-ne v0, v2, :cond_1

    .line 103
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 104
    instance-of v1, v0, Lcom/sec/vip/amschaton/BaseAMSActivity;

    if-eqz v1, :cond_0

    .line 106
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/vip/amschaton/BaseAMSActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 108
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 109
    const-string v2, "AMS_ACTION"

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 110
    check-cast v0, Lcom/sec/chaton/base/BaseActivity;

    invoke-virtual {v0, v1}, Lcom/sec/chaton/base/BaseActivity;->a(Landroid/content/Intent;)V

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->m:Ljava/lang/String;

    move-object v0, p0

    move v3, v2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a(Ljava/lang/String;ZZZZ)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 59
    .line 60
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 97
    :goto_0
    return-void

    .line 62
    :sswitch_0
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->o:I

    const/16 v1, 0x7d2

    if-ne v0, v1, :cond_0

    .line 64
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/sec/vip/amschaton/q;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 77
    :goto_1
    if-ne v0, v2, :cond_2

    .line 78
    const v0, 0x7f0b00f6

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a(I)V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->m:Ljava/lang/String;

    .line 85
    :goto_2
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->c()V

    goto :goto_0

    .line 66
    :cond_0
    iget v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->o:I

    const/16 v1, 0x7d0

    if-ne v0, v1, :cond_1

    .line 69
    invoke-static {}, Lcom/sec/vip/amschaton/q;->a()Lcom/sec/vip/amschaton/q;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/sec/vip/amschaton/q;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 82
    :cond_2
    const-string v0, "Selected item was not deleted!"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 88
    :sswitch_1
    iget-object v1, p0, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->m:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a(Ljava/lang/String;ZZZZ)V

    goto :goto_0

    .line 91
    :sswitch_2
    invoke-static {}, Lcom/sec/chaton/util/bl;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 92
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->g()V

    .line 94
    :cond_3
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->a()V

    goto :goto_0

    .line 60
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f070126 -> :sswitch_1
        0x7f070283 -> :sswitch_0
        0x7f070284 -> :sswitch_2
    .end sparse-switch
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0}, Lcom/sec/vip/amschaton/fragment/AMSViewerFragment;->onStart()V

    .line 36
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/fragment/AMSPreviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0051

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 37
    return-void
.end method
