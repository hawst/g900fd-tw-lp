.class public Lcom/sec/vip/amschaton/az;
.super Lcom/sec/vip/amschaton/aw;
.source "AMSToolTextPopup.java"


# instance fields
.field private g:I

.field private h:[Landroid/widget/ImageView;

.field private i:I

.field private j:[Landroid/widget/ImageView;

.field private k:Landroid/widget/ImageView;

.field private l:[I

.field private m:[I

.field private n:Landroid/view/View;

.field private o:Landroid/view/View$OnClickListener;

.field private p:Landroid/view/View$OnClickListener;

.field private q:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/view/View;Z)V
    .locals 3

    .prologue
    const/16 v2, 0xf

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p4}, Lcom/sec/vip/amschaton/aw;-><init>(Landroid/content/Context;IZ)V

    .line 18
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/vip/amschaton/az;->g:I

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/vip/amschaton/az;->i:I

    .line 28
    iput-object v1, p0, Lcom/sec/vip/amschaton/az;->j:[Landroid/widget/ImageView;

    .line 29
    iput-object v1, p0, Lcom/sec/vip/amschaton/az;->k:Landroid/widget/ImageView;

    .line 30
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/vip/amschaton/az;->l:[I

    .line 32
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/vip/amschaton/az;->m:[I

    .line 34
    iput-object v1, p0, Lcom/sec/vip/amschaton/az;->n:Landroid/view/View;

    .line 114
    new-instance v0, Lcom/sec/vip/amschaton/bd;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/bd;-><init>(Lcom/sec/vip/amschaton/az;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/az;->o:Landroid/view/View$OnClickListener;

    .line 176
    new-instance v0, Lcom/sec/vip/amschaton/be;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/be;-><init>(Lcom/sec/vip/amschaton/az;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/az;->p:Landroid/view/View$OnClickListener;

    .line 217
    new-instance v0, Lcom/sec/vip/amschaton/bg;

    invoke-direct {v0, p0}, Lcom/sec/vip/amschaton/bg;-><init>(Lcom/sec/vip/amschaton/az;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/az;->q:Landroid/view/View$OnClickListener;

    .line 38
    invoke-direct {p0, p3}, Lcom/sec/vip/amschaton/az;->a(Landroid/view/View;)V

    .line 39
    return-void

    .line 30
    nop

    :array_0
    .array-data 4
        0x7f07021f
        0x7f070220
        0x7f070221
        0x7f070222
        0x7f070223
        0x7f070224
        0x7f070225
        0x7f070226
        0x7f070227
        0x7f070228
        0x7f070229
        0x7f07022a
        0x7f07022b
        0x7f07022c
        0x7f07022d
    .end array-data

    .line 32
    :array_1
    .array-data 4
        0x7f080028
        0x7f080029
        0x7f08002a
        0x7f08002b
        0x7f08002c
        0x7f08002d
        0x7f08002e
        0x7f08002f
        0x7f080030
        0x7f080031
        0x7f080032
        0x7f080033
        0x7f080034
        0x7f080035
        0x7f080036
    .end array-data
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/az;I)I
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/az;->d(I)I

    move-result v0

    return v0
.end method

.method private a(IZ)V
    .locals 1

    .prologue
    .line 98
    if-ltz p1, :cond_0

    const/4 v0, 0x5

    if-lt p1, v0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->h:[Landroid/widget/ImageView;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v8, 0xf

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x5

    .line 43
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/az;->f()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f070274

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/vip/amschaton/ba;

    invoke-direct {v2, p0}, Lcom/sec/vip/amschaton/ba;-><init>(Lcom/sec/vip/amschaton/az;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    new-array v0, v6, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/az;->h:[Landroid/widget/ImageView;

    .line 57
    new-array v3, v6, [I

    fill-array-data v3, :array_0

    move v2, v1

    .line 58
    :goto_0
    if-ge v2, v6, :cond_0

    .line 59
    iget-object v4, p0, Lcom/sec/vip/amschaton/az;->h:[Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/az;->f()Landroid/view/View;

    move-result-object v0

    aget v5, v3, v2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v4, v2

    .line 60
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->h:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    iget-object v4, p0, Lcom/sec/vip/amschaton/az;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 62
    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0, v7}, Lcom/sec/vip/amschaton/az;->a(IZ)V

    .line 65
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/az;->f()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f070281

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/vip/amschaton/az;->n:Landroid/view/View;

    .line 66
    new-array v0, v8, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/az;->j:[Landroid/widget/ImageView;

    .line 67
    :goto_1
    if-ge v1, v8, :cond_1

    .line 68
    iget-object v2, p0, Lcom/sec/vip/amschaton/az;->j:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->n:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/vip/amschaton/az;->l:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v2, v1

    .line 69
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->j:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/az;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->n:Landroid/view/View;

    const v1, 0x7f070289

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/vip/amschaton/az;->k:Landroid/widget/ImageView;

    .line 72
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->k:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/vip/amschaton/az;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget v0, p0, Lcom/sec/vip/amschaton/az;->i:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/az;->d(I)I

    move-result v0

    invoke-direct {p0, v0, v7}, Lcom/sec/vip/amschaton/az;->b(IZ)V

    .line 75
    invoke-direct {p0, p1}, Lcom/sec/vip/amschaton/az;->b(Landroid/view/View;)V

    .line 76
    return-void

    .line 57
    :array_0
    .array-data 4
        0x7f07027c
        0x7f07027d
        0x7f07027e
        0x7f07027f
        0x7f070280
    .end array-data
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/az;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/vip/amschaton/az;->d()V

    return-void
.end method

.method static synthetic a(Lcom/sec/vip/amschaton/az;IZ)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/sec/vip/amschaton/az;->b(IZ)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 147
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/sec/vip/amschaton/az;->j:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 151
    return-void
.end method

.method private b(IZ)V
    .locals 4

    .prologue
    const/16 v3, 0xf

    const/4 v1, 0x0

    .line 133
    if-lt p1, v3, :cond_0

    .line 144
    :goto_0
    return-void

    .line 136
    :cond_0
    if-gez p1, :cond_2

    move v0, v1

    .line 137
    :goto_1
    if-ge v0, v3, :cond_1

    .line 138
    iget-object v2, p0, Lcom/sec/vip/amschaton/az;->j:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->j:[Landroid/widget/ImageView;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/sec/vip/amschaton/bc;

    invoke-direct {v0, p0, p1}, Lcom/sec/vip/amschaton/bc;-><init>(Lcom/sec/vip/amschaton/az;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 88
    return-void
.end method

.method static synthetic b(Lcom/sec/vip/amschaton/az;)[Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->h:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/vip/amschaton/az;)[Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->j:[Landroid/widget/ImageView;

    return-object v0
.end method

.method private d(I)I
    .locals 3

    .prologue
    .line 168
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xf

    if-ge v0, v1, :cond_1

    .line 169
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/az;->e()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vip/amschaton/az;->m:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 173
    :goto_1
    return v0

    .line 168
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic d(Lcom/sec/vip/amschaton/az;)I
    .locals 1

    .prologue
    .line 11
    iget v0, p0, Lcom/sec/vip/amschaton/az;->i:I

    return v0
.end method

.method private d()V
    .locals 0

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/az;->dismiss()V

    .line 92
    return-void
.end method

.method static synthetic e(Lcom/sec/vip/amschaton/az;)[I
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->m:[I

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/sec/vip/amschaton/az;->g:I

    return v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/vip/amschaton/az;->g:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/az;->a(IZ)V

    .line 106
    iput p1, p0, Lcom/sec/vip/amschaton/az;->g:I

    .line 107
    iget v0, p0, Lcom/sec/vip/amschaton/az;->g:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/az;->a(IZ)V

    .line 108
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/sec/vip/amschaton/az;->i:I

    return v0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 158
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/az;->a(Z)V

    .line 159
    iput p1, p0, Lcom/sec/vip/amschaton/az;->i:I

    .line 160
    iget v0, p0, Lcom/sec/vip/amschaton/az;->i:I

    invoke-direct {p0, v0}, Lcom/sec/vip/amschaton/az;->d(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/vip/amschaton/az;->b(IZ)V

    .line 161
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->d:Lcom/sec/vip/amschaton/i;

    new-instance v1, Lcom/sec/vip/amschaton/bf;

    invoke-direct {v1, p0}, Lcom/sec/vip/amschaton/bf;-><init>(Lcom/sec/vip/amschaton/az;)V

    invoke-virtual {v0, v1}, Lcom/sec/vip/amschaton/i;->a(Lcom/sec/vip/amschaton/bi;)V

    .line 215
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 192
    new-instance v0, Lcom/sec/vip/amschaton/i;

    invoke-virtual {p0}, Lcom/sec/vip/amschaton/az;->e()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/vip/amschaton/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/vip/amschaton/az;->d:Lcom/sec/vip/amschaton/i;

    .line 193
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->d:Lcom/sec/vip/amschaton/i;

    invoke-virtual {v0, p1}, Lcom/sec/vip/amschaton/i;->a(I)V

    .line 194
    invoke-virtual {p0}, Lcom/sec/vip/amschaton/az;->c()V

    .line 195
    iget-object v0, p0, Lcom/sec/vip/amschaton/az;->d:Lcom/sec/vip/amschaton/i;

    invoke-virtual {v0}, Lcom/sec/vip/amschaton/i;->d()V

    .line 196
    return-void
.end method
