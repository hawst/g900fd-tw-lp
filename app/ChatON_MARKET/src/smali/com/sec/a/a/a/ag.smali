.class public final Lcom/sec/a/a/a/ag;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/ai;


# static fields
.field private static final a:Lcom/sec/a/a/a/ag;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6655
    new-instance v0, Lcom/sec/a/a/a/ag;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/ag;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/ag;->a:Lcom/sec/a/a/a/ag;

    .line 6656
    sget-object v0, Lcom/sec/a/a/a/ag;->a:Lcom/sec/a/a/a/ag;

    invoke-direct {v0}, Lcom/sec/a/a/a/ag;->t()V

    .line 6657
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/ah;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5969
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 6159
    iput-byte v0, p0, Lcom/sec/a/a/a/ag;->h:B

    .line 6189
    iput v0, p0, Lcom/sec/a/a/a/ag;->i:I

    .line 5970
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/ah;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 5966
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/ag;-><init>(Lcom/sec/a/a/a/ah;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5972
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 6159
    iput-byte v0, p0, Lcom/sec/a/a/a/ag;->h:B

    .line 6189
    iput v0, p0, Lcom/sec/a/a/a/ag;->i:I

    .line 5973
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/ag;I)I
    .locals 0

    .prologue
    .line 5966
    iput p1, p0, Lcom/sec/a/a/a/ag;->b:I

    return p1
.end method

.method public static a()Lcom/sec/a/a/a/ag;
    .locals 1

    .prologue
    .line 5978
    sget-object v0, Lcom/sec/a/a/a/ag;->a:Lcom/sec/a/a/a/ag;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/ag;
    .locals 1

    .prologue
    .line 6233
    invoke-static {}, Lcom/sec/a/a/a/ag;->newBuilder()Lcom/sec/a/a/a/ah;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/ah;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/ah;

    invoke-static {v0}, Lcom/sec/a/a/a/ah;->a(Lcom/sec/a/a/a/ah;)Lcom/sec/a/a/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/ag;)Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6287
    invoke-static {}, Lcom/sec/a/a/a/ag;->newBuilder()Lcom/sec/a/a/a/ah;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/ah;->a(Lcom/sec/a/a/a/ag;)Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a/ag;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 5966
    iput-object p1, p0, Lcom/sec/a/a/a/ag;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/ag;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 5966
    iput-object p1, p0, Lcom/sec/a/a/a/ag;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/ag;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 5966
    iput-object p1, p0, Lcom/sec/a/a/a/ag;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic d(Lcom/sec/a/a/a/ag;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 5966
    iput-object p1, p0, Lcom/sec/a/a/a/ag;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic e(Lcom/sec/a/a/a/ag;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 5966
    iput-object p1, p0, Lcom/sec/a/a/a/ag;->g:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6279
    invoke-static {}, Lcom/sec/a/a/a/ah;->f()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6009
    iget-object v0, p0, Lcom/sec/a/a/a/ag;->c:Ljava/lang/Object;

    .line 6010
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6011
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6012
    iput-object v0, p0, Lcom/sec/a/a/a/ag;->c:Ljava/lang/Object;

    .line 6015
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private p()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6042
    iget-object v0, p0, Lcom/sec/a/a/a/ag;->d:Ljava/lang/Object;

    .line 6043
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6044
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6045
    iput-object v0, p0, Lcom/sec/a/a/a/ag;->d:Ljava/lang/Object;

    .line 6048
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private q()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6075
    iget-object v0, p0, Lcom/sec/a/a/a/ag;->e:Ljava/lang/Object;

    .line 6076
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6077
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6078
    iput-object v0, p0, Lcom/sec/a/a/a/ag;->e:Ljava/lang/Object;

    .line 6081
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private r()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6108
    iget-object v0, p0, Lcom/sec/a/a/a/ag;->f:Ljava/lang/Object;

    .line 6109
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6110
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6111
    iput-object v0, p0, Lcom/sec/a/a/a/ag;->f:Ljava/lang/Object;

    .line 6114
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private s()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6141
    iget-object v0, p0, Lcom/sec/a/a/a/ag;->g:Ljava/lang/Object;

    .line 6142
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6143
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6144
    iput-object v0, p0, Lcom/sec/a/a/a/ag;->g:Ljava/lang/Object;

    .line 6147
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private t()V
    .locals 1

    .prologue
    .line 6152
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ag;->c:Ljava/lang/Object;

    .line 6153
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ag;->d:Ljava/lang/Object;

    .line 6154
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ag;->e:Ljava/lang/Object;

    .line 6155
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ag;->f:Ljava/lang/Object;

    .line 6156
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ag;->g:Ljava/lang/Object;

    .line 6157
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/ag;
    .locals 1

    .prologue
    .line 5982
    sget-object v0, Lcom/sec/a/a/a/ag;->a:Lcom/sec/a/a/a/ag;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5991
    iget v1, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5995
    iget-object v0, p0, Lcom/sec/a/a/a/ag;->c:Ljava/lang/Object;

    .line 5996
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5997
    check-cast v0, Ljava/lang/String;

    .line 6004
    :goto_0
    return-object v0

    .line 5999
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6000
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6001
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6002
    iput-object v1, p0, Lcom/sec/a/a/a/ag;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6004
    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 6024
    iget v0, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6028
    iget-object v0, p0, Lcom/sec/a/a/a/ag;->d:Ljava/lang/Object;

    .line 6029
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6030
    check-cast v0, Ljava/lang/String;

    .line 6037
    :goto_0
    return-object v0

    .line 6032
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6033
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6034
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6035
    iput-object v1, p0, Lcom/sec/a/a/a/ag;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6037
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 6057
    iget v0, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5966
    invoke-virtual {p0}, Lcom/sec/a/a/a/ag;->b()Lcom/sec/a/a/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    .line 6192
    iget v0, p0, Lcom/sec/a/a/a/ag;->i:I

    .line 6193
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 6213
    :goto_0
    return v0

    .line 6196
    :cond_0
    const/4 v0, 0x0

    .line 6197
    iget v1, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 6198
    invoke-direct {p0}, Lcom/sec/a/a/a/ag;->o()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6200
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 6201
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/ag;->p()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6203
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 6204
    invoke-direct {p0}, Lcom/sec/a/a/a/ag;->q()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6206
    :cond_3
    iget v1, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 6207
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sec/a/a/a/ag;->r()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6209
    :cond_4
    iget v1, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 6210
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sec/a/a/a/ag;->s()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6212
    :cond_5
    iput v0, p0, Lcom/sec/a/a/a/ag;->i:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6061
    iget-object v0, p0, Lcom/sec/a/a/a/ag;->e:Ljava/lang/Object;

    .line 6062
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6063
    check-cast v0, Ljava/lang/String;

    .line 6070
    :goto_0
    return-object v0

    .line 6065
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6066
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6067
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6068
    iput-object v1, p0, Lcom/sec/a/a/a/ag;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6070
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 6090
    iget v0, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 6162
    iget-byte v1, p0, Lcom/sec/a/a/a/ag;->h:B

    .line 6163
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 6164
    if-ne v1, v0, :cond_0

    .line 6167
    :goto_0
    return v0

    .line 6164
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6166
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/ag;->h:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6094
    iget-object v0, p0, Lcom/sec/a/a/a/ag;->f:Ljava/lang/Object;

    .line 6095
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6096
    check-cast v0, Ljava/lang/String;

    .line 6103
    :goto_0
    return-object v0

    .line 6098
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6099
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6100
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6101
    iput-object v1, p0, Lcom/sec/a/a/a/ag;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6103
    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 6123
    iget v0, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6127
    iget-object v0, p0, Lcom/sec/a/a/a/ag;->g:Ljava/lang/Object;

    .line 6128
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6129
    check-cast v0, Ljava/lang/String;

    .line 6136
    :goto_0
    return-object v0

    .line 6131
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6132
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6133
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6134
    iput-object v1, p0, Lcom/sec/a/a/a/ag;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6136
    goto :goto_0
.end method

.method public m()Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6283
    invoke-static {}, Lcom/sec/a/a/a/ag;->newBuilder()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6291
    invoke-static {p0}, Lcom/sec/a/a/a/ag;->a(Lcom/sec/a/a/a/ag;)Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5966
    invoke-virtual {p0}, Lcom/sec/a/a/a/ag;->m()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5966
    invoke-virtual {p0}, Lcom/sec/a/a/a/ag;->n()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6220
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 6171
    invoke-virtual {p0}, Lcom/sec/a/a/a/ag;->getSerializedSize()I

    .line 6172
    iget v0, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 6173
    invoke-direct {p0}, Lcom/sec/a/a/a/ag;->o()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6175
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 6176
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/ag;->p()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6178
    :cond_1
    iget v0, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 6179
    invoke-direct {p0}, Lcom/sec/a/a/a/ag;->q()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6181
    :cond_2
    iget v0, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 6182
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sec/a/a/a/ag;->r()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6184
    :cond_3
    iget v0, p0, Lcom/sec/a/a/a/ag;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 6185
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/a/a/a/ag;->s()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6187
    :cond_4
    return-void
.end method
