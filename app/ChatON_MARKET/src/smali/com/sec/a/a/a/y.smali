.class public final Lcom/sec/a/a/a/y;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/x;",
        "Lcom/sec/a/a/a/y;",
        ">;",
        "Lcom/sec/a/a/a/z;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:J

.field private d:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3840
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 3841
    invoke-direct {p0}, Lcom/sec/a/a/a/y;->g()V

    .line 3842
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/y;)Lcom/sec/a/a/a/x;
    .locals 1

    .prologue
    .line 3836
    invoke-direct {p0}, Lcom/sec/a/a/a/y;->i()Lcom/sec/a/a/a/x;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/y;
    .locals 1

    .prologue
    .line 3836
    invoke-static {}, Lcom/sec/a/a/a/y;->h()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 3845
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/y;
    .locals 1

    .prologue
    .line 3848
    new-instance v0, Lcom/sec/a/a/a/y;

    invoke-direct {v0}, Lcom/sec/a/a/a/y;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/x;
    .locals 2

    .prologue
    .line 3879
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->e()Lcom/sec/a/a/a/x;

    move-result-object v0

    .line 3880
    invoke-virtual {v0}, Lcom/sec/a/a/a/x;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3881
    invoke-static {v0}, Lcom/sec/a/a/a/y;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 3883
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/y;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3852
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 3853
    iput v2, p0, Lcom/sec/a/a/a/y;->b:I

    .line 3854
    iget v0, p0, Lcom/sec/a/a/a/y;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/y;->a:I

    .line 3855
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/a/a/a/y;->c:J

    .line 3856
    iget v0, p0, Lcom/sec/a/a/a/y;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/y;->a:I

    .line 3857
    iput v2, p0, Lcom/sec/a/a/a/y;->d:I

    .line 3858
    iget v0, p0, Lcom/sec/a/a/a/y;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/a/a/a/y;->a:I

    .line 3859
    return-object p0
.end method

.method public a(I)Lcom/sec/a/a/a/y;
    .locals 1

    .prologue
    .line 3972
    iget v0, p0, Lcom/sec/a/a/a/y;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/y;->a:I

    .line 3973
    iput p1, p0, Lcom/sec/a/a/a/y;->b:I

    .line 3975
    return-object p0
.end method

.method public a(J)Lcom/sec/a/a/a/y;
    .locals 1

    .prologue
    .line 3997
    iget v0, p0, Lcom/sec/a/a/a/y;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/y;->a:I

    .line 3998
    iput-wide p1, p0, Lcom/sec/a/a/a/y;->c:J

    .line 4000
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/y;
    .locals 2

    .prologue
    .line 3927
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 3928
    sparse-switch v0, :sswitch_data_0

    .line 3933
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/y;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3935
    :sswitch_0
    return-object p0

    .line 3940
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/y;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/y;->a:I

    .line 3941
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/y;->b:I

    goto :goto_0

    .line 3945
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/y;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/y;->a:I

    .line 3946
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/a/a/a/y;->c:J

    goto :goto_0

    .line 3950
    :sswitch_3
    iget v0, p0, Lcom/sec/a/a/a/y;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/y;->a:I

    .line 3951
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/y;->d:I

    goto :goto_0

    .line 3928
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/x;)Lcom/sec/a/a/a/y;
    .locals 2

    .prologue
    .line 3907
    invoke-static {}, Lcom/sec/a/a/a/x;->a()Lcom/sec/a/a/a/x;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 3918
    :cond_0
    :goto_0
    return-object p0

    .line 3909
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/x;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3910
    invoke-virtual {p1}, Lcom/sec/a/a/a/x;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/y;->a(I)Lcom/sec/a/a/a/y;

    .line 3912
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/x;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3913
    invoke-virtual {p1}, Lcom/sec/a/a/a/x;->f()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/a/a/a/y;->a(J)Lcom/sec/a/a/a/y;

    .line 3915
    :cond_3
    invoke-virtual {p1}, Lcom/sec/a/a/a/x;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3916
    invoke-virtual {p1}, Lcom/sec/a/a/a/x;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/y;->b(I)Lcom/sec/a/a/a/y;

    goto :goto_0
.end method

.method public b()Lcom/sec/a/a/a/y;
    .locals 2

    .prologue
    .line 3863
    invoke-static {}, Lcom/sec/a/a/a/y;->h()Lcom/sec/a/a/a/y;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->e()Lcom/sec/a/a/a/x;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/y;->a(Lcom/sec/a/a/a/x;)Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/sec/a/a/a/y;
    .locals 1

    .prologue
    .line 4022
    iget v0, p0, Lcom/sec/a/a/a/y;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/y;->a:I

    .line 4023
    iput p1, p0, Lcom/sec/a/a/a/y;->d:I

    .line 4025
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->d()Lcom/sec/a/a/a/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->e()Lcom/sec/a/a/a/x;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/x;
    .locals 1

    .prologue
    .line 3867
    invoke-static {}, Lcom/sec/a/a/a/x;->a()Lcom/sec/a/a/a/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->a()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->a()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->b()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->b()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->b()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->b()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/x;
    .locals 2

    .prologue
    .line 3871
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->e()Lcom/sec/a/a/a/x;

    move-result-object v0

    .line 3872
    invoke-virtual {v0}, Lcom/sec/a/a/a/x;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3873
    invoke-static {v0}, Lcom/sec/a/a/a/y;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 3875
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/a/a/a/x;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 3887
    new-instance v2, Lcom/sec/a/a/a/x;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/x;-><init>(Lcom/sec/a/a/a/y;Lcom/sec/a/a/a/b;)V

    .line 3888
    iget v3, p0, Lcom/sec/a/a/a/y;->a:I

    .line 3889
    const/4 v1, 0x0

    .line 3890
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 3893
    :goto_0
    iget v1, p0, Lcom/sec/a/a/a/y;->b:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/x;->a(Lcom/sec/a/a/a/x;I)I

    .line 3894
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 3895
    or-int/lit8 v0, v0, 0x2

    .line 3897
    :cond_0
    iget-wide v4, p0, Lcom/sec/a/a/a/y;->c:J

    invoke-static {v2, v4, v5}, Lcom/sec/a/a/a/x;->a(Lcom/sec/a/a/a/x;J)J

    .line 3898
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 3899
    or-int/lit8 v0, v0, 0x4

    .line 3901
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/y;->d:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/x;->b(Lcom/sec/a/a/a/x;I)I

    .line 3902
    invoke-static {v2, v0}, Lcom/sec/a/a/a/x;->c(Lcom/sec/a/a/a/x;I)I

    .line 3903
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->c()Lcom/sec/a/a/a/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0}, Lcom/sec/a/a/a/y;->c()Lcom/sec/a/a/a/x;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 3922
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/y;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3836
    check-cast p1, Lcom/sec/a/a/a/x;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/y;->a(Lcom/sec/a/a/a/x;)Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3836
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/y;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method
