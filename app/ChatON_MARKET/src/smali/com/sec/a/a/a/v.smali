.class public final Lcom/sec/a/a/a/v;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/w;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/u;",
        "Lcom/sec/a/a/a/v;",
        ">;",
        "Lcom/sec/a/a/a/w;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/a/a/a/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 5292
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 5392
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    .line 5293
    invoke-direct {p0}, Lcom/sec/a/a/a/v;->g()V

    .line 5294
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/v;)Lcom/sec/a/a/a/u;
    .locals 1

    .prologue
    .line 5288
    invoke-direct {p0}, Lcom/sec/a/a/a/v;->i()Lcom/sec/a/a/a/u;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/v;
    .locals 1

    .prologue
    .line 5288
    invoke-static {}, Lcom/sec/a/a/a/v;->h()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 5297
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/v;
    .locals 1

    .prologue
    .line 5300
    new-instance v0, Lcom/sec/a/a/a/v;

    invoke-direct {v0}, Lcom/sec/a/a/a/v;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/u;
    .locals 2

    .prologue
    .line 5327
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->e()Lcom/sec/a/a/a/u;

    move-result-object v0

    .line 5328
    invoke-virtual {v0}, Lcom/sec/a/a/a/u;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5329
    invoke-static {v0}, Lcom/sec/a/a/a/v;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 5331
    :cond_0
    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 5395
    iget v0, p0, Lcom/sec/a/a/a/v;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 5396
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    .line 5397
    iget v0, p0, Lcom/sec/a/a/a/v;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/v;->a:I

    .line 5399
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/v;
    .locals 1

    .prologue
    .line 5304
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 5305
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    .line 5306
    iget v0, p0, Lcom/sec/a/a/a/v;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/v;->a:I

    .line 5307
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/v;
    .locals 1

    .prologue
    .line 5367
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 5368
    sparse-switch v0, :sswitch_data_0

    .line 5373
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/v;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5375
    :sswitch_0
    return-object p0

    .line 5380
    :sswitch_1
    invoke-static {}, Lcom/sec/a/a/a/r;->newBuilder()Lcom/sec/a/a/a/s;

    move-result-object v0

    .line 5381
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 5382
    invoke-virtual {v0}, Lcom/sec/a/a/a/s;->e()Lcom/sec/a/a/a/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/v;->a(Lcom/sec/a/a/a/r;)Lcom/sec/a/a/a/v;

    goto :goto_0

    .line 5368
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/r;)Lcom/sec/a/a/a/v;
    .locals 1

    .prologue
    .line 5431
    if-nez p1, :cond_0

    .line 5432
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5434
    :cond_0
    invoke-direct {p0}, Lcom/sec/a/a/a/v;->j()V

    .line 5435
    iget-object v0, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5437
    return-object p0
.end method

.method public a(Lcom/sec/a/a/a/u;)Lcom/sec/a/a/a/v;
    .locals 2

    .prologue
    .line 5346
    invoke-static {}, Lcom/sec/a/a/a/u;->a()Lcom/sec/a/a/a/u;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 5358
    :cond_0
    :goto_0
    return-object p0

    .line 5348
    :cond_1
    invoke-static {p1}, Lcom/sec/a/a/a/u;->b(Lcom/sec/a/a/a/u;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5349
    iget-object v0, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5350
    invoke-static {p1}, Lcom/sec/a/a/a/u;->b(Lcom/sec/a/a/a/u;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    .line 5351
    iget v0, p0, Lcom/sec/a/a/a/v;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/v;->a:I

    goto :goto_0

    .line 5353
    :cond_2
    invoke-direct {p0}, Lcom/sec/a/a/a/v;->j()V

    .line 5354
    iget-object v0, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    invoke-static {p1}, Lcom/sec/a/a/a/u;->b(Lcom/sec/a/a/a/u;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public b()Lcom/sec/a/a/a/v;
    .locals 2

    .prologue
    .line 5311
    invoke-static {}, Lcom/sec/a/a/a/v;->h()Lcom/sec/a/a/a/v;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->e()Lcom/sec/a/a/a/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/v;->a(Lcom/sec/a/a/a/u;)Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->d()Lcom/sec/a/a/a/u;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->e()Lcom/sec/a/a/a/u;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/u;
    .locals 1

    .prologue
    .line 5315
    invoke-static {}, Lcom/sec/a/a/a/u;->a()Lcom/sec/a/a/a/u;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->a()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->a()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->b()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->b()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->b()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->b()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/u;
    .locals 2

    .prologue
    .line 5319
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->e()Lcom/sec/a/a/a/u;

    move-result-object v0

    .line 5320
    invoke-virtual {v0}, Lcom/sec/a/a/a/u;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5321
    invoke-static {v0}, Lcom/sec/a/a/a/v;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 5323
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/a/a/a/u;
    .locals 3

    .prologue
    .line 5335
    new-instance v0, Lcom/sec/a/a/a/u;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/a/a/a/u;-><init>(Lcom/sec/a/a/a/v;Lcom/sec/a/a/a/b;)V

    .line 5336
    iget v1, p0, Lcom/sec/a/a/a/v;->a:I

    .line 5337
    iget v1, p0, Lcom/sec/a/a/a/v;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 5338
    iget-object v1, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    .line 5339
    iget v1, p0, Lcom/sec/a/a/a/v;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/sec/a/a/a/v;->a:I

    .line 5341
    :cond_0
    iget-object v1, p0, Lcom/sec/a/a/a/v;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/sec/a/a/a/u;->a(Lcom/sec/a/a/a/u;Ljava/util/List;)Ljava/util/List;

    .line 5342
    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->c()Lcom/sec/a/a/a/u;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0}, Lcom/sec/a/a/a/v;->c()Lcom/sec/a/a/a/u;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 5362
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/v;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5288
    check-cast p1, Lcom/sec/a/a/a/u;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/v;->a(Lcom/sec/a/a/a/u;)Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5288
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/v;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method
