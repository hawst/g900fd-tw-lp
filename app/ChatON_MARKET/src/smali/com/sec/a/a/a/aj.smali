.class public final Lcom/sec/a/a/a/aj;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/al;


# static fields
.field private static final a:Lcom/sec/a/a/a/aj;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078
    new-instance v0, Lcom/sec/a/a/a/aj;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/aj;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/aj;->a:Lcom/sec/a/a/a/aj;

    .line 2079
    sget-object v0, Lcom/sec/a/a/a/aj;->a:Lcom/sec/a/a/a/aj;

    invoke-direct {v0}, Lcom/sec/a/a/a/aj;->r()V

    .line 2080
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/ak;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1466
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 1614
    iput-byte v0, p0, Lcom/sec/a/a/a/aj;->h:B

    .line 1644
    iput v0, p0, Lcom/sec/a/a/a/aj;->i:I

    .line 1467
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/ak;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 1463
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/aj;-><init>(Lcom/sec/a/a/a/ak;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1469
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1614
    iput-byte v0, p0, Lcom/sec/a/a/a/aj;->h:B

    .line 1644
    iput v0, p0, Lcom/sec/a/a/a/aj;->i:I

    .line 1470
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/aj;I)I
    .locals 0

    .prologue
    .line 1463
    iput p1, p0, Lcom/sec/a/a/a/aj;->c:I

    return p1
.end method

.method public static a()Lcom/sec/a/a/a/aj;
    .locals 1

    .prologue
    .line 1475
    sget-object v0, Lcom/sec/a/a/a/aj;->a:Lcom/sec/a/a/a/aj;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/aj;
    .locals 1

    .prologue
    .line 1688
    invoke-static {}, Lcom/sec/a/a/a/aj;->newBuilder()Lcom/sec/a/a/a/ak;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/ak;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/ak;

    invoke-static {v0}, Lcom/sec/a/a/a/ak;->a(Lcom/sec/a/a/a/ak;)Lcom/sec/a/a/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/aj;)Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1742
    invoke-static {}, Lcom/sec/a/a/a/aj;->newBuilder()Lcom/sec/a/a/a/ak;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/ak;->a(Lcom/sec/a/a/a/aj;)Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a/aj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 1463
    iput-object p1, p0, Lcom/sec/a/a/a/aj;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/aj;I)I
    .locals 0

    .prologue
    .line 1463
    iput p1, p0, Lcom/sec/a/a/a/aj;->d:I

    return p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/aj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 1463
    iput-object p1, p0, Lcom/sec/a/a/a/aj;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/aj;I)I
    .locals 0

    .prologue
    .line 1463
    iput p1, p0, Lcom/sec/a/a/a/aj;->b:I

    return p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/aj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 1463
    iput-object p1, p0, Lcom/sec/a/a/a/aj;->g:Ljava/lang/Object;

    return-object p1
.end method

.method public static newBuilder()Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1734
    invoke-static {}, Lcom/sec/a/a/a/ak;->f()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 1530
    iget-object v0, p0, Lcom/sec/a/a/a/aj;->e:Ljava/lang/Object;

    .line 1531
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1532
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1533
    iput-object v0, p0, Lcom/sec/a/a/a/aj;->e:Ljava/lang/Object;

    .line 1536
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private p()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 1563
    iget-object v0, p0, Lcom/sec/a/a/a/aj;->f:Ljava/lang/Object;

    .line 1564
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1565
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1566
    iput-object v0, p0, Lcom/sec/a/a/a/aj;->f:Ljava/lang/Object;

    .line 1569
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private q()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 1596
    iget-object v0, p0, Lcom/sec/a/a/a/aj;->g:Ljava/lang/Object;

    .line 1597
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1598
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1599
    iput-object v0, p0, Lcom/sec/a/a/a/aj;->g:Ljava/lang/Object;

    .line 1602
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private r()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1607
    iput v0, p0, Lcom/sec/a/a/a/aj;->c:I

    .line 1608
    iput v0, p0, Lcom/sec/a/a/a/aj;->d:I

    .line 1609
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/aj;->e:Ljava/lang/Object;

    .line 1610
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/aj;->f:Ljava/lang/Object;

    .line 1611
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/aj;->g:Ljava/lang/Object;

    .line 1612
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/aj;
    .locals 1

    .prologue
    .line 1479
    sget-object v0, Lcom/sec/a/a/a/aj;->a:Lcom/sec/a/a/a/aj;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1488
    iget v1, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 1492
    iget v0, p0, Lcom/sec/a/a/a/aj;->c:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 1500
    iget v0, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 1504
    iget v0, p0, Lcom/sec/a/a/a/aj;->d:I

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 1512
    iget v0, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1463
    invoke-virtual {p0}, Lcom/sec/a/a/a/aj;->b()Lcom/sec/a/a/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1647
    iget v0, p0, Lcom/sec/a/a/a/aj;->i:I

    .line 1648
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1668
    :goto_0
    return v0

    .line 1651
    :cond_0
    const/4 v0, 0x0

    .line 1652
    iget v1, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 1653
    iget v1, p0, Lcom/sec/a/a/a/aj;->c:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1655
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 1656
    iget v1, p0, Lcom/sec/a/a/a/aj;->d:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1658
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 1659
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/aj;->o()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1661
    :cond_3
    iget v1, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 1662
    invoke-direct {p0}, Lcom/sec/a/a/a/aj;->p()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1664
    :cond_4
    iget v1, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 1665
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sec/a/a/a/aj;->q()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1667
    :cond_5
    iput v0, p0, Lcom/sec/a/a/a/aj;->i:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1516
    iget-object v0, p0, Lcom/sec/a/a/a/aj;->e:Ljava/lang/Object;

    .line 1517
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1518
    check-cast v0, Ljava/lang/String;

    .line 1525
    :goto_0
    return-object v0

    .line 1520
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 1521
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1522
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1523
    iput-object v1, p0, Lcom/sec/a/a/a/aj;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1525
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 1545
    iget v0, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1617
    iget-byte v1, p0, Lcom/sec/a/a/a/aj;->h:B

    .line 1618
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 1619
    if-ne v1, v0, :cond_0

    .line 1622
    :goto_0
    return v0

    .line 1619
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1621
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/aj;->h:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1549
    iget-object v0, p0, Lcom/sec/a/a/a/aj;->f:Ljava/lang/Object;

    .line 1550
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1551
    check-cast v0, Ljava/lang/String;

    .line 1558
    :goto_0
    return-object v0

    .line 1553
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 1554
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1555
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1556
    iput-object v1, p0, Lcom/sec/a/a/a/aj;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1558
    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 1578
    iget v0, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1582
    iget-object v0, p0, Lcom/sec/a/a/a/aj;->g:Ljava/lang/Object;

    .line 1583
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1584
    check-cast v0, Ljava/lang/String;

    .line 1591
    :goto_0
    return-object v0

    .line 1586
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 1587
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1588
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1589
    iput-object v1, p0, Lcom/sec/a/a/a/aj;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1591
    goto :goto_0
.end method

.method public m()Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1738
    invoke-static {}, Lcom/sec/a/a/a/aj;->newBuilder()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1746
    invoke-static {p0}, Lcom/sec/a/a/a/aj;->a(Lcom/sec/a/a/a/aj;)Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1463
    invoke-virtual {p0}, Lcom/sec/a/a/a/aj;->m()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1463
    invoke-virtual {p0}, Lcom/sec/a/a/a/aj;->n()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1675
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1626
    invoke-virtual {p0}, Lcom/sec/a/a/a/aj;->getSerializedSize()I

    .line 1627
    iget v0, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1628
    iget v0, p0, Lcom/sec/a/a/a/aj;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1630
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1631
    iget v0, p0, Lcom/sec/a/a/a/aj;->d:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1633
    :cond_1
    iget v0, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 1634
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/aj;->o()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1636
    :cond_2
    iget v0, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1637
    invoke-direct {p0}, Lcom/sec/a/a/a/aj;->p()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1639
    :cond_3
    iget v0, p0, Lcom/sec/a/a/a/aj;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 1640
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sec/a/a/a/aj;->q()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1642
    :cond_4
    return-void
.end method
