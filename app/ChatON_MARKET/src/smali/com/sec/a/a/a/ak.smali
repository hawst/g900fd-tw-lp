.class public final Lcom/sec/a/a/a/ak;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/al;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/aj;",
        "Lcom/sec/a/a/a/ak;",
        ">;",
        "Lcom/sec/a/a/a/al;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1753
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 1952
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ak;->d:Ljava/lang/Object;

    .line 1993
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ak;->e:Ljava/lang/Object;

    .line 2034
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ak;->f:Ljava/lang/Object;

    .line 1754
    invoke-direct {p0}, Lcom/sec/a/a/a/ak;->g()V

    .line 1755
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/ak;)Lcom/sec/a/a/a/aj;
    .locals 1

    .prologue
    .line 1749
    invoke-direct {p0}, Lcom/sec/a/a/a/ak;->i()Lcom/sec/a/a/a/aj;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1749
    invoke-static {}, Lcom/sec/a/a/a/ak;->h()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 1758
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1761
    new-instance v0, Lcom/sec/a/a/a/ak;

    invoke-direct {v0}, Lcom/sec/a/a/a/ak;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/aj;
    .locals 2

    .prologue
    .line 1796
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->e()Lcom/sec/a/a/a/aj;

    move-result-object v0

    .line 1797
    invoke-virtual {v0}, Lcom/sec/a/a/a/aj;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1798
    invoke-static {v0}, Lcom/sec/a/a/a/ak;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 1800
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/ak;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1765
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 1766
    iput v1, p0, Lcom/sec/a/a/a/ak;->b:I

    .line 1767
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1768
    iput v1, p0, Lcom/sec/a/a/a/ak;->c:I

    .line 1769
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1770
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ak;->d:Ljava/lang/Object;

    .line 1771
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1772
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ak;->e:Ljava/lang/Object;

    .line 1773
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1774
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ak;->f:Ljava/lang/Object;

    .line 1775
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1776
    return-object p0
.end method

.method public a(I)Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1913
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1914
    iput p1, p0, Lcom/sec/a/a/a/ak;->b:I

    .line 1916
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1858
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 1859
    sparse-switch v0, :sswitch_data_0

    .line 1864
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/ak;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1866
    :sswitch_0
    return-object p0

    .line 1871
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1872
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/ak;->b:I

    goto :goto_0

    .line 1876
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1877
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/ak;->c:I

    goto :goto_0

    .line 1881
    :sswitch_3
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1882
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ak;->d:Ljava/lang/Object;

    goto :goto_0

    .line 1886
    :sswitch_4
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1887
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ak;->e:Ljava/lang/Object;

    goto :goto_0

    .line 1891
    :sswitch_5
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1892
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ak;->f:Ljava/lang/Object;

    goto :goto_0

    .line 1859
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/aj;)Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1832
    invoke-static {}, Lcom/sec/a/a/a/aj;->a()Lcom/sec/a/a/a/aj;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1849
    :cond_0
    :goto_0
    return-object p0

    .line 1834
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/aj;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1835
    invoke-virtual {p1}, Lcom/sec/a/a/a/aj;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ak;->a(I)Lcom/sec/a/a/a/ak;

    .line 1837
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/aj;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1838
    invoke-virtual {p1}, Lcom/sec/a/a/a/aj;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ak;->b(I)Lcom/sec/a/a/a/ak;

    .line 1840
    :cond_3
    invoke-virtual {p1}, Lcom/sec/a/a/a/aj;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1841
    invoke-virtual {p1}, Lcom/sec/a/a/a/aj;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ak;->a(Ljava/lang/String;)Lcom/sec/a/a/a/ak;

    .line 1843
    :cond_4
    invoke-virtual {p1}, Lcom/sec/a/a/a/aj;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1844
    invoke-virtual {p1}, Lcom/sec/a/a/a/aj;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ak;->b(Ljava/lang/String;)Lcom/sec/a/a/a/ak;

    .line 1846
    :cond_5
    invoke-virtual {p1}, Lcom/sec/a/a/a/aj;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847
    invoke-virtual {p1}, Lcom/sec/a/a/a/aj;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ak;->c(Ljava/lang/String;)Lcom/sec/a/a/a/ak;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1970
    if-nez p1, :cond_0

    .line 1971
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1973
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1974
    iput-object p1, p0, Lcom/sec/a/a/a/ak;->d:Ljava/lang/Object;

    .line 1976
    return-object p0
.end method

.method public b()Lcom/sec/a/a/a/ak;
    .locals 2

    .prologue
    .line 1780
    invoke-static {}, Lcom/sec/a/a/a/ak;->h()Lcom/sec/a/a/a/ak;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->e()Lcom/sec/a/a/a/aj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/ak;->a(Lcom/sec/a/a/a/aj;)Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 1938
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1939
    iput p1, p0, Lcom/sec/a/a/a/ak;->c:I

    .line 1941
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 2011
    if-nez p1, :cond_0

    .line 2012
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2014
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 2015
    iput-object p1, p0, Lcom/sec/a/a/a/ak;->e:Ljava/lang/Object;

    .line 2017
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->d()Lcom/sec/a/a/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->e()Lcom/sec/a/a/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/aj;
    .locals 1

    .prologue
    .line 1784
    invoke-static {}, Lcom/sec/a/a/a/aj;->a()Lcom/sec/a/a/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/a/a/a/ak;
    .locals 1

    .prologue
    .line 2052
    if-nez p1, :cond_0

    .line 2053
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2055
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ak;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 2056
    iput-object p1, p0, Lcom/sec/a/a/a/ak;->f:Ljava/lang/Object;

    .line 2058
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->a()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->a()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->b()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->b()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->b()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->b()Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/aj;
    .locals 2

    .prologue
    .line 1788
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->e()Lcom/sec/a/a/a/aj;

    move-result-object v0

    .line 1789
    invoke-virtual {v0}, Lcom/sec/a/a/a/aj;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1790
    invoke-static {v0}, Lcom/sec/a/a/a/ak;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1792
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/a/a/a/aj;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1804
    new-instance v2, Lcom/sec/a/a/a/aj;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/aj;-><init>(Lcom/sec/a/a/a/ak;Lcom/sec/a/a/a/b;)V

    .line 1805
    iget v3, p0, Lcom/sec/a/a/a/ak;->a:I

    .line 1806
    const/4 v1, 0x0

    .line 1807
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 1810
    :goto_0
    iget v1, p0, Lcom/sec/a/a/a/ak;->b:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/aj;->a(Lcom/sec/a/a/a/aj;I)I

    .line 1811
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1812
    or-int/lit8 v0, v0, 0x2

    .line 1814
    :cond_0
    iget v1, p0, Lcom/sec/a/a/a/ak;->c:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/aj;->b(Lcom/sec/a/a/a/aj;I)I

    .line 1815
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 1816
    or-int/lit8 v0, v0, 0x4

    .line 1818
    :cond_1
    iget-object v1, p0, Lcom/sec/a/a/a/ak;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/aj;->a(Lcom/sec/a/a/a/aj;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1819
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 1820
    or-int/lit8 v0, v0, 0x8

    .line 1822
    :cond_2
    iget-object v1, p0, Lcom/sec/a/a/a/ak;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/aj;->b(Lcom/sec/a/a/a/aj;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1823
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 1824
    or-int/lit8 v0, v0, 0x10

    .line 1826
    :cond_3
    iget-object v1, p0, Lcom/sec/a/a/a/ak;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/aj;->c(Lcom/sec/a/a/a/aj;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1827
    invoke-static {v2, v0}, Lcom/sec/a/a/a/aj;->c(Lcom/sec/a/a/a/aj;I)I

    .line 1828
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->c()Lcom/sec/a/a/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0}, Lcom/sec/a/a/a/ak;->c()Lcom/sec/a/a/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 1853
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/ak;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1749
    check-cast p1, Lcom/sec/a/a/a/aj;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/ak;->a(Lcom/sec/a/a/a/aj;)Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1749
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/ak;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ak;

    move-result-object v0

    return-object v0
.end method
