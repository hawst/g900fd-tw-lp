.class public final Lcom/sec/a/a/a/ad;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/af;


# static fields
.field private static final a:Lcom/sec/a/a/a/ad;


# instance fields
.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:I

.field private g:Ljava/lang/Object;

.field private h:I

.field private i:I

.field private j:Ljava/lang/Object;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7530
    new-instance v0, Lcom/sec/a/a/a/ad;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/ad;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/ad;->a:Lcom/sec/a/a/a/ad;

    .line 7531
    sget-object v0, Lcom/sec/a/a/a/ad;->a:Lcom/sec/a/a/a/ad;

    invoke-direct {v0}, Lcom/sec/a/a/a/ad;->y()V

    .line 7532
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/ae;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6708
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 6916
    iput-byte v0, p0, Lcom/sec/a/a/a/ad;->k:B

    .line 6955
    iput v0, p0, Lcom/sec/a/a/a/ad;->l:I

    .line 6709
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/ae;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 6705
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/ad;-><init>(Lcom/sec/a/a/a/ae;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6711
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 6916
    iput-byte v0, p0, Lcom/sec/a/a/a/ad;->k:B

    .line 6955
    iput v0, p0, Lcom/sec/a/a/a/ad;->l:I

    .line 6712
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/ad;I)I
    .locals 0

    .prologue
    .line 6705
    iput p1, p0, Lcom/sec/a/a/a/ad;->c:I

    return p1
.end method

.method public static a()Lcom/sec/a/a/a/ad;
    .locals 1

    .prologue
    .line 6717
    sget-object v0, Lcom/sec/a/a/a/ad;->a:Lcom/sec/a/a/a/ad;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/ad;
    .locals 1

    .prologue
    .line 7008
    invoke-static {}, Lcom/sec/a/a/a/ad;->newBuilder()Lcom/sec/a/a/a/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/ae;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/ae;

    invoke-static {v0}, Lcom/sec/a/a/a/ae;->a(Lcom/sec/a/a/a/ae;)Lcom/sec/a/a/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/ad;)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7061
    invoke-static {}, Lcom/sec/a/a/a/ad;->newBuilder()Lcom/sec/a/a/a/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/ae;->a(Lcom/sec/a/a/a/ad;)Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a/ad;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 6705
    iput-object p1, p0, Lcom/sec/a/a/a/ad;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/ad;I)I
    .locals 0

    .prologue
    .line 6705
    iput p1, p0, Lcom/sec/a/a/a/ad;->f:I

    return p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/ad;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 6705
    iput-object p1, p0, Lcom/sec/a/a/a/ad;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/ad;I)I
    .locals 0

    .prologue
    .line 6705
    iput p1, p0, Lcom/sec/a/a/a/ad;->h:I

    return p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/ad;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 6705
    iput-object p1, p0, Lcom/sec/a/a/a/ad;->g:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic d(Lcom/sec/a/a/a/ad;I)I
    .locals 0

    .prologue
    .line 6705
    iput p1, p0, Lcom/sec/a/a/a/ad;->i:I

    return p1
.end method

.method static synthetic d(Lcom/sec/a/a/a/ad;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 6705
    iput-object p1, p0, Lcom/sec/a/a/a/ad;->j:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic e(Lcom/sec/a/a/a/ad;I)I
    .locals 0

    .prologue
    .line 6705
    iput p1, p0, Lcom/sec/a/a/a/ad;->b:I

    return p1
.end method

.method public static newBuilder()Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7053
    invoke-static {}, Lcom/sec/a/a/a/ae;->f()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method private u()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6760
    iget-object v0, p0, Lcom/sec/a/a/a/ad;->d:Ljava/lang/Object;

    .line 6761
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6762
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6763
    iput-object v0, p0, Lcom/sec/a/a/a/ad;->d:Ljava/lang/Object;

    .line 6766
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private v()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6793
    iget-object v0, p0, Lcom/sec/a/a/a/ad;->e:Ljava/lang/Object;

    .line 6794
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6795
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6796
    iput-object v0, p0, Lcom/sec/a/a/a/ad;->e:Ljava/lang/Object;

    .line 6799
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private w()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6838
    iget-object v0, p0, Lcom/sec/a/a/a/ad;->g:Ljava/lang/Object;

    .line 6839
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6840
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6841
    iput-object v0, p0, Lcom/sec/a/a/a/ad;->g:Ljava/lang/Object;

    .line 6844
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private x()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6895
    iget-object v0, p0, Lcom/sec/a/a/a/ad;->j:Ljava/lang/Object;

    .line 6896
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6897
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6898
    iput-object v0, p0, Lcom/sec/a/a/a/ad;->j:Ljava/lang/Object;

    .line 6901
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private y()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6906
    iput v1, p0, Lcom/sec/a/a/a/ad;->c:I

    .line 6907
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ad;->d:Ljava/lang/Object;

    .line 6908
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ad;->e:Ljava/lang/Object;

    .line 6909
    iput v1, p0, Lcom/sec/a/a/a/ad;->f:I

    .line 6910
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ad;->g:Ljava/lang/Object;

    .line 6911
    iput v1, p0, Lcom/sec/a/a/a/ad;->h:I

    .line 6912
    iput v1, p0, Lcom/sec/a/a/a/ad;->i:I

    .line 6913
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ad;->j:Ljava/lang/Object;

    .line 6914
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/ad;
    .locals 1

    .prologue
    .line 6721
    sget-object v0, Lcom/sec/a/a/a/ad;->a:Lcom/sec/a/a/a/ad;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6730
    iget v1, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 6734
    iget v0, p0, Lcom/sec/a/a/a/ad;->c:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 6742
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6746
    iget-object v0, p0, Lcom/sec/a/a/a/ad;->d:Ljava/lang/Object;

    .line 6747
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6748
    check-cast v0, Ljava/lang/String;

    .line 6755
    :goto_0
    return-object v0

    .line 6750
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6751
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6752
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6753
    iput-object v1, p0, Lcom/sec/a/a/a/ad;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6755
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 6775
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6705
    invoke-virtual {p0}, Lcom/sec/a/a/a/ad;->b()Lcom/sec/a/a/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    .line 6958
    iget v0, p0, Lcom/sec/a/a/a/ad;->l:I

    .line 6959
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 6988
    :goto_0
    return v0

    .line 6962
    :cond_0
    const/4 v0, 0x0

    .line 6963
    iget v1, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 6964
    iget v1, p0, Lcom/sec/a/a/a/ad;->c:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6966
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 6967
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/ad;->u()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6969
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 6970
    invoke-direct {p0}, Lcom/sec/a/a/a/ad;->v()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6972
    :cond_3
    iget v1, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 6973
    const/4 v1, 0x5

    iget v2, p0, Lcom/sec/a/a/a/ad;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6975
    :cond_4
    iget v1, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 6976
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sec/a/a/a/ad;->w()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6978
    :cond_5
    iget v1, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 6979
    const/4 v1, 0x7

    iget v2, p0, Lcom/sec/a/a/a/ad;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6981
    :cond_6
    iget v1, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 6982
    iget v1, p0, Lcom/sec/a/a/a/ad;->i:I

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6984
    :cond_7
    iget v1, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 6985
    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/sec/a/a/a/ad;->x()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6987
    :cond_8
    iput v0, p0, Lcom/sec/a/a/a/ad;->l:I

    goto/16 :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6779
    iget-object v0, p0, Lcom/sec/a/a/a/ad;->e:Ljava/lang/Object;

    .line 6780
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6781
    check-cast v0, Ljava/lang/String;

    .line 6788
    :goto_0
    return-object v0

    .line 6783
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6784
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6785
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6786
    iput-object v1, p0, Lcom/sec/a/a/a/ad;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6788
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 6808
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 6919
    iget-byte v1, p0, Lcom/sec/a/a/a/ad;->k:B

    .line 6920
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 6921
    if-ne v1, v0, :cond_0

    .line 6924
    :goto_0
    return v0

    .line 6921
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6923
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/ad;->k:B

    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 6812
    iget v0, p0, Lcom/sec/a/a/a/ad;->f:I

    return v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 6820
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6824
    iget-object v0, p0, Lcom/sec/a/a/a/ad;->g:Ljava/lang/Object;

    .line 6825
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6826
    check-cast v0, Ljava/lang/String;

    .line 6833
    :goto_0
    return-object v0

    .line 6828
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6829
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6830
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6831
    iput-object v1, p0, Lcom/sec/a/a/a/ad;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6833
    goto :goto_0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 6853
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 6857
    iget v0, p0, Lcom/sec/a/a/a/ad;->h:I

    return v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6705
    invoke-virtual {p0}, Lcom/sec/a/a/a/ad;->s()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 6865
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 6869
    iget v0, p0, Lcom/sec/a/a/a/ad;->i:I

    return v0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 6877
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6881
    iget-object v0, p0, Lcom/sec/a/a/a/ad;->j:Ljava/lang/Object;

    .line 6882
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6883
    check-cast v0, Ljava/lang/String;

    .line 6890
    :goto_0
    return-object v0

    .line 6885
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6886
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6887
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6888
    iput-object v1, p0, Lcom/sec/a/a/a/ad;->j:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6890
    goto :goto_0
.end method

.method public s()Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7057
    invoke-static {}, Lcom/sec/a/a/a/ad;->newBuilder()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public t()Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7065
    invoke-static {p0}, Lcom/sec/a/a/a/ad;->a(Lcom/sec/a/a/a/ad;)Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6705
    invoke-virtual {p0}, Lcom/sec/a/a/a/ad;->t()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6995
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 6928
    invoke-virtual {p0}, Lcom/sec/a/a/a/ad;->getSerializedSize()I

    .line 6929
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 6930
    iget v0, p0, Lcom/sec/a/a/a/ad;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 6932
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 6933
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/ad;->u()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6935
    :cond_1
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 6936
    invoke-direct {p0}, Lcom/sec/a/a/a/ad;->v()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6938
    :cond_2
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 6939
    const/4 v0, 0x5

    iget v1, p0, Lcom/sec/a/a/a/ad;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 6941
    :cond_3
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 6942
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sec/a/a/a/ad;->w()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6944
    :cond_4
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 6945
    const/4 v0, 0x7

    iget v1, p0, Lcom/sec/a/a/a/ad;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 6947
    :cond_5
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 6948
    iget v0, p0, Lcom/sec/a/a/a/ad;->i:I

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 6950
    :cond_6
    iget v0, p0, Lcom/sec/a/a/a/ad;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 6951
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/sec/a/a/a/ad;->x()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6953
    :cond_7
    return-void
.end method
