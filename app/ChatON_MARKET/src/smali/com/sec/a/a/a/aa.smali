.class public final Lcom/sec/a/a/a/aa;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/ac;


# static fields
.field private static final a:Lcom/sec/a/a/a/aa;


# instance fields
.field private b:I

.field private c:I

.field private d:J

.field private e:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3627
    new-instance v0, Lcom/sec/a/a/a/aa;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/aa;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/aa;->a:Lcom/sec/a/a/a/aa;

    .line 3628
    sget-object v0, Lcom/sec/a/a/a/aa;->a:Lcom/sec/a/a/a/aa;

    invoke-direct {v0}, Lcom/sec/a/a/a/aa;->k()V

    .line 3629
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/ab;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3243
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 3302
    iput-byte v0, p0, Lcom/sec/a/a/a/aa;->f:B

    .line 3326
    iput v0, p0, Lcom/sec/a/a/a/aa;->g:I

    .line 3244
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/ab;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 3240
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/aa;-><init>(Lcom/sec/a/a/a/ab;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3246
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3302
    iput-byte v0, p0, Lcom/sec/a/a/a/aa;->f:B

    .line 3326
    iput v0, p0, Lcom/sec/a/a/a/aa;->g:I

    .line 3247
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/aa;I)I
    .locals 0

    .prologue
    .line 3240
    iput p1, p0, Lcom/sec/a/a/a/aa;->c:I

    return p1
.end method

.method static synthetic a(Lcom/sec/a/a/a/aa;J)J
    .locals 0

    .prologue
    .line 3240
    iput-wide p1, p0, Lcom/sec/a/a/a/aa;->d:J

    return-wide p1
.end method

.method public static a()Lcom/sec/a/a/a/aa;
    .locals 1

    .prologue
    .line 3252
    sget-object v0, Lcom/sec/a/a/a/aa;->a:Lcom/sec/a/a/a/aa;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/aa;
    .locals 1

    .prologue
    .line 3364
    invoke-static {}, Lcom/sec/a/a/a/aa;->newBuilder()Lcom/sec/a/a/a/ab;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/ab;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/ab;

    invoke-static {v0}, Lcom/sec/a/a/a/ab;->a(Lcom/sec/a/a/a/ab;)Lcom/sec/a/a/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/aa;)Lcom/sec/a/a/a/ab;
    .locals 1

    .prologue
    .line 3417
    invoke-static {}, Lcom/sec/a/a/a/aa;->newBuilder()Lcom/sec/a/a/a/ab;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/ab;->a(Lcom/sec/a/a/a/aa;)Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/sec/a/a/a/aa;I)I
    .locals 0

    .prologue
    .line 3240
    iput p1, p0, Lcom/sec/a/a/a/aa;->e:I

    return p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/aa;I)I
    .locals 0

    .prologue
    .line 3240
    iput p1, p0, Lcom/sec/a/a/a/aa;->b:I

    return p1
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3297
    iput v2, p0, Lcom/sec/a/a/a/aa;->c:I

    .line 3298
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/a/a/a/aa;->d:J

    .line 3299
    iput v2, p0, Lcom/sec/a/a/a/aa;->e:I

    .line 3300
    return-void
.end method

.method public static newBuilder()Lcom/sec/a/a/a/ab;
    .locals 1

    .prologue
    .line 3409
    invoke-static {}, Lcom/sec/a/a/a/ab;->f()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/aa;
    .locals 1

    .prologue
    .line 3256
    sget-object v0, Lcom/sec/a/a/a/aa;->a:Lcom/sec/a/a/a/aa;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3265
    iget v1, p0, Lcom/sec/a/a/a/aa;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 3269
    iget v0, p0, Lcom/sec/a/a/a/aa;->c:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 3277
    iget v0, p0, Lcom/sec/a/a/a/aa;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 3281
    iget-wide v0, p0, Lcom/sec/a/a/a/aa;->d:J

    return-wide v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 3289
    iget v0, p0, Lcom/sec/a/a/a/aa;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3240
    invoke-virtual {p0}, Lcom/sec/a/a/a/aa;->b()Lcom/sec/a/a/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3329
    iget v0, p0, Lcom/sec/a/a/a/aa;->g:I

    .line 3330
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3344
    :goto_0
    return v0

    .line 3333
    :cond_0
    const/4 v0, 0x0

    .line 3334
    iget v1, p0, Lcom/sec/a/a/a/aa;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 3335
    iget v1, p0, Lcom/sec/a/a/a/aa;->c:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3337
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/aa;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 3338
    iget-wide v1, p0, Lcom/sec/a/a/a/aa;->d:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3340
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/aa;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 3341
    const/4 v1, 0x3

    iget v2, p0, Lcom/sec/a/a/a/aa;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3343
    :cond_3
    iput v0, p0, Lcom/sec/a/a/a/aa;->g:I

    goto :goto_0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 3293
    iget v0, p0, Lcom/sec/a/a/a/aa;->e:I

    return v0
.end method

.method public i()Lcom/sec/a/a/a/ab;
    .locals 1

    .prologue
    .line 3413
    invoke-static {}, Lcom/sec/a/a/a/aa;->newBuilder()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3305
    iget-byte v1, p0, Lcom/sec/a/a/a/aa;->f:B

    .line 3306
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 3307
    if-ne v1, v0, :cond_0

    .line 3310
    :goto_0
    return v0

    .line 3307
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3309
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/aa;->f:B

    goto :goto_0
.end method

.method public j()Lcom/sec/a/a/a/ab;
    .locals 1

    .prologue
    .line 3421
    invoke-static {p0}, Lcom/sec/a/a/a/aa;->a(Lcom/sec/a/a/a/aa;)Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3240
    invoke-virtual {p0}, Lcom/sec/a/a/a/aa;->i()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3240
    invoke-virtual {p0}, Lcom/sec/a/a/a/aa;->j()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3351
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3314
    invoke-virtual {p0}, Lcom/sec/a/a/a/aa;->getSerializedSize()I

    .line 3315
    iget v0, p0, Lcom/sec/a/a/a/aa;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3316
    iget v0, p0, Lcom/sec/a/a/a/aa;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3318
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/aa;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3319
    iget-wide v0, p0, Lcom/sec/a/a/a/aa;->d:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 3321
    :cond_1
    iget v0, p0, Lcom/sec/a/a/a/aa;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 3322
    const/4 v0, 0x3

    iget v1, p0, Lcom/sec/a/a/a/aa;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3324
    :cond_2
    return-void
.end method
