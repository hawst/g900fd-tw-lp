.class public final Lcom/sec/a/a/a/l;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/n;


# static fields
.field private static final a:Lcom/sec/a/a/a/l;


# instance fields
.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 392
    new-instance v0, Lcom/sec/a/a/a/l;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/l;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/l;->a:Lcom/sec/a/a/a/l;

    .line 393
    sget-object v0, Lcom/sec/a/a/a/l;->a:Lcom/sec/a/a/a/l;

    invoke-direct {v0}, Lcom/sec/a/a/a/l;->j()V

    .line 394
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/m;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 29
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 96
    iput-byte v0, p0, Lcom/sec/a/a/a/l;->e:B

    .line 117
    iput v0, p0, Lcom/sec/a/a/a/l;->f:I

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/m;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/l;-><init>(Lcom/sec/a/a/a/m;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 32
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 96
    iput-byte v0, p0, Lcom/sec/a/a/a/l;->e:B

    .line 117
    iput v0, p0, Lcom/sec/a/a/a/l;->f:I

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/l;I)I
    .locals 0

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/a/a/a/l;->c:I

    return p1
.end method

.method public static a()Lcom/sec/a/a/a/l;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/a/a/a/l;->a:Lcom/sec/a/a/a/l;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/l;
    .locals 1

    .prologue
    .line 152
    invoke-static {}, Lcom/sec/a/a/a/l;->newBuilder()Lcom/sec/a/a/a/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/m;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/m;

    invoke-static {v0}, Lcom/sec/a/a/a/m;->a(Lcom/sec/a/a/a/m;)Lcom/sec/a/a/a/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/l;)Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 205
    invoke-static {}, Lcom/sec/a/a/a/l;->newBuilder()Lcom/sec/a/a/a/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/m;->a(Lcom/sec/a/a/a/l;)Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a/l;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/a/a/a/l;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/l;I)I
    .locals 0

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/a/a/a/l;->b:I

    return p1
.end method

.method private i()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/a/a/a/l;->d:Ljava/lang/Object;

    .line 82
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 83
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 84
    iput-object v0, p0, Lcom/sec/a/a/a/l;->d:Ljava/lang/Object;

    .line 87
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/a/a/a/l;->c:I

    .line 93
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/l;->d:Ljava/lang/Object;

    .line 94
    return-void
.end method

.method public static newBuilder()Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 197
    invoke-static {}, Lcom/sec/a/a/a/m;->f()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/l;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/a/a/a/l;->a:Lcom/sec/a/a/a/l;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 51
    iget v1, p0, Lcom/sec/a/a/a/l;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/a/a/a/l;->c:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/a/a/a/l;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/a/a/a/l;->d:Ljava/lang/Object;

    .line 68
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 69
    check-cast v0, Ljava/lang/String;

    .line 76
    :goto_0
    return-object v0

    .line 71
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 72
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    iput-object v1, p0, Lcom/sec/a/a/a/l;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 76
    goto :goto_0
.end method

.method public g()Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 201
    invoke-static {}, Lcom/sec/a/a/a/l;->newBuilder()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/a/a/a/l;->b()Lcom/sec/a/a/a/l;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 120
    iget v0, p0, Lcom/sec/a/a/a/l;->f:I

    .line 121
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 132
    :goto_0
    return v0

    .line 124
    :cond_0
    const/4 v0, 0x0

    .line 125
    iget v1, p0, Lcom/sec/a/a/a/l;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 126
    iget v1, p0, Lcom/sec/a/a/a/l;->c:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/l;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 129
    invoke-direct {p0}, Lcom/sec/a/a/a/l;->i()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_2
    iput v0, p0, Lcom/sec/a/a/a/l;->f:I

    goto :goto_0
.end method

.method public h()Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 209
    invoke-static {p0}, Lcom/sec/a/a/a/l;->a(Lcom/sec/a/a/a/l;)Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 99
    iget-byte v1, p0, Lcom/sec/a/a/a/l;->e:B

    .line 100
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 101
    if-ne v1, v0, :cond_0

    .line 104
    :goto_0
    return v0

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 103
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/l;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/a/a/a/l;->g()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/a/a/a/l;->h()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 108
    invoke-virtual {p0}, Lcom/sec/a/a/a/l;->getSerializedSize()I

    .line 109
    iget v0, p0, Lcom/sec/a/a/a/l;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 110
    iget v0, p0, Lcom/sec/a/a/a/l;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 112
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/l;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 113
    invoke-direct {p0}, Lcom/sec/a/a/a/l;->i()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 115
    :cond_1
    return-void
.end method
