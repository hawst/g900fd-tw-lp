.class public final Lcom/sec/a/a/a/am;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/ao;


# static fields
.field private static final a:Lcom/sec/a/a/a/am;


# instance fields
.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1428
    new-instance v0, Lcom/sec/a/a/a/am;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/am;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/am;->a:Lcom/sec/a/a/a/am;

    .line 1429
    sget-object v0, Lcom/sec/a/a/a/am;->a:Lcom/sec/a/a/a/am;

    invoke-direct {v0}, Lcom/sec/a/a/a/am;->p()V

    .line 1430
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/an;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 874
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 1009
    iput-byte v0, p0, Lcom/sec/a/a/a/am;->g:B

    .line 1036
    iput v0, p0, Lcom/sec/a/a/a/am;->h:I

    .line 875
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/an;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 871
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/am;-><init>(Lcom/sec/a/a/a/an;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 877
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1009
    iput-byte v0, p0, Lcom/sec/a/a/a/am;->g:B

    .line 1036
    iput v0, p0, Lcom/sec/a/a/a/am;->h:I

    .line 878
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/am;I)I
    .locals 0

    .prologue
    .line 871
    iput p1, p0, Lcom/sec/a/a/a/am;->c:I

    return p1
.end method

.method public static a()Lcom/sec/a/a/a/am;
    .locals 1

    .prologue
    .line 883
    sget-object v0, Lcom/sec/a/a/a/am;->a:Lcom/sec/a/a/a/am;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/am;
    .locals 1

    .prologue
    .line 1077
    invoke-static {}, Lcom/sec/a/a/a/am;->newBuilder()Lcom/sec/a/a/a/an;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/an;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/an;

    invoke-static {v0}, Lcom/sec/a/a/a/an;->a(Lcom/sec/a/a/a/an;)Lcom/sec/a/a/a/am;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/am;)Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1131
    invoke-static {}, Lcom/sec/a/a/a/am;->newBuilder()Lcom/sec/a/a/a/an;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/an;->a(Lcom/sec/a/a/a/am;)Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a/am;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 871
    iput-object p1, p0, Lcom/sec/a/a/a/am;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/am;I)I
    .locals 0

    .prologue
    .line 871
    iput p1, p0, Lcom/sec/a/a/a/am;->b:I

    return p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/am;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 871
    iput-object p1, p0, Lcom/sec/a/a/a/am;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/am;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 871
    iput-object p1, p0, Lcom/sec/a/a/a/am;->f:Ljava/lang/Object;

    return-object p1
.end method

.method private m()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 926
    iget-object v0, p0, Lcom/sec/a/a/a/am;->d:Ljava/lang/Object;

    .line 927
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 928
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 929
    iput-object v0, p0, Lcom/sec/a/a/a/am;->d:Ljava/lang/Object;

    .line 932
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private n()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 959
    iget-object v0, p0, Lcom/sec/a/a/a/am;->e:Ljava/lang/Object;

    .line 960
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 961
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 962
    iput-object v0, p0, Lcom/sec/a/a/a/am;->e:Ljava/lang/Object;

    .line 965
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static newBuilder()Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1123
    invoke-static {}, Lcom/sec/a/a/a/an;->f()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 992
    iget-object v0, p0, Lcom/sec/a/a/a/am;->f:Ljava/lang/Object;

    .line 993
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 994
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 995
    iput-object v0, p0, Lcom/sec/a/a/a/am;->f:Ljava/lang/Object;

    .line 998
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private p()V
    .locals 1

    .prologue
    .line 1003
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/a/a/a/am;->c:I

    .line 1004
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/am;->d:Ljava/lang/Object;

    .line 1005
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/am;->e:Ljava/lang/Object;

    .line 1006
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/am;->f:Ljava/lang/Object;

    .line 1007
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/am;
    .locals 1

    .prologue
    .line 887
    sget-object v0, Lcom/sec/a/a/a/am;->a:Lcom/sec/a/a/a/am;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 896
    iget v1, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 900
    iget v0, p0, Lcom/sec/a/a/a/am;->c:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 908
    iget v0, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 912
    iget-object v0, p0, Lcom/sec/a/a/a/am;->d:Ljava/lang/Object;

    .line 913
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 914
    check-cast v0, Ljava/lang/String;

    .line 921
    :goto_0
    return-object v0

    .line 916
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 917
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 918
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 919
    iput-object v1, p0, Lcom/sec/a/a/a/am;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 921
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 941
    iget v0, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 871
    invoke-virtual {p0}, Lcom/sec/a/a/a/am;->b()Lcom/sec/a/a/a/am;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1039
    iget v0, p0, Lcom/sec/a/a/a/am;->h:I

    .line 1040
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1057
    :goto_0
    return v0

    .line 1043
    :cond_0
    const/4 v0, 0x0

    .line 1044
    iget v1, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 1045
    iget v1, p0, Lcom/sec/a/a/a/am;->c:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1047
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 1048
    invoke-direct {p0}, Lcom/sec/a/a/a/am;->m()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1050
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 1051
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/am;->n()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1053
    :cond_3
    iget v1, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 1054
    invoke-direct {p0}, Lcom/sec/a/a/a/am;->o()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1056
    :cond_4
    iput v0, p0, Lcom/sec/a/a/a/am;->h:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 945
    iget-object v0, p0, Lcom/sec/a/a/a/am;->e:Ljava/lang/Object;

    .line 946
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 947
    check-cast v0, Ljava/lang/String;

    .line 954
    :goto_0
    return-object v0

    .line 949
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 950
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 951
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 952
    iput-object v1, p0, Lcom/sec/a/a/a/am;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 954
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 974
    iget v0, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1012
    iget-byte v1, p0, Lcom/sec/a/a/a/am;->g:B

    .line 1013
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 1014
    if-ne v1, v0, :cond_0

    .line 1017
    :goto_0
    return v0

    .line 1014
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1016
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/am;->g:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 978
    iget-object v0, p0, Lcom/sec/a/a/a/am;->f:Ljava/lang/Object;

    .line 979
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 980
    check-cast v0, Ljava/lang/String;

    .line 987
    :goto_0
    return-object v0

    .line 982
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 983
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 984
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 985
    iput-object v1, p0, Lcom/sec/a/a/a/am;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 987
    goto :goto_0
.end method

.method public k()Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1127
    invoke-static {}, Lcom/sec/a/a/a/am;->newBuilder()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1135
    invoke-static {p0}, Lcom/sec/a/a/a/am;->a(Lcom/sec/a/a/a/am;)Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 871
    invoke-virtual {p0}, Lcom/sec/a/a/a/am;->k()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 871
    invoke-virtual {p0}, Lcom/sec/a/a/a/am;->l()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1064
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1021
    invoke-virtual {p0}, Lcom/sec/a/a/a/am;->getSerializedSize()I

    .line 1022
    iget v0, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1023
    iget v0, p0, Lcom/sec/a/a/a/am;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1025
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1026
    invoke-direct {p0}, Lcom/sec/a/a/a/am;->m()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1028
    :cond_1
    iget v0, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 1029
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/am;->n()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1031
    :cond_2
    iget v0, p0, Lcom/sec/a/a/a/am;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1032
    invoke-direct {p0}, Lcom/sec/a/a/a/am;->o()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 1034
    :cond_3
    return-void
.end method
