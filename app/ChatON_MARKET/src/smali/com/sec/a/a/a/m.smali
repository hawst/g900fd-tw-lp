.class public final Lcom/sec/a/a/a/m;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/l;",
        "Lcom/sec/a/a/a/m;",
        ">;",
        "Lcom/sec/a/a/a/n;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 216
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 348
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/m;->c:Ljava/lang/Object;

    .line 217
    invoke-direct {p0}, Lcom/sec/a/a/a/m;->g()V

    .line 218
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/m;)Lcom/sec/a/a/a/l;
    .locals 1

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/sec/a/a/a/m;->i()Lcom/sec/a/a/a/l;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 212
    invoke-static {}, Lcom/sec/a/a/a/m;->h()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 221
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 224
    new-instance v0, Lcom/sec/a/a/a/m;

    invoke-direct {v0}, Lcom/sec/a/a/a/m;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/l;
    .locals 2

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->e()Lcom/sec/a/a/a/l;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Lcom/sec/a/a/a/l;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 255
    invoke-static {v0}, Lcom/sec/a/a/a/m;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 257
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 228
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 229
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/a/a/a/m;->b:I

    .line 230
    iget v0, p0, Lcom/sec/a/a/a/m;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/m;->a:I

    .line 231
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/m;->c:Ljava/lang/Object;

    .line 232
    iget v0, p0, Lcom/sec/a/a/a/m;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/m;->a:I

    .line 233
    return-object p0
.end method

.method public a(I)Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 334
    iget v0, p0, Lcom/sec/a/a/a/m;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/m;->a:I

    .line 335
    iput p1, p0, Lcom/sec/a/a/a/m;->b:I

    .line 337
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 294
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 295
    sparse-switch v0, :sswitch_data_0

    .line 300
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/m;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    :sswitch_0
    return-object p0

    .line 307
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/m;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/m;->a:I

    .line 308
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/m;->b:I

    goto :goto_0

    .line 312
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/m;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/m;->a:I

    .line 313
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/m;->c:Ljava/lang/Object;

    goto :goto_0

    .line 295
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/l;)Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 277
    invoke-static {}, Lcom/sec/a/a/a/l;->a()Lcom/sec/a/a/a/l;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 285
    :cond_0
    :goto_0
    return-object p0

    .line 279
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/l;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 280
    invoke-virtual {p1}, Lcom/sec/a/a/a/l;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/m;->a(I)Lcom/sec/a/a/a/m;

    .line 282
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/l;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p1}, Lcom/sec/a/a/a/l;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/m;->a(Ljava/lang/String;)Lcom/sec/a/a/a/m;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/a/a/a/m;
    .locals 1

    .prologue
    .line 366
    if-nez p1, :cond_0

    .line 367
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 369
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/m;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/m;->a:I

    .line 370
    iput-object p1, p0, Lcom/sec/a/a/a/m;->c:Ljava/lang/Object;

    .line 372
    return-object p0
.end method

.method public b()Lcom/sec/a/a/a/m;
    .locals 2

    .prologue
    .line 237
    invoke-static {}, Lcom/sec/a/a/a/m;->h()Lcom/sec/a/a/a/m;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->e()Lcom/sec/a/a/a/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/m;->a(Lcom/sec/a/a/a/l;)Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->d()Lcom/sec/a/a/a/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->e()Lcom/sec/a/a/a/l;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/l;
    .locals 1

    .prologue
    .line 241
    invoke-static {}, Lcom/sec/a/a/a/l;->a()Lcom/sec/a/a/a/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->a()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->a()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->b()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->b()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->b()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->b()Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/l;
    .locals 2

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->e()Lcom/sec/a/a/a/l;

    move-result-object v0

    .line 246
    invoke-virtual {v0}, Lcom/sec/a/a/a/l;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 247
    invoke-static {v0}, Lcom/sec/a/a/a/m;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 249
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/a/a/a/l;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 261
    new-instance v2, Lcom/sec/a/a/a/l;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/l;-><init>(Lcom/sec/a/a/a/m;Lcom/sec/a/a/a/b;)V

    .line 262
    iget v3, p0, Lcom/sec/a/a/a/m;->a:I

    .line 263
    const/4 v1, 0x0

    .line 264
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 267
    :goto_0
    iget v1, p0, Lcom/sec/a/a/a/m;->b:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/l;->a(Lcom/sec/a/a/a/l;I)I

    .line 268
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 269
    or-int/lit8 v0, v0, 0x2

    .line 271
    :cond_0
    iget-object v1, p0, Lcom/sec/a/a/a/m;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/l;->a(Lcom/sec/a/a/a/l;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    invoke-static {v2, v0}, Lcom/sec/a/a/a/l;->b(Lcom/sec/a/a/a/l;I)I

    .line 273
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->c()Lcom/sec/a/a/a/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/a/a/a/m;->c()Lcom/sec/a/a/a/l;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/m;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 212
    check-cast p1, Lcom/sec/a/a/a/l;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/m;->a(Lcom/sec/a/a/a/l;)Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/m;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/m;

    move-result-object v0

    return-object v0
.end method
