.class public final Lcom/sec/a/a/a/ab;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/ac;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/aa;",
        "Lcom/sec/a/a/a/ab;",
        ">;",
        "Lcom/sec/a/a/a/ac;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:J

.field private d:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3428
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 3429
    invoke-direct {p0}, Lcom/sec/a/a/a/ab;->g()V

    .line 3430
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/ab;)Lcom/sec/a/a/a/aa;
    .locals 1

    .prologue
    .line 3424
    invoke-direct {p0}, Lcom/sec/a/a/a/ab;->i()Lcom/sec/a/a/a/aa;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/ab;
    .locals 1

    .prologue
    .line 3424
    invoke-static {}, Lcom/sec/a/a/a/ab;->h()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 3433
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/ab;
    .locals 1

    .prologue
    .line 3436
    new-instance v0, Lcom/sec/a/a/a/ab;

    invoke-direct {v0}, Lcom/sec/a/a/a/ab;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/aa;
    .locals 2

    .prologue
    .line 3467
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->e()Lcom/sec/a/a/a/aa;

    move-result-object v0

    .line 3468
    invoke-virtual {v0}, Lcom/sec/a/a/a/aa;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3469
    invoke-static {v0}, Lcom/sec/a/a/a/ab;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 3471
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/ab;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3440
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 3441
    iput v2, p0, Lcom/sec/a/a/a/ab;->b:I

    .line 3442
    iget v0, p0, Lcom/sec/a/a/a/ab;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/ab;->a:I

    .line 3443
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/a/a/a/ab;->c:J

    .line 3444
    iget v0, p0, Lcom/sec/a/a/a/ab;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/ab;->a:I

    .line 3445
    iput v2, p0, Lcom/sec/a/a/a/ab;->d:I

    .line 3446
    iget v0, p0, Lcom/sec/a/a/a/ab;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/a/a/a/ab;->a:I

    .line 3447
    return-object p0
.end method

.method public a(I)Lcom/sec/a/a/a/ab;
    .locals 1

    .prologue
    .line 3560
    iget v0, p0, Lcom/sec/a/a/a/ab;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/ab;->a:I

    .line 3561
    iput p1, p0, Lcom/sec/a/a/a/ab;->b:I

    .line 3563
    return-object p0
.end method

.method public a(J)Lcom/sec/a/a/a/ab;
    .locals 1

    .prologue
    .line 3585
    iget v0, p0, Lcom/sec/a/a/a/ab;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/ab;->a:I

    .line 3586
    iput-wide p1, p0, Lcom/sec/a/a/a/ab;->c:J

    .line 3588
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ab;
    .locals 2

    .prologue
    .line 3515
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 3516
    sparse-switch v0, :sswitch_data_0

    .line 3521
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/ab;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3523
    :sswitch_0
    return-object p0

    .line 3528
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/ab;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/ab;->a:I

    .line 3529
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/ab;->b:I

    goto :goto_0

    .line 3533
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/ab;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/ab;->a:I

    .line 3534
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/a/a/a/ab;->c:J

    goto :goto_0

    .line 3538
    :sswitch_3
    iget v0, p0, Lcom/sec/a/a/a/ab;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/ab;->a:I

    .line 3539
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/ab;->d:I

    goto :goto_0

    .line 3516
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/aa;)Lcom/sec/a/a/a/ab;
    .locals 2

    .prologue
    .line 3495
    invoke-static {}, Lcom/sec/a/a/a/aa;->a()Lcom/sec/a/a/a/aa;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 3506
    :cond_0
    :goto_0
    return-object p0

    .line 3497
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/aa;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3498
    invoke-virtual {p1}, Lcom/sec/a/a/a/aa;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ab;->a(I)Lcom/sec/a/a/a/ab;

    .line 3500
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/aa;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3501
    invoke-virtual {p1}, Lcom/sec/a/a/a/aa;->f()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/a/a/a/ab;->a(J)Lcom/sec/a/a/a/ab;

    .line 3503
    :cond_3
    invoke-virtual {p1}, Lcom/sec/a/a/a/aa;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3504
    invoke-virtual {p1}, Lcom/sec/a/a/a/aa;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ab;->b(I)Lcom/sec/a/a/a/ab;

    goto :goto_0
.end method

.method public b()Lcom/sec/a/a/a/ab;
    .locals 2

    .prologue
    .line 3451
    invoke-static {}, Lcom/sec/a/a/a/ab;->h()Lcom/sec/a/a/a/ab;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->e()Lcom/sec/a/a/a/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/ab;->a(Lcom/sec/a/a/a/aa;)Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/sec/a/a/a/ab;
    .locals 1

    .prologue
    .line 3610
    iget v0, p0, Lcom/sec/a/a/a/ab;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/ab;->a:I

    .line 3611
    iput p1, p0, Lcom/sec/a/a/a/ab;->d:I

    .line 3613
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->d()Lcom/sec/a/a/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->e()Lcom/sec/a/a/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/aa;
    .locals 1

    .prologue
    .line 3455
    invoke-static {}, Lcom/sec/a/a/a/aa;->a()Lcom/sec/a/a/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->a()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->a()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->b()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->b()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->b()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->b()Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/aa;
    .locals 2

    .prologue
    .line 3459
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->e()Lcom/sec/a/a/a/aa;

    move-result-object v0

    .line 3460
    invoke-virtual {v0}, Lcom/sec/a/a/a/aa;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3461
    invoke-static {v0}, Lcom/sec/a/a/a/ab;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 3463
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/a/a/a/aa;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 3475
    new-instance v2, Lcom/sec/a/a/a/aa;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/aa;-><init>(Lcom/sec/a/a/a/ab;Lcom/sec/a/a/a/b;)V

    .line 3476
    iget v3, p0, Lcom/sec/a/a/a/ab;->a:I

    .line 3477
    const/4 v1, 0x0

    .line 3478
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 3481
    :goto_0
    iget v1, p0, Lcom/sec/a/a/a/ab;->b:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/aa;->a(Lcom/sec/a/a/a/aa;I)I

    .line 3482
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 3483
    or-int/lit8 v0, v0, 0x2

    .line 3485
    :cond_0
    iget-wide v4, p0, Lcom/sec/a/a/a/ab;->c:J

    invoke-static {v2, v4, v5}, Lcom/sec/a/a/a/aa;->a(Lcom/sec/a/a/a/aa;J)J

    .line 3486
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 3487
    or-int/lit8 v0, v0, 0x4

    .line 3489
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/ab;->d:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/aa;->b(Lcom/sec/a/a/a/aa;I)I

    .line 3490
    invoke-static {v2, v0}, Lcom/sec/a/a/a/aa;->c(Lcom/sec/a/a/a/aa;I)I

    .line 3491
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->c()Lcom/sec/a/a/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0}, Lcom/sec/a/a/a/ab;->c()Lcom/sec/a/a/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 3510
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/ab;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3424
    check-cast p1, Lcom/sec/a/a/a/aa;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/ab;->a(Lcom/sec/a/a/a/aa;)Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3424
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/ab;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ab;

    move-result-object v0

    return-object v0
.end method
