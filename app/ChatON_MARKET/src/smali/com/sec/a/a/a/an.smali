.class public final Lcom/sec/a/a/a/an;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/ao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/am;",
        "Lcom/sec/a/a/a/an;",
        ">;",
        "Lcom/sec/a/a/a/ao;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1142
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 1302
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/an;->c:Ljava/lang/Object;

    .line 1343
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/an;->d:Ljava/lang/Object;

    .line 1384
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/an;->e:Ljava/lang/Object;

    .line 1143
    invoke-direct {p0}, Lcom/sec/a/a/a/an;->g()V

    .line 1144
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/an;)Lcom/sec/a/a/a/am;
    .locals 1

    .prologue
    .line 1138
    invoke-direct {p0}, Lcom/sec/a/a/a/an;->i()Lcom/sec/a/a/a/am;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1138
    invoke-static {}, Lcom/sec/a/a/a/an;->h()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 1147
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1150
    new-instance v0, Lcom/sec/a/a/a/an;

    invoke-direct {v0}, Lcom/sec/a/a/a/an;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/am;
    .locals 2

    .prologue
    .line 1183
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->e()Lcom/sec/a/a/a/am;

    move-result-object v0

    .line 1184
    invoke-virtual {v0}, Lcom/sec/a/a/a/am;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1185
    invoke-static {v0}, Lcom/sec/a/a/a/an;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 1187
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1154
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 1155
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/a/a/a/an;->b:I

    .line 1156
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1157
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/an;->c:Ljava/lang/Object;

    .line 1158
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1159
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/an;->d:Ljava/lang/Object;

    .line 1160
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1161
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/an;->e:Ljava/lang/Object;

    .line 1162
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1163
    return-object p0
.end method

.method public a(I)Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1288
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1289
    iput p1, p0, Lcom/sec/a/a/a/an;->b:I

    .line 1291
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1238
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 1239
    sparse-switch v0, :sswitch_data_0

    .line 1244
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/an;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1246
    :sswitch_0
    return-object p0

    .line 1251
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1252
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/an;->b:I

    goto :goto_0

    .line 1256
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1257
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/an;->c:Ljava/lang/Object;

    goto :goto_0

    .line 1261
    :sswitch_3
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1262
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/an;->d:Ljava/lang/Object;

    goto :goto_0

    .line 1266
    :sswitch_4
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1267
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/an;->e:Ljava/lang/Object;

    goto :goto_0

    .line 1239
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/am;)Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1215
    invoke-static {}, Lcom/sec/a/a/a/am;->a()Lcom/sec/a/a/a/am;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1229
    :cond_0
    :goto_0
    return-object p0

    .line 1217
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/am;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1218
    invoke-virtual {p1}, Lcom/sec/a/a/a/am;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/an;->a(I)Lcom/sec/a/a/a/an;

    .line 1220
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/am;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1221
    invoke-virtual {p1}, Lcom/sec/a/a/a/am;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/an;->a(Ljava/lang/String;)Lcom/sec/a/a/a/an;

    .line 1223
    :cond_3
    invoke-virtual {p1}, Lcom/sec/a/a/a/am;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1224
    invoke-virtual {p1}, Lcom/sec/a/a/a/am;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/an;->b(Ljava/lang/String;)Lcom/sec/a/a/a/an;

    .line 1226
    :cond_4
    invoke-virtual {p1}, Lcom/sec/a/a/a/am;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1227
    invoke-virtual {p1}, Lcom/sec/a/a/a/am;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/an;->c(Ljava/lang/String;)Lcom/sec/a/a/a/an;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1320
    if-nez p1, :cond_0

    .line 1321
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1323
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1324
    iput-object p1, p0, Lcom/sec/a/a/a/an;->c:Ljava/lang/Object;

    .line 1326
    return-object p0
.end method

.method public b()Lcom/sec/a/a/a/an;
    .locals 2

    .prologue
    .line 1167
    invoke-static {}, Lcom/sec/a/a/a/an;->h()Lcom/sec/a/a/a/an;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->e()Lcom/sec/a/a/a/am;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/an;->a(Lcom/sec/a/a/a/am;)Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1361
    if-nez p1, :cond_0

    .line 1362
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1364
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1365
    iput-object p1, p0, Lcom/sec/a/a/a/an;->d:Ljava/lang/Object;

    .line 1367
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->d()Lcom/sec/a/a/a/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->e()Lcom/sec/a/a/a/am;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/am;
    .locals 1

    .prologue
    .line 1171
    invoke-static {}, Lcom/sec/a/a/a/am;->a()Lcom/sec/a/a/a/am;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/a/a/a/an;
    .locals 1

    .prologue
    .line 1402
    if-nez p1, :cond_0

    .line 1403
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1405
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/an;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1406
    iput-object p1, p0, Lcom/sec/a/a/a/an;->e:Ljava/lang/Object;

    .line 1408
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->a()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->a()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->b()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->b()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->b()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->b()Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/am;
    .locals 2

    .prologue
    .line 1175
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->e()Lcom/sec/a/a/a/am;

    move-result-object v0

    .line 1176
    invoke-virtual {v0}, Lcom/sec/a/a/a/am;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1177
    invoke-static {v0}, Lcom/sec/a/a/a/an;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1179
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/a/a/a/am;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1191
    new-instance v2, Lcom/sec/a/a/a/am;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/am;-><init>(Lcom/sec/a/a/a/an;Lcom/sec/a/a/a/b;)V

    .line 1192
    iget v3, p0, Lcom/sec/a/a/a/an;->a:I

    .line 1193
    const/4 v1, 0x0

    .line 1194
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 1197
    :goto_0
    iget v1, p0, Lcom/sec/a/a/a/an;->b:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/am;->a(Lcom/sec/a/a/a/am;I)I

    .line 1198
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1199
    or-int/lit8 v0, v0, 0x2

    .line 1201
    :cond_0
    iget-object v1, p0, Lcom/sec/a/a/a/an;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/am;->a(Lcom/sec/a/a/a/am;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1202
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 1203
    or-int/lit8 v0, v0, 0x4

    .line 1205
    :cond_1
    iget-object v1, p0, Lcom/sec/a/a/a/an;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/am;->b(Lcom/sec/a/a/a/am;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1206
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 1207
    or-int/lit8 v0, v0, 0x8

    .line 1209
    :cond_2
    iget-object v1, p0, Lcom/sec/a/a/a/an;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/am;->c(Lcom/sec/a/a/a/am;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210
    invoke-static {v2, v0}, Lcom/sec/a/a/a/am;->b(Lcom/sec/a/a/a/am;I)I

    .line 1211
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->c()Lcom/sec/a/a/a/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/sec/a/a/a/an;->c()Lcom/sec/a/a/a/am;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 1233
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/an;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1138
    check-cast p1, Lcom/sec/a/a/a/am;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/an;->a(Lcom/sec/a/a/a/am;)Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/an;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/an;

    move-result-object v0

    return-object v0
.end method
