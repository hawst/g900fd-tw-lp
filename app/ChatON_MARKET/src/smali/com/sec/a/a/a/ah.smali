.class public final Lcom/sec/a/a/a/ah;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/ai;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/ag;",
        "Lcom/sec/a/a/a/ah;",
        ">;",
        "Lcom/sec/a/a/a/ai;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 6298
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 6447
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->b:Ljava/lang/Object;

    .line 6488
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->c:Ljava/lang/Object;

    .line 6529
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->d:Ljava/lang/Object;

    .line 6570
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->e:Ljava/lang/Object;

    .line 6611
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->f:Ljava/lang/Object;

    .line 6299
    invoke-direct {p0}, Lcom/sec/a/a/a/ah;->g()V

    .line 6300
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/ah;)Lcom/sec/a/a/a/ag;
    .locals 1

    .prologue
    .line 6294
    invoke-direct {p0}, Lcom/sec/a/a/a/ah;->i()Lcom/sec/a/a/a/ag;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6294
    invoke-static {}, Lcom/sec/a/a/a/ah;->h()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 6303
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6306
    new-instance v0, Lcom/sec/a/a/a/ah;

    invoke-direct {v0}, Lcom/sec/a/a/a/ah;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/ag;
    .locals 2

    .prologue
    .line 6341
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->e()Lcom/sec/a/a/a/ag;

    move-result-object v0

    .line 6342
    invoke-virtual {v0}, Lcom/sec/a/a/a/ag;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6343
    invoke-static {v0}, Lcom/sec/a/a/a/ah;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 6345
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6310
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 6311
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->b:Ljava/lang/Object;

    .line 6312
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6313
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->c:Ljava/lang/Object;

    .line 6314
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6315
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->d:Ljava/lang/Object;

    .line 6316
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6317
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->e:Ljava/lang/Object;

    .line 6318
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6319
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->f:Ljava/lang/Object;

    .line 6320
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6321
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6403
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 6404
    sparse-switch v0, :sswitch_data_0

    .line 6409
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/ah;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6411
    :sswitch_0
    return-object p0

    .line 6416
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6417
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->b:Ljava/lang/Object;

    goto :goto_0

    .line 6421
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6422
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->c:Ljava/lang/Object;

    goto :goto_0

    .line 6426
    :sswitch_3
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6427
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->d:Ljava/lang/Object;

    goto :goto_0

    .line 6431
    :sswitch_4
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6432
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->e:Ljava/lang/Object;

    goto :goto_0

    .line 6436
    :sswitch_5
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6437
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ah;->f:Ljava/lang/Object;

    goto :goto_0

    .line 6404
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/ag;)Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6377
    invoke-static {}, Lcom/sec/a/a/a/ag;->a()Lcom/sec/a/a/a/ag;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 6394
    :cond_0
    :goto_0
    return-object p0

    .line 6379
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/ag;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6380
    invoke-virtual {p1}, Lcom/sec/a/a/a/ag;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ah;->a(Ljava/lang/String;)Lcom/sec/a/a/a/ah;

    .line 6382
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/ag;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6383
    invoke-virtual {p1}, Lcom/sec/a/a/a/ag;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ah;->b(Ljava/lang/String;)Lcom/sec/a/a/a/ah;

    .line 6385
    :cond_3
    invoke-virtual {p1}, Lcom/sec/a/a/a/ag;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6386
    invoke-virtual {p1}, Lcom/sec/a/a/a/ag;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ah;->c(Ljava/lang/String;)Lcom/sec/a/a/a/ah;

    .line 6388
    :cond_4
    invoke-virtual {p1}, Lcom/sec/a/a/a/ag;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 6389
    invoke-virtual {p1}, Lcom/sec/a/a/a/ag;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ah;->d(Ljava/lang/String;)Lcom/sec/a/a/a/ah;

    .line 6391
    :cond_5
    invoke-virtual {p1}, Lcom/sec/a/a/a/ag;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6392
    invoke-virtual {p1}, Lcom/sec/a/a/a/ag;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ah;->e(Ljava/lang/String;)Lcom/sec/a/a/a/ah;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6465
    if-nez p1, :cond_0

    .line 6466
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6468
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6469
    iput-object p1, p0, Lcom/sec/a/a/a/ah;->b:Ljava/lang/Object;

    .line 6471
    return-object p0
.end method

.method public b()Lcom/sec/a/a/a/ah;
    .locals 2

    .prologue
    .line 6325
    invoke-static {}, Lcom/sec/a/a/a/ah;->h()Lcom/sec/a/a/a/ah;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->e()Lcom/sec/a/a/a/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/ah;->a(Lcom/sec/a/a/a/ag;)Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6506
    if-nez p1, :cond_0

    .line 6507
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6509
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6510
    iput-object p1, p0, Lcom/sec/a/a/a/ah;->c:Ljava/lang/Object;

    .line 6512
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->d()Lcom/sec/a/a/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->e()Lcom/sec/a/a/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/ag;
    .locals 1

    .prologue
    .line 6329
    invoke-static {}, Lcom/sec/a/a/a/ag;->a()Lcom/sec/a/a/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6547
    if-nez p1, :cond_0

    .line 6548
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6550
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6551
    iput-object p1, p0, Lcom/sec/a/a/a/ah;->d:Ljava/lang/Object;

    .line 6553
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->a()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->a()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->b()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->b()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->b()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->b()Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/ag;
    .locals 2

    .prologue
    .line 6333
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->e()Lcom/sec/a/a/a/ag;

    move-result-object v0

    .line 6334
    invoke-virtual {v0}, Lcom/sec/a/a/a/ag;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6335
    invoke-static {v0}, Lcom/sec/a/a/a/ah;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 6337
    :cond_0
    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6588
    if-nez p1, :cond_0

    .line 6589
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6591
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6592
    iput-object p1, p0, Lcom/sec/a/a/a/ah;->e:Ljava/lang/Object;

    .line 6594
    return-object p0
.end method

.method public e()Lcom/sec/a/a/a/ag;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 6349
    new-instance v2, Lcom/sec/a/a/a/ag;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/ag;-><init>(Lcom/sec/a/a/a/ah;Lcom/sec/a/a/a/b;)V

    .line 6350
    iget v3, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6351
    const/4 v1, 0x0

    .line 6352
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 6355
    :goto_0
    iget-object v1, p0, Lcom/sec/a/a/a/ah;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ag;->a(Lcom/sec/a/a/a/ag;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6356
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 6357
    or-int/lit8 v0, v0, 0x2

    .line 6359
    :cond_0
    iget-object v1, p0, Lcom/sec/a/a/a/ah;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ag;->b(Lcom/sec/a/a/a/ag;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6360
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 6361
    or-int/lit8 v0, v0, 0x4

    .line 6363
    :cond_1
    iget-object v1, p0, Lcom/sec/a/a/a/ah;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ag;->c(Lcom/sec/a/a/a/ag;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6364
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 6365
    or-int/lit8 v0, v0, 0x8

    .line 6367
    :cond_2
    iget-object v1, p0, Lcom/sec/a/a/a/ah;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ag;->d(Lcom/sec/a/a/a/ag;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6368
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 6369
    or-int/lit8 v0, v0, 0x10

    .line 6371
    :cond_3
    iget-object v1, p0, Lcom/sec/a/a/a/ah;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ag;->e(Lcom/sec/a/a/a/ag;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6372
    invoke-static {v2, v0}, Lcom/sec/a/a/a/ag;->a(Lcom/sec/a/a/a/ag;I)I

    .line 6373
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)Lcom/sec/a/a/a/ah;
    .locals 1

    .prologue
    .line 6629
    if-nez p1, :cond_0

    .line 6630
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6632
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/a/a/a/ah;->a:I

    .line 6633
    iput-object p1, p0, Lcom/sec/a/a/a/ah;->f:Ljava/lang/Object;

    .line 6635
    return-object p0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->c()Lcom/sec/a/a/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0}, Lcom/sec/a/a/a/ah;->c()Lcom/sec/a/a/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 6398
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/ah;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6294
    check-cast p1, Lcom/sec/a/a/a/ag;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/ah;->a(Lcom/sec/a/a/a/ag;)Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6294
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/ah;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ah;

    move-result-object v0

    return-object v0
.end method
