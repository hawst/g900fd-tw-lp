.class public final Lcom/sec/a/a/a/i;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/k;


# static fields
.field private static final a:Lcom/sec/a/a/a/i;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 841
    new-instance v0, Lcom/sec/a/a/a/i;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/i;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/i;->a:Lcom/sec/a/a/a/i;

    .line 842
    sget-object v0, Lcom/sec/a/a/a/i;->a:Lcom/sec/a/a/a/i;

    invoke-direct {v0}, Lcom/sec/a/a/a/i;->l()V

    .line 843
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/j;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 420
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 500
    iput-byte v0, p0, Lcom/sec/a/a/a/i;->f:B

    .line 524
    iput v0, p0, Lcom/sec/a/a/a/i;->g:I

    .line 421
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/j;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 417
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/i;-><init>(Lcom/sec/a/a/a/j;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 423
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 500
    iput-byte v0, p0, Lcom/sec/a/a/a/i;->f:B

    .line 524
    iput v0, p0, Lcom/sec/a/a/a/i;->g:I

    .line 424
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/i;I)I
    .locals 0

    .prologue
    .line 417
    iput p1, p0, Lcom/sec/a/a/a/i;->c:I

    return p1
.end method

.method public static a()Lcom/sec/a/a/a/i;
    .locals 1

    .prologue
    .line 429
    sget-object v0, Lcom/sec/a/a/a/i;->a:Lcom/sec/a/a/a/i;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/i;
    .locals 1

    .prologue
    .line 562
    invoke-static {}, Lcom/sec/a/a/a/i;->newBuilder()Lcom/sec/a/a/a/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/j;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/j;

    invoke-static {v0}, Lcom/sec/a/a/a/j;->a(Lcom/sec/a/a/a/j;)Lcom/sec/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/i;)Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 615
    invoke-static {}, Lcom/sec/a/a/a/i;->newBuilder()Lcom/sec/a/a/a/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/j;->a(Lcom/sec/a/a/a/i;)Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a/i;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lcom/sec/a/a/a/i;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/i;I)I
    .locals 0

    .prologue
    .line 417
    iput p1, p0, Lcom/sec/a/a/a/i;->d:I

    return p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/i;I)I
    .locals 0

    .prologue
    .line 417
    iput p1, p0, Lcom/sec/a/a/a/i;->b:I

    return p1
.end method

.method private k()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Lcom/sec/a/a/a/i;->e:Ljava/lang/Object;

    .line 485
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 486
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 487
    iput-object v0, p0, Lcom/sec/a/a/a/i;->e:Ljava/lang/Object;

    .line 490
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private l()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 495
    iput v0, p0, Lcom/sec/a/a/a/i;->c:I

    .line 496
    iput v0, p0, Lcom/sec/a/a/a/i;->d:I

    .line 497
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/i;->e:Ljava/lang/Object;

    .line 498
    return-void
.end method

.method public static newBuilder()Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 607
    invoke-static {}, Lcom/sec/a/a/a/j;->f()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/i;
    .locals 1

    .prologue
    .line 433
    sget-object v0, Lcom/sec/a/a/a/i;->a:Lcom/sec/a/a/a/i;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 442
    iget v1, p0, Lcom/sec/a/a/a/i;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 446
    iget v0, p0, Lcom/sec/a/a/a/i;->c:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 454
    iget v0, p0, Lcom/sec/a/a/a/i;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 458
    iget v0, p0, Lcom/sec/a/a/a/i;->d:I

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 466
    iget v0, p0, Lcom/sec/a/a/a/i;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 417
    invoke-virtual {p0}, Lcom/sec/a/a/a/i;->b()Lcom/sec/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 527
    iget v0, p0, Lcom/sec/a/a/a/i;->g:I

    .line 528
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 542
    :goto_0
    return v0

    .line 531
    :cond_0
    const/4 v0, 0x0

    .line 532
    iget v1, p0, Lcom/sec/a/a/a/i;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 533
    iget v1, p0, Lcom/sec/a/a/a/i;->c:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 535
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/i;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 536
    iget v1, p0, Lcom/sec/a/a/a/i;->d:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 538
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/i;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 539
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/i;->k()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 541
    :cond_3
    iput v0, p0, Lcom/sec/a/a/a/i;->g:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/a/a/a/i;->e:Ljava/lang/Object;

    .line 471
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 472
    check-cast v0, Ljava/lang/String;

    .line 479
    :goto_0
    return-object v0

    .line 474
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 475
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 476
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 477
    iput-object v1, p0, Lcom/sec/a/a/a/i;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 479
    goto :goto_0
.end method

.method public i()Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 611
    invoke-static {}, Lcom/sec/a/a/a/i;->newBuilder()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 503
    iget-byte v1, p0, Lcom/sec/a/a/a/i;->f:B

    .line 504
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 505
    if-ne v1, v0, :cond_0

    .line 508
    :goto_0
    return v0

    .line 505
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 507
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/i;->f:B

    goto :goto_0
.end method

.method public j()Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 619
    invoke-static {p0}, Lcom/sec/a/a/a/i;->a(Lcom/sec/a/a/a/i;)Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 417
    invoke-virtual {p0}, Lcom/sec/a/a/a/i;->i()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 417
    invoke-virtual {p0}, Lcom/sec/a/a/a/i;->j()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 512
    invoke-virtual {p0}, Lcom/sec/a/a/a/i;->getSerializedSize()I

    .line 513
    iget v0, p0, Lcom/sec/a/a/a/i;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 514
    iget v0, p0, Lcom/sec/a/a/a/i;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 516
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/i;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 517
    iget v0, p0, Lcom/sec/a/a/a/i;->d:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 519
    :cond_1
    iget v0, p0, Lcom/sec/a/a/a/i;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 520
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/i;->k()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 522
    :cond_2
    return-void
.end method
