.class public Lcom/sec/a/a/a/ap;
.super Ljava/lang/Object;
.source "MsgFrontendCommon.java"


# direct methods
.method public static a(Ljava/lang/Object;)B
    .locals 1

    .prologue
    .line 47
    instance-of v0, p0, Lcom/sec/a/a/a/l;

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x0

    .line 74
    :goto_0
    return v0

    .line 49
    :cond_0
    instance-of v0, p0, Lcom/sec/a/a/a/i;

    if-eqz v0, :cond_1

    .line 50
    const/4 v0, 0x1

    goto :goto_0

    .line 51
    :cond_1
    instance-of v0, p0, Lcom/sec/a/a/a/am;

    if-eqz v0, :cond_2

    .line 52
    const/4 v0, 0x2

    goto :goto_0

    .line 53
    :cond_2
    instance-of v0, p0, Lcom/sec/a/a/a/aj;

    if-eqz v0, :cond_3

    .line 54
    const/4 v0, 0x3

    goto :goto_0

    .line 55
    :cond_3
    instance-of v0, p0, Lcom/sec/a/a/a/f;

    if-eqz v0, :cond_4

    .line 56
    const/4 v0, 0x4

    goto :goto_0

    .line 57
    :cond_4
    instance-of v0, p0, Lcom/sec/a/a/a/c;

    if-eqz v0, :cond_5

    .line 58
    const/4 v0, 0x5

    goto :goto_0

    .line 59
    :cond_5
    instance-of v0, p0, Lcom/sec/a/a/a/aa;

    if-eqz v0, :cond_6

    .line 60
    const/4 v0, 0x6

    goto :goto_0

    .line 61
    :cond_6
    instance-of v0, p0, Lcom/sec/a/a/a/x;

    if-eqz v0, :cond_7

    .line 62
    const/4 v0, 0x7

    goto :goto_0

    .line 63
    :cond_7
    instance-of v0, p0, Lcom/sec/a/a/a/r;

    if-eqz v0, :cond_8

    .line 64
    const/16 v0, 0x8

    goto :goto_0

    .line 65
    :cond_8
    instance-of v0, p0, Lcom/sec/a/a/a/u;

    if-eqz v0, :cond_9

    .line 66
    const/16 v0, 0x9

    goto :goto_0

    .line 67
    :cond_9
    instance-of v0, p0, Lcom/sec/a/a/a/o;

    if-eqz v0, :cond_a

    .line 68
    const/16 v0, 0xa

    goto :goto_0

    .line 69
    :cond_a
    instance-of v0, p0, Lcom/sec/a/a/a/ag;

    if-eqz v0, :cond_b

    .line 70
    const/16 v0, 0xb

    goto :goto_0

    .line 71
    :cond_b
    instance-of v0, p0, Lcom/sec/a/a/a/ad;

    if-eqz v0, :cond_c

    .line 72
    const/16 v0, 0xc

    goto :goto_0

    .line 74
    :cond_c
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(B[B)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 79
    .line 81
    packed-switch p0, :pswitch_data_0

    .line 128
    :goto_0
    return-object v0

    .line 83
    :pswitch_0
    :try_start_0
    invoke-static {p1}, Lcom/sec/a/a/a/l;->a([B)Lcom/sec/a/a/a/l;

    move-result-object v0

    goto :goto_0

    .line 86
    :pswitch_1
    invoke-static {p1}, Lcom/sec/a/a/a/i;->a([B)Lcom/sec/a/a/a/i;

    move-result-object v0

    goto :goto_0

    .line 89
    :pswitch_2
    invoke-static {p1}, Lcom/sec/a/a/a/am;->a([B)Lcom/sec/a/a/a/am;

    move-result-object v0

    goto :goto_0

    .line 92
    :pswitch_3
    invoke-static {p1}, Lcom/sec/a/a/a/aj;->a([B)Lcom/sec/a/a/a/aj;

    move-result-object v0

    goto :goto_0

    .line 95
    :pswitch_4
    invoke-static {p1}, Lcom/sec/a/a/a/f;->a([B)Lcom/sec/a/a/a/f;

    move-result-object v0

    goto :goto_0

    .line 98
    :pswitch_5
    invoke-static {p1}, Lcom/sec/a/a/a/c;->a([B)Lcom/sec/a/a/a/c;

    move-result-object v0

    goto :goto_0

    .line 101
    :pswitch_6
    invoke-static {p1}, Lcom/sec/a/a/a/aa;->a([B)Lcom/sec/a/a/a/aa;

    move-result-object v0

    goto :goto_0

    .line 104
    :pswitch_7
    invoke-static {p1}, Lcom/sec/a/a/a/x;->a([B)Lcom/sec/a/a/a/x;

    move-result-object v0

    goto :goto_0

    .line 107
    :pswitch_8
    invoke-static {p1}, Lcom/sec/a/a/a/r;->a([B)Lcom/sec/a/a/a/r;

    move-result-object v0

    goto :goto_0

    .line 110
    :pswitch_9
    invoke-static {p1}, Lcom/sec/a/a/a/u;->a([B)Lcom/sec/a/a/a/u;

    move-result-object v0

    goto :goto_0

    .line 113
    :pswitch_a
    invoke-static {p1}, Lcom/sec/a/a/a/o;->a([B)Lcom/sec/a/a/a/o;

    move-result-object v0

    goto :goto_0

    .line 116
    :pswitch_b
    invoke-static {p1}, Lcom/sec/a/a/a/ag;->a([B)Lcom/sec/a/a/a/ag;

    move-result-object v0

    goto :goto_0

    .line 119
    :pswitch_c
    invoke-static {p1}, Lcom/sec/a/a/a/ad;->a([B)Lcom/sec/a/a/a/ad;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 124
    :catch_0
    move-exception v1

    .line 125
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->printStackTrace()V

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static a(B)Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 133
    packed-switch p0, :pswitch_data_0

    .line 176
    :goto_0
    return-object v0

    .line 135
    :pswitch_0
    const-string v0, "InitRequest"

    goto :goto_0

    .line 138
    :pswitch_1
    const-string v0, "InitReply"

    goto :goto_0

    .line 141
    :pswitch_2
    const-string v0, "RegistrationRequest"

    goto :goto_0

    .line 144
    :pswitch_3
    const-string v0, "RegistrationReply"

    goto :goto_0

    .line 147
    :pswitch_4
    const-string v0, "DeregistrationRequest"

    goto :goto_0

    .line 150
    :pswitch_5
    const-string v0, "DeregistrationReply"

    goto :goto_0

    .line 153
    :pswitch_6
    const-string v0, "PingRequest"

    goto :goto_0

    .line 156
    :pswitch_7
    const-string v0, "PingReply"

    goto :goto_0

    .line 159
    :pswitch_8
    const-string v0, "NotiElement"

    goto :goto_0

    .line 162
    :pswitch_9
    const-string v0, "NotiGroup"

    goto :goto_0

    .line 165
    :pswitch_a
    const-string v0, "NotiAcks"

    goto :goto_0

    .line 168
    :pswitch_b
    const-string v0, "ProvisionRequest"

    goto :goto_0

    .line 171
    :pswitch_c
    const-string v0, "ProvisionReply"

    goto :goto_0

    .line 133
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    invoke-static {p0}, Lcom/sec/a/a/a/ap;->a(Ljava/lang/Object;)B

    move-result v0

    invoke-static {v0}, Lcom/sec/a/a/a/ap;->a(B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
