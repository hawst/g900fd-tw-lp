.class public final Lcom/sec/a/a/a/j;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/i;",
        "Lcom/sec/a/a/a/j;",
        ">;",
        "Lcom/sec/a/a/a/k;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 626
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 797
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/j;->d:Ljava/lang/Object;

    .line 627
    invoke-direct {p0}, Lcom/sec/a/a/a/j;->g()V

    .line 628
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/j;)Lcom/sec/a/a/a/i;
    .locals 1

    .prologue
    .line 622
    invoke-direct {p0}, Lcom/sec/a/a/a/j;->i()Lcom/sec/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 622
    invoke-static {}, Lcom/sec/a/a/a/j;->h()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 631
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 634
    new-instance v0, Lcom/sec/a/a/a/j;

    invoke-direct {v0}, Lcom/sec/a/a/a/j;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/i;
    .locals 2

    .prologue
    .line 665
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->e()Lcom/sec/a/a/a/i;

    move-result-object v0

    .line 666
    invoke-virtual {v0}, Lcom/sec/a/a/a/i;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 667
    invoke-static {v0}, Lcom/sec/a/a/a/j;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 669
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/j;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 638
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 639
    iput v1, p0, Lcom/sec/a/a/a/j;->b:I

    .line 640
    iget v0, p0, Lcom/sec/a/a/a/j;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/j;->a:I

    .line 641
    iput v1, p0, Lcom/sec/a/a/a/j;->c:I

    .line 642
    iget v0, p0, Lcom/sec/a/a/a/j;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/j;->a:I

    .line 643
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/j;->d:Ljava/lang/Object;

    .line 644
    iget v0, p0, Lcom/sec/a/a/a/j;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/a/a/a/j;->a:I

    .line 645
    return-object p0
.end method

.method public a(I)Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 758
    iget v0, p0, Lcom/sec/a/a/a/j;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/j;->a:I

    .line 759
    iput p1, p0, Lcom/sec/a/a/a/j;->b:I

    .line 761
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 713
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 714
    sparse-switch v0, :sswitch_data_0

    .line 719
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/j;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 721
    :sswitch_0
    return-object p0

    .line 726
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/j;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/j;->a:I

    .line 727
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/j;->b:I

    goto :goto_0

    .line 731
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/j;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/j;->a:I

    .line 732
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/j;->c:I

    goto :goto_0

    .line 736
    :sswitch_3
    iget v0, p0, Lcom/sec/a/a/a/j;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/j;->a:I

    .line 737
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/j;->d:Ljava/lang/Object;

    goto :goto_0

    .line 714
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/i;)Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 693
    invoke-static {}, Lcom/sec/a/a/a/i;->a()Lcom/sec/a/a/a/i;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 704
    :cond_0
    :goto_0
    return-object p0

    .line 695
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/i;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 696
    invoke-virtual {p1}, Lcom/sec/a/a/a/i;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/j;->a(I)Lcom/sec/a/a/a/j;

    .line 698
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/i;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 699
    invoke-virtual {p1}, Lcom/sec/a/a/a/i;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/j;->b(I)Lcom/sec/a/a/a/j;

    .line 701
    :cond_3
    invoke-virtual {p1}, Lcom/sec/a/a/a/i;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 702
    invoke-virtual {p1}, Lcom/sec/a/a/a/i;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/j;->a(Ljava/lang/String;)Lcom/sec/a/a/a/j;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 815
    if-nez p1, :cond_0

    .line 816
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 818
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/j;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/j;->a:I

    .line 819
    iput-object p1, p0, Lcom/sec/a/a/a/j;->d:Ljava/lang/Object;

    .line 821
    return-object p0
.end method

.method public b()Lcom/sec/a/a/a/j;
    .locals 2

    .prologue
    .line 649
    invoke-static {}, Lcom/sec/a/a/a/j;->h()Lcom/sec/a/a/a/j;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->e()Lcom/sec/a/a/a/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/j;->a(Lcom/sec/a/a/a/i;)Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/sec/a/a/a/j;
    .locals 1

    .prologue
    .line 783
    iget v0, p0, Lcom/sec/a/a/a/j;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/j;->a:I

    .line 784
    iput p1, p0, Lcom/sec/a/a/a/j;->c:I

    .line 786
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->d()Lcom/sec/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->e()Lcom/sec/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/i;
    .locals 1

    .prologue
    .line 653
    invoke-static {}, Lcom/sec/a/a/a/i;->a()Lcom/sec/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->a()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->a()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->b()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->b()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->b()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->b()Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/i;
    .locals 2

    .prologue
    .line 657
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->e()Lcom/sec/a/a/a/i;

    move-result-object v0

    .line 658
    invoke-virtual {v0}, Lcom/sec/a/a/a/i;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 659
    invoke-static {v0}, Lcom/sec/a/a/a/j;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 661
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/a/a/a/i;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 673
    new-instance v2, Lcom/sec/a/a/a/i;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/i;-><init>(Lcom/sec/a/a/a/j;Lcom/sec/a/a/a/b;)V

    .line 674
    iget v3, p0, Lcom/sec/a/a/a/j;->a:I

    .line 675
    const/4 v1, 0x0

    .line 676
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 679
    :goto_0
    iget v1, p0, Lcom/sec/a/a/a/j;->b:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/i;->a(Lcom/sec/a/a/a/i;I)I

    .line 680
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 681
    or-int/lit8 v0, v0, 0x2

    .line 683
    :cond_0
    iget v1, p0, Lcom/sec/a/a/a/j;->c:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/i;->b(Lcom/sec/a/a/a/i;I)I

    .line 684
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 685
    or-int/lit8 v0, v0, 0x4

    .line 687
    :cond_1
    iget-object v1, p0, Lcom/sec/a/a/a/j;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/i;->a(Lcom/sec/a/a/a/i;Ljava/lang/Object;)Ljava/lang/Object;

    .line 688
    invoke-static {v2, v0}, Lcom/sec/a/a/a/i;->c(Lcom/sec/a/a/a/i;I)I

    .line 689
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->c()Lcom/sec/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/a/a/a/j;->c()Lcom/sec/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 708
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/j;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 622
    check-cast p1, Lcom/sec/a/a/a/i;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/j;->a(Lcom/sec/a/a/a/i;)Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 622
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/j;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/j;

    move-result-object v0

    return-object v0
.end method
