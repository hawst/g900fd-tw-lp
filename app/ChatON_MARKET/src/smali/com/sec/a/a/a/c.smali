.class public final Lcom/sec/a/a/a/c;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/e;


# static fields
.field private static final a:Lcom/sec/a/a/a/c;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3215
    new-instance v0, Lcom/sec/a/a/a/c;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/c;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/c;->a:Lcom/sec/a/a/a/c;

    .line 3216
    sget-object v0, Lcom/sec/a/a/a/c;->a:Lcom/sec/a/a/a/c;

    invoke-direct {v0}, Lcom/sec/a/a/a/c;->o()V

    .line 3217
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/d;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2698
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 2812
    iput-byte v0, p0, Lcom/sec/a/a/a/c;->g:B

    .line 2839
    iput v0, p0, Lcom/sec/a/a/a/c;->h:I

    .line 2699
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/d;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 2695
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/c;-><init>(Lcom/sec/a/a/a/d;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2701
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2812
    iput-byte v0, p0, Lcom/sec/a/a/a/c;->g:B

    .line 2839
    iput v0, p0, Lcom/sec/a/a/a/c;->h:I

    .line 2702
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/c;I)I
    .locals 0

    .prologue
    .line 2695
    iput p1, p0, Lcom/sec/a/a/a/c;->c:I

    return p1
.end method

.method public static a()Lcom/sec/a/a/a/c;
    .locals 1

    .prologue
    .line 2707
    sget-object v0, Lcom/sec/a/a/a/c;->a:Lcom/sec/a/a/a/c;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/c;
    .locals 1

    .prologue
    .line 2880
    invoke-static {}, Lcom/sec/a/a/a/c;->newBuilder()Lcom/sec/a/a/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/d;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/d;

    invoke-static {v0}, Lcom/sec/a/a/a/d;->a(Lcom/sec/a/a/a/d;)Lcom/sec/a/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/c;)Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 2934
    invoke-static {}, Lcom/sec/a/a/a/c;->newBuilder()Lcom/sec/a/a/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/d;->a(Lcom/sec/a/a/a/c;)Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a/c;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 2695
    iput-object p1, p0, Lcom/sec/a/a/a/c;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/c;I)I
    .locals 0

    .prologue
    .line 2695
    iput p1, p0, Lcom/sec/a/a/a/c;->d:I

    return p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/c;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 2695
    iput-object p1, p0, Lcom/sec/a/a/a/c;->f:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/c;I)I
    .locals 0

    .prologue
    .line 2695
    iput p1, p0, Lcom/sec/a/a/a/c;->b:I

    return p1
.end method

.method private m()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2762
    iget-object v0, p0, Lcom/sec/a/a/a/c;->e:Ljava/lang/Object;

    .line 2763
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2764
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2765
    iput-object v0, p0, Lcom/sec/a/a/a/c;->e:Ljava/lang/Object;

    .line 2768
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private n()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2795
    iget-object v0, p0, Lcom/sec/a/a/a/c;->f:Ljava/lang/Object;

    .line 2796
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2797
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2798
    iput-object v0, p0, Lcom/sec/a/a/a/c;->f:Ljava/lang/Object;

    .line 2801
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static newBuilder()Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 2926
    invoke-static {}, Lcom/sec/a/a/a/d;->f()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2806
    iput v0, p0, Lcom/sec/a/a/a/c;->c:I

    .line 2807
    iput v0, p0, Lcom/sec/a/a/a/c;->d:I

    .line 2808
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/c;->e:Ljava/lang/Object;

    .line 2809
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/c;->f:Ljava/lang/Object;

    .line 2810
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/c;
    .locals 1

    .prologue
    .line 2711
    sget-object v0, Lcom/sec/a/a/a/c;->a:Lcom/sec/a/a/a/c;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2720
    iget v1, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 2724
    iget v0, p0, Lcom/sec/a/a/a/c;->c:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 2732
    iget v0, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 2736
    iget v0, p0, Lcom/sec/a/a/a/c;->d:I

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 2744
    iget v0, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2695
    invoke-virtual {p0}, Lcom/sec/a/a/a/c;->b()Lcom/sec/a/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2842
    iget v0, p0, Lcom/sec/a/a/a/c;->h:I

    .line 2843
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2860
    :goto_0
    return v0

    .line 2846
    :cond_0
    const/4 v0, 0x0

    .line 2847
    iget v1, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2848
    iget v1, p0, Lcom/sec/a/a/a/c;->c:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2850
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2851
    iget v1, p0, Lcom/sec/a/a/a/c;->d:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2853
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 2854
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/c;->m()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2856
    :cond_3
    iget v1, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2857
    invoke-direct {p0}, Lcom/sec/a/a/a/c;->n()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2859
    :cond_4
    iput v0, p0, Lcom/sec/a/a/a/c;->h:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2748
    iget-object v0, p0, Lcom/sec/a/a/a/c;->e:Ljava/lang/Object;

    .line 2749
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2750
    check-cast v0, Ljava/lang/String;

    .line 2757
    :goto_0
    return-object v0

    .line 2752
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2753
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2754
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2755
    iput-object v1, p0, Lcom/sec/a/a/a/c;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2757
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 2777
    iget v0, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2815
    iget-byte v1, p0, Lcom/sec/a/a/a/c;->g:B

    .line 2816
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 2817
    if-ne v1, v0, :cond_0

    .line 2820
    :goto_0
    return v0

    .line 2817
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2819
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/c;->g:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2781
    iget-object v0, p0, Lcom/sec/a/a/a/c;->f:Ljava/lang/Object;

    .line 2782
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2783
    check-cast v0, Ljava/lang/String;

    .line 2790
    :goto_0
    return-object v0

    .line 2785
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2786
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2787
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2788
    iput-object v1, p0, Lcom/sec/a/a/a/c;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2790
    goto :goto_0
.end method

.method public k()Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 2930
    invoke-static {}, Lcom/sec/a/a/a/c;->newBuilder()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 2938
    invoke-static {p0}, Lcom/sec/a/a/a/c;->a(Lcom/sec/a/a/a/c;)Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2695
    invoke-virtual {p0}, Lcom/sec/a/a/a/c;->k()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2695
    invoke-virtual {p0}, Lcom/sec/a/a/a/c;->l()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2867
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2824
    invoke-virtual {p0}, Lcom/sec/a/a/a/c;->getSerializedSize()I

    .line 2825
    iget v0, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2826
    iget v0, p0, Lcom/sec/a/a/a/c;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 2828
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2829
    iget v0, p0, Lcom/sec/a/a/a/c;->d:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 2831
    :cond_1
    iget v0, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2832
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/c;->m()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2834
    :cond_2
    iget v0, p0, Lcom/sec/a/a/a/c;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2835
    invoke-direct {p0}, Lcom/sec/a/a/a/c;->n()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2837
    :cond_3
    return-void
.end method
