.class public final Lcom/sec/a/a/a/p;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/o;",
        "Lcom/sec/a/a/a/p;",
        ">;",
        "Lcom/sec/a/a/a/q;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/protobuf/LazyStringList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 5710
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 5825
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/p;->b:Ljava/lang/Object;

    .line 5866
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    .line 5711
    invoke-direct {p0}, Lcom/sec/a/a/a/p;->g()V

    .line 5712
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/p;)Lcom/sec/a/a/a/o;
    .locals 1

    .prologue
    .line 5706
    invoke-direct {p0}, Lcom/sec/a/a/a/p;->i()Lcom/sec/a/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/p;
    .locals 1

    .prologue
    .line 5706
    invoke-static {}, Lcom/sec/a/a/a/p;->h()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 5715
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/p;
    .locals 1

    .prologue
    .line 5718
    new-instance v0, Lcom/sec/a/a/a/p;

    invoke-direct {v0}, Lcom/sec/a/a/a/p;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/o;
    .locals 2

    .prologue
    .line 5747
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->e()Lcom/sec/a/a/a/o;

    move-result-object v0

    .line 5748
    invoke-virtual {v0}, Lcom/sec/a/a/a/o;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5749
    invoke-static {v0}, Lcom/sec/a/a/a/p;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 5751
    :cond_0
    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 5869
    iget v0, p0, Lcom/sec/a/a/a/p;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 5870
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    .line 5871
    iget v0, p0, Lcom/sec/a/a/a/p;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/p;->a:I

    .line 5873
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/p;
    .locals 1

    .prologue
    .line 5722
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 5723
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/p;->b:Ljava/lang/Object;

    .line 5724
    iget v0, p0, Lcom/sec/a/a/a/p;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/p;->a:I

    .line 5725
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    .line 5726
    iget v0, p0, Lcom/sec/a/a/a/p;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/p;->a:I

    .line 5727
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/p;
    .locals 2

    .prologue
    .line 5796
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 5797
    sparse-switch v0, :sswitch_data_0

    .line 5802
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/p;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5804
    :sswitch_0
    return-object p0

    .line 5809
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/p;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/p;->a:I

    .line 5810
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/p;->b:Ljava/lang/Object;

    goto :goto_0

    .line 5814
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/a/a/a/p;->j()V

    .line 5815
    iget-object v0, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 5797
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/o;)Lcom/sec/a/a/a/p;
    .locals 2

    .prologue
    .line 5772
    invoke-static {}, Lcom/sec/a/a/a/o;->a()Lcom/sec/a/a/a/o;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 5787
    :cond_0
    :goto_0
    return-object p0

    .line 5774
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/o;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5775
    invoke-virtual {p1}, Lcom/sec/a/a/a/o;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/p;->a(Ljava/lang/String;)Lcom/sec/a/a/a/p;

    .line 5777
    :cond_2
    invoke-static {p1}, Lcom/sec/a/a/a/o;->b(Lcom/sec/a/a/a/o;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5778
    iget-object v0, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5779
    invoke-static {p1}, Lcom/sec/a/a/a/o;->b(Lcom/sec/a/a/a/o;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    .line 5780
    iget v0, p0, Lcom/sec/a/a/a/p;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/p;->a:I

    goto :goto_0

    .line 5782
    :cond_3
    invoke-direct {p0}, Lcom/sec/a/a/a/p;->j()V

    .line 5783
    iget-object v0, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1}, Lcom/sec/a/a/a/o;->b(Lcom/sec/a/a/a/o;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/a/a/a/p;
    .locals 1

    .prologue
    .line 5843
    if-nez p1, :cond_0

    .line 5844
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5846
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/p;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/p;->a:I

    .line 5847
    iput-object p1, p0, Lcom/sec/a/a/a/p;->b:Ljava/lang/Object;

    .line 5849
    return-object p0
.end method

.method public b()Lcom/sec/a/a/a/p;
    .locals 2

    .prologue
    .line 5731
    invoke-static {}, Lcom/sec/a/a/a/p;->h()Lcom/sec/a/a/a/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->e()Lcom/sec/a/a/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/p;->a(Lcom/sec/a/a/a/o;)Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/a/a/a/p;
    .locals 1

    .prologue
    .line 5898
    if-nez p1, :cond_0

    .line 5899
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5901
    :cond_0
    invoke-direct {p0}, Lcom/sec/a/a/a/p;->j()V

    .line 5902
    iget-object v0, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 5904
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->d()Lcom/sec/a/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->e()Lcom/sec/a/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/o;
    .locals 1

    .prologue
    .line 5735
    invoke-static {}, Lcom/sec/a/a/a/o;->a()Lcom/sec/a/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->a()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->a()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->b()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->b()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->b()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->b()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/o;
    .locals 2

    .prologue
    .line 5739
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->e()Lcom/sec/a/a/a/o;

    move-result-object v0

    .line 5740
    invoke-virtual {v0}, Lcom/sec/a/a/a/o;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5741
    invoke-static {v0}, Lcom/sec/a/a/a/p;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 5743
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/a/a/a/o;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 5755
    new-instance v2, Lcom/sec/a/a/a/o;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/o;-><init>(Lcom/sec/a/a/a/p;Lcom/sec/a/a/a/b;)V

    .line 5756
    iget v3, p0, Lcom/sec/a/a/a/p;->a:I

    .line 5757
    const/4 v1, 0x0

    .line 5758
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    .line 5761
    :goto_0
    iget-object v1, p0, Lcom/sec/a/a/a/p;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/o;->a(Lcom/sec/a/a/a/o;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5762
    iget v1, p0, Lcom/sec/a/a/a/p;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 5763
    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v3, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v3}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    .line 5764
    iget v1, p0, Lcom/sec/a/a/a/p;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/sec/a/a/a/p;->a:I

    .line 5766
    :cond_0
    iget-object v1, p0, Lcom/sec/a/a/a/p;->c:Lcom/google/protobuf/LazyStringList;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/o;->a(Lcom/sec/a/a/a/o;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 5767
    invoke-static {v2, v0}, Lcom/sec/a/a/a/o;->a(Lcom/sec/a/a/a/o;I)I

    .line 5768
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->c()Lcom/sec/a/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0}, Lcom/sec/a/a/a/p;->c()Lcom/sec/a/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 5791
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/p;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5706
    check-cast p1, Lcom/sec/a/a/a/o;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/p;->a(Lcom/sec/a/a/a/o;)Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5706
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/p;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method
