.class public final Lcom/sec/a/a/a/g;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/f;",
        "Lcom/sec/a/a/a/g;",
        ">;",
        "Lcom/sec/a/a/a/h;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2379
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 2539
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/g;->c:Ljava/lang/Object;

    .line 2580
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/g;->d:Ljava/lang/Object;

    .line 2621
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/g;->e:Ljava/lang/Object;

    .line 2380
    invoke-direct {p0}, Lcom/sec/a/a/a/g;->g()V

    .line 2381
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/g;)Lcom/sec/a/a/a/f;
    .locals 1

    .prologue
    .line 2375
    invoke-direct {p0}, Lcom/sec/a/a/a/g;->i()Lcom/sec/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2375
    invoke-static {}, Lcom/sec/a/a/a/g;->h()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 2384
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2387
    new-instance v0, Lcom/sec/a/a/a/g;

    invoke-direct {v0}, Lcom/sec/a/a/a/g;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/f;
    .locals 2

    .prologue
    .line 2420
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->e()Lcom/sec/a/a/a/f;

    move-result-object v0

    .line 2421
    invoke-virtual {v0}, Lcom/sec/a/a/a/f;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2422
    invoke-static {v0}, Lcom/sec/a/a/a/g;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 2424
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2391
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 2392
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/a/a/a/g;->b:I

    .line 2393
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2394
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/g;->c:Ljava/lang/Object;

    .line 2395
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2396
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/g;->d:Ljava/lang/Object;

    .line 2397
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2398
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/g;->e:Ljava/lang/Object;

    .line 2399
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2400
    return-object p0
.end method

.method public a(I)Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2525
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2526
    iput p1, p0, Lcom/sec/a/a/a/g;->b:I

    .line 2528
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2475
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 2476
    sparse-switch v0, :sswitch_data_0

    .line 2481
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/g;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2483
    :sswitch_0
    return-object p0

    .line 2488
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2489
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/g;->b:I

    goto :goto_0

    .line 2493
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2494
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/g;->c:Ljava/lang/Object;

    goto :goto_0

    .line 2498
    :sswitch_3
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2499
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/g;->d:Ljava/lang/Object;

    goto :goto_0

    .line 2503
    :sswitch_4
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2504
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/g;->e:Ljava/lang/Object;

    goto :goto_0

    .line 2476
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/f;)Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2452
    invoke-static {}, Lcom/sec/a/a/a/f;->a()Lcom/sec/a/a/a/f;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2466
    :cond_0
    :goto_0
    return-object p0

    .line 2454
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/f;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2455
    invoke-virtual {p1}, Lcom/sec/a/a/a/f;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/g;->a(I)Lcom/sec/a/a/a/g;

    .line 2457
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/f;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2458
    invoke-virtual {p1}, Lcom/sec/a/a/a/f;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/g;->a(Ljava/lang/String;)Lcom/sec/a/a/a/g;

    .line 2460
    :cond_3
    invoke-virtual {p1}, Lcom/sec/a/a/a/f;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2461
    invoke-virtual {p1}, Lcom/sec/a/a/a/f;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/g;->b(Ljava/lang/String;)Lcom/sec/a/a/a/g;

    .line 2463
    :cond_4
    invoke-virtual {p1}, Lcom/sec/a/a/a/f;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2464
    invoke-virtual {p1}, Lcom/sec/a/a/a/f;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/g;->c(Ljava/lang/String;)Lcom/sec/a/a/a/g;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2557
    if-nez p1, :cond_0

    .line 2558
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2560
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2561
    iput-object p1, p0, Lcom/sec/a/a/a/g;->c:Ljava/lang/Object;

    .line 2563
    return-object p0
.end method

.method public b()Lcom/sec/a/a/a/g;
    .locals 2

    .prologue
    .line 2404
    invoke-static {}, Lcom/sec/a/a/a/g;->h()Lcom/sec/a/a/a/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->e()Lcom/sec/a/a/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/g;->a(Lcom/sec/a/a/a/f;)Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2598
    if-nez p1, :cond_0

    .line 2599
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2601
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2602
    iput-object p1, p0, Lcom/sec/a/a/a/g;->d:Ljava/lang/Object;

    .line 2604
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->d()Lcom/sec/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->e()Lcom/sec/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/f;
    .locals 1

    .prologue
    .line 2408
    invoke-static {}, Lcom/sec/a/a/a/f;->a()Lcom/sec/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2639
    if-nez p1, :cond_0

    .line 2640
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2642
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/g;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2643
    iput-object p1, p0, Lcom/sec/a/a/a/g;->e:Ljava/lang/Object;

    .line 2645
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->a()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->a()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->b()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->b()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->b()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->b()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/f;
    .locals 2

    .prologue
    .line 2412
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->e()Lcom/sec/a/a/a/f;

    move-result-object v0

    .line 2413
    invoke-virtual {v0}, Lcom/sec/a/a/a/f;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2414
    invoke-static {v0}, Lcom/sec/a/a/a/g;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2416
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/a/a/a/f;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2428
    new-instance v2, Lcom/sec/a/a/a/f;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/f;-><init>(Lcom/sec/a/a/a/g;Lcom/sec/a/a/a/b;)V

    .line 2429
    iget v3, p0, Lcom/sec/a/a/a/g;->a:I

    .line 2430
    const/4 v1, 0x0

    .line 2431
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 2434
    :goto_0
    iget v1, p0, Lcom/sec/a/a/a/g;->b:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/f;->a(Lcom/sec/a/a/a/f;I)I

    .line 2435
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2436
    or-int/lit8 v0, v0, 0x2

    .line 2438
    :cond_0
    iget-object v1, p0, Lcom/sec/a/a/a/g;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/f;->a(Lcom/sec/a/a/a/f;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2439
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2440
    or-int/lit8 v0, v0, 0x4

    .line 2442
    :cond_1
    iget-object v1, p0, Lcom/sec/a/a/a/g;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/f;->b(Lcom/sec/a/a/a/f;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2443
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 2444
    or-int/lit8 v0, v0, 0x8

    .line 2446
    :cond_2
    iget-object v1, p0, Lcom/sec/a/a/a/g;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/f;->c(Lcom/sec/a/a/a/f;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2447
    invoke-static {v2, v0}, Lcom/sec/a/a/a/f;->b(Lcom/sec/a/a/a/f;I)I

    .line 2448
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->c()Lcom/sec/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0}, Lcom/sec/a/a/a/g;->c()Lcom/sec/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 2470
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/g;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2375
    check-cast p1, Lcom/sec/a/a/a/f;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/g;->a(Lcom/sec/a/a/a/f;)Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2375
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/g;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method
