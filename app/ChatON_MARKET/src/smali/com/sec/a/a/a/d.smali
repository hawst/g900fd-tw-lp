.class public final Lcom/sec/a/a/a/d;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/c;",
        "Lcom/sec/a/a/a/d;",
        ">;",
        "Lcom/sec/a/a/a/e;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2945
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 3130
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/d;->d:Ljava/lang/Object;

    .line 3171
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/d;->e:Ljava/lang/Object;

    .line 2946
    invoke-direct {p0}, Lcom/sec/a/a/a/d;->g()V

    .line 2947
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/d;)Lcom/sec/a/a/a/c;
    .locals 1

    .prologue
    .line 2941
    invoke-direct {p0}, Lcom/sec/a/a/a/d;->i()Lcom/sec/a/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 2941
    invoke-static {}, Lcom/sec/a/a/a/d;->h()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 2950
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 2953
    new-instance v0, Lcom/sec/a/a/a/d;

    invoke-direct {v0}, Lcom/sec/a/a/a/d;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/c;
    .locals 2

    .prologue
    .line 2986
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->e()Lcom/sec/a/a/a/c;

    move-result-object v0

    .line 2987
    invoke-virtual {v0}, Lcom/sec/a/a/a/c;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2988
    invoke-static {v0}, Lcom/sec/a/a/a/d;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 2990
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/d;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2957
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 2958
    iput v1, p0, Lcom/sec/a/a/a/d;->b:I

    .line 2959
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 2960
    iput v1, p0, Lcom/sec/a/a/a/d;->c:I

    .line 2961
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 2962
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/d;->d:Ljava/lang/Object;

    .line 2963
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 2964
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/d;->e:Ljava/lang/Object;

    .line 2965
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 2966
    return-object p0
.end method

.method public a(I)Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 3091
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 3092
    iput p1, p0, Lcom/sec/a/a/a/d;->b:I

    .line 3094
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 3041
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 3042
    sparse-switch v0, :sswitch_data_0

    .line 3047
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/d;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3049
    :sswitch_0
    return-object p0

    .line 3054
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 3055
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/d;->b:I

    goto :goto_0

    .line 3059
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 3060
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/d;->c:I

    goto :goto_0

    .line 3064
    :sswitch_3
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 3065
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/d;->d:Ljava/lang/Object;

    goto :goto_0

    .line 3069
    :sswitch_4
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 3070
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/d;->e:Ljava/lang/Object;

    goto :goto_0

    .line 3042
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/c;)Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 3018
    invoke-static {}, Lcom/sec/a/a/a/c;->a()Lcom/sec/a/a/a/c;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 3032
    :cond_0
    :goto_0
    return-object p0

    .line 3020
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/c;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3021
    invoke-virtual {p1}, Lcom/sec/a/a/a/c;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/d;->a(I)Lcom/sec/a/a/a/d;

    .line 3023
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/c;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3024
    invoke-virtual {p1}, Lcom/sec/a/a/a/c;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/d;->b(I)Lcom/sec/a/a/a/d;

    .line 3026
    :cond_3
    invoke-virtual {p1}, Lcom/sec/a/a/a/c;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3027
    invoke-virtual {p1}, Lcom/sec/a/a/a/c;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/d;->a(Ljava/lang/String;)Lcom/sec/a/a/a/d;

    .line 3029
    :cond_4
    invoke-virtual {p1}, Lcom/sec/a/a/a/c;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3030
    invoke-virtual {p1}, Lcom/sec/a/a/a/c;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/d;->b(Ljava/lang/String;)Lcom/sec/a/a/a/d;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 3148
    if-nez p1, :cond_0

    .line 3149
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3151
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 3152
    iput-object p1, p0, Lcom/sec/a/a/a/d;->d:Ljava/lang/Object;

    .line 3154
    return-object p0
.end method

.method public b()Lcom/sec/a/a/a/d;
    .locals 2

    .prologue
    .line 2970
    invoke-static {}, Lcom/sec/a/a/a/d;->h()Lcom/sec/a/a/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->e()Lcom/sec/a/a/a/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/d;->a(Lcom/sec/a/a/a/c;)Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 3116
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 3117
    iput p1, p0, Lcom/sec/a/a/a/d;->c:I

    .line 3119
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/a/a/a/d;
    .locals 1

    .prologue
    .line 3189
    if-nez p1, :cond_0

    .line 3190
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3192
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/d;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/d;->a:I

    .line 3193
    iput-object p1, p0, Lcom/sec/a/a/a/d;->e:Ljava/lang/Object;

    .line 3195
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->d()Lcom/sec/a/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->e()Lcom/sec/a/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/c;
    .locals 1

    .prologue
    .line 2974
    invoke-static {}, Lcom/sec/a/a/a/c;->a()Lcom/sec/a/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->a()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->a()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->b()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->b()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->b()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->b()Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/c;
    .locals 2

    .prologue
    .line 2978
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->e()Lcom/sec/a/a/a/c;

    move-result-object v0

    .line 2979
    invoke-virtual {v0}, Lcom/sec/a/a/a/c;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2980
    invoke-static {v0}, Lcom/sec/a/a/a/d;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2982
    :cond_0
    return-object v0
.end method

.method public e()Lcom/sec/a/a/a/c;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2994
    new-instance v2, Lcom/sec/a/a/a/c;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/c;-><init>(Lcom/sec/a/a/a/d;Lcom/sec/a/a/a/b;)V

    .line 2995
    iget v3, p0, Lcom/sec/a/a/a/d;->a:I

    .line 2996
    const/4 v1, 0x0

    .line 2997
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 3000
    :goto_0
    iget v1, p0, Lcom/sec/a/a/a/d;->b:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/c;->a(Lcom/sec/a/a/a/c;I)I

    .line 3001
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 3002
    or-int/lit8 v0, v0, 0x2

    .line 3004
    :cond_0
    iget v1, p0, Lcom/sec/a/a/a/d;->c:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/c;->b(Lcom/sec/a/a/a/c;I)I

    .line 3005
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 3006
    or-int/lit8 v0, v0, 0x4

    .line 3008
    :cond_1
    iget-object v1, p0, Lcom/sec/a/a/a/d;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/c;->a(Lcom/sec/a/a/a/c;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3009
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 3010
    or-int/lit8 v0, v0, 0x8

    .line 3012
    :cond_2
    iget-object v1, p0, Lcom/sec/a/a/a/d;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/c;->b(Lcom/sec/a/a/a/c;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3013
    invoke-static {v2, v0}, Lcom/sec/a/a/a/c;->c(Lcom/sec/a/a/a/c;I)I

    .line 3014
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->c()Lcom/sec/a/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/sec/a/a/a/d;->c()Lcom/sec/a/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 3036
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/d;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2941
    check-cast p1, Lcom/sec/a/a/a/c;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/d;->a(Lcom/sec/a/a/a/c;)Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2941
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/d;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/d;

    move-result-object v0

    return-object v0
.end method
