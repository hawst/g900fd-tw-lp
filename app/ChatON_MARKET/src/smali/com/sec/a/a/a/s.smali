.class public final Lcom/sec/a/a/a/s;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/r;",
        "Lcom/sec/a/a/a/s;",
        ">;",
        "Lcom/sec/a/a/a/t;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:I

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:J

.field private j:I

.field private k:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 4546
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 4765
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->b:Ljava/lang/Object;

    .line 4806
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->c:Ljava/lang/Object;

    .line 4897
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->f:Ljava/lang/Object;

    .line 4938
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->g:Ljava/lang/Object;

    .line 4979
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->h:Ljava/lang/Object;

    .line 5070
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->k:Ljava/lang/Object;

    .line 4547
    invoke-direct {p0}, Lcom/sec/a/a/a/s;->g()V

    .line 4548
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/s;)Lcom/sec/a/a/a/r;
    .locals 1

    .prologue
    .line 4542
    invoke-direct {p0}, Lcom/sec/a/a/a/s;->i()Lcom/sec/a/a/a/r;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 4542
    invoke-static {}, Lcom/sec/a/a/a/s;->h()Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 4551
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 4554
    new-instance v0, Lcom/sec/a/a/a/s;

    invoke-direct {v0}, Lcom/sec/a/a/a/s;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/r;
    .locals 2

    .prologue
    .line 4599
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->e()Lcom/sec/a/a/a/r;

    move-result-object v0

    .line 4600
    invoke-virtual {v0}, Lcom/sec/a/a/a/r;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4601
    invoke-static {v0}, Lcom/sec/a/a/a/s;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 4603
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/s;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4558
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 4559
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->b:Ljava/lang/Object;

    .line 4560
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4561
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->c:Ljava/lang/Object;

    .line 4562
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4563
    iput v2, p0, Lcom/sec/a/a/a/s;->d:I

    .line 4564
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4565
    iput v2, p0, Lcom/sec/a/a/a/s;->e:I

    .line 4566
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4567
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->f:Ljava/lang/Object;

    .line 4568
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4569
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->g:Ljava/lang/Object;

    .line 4570
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4571
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->h:Ljava/lang/Object;

    .line 4572
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4573
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/a/a/a/s;->i:J

    .line 4574
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4575
    iput v2, p0, Lcom/sec/a/a/a/s;->j:I

    .line 4576
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4577
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/s;->k:Ljava/lang/Object;

    .line 4578
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4579
    return-object p0
.end method

.method public a(I)Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 4858
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4859
    iput p1, p0, Lcom/sec/a/a/a/s;->d:I

    .line 4861
    return-object p0
.end method

.method public a(J)Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 5031
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 5032
    iput-wide p1, p0, Lcom/sec/a/a/a/s;->i:J

    .line 5034
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/s;
    .locals 2

    .prologue
    .line 4696
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 4697
    sparse-switch v0, :sswitch_data_0

    .line 4702
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/s;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4704
    :sswitch_0
    return-object p0

    .line 4709
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4710
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/s;->b:Ljava/lang/Object;

    goto :goto_0

    .line 4714
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4715
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/s;->c:Ljava/lang/Object;

    goto :goto_0

    .line 4719
    :sswitch_3
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4720
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/s;->d:I

    goto :goto_0

    .line 4724
    :sswitch_4
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4725
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/s;->e:I

    goto :goto_0

    .line 4729
    :sswitch_5
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4730
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/s;->f:Ljava/lang/Object;

    goto :goto_0

    .line 4734
    :sswitch_6
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4735
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/s;->g:Ljava/lang/Object;

    goto :goto_0

    .line 4739
    :sswitch_7
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4740
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/s;->h:Ljava/lang/Object;

    goto :goto_0

    .line 4744
    :sswitch_8
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4745
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/a/a/a/s;->i:J

    goto :goto_0

    .line 4749
    :sswitch_9
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4750
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/s;->j:I

    goto/16 :goto_0

    .line 4754
    :sswitch_a
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4755
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/s;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4697
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x58 -> :sswitch_8
        0x60 -> :sswitch_9
        0x6a -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/r;)Lcom/sec/a/a/a/s;
    .locals 2

    .prologue
    .line 4655
    invoke-static {}, Lcom/sec/a/a/a/r;->a()Lcom/sec/a/a/a/r;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 4687
    :cond_0
    :goto_0
    return-object p0

    .line 4657
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4658
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/s;->a(Ljava/lang/String;)Lcom/sec/a/a/a/s;

    .line 4660
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4661
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/s;->b(Ljava/lang/String;)Lcom/sec/a/a/a/s;

    .line 4663
    :cond_3
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4664
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/s;->a(I)Lcom/sec/a/a/a/s;

    .line 4666
    :cond_4
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4667
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->j()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/s;->b(I)Lcom/sec/a/a/a/s;

    .line 4669
    :cond_5
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4670
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/s;->c(Ljava/lang/String;)Lcom/sec/a/a/a/s;

    .line 4672
    :cond_6
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4673
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/s;->d(Ljava/lang/String;)Lcom/sec/a/a/a/s;

    .line 4675
    :cond_7
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->o()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4676
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/s;->e(Ljava/lang/String;)Lcom/sec/a/a/a/s;

    .line 4678
    :cond_8
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->q()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4679
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->r()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/a/a/a/s;->a(J)Lcom/sec/a/a/a/s;

    .line 4681
    :cond_9
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->s()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 4682
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->t()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/s;->c(I)Lcom/sec/a/a/a/s;

    .line 4684
    :cond_a
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4685
    invoke-virtual {p1}, Lcom/sec/a/a/a/r;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/s;->f(Ljava/lang/String;)Lcom/sec/a/a/a/s;

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 4783
    if-nez p1, :cond_0

    .line 4784
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4786
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4787
    iput-object p1, p0, Lcom/sec/a/a/a/s;->b:Ljava/lang/Object;

    .line 4789
    return-object p0
.end method

.method public b()Lcom/sec/a/a/a/s;
    .locals 2

    .prologue
    .line 4583
    invoke-static {}, Lcom/sec/a/a/a/s;->h()Lcom/sec/a/a/a/s;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->e()Lcom/sec/a/a/a/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/s;->a(Lcom/sec/a/a/a/r;)Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 4883
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4884
    iput p1, p0, Lcom/sec/a/a/a/s;->e:I

    .line 4886
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 4824
    if-nez p1, :cond_0

    .line 4825
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4827
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4828
    iput-object p1, p0, Lcom/sec/a/a/a/s;->c:Ljava/lang/Object;

    .line 4830
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->d()Lcom/sec/a/a/a/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->e()Lcom/sec/a/a/a/r;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/r;
    .locals 1

    .prologue
    .line 4587
    invoke-static {}, Lcom/sec/a/a/a/r;->a()Lcom/sec/a/a/a/r;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 5056
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 5057
    iput p1, p0, Lcom/sec/a/a/a/s;->j:I

    .line 5059
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 4915
    if-nez p1, :cond_0

    .line 4916
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4918
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4919
    iput-object p1, p0, Lcom/sec/a/a/a/s;->f:Ljava/lang/Object;

    .line 4921
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->a()Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->a()Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->b()Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->b()Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->b()Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->b()Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/r;
    .locals 2

    .prologue
    .line 4591
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->e()Lcom/sec/a/a/a/r;

    move-result-object v0

    .line 4592
    invoke-virtual {v0}, Lcom/sec/a/a/a/r;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4593
    invoke-static {v0}, Lcom/sec/a/a/a/s;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 4595
    :cond_0
    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 4956
    if-nez p1, :cond_0

    .line 4957
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4959
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4960
    iput-object p1, p0, Lcom/sec/a/a/a/s;->g:Ljava/lang/Object;

    .line 4962
    return-object p0
.end method

.method public e()Lcom/sec/a/a/a/r;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 4607
    new-instance v2, Lcom/sec/a/a/a/r;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/r;-><init>(Lcom/sec/a/a/a/s;Lcom/sec/a/a/a/b;)V

    .line 4608
    iget v3, p0, Lcom/sec/a/a/a/s;->a:I

    .line 4609
    const/4 v1, 0x0

    .line 4610
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    .line 4613
    :goto_0
    iget-object v1, p0, Lcom/sec/a/a/a/s;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/r;->a(Lcom/sec/a/a/a/r;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4614
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 4615
    or-int/lit8 v0, v0, 0x2

    .line 4617
    :cond_0
    iget-object v1, p0, Lcom/sec/a/a/a/s;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/r;->b(Lcom/sec/a/a/a/r;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4618
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 4619
    or-int/lit8 v0, v0, 0x4

    .line 4621
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/s;->d:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/r;->a(Lcom/sec/a/a/a/r;I)I

    .line 4622
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 4623
    or-int/lit8 v0, v0, 0x8

    .line 4625
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/s;->e:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/r;->b(Lcom/sec/a/a/a/r;I)I

    .line 4626
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 4627
    or-int/lit8 v0, v0, 0x10

    .line 4629
    :cond_3
    iget-object v1, p0, Lcom/sec/a/a/a/s;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/r;->c(Lcom/sec/a/a/a/r;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4630
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 4631
    or-int/lit8 v0, v0, 0x20

    .line 4633
    :cond_4
    iget-object v1, p0, Lcom/sec/a/a/a/s;->g:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/r;->d(Lcom/sec/a/a/a/r;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4634
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 4635
    or-int/lit8 v0, v0, 0x40

    .line 4637
    :cond_5
    iget-object v1, p0, Lcom/sec/a/a/a/s;->h:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/r;->e(Lcom/sec/a/a/a/r;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4638
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 4639
    or-int/lit16 v0, v0, 0x80

    .line 4641
    :cond_6
    iget-wide v4, p0, Lcom/sec/a/a/a/s;->i:J

    invoke-static {v2, v4, v5}, Lcom/sec/a/a/a/r;->a(Lcom/sec/a/a/a/r;J)J

    .line 4642
    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 4643
    or-int/lit16 v0, v0, 0x100

    .line 4645
    :cond_7
    iget v1, p0, Lcom/sec/a/a/a/s;->j:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/r;->c(Lcom/sec/a/a/a/r;I)I

    .line 4646
    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    .line 4647
    or-int/lit16 v0, v0, 0x200

    .line 4649
    :cond_8
    iget-object v1, p0, Lcom/sec/a/a/a/s;->k:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/r;->f(Lcom/sec/a/a/a/r;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4650
    invoke-static {v2, v0}, Lcom/sec/a/a/a/r;->d(Lcom/sec/a/a/a/r;I)I

    .line 4651
    return-object v2

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 4997
    if-nez p1, :cond_0

    .line 4998
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5000
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 5001
    iput-object p1, p0, Lcom/sec/a/a/a/s;->h:Ljava/lang/Object;

    .line 5003
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/sec/a/a/a/s;
    .locals 1

    .prologue
    .line 5088
    if-nez p1, :cond_0

    .line 5089
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5091
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/s;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sec/a/a/a/s;->a:I

    .line 5092
    iput-object p1, p0, Lcom/sec/a/a/a/s;->k:Ljava/lang/Object;

    .line 5094
    return-object p0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->c()Lcom/sec/a/a/a/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0}, Lcom/sec/a/a/a/s;->c()Lcom/sec/a/a/a/r;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 4691
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/s;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4542
    check-cast p1, Lcom/sec/a/a/a/r;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/s;->a(Lcom/sec/a/a/a/r;)Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4542
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/s;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/s;

    move-result-object v0

    return-object v0
.end method
