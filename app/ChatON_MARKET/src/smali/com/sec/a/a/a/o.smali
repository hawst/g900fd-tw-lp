.class public final Lcom/sec/a/a/a/o;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/q;


# static fields
.field private static final a:Lcom/sec/a/a/a/o;


# instance fields
.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/protobuf/LazyStringList;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 5931
    new-instance v0, Lcom/sec/a/a/a/o;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/o;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/o;->a:Lcom/sec/a/a/a/o;

    .line 5932
    sget-object v0, Lcom/sec/a/a/a/o;->a:Lcom/sec/a/a/a/o;

    invoke-direct {v0}, Lcom/sec/a/a/a/o;->i()V

    .line 5933
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/p;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5514
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 5585
    iput-byte v0, p0, Lcom/sec/a/a/a/o;->e:B

    .line 5606
    iput v0, p0, Lcom/sec/a/a/a/o;->f:I

    .line 5515
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/p;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 5511
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/o;-><init>(Lcom/sec/a/a/a/p;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5517
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5585
    iput-byte v0, p0, Lcom/sec/a/a/a/o;->e:B

    .line 5606
    iput v0, p0, Lcom/sec/a/a/a/o;->f:I

    .line 5518
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/o;I)I
    .locals 0

    .prologue
    .line 5511
    iput p1, p0, Lcom/sec/a/a/a/o;->b:I

    return p1
.end method

.method static synthetic a(Lcom/sec/a/a/a/o;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    .prologue
    .line 5511
    iput-object p1, p0, Lcom/sec/a/a/a/o;->d:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method public static a()Lcom/sec/a/a/a/o;
    .locals 1

    .prologue
    .line 5523
    sget-object v0, Lcom/sec/a/a/a/o;->a:Lcom/sec/a/a/a/o;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/o;
    .locals 1

    .prologue
    .line 5646
    invoke-static {}, Lcom/sec/a/a/a/o;->newBuilder()Lcom/sec/a/a/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/p;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/p;

    invoke-static {v0}, Lcom/sec/a/a/a/p;->a(Lcom/sec/a/a/a/p;)Lcom/sec/a/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/o;)Lcom/sec/a/a/a/p;
    .locals 1

    .prologue
    .line 5699
    invoke-static {}, Lcom/sec/a/a/a/o;->newBuilder()Lcom/sec/a/a/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/p;->a(Lcom/sec/a/a/a/o;)Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a/o;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 5511
    iput-object p1, p0, Lcom/sec/a/a/a/o;->c:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/o;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    .prologue
    .line 5511
    iget-object v0, p0, Lcom/sec/a/a/a/o;->d:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method private h()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 5554
    iget-object v0, p0, Lcom/sec/a/a/a/o;->c:Ljava/lang/Object;

    .line 5555
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5556
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 5557
    iput-object v0, p0, Lcom/sec/a/a/a/o;->c:Ljava/lang/Object;

    .line 5560
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 5581
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/o;->c:Ljava/lang/Object;

    .line 5582
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sec/a/a/a/o;->d:Lcom/google/protobuf/LazyStringList;

    .line 5583
    return-void
.end method

.method public static newBuilder()Lcom/sec/a/a/a/p;
    .locals 1

    .prologue
    .line 5691
    invoke-static {}, Lcom/sec/a/a/a/p;->f()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/o;
    .locals 1

    .prologue
    .line 5527
    sget-object v0, Lcom/sec/a/a/a/o;->a:Lcom/sec/a/a/a/o;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5536
    iget v1, p0, Lcom/sec/a/a/a/o;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5540
    iget-object v0, p0, Lcom/sec/a/a/a/o;->c:Ljava/lang/Object;

    .line 5541
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5542
    check-cast v0, Ljava/lang/String;

    .line 5549
    :goto_0
    return-object v0

    .line 5544
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 5545
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 5546
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5547
    iput-object v1, p0, Lcom/sec/a/a/a/o;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 5549
    goto :goto_0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5569
    iget-object v0, p0, Lcom/sec/a/a/a/o;->d:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public f()Lcom/sec/a/a/a/p;
    .locals 1

    .prologue
    .line 5695
    invoke-static {}, Lcom/sec/a/a/a/o;->newBuilder()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/sec/a/a/a/p;
    .locals 1

    .prologue
    .line 5703
    invoke-static {p0}, Lcom/sec/a/a/a/o;->a(Lcom/sec/a/a/a/o;)Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5511
    invoke-virtual {p0}, Lcom/sec/a/a/a/o;->b()Lcom/sec/a/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 5609
    iget v0, p0, Lcom/sec/a/a/a/o;->f:I

    .line 5610
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 5626
    :goto_0
    return v0

    .line 5614
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/o;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 5615
    invoke-direct {p0}, Lcom/sec/a/a/a/o;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v1

    .line 5619
    :goto_2
    iget-object v3, p0, Lcom/sec/a/a/a/o;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 5620
    iget-object v3, p0, Lcom/sec/a/a/a/o;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5619
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5622
    :cond_1
    add-int/2addr v0, v2

    .line 5623
    invoke-virtual {p0}, Lcom/sec/a/a/a/o;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5625
    iput v0, p0, Lcom/sec/a/a/a/o;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 5588
    iget-byte v1, p0, Lcom/sec/a/a/a/o;->e:B

    .line 5589
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 5590
    if-ne v1, v0, :cond_0

    .line 5593
    :goto_0
    return v0

    .line 5590
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5592
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/o;->e:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5511
    invoke-virtual {p0}, Lcom/sec/a/a/a/o;->f()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5511
    invoke-virtual {p0}, Lcom/sec/a/a/a/o;->g()Lcom/sec/a/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5633
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 5597
    invoke-virtual {p0}, Lcom/sec/a/a/a/o;->getSerializedSize()I

    .line 5598
    iget v0, p0, Lcom/sec/a/a/a/o;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 5599
    invoke-direct {p0}, Lcom/sec/a/a/a/o;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5601
    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/a/a/a/o;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 5602
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/a/a/a/o;->d:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5601
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5604
    :cond_1
    return-void
.end method
