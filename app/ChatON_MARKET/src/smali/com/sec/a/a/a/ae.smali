.class public final Lcom/sec/a/a/a/ae;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/af;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sec/a/a/a/ad;",
        "Lcom/sec/a/a/a/ae;",
        ">;",
        "Lcom/sec/a/a/a/af;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:I

.field private f:Ljava/lang/Object;

.field private g:I

.field private h:I

.field private i:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 7072
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 7288
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->c:Ljava/lang/Object;

    .line 7329
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->d:Ljava/lang/Object;

    .line 7395
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->f:Ljava/lang/Object;

    .line 7486
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->i:Ljava/lang/Object;

    .line 7073
    invoke-direct {p0}, Lcom/sec/a/a/a/ae;->g()V

    .line 7074
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/ae;)Lcom/sec/a/a/a/ad;
    .locals 1

    .prologue
    .line 7068
    invoke-direct {p0}, Lcom/sec/a/a/a/ae;->i()Lcom/sec/a/a/a/ad;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7068
    invoke-static {}, Lcom/sec/a/a/a/ae;->h()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 7077
    return-void
.end method

.method private static h()Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7080
    new-instance v0, Lcom/sec/a/a/a/ae;

    invoke-direct {v0}, Lcom/sec/a/a/a/ae;-><init>()V

    return-object v0
.end method

.method private i()Lcom/sec/a/a/a/ad;
    .locals 2

    .prologue
    .line 7121
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->e()Lcom/sec/a/a/a/ad;

    move-result-object v0

    .line 7122
    invoke-virtual {v0}, Lcom/sec/a/a/a/ad;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 7123
    invoke-static {v0}, Lcom/sec/a/a/a/ae;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 7125
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/a/a/a/ae;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7084
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 7085
    iput v1, p0, Lcom/sec/a/a/a/ae;->b:I

    .line 7086
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7087
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->c:Ljava/lang/Object;

    .line 7088
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7089
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->d:Ljava/lang/Object;

    .line 7090
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7091
    iput v1, p0, Lcom/sec/a/a/a/ae;->e:I

    .line 7092
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7093
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->f:Ljava/lang/Object;

    .line 7094
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7095
    iput v1, p0, Lcom/sec/a/a/a/ae;->g:I

    .line 7096
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7097
    iput v1, p0, Lcom/sec/a/a/a/ae;->h:I

    .line 7098
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7099
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->i:Ljava/lang/Object;

    .line 7100
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7101
    return-object p0
.end method

.method public a(I)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7274
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7275
    iput p1, p0, Lcom/sec/a/a/a/ae;->b:I

    .line 7277
    return-object p0
.end method

.method public a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7204
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 7205
    sparse-switch v0, :sswitch_data_0

    .line 7210
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/a/a/a/ae;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7212
    :sswitch_0
    return-object p0

    .line 7217
    :sswitch_1
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7218
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/ae;->b:I

    goto :goto_0

    .line 7222
    :sswitch_2
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7223
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->c:Ljava/lang/Object;

    goto :goto_0

    .line 7227
    :sswitch_3
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7228
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->d:Ljava/lang/Object;

    goto :goto_0

    .line 7232
    :sswitch_4
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7233
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/ae;->e:I

    goto :goto_0

    .line 7237
    :sswitch_5
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7238
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->f:Ljava/lang/Object;

    goto :goto_0

    .line 7242
    :sswitch_6
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7243
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/ae;->g:I

    goto :goto_0

    .line 7247
    :sswitch_7
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7248
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sec/a/a/a/ae;->h:I

    goto :goto_0

    .line 7252
    :sswitch_8
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7253
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/ae;->i:Ljava/lang/Object;

    goto :goto_0

    .line 7205
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Lcom/sec/a/a/a/ad;)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7169
    invoke-static {}, Lcom/sec/a/a/a/ad;->a()Lcom/sec/a/a/a/ad;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 7195
    :cond_0
    :goto_0
    return-object p0

    .line 7171
    :cond_1
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7172
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ae;->a(I)Lcom/sec/a/a/a/ae;

    .line 7174
    :cond_2
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7175
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ae;->a(Ljava/lang/String;)Lcom/sec/a/a/a/ae;

    .line 7177
    :cond_3
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 7178
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ae;->b(Ljava/lang/String;)Lcom/sec/a/a/a/ae;

    .line 7180
    :cond_4
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 7181
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->j()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ae;->b(I)Lcom/sec/a/a/a/ae;

    .line 7183
    :cond_5
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 7184
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ae;->c(Ljava/lang/String;)Lcom/sec/a/a/a/ae;

    .line 7186
    :cond_6
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 7187
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->n()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ae;->c(I)Lcom/sec/a/a/a/ae;

    .line 7189
    :cond_7
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->o()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 7190
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->p()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ae;->d(I)Lcom/sec/a/a/a/ae;

    .line 7192
    :cond_8
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7193
    invoke-virtual {p1}, Lcom/sec/a/a/a/ad;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/a/a/a/ae;->d(Ljava/lang/String;)Lcom/sec/a/a/a/ae;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7306
    if-nez p1, :cond_0

    .line 7307
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7309
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7310
    iput-object p1, p0, Lcom/sec/a/a/a/ae;->c:Ljava/lang/Object;

    .line 7312
    return-object p0
.end method

.method public b()Lcom/sec/a/a/a/ae;
    .locals 2

    .prologue
    .line 7105
    invoke-static {}, Lcom/sec/a/a/a/ae;->h()Lcom/sec/a/a/a/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->e()Lcom/sec/a/a/a/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/a/a/a/ae;->a(Lcom/sec/a/a/a/ad;)Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7381
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7382
    iput p1, p0, Lcom/sec/a/a/a/ae;->e:I

    .line 7384
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7347
    if-nez p1, :cond_0

    .line 7348
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7350
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7351
    iput-object p1, p0, Lcom/sec/a/a/a/ae;->d:Ljava/lang/Object;

    .line 7353
    return-object p0
.end method

.method public synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->d()Lcom/sec/a/a/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->e()Lcom/sec/a/a/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/sec/a/a/a/ad;
    .locals 1

    .prologue
    .line 7109
    invoke-static {}, Lcom/sec/a/a/a/ad;->a()Lcom/sec/a/a/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7447
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7448
    iput p1, p0, Lcom/sec/a/a/a/ae;->g:I

    .line 7450
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7413
    if-nez p1, :cond_0

    .line 7414
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7416
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7417
    iput-object p1, p0, Lcom/sec/a/a/a/ae;->f:Ljava/lang/Object;

    .line 7419
    return-object p0
.end method

.method public synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->a()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->a()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->b()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->b()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->b()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->b()Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/sec/a/a/a/ad;
    .locals 2

    .prologue
    .line 7113
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->e()Lcom/sec/a/a/a/ad;

    move-result-object v0

    .line 7114
    invoke-virtual {v0}, Lcom/sec/a/a/a/ad;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 7115
    invoke-static {v0}, Lcom/sec/a/a/a/ae;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 7117
    :cond_0
    return-object v0
.end method

.method public d(I)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7472
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7473
    iput p1, p0, Lcom/sec/a/a/a/ae;->h:I

    .line 7475
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/sec/a/a/a/ae;
    .locals 1

    .prologue
    .line 7504
    if-nez p1, :cond_0

    .line 7505
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7507
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/ae;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7508
    iput-object p1, p0, Lcom/sec/a/a/a/ae;->i:Ljava/lang/Object;

    .line 7510
    return-object p0
.end method

.method public e()Lcom/sec/a/a/a/ad;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 7129
    new-instance v2, Lcom/sec/a/a/a/ad;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/sec/a/a/a/ad;-><init>(Lcom/sec/a/a/a/ae;Lcom/sec/a/a/a/b;)V

    .line 7130
    iget v3, p0, Lcom/sec/a/a/a/ae;->a:I

    .line 7131
    const/4 v1, 0x0

    .line 7132
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    .line 7135
    :goto_0
    iget v1, p0, Lcom/sec/a/a/a/ae;->b:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ad;->a(Lcom/sec/a/a/a/ad;I)I

    .line 7136
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 7137
    or-int/lit8 v0, v0, 0x2

    .line 7139
    :cond_0
    iget-object v1, p0, Lcom/sec/a/a/a/ae;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ad;->a(Lcom/sec/a/a/a/ad;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7140
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 7141
    or-int/lit8 v0, v0, 0x4

    .line 7143
    :cond_1
    iget-object v1, p0, Lcom/sec/a/a/a/ae;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ad;->b(Lcom/sec/a/a/a/ad;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7144
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 7145
    or-int/lit8 v0, v0, 0x8

    .line 7147
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/ae;->e:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ad;->b(Lcom/sec/a/a/a/ad;I)I

    .line 7148
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 7149
    or-int/lit8 v0, v0, 0x10

    .line 7151
    :cond_3
    iget-object v1, p0, Lcom/sec/a/a/a/ae;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ad;->c(Lcom/sec/a/a/a/ad;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7152
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 7153
    or-int/lit8 v0, v0, 0x20

    .line 7155
    :cond_4
    iget v1, p0, Lcom/sec/a/a/a/ae;->g:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ad;->c(Lcom/sec/a/a/a/ad;I)I

    .line 7156
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 7157
    or-int/lit8 v0, v0, 0x40

    .line 7159
    :cond_5
    iget v1, p0, Lcom/sec/a/a/a/ae;->h:I

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ad;->d(Lcom/sec/a/a/a/ad;I)I

    .line 7160
    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    .line 7161
    or-int/lit16 v0, v0, 0x80

    .line 7163
    :cond_6
    iget-object v1, p0, Lcom/sec/a/a/a/ae;->i:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/sec/a/a/a/ad;->d(Lcom/sec/a/a/a/ad;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7164
    invoke-static {v2, v0}, Lcom/sec/a/a/a/ad;->e(Lcom/sec/a/a/a/ad;I)I

    .line 7165
    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->c()Lcom/sec/a/a/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0}, Lcom/sec/a/a/a/ae;->c()Lcom/sec/a/a/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 7199
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/ae;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7068
    check-cast p1, Lcom/sec/a/a/a/ad;

    invoke-virtual {p0, p1}, Lcom/sec/a/a/a/ae;->a(Lcom/sec/a/a/a/ad;)Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7068
    invoke-virtual {p0, p1, p2}, Lcom/sec/a/a/a/ae;->a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sec/a/a/a/ae;

    move-result-object v0

    return-object v0
.end method
