.class public final Lcom/sec/a/a/a/f;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/h;


# static fields
.field private static final a:Lcom/sec/a/a/a/f;


# instance fields
.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2665
    new-instance v0, Lcom/sec/a/a/a/f;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/f;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/f;->a:Lcom/sec/a/a/a/f;

    .line 2666
    sget-object v0, Lcom/sec/a/a/a/f;->a:Lcom/sec/a/a/a/f;

    invoke-direct {v0}, Lcom/sec/a/a/a/f;->p()V

    .line 2667
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/g;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2111
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 2246
    iput-byte v0, p0, Lcom/sec/a/a/a/f;->g:B

    .line 2273
    iput v0, p0, Lcom/sec/a/a/a/f;->h:I

    .line 2112
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/g;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 2108
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/f;-><init>(Lcom/sec/a/a/a/g;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2114
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2246
    iput-byte v0, p0, Lcom/sec/a/a/a/f;->g:B

    .line 2273
    iput v0, p0, Lcom/sec/a/a/a/f;->h:I

    .line 2115
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/f;I)I
    .locals 0

    .prologue
    .line 2108
    iput p1, p0, Lcom/sec/a/a/a/f;->c:I

    return p1
.end method

.method public static a()Lcom/sec/a/a/a/f;
    .locals 1

    .prologue
    .line 2120
    sget-object v0, Lcom/sec/a/a/a/f;->a:Lcom/sec/a/a/a/f;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/f;
    .locals 1

    .prologue
    .line 2314
    invoke-static {}, Lcom/sec/a/a/a/f;->newBuilder()Lcom/sec/a/a/a/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/g;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/g;

    invoke-static {v0}, Lcom/sec/a/a/a/g;->a(Lcom/sec/a/a/a/g;)Lcom/sec/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/f;)Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2368
    invoke-static {}, Lcom/sec/a/a/a/f;->newBuilder()Lcom/sec/a/a/a/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/g;->a(Lcom/sec/a/a/a/f;)Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a/f;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 2108
    iput-object p1, p0, Lcom/sec/a/a/a/f;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/f;I)I
    .locals 0

    .prologue
    .line 2108
    iput p1, p0, Lcom/sec/a/a/a/f;->b:I

    return p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/f;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 2108
    iput-object p1, p0, Lcom/sec/a/a/a/f;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/f;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 2108
    iput-object p1, p0, Lcom/sec/a/a/a/f;->f:Ljava/lang/Object;

    return-object p1
.end method

.method private m()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2163
    iget-object v0, p0, Lcom/sec/a/a/a/f;->d:Ljava/lang/Object;

    .line 2164
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2165
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2166
    iput-object v0, p0, Lcom/sec/a/a/a/f;->d:Ljava/lang/Object;

    .line 2169
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private n()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2196
    iget-object v0, p0, Lcom/sec/a/a/a/f;->e:Ljava/lang/Object;

    .line 2197
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2198
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2199
    iput-object v0, p0, Lcom/sec/a/a/a/f;->e:Ljava/lang/Object;

    .line 2202
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static newBuilder()Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2360
    invoke-static {}, Lcom/sec/a/a/a/g;->f()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2229
    iget-object v0, p0, Lcom/sec/a/a/a/f;->f:Ljava/lang/Object;

    .line 2230
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2231
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2232
    iput-object v0, p0, Lcom/sec/a/a/a/f;->f:Ljava/lang/Object;

    .line 2235
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private p()V
    .locals 1

    .prologue
    .line 2240
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/a/a/a/f;->c:I

    .line 2241
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/f;->d:Ljava/lang/Object;

    .line 2242
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/f;->e:Ljava/lang/Object;

    .line 2243
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/a/a/a/f;->f:Ljava/lang/Object;

    .line 2244
    return-void
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/f;
    .locals 1

    .prologue
    .line 2124
    sget-object v0, Lcom/sec/a/a/a/f;->a:Lcom/sec/a/a/a/f;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2133
    iget v1, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 2137
    iget v0, p0, Lcom/sec/a/a/a/f;->c:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 2145
    iget v0, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2149
    iget-object v0, p0, Lcom/sec/a/a/a/f;->d:Ljava/lang/Object;

    .line 2150
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2151
    check-cast v0, Ljava/lang/String;

    .line 2158
    :goto_0
    return-object v0

    .line 2153
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2154
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2155
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2156
    iput-object v1, p0, Lcom/sec/a/a/a/f;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2158
    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 2178
    iget v0, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2108
    invoke-virtual {p0}, Lcom/sec/a/a/a/f;->b()Lcom/sec/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2276
    iget v0, p0, Lcom/sec/a/a/a/f;->h:I

    .line 2277
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2294
    :goto_0
    return v0

    .line 2280
    :cond_0
    const/4 v0, 0x0

    .line 2281
    iget v1, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2282
    iget v1, p0, Lcom/sec/a/a/a/f;->c:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2284
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2285
    invoke-direct {p0}, Lcom/sec/a/a/a/f;->m()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2287
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 2288
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/f;->n()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2290
    :cond_3
    iget v1, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2291
    invoke-direct {p0}, Lcom/sec/a/a/a/f;->o()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2293
    :cond_4
    iput v0, p0, Lcom/sec/a/a/a/f;->h:I

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2182
    iget-object v0, p0, Lcom/sec/a/a/a/f;->e:Ljava/lang/Object;

    .line 2183
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2184
    check-cast v0, Ljava/lang/String;

    .line 2191
    :goto_0
    return-object v0

    .line 2186
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2187
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2188
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2189
    iput-object v1, p0, Lcom/sec/a/a/a/f;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2191
    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 2211
    iget v0, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2249
    iget-byte v1, p0, Lcom/sec/a/a/a/f;->g:B

    .line 2250
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 2251
    if-ne v1, v0, :cond_0

    .line 2254
    :goto_0
    return v0

    .line 2251
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2253
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/f;->g:B

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2215
    iget-object v0, p0, Lcom/sec/a/a/a/f;->f:Ljava/lang/Object;

    .line 2216
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2217
    check-cast v0, Ljava/lang/String;

    .line 2224
    :goto_0
    return-object v0

    .line 2219
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2220
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2221
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2222
    iput-object v1, p0, Lcom/sec/a/a/a/f;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2224
    goto :goto_0
.end method

.method public k()Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2364
    invoke-static {}, Lcom/sec/a/a/a/f;->newBuilder()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/sec/a/a/a/g;
    .locals 1

    .prologue
    .line 2372
    invoke-static {p0}, Lcom/sec/a/a/a/f;->a(Lcom/sec/a/a/a/f;)Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2108
    invoke-virtual {p0}, Lcom/sec/a/a/a/f;->k()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2108
    invoke-virtual {p0}, Lcom/sec/a/a/a/f;->l()Lcom/sec/a/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2301
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2258
    invoke-virtual {p0}, Lcom/sec/a/a/a/f;->getSerializedSize()I

    .line 2259
    iget v0, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2260
    iget v0, p0, Lcom/sec/a/a/a/f;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 2262
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2263
    invoke-direct {p0}, Lcom/sec/a/a/a/f;->m()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2265
    :cond_1
    iget v0, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2266
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sec/a/a/a/f;->n()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2268
    :cond_2
    iget v0, p0, Lcom/sec/a/a/a/f;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2269
    invoke-direct {p0}, Lcom/sec/a/a/a/f;->o()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2271
    :cond_3
    return-void
.end method
