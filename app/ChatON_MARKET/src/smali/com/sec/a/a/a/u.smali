.class public final Lcom/sec/a/a/a/u;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/w;


# static fields
.field private static final a:Lcom/sec/a/a/a/u;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/a/a/a/r;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 5489
    new-instance v0, Lcom/sec/a/a/a/u;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/u;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/u;->a:Lcom/sec/a/a/a/u;

    .line 5490
    sget-object v0, Lcom/sec/a/a/a/u;->a:Lcom/sec/a/a/a/u;

    invoke-direct {v0}, Lcom/sec/a/a/a/u;->g()V

    .line 5491
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5134
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 5178
    iput-byte v0, p0, Lcom/sec/a/a/a/u;->c:B

    .line 5196
    iput v0, p0, Lcom/sec/a/a/a/u;->d:I

    .line 5135
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/v;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 5131
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/u;-><init>(Lcom/sec/a/a/a/v;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5137
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5178
    iput-byte v0, p0, Lcom/sec/a/a/a/u;->c:B

    .line 5196
    iput v0, p0, Lcom/sec/a/a/a/u;->d:I

    .line 5138
    return-void
.end method

.method public static a()Lcom/sec/a/a/a/u;
    .locals 1

    .prologue
    .line 5143
    sget-object v0, Lcom/sec/a/a/a/u;->a:Lcom/sec/a/a/a/u;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/u;
    .locals 1

    .prologue
    .line 5228
    invoke-static {}, Lcom/sec/a/a/a/u;->newBuilder()Lcom/sec/a/a/a/v;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/v;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/v;

    invoke-static {v0}, Lcom/sec/a/a/a/v;->a(Lcom/sec/a/a/a/v;)Lcom/sec/a/a/a/u;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/u;)Lcom/sec/a/a/a/v;
    .locals 1

    .prologue
    .line 5281
    invoke-static {}, Lcom/sec/a/a/a/u;->newBuilder()Lcom/sec/a/a/a/v;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/v;->a(Lcom/sec/a/a/a/u;)Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/a/a/a/u;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 5131
    iput-object p1, p0, Lcom/sec/a/a/a/u;->b:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/a/a/a/u;)Ljava/util/List;
    .locals 1

    .prologue
    .line 5131
    iget-object v0, p0, Lcom/sec/a/a/a/u;->b:Ljava/util/List;

    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 5175
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a/u;->b:Ljava/util/List;

    .line 5176
    return-void
.end method

.method public static newBuilder()Lcom/sec/a/a/a/v;
    .locals 1

    .prologue
    .line 5273
    invoke-static {}, Lcom/sec/a/a/a/v;->f()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/u;
    .locals 1

    .prologue
    .line 5147
    sget-object v0, Lcom/sec/a/a/a/u;->a:Lcom/sec/a/a/a/u;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/a/a/a/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5155
    iget-object v0, p0, Lcom/sec/a/a/a/u;->b:Ljava/util/List;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 5163
    iget-object v0, p0, Lcom/sec/a/a/a/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public e()Lcom/sec/a/a/a/v;
    .locals 1

    .prologue
    .line 5277
    invoke-static {}, Lcom/sec/a/a/a/u;->newBuilder()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/sec/a/a/a/v;
    .locals 1

    .prologue
    .line 5285
    invoke-static {p0}, Lcom/sec/a/a/a/u;->a(Lcom/sec/a/a/a/u;)Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5131
    invoke-virtual {p0}, Lcom/sec/a/a/a/u;->b()Lcom/sec/a/a/a/u;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 5199
    iget v2, p0, Lcom/sec/a/a/a/u;->d:I

    .line 5200
    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    .line 5208
    :goto_0
    return v2

    :cond_0
    move v1, v0

    move v2, v0

    .line 5204
    :goto_1
    iget-object v0, p0, Lcom/sec/a/a/a/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 5205
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sec/a/a/a/u;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v2, v0

    .line 5204
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 5207
    :cond_1
    iput v2, p0, Lcom/sec/a/a/a/u;->d:I

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 5181
    iget-byte v1, p0, Lcom/sec/a/a/a/u;->c:B

    .line 5182
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 5183
    if-ne v1, v0, :cond_0

    .line 5186
    :goto_0
    return v0

    .line 5183
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5185
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/u;->c:B

    goto :goto_0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5131
    invoke-virtual {p0}, Lcom/sec/a/a/a/u;->e()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5131
    invoke-virtual {p0}, Lcom/sec/a/a/a/u;->f()Lcom/sec/a/a/a/v;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5215
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    .line 5190
    invoke-virtual {p0}, Lcom/sec/a/a/a/u;->getSerializedSize()I

    .line 5191
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/a/a/a/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 5192
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/a/a/a/u;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 5191
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 5194
    :cond_0
    return-void
.end method
