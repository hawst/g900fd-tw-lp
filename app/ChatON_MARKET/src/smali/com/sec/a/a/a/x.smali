.class public final Lcom/sec/a/a/a/x;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MsgFrontend.java"

# interfaces
.implements Lcom/sec/a/a/a/z;


# static fields
.field private static final a:Lcom/sec/a/a/a/x;


# instance fields
.field private b:I

.field private c:I

.field private d:J

.field private e:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 4039
    new-instance v0, Lcom/sec/a/a/a/x;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/a/a/a/x;-><init>(Z)V

    sput-object v0, Lcom/sec/a/a/a/x;->a:Lcom/sec/a/a/a/x;

    .line 4040
    sget-object v0, Lcom/sec/a/a/a/x;->a:Lcom/sec/a/a/a/x;

    invoke-direct {v0}, Lcom/sec/a/a/a/x;->k()V

    .line 4041
    return-void
.end method

.method private constructor <init>(Lcom/sec/a/a/a/y;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3655
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 3714
    iput-byte v0, p0, Lcom/sec/a/a/a/x;->f:B

    .line 3738
    iput v0, p0, Lcom/sec/a/a/a/x;->g:I

    .line 3656
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/a/a/a/y;Lcom/sec/a/a/a/b;)V
    .locals 0

    .prologue
    .line 3652
    invoke-direct {p0, p1}, Lcom/sec/a/a/a/x;-><init>(Lcom/sec/a/a/a/y;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3658
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3714
    iput-byte v0, p0, Lcom/sec/a/a/a/x;->f:B

    .line 3738
    iput v0, p0, Lcom/sec/a/a/a/x;->g:I

    .line 3659
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a/x;I)I
    .locals 0

    .prologue
    .line 3652
    iput p1, p0, Lcom/sec/a/a/a/x;->c:I

    return p1
.end method

.method static synthetic a(Lcom/sec/a/a/a/x;J)J
    .locals 0

    .prologue
    .line 3652
    iput-wide p1, p0, Lcom/sec/a/a/a/x;->d:J

    return-wide p1
.end method

.method public static a()Lcom/sec/a/a/a/x;
    .locals 1

    .prologue
    .line 3664
    sget-object v0, Lcom/sec/a/a/a/x;->a:Lcom/sec/a/a/a/x;

    return-object v0
.end method

.method public static a([B)Lcom/sec/a/a/a/x;
    .locals 1

    .prologue
    .line 3776
    invoke-static {}, Lcom/sec/a/a/a/x;->newBuilder()Lcom/sec/a/a/a/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/y;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sec/a/a/a/y;

    invoke-static {v0}, Lcom/sec/a/a/a/y;->a(Lcom/sec/a/a/a/y;)Lcom/sec/a/a/a/x;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/sec/a/a/a/x;)Lcom/sec/a/a/a/y;
    .locals 1

    .prologue
    .line 3829
    invoke-static {}, Lcom/sec/a/a/a/x;->newBuilder()Lcom/sec/a/a/a/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/a/a/a/y;->a(Lcom/sec/a/a/a/x;)Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/sec/a/a/a/x;I)I
    .locals 0

    .prologue
    .line 3652
    iput p1, p0, Lcom/sec/a/a/a/x;->e:I

    return p1
.end method

.method static synthetic c(Lcom/sec/a/a/a/x;I)I
    .locals 0

    .prologue
    .line 3652
    iput p1, p0, Lcom/sec/a/a/a/x;->b:I

    return p1
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3709
    iput v2, p0, Lcom/sec/a/a/a/x;->c:I

    .line 3710
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/a/a/a/x;->d:J

    .line 3711
    iput v2, p0, Lcom/sec/a/a/a/x;->e:I

    .line 3712
    return-void
.end method

.method public static newBuilder()Lcom/sec/a/a/a/y;
    .locals 1

    .prologue
    .line 3821
    invoke-static {}, Lcom/sec/a/a/a/y;->f()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Lcom/sec/a/a/a/x;
    .locals 1

    .prologue
    .line 3668
    sget-object v0, Lcom/sec/a/a/a/x;->a:Lcom/sec/a/a/a/x;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3677
    iget v1, p0, Lcom/sec/a/a/a/x;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 3681
    iget v0, p0, Lcom/sec/a/a/a/x;->c:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 3689
    iget v0, p0, Lcom/sec/a/a/a/x;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 3693
    iget-wide v0, p0, Lcom/sec/a/a/a/x;->d:J

    return-wide v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 3701
    iget v0, p0, Lcom/sec/a/a/a/x;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3652
    invoke-virtual {p0}, Lcom/sec/a/a/a/x;->b()Lcom/sec/a/a/a/x;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3741
    iget v0, p0, Lcom/sec/a/a/a/x;->g:I

    .line 3742
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3756
    :goto_0
    return v0

    .line 3745
    :cond_0
    const/4 v0, 0x0

    .line 3746
    iget v1, p0, Lcom/sec/a/a/a/x;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 3747
    iget v1, p0, Lcom/sec/a/a/a/x;->c:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3749
    :cond_1
    iget v1, p0, Lcom/sec/a/a/a/x;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 3750
    iget-wide v1, p0, Lcom/sec/a/a/a/x;->d:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3752
    :cond_2
    iget v1, p0, Lcom/sec/a/a/a/x;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 3753
    const/4 v1, 0x3

    iget v2, p0, Lcom/sec/a/a/a/x;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3755
    :cond_3
    iput v0, p0, Lcom/sec/a/a/a/x;->g:I

    goto :goto_0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 3705
    iget v0, p0, Lcom/sec/a/a/a/x;->e:I

    return v0
.end method

.method public i()Lcom/sec/a/a/a/y;
    .locals 1

    .prologue
    .line 3825
    invoke-static {}, Lcom/sec/a/a/a/x;->newBuilder()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3717
    iget-byte v1, p0, Lcom/sec/a/a/a/x;->f:B

    .line 3718
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 3719
    if-ne v1, v0, :cond_0

    .line 3722
    :goto_0
    return v0

    .line 3719
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3721
    :cond_1
    iput-byte v0, p0, Lcom/sec/a/a/a/x;->f:B

    goto :goto_0
.end method

.method public j()Lcom/sec/a/a/a/y;
    .locals 1

    .prologue
    .line 3833
    invoke-static {p0}, Lcom/sec/a/a/a/x;->a(Lcom/sec/a/a/a/x;)Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3652
    invoke-virtual {p0}, Lcom/sec/a/a/a/x;->i()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3652
    invoke-virtual {p0}, Lcom/sec/a/a/a/x;->j()Lcom/sec/a/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3763
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3726
    invoke-virtual {p0}, Lcom/sec/a/a/a/x;->getSerializedSize()I

    .line 3727
    iget v0, p0, Lcom/sec/a/a/a/x;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3728
    iget v0, p0, Lcom/sec/a/a/a/x;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3730
    :cond_0
    iget v0, p0, Lcom/sec/a/a/a/x;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3731
    iget-wide v0, p0, Lcom/sec/a/a/a/x;->d:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 3733
    :cond_1
    iget v0, p0, Lcom/sec/a/a/a/x;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 3734
    const/4 v0, 0x3

    iget v1, p0, Lcom/sec/a/a/a/x;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3736
    :cond_2
    return-void
.end method
