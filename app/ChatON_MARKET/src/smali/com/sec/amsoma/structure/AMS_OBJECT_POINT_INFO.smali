.class public Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;
.super Ljava/lang/Object;
.source "AMS_OBJECT_POINT_INFO.java"


# instance fields
.field m_bPressureFlag:Z

.field m_nPointNum:I

.field m_nSimpleNum:I

.field m_pcSimpleFlag:[C

.field m_pnAMSPointPressure:[S

.field m_pnAMSPointX:[S

.field m_pnAMSPointY:[S


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getM_bPressureFlag()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_bPressureFlag:Z

    return v0
.end method

.method public getM_nPointNum()I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_nPointNum:I

    return v0
.end method

.method public getM_nSimpleNum()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_nSimpleNum:I

    return v0
.end method

.method public getM_pcSimpleFlag(I)C
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pcSimpleFlag:[C

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pcSimpleFlag:[C

    aget-char v0, v0, p1

    .line 90
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getM_pcSimpleFlag()[C
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pcSimpleFlag:[C

    return-object v0
.end method

.method public getM_pnAMSPointPressure(I)S
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointPressure:[S

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointPressure:[S

    aget-short v0, v0, p1

    .line 105
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getM_pnAMSPointPressure()[S
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointPressure:[S

    return-object v0
.end method

.method public getM_pnAMSPointX(I)S
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointX:[S

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointX:[S

    aget-short v0, v0, p1

    .line 28
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getM_pnAMSPointX()[S
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointX:[S

    return-object v0
.end method

.method public getM_pnAMSPointY(I)S
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointY:[S

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointY:[S

    aget-short v0, v0, p1

    .line 55
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getM_pnAMSPointY()[S
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointY:[S

    return-object v0
.end method

.method public setM_bPressureFlag(Z)V
    .locals 0

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_bPressureFlag:Z

    .line 118
    return-void
.end method

.method public setM_nPointNum(I)V
    .locals 0

    .prologue
    .line 17
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_nPointNum:I

    .line 18
    return-void
.end method

.method public setM_nSimpleNum(I)V
    .locals 0

    .prologue
    .line 79
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_nSimpleNum:I

    .line 80
    return-void
.end method

.method public setM_pcSimpleFlag([C)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pcSimpleFlag:[C

    .line 95
    return-void
.end method

.method public setM_pnAMSPointPressure([S)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointPressure:[S

    .line 110
    return-void
.end method

.method public setM_pnAMSPointX([S)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 33
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-gt v1, v2, :cond_1

    .line 34
    iput-object p1, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointX:[S

    .line 45
    :cond_0
    return-void

    .line 36
    :cond_1
    array-length v1, p1

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointX:[S

    .line 40
    array-length v3, p1

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-short v4, p1, v0

    .line 41
    iget-object v5, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointX:[S

    add-int/lit8 v2, v1, 0x1

    aput-short v4, v5, v1

    .line 40
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0
.end method

.method public setM_pnAMSPointY([S)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 60
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-gt v1, v2, :cond_1

    .line 61
    iput-object p1, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointY:[S

    .line 72
    :cond_0
    return-void

    .line 63
    :cond_1
    array-length v1, p1

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointY:[S

    .line 67
    array-length v3, p1

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-short v4, p1, v0

    .line 68
    iget-object v5, p0, Lcom/sec/amsoma/structure/AMS_OBJECT_POINT_INFO;->m_pnAMSPointY:[S

    add-int/lit8 v2, v1, 0x1

    aput-short v4, v5, v1

    .line 67
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0
.end method
