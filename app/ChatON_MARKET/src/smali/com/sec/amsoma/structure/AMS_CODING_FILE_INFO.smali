.class public Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;
.super Ljava/lang/Object;
.source "AMS_CODING_FILE_INFO.java"


# instance fields
.field public m_eExportType:I

.field public m_nCurPageIndex:I

.field public m_nMessageID:I

.field public m_nTotalPageNum:I

.field public m_strFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getM_eExportType()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->m_eExportType:I

    return v0
.end method

.method public getM_nCurPageIndex()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->m_nCurPageIndex:I

    return v0
.end method

.method public getM_nMessageID()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->m_nMessageID:I

    return v0
.end method

.method public getM_nTotalPageNum()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->m_nTotalPageNum:I

    return v0
.end method

.method public getM_strFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->m_strFileName:Ljava/lang/String;

    return-object v0
.end method

.method public setM_eExportType(I)V
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->m_eExportType:I

    .line 25
    return-void
.end method

.method public setM_nCurPageIndex(I)V
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->m_nCurPageIndex:I

    .line 41
    return-void
.end method

.method public setM_nMessageID(I)V
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->m_nMessageID:I

    .line 57
    return-void
.end method

.method public setM_nTotalPageNum(I)V
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->m_nTotalPageNum:I

    .line 49
    return-void
.end method

.method public setM_strFileName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;->m_strFileName:Ljava/lang/String;

    .line 33
    return-void
.end method
