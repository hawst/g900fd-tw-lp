.class public Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;
.super Ljava/lang/Object;
.source "AMS_SELECT_OBJECT.java"


# instance fields
.field m_UserStampHeight:I

.field m_UserStampMemory:[I

.field m_UserStampWidth:I

.field m_nExcludedDrawingBPP:I

.field m_nExcludedDrawingHeight:I

.field m_nExcludedDrawingWidth:I

.field m_nSelectObjectBPP:I

.field m_pExcludedDrawingMemory:[I

.field m_pSelectObjectData:I

.field m_pSelectObjectNode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getM_UserStampHeight()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_UserStampHeight:I

    return v0
.end method

.method public getM_UserStampMemory()[I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_UserStampMemory:[I

    return-object v0
.end method

.method public getM_UserStampWidth()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_UserStampWidth:I

    return v0
.end method

.method public getM_nExcludedDrawingBPP()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_nExcludedDrawingBPP:I

    return v0
.end method

.method public getM_nExcludedDrawingHeight()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_nExcludedDrawingHeight:I

    return v0
.end method

.method public getM_nExcludedDrawingWidth()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_nExcludedDrawingWidth:I

    return v0
.end method

.method public getM_nSelectObjectBPP()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_nSelectObjectBPP:I

    return v0
.end method

.method public getM_pExcludedDrawingMemory()[I
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_pExcludedDrawingMemory:[I

    return-object v0
.end method

.method public getM_pSelectObjectData()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_pSelectObjectData:I

    return v0
.end method

.method public getM_pSelectObjectNode()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_pSelectObjectNode:I

    return v0
.end method

.method public setM_UserStampHeight(I)V
    .locals 0

    .prologue
    .line 98
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_UserStampHeight:I

    .line 99
    return-void
.end method

.method public setM_UserStampMemory([III)V
    .locals 4

    .prologue
    .line 78
    mul-int v1, p2, p3

    .line 79
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_UserStampMemory:[I

    .line 80
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 81
    iget-object v2, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_UserStampMemory:[I

    aget v3, p1, v0

    aput v3, v2, v0

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method

.method public setM_UserStampWidth(I)V
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_UserStampWidth:I

    .line 91
    return-void
.end method

.method public setM_nExcludedDrawingBPP(I)V
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_nExcludedDrawingBPP:I

    .line 52
    return-void
.end method

.method public setM_nExcludedDrawingHeight(I)V
    .locals 0

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_nExcludedDrawingHeight:I

    .line 44
    return-void
.end method

.method public setM_nExcludedDrawingWidth(I)V
    .locals 0

    .prologue
    .line 35
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_nExcludedDrawingWidth:I

    .line 36
    return-void
.end method

.method public setM_nSelectObjectBPP(I)V
    .locals 0

    .prologue
    .line 106
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_nSelectObjectBPP:I

    .line 107
    return-void
.end method

.method public setM_pExcludedDrawingMemory([III)V
    .locals 4

    .prologue
    .line 22
    mul-int v1, p2, p3

    .line 24
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_pExcludedDrawingMemory:[I

    .line 25
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 26
    iget-object v2, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_pExcludedDrawingMemory:[I

    aget v3, p1, v0

    aput v3, v2, v0

    .line 25
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_0
    return-void
.end method

.method public setM_pSelectObjectData(I)V
    .locals 0

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_pSelectObjectData:I

    .line 68
    return-void
.end method

.method public setM_pSelectObjectNode(I)V
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/amsoma/structure/AMS_SELECT_OBJECT;->m_pSelectObjectNode:I

    .line 60
    return-void
.end method
