.class public Lcom/sec/amsoma/AMSLibs;
.super Ljava/lang/Object;
.source "AMSLibs.java"


# static fields
.field public static final DEF_VIP_AMSLIB_BK_COLOR:B = 0x0t

.field public static final DEF_VIP_AMSLIB_BK_INDEX_IMAGE:B = 0x2t

.field public static final DEF_VIP_AMSLIB_BK_PATTERN:B = 0x1t

.field public static final DEF_VIP_AMSLIB_BK_RAW_IMAGE:B = 0x4t

.field public static final DEF_VIP_AMSLIB_BK_SCREEN_BUF:B = 0x5t

.field public static final DEF_VIP_AMS_ANIMATION_TURN_PAGE_EFFECT_BRIGHT_VAR:I = 0x3

.field public static final DEF_VIP_AMS_ANIMATION_TURN_PAGE_EFFECT_SIDE_CURL:I = 0x1

.field public static final DEF_VIP_AMS_ANIMATION_TURN_PAGE_EFFECT_SLIDE_MOVE:I = 0x2

.field public static final DEF_VIP_AMS_ANIMATION_TURN_PAGE_NONE:I = 0x0

.field public static final ENUM_VIP_AMS_AUDIO_BG_INDEX:I = 0x1

.field public static final ENUM_VIP_AMS_AUDIO_BG_MP3:I = 0x2

.field public static final ENUM_VIP_AMS_AUDIO_BG_VOICE:I = 0x3

.field public static final ENUM_VIP_AMS_AUDIO_NONE:I = 0x0

.field public static final ENUM_VIP_AMS_CB_ERROR_DISK_FULL:B = 0x2ct

.field public static final ENUM_VIP_AMS_CB_ERROR_DRAW:B = 0x30t

.field public static final ENUM_VIP_AMS_CB_ERROR_FILE_OPEN:B = 0x29t

.field public static final ENUM_VIP_AMS_CB_ERROR_FILE_READ:B = 0x2at

.field public static final ENUM_VIP_AMS_CB_ERROR_FILE_WRITE:B = 0x2bt

.field public static final ENUM_VIP_AMS_CB_ERROR_INVALID_LIB_VERSION:B = 0x31t

.field public static final ENUM_VIP_AMS_CB_ERROR_JPEG_DECODING:B = 0x2ft

.field public static final ENUM_VIP_AMS_CB_ERROR_JPEG_ENCODING:B = 0x2et

.field public static final ENUM_VIP_AMS_CB_ERROR_LIMITED_LIB_VERSION:B = 0x32t

.field public static final ENUM_VIP_AMS_CB_ERROR_MEMORY_ALLOC:B = 0x2dt

.field public static final ENUM_VIP_AMS_DIAGRAMSTYLE_CIRCLE:B = 0x1t

.field public static final ENUM_VIP_AMS_DIAGRAMSTYLE_CIRCLE_FILL:B = 0x4t

.field public static final ENUM_VIP_AMS_DIAGRAMSTYLE_LINE:B = 0x6t

.field public static final ENUM_VIP_AMS_DIAGRAMSTYLE_RECT:B = 0x0t

.field public static final ENUM_VIP_AMS_DIAGRAMSTYLE_RECT_FILL:B = 0x3t

.field public static final ENUM_VIP_AMS_DIAGRAMSTYLE_TRI:B = 0x2t

.field public static final ENUM_VIP_AMS_DIAGRAMSTYLE_TRI_FILL:B = 0x5t

.field public static final ENUM_VIP_AMS_DIAGRAM_OBJECT_NUM_FULL:B = 0xdt

.field public static final ENUM_VIP_AMS_ERROR_CONVERT_IMAGEBUFFER:B = 0x12t

.field public static final ENUM_VIP_AMS_ERROR_DATA_FULL:B = 0x9t

.field public static final ENUM_VIP_AMS_ERROR_DECODE_BUFFER:B = 0x13t

.field public static final ENUM_VIP_AMS_ERROR_DECODE_DATA:B = 0x15t

.field public static final ENUM_VIP_AMS_ERROR_DECODE_EXIF:B = 0x16t

.field public static final ENUM_VIP_AMS_ERROR_DECODE_HEADER:B = 0x14t

.field public static final ENUM_VIP_AMS_ERROR_DECODE_IMAGE:B = 0x17t

.field public static final ENUM_VIP_AMS_ERROR_DRAW:B = 0x22t

.field public static final ENUM_VIP_AMS_ERROR_ENCODE_DATA:B = 0x24t

.field public static final ENUM_VIP_AMS_ERROR_ENCODE_IMAGE:B = 0x10t

.field public static final ENUM_VIP_AMS_ERROR_INVALID_APP_VERSION:B = 0x2t

.field public static final ENUM_VIP_AMS_ERROR_INVALID_CLIP:B = 0x26t

.field public static final ENUM_VIP_AMS_ERROR_INVALID_DRAWING_DATA:B = 0x1dt

.field public static final ENUM_VIP_AMS_ERROR_INVALID_FILEFORMAT:B = 0x3t

.field public static final ENUM_VIP_AMS_ERROR_INVALID_FILEINFO:B = 0x1at

.field public static final ENUM_VIP_AMS_ERROR_INVALID_HEADER:B = 0x4t

.field public static final ENUM_VIP_AMS_ERROR_INVALID_IMAGE_SIZE:B = 0x1t

.field public static final ENUM_VIP_AMS_ERROR_INVALID_INSTANCE:B = 0x6t

.field public static final ENUM_VIP_AMS_ERROR_INVALID_OBJECT_DATA:B = 0x1ft

.field public static final ENUM_VIP_AMS_ERROR_INVALID_OBJECT_STYLE:B = 0x20t

.field public static final ENUM_VIP_AMS_ERROR_INVALID_PAGE:B = 0x7t

.field public static final ENUM_VIP_AMS_ERROR_LOAD_JPEGTORAW:B = 0x11t

.field public static final ENUM_VIP_AMS_ERROR_NO:B = 0x0t

.field public static final ENUM_VIP_AMS_ERROR_NOMORE_PLAY_OBJECT:B = 0x27t

.field public static final ENUM_VIP_AMS_ERROR_NOT_READY:B = 0x5t

.field public static final ENUM_VIP_AMS_ERROR_OBJECT_ALLOC:B = 0x1et

.field public static final ENUM_VIP_AMS_ERROR_OBJECT_IMAGE_NUM_FULL:B = 0x33t

.field public static final ENUM_VIP_AMS_ERROR_OBJECT_PROPERTY_SETTING:B = 0x1bt

.field public static final ENUM_VIP_AMS_ERROR_OBJECT_START_SETTING:B = 0x1ct

.field public static final ENUM_VIP_AMS_ERROR_POINT_FULL:B = 0x8t

.field public static final ENUM_VIP_AMS_ERROR_SAVE_FLASH:B = 0x18t

.field public static final ENUM_VIP_AMS_ERROR_STRING_LENGTH:B = 0x21t

.field public static final ENUM_VIP_AMS_ERROR_TURN_PAGE:B = 0x28t

.field public static final ENUM_VIP_AMS_ERROR_UNDEFINED:B = 0x19t

.field public static final ENUM_VIP_AMS_ERROR_UNDO_REDO:B = 0x23t

.field public static final ENUM_VIP_AMS_ERROR_USERIMAGE_ID_FULL:B = 0xft

.field public static final ENUM_VIP_AMS_ERROR_USERIMAGE_TRANSMIT_FULL:B = 0xet

.field public static final ENUM_VIP_AMS_ERROR_ZOOM_INVALID_NEXTREGION:B = 0x25t

.field public static final ENUM_VIP_AMS_PENSTYLE_CRAYON:B = 0x1t

.field public static final ENUM_VIP_AMS_PENSTYLE_DOTLINE:B = 0x3t

.field public static final ENUM_VIP_AMS_PENSTYLE_PENCIL:B = 0x0t

.field public static final ENUM_VIP_AMS_PENSTYLE_SHINY:B = 0x2t

.field public static final ENUM_VIP_AMS_PENSTYLE_TRANSPARENT:B = 0x4t

.field public static final ENUM_VIP_AMS_PEN_OBJECT_NUM_FULL:B = 0xat

.field public static final ENUM_VIP_AMS_SAVE_THUMBNAIL_IMAGE_SIZE:I = 0x190

.field public static final ENUM_VIP_AMS_SAVE_USER_STAMPSIZE:B = 0x78t

.field public static final ENUM_VIP_AMS_STAMPSIZE_NORMAL:B = 0x2t

.field public static final ENUM_VIP_AMS_STAMPSIZE_SMALL:B = 0x1t

.field public static final ENUM_VIP_AMS_STAMPSIZE_UNDEFINED:B = 0x0t

.field public static final ENUM_VIP_AMS_STAMPSTYLE_ANIMATION:B = 0x1t

.field public static final ENUM_VIP_AMS_STAMPSTYLE_BASIC:B = 0x0t

.field public static final ENUM_VIP_AMS_STAMPSTYLE_USER:B = 0x2t

.field public static final ENUM_VIP_AMS_STAMPSTYLE_USER_CIRCLE:B = 0x3t

.field public static final ENUM_VIP_AMS_STAMPSTYLE_USER_HEART:B = 0x4t

.field public static final ENUM_VIP_AMS_STAMPSTYLE_USER_RECT:B = 0x6t

.field public static final ENUM_VIP_AMS_STAMPSTYLE_USER_STAR:B = 0x5t

.field public static final ENUM_VIP_AMS_STAMP_OBJECT_NUM_FULL:B = 0xct

.field public static final ENUM_VIP_AMS_TEXTSIZE_LARGE:B = 0x2t

.field public static final ENUM_VIP_AMS_TEXTSIZE_NORMAL:B = 0x1t

.field public static final ENUM_VIP_AMS_TEXTSIZE_SMALL:B = 0x0t

.field public static final ENUM_VIP_AMS_TEXT_OBJECT_NUM_FULL:B = 0xbt

.field public static final ENUM_VIP_AMS_TYPE_DIAGRAM:B = 0x4t

.field public static final ENUM_VIP_AMS_TYPE_PEN:B = 0x1t

.field public static final ENUM_VIP_AMS_TYPE_STAMP:B = 0x3t

.field public static final ENUM_VIP_AMS_TYPE_TEXT:B = 0x2t

.field public static final ENUM_VIP_AMS_TYPE_UNDEFINED:B = 0x0t

.field public static final ENUM_VIP_AMS_UNDO_REDO_DRAW_ALL:I = 0x2

.field public static final ENUM_VIP_AMS_UNDO_REDO_ERROR:I = 0x0

.field public static final ENUM_VIP_AMS_UNDO_REDO_UPDATE:I = 0x1

.field public static final ENUM_VIP_AMS_WQVGA_DRAW_BASIC_STAMPSIZE:B = 0x37t

.field public static final ENUM_VIP_AMS_WQVGA_DRAW_USER_STAMPSIZE_LARGE:B = 0x3ct

.field public static final ENUM_VIP_AMS_WQVGA_DRAW_USER_STAMPSIZE_SMALL:B = 0x1et

.field public static final ENUM_VIP_AMS_WQVGA_SAVE_USER_STAMPIMAGE_SIZE:I = 0x2800

.field public static final ENUM_VIP_AMS_WVGA_DRAW_BASIC_STAMPSIZE:B = 0x6et

.field public static final ENUM_VIP_AMS_WVGA_DRAW_USER_STAMPSIZE_LARGE:B = 0x78t

.field public static final ENUM_VIP_AMS_WVGA_DRAW_USER_STAMPSIZE_SMALL:B = 0x3ct

.field public static g_nCurObjectImageBPP:I

.field public static g_nCurObjectImageHeight:I

.field public static g_nCurObjectImageWidth:I

.field public static g_nCurrentBackgroundBPP:I

.field public static g_nCurrentBackgroundHeight:I

.field public static g_nCurrentBackgroundWidth:I

.field public static g_nNumTotalObject:I

.field public static g_nNumTotalPenPoint:I

.field public static g_nTextHeight:I

.field public static g_nTextWidth:I


# instance fields
.field public UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 273
    :try_start_0
    const-string v0, "AMSWiFiLibs-1.0.6"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    :goto_0
    return-void

    .line 275
    :catch_0
    move-exception v0

    .line 276
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    new-instance v0, Lcom/sec/amsoma/structure/AMS_UI_DATA;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_UI_DATA;-><init>()V

    iput-object v0, p0, Lcom/sec/amsoma/AMSLibs;->UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    return-void
.end method

.method public static makeBuffer([III)Ljava/nio/IntBuffer;
    .locals 1

    .prologue
    .line 528
    mul-int v0, p1, p2

    invoke-static {v0}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v0

    .line 529
    invoke-virtual {v0, p0}, Ljava/nio/IntBuffer;->put([I)Ljava/nio/IntBuffer;

    .line 530
    invoke-virtual {v0}, Ljava/nio/IntBuffer;->rewind()Ljava/nio/Buffer;

    .line 531
    return-object v0
.end method


# virtual methods
.method public Callback_deleteTempFile([B)V
    .locals 2

    .prologue
    .line 670
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    .line 671
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 672
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 675
    :cond_0
    if-eqz v1, :cond_1

    .line 678
    :cond_1
    return-void
.end method

.method public Callback_loadImageFileToRawImage([B)I
    .locals 1

    .prologue
    .line 666
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {p0, v0}, Lcom/sec/amsoma/AMSLibs;->loadImageFileToRawImage(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public Callback_loadImageStreamToRawImage([B[BI)I
    .locals 3

    .prologue
    .line 632
    invoke-virtual {p0, p1}, Lcom/sec/amsoma/AMSLibs;->Callback_deleteTempFile([B)V

    .line 636
    const/4 v1, 0x0

    .line 639
    :try_start_0
    new-instance v2, Ljava/io/File;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 640
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 641
    if-nez v0, :cond_0

    .line 643
    const/16 v0, 0x2b

    .line 662
    :goto_0
    return v0

    .line 645
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v0, p2, v1, p3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 650
    :goto_1
    if-eqz v0, :cond_1

    .line 651
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 659
    :cond_1
    :goto_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {p0, v0}, Lcom/sec/amsoma/AMSLibs;->loadImageFileToRawImage(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 646
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 647
    :goto_3
    :try_start_3
    const-string v1, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 655
    :catch_1
    move-exception v0

    .line 656
    const-string v0, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 646
    :catch_2
    move-exception v1

    goto :goto_3
.end method

.method public Callback_saveRawImageToImageFile([B[IIIIII)I
    .locals 8

    .prologue
    .line 535
    if-nez p2, :cond_1

    .line 536
    const/16 v0, 0x29

    .line 580
    :cond_0
    :goto_0
    return v0

    .line 539
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/amsoma/AMSLibs;->Callback_deleteTempFile([B)V

    .line 541
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p3, p4, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 542
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p2

    move v3, p3

    move v6, p3

    move v7, p4

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 544
    new-instance v3, Ljava/io/File;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 545
    const/4 v2, 0x0

    .line 548
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 549
    if-nez v1, :cond_2

    .line 551
    const/16 v0, 0x29

    .line 571
    if-eqz v1, :cond_0

    .line 572
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 575
    :catch_0
    move-exception v0

    .line 577
    const/16 v0, 0x29

    goto :goto_0

    .line 554
    :cond_2
    if-nez p7, :cond_4

    .line 555
    :try_start_2
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v0, v2, p6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 561
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 563
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 571
    if-eqz v1, :cond_3

    .line 572
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 580
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 556
    :cond_4
    const/4 v2, 0x1

    if-ne p7, v2, :cond_5

    .line 557
    :try_start_4
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v0, v2, p6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 564
    :catch_1
    move-exception v0

    .line 565
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 567
    const/16 v0, 0x29

    .line 571
    if-eqz v1, :cond_0

    .line 572
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    .line 575
    :catch_2
    move-exception v0

    .line 577
    const/16 v0, 0x29

    goto :goto_0

    .line 559
    :cond_5
    :try_start_7
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v0, v2, p6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 569
    :catchall_0
    move-exception v0

    .line 571
    :goto_3
    if-eqz v1, :cond_6

    .line 572
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    .line 577
    :cond_6
    throw v0

    .line 575
    :catch_3
    move-exception v0

    .line 577
    const/16 v0, 0x29

    goto :goto_0

    .line 575
    :catch_4
    move-exception v0

    .line 577
    const/16 v0, 0x29

    goto :goto_0

    .line 569
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 564
    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method public Callback_saveRawImageToJPEGFileWithLimitSize([B[IIIIII)I
    .locals 10

    .prologue
    .line 585
    .line 586
    const/4 v1, 0x0

    .line 587
    invoke-virtual {p0, p1}, Lcom/sec/amsoma/AMSLibs;->Callback_deleteTempFile([B)V

    move v9, v1

    move/from16 v7, p6

    .line 590
    :goto_0
    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v8}, Lcom/sec/amsoma/AMSLibs;->Callback_saveRawImageToImageFile([B[IIIIII)I

    move-result v1

    .line 591
    if-eqz v1, :cond_0

    .line 628
    :goto_1
    return v1

    .line 595
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 596
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 598
    if-eqz v2, :cond_1

    .line 599
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 602
    :cond_1
    if-eqz v1, :cond_7

    .line 603
    invoke-virtual {v1}, Ljava/io/File;->length()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    long-to-int v1, v1

    .line 607
    :goto_2
    move/from16 v0, p7

    if-ge v1, v0, :cond_3

    .line 608
    const/4 v2, 0x0

    .line 625
    :goto_3
    const/16 v3, 0x2b

    if-eq v2, v3, :cond_2

    if-nez v1, :cond_6

    :cond_2
    move v1, v2

    .line 626
    goto :goto_1

    .line 611
    :cond_3
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/sec/amsoma/AMSLibs;->Callback_deleteTempFile([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 612
    const/16 v2, 0x1e

    if-gt v7, v2, :cond_4

    .line 613
    add-int/lit8 v7, v7, -0x5

    .line 617
    :goto_4
    const/4 v2, 0x5

    if-ge v7, v2, :cond_5

    .line 618
    const/16 v2, 0x2b

    goto :goto_3

    .line 615
    :cond_4
    add-int/lit8 v7, v7, -0xa

    goto :goto_4

    :cond_5
    move v9, v1

    .line 623
    goto :goto_0

    .line 621
    :catch_0
    move-exception v1

    move v1, v9

    .line 622
    :goto_5
    const-string v2, "IOException"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/chaton/util/y;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v9, v1

    .line 623
    goto :goto_0

    :cond_6
    move v1, v2

    .line 628
    goto :goto_1

    .line 621
    :catch_1
    move-exception v2

    goto :goto_5

    :cond_7
    move v1, v9

    goto :goto_2
.end method

.method public Callback_writeData([BI[B)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 681
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 712
    :cond_0
    :goto_0
    return v0

    .line 686
    :cond_1
    const/4 v3, 0x0

    .line 688
    :try_start_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    .line 689
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v4, "rw"

    invoke-direct {v2, v1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 694
    if-ltz p2, :cond_2

    int-to-long v3, p2

    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-gez v1, :cond_2

    .line 695
    int-to-long v3, p2

    invoke-virtual {v2, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 696
    invoke-virtual {v2, p3}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 703
    :cond_2
    if-eqz v2, :cond_3

    .line 705
    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 712
    :cond_3
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 706
    :catch_0
    move-exception v0

    .line 707
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 699
    :catch_1
    move-exception v1

    move-object v2, v3

    .line 700
    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 703
    if-eqz v2, :cond_0

    .line 705
    :try_start_4
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 706
    :catch_2
    move-exception v1

    .line 707
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 703
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v3, :cond_4

    .line 705
    :try_start_5
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 708
    :cond_4
    :goto_4
    throw v0

    .line 706
    :catch_3
    move-exception v1

    .line 707
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 703
    :catchall_1
    move-exception v0

    move-object v3, v2

    goto :goto_3

    .line 699
    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method public Convert565(III)I
    .locals 2

    .prologue
    .line 303
    mul-int/lit8 v0, p1, 0x1f

    add-int/lit8 v0, v0, 0x7f

    div-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0xb

    mul-int/lit8 v1, p2, 0x3f

    add-int/lit8 v1, v1, 0x7f

    div-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x5

    or-int/2addr v0, v1

    mul-int/lit8 v1, p3, 0x1f

    add-int/lit8 v1, v1, 0x7f

    div-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public GetAnimationObjectInfo(II)V
    .locals 0

    .prologue
    .line 763
    sput p1, Lcom/sec/amsoma/AMSLibs;->g_nNumTotalPenPoint:I

    .line 764
    sput p2, Lcom/sec/amsoma/AMSLibs;->g_nNumTotalObject:I

    .line 765
    return-void
.end method

.method public GetBValue(I)I
    .locals 1

    .prologue
    .line 291
    shr-int/lit8 v0, p1, 0x0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public GetCurObjectImageBufInfo(III)V
    .locals 0

    .prologue
    .line 751
    sput p1, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageWidth:I

    .line 752
    sput p2, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageHeight:I

    .line 753
    sput p3, Lcom/sec/amsoma/AMSLibs;->g_nCurObjectImageBPP:I

    .line 754
    return-void
.end method

.method public GetCurrentBackgroundBufInfo(III)V
    .locals 0

    .prologue
    .line 757
    sput p1, Lcom/sec/amsoma/AMSLibs;->g_nCurrentBackgroundWidth:I

    .line 758
    sput p2, Lcom/sec/amsoma/AMSLibs;->g_nCurrentBackgroundHeight:I

    .line 759
    sput p3, Lcom/sec/amsoma/AMSLibs;->g_nCurrentBackgroundBPP:I

    .line 760
    return-void
.end method

.method public GetGValue(I)I
    .locals 1

    .prologue
    .line 295
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public GetRValue(I)I
    .locals 1

    .prologue
    .line 299
    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public NewEncoding(ILcom/sec/amsoma/structure/AMS_UI_DATA;)Z
    .locals 2

    .prologue
    .line 308
    iput-object p2, p0, Lcom/sec/amsoma/AMSLibs;->UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    .line 309
    new-instance v0, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;

    invoke-direct {v0}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;-><init>()V

    .line 312
    iget-object v1, p0, Lcom/sec/amsoma/AMSLibs;->UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget-byte v1, v1, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_cObjectSize:B

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_cSize(B)V

    .line 313
    iget-object v1, p0, Lcom/sec/amsoma/AMSLibs;->UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget-byte v1, v1, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_cObjectStyle:B

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_cStyle(I)V

    .line 315
    iget-object v1, p0, Lcom/sec/amsoma/AMSLibs;->UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v1, v1, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rgbPen:I

    invoke-virtual {p0, v1}, Lcom/sec/amsoma/AMSLibs;->GetRValue(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_nColorR(I)V

    .line 316
    iget-object v1, p0, Lcom/sec/amsoma/AMSLibs;->UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v1, v1, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rgbPen:I

    invoke-virtual {p0, v1}, Lcom/sec/amsoma/AMSLibs;->GetGValue(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_nColorG(I)V

    .line 317
    iget-object v1, p0, Lcom/sec/amsoma/AMSLibs;->UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget v1, v1, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rgbPen:I

    invoke-virtual {p0, v1}, Lcom/sec/amsoma/AMSLibs;->GetBValue(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;->setM_nColorB(I)V

    .line 319
    iget-object v1, p0, Lcom/sec/amsoma/AMSLibs;->UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    iget-object v1, v1, Lcom/sec/amsoma/structure/AMS_UI_DATA;->m_rectRegion:Lcom/sec/amsoma/structure/AMS_RECT;

    invoke-virtual {p0, p1, v1, v0}, Lcom/sec/amsoma/AMSLibs;->VipAMS_TotalNewEncoding(ILcom/sec/amsoma/structure/AMS_RECT;Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;)Z

    move-result v0

    return v0
.end method

.method public native VipAMS_AddFrame(I)Z
.end method

.method public native VipAMS_AddObjectImage(IB[IIIIS)Z
.end method

.method public native VipAMS_AnimationEnd(I)Z
.end method

.method public native VipAMS_AnimationNext(I)Z
.end method

.method public native VipAMS_AnimationStart(I)Z
.end method

.method public native VipAMS_Blur([IIII)Z
.end method

.method public native VipAMS_Bright([III)Z
.end method

.method public native VipAMS_CancelSelect(I)Z
.end method

.method public native VipAMS_ChangeSelectDiagram(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;Lcom/sec/amsoma/structure/AMS_RECT;B)Z
.end method

.method public native VipAMS_ChangeSelectStamp(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;Lcom/sec/amsoma/structure/AMS_RECT;III)Z
.end method

.method public native VipAMS_ChangeSelectText(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;Lcom/sec/amsoma/structure/AMS_RECT;B[CS[CS)Z
.end method

.method public native VipAMS_ClearEncoding(I)Z
.end method

.method public native VipAMS_Close(I)V
.end method

.method public native VipAMS_Dark([III)Z
.end method

.method public native VipAMS_DeleteSelectObject(ILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;)Z
.end method

.method public native VipAMS_EncodeNewDiagram(ILcom/sec/amsoma/structure/AMS_RECT;B)Z
.end method

.method public native VipAMS_EncodeNewMultiImage(ILcom/sec/amsoma/structure/AMS_RECT;BB)I
.end method

.method public native VipAMS_EncodeNewStamp(ILcom/sec/amsoma/structure/AMS_RECT;BBB[IIII)Z
.end method

.method public native VipAMS_EncodeNewText(ILcom/sec/amsoma/structure/AMS_RECT;B[CS[CS)Z
.end method

.method public native VipAMS_EncodePointData(ISSS)Z
.end method

.method public native VipAMS_EncodeStrokeEnd(I)Z
.end method

.method public native VipAMS_EncodeStrokeStart(I)Z
.end method

.method public native VipAMS_GetAnimationInfo(I)Z
.end method

.method public native VipAMS_GetBGAudioFile(I)[B
.end method

.method public native VipAMS_GetBGAudioStyle(I)I
.end method

.method public native VipAMS_GetBGMIndex(I)I
.end method

.method public native VipAMS_GetBackgroundColorA(I)I
.end method

.method public native VipAMS_GetBackgroundColorB(I)I
.end method

.method public native VipAMS_GetBackgroundColorG(I)I
.end method

.method public native VipAMS_GetBackgroundColorR(I)I
.end method

.method public native VipAMS_GetBackgroundIndex(I)B
.end method

.method public native VipAMS_GetBackgroundStyle(I)I
.end method

.method public native VipAMS_GetCodingHeight(I)I
.end method

.method public native VipAMS_GetCodingWidth(I)I
.end method

.method public native VipAMS_GetCurObjectColorA(I)I
.end method

.method public native VipAMS_GetCurObjectColorB(I)I
.end method

.method public native VipAMS_GetCurObjectColorG(I)I
.end method

.method public native VipAMS_GetCurObjectColorR(I)I
.end method

.method public native VipAMS_GetCurObjectSize(I)B
.end method

.method public native VipAMS_GetCurObjectStyle(I)B
.end method

.method public native VipAMS_GetCurObjectType(I)B
.end method

.method public native VipAMS_GetCurrentBackgroundBuf(I)[I
.end method

.method public native VipAMS_GetCurrentFrame(I)I
.end method

.method public native VipAMS_GetErrorCode(I)I
.end method

.method public native VipAMS_GetFileStreamInfo([BI)V
.end method

.method public native VipAMS_GetFreeSpace(I)I
.end method

.method public native VipAMS_GetImageInfo([IIII)Z
.end method

.method public native VipAMS_GetLastFrame(I)I
.end method

.method public native VipAMS_GetLibVersion(I)Ljava/lang/String;
.end method

.method public native VipAMS_GetNextDrawingData(IILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I
.end method

.method public native VipAMS_GetNextUpdateObject(IILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I
.end method

.method public native VipAMS_GetNextUpdateRegion(IILcom/sec/amsoma/structure/AMS_RECT;)I
.end method

.method public VipAMS_GetObjectImageBuf(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)[I
    .locals 1

    .prologue
    .line 363
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetObjectImageBuf(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;B)[I

    move-result-object v0

    return-object v0
.end method

.method public native VipAMS_GetObjectImageBuf(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;B)[I
.end method

.method public native VipAMS_GetObjectImageDuration(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;B)I
.end method

.method public native VipAMS_GetObjectImageNum(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I
.end method

.method public native VipAMS_GetObjectIteration(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)I
.end method

.method public native VipAMS_GetSelectObjectData(ILcom/sec/amsoma/structure/AMS_OBJECT_DATA;)V
.end method

.method public native VipAMS_GetTotalObjectNum(I)I
.end method

.method public native VipAMS_GetTurnPageOption(I)B
.end method

.method public native VipAMS_GetUIImageBuffer([III)Z
.end method

.method public native VipAMS_Gray([III)Z
.end method

.method public native VipAMS_Init()V
.end method

.method public native VipAMS_IsAMSJPEGFile(Lcom/sec/amsoma/structure/AMS_UI_DATA;[Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;)I
.end method

.method public native VipAMS_IsAnimationEnable(I)Z
.end method

.method public native VipAMS_IsEditEnable(I)Z
.end method

.method public native VipAMS_IsEnableRedo(I)Z
.end method

.method public native VipAMS_IsEnableUndo(I)Z
.end method

.method public native VipAMS_Negative([III)Z
.end method

.method public native VipAMS_Open(Lcom/sec/amsoma/structure/AMS_OPTION;)I
.end method

.method public native VipAMS_PencilSketch([III)Z
.end method

.method public native VipAMS_Redo(I)I
.end method

.method public native VipAMS_RemoveFrame(IB)Z
.end method

.method public native VipAMS_SelectObject(IIILcom/sec/amsoma/structure/AMS_SELECT_OBJECT;I)Z
.end method

.method public native VipAMS_Sepia([III)Z
.end method

.method public native VipAMS_SetAMSImageBuf(I[III)I
.end method

.method public native VipAMS_SetBGAudioFile(I[BI)Z
.end method

.method public native VipAMS_SetBGAudioStyle(II)Z
.end method

.method public native VipAMS_SetBGMIndex(II)Z
.end method

.method public native VipAMS_SetBackgroundColor(IIII)Z
.end method

.method public native VipAMS_SetBackgroundIndex(IB)Z
.end method

.method public native VipAMS_SetBackgroundRawImage(I[IIII)Z
.end method

.method public native VipAMS_SetCurObjectColor(IIII)Z
.end method

.method public native VipAMS_SetCurObjectSize(IB)Z
.end method

.method public native VipAMS_SetCurObjectStyle(IB)Z
.end method

.method public native VipAMS_SetCurObjectType(IB)Z
.end method

.method public native VipAMS_SetCurrentFrame(IB)Z
.end method

.method public native VipAMS_SetEditEnable(IZ)Z
.end method

.method public native VipAMS_SetNextFrame(I)Z
.end method

.method public native VipAMS_SetPrevFrame(I)Z
.end method

.method public native VipAMS_SetTempFilePath([BI)Z
.end method

.method public native VipAMS_SetTurnPageOption(IB)Z
.end method

.method public native VipAMS_TotalEndEncoding(I[Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;)I
.end method

.method public native VipAMS_TotalLoadEncodingFile(I[Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;Lcom/sec/amsoma/structure/AMS_RECT;)I
.end method

.method public native VipAMS_TotalNewEncoding(ILcom/sec/amsoma/structure/AMS_RECT;Lcom/sec/amsoma/structure/AMS_OBJECT_DATA;)Z
.end method

.method public native VipAMS_TotalSaveEncoding(I[Lcom/sec/amsoma/structure/AMS_CODING_FILE_INFO;)I
.end method

.method public native VipAMS_Undo(I)I
.end method

.method public getUIData()Lcom/sec/amsoma/structure/AMS_UI_DATA;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/amsoma/AMSLibs;->UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    return-object v0
.end method

.method public loadImageFileToRawImage(Ljava/lang/String;)I
    .locals 10

    .prologue
    const/16 v8, 0x29

    const/4 v2, 0x0

    .line 716
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 717
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 718
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 719
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 720
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 721
    const/16 v9, 0x20

    .line 725
    mul-int v1, v3, v7

    :try_start_0
    new-array v1, v1, [I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 731
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 732
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 735
    if-nez v1, :cond_1

    move v2, v8

    .line 747
    :cond_0
    :goto_0
    return v2

    .line 726
    :catch_0
    move-exception v1

    .line 727
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 729
    const/16 v2, 0x2d

    goto :goto_0

    .line 739
    :cond_1
    invoke-virtual {p0, v1, v3, v7, v9}, Lcom/sec/amsoma/AMSLibs;->VipAMS_GetImageInfo([IIII)Z

    move-result v0

    .line 741
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 743
    if-nez v0, :cond_0

    move v2, v8

    .line 744
    goto :goto_0
.end method

.method public setUIData(Lcom/sec/amsoma/structure/AMS_UI_DATA;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/sec/amsoma/AMSLibs;->UIData:Lcom/sec/amsoma/structure/AMS_UI_DATA;

    .line 287
    return-void
.end method
