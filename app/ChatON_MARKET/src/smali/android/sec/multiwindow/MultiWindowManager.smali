.class public Landroid/sec/multiwindow/MultiWindowManager;
.super Landroid/sec/multiwindow/MultiWindowImpl;
.source "MultiWindowManager.java"

# interfaces
.implements Landroid/sec/multiwindow/IMultiWindowManager;


# static fields
.field public static final ARRANGE_MODE_CASCADE:I

.field public static final ARRANGE_MODE_SPLIT:I

.field public static final FLAG_STYLE_FREE:I

.field public static final FLAG_STYLE_MINIMIZE:I

.field public static final FLAG_STYLE_PINUP:I

.field public static final FLAG_STYLE_POPUP:I

.field private static final MAX_RUNNING_TASK:I = 0x3e8

.field public static final TAG:Ljava/lang/String; = "MultiWindowManager"

.field public static final ZONE_A:I

.field public static final ZONE_B:I

.field public static final ZONE_MASK:I

.field public static final ZONE_UNKNOWN:I

.field private static mSelf:Landroid/sec/multiwindow/MultiWindowManager;

.field private static sIsMultiWindowEnabled:Z

.field private static sIsMultiWindowPhoneStyle:Z

.field private static sQueried:Z

.field private static sStyleQueried:Z


# instance fields
.field private mSupportAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    sput v0, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_FREE:I

    .line 36
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    sput v0, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_PINUP:I

    .line 40
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    sput v0, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_MINIMIZE:I

    .line 44
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_NO_DECORATION:I

    sput v0, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_POPUP:I

    .line 49
    sget v0, Landroid/sec/multiwindow/Constants$Configuration;->ARRANGE_SPLITED:I

    sput v0, Landroid/sec/multiwindow/MultiWindowManager;->ARRANGE_MODE_SPLIT:I

    .line 54
    sget v0, Landroid/sec/multiwindow/Constants$Configuration;->ARRANGE_CASCADE:I

    sput v0, Landroid/sec/multiwindow/MultiWindowManager;->ARRANGE_MODE_CASCADE:I

    .line 59
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

    sput v0, Landroid/sec/multiwindow/MultiWindowManager;->ZONE_MASK:I

    .line 64
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_A:I

    sput v0, Landroid/sec/multiwindow/MultiWindowManager;->ZONE_A:I

    .line 68
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_B:I

    sput v0, Landroid/sec/multiwindow/MultiWindowManager;->ZONE_B:I

    .line 73
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_UNKNOWN:I

    sput v0, Landroid/sec/multiwindow/MultiWindowManager;->ZONE_UNKNOWN:I

    .line 78
    const/4 v0, 0x0

    sput-object v0, Landroid/sec/multiwindow/MultiWindowManager;->mSelf:Landroid/sec/multiwindow/MultiWindowManager;

    .line 80
    sput-boolean v1, Landroid/sec/multiwindow/MultiWindowManager;->sQueried:Z

    .line 82
    sput-boolean v1, Landroid/sec/multiwindow/MultiWindowManager;->sIsMultiWindowEnabled:Z

    .line 84
    sput-boolean v1, Landroid/sec/multiwindow/MultiWindowManager;->sStyleQueried:Z

    .line 86
    sput-boolean v1, Landroid/sec/multiwindow/MultiWindowManager;->sIsMultiWindowPhoneStyle:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 113
    invoke-direct {p0, p1}, Landroid/sec/multiwindow/MultiWindowImpl;-><init>(Landroid/content/Context;)V

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/sec/multiwindow/MultiWindowManager;->mSupportAppList:Ljava/util/ArrayList;

    .line 115
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 117
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 119
    const-string v3, "getFrontActivityPosition"

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Class;

    invoke-virtual {p0, v2, v0, v3, v1}, Landroid/sec/multiwindow/MultiWindowManager;->putMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 122
    invoke-static {p1}, Landroid/sec/multiwindow/MultiWindowManager;->isMultiWindowPhoneStyle(Landroid/content/Context;)Z

    .line 123
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindowManager;->loadDefaultConfig()V

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 302
    invoke-direct {p0, p1}, Landroid/sec/multiwindow/MultiWindowManager;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/sec/multiwindow/IMultiWindowManager;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 306
    const/4 v0, 0x0

    return-object v0
.end method

.method private getMultiWindowInfos(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 304
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getSelf(Landroid/content/Context;)Landroid/sec/multiwindow/MultiWindowManager;
    .locals 1

    .prologue
    .line 98
    if-nez p0, :cond_0

    .line 99
    const/4 v0, 0x0

    .line 104
    :goto_0
    return-object v0

    .line 101
    :cond_0
    sget-object v0, Landroid/sec/multiwindow/MultiWindowManager;->mSelf:Landroid/sec/multiwindow/MultiWindowManager;

    if-nez v0, :cond_1

    .line 102
    new-instance v0, Landroid/sec/multiwindow/MultiWindowManager;

    invoke-direct {v0, p0}, Landroid/sec/multiwindow/MultiWindowManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Landroid/sec/multiwindow/MultiWindowManager;->mSelf:Landroid/sec/multiwindow/MultiWindowManager;

    .line 104
    :cond_1
    sget-object v0, Landroid/sec/multiwindow/MultiWindowManager;->mSelf:Landroid/sec/multiwindow/MultiWindowManager;

    goto :goto_0
.end method

.method public static getService()Landroid/sec/multiwindow/IMultiWindowService;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 308
    const/4 v0, 0x0

    return-object v0
.end method

.method public static isMultiWindowPhoneStyle(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 281
    if-nez p0, :cond_0

    .line 282
    const/4 v0, 0x0

    .line 291
    :goto_0
    return v0

    .line 284
    :cond_0
    sget-boolean v0, Landroid/sec/multiwindow/MultiWindowManager;->sStyleQueried:Z

    if-nez v0, :cond_1

    .line 285
    const/4 v0, 0x1

    sput-boolean v0, Landroid/sec/multiwindow/MultiWindowManager;->sStyleQueried:Z

    .line 286
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 287
    if-eqz v0, :cond_1

    .line 288
    sget-object v1, Landroid/sec/multiwindow/Constants$PackageManager;->FEATURE_MULTIWINDOW_PHONE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Landroid/sec/multiwindow/MultiWindowManager;->sIsMultiWindowPhoneStyle:Z

    .line 291
    :cond_1
    sget-boolean v0, Landroid/sec/multiwindow/MultiWindowManager;->sIsMultiWindowPhoneStyle:Z

    goto :goto_0
.end method

.method public static isMultiWindowServiceEnabled(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 263
    if-nez p0, :cond_0

    .line 264
    const/4 v0, 0x0

    .line 273
    :goto_0
    return v0

    .line 266
    :cond_0
    sget-boolean v0, Landroid/sec/multiwindow/MultiWindowManager;->sQueried:Z

    if-nez v0, :cond_1

    .line 267
    const/4 v0, 0x1

    sput-boolean v0, Landroid/sec/multiwindow/MultiWindowManager;->sQueried:Z

    .line 268
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_1

    .line 270
    sget-object v1, Landroid/sec/multiwindow/Constants$PackageManager;->FEATURE_MULTIWINDOW:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Landroid/sec/multiwindow/MultiWindowManager;->sIsMultiWindowEnabled:Z

    .line 273
    :cond_1
    sget-boolean v0, Landroid/sec/multiwindow/MultiWindowManager;->sIsMultiWindowEnabled:Z

    goto :goto_0
.end method


# virtual methods
.method public arrange(I)V
    .locals 2

    .prologue
    .line 166
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Landroid/sec/multiwindow/Constants$Intent;->ACTION_ARRANGE_WINDOWS:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    sget-object v1, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_ARRANGE_MODE:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 168
    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 169
    return-void
.end method

.method public arrangeCascade()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 184
    sget v0, Landroid/sec/multiwindow/Constants$Configuration;->ARRANGE_CASCADE:I

    invoke-virtual {p0, v0}, Landroid/sec/multiwindow/MultiWindowManager;->arrange(I)V

    .line 185
    return-void
.end method

.method public arrangeSplit()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 176
    sget v0, Landroid/sec/multiwindow/Constants$Configuration;->ARRANGE_SPLITED:I

    invoke-virtual {p0, v0}, Landroid/sec/multiwindow/MultiWindowManager;->arrange(I)V

    .line 177
    return-void
.end method

.method public arrangeTemplate(I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public arrangeWindows(I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 429
    const/4 v0, 0x0

    return v0
.end method

.method public capture(Landroid/content/ComponentName;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 389
    const/4 v0, 0x0

    return-object v0
.end method

.method public exchangeSplitScreens()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 421
    const/4 v0, 0x0

    return v0
.end method

.method public finishAllWindow()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 379
    const/4 v0, 0x0

    return v0
.end method

.method public finishAppPid(I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 381
    const/4 v0, 0x0

    return v0
.end method

.method public getFrontActivityPosition()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 127
    const-string v0, "getFrontActivityPosition"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Landroid/sec/multiwindow/MultiWindowManager;->invoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    return-object v0
.end method

.method public getMultiLayout()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 425
    const/4 v0, 0x1

    return v0
.end method

.method public getMultiWindowInfos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 315
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMultiWindowList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 347
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMultiWindowList(Landroid/content/ComponentName;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ComponentName;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 349
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMultiWindowListOwnerList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 359
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMultiWindowMode()Landroid/sec/multiwindow/MultiWindowType;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 323
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPinupWindow()Landroid/content/ComponentName;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 377
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPinupWindowList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 355
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getRegisteredList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ActivityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 216
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 218
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 219
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 220
    sget-object v3, Landroid/sec/multiwindow/Constants$Intent;->CATEGORY_MULTIWINDOW_LAUNCHER:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 222
    iget-object v3, p0, Landroid/sec/multiwindow/MultiWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 223
    const/16 v4, 0x80

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 225
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 226
    if-eqz v0, :cond_0

    .line 227
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 232
    :cond_1
    sget-boolean v0, Landroid/sec/multiwindow/MultiWindowManager;->sIsMultiWindowPhoneStyle:Z

    if-eqz v0, :cond_3

    .line 233
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 234
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 236
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 237
    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 238
    iget-object v4, p0, Landroid/sec/multiwindow/MultiWindowManager;->mSupportAppList:Ljava/util/ArrayList;

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 239
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 245
    :cond_3
    return-object v1
.end method

.method public getResumedWindow()Landroid/content/ComponentName;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 375
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRunningList(I)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindowManager;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 255
    const/16 v1, 0x3e8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/app/ActivityManager;->getRunningTasks(IILandroid/app/IThumbnailReceiver;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRunningWindowList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 351
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getSplitList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 357
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getVisibleWindowList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 353
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getWindowInstance(Landroid/content/ComponentName;)Landroid/sec/multiwindow/IMultiWindow;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 325
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWindowInstance(Landroid/content/Intent;)Landroid/sec/multiwindow/IMultiWindow;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 327
    const/4 v0, 0x0

    return-object v0
.end method

.method public isMultiWindow(Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 363
    const/4 v0, 0x0

    return v0
.end method

.method public isMultiWindow(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 365
    const/4 v0, 0x0

    return v0
.end method

.method public isPermissionOff()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 427
    const/4 v0, 0x0

    return v0
.end method

.method public isPinup(Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 371
    const/4 v0, 0x0

    return v0
.end method

.method public isPinup(Landroid/content/ComponentName;I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 373
    const/4 v0, 0x0

    return v0
.end method

.method public isRunning(Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 367
    const/4 v0, 0x0

    return v0
.end method

.method public isVisible(Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 369
    const/4 v0, 0x0

    return v0
.end method

.method public loadDefaultConfig()V
    .locals 5

    .prologue
    .line 295
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x107003f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 296
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 297
    iget-object v4, p0, Landroid/sec/multiwindow/MultiWindowManager;->mSupportAppList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 299
    :cond_0
    return-void
.end method

.method public makeIntent(ILandroid/graphics/Rect;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 138
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 139
    sget v1, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_FREE:I

    sget v2, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_PINUP:I

    or-int/2addr v1, v2

    sget v2, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_MINIMIZE:I

    or-int/2addr v1, v2

    sget v2, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_POPUP:I

    or-int/2addr v1, v2

    sget v2, Landroid/sec/multiwindow/MultiWindowManager;->ZONE_A:I

    or-int/2addr v1, v2

    sget v2, Landroid/sec/multiwindow/MultiWindowManager;->ZONE_B:I

    or-int/2addr v1, v2

    sget v2, Landroid/sec/multiwindow/MultiWindowManager;->ZONE_UNKNOWN:I

    or-int/2addr v1, v2

    xor-int/lit8 v1, v1, -0x1

    .line 142
    and-int/2addr v1, p1

    if-nez v1, :cond_1

    .line 144
    sget v1, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_FREE:I

    and-int/2addr v1, p1

    if-eqz v1, :cond_1

    .line 145
    sget-boolean v1, Landroid/sec/multiwindow/MultiWindowManager;->sIsMultiWindowPhoneStyle:Z

    if-eqz v1, :cond_0

    .line 146
    sget v1, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_PINUP:I

    sget v2, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_MINIMIZE:I

    or-int/2addr v1, v2

    sget v2, Landroid/sec/multiwindow/MultiWindowManager;->FLAG_STYLE_POPUP:I

    or-int/2addr v1, v2

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr p1, v1

    .line 148
    :cond_0
    sget-object v1, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_WINDOW_MODE:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 152
    :cond_1
    sget-boolean v1, Landroid/sec/multiwindow/MultiWindowManager;->sIsMultiWindowPhoneStyle:Z

    if-nez v1, :cond_2

    .line 153
    if-eqz p2, :cond_2

    .line 154
    sget-object v1, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_WINDOW_POSITION:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 158
    :cond_2
    return-object v0
.end method

.method public minimizeAll()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 192
    sget v0, Landroid/sec/multiwindow/Constants$Configuration;->ARRANGE_MINIMIZED_ALL:I

    invoke-virtual {p0, v0}, Landroid/sec/multiwindow/MultiWindowManager;->arrange(I)V

    .line 193
    return-void
.end method

.method public notifyFinished(Landroid/app/Activity;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 385
    const/4 v0, 0x0

    return v0
.end method

.method public notifyForceFinished(Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 387
    const/4 v0, 0x0

    return v0
.end method

.method public notifySIP(I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 433
    const/4 v0, 0x1

    return v0
.end method

.method public notifyStarted(Landroid/app/Activity;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 383
    const/4 v0, 0x0

    return v0
.end method

.method public orientationChangeRestore(Landroid/content/ComponentName;I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 431
    const/4 v0, 0x0

    return v0
.end method

.method public postResume(Landroid/app/Activity;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 391
    const/4 v0, 0x0

    return v0
.end method

.method public registerMultiWindow(Landroid/content/ComponentName;)Landroid/sec/multiwindow/IMultiWindow;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 331
    const/4 v0, 0x0

    return-object v0
.end method

.method public registerMultiWindow(Landroid/content/ComponentName;II)Landroid/sec/multiwindow/IMultiWindow;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 333
    const/4 v0, 0x0

    return-object v0
.end method

.method public registerMultiWindow(Landroid/content/ComponentName;IIZ)Landroid/sec/multiwindow/IMultiWindow;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 335
    const/4 v0, 0x0

    return-object v0
.end method

.method public registerMultiWindow(Landroid/content/ComponentName;Ljava/util/List;)Landroid/sec/multiwindow/IMultiWindow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ComponentName;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Point;",
            ">;)",
            "Landroid/sec/multiwindow/IMultiWindow;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 337
    const/4 v0, 0x0

    return-object v0
.end method

.method public registerMultiWindowPair(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 343
    const/4 v0, 0x0

    return v0
.end method

.method public removeMultiWindow(Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 341
    const/4 v0, 0x0

    return v0
.end method

.method public removeMultiWindowOwnerItem(Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 361
    const/4 v0, 0x0

    return v0
.end method

.method public removeMultiWindowPair(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 345
    const/4 v0, 0x0

    return v0
.end method

.method public restoreAll()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 200
    sget v0, Landroid/sec/multiwindow/Constants$Configuration;->ARRANGE_RESTORED_ALL:I

    invoke-virtual {p0, v0}, Landroid/sec/multiwindow/MultiWindowManager;->arrange(I)V

    .line 201
    return-void
.end method

.method public screenShot(Landroid/content/ComponentName;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 329
    const/4 v0, 0x0

    return-object v0
.end method

.method public sendCreateAction(Landroid/app/Activity;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 393
    const/4 v0, 0x0

    return v0
.end method

.method public sendDestroyAction(Landroid/app/Activity;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 403
    const/4 v0, 0x0

    return v0
.end method

.method public sendFinishAction(Landroid/app/Activity;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 401
    const/4 v0, 0x0

    return v0
.end method

.method public sendPauseAction(Landroid/app/Activity;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 399
    const/4 v0, 0x0

    return v0
.end method

.method public sendResumeAction(Landroid/app/Activity;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 397
    const/4 v0, 0x0

    return v0
.end method

.method public sendStartAction(Landroid/app/Activity;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 395
    const/4 v0, 0x0

    return v0
.end method

.method public setMultiLayout(Z)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 423
    const/4 v0, 0x1

    return v0
.end method

.method public setMultiWindowMode(Landroid/sec/multiwindow/MultiWindowType;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 321
    const/4 v0, 0x0

    return v0
.end method

.method public setPreferenceValue(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 435
    return-void
.end method

.method public showSwitchWindowGuideDialog(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 437
    return-void
.end method

.method public startLauncher()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 405
    const/4 v0, 0x0

    return v0
.end method

.method public startLauncher(Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 407
    const/4 v0, 0x0

    return v0
.end method

.method public startLauncher(Landroid/content/Context;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 409
    const/4 v0, 0x0

    return v0
.end method

.method public startLauncher(Landroid/content/Context;Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 411
    const/4 v0, 0x0

    return v0
.end method

.method public startSplitScreens(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 413
    const/4 v0, 0x0

    return v0
.end method

.method public startSplitScreens(Landroid/content/ComponentName;Landroid/content/Intent;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 415
    const/4 v0, 0x0

    return v0
.end method

.method public startSplitScreens(Landroid/content/Intent;Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 417
    const/4 v0, 0x0

    return v0
.end method

.method public startSplitScreensForLauncher(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 419
    const/4 v0, 0x0

    return v0
.end method

.method public unRegisterMultiWindow(Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 339
    const/4 v0, 0x0

    return v0
.end method
