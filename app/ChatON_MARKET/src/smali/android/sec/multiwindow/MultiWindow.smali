.class public Landroid/sec/multiwindow/MultiWindow;
.super Landroid/sec/multiwindow/MultiWindowImpl;
.source "MultiWindow.java"

# interfaces
.implements Landroid/sec/multiwindow/IMultiWindow;


# static fields
.field protected static final TAG:Ljava/lang/String; = "MultiWindow"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mArrangeable:Z

.field private mDefaultSize:Landroid/graphics/Rect;

.field private mMaximumSize:Landroid/graphics/Rect;

.field private mMinimumSize:Landroid/graphics/Rect;

.field private mWindowMode:I


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 123
    invoke-direct {p0, p1}, Landroid/sec/multiwindow/MultiWindowImpl;-><init>(Landroid/content/Context;)V

    .line 32
    iput-boolean v7, p0, Landroid/sec/multiwindow/MultiWindow;->mArrangeable:Z

    .line 125
    iput-object p1, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    .line 126
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 128
    iget-object v3, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    const-string v4, "getWindowMode"

    move-object v0, v1

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {p0, v2, v3, v4, v0}, Landroid/sec/multiwindow/MultiWindow;->putMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 129
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    const-string v3, "setWindowMode"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v6

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v7

    invoke-virtual {p0, v2, v0, v3, v4}, Landroid/sec/multiwindow/MultiWindow;->putMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 132
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    const-string v3, "getWindowInfo"

    check-cast v1, [Ljava/lang/Class;

    invoke-virtual {p0, v2, v0, v3, v1}, Landroid/sec/multiwindow/MultiWindow;->putMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 133
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    const-string v1, "setWindowInfo"

    new-array v3, v7, [Ljava/lang/Class;

    const-class v4, Landroid/os/Bundle;

    aput-object v4, v3, v6

    invoke-virtual {p0, v2, v0, v1, v3}, Landroid/sec/multiwindow/MultiWindow;->putMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 137
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getWindowInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 138
    sget-object v0, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_WINDOW_DEFAULT_SIZE:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mDefaultSize:Landroid/graphics/Rect;

    .line 140
    sget-object v0, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_WINDOW_MINIMUM_SIZE:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mMinimumSize:Landroid/graphics/Rect;

    .line 143
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 144
    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 145
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v6, v6, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mMaximumSize:Landroid/graphics/Rect;

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/sec/multiwindow/IMultiWindowService;Landroid/content/ComponentName;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 486
    invoke-direct {p0, p1}, Landroid/sec/multiwindow/MultiWindowImpl;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/sec/multiwindow/MultiWindow;->mArrangeable:Z

    .line 486
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/sec/multiwindow/IMultiWindowService;Landroid/content/Intent;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 484
    invoke-direct {p0, p1}, Landroid/sec/multiwindow/MultiWindowImpl;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/sec/multiwindow/MultiWindow;->mArrangeable:Z

    .line 484
    return-void
.end method

.method private checkMode(I)Z
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-virtual {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->mode(I)I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkOption(I)Z
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-virtual {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->option(I)I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createInstance(Landroid/app/Activity;)Landroid/sec/multiwindow/MultiWindow;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 108
    if-nez p0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-object v0

    .line 111
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v2, Landroid/sec/multiwindow/Constants$PackageManager;->FEATURE_MULTIWINDOW:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    new-instance v0, Landroid/sec/multiwindow/MultiWindow;

    invoke-direct {v0, p0}, Landroid/sec/multiwindow/MultiWindow;-><init>(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private getLastSize()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getWindowInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 55
    sget-object v1, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_WINDOW_LAST_SIZE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 57
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mDefaultSize:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method private getWindowInfo()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 73
    const-string v1, "getWindowInfo"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0}, Landroid/sec/multiwindow/MultiWindow;->invoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method

.method protected static isEnabledComponentName(Landroid/content/Context;Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 528
    const/4 v0, 0x1

    return v0
.end method

.method private setLastSize(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getWindowInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 48
    sget-object v1, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_WINDOW_LAST_SIZE:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 50
    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowInfo(Landroid/os/Bundle;)V

    .line 51
    return-void
.end method

.method private setWindowInfo(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 69
    const-string v0, "setWindowInfo"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/sec/multiwindow/MultiWindow;->invoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    return-void
.end method

.method private setWindowMode(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 61
    const-string v0, "setWindowMode"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/sec/multiwindow/MultiWindow;->invoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    return-void
.end method

.method private setWindowParams()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 77
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 79
    iget v1, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-virtual {p0, v1}, Landroid/sec/multiwindow/MultiWindow;->mode(I)I

    move-result v1

    sget v2, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    if-ne v1, v2, :cond_0

    .line 80
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getLastSize()Landroid/graphics/Rect;

    move-result-object v1

    .line 81
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 82
    iget v2, v1, Landroid/graphics/Rect;->top:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 83
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 84
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 85
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 94
    :goto_0
    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 95
    return-void

    .line 87
    :cond_0
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 88
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 89
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 90
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 91
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    goto :goto_0
.end method

.method private updateWindowMode()V
    .locals 2

    .prologue
    .line 65
    const-string v1, "getWindowMode"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0}, Landroid/sec/multiwindow/MultiWindow;->invoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 66
    return-void
.end method


# virtual methods
.method public finish()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 492
    const/4 v0, 0x0

    return v0
.end method

.method public finish(I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 490
    const/4 v0, 0x0

    return v0
.end method

.method public fitToHalf(I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 522
    const/4 v0, 0x0

    return v0
.end method

.method public getArrangeable()Z
    .locals 1

    .prologue
    .line 456
    iget-boolean v0, p0, Landroid/sec/multiwindow/MultiWindow;->mArrangeable:Z

    return v0
.end method

.method public getHeight()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 516
    const/4 v0, -0x1

    return v0
.end method

.method public getMinimumSize()Landroid/graphics/Point;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 520
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public getMultiWindowEnabled()Z
    .locals 1

    .prologue
    .line 431
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 432
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_RESIZE:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkOption(I)Z

    move-result v0

    return v0
.end method

.method public getRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 388
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 390
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getLastSize()Landroid/graphics/Rect;

    move-result-object v0

    .line 393
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mMaximumSize:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public getRect(I)Landroid/graphics/Rect;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 512
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    return-object v0
.end method

.method public getWidth()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 514
    const/4 v0, -0x1

    return v0
.end method

.method public getZone()I
    .locals 2

    .prologue
    .line 464
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 465
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

    and-int/2addr v0, v1

    return v0
.end method

.method public isMaximized()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 504
    const/4 v0, 0x0

    return v0
.end method

.method public isMaximized(I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 506
    const/4 v0, 0x0

    return v0
.end method

.method public isMinimized()Z
    .locals 1

    .prologue
    .line 189
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 190
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkOption(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMultiWindow()Z
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 167
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkMode(I)Z

    move-result v0

    return v0
.end method

.method public isNormalWindow()Z
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 156
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_NORMAL:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkMode(I)Z

    move-result v0

    return v0
.end method

.method public isPinup()Z
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 178
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkOption(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStartedAsPinup()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 482
    const/4 v0, 0x0

    return v0
.end method

.method public isStartingSplitScreen(Landroid/content/ComponentName;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 508
    const/4 v0, 0x0

    return v0
.end method

.method public maximize()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 494
    const/4 v0, 0x0

    return v0
.end method

.method public minimize()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 500
    const/4 v0, 0x0

    return v0
.end method

.method public minimize(I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 502
    const/4 v0, 0x0

    return v0
.end method

.method public minimize(Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 332
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v2

    if-nez v2, :cond_1

    .line 357
    :cond_0
    :goto_0
    return v0

    .line 336
    :cond_1
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 338
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->getMultiWindowEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 342
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->isMinimized()Z

    move-result v0

    if-ne p1, v0, :cond_2

    move v0, v1

    .line 343
    goto :goto_0

    .line 346
    :cond_2
    if-eqz p1, :cond_3

    .line 347
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v2, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    or-int/2addr v0, v2

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 348
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v2, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    or-int/2addr v0, v2

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 354
    :goto_1
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 355
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    move v0, v1

    .line 357
    goto :goto_0

    .line 351
    :cond_3
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v2, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v0, v2

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_1
.end method

.method public multiWindow()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 220
    invoke-virtual {p0, v0, v0}, Landroid/sec/multiwindow/MultiWindow;->multiWindow(ZZ)Z

    move-result v0

    return v0
.end method

.method public multiWindow(Z)Z
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/sec/multiwindow/MultiWindow;->multiWindow(ZZ)Z

    move-result v0

    return v0
.end method

.method public multiWindow(ZZ)Z
    .locals 1

    .prologue
    .line 235
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_UNKNOWN:I

    invoke-virtual {p0, p1, p2, v0}, Landroid/sec/multiwindow/MultiWindow;->multiWindow(ZZI)Z

    move-result v0

    return v0
.end method

.method public multiWindow(ZZI)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 247
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 249
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->getMultiWindowEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 288
    :cond_0
    :goto_0
    return v0

    .line 253
    :cond_1
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_0

    .line 257
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_MASK:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 258
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 260
    if-eqz p1, :cond_3

    .line 261
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 267
    :goto_1
    if-eqz p2, :cond_4

    .line 268
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 274
    :goto_2
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_UNKNOWN:I

    if-ne p3, v0, :cond_5

    .line 275
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->getArrange(Landroid/content/res/Configuration;)I

    move-result v0

    .line 276
    sget v1, Landroid/sec/multiwindow/Constants$Configuration;->ARRANGE_SPLITED:I

    if-ne v0, v1, :cond_2

    .line 277
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 278
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_A:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 285
    :cond_2
    :goto_3
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    .line 286
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 288
    const/4 v0, 0x1

    goto :goto_0

    .line 264
    :cond_3
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_1

    .line 271
    :cond_4
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_2

    .line 281
    :cond_5
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 282
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    or-int/2addr v0, p3

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_3
.end method

.method public normalWindow()Z
    .locals 2

    .prologue
    .line 201
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 203
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_MASK:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 204
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_NORMAL:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 205
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 206
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 208
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 209
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    .line 210
    const/4 v0, 0x1

    return v0
.end method

.method public pinUp(Z)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 524
    const/4 v0, 0x0

    return v0
.end method

.method public pinUp(ZI)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 526
    const/4 v0, 0x0

    return v0
.end method

.method public pinup(Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 300
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_1

    .line 320
    :cond_0
    :goto_0
    return v0

    .line 304
    :cond_1
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 306
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->getMultiWindowEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 310
    if-eqz p1, :cond_2

    .line 311
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 317
    :goto_1
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 318
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    .line 320
    const/4 v0, 0x1

    goto :goto_0

    .line 314
    :cond_2
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_1
.end method

.method public relayout(Landroid/graphics/Rect;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 510
    const/4 v0, 0x0

    return v0
.end method

.method public restore()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 496
    const/4 v0, 0x0

    return v0
.end method

.method public restore(I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 498
    const/4 v0, 0x1

    return v0
.end method

.method public setArrangeable(Z)Z
    .locals 1

    .prologue
    .line 444
    iput-boolean p1, p0, Landroid/sec/multiwindow/MultiWindow;->mArrangeable:Z

    .line 445
    const/4 v0, 0x1

    return v0
.end method

.method public setMinimumSize(II)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 518
    const/4 v0, 0x0

    return v0
.end method

.method public setMultiWindowEnabled(Z)Z
    .locals 2

    .prologue
    .line 405
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 407
    if-eqz p1, :cond_0

    .line 408
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_RESIZE:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 418
    :goto_0
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 419
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    .line 421
    const/4 v0, 0x1

    return v0

    .line 411
    :cond_0
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_MASK:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 412
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_NORMAL:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 413
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 414
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 415
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_RESIZE:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_0
.end method

.method public setRect(Landroid/graphics/Rect;)Z
    .locals 2

    .prologue
    .line 368
    if-eqz p1, :cond_0

    .line 369
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mMinimumSize:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mMinimumSize:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mMaximumSize:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-gt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mMaximumSize:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 371
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 372
    invoke-direct {p0, p1}, Landroid/sec/multiwindow/MultiWindow;->setLastSize(Landroid/graphics/Rect;)V

    .line 373
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    .line 375
    const/4 v0, 0x1

    .line 379
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setZone(I)V
    .locals 2

    .prologue
    .line 472
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 473
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 474
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

    and-int/2addr v1, p1

    or-int/2addr v0, v1

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 475
    return-void
.end method

.method public start()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 488
    const/4 v0, 0x0

    return v0
.end method
