.class public Landroid/sec/multiwindow/Constants$WindowManagerPolicy;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field static FIELD_NAMES:[Ljava/lang/String;

.field public static WINDOW_INFO_NOTHING_CHANGED:I

.field public static WINDOW_INFO_SIZE_CHANGED:I

.field public static WINDOW_MODE_FREESTYLE:I

.field public static WINDOW_MODE_MASK:I

.field public static WINDOW_MODE_NORMAL:I

.field public static WINDOW_MODE_OPTION_COMMON_FIXED_RATIO:I

.field public static WINDOW_MODE_OPTION_COMMON_FIXED_SIZE:I

.field public static WINDOW_MODE_OPTION_COMMON_HIDDEN:I

.field public static WINDOW_MODE_OPTION_COMMON_INHERIT:I

.field public static WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

.field public static WINDOW_MODE_OPTION_COMMON_NO_CONTROLBAR:I

.field public static WINDOW_MODE_OPTION_COMMON_NO_DECORATION:I

.field public static WINDOW_MODE_OPTION_COMMON_PINUP:I

.field public static WINDOW_MODE_OPTION_COMMON_RESIZE:I

.field public static WINDOW_MODE_OPTION_SPLIT_ZONE_A:I

.field public static WINDOW_MODE_OPTION_SPLIT_ZONE_B:I

.field public static WINDOW_MODE_OPTION_SPLIT_ZONE_C:I

.field public static WINDOW_MODE_OPTION_SPLIT_ZONE_D:I

.field public static WINDOW_MODE_OPTION_SPLIT_ZONE_E:I

.field public static WINDOW_MODE_OPTION_SPLIT_ZONE_F:I

.field public static WINDOW_MODE_OPTION_SPLIT_ZONE_FULL:I

.field public static WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

.field public static WINDOW_MODE_OPTION_SPLIT_ZONE_UNKNOWN:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 133
    const/16 v1, 0x17

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "WINDOW_MODE_MASK"

    aput-object v2, v1, v0

    const/4 v2, 0x1

    const-string v3, "WINDOW_MODE_NORMAL"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "WINDOW_MODE_FREESTYLE"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "WINDOW_MODE_OPTION_COMMON_PINUP"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "WINDOW_MODE_OPTION_COMMON_INHERIT"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "WINDOW_MODE_OPTION_COMMON_RESIZE"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "WINDOW_MODE_OPTION_COMMON_MINIMIZED"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "WINDOW_MODE_OPTION_COMMON_HIDDEN"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "WINDOW_MODE_OPTION_COMMON_FIXED_SIZE"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "WINDOW_MODE_OPTION_COMMON_FIXED_RATIO"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "WINDOW_INFO_NOTHING_CHANGED"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "WINDOW_INFO_SIZE_CHANGED"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "WINDOW_MODE_OPTION_COMMON_NO_DECORATION"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "WINDOW_MODE_OPTION_SPLIT_ZONE_MASK"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "WINDOW_MODE_OPTION_SPLIT_ZONE_F"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "WINDOW_MODE_OPTION_SPLIT_ZONE_E"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "WINDOW_MODE_OPTION_SPLIT_ZONE_D"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "WINDOW_MODE_OPTION_SPLIT_ZONE_C"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "WINDOW_MODE_OPTION_SPLIT_ZONE_UNKNOWN"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "WINDOW_MODE_OPTION_SPLIT_ZONE_B"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "WINDOW_MODE_OPTION_SPLIT_ZONE_A"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "WINDOW_MODE_OPTION_SPLIT_ZONE_FULL"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "WINDOW_MODE_OPTION_COMMON_NO_CONTROLBAR"

    aput-object v3, v1, v2

    sput-object v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->FIELD_NAMES:[Ljava/lang/String;

    .line 160
    sget-object v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->FIELD_NAMES:[Ljava/lang/String;

    array-length v1, v1

    .line 161
    :goto_0
    if-ge v0, v1, :cond_0

    .line 163
    :try_start_0
    const-class v2, Landroid/view/WindowManagerPolicy;

    sget-object v3, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 165
    const-class v3, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;

    sget-object v4, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 166
    invoke-virtual {v2, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v3, v3, v2}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_0
    return-void

    .line 169
    :catch_0
    move-exception v2

    goto :goto_1

    .line 168
    :catch_1
    move-exception v2

    goto :goto_1

    .line 167
    :catch_2
    move-exception v2

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
