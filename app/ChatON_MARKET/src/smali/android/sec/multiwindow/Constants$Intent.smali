.class public Landroid/sec/multiwindow/Constants$Intent;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static ACTION_ARRANGE_WINDOWS:Ljava/lang/String;

.field public static CATEGORY_MULTIWINDOW_LAUNCHER:Ljava/lang/String;

.field public static EXTRA_ARRANGE_MODE:Ljava/lang/String;

.field public static EXTRA_WINDOW_ARRANGED_SIZE:Ljava/lang/String;

.field public static EXTRA_WINDOW_DEFAULT_SIZE:Ljava/lang/String;

.field public static EXTRA_WINDOW_LAST_SIZE:Ljava/lang/String;

.field public static EXTRA_WINDOW_MINIMIZED_SIZE:Ljava/lang/String;

.field public static EXTRA_WINDOW_MINIMIZED_SLOT:Ljava/lang/String;

.field public static EXTRA_WINDOW_MINIMUM_SIZE:Ljava/lang/String;

.field public static EXTRA_WINDOW_MODE:Ljava/lang/String;

.field public static EXTRA_WINDOW_OUT_OF_ARRANGE:Ljava/lang/String;

.field public static EXTRA_WINDOW_POSITION:Ljava/lang/String;

.field static FIELD_NAMES:[Ljava/lang/String;

.field public static METADATA_MULTIWINDOW_DEF_HEIGHT:Ljava/lang/String;

.field public static METADATA_MULTIWINDOW_DEF_WIDTH:Ljava/lang/String;

.field public static METADATA_MULTIWINDOW_MIN_HEIGHT:Ljava/lang/String;

.field public static METADATA_MULTIWINDOW_MIN_WIDTH:Ljava/lang/String;

.field public static METADATA_MULTIWINDOW_STYLE:Ljava/lang/String;

.field public static METADATA_SUPPORT_MULTIWINDOW:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 47
    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "EXTRA_WINDOW_MODE"

    aput-object v2, v1, v0

    const/4 v2, 0x1

    const-string v3, "EXTRA_WINDOW_POSITION"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "EXTRA_WINDOW_MINIMUM_SIZE"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "EXTRA_WINDOW_MINIMIZED_SIZE"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "EXTRA_WINDOW_MINIMIZED_SLOT"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "EXTRA_WINDOW_DEFAULT_SIZE"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "EXTRA_WINDOW_LAST_SIZE"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "EXTRA_WINDOW_ARRANGED_SIZE"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "EXTRA_WINDOW_OUT_OF_ARRANGE"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "CATEGORY_MULTIWINDOW_LAUNCHER"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "METADATA_SUPPORT_MULTIWINDOW"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "METADATA_MULTIWINDOW_STYLE"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "METADATA_MULTIWINDOW_MIN_WIDTH"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "METADATA_MULTIWINDOW_MIN_HEIGHT"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "METADATA_MULTIWINDOW_DEF_WIDTH"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "METADATA_MULTIWINDOW_DEF_HEIGHT"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "ACTION_ARRANGE_WINDOWS"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "EXTRA_ARRANGE_MODE"

    aput-object v3, v1, v2

    sput-object v1, Landroid/sec/multiwindow/Constants$Intent;->FIELD_NAMES:[Ljava/lang/String;

    .line 69
    sget-object v1, Landroid/sec/multiwindow/Constants$Intent;->FIELD_NAMES:[Ljava/lang/String;

    array-length v1, v1

    .line 70
    :goto_0
    if-ge v0, v1, :cond_0

    .line 72
    :try_start_0
    const-class v2, Landroid/content/Intent;

    sget-object v3, Landroid/sec/multiwindow/Constants$Intent;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 73
    const-class v3, Landroid/sec/multiwindow/Constants$Intent;

    sget-object v4, Landroid/sec/multiwindow/Constants$Intent;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 74
    invoke-virtual {v2, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v3, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_0
    return-void

    .line 77
    :catch_0
    move-exception v2

    goto :goto_1

    .line 76
    :catch_1
    move-exception v2

    goto :goto_1

    .line 75
    :catch_2
    move-exception v2

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
