.class public abstract Landroid/sec/multiwindow/MWOnDragListener;
.super Ljava/lang/Object;
.source "MWOnDragListener.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# static fields
.field public static final TAG:Ljava/lang/String; = "MWOnDragListener"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 30
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 47
    const-string v0, "MWOnDragListener"

    const-string v1, "Unknown action type received by OnDragListener."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    const/4 v0, 0x0

    :goto_0
    :pswitch_0
    return v0

    .line 40
    :pswitch_1
    invoke-virtual {p0, p2}, Landroid/sec/multiwindow/MWOnDragListener;->onDrop(Landroid/view/DragEvent;)V

    .line 41
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 44
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 30
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public abstract onDrop(Landroid/view/DragEvent;)V
.end method
