.class public Landroid/sec/multiwindow/Constants$Configuration;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static ARRANGE_CASCADE:I

.field public static ARRANGE_MINIMIZED_ALL:I

.field public static ARRANGE_RESTORED_ALL:I

.field public static ARRANGE_SPLITED:I

.field public static ARRANGE_TILED:I

.field public static ARRANGE_TOGGLE_MASK:I

.field public static ARRANGE_UNDEFINED:I

.field static FIELD_NAMES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 184
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "ARRANGE_UNDEFINED"

    aput-object v2, v1, v0

    const/4 v2, 0x1

    const-string v3, "ARRANGE_TILED"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "ARRANGE_CASCADE"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "ARRANGE_SPLITED"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "ARRANGE_MINIMIZED_ALL"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "ARRANGE_RESTORED_ALL"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "ARRANGE_TOGGLE_MASK"

    aput-object v3, v1, v2

    sput-object v1, Landroid/sec/multiwindow/Constants$Configuration;->FIELD_NAMES:[Ljava/lang/String;

    .line 195
    sget-object v1, Landroid/sec/multiwindow/Constants$Configuration;->FIELD_NAMES:[Ljava/lang/String;

    array-length v1, v1

    .line 196
    :goto_0
    if-ge v0, v1, :cond_0

    .line 198
    :try_start_0
    const-class v2, Landroid/content/res/Configuration;

    sget-object v3, Landroid/sec/multiwindow/Constants$Configuration;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 200
    const-class v3, Landroid/sec/multiwindow/Constants$Configuration;

    sget-object v4, Landroid/sec/multiwindow/Constants$Configuration;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 201
    invoke-virtual {v2, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v3, v3, v2}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    :cond_0
    return-void

    .line 204
    :catch_0
    move-exception v2

    goto :goto_1

    .line 203
    :catch_1
    move-exception v2

    goto :goto_1

    .line 202
    :catch_2
    move-exception v2

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
