.class public interface abstract Landroid/os/IVoIPCallbackInterface;
.super Ljava/lang/Object;
.source "IVoIPCallbackInterface.java"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract answerVoIPCall()Z
.end method

.method public abstract hangupVoIPCall()Z
.end method

.method public abstract holdVoIPCall()Z
.end method

.method public abstract moveVoIPToTop()Z
.end method

.method public abstract resumeVoIPCall()Z
.end method
