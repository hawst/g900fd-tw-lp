.class public Landroid/support/v4/app/ActionBarHookActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "ActionBarHookActivity.java"


# instance fields
.field private mMenuImpl:Lcom/sec/common/actionbar/t;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 23
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 24
    if-nez p1, :cond_0

    .line 25
    invoke-virtual {p0, p2}, Landroid/support/v4/app/ActionBarHookActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    .line 27
    instance-of v0, p2, Lcom/sec/common/actionbar/t;

    if-eqz v0, :cond_1

    move-object v0, p2

    .line 28
    check-cast v0, Lcom/sec/common/actionbar/t;

    iput-object v0, p0, Landroid/support/v4/app/ActionBarHookActivity;->mMenuImpl:Lcom/sec/common/actionbar/t;

    .line 29
    iget-object v0, p0, Landroid/support/v4/app/ActionBarHookActivity;->mFragments:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {p0}, Landroid/support/v4/app/ActionBarHookActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    invoke-virtual {v0, p2, v2}, Landroid/support/v4/app/FragmentManagerImpl;->dispatchCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    or-int/2addr v0, v1

    .line 36
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 41
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 42
    if-nez p1, :cond_1

    .line 43
    iget-object v0, p0, Landroid/support/v4/app/ActionBarHookActivity;->mMenuImpl:Lcom/sec/common/actionbar/t;

    invoke-virtual {p0, v0}, Landroid/support/v4/app/ActionBarHookActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 44
    iget-object v1, p0, Landroid/support/v4/app/ActionBarHookActivity;->mFragments:Landroid/support/v4/app/FragmentManagerImpl;

    iget-object v2, p0, Landroid/support/v4/app/ActionBarHookActivity;->mMenuImpl:Lcom/sec/common/actionbar/t;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManagerImpl;->dispatchPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 45
    if-eqz v0, :cond_0

    invoke-interface {p3}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 49
    :goto_0
    return v0

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 49
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method
