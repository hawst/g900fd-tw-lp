.class public final enum La/a/b/c;
.super Ljava/lang/Enum;
.source "Image.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "La/a/b/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:La/a/b/c;

.field public static final enum b:La/a/b/c;

.field private static final synthetic c:[La/a/b/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, La/a/b/c;

    const-string v1, "NO_OVERLAY_NO_CROP"

    invoke-direct {v0, v1, v2}, La/a/b/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/c;->a:La/a/b/c;

    .line 19
    new-instance v0, La/a/b/c;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, La/a/b/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, La/a/b/c;->b:La/a/b/c;

    .line 17
    const/4 v0, 0x2

    new-array v0, v0, [La/a/b/c;

    sget-object v1, La/a/b/c;->a:La/a/b/c;

    aput-object v1, v0, v2

    sget-object v1, La/a/b/c;->b:La/a/b/c;

    aput-object v1, v0, v3

    sput-object v0, La/a/b/c;->c:[La/a/b/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)La/a/b/c;
    .locals 1

    .prologue
    .line 1
    const-class v0, La/a/b/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, La/a/b/c;

    return-object v0
.end method

.method public static values()[La/a/b/c;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, La/a/b/c;->c:[La/a/b/c;

    array-length v1, v0

    new-array v2, v1, [La/a/b/c;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
