.class public Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;
.super Landroid/content/ContentProvider;
.source "PartnerBookmarksProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final BOOKMARKS_PROJECTION_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final URI_MATCHER:Landroid/content/UriMatcher;


# instance fields
.field private cutomerRootId:J

.field private mOpenHelper:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 89
    new-instance v3, Landroid/content/UriMatcher;

    const/4 v4, -0x1

    invoke-direct {v3, v4}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v3, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 90
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->BOOKMARKS_PROJECTION_MAP:Ljava/util/Map;

    .line 107
    sget-object v2, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 108
    .local v2, "matcher":Landroid/content/UriMatcher;
    const-string v0, "com.android.partnerbookmarks"

    .line 109
    .local v0, "authority":Ljava/lang/String;
    const-string v3, "com.android.partnerbookmarks"

    const-string v4, "bookmarks"

    const/16 v5, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 110
    const-string v3, "com.android.partnerbookmarks"

    const-string v4, "bookmarks/#"

    const/16 v5, 0x3e9

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 111
    const-string v3, "com.android.partnerbookmarks"

    const-string v4, "bookmarks/folder"

    const/16 v5, 0x3ea

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 112
    const-string v3, "com.android.partnerbookmarks"

    const-string v4, "bookmarks/folder/#"

    const/16 v5, 0x3eb

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 113
    const-string v3, "com.android.partnerbookmarks"

    const-string v4, "bookmarks/folder/id"

    const/16 v5, 0x3ec

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 116
    sget-object v1, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->BOOKMARKS_PROJECTION_MAP:Ljava/util/Map;

    .line 117
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "_id"

    const-string v4, "_id"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    const-string v3, "title"

    const-string v4, "title"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    const-string v3, "url"

    const-string v4, "url"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    const-string v3, "type"

    const-string v4, "type"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    const-string v3, "parent"

    const-string v4, "parent"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    const-string v3, "favicon"

    const-string v4, "favicon"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    const-string v3, "touchicon"

    const-string v4, "touchicon"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 79
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->cutomerRootId:J

    .line 133
    return-void
.end method

.method static synthetic access$000(Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;)Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->mOpenHelper:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;

    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 589
    const-string v6, "PartnerBookmarksProvider"

    const-string v7, "delete : called"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    const-string v3, "com.android.browser.permission.WRITE_HISTORY_BOOKMARKS"

    .line 592
    .local v3, "permission":Ljava/lang/String;
    const-string v2, "caller does not have PERMISSION_GRANTED"

    .line 594
    .local v2, "message":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v7

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v8

    invoke-virtual {v6, v3, v7, v8, v2}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V

    .line 596
    iget-object v6, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->mOpenHelper:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;

    invoke-virtual {v6}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 597
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_1

    .line 598
    const-string v6, "PartnerBookmarksProvider"

    const-string v7, "delete : db is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v5

    .line 611
    :cond_0
    :goto_0
    return v1

    .line 601
    :cond_1
    const/4 v1, 0x0

    .line 604
    .local v1, "deleted":I
    :try_start_0
    const-string v6, "bookmarks"

    invoke-virtual {v0, v6, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 608
    :goto_1
    if-lez v1, :cond_0

    .line 609
    invoke-virtual {p0}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto :goto_0

    .line 605
    :catch_0
    move-exception v4

    .line 606
    .local v4, "t":Ljava/lang/Throwable;
    const-string v6, "PartnerBookmarksProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "delete of Uri "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " failed exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 550
    sget-object v1, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 551
    .local v0, "match":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    .line 552
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "vnd.android.cursor.item/partnerbookmark"

    goto :goto_0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v7, 0x0

    .line 557
    const-string v8, "PartnerBookmarksProvider"

    const-string v9, "insert : called"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    const-string v5, "com.android.browser.permission.WRITE_HISTORY_BOOKMARKS"

    .line 560
    .local v5, "permission":Ljava/lang/String;
    const-string v4, "caller does not have PERMISSION_GRANTED"

    .line 562
    .local v4, "message":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v9

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v10

    invoke-virtual {v8, v5, v9, v10, v4}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V

    .line 564
    iget-object v8, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->mOpenHelper:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;

    invoke-virtual {v8}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 565
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 566
    const-string v8, "PartnerBookmarksProvider"

    const-string v9, "insert : db is null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v7

    .line 584
    :goto_0
    return-object v1

    .line 569
    :cond_0
    const-wide/16 v2, -0x1

    .line 572
    .local v2, "id":J
    :try_start_0
    const-string v8, "bookmarks"

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 578
    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-gez v8, :cond_1

    move-object v1, v7

    .line 579
    goto :goto_0

    .line 573
    :catch_0
    move-exception v6

    .line 574
    .local v6, "t":Ljava/lang/Throwable;
    const-string v8, "PartnerBookmarksProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "insert of Uri "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " failed exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    const-wide/16 v2, -0x1

    goto :goto_1

    .line 581
    .end local v6    # "t":Ljava/lang/Throwable;
    :cond_1
    invoke-static {p1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 582
    .local v1, "inserted":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v1, v7, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 473
    iget-object v0, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->mOpenHelper:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;

    invoke-virtual {p0}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->prepareForConfiguration(Landroid/content/res/Configuration;)V

    .line 474
    return-void
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 467
    new-instance v0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;

    invoke-virtual {p0}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;-><init>(Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->mOpenHelper:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;

    .line 468
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 20
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 479
    sget-object v3, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v18

    .line 480
    .local v18, "match":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->mOpenHelper:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->prepareForConfiguration(Landroid/content/res/Configuration;)V

    .line 481
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->mOpenHelper:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;

    invoke-virtual {v3}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v17

    .line 482
    .local v17, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 483
    .local v2, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v3, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 484
    .local v13, "limit":Ljava/lang/String;
    const-string v3, "groupBy"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 485
    .local v10, "groupBy":Ljava/lang/String;
    packed-switch v18, :pswitch_data_0

    .line 541
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URL "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 489
    :pswitch_0
    const/16 v3, 0x3e9

    move/from16 v0, v18

    if-ne v0, v3, :cond_2

    .line 491
    const-string v3, "bookmarks._id=?"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Landroid/database/DatabaseUtils;->concatenateWhere(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 494
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 505
    :cond_0
    :goto_0
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 506
    const-string p5, "_id DESC, _id ASC"

    .line 508
    :cond_1
    sget-object v3, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->BOOKMARKS_PROJECTION_MAP:Ljava/util/Map;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 509
    const-string v3, "bookmarks"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 545
    const/4 v11, 0x0

    move-object v5, v2

    move-object/from16 v6, v17

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v12, p5

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    :goto_1
    return-object v16

    .line 496
    :cond_2
    const/16 v3, 0x3eb

    move/from16 v0, v18

    if-ne v0, v3, :cond_0

    .line 498
    const-string v3, "bookmarks.parent=?"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Landroid/database/DatabaseUtils;->concatenateWhere(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 501
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    goto :goto_0

    .line 514
    :pswitch_1
    const-string v3, "bookmarks"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 518
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 519
    const-string p5, "_id DESC, _id ASC"

    .line 521
    :cond_3
    sget-object v3, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->BOOKMARKS_PROJECTION_MAP:Ljava/util/Map;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 522
    const-string v4, "parent=?"

    .line 523
    .local v4, "where":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-static {v4, v0}, Landroid/database/DatabaseUtils;->concatenateWhere(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 524
    const/4 v3, 0x1

    new-array v14, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-wide/16 v6, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v14, v3

    .line 525
    .local v14, "args":[Ljava/lang/String;
    if-eqz p4, :cond_4

    .line 526
    move-object/from16 v0, p4

    invoke-static {v14, v0}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 528
    :cond_4
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p2

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 529
    .local v19, "query":Ljava/lang/String;
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v14}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 530
    .local v16, "cursor":Landroid/database/Cursor;
    goto :goto_1

    .line 534
    .end local v4    # "where":Ljava/lang/String;
    .end local v14    # "args":[Ljava/lang/String;
    .end local v16    # "cursor":Landroid/database/Cursor;
    .end local v19    # "query":Ljava/lang/String;
    :pswitch_2
    new-instance v15, Landroid/database/MatrixCursor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v3, v5

    invoke-direct {v15, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 536
    .local v15, "c":Landroid/database/MatrixCursor;
    invoke-virtual {v15}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const-wide/16 v6, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-object/from16 v16, v15

    .line 537
    goto/16 :goto_1

    .line 485
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 616
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
