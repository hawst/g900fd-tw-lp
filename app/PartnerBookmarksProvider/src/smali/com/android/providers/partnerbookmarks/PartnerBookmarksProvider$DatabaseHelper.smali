.class final Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PartnerBookmarksProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DatabaseHelper"
.end annotation


# instance fields
.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field final synthetic this$0:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;


# direct methods
.method public constructor <init>(Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->this$0:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;

    .line 141
    const-string v0, "partnerBookmarks.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 142
    const-string v0, "pbppref"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 144
    return-void
.end method

.method private createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 175
    const-string v0, "CREATE TABLE bookmarks(_id INTEGER NOT NULL DEFAULT 0,title TEXT,url TEXT,type INTEGER NOT NULL DEFAULT 0,parent INTEGER,favicon BLOB,touchicon BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method private dropTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 193
    const-string v0, "DROP TABLE IF EXISTS bookmarks"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 194
    return-void
.end method

.method private getConfigSignature(Landroid/content/res/Configuration;)Ljava/lang/String;
    .locals 2
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mmc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/content/res/Configuration;->mcc:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-mnc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/content/res/Configuration;->mnc:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-loc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 198
    monitor-enter p0

    .line 199
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 207
    monitor-exit p0

    .line 208
    return-void

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->dropTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 219
    invoke-virtual {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 220
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->dropTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 213
    invoke-virtual {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 214
    return-void
.end method

.method public declared-synchronized prepareForConfiguration(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 153
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->this$0:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;

    # getter for: Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->mOpenHelper:Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;
    invoke-static {v3}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;->access$000(Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider;)Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 154
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, p1}, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->getConfigSignature(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v2

    .line 155
    .local v2, "newSignature":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksProvider$DatabaseHelper;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "config"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 166
    .local v0, "activeSignature":Ljava/lang/String;
    monitor-exit p0

    return-void

    .line 153
    .end local v0    # "activeSignature":Ljava/lang/String;
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "newSignature":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
