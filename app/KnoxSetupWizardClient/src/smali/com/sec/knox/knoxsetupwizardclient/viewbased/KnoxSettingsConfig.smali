.class public Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;
.super Ljava/lang/Object;
.source "KnoxSettingsConfig.java"


# static fields
.field public static final approvedInstallers:[Ljava/lang/String;

.field public static final approvedPackages:[Ljava/lang/String;

.field public static final defaultHideBrowserPkg:[Ljava/lang/String;

.field public static final defaultPreloadedApps:[Ljava/lang/String;

.field public static final excludedComponents:[[Ljava/lang/String;

.field public static final excludedComponents_noncom:[[Ljava/lang/String;

.field public static final excludedPackages:[Ljava/lang/String;

.field public static final excludedPermissions:[Ljava/lang/String;

.field public static final mdmPackages:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 16
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.android.calendar"

    aput-object v1, v0, v4

    const-string v1, "com.sec.android.app.sbrowser"

    aput-object v1, v0, v5

    const-string v1, "com.sec.android.app.music"

    aput-object v1, v0, v6

    const-string v1, "com.samsung.everglades.video"

    aput-object v1, v0, v7

    const-string v1, "com.sec.android.app.samsungapps"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "com.android.email"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.sec.android.gallery3d"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.sec.android.app.camera"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.samsung.android.app.memo"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.android.contacts"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.sec.android.app.myfiles"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.infraware.polarisviewer"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.infraware.polarisoffice"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "com.sec.knox.containeragent2"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "com.android.settings"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "com.samsung.android.app.pinboard"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "com.samsung.android.snote"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "com.hancom.office.editor"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "com.samsung.contacts"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "com.sec.android.quickmemo"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->defaultPreloadedApps:[Ljava/lang/String;

    .line 39
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.android.browser"

    aput-object v1, v0, v4

    const-string v1, "com.sec.webbrowserminiapp"

    aput-object v1, v0, v5

    const-string v1, "com.android.chrome"

    aput-object v1, v0, v6

    const-string v1, "com.sec.android.app.sbrowser"

    aput-object v1, v0, v7

    const-string v1, "com.android.browser.provider"

    aput-object v1, v0, v8

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->defaultHideBrowserPkg:[Ljava/lang/String;

    .line 47
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.android.settings"

    aput-object v1, v0, v4

    const-string v1, "com.sec.knox.knoxsetupwizardclient"

    aput-object v1, v0, v5

    const-string v1, "com.sec.knox.containeragent2"

    aput-object v1, v0, v6

    const-string v1, "com.android.mms"

    aput-object v1, v0, v7

    const-string v1, "com.sec.knox.setupwizardstub"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "com.sec.chaton"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.sec.pcw"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.sec.knox.shortcutsms"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.sec.knox.switcher"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.sec.watchon.phone"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.sec.android.automotive.drivelink"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.samsung.android.app.lifetimes"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.sec.android.app.shealth"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "com.sec.android.app.voicenote"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "com.sec.android.app.kidshome"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "com.sec.knox.app.container"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "com.sec.knox.shortcuti"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "com.sec.knox.containeragent"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "com.sec.android.app.samsungapps"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "tv.peel.smartremote"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "com.sec.keystringscreen"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->excludedPackages:[Ljava/lang/String;

    .line 71
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.android.chrome"

    aput-object v1, v0, v4

    const-string v1, "com.google.android.apps"

    aput-object v1, v0, v5

    const-string v1, "com.google.android.apps.docs"

    aput-object v1, v0, v6

    const-string v1, "com.google.android.gm"

    aput-object v1, v0, v7

    const-string v1, "com.google.android.googlequicksearchbox"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "com.google.android.talk"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.google.android.apps.maps"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.google.android.apps.books"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.google.android.play.games"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.google.android.music"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.google.android.videos"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.google.android.apps.magazines"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.google.android.youtube"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->approvedPackages:[Ljava/lang/String;

    .line 90
    const/16 v0, 0xf

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.sec.android.gallery3d"

    aput-object v2, v1, v4

    const-string v2, "com.sec.android.gallery3d.app.LockScreen"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.sec.android.gallery3d"

    aput-object v2, v1, v4

    const-string v2, "com.sec.android.gallery3d.app.BothScreen"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.android.contacts"

    aput-object v2, v1, v4

    const-string v2, "com.sec.android.app.contacts.RecntcallEntryActivity"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.sec.knox.shortcutsms"

    aput-object v2, v1, v4

    const-string v2, "com.sec.knox.shortcutsms.MainActivity"

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.sec.knox.shortcutsms"

    aput-object v2, v1, v4

    const-string v2, "com.sec.knox.shortcutsms.CallMainActivity"

    aput-object v2, v1, v5

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.UsbSettings"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.dualsimwidget.DualsimWidget"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.favoritesettingswidget.FavoriteSettingsWidget"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.accessibilitywidget.AccessibilityWidgetProviderMobileHotspot"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.accessibilitywidget.AccessibilityWidgetProviderNegativeColor"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.powersavingmode.ConnectivityLocationWidgetProvider"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.accessibilitywidget.AccessibilityWidgetProviderAssistiveLight"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.widget.SmartSwitchWidgetProvider"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.google.android.gm"

    aput-object v3, v2, v4

    const-string v3, "com.google.android.gm.widget.GoogleMailWidgetProvider"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.google.android.gm"

    aput-object v3, v2, v4

    const-string v3, "com.google.android.gm.CreateShortcutActivityGoogleMail"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->excludedComponents:[[Ljava/lang/String;

    .line 108
    new-array v0, v6, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.android.settings"

    aput-object v2, v1, v4

    const-string v2, "com.android.settings.TetherSettings"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.android.settings"

    aput-object v2, v1, v4

    const-string v2, "com.android.settings.Settings$TetherSettingsActivity2"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->excludedComponents_noncom:[[Ljava/lang/String;

    .line 112
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "com.google.android.c2dm.permission.RECEIVE"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->excludedPermissions:[Ljava/lang/String;

    .line 116
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "com.android.vending"

    aput-object v1, v0, v4

    const-string v1, "com.sec.android.app.samsungapps"

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->approvedInstallers:[Ljava/lang/String;

    .line 121
    new-array v0, v8, [Ljava/lang/String;

    const-string v1, "com.samsung.mdmtest1"

    aput-object v1, v0, v4

    const-string v1, "com.samsung.mdmtest2"

    aput-object v1, v0, v5

    const-string v1, "com.samsung.edmtest"

    aput-object v1, v0, v6

    const-string v1, "com.samsung.containertool"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->mdmPackages:[Ljava/lang/String;

    return-void
.end method
