.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$5;
.super Ljava/lang/Object;
.source "FolderContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setCallBack()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 0

    .prologue
    .line 899
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 902
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 904
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentPersonaId:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 905
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.sec.knox.containeragent2"

    const-string v3, "com.sec.knox.containeragent2.ui.settings.KnoxSettingsActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    .local v0, "component":Landroid/content/ComponentName;
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 909
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 910
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 911
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 912
    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserHandle:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 914
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showMoreLayout(Z)V

    .line 915
    return-void

    .line 907
    .end local v0    # "component":Landroid/content/ComponentName;
    :cond_0
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.sec.knox.containeragent2"

    const-string v3, "com.sec.knox.containeragent2.ui.settings.KnoxSettingsActivity2"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v0    # "component":Landroid/content/ComponentName;
    goto :goto_0
.end method
