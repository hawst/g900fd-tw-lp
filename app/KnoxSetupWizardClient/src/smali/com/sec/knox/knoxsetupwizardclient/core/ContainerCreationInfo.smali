.class public Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;
.super Ljava/lang/Object;
.source "ContainerCreationInfo.java"


# static fields
.field private static backupPin:Ljava/lang/String;

.field private static backupPinForPattern:Ljava/lang/String;

.field private static containerAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

.field private static isFingerprintPlus:Z

.field private static isFingerprintSupplement:Z

.field private static isRestore:Z

.field private static knoxName:Ljava/lang/String;

.field private static password:Ljava/lang/String;

.field private static passwordType:I

.field private static securityTimeOut:I

.field private static simplePassword:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 26
    sput-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->knoxName:Ljava/lang/String;

    .line 27
    sput-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->password:Ljava/lang/String;

    .line 28
    sput v2, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->passwordType:I

    .line 29
    sput-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->backupPin:Ljava/lang/String;

    .line 30
    sput-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    .line 31
    sput-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->containerAppList:Ljava/util/List;

    .line 32
    const/4 v0, -0x1

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->securityTimeOut:I

    .line 33
    sput-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->backupPinForPattern:Ljava/lang/String;

    .line 34
    sput-boolean v2, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->isFingerprintPlus:Z

    .line 35
    sput-boolean v2, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->simplePassword:Z

    .line 36
    sput-boolean v2, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->isFingerprintSupplement:Z

    .line 37
    sput-boolean v2, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->isRestore:Z

    return-void
.end method

.method public static getBackupPin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->backupPin:Ljava/lang/String;

    return-object v0
.end method

.method public static getContainerCreationParams()Lcom/sec/knox/container/ContainerCreationParams;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    return-object v0
.end method

.method public static getFingerprintPlus()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->isFingerprintPlus:Z

    return v0
.end method

.method public static getFingerprintSupplement()Z
    .locals 1

    .prologue
    .line 52
    sget-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->isFingerprintSupplement:Z

    return v0
.end method

.method public static getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->knoxName:Ljava/lang/String;

    return-object v0
.end method

.method public static getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->password:Ljava/lang/String;

    return-object v0
.end method

.method public static getPasswordType()I
    .locals 1

    .prologue
    .line 76
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->passwordType:I

    return v0
.end method

.method public static getRestore()Z
    .locals 1

    .prologue
    .line 137
    sget-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->isRestore:Z

    return v0
.end method

.method public static getSecurityTimeOut()I
    .locals 1

    .prologue
    .line 121
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->securityTimeOut:I

    return v0
.end method

.method public static getSimplePassword()Z
    .locals 1

    .prologue
    .line 129
    sget-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->simplePassword:Z

    return v0
.end method

.method public static setBackupPin(Ljava/lang/String;)V
    .locals 0
    .param p0, "pin"    # Ljava/lang/String;

    .prologue
    .line 80
    sput-object p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->backupPin:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public static setBackupPinForPattern(Ljava/lang/String;)V
    .locals 0
    .param p0, "pin"    # Ljava/lang/String;

    .prologue
    .line 88
    sput-object p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->backupPinForPattern:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public static setContainerAppList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p0, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sput-object p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->containerAppList:Ljava/util/List;

    .line 106
    return-void
.end method

.method public static setContainerCreationParams(Lcom/sec/knox/container/ContainerCreationParams;)V
    .locals 0
    .param p0, "param"    # Lcom/sec/knox/container/ContainerCreationParams;

    .prologue
    .line 96
    sput-object p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    .line 97
    return-void
.end method

.method public static setFingerprintPlus(Z)V
    .locals 0
    .param p0, "isFingerPlus"    # Z

    .prologue
    .line 40
    sput-boolean p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->isFingerprintPlus:Z

    .line 41
    return-void
.end method

.method public static setFingerprintSupplement(Z)V
    .locals 0
    .param p0, "isFingerSupplement"    # Z

    .prologue
    .line 48
    sput-boolean p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->isFingerprintSupplement:Z

    .line 49
    return-void
.end method

.method public static setName(Ljava/lang/String;)V
    .locals 0
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    sput-object p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->knoxName:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public static setPassword(Ljava/lang/String;)V
    .locals 0
    .param p0, "pwd"    # Ljava/lang/String;

    .prologue
    .line 64
    sput-object p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->password:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public static setPasswordType(I)V
    .locals 0
    .param p0, "lockType"    # I

    .prologue
    .line 72
    sput p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->passwordType:I

    .line 73
    return-void
.end method

.method public static setRestore(Z)V
    .locals 0
    .param p0, "restore"    # Z

    .prologue
    .line 133
    sput-boolean p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->isRestore:Z

    .line 134
    return-void
.end method

.method public static setSecurityTimeOut(I)V
    .locals 0
    .param p0, "timeOutVal"    # I

    .prologue
    .line 117
    sput p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->securityTimeOut:I

    .line 118
    return-void
.end method

.method public static setSimplePassword(Z)V
    .locals 0
    .param p0, "value"    # Z

    .prologue
    .line 125
    sput-boolean p0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->simplePassword:Z

    .line 126
    return-void
.end method
