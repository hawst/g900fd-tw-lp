.class final enum Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;
.super Ljava/lang/Enum;
.source "SetupWizardSetPatternActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "RightButtonMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

.field public static final enum Confirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

.field public static final enum ConfirmDisabled:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

.field public static final enum Continue:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

.field public static final enum ContinueDisabled:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

.field public static final enum Ok:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;


# instance fields
.field final enabled:Z

.field final text:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 233
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    const-string v1, "Continue"

    const v2, 0x7f0c008a

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Continue:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    .line 234
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    const-string v1, "ContinueDisabled"

    const v2, 0x7f0c008a

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->ContinueDisabled:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    .line 235
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    const-string v1, "Confirm"

    const v2, 0x7f0c008b

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Confirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    .line 236
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    const-string v1, "ConfirmDisabled"

    const v2, 0x7f0c008b

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->ConfirmDisabled:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    .line 237
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    const-string v1, "Ok"

    const v2, 0x104000a

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Ok:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    .line 232
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Continue:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->ContinueDisabled:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Confirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->ConfirmDisabled:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Ok:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->$VALUES:[Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0
    .param p3, "text"    # I
    .param p4, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)V"
        }
    .end annotation

    .prologue
    .line 243
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 244
    iput p3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->text:I

    .line 245
    iput-boolean p4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->enabled:Z

    .line 246
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 232
    const-class v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->$VALUES:[Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    invoke-virtual {v0}, [Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    return-object v0
.end method
