.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$4;
.super Landroid/os/AsyncTask;
.source "SetupWizardWelcome.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->createPersona()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/os/Bundle;)Ljava/lang/Integer;
    .locals 5
    .param p1, "params"    # [Landroid/os/Bundle;

    .prologue
    .line 415
    const-string v3, "knox-b2c"

    .line 416
    .local v3, "type":Ljava/lang/String;
    const/4 v2, -0x1

    .line 417
    .local v2, "result":I
    const/4 v1, 0x0

    .line 418
    .local v1, "apkUri":Landroid/net/Uri;
    new-instance v0, Ljava/io/File;

    const-string v4, "/system/container/ContainerAgent2/ContainerAgent2.apk"

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 420
    .local v0, "adminF":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 421
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 424
    :cond_0
    if-eqz v1, :cond_1

    .line 425
    invoke-static {v3, v1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->createContainer(Ljava/lang/String;Landroid/net/Uri;)I

    move-result v2

    .line 428
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    return-object v4
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 411
    check-cast p1, [Landroid/os/Bundle;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$4;->doInBackground([Landroid/os/Bundle;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 432
    const-string v0, "KnoxSetupWizardClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const/4 v0, 0x0

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isCreationInProgress:Z
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$1102(Z)Z

    .line 434
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 411
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$4;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
