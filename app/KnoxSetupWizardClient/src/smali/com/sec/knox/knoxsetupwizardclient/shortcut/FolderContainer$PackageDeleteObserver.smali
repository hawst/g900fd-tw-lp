.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "FolderContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageDeleteObserver"
.end annotation


# instance fields
.field bFinished:Z

.field numberOfShouldBeRemoved:I

.field result:I

.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method private constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 1

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    .line 201
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->bFinished:Z

    .line 202
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->result:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    .param p2, "x1"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$1;

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    return-void
.end method

.method private declared-synchronized removeOrDisableTheApp()V
    .locals 3

    .prologue
    .line 225
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->numberOfShouldBeRemoved:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->numberOfShouldBeRemoved:I

    .line 226
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->numberOfShouldBeRemoved:I

    if-nez v0, :cond_0

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->bFinished:Z

    .line 228
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->result:I

    .line 229
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 231
    :cond_0
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->numberOfShouldBeRemoved:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " apps are left to removed or disabled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    monitor-exit p0

    return-void

    .line 225
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private removeTheApp()V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->removeOrDisableTheApp()V

    .line 222
    return-void
.end method


# virtual methods
.method public disableTheApp()V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->removeOrDisableTheApp()V

    .line 218
    return-void
.end method

.method public packageDeleted(Ljava/lang/String;I)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 208
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "packageDeleted: name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 210
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->removeTheApp()V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 212
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->removeOrDisableTheApp()V

    goto :goto_0
.end method

.method public declared-synchronized setNumberOfShouldBeRemoved(I)V
    .locals 3
    .param p1, "num"    # I

    .prologue
    .line 235
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->numberOfShouldBeRemoved:I

    .line 236
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "numberOfShouldBeRemoved= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    monitor-exit p0

    return-void

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
