.class public Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
.super Landroid/app/Activity;
.source "FolderContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;,
        Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;,
        Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field protected static mContext:Landroid/content/Context;


# instance fields
.field LWCFolderReceiver:Landroid/content/BroadcastReceiver;

.field private addIconAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;

.field private allAppsSelected:I

.field final bgImgforDragDrop:[I

.field bottomPosition:I

.field private defaultbg:Landroid/graphics/drawable/Drawable;

.field dragOffset:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private existRemoveableApp:Z

.field private fadeOutAnimation:Landroid/view/animation/Animation;

.field folderOffsetX:F

.field gridMainScale:F

.field iPackageManager:Landroid/content/pm/IPackageManager;

.field iconScale:F

.field public isDragMode:Z

.field isFolderType:Z

.field isMoreLayout:Z

.field private isMoveAppsToContainerAllowed:Z

.field private isOrientationSupported:Z

.field private itemSelector:Landroid/widget/TextView;

.field protected listAppInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mAddApp:Z

.field private mAddGrid:Landroid/widget/GridView;

.field private mBtnAdd:Landroid/widget/TextView;

.field private mBtnDone:Landroid/widget/TextView;

.field protected mBtnLock:Landroid/widget/ImageView;

.field private mBtnLockUnlock:Landroid/widget/TextView;

.field private mBtnMore:Landroid/widget/ImageView;

.field private mBtnRemove:Landroid/widget/TextView;

.field private mBtnSetting:Landroid/widget/TextView;

.field mCallMoveToDeskBack:Z

.field public mCompanyIcon:Landroid/widget/ImageView;

.field mCurrentModeString:Ljava/lang/String;

.field mCurrentPersonaId:I

.field mCurrentUserHandle:Landroid/os/UserHandle;

.field mCurrentUserId:I

.field private mDPM:Landroid/app/admin/DevicePolicyManager;

.field mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

.field private mDeselectAll:Landroid/widget/TextView;

.field mDevicePolicyManager:Landroid/app/admin/IDevicePolicyManager;

.field public mDragDropBG:Landroid/widget/ImageView;

.field mEkm:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

.field private mEmptyListText:Landroid/widget/TextView;

.field private mFolderArea:Landroid/widget/RelativeLayout;

.field private mGrayArea:Landroid/widget/RelativeLayout;

.field private mGrayAreaBg:Landroid/widget/RelativeLayout;

.field public mGridMain:Landroid/widget/GridView;

.field private mImgDivider:Landroid/widget/ImageView;

.field public mIsFadeout:Z

.field public mIsUninstall:Z

.field public mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

.field private mKnoxFolderActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

.field private mLayoutMore:Landroid/widget/LinearLayout;

.field private mListSelectedAppInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mPM:Landroid/os/PersonaManager;

.field private mPopUp:Landroid/widget/PopupWindow;

.field private mPopupDivider:Landroid/widget/ImageView;

.field public mRemoveApp:Z

.field private mSelectAll:Landroid/widget/TextView;

.field private mSeparator:Landroid/widget/ImageView;

.field mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

.field public mTitle:Landroid/widget/TextView;

.field public mUnlockReq:Z

.field matrixLayoutInfos:[I

.field maxGridXCount:I

.field maxGridYCount:I

.field private movedAppPosition:I

.field private onMenuClick:Landroid/view/View$OnClickListener;

.field private pkgInstall:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

.field private presseBg:Landroid/graphics/drawable/Drawable;

.field prevDestPosition:I

.field public tActivityName:Ljava/lang/String;

.field public tPkgName:Ljava/lang/String;

.field topPosition:I

.field typeObj:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const-string v0, "FolderContainer"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    .line 130
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    .line 131
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddApp:Z

    .line 132
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mRemoveApp:Z

    .line 133
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mUnlockReq:Z

    .line 134
    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->tPkgName:Ljava/lang/String;

    .line 135
    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->tActivityName:Ljava/lang/String;

    .line 145
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isFolderType:Z

    .line 148
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCallMoveToDeskBack:Z

    .line 149
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    .line 151
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isDragMode:Z

    .line 152
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsFadeout:Z

    .line 160
    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->matrixLayoutInfos:[I

    .line 161
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->bgImgforDragDrop:[I

    .line 172
    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDevicePolicyManager:Landroid/app/admin/IDevicePolicyManager;

    .line 177
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isOrientationSupported:Z

    .line 186
    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->allAppsSelected:I

    .line 192
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->existRemoveableApp:Z

    .line 194
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    .line 582
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->onMenuClick:Landroid/view/View$OnClickListener;

    .line 1835
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$13;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$13;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->LWCFolderReceiver:Landroid/content/BroadcastReceiver;

    return-void

    .line 161
    nop

    :array_0
    .array-data 4
        0x7f020020
        0x7f020021
        0x7f020022
        0x7f020023
        0x7f020025
        0x7f020026
        0x7f020027
    .end array-data
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->presseBg:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSelectAll:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDeselectAll:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopupDivider:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->allAppsSelected:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->allAppsSelected:I

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->defaultbg:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGrayArea:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setMoreLayoutBtnEnabling()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mEmptyListText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mFolderArea:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxFolderActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->addIconAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showRemoveLayout(Z)V

    return-void
.end method

.method private displayhHelpPopup(IZ)V
    .locals 4
    .param p1, "personaId"    # I
    .param p2, "value"    # Z

    .prologue
    .line 554
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    invoke-virtual {v1, p1}, Landroid/os/PersonaManager;->isKioskModeEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 563
    :goto_0
    return-void

    .line 558
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 559
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.knox.containeragent2"

    const-string v3, "com.sec.knox.containeragent2.ui.help.KnoxHelpLauncher"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 560
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 561
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/os/UserHandle;

    invoke-direct {v2, p1}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 562
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    const/4 v2, 0x2

    invoke-virtual {v1, p1, v2, p2}, Landroid/os/PersonaManager;->setShownHelp(IIZ)V

    goto :goto_0
.end method

.method private fadeInFolderContainer()V
    .locals 4

    .prologue
    .line 1433
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGrayAreaBg:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_0

    .line 1440
    :goto_0
    return-void

    .line 1436
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1437
    .local v0, "fadeIn":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1438
    const-wide/16 v2, 0x1db

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1439
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGrayAreaBg:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private fadeOutFolderContainerWithFinish()V
    .locals 2

    .prologue
    .line 1479
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGrayArea:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_1

    .line 1486
    :cond_0
    :goto_0
    return-void

    .line 1482
    :cond_1
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsFadeout:Z

    if-nez v0, :cond_0

    .line 1485
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGrayArea:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->fadeOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private getPopupDisplay()Landroid/widget/PopupWindow;
    .locals 6

    .prologue
    const/4 v5, -0x2

    .line 567
    sget-object v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 568
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030015

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 569
    .local v2, "view":Landroid/view/View;
    new-instance v1, Landroid/widget/PopupWindow;

    invoke-direct {v1, v2, v5, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    .line 570
    .local v1, "popupWindow":Landroid/widget/PopupWindow;
    const v3, 0x7f0f0076

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSelectAll:Landroid/widget/TextView;

    .line 571
    const v3, 0x7f0f0078

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDeselectAll:Landroid/widget/TextView;

    .line 572
    const v3, 0x7f0f0077

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopupDivider:Landroid/widget/ImageView;

    .line 574
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSelectAll:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->onMenuClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 575
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDeselectAll:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->onMenuClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 577
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 579
    return-object v1
.end method

.method private initAnimation()V
    .locals 4

    .prologue
    .line 1443
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->fadeOutAnimation:Landroid/view/animation/Animation;

    .line 1444
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->fadeOutAnimation:Landroid/view/animation/Animation;

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1445
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->fadeOutAnimation:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1447
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$12;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$12;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    .line 1465
    .local v0, "animationListener":Landroid/view/animation/Animation$AnimationListener;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->fadeOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1466
    return-void
.end method

.method private refreshNumberOfSelected()V
    .locals 5

    .prologue
    .line 742
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    const v2, 0x7f0c00f7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 743
    return-void
.end method

.method private setMoreLayoutBtnEnabling()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 821
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->existRemoveableApp:Z

    if-nez v0, :cond_0

    .line 822
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnRemove:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 826
    :goto_0
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isMoveAppsToContainerAllowed:Z

    if-nez v0, :cond_1

    .line 827
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnAdd:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 848
    :goto_1
    return-void

    .line 824
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnRemove:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 829
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->pkgInstall:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    if-nez v0, :cond_3

    .line 830
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-direct {v0, v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->pkgInstall:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    .line 831
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->pkgInstall:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->getPackagesToInstall()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 832
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnAdd:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    .line 834
    :cond_2
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnAdd:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    .line 838
    :cond_3
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->pkgInstall:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->getPackagesToInstall()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 840
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnAdd:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    .line 843
    :cond_4
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnAdd:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1
.end method

.method private showRemoveLayout(Z)V
    .locals 3
    .param p1, "bShow"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 859
    if-eqz p1, :cond_2

    .line 860
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnSetting:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 861
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnSetting:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 862
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mImgDivider:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 863
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mImgDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 864
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnMore:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 865
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnDone:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 867
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->refreshNumberOfSelected()V

    .line 879
    :goto_0
    return-void

    .line 870
    :cond_2
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnSetting:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 871
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnSetting:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 872
    :cond_3
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mImgDivider:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 873
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mImgDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 874
    :cond_4
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnMore:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 875
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnDone:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 877
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentPersonaId:I

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setFolderTitle(I)V

    goto :goto_0
.end method


# virtual methods
.method public ItemLongClick(Landroid/view/View;)Z
    .locals 18
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1095
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    const/4 v14, 0x1

    if-ne v13, v14, :cond_0

    .line 1096
    const/4 v13, 0x1

    .line 1174
    :goto_0
    return v13

    .line 1098
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    new-instance v14, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1100
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;

    invoke-virtual {v13}, Landroid/widget/GridView;->getVisibility()I

    move-result v13

    const/16 v14, 0x8

    if-ne v13, v14, :cond_1

    .line 1101
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setVisibility(I)V

    .line 1103
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 1105
    .local v2, "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    iget v10, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->position:I

    .line 1107
    .local v10, "position":I
    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I

    .line 1108
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I

    if-nez v13, :cond_2

    .line 1110
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    .line 1112
    :cond_2
    new-instance v8, Landroid/content/ClipData$Item;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v8, v13}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    .line 1113
    .local v8, "item":Landroid/content/ClipData$Item;
    const v13, 0x7f0f0060

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 1115
    .local v12, "tvName":Landroid/widget/TextView;
    const/4 v13, 0x1

    new-array v9, v13, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "text/plain"

    aput-object v14, v9, v13

    .line 1116
    .local v9, "mimeTypes":[Ljava/lang/String;
    new-instance v4, Landroid/content/ClipData;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v4, v13, v9, v8}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    .line 1117
    .local v4, "data":Landroid/content/ClipData;
    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1119
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    if-ge v6, v13, :cond_3

    .line 1120
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1125
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    int-to-double v14, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridXCount:I

    int-to-double v0, v13

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v14

    double-to-int v7, v14

    .line 1126
    .local v7, "indexBg":I
    if-eqz v7, :cond_4

    .line 1127
    add-int/lit8 v7, v7, -0x1

    .line 1130
    :cond_4
    const/4 v13, 0x4

    if-le v7, v13, :cond_5

    const/4 v7, 0x4

    .line 1132
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDragDropBG:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->bgImgforDragDrop:[I

    const/4 v15, 0x0

    aget v14, v14, v15

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1133
    new-instance v11, Landroid/view/View$DragShadowBuilder;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 1136
    .local v11, "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    const/4 v13, 0x0

    :try_start_0
    move-object/from16 v0, p1

    move-object/from16 v1, p1

    invoke-virtual {v0, v4, v11, v1, v13}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1152
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->gridMainScale:F

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setScaleX(F)V

    .line 1153
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->gridMainScale:F

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setScaleY(F)V

    .line 1155
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iconScale:F

    invoke-virtual {v13, v14}, Landroid/view/View;->setScaleX(F)V

    .line 1156
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iconScale:F

    invoke-virtual {v13, v14}, Landroid/view/View;->setScaleY(F)V

    .line 1157
    const/4 v6, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    if-ge v6, v13, :cond_6

    .line 1160
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v13}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v13

    invoke-interface {v13, v6}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 1161
    .local v3, "appInfo2":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    iget-object v13, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iconScale:F

    invoke-virtual {v13, v14}, Landroid/view/View;->setScaleX(F)V

    .line 1162
    iget-object v13, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iconScale:F

    invoke-virtual {v13, v14}, Landroid/view/View;->setScaleY(F)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1157
    .end local v3    # "appInfo2":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1142
    :catch_0
    move-exception v5

    .line 1144
    .local v5, "e":Ljava/lang/Exception;
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isDragMode:Z

    .line 1145
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setOriginalGridView()V

    .line 1146
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v13}, Landroid/widget/GridView;->invalidate()V

    .line 1147
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDragDropBG:Landroid/widget/ImageView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1148
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1149
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 1174
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_6
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 1164
    :catch_1
    move-exception v13

    goto :goto_3
.end method

.method public createOutline(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "src"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 1827
    if-eqz p1, :cond_0

    .line 1828
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 1829
    .local v0, "p":Landroid/graphics/Paint;
    new-instance v2, Landroid/graphics/BlurMaskFilter;

    const/high16 v3, 0x41a00000    # 20.0f

    sget-object v4, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v2, v3, v4}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 1830
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1832
    .end local v0    # "p":Landroid/graphics/Paint;
    :cond_0
    return-object v1
.end method

.method public displayGridIconsAnimation(III)V
    .locals 20
    .param p1, "destInAir"    # I
    .param p2, "sourceInAir"    # I
    .param p3, "sourcePosition"    # I

    .prologue
    .line 1785
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v3}, Landroid/widget/GridView;->getNumColumns()I

    move-result v12

    .line 1786
    .local v12, "columnCount":I
    rem-int v15, p2, v12

    .line 1787
    .local v15, "sourceCol":I
    div-int v16, p2, v12

    .line 1788
    .local v16, "sourceRow":I
    rem-int v13, p1, v12

    .line 1789
    .local v13, "destCol":I
    div-int v14, p1, v12

    .line 1791
    .local v14, "destRow":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 1792
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1795
    :cond_0
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->folderOffsetX:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    rem-int/2addr v3, v12

    sub-int v3, v15, v3

    int-to-float v3, v3

    mul-float v4, v5, v3

    .line 1796
    .local v4, "fromX":F
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->folderOffsetX:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    rem-int/2addr v3, v12

    sub-int v3, v13, v3

    int-to-float v3, v3

    mul-float v6, v5, v3

    .line 1797
    .local v6, "toX":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    div-int/2addr v3, v12

    sub-int v3, v16, v3

    int-to-float v8, v3

    .line 1798
    .local v8, "fromY":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    div-int/2addr v3, v12

    sub-int v3, v14, v3

    int-to-float v10, v3

    .line 1800
    .local v10, "toY":F
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x1

    const/4 v5, 0x1

    const/4 v7, 0x1

    const/4 v9, 0x1

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 1805
    .local v2, "trans":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v18, 0xc8

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1806
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 1808
    if-ltz p2, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, p2

    if-ge v0, v3, :cond_1

    .line 1809
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v7}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v7

    sub-int/2addr v3, v7

    invoke-virtual {v5, v3}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1811
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v7}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v7

    sub-int/2addr v3, v7

    invoke-virtual {v5, v3}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1823
    :cond_1
    :goto_0
    return-void

    .line 1815
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v3}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v5, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 1816
    .local v11, "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    iget-object v3, v11, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public enableDoneBtn(Z)V
    .locals 2
    .param p1, "status"    # Z

    .prologue
    .line 1469
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnDone:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1470
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnDone:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1471
    if-eqz p1, :cond_1

    .line 1472
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnDone:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1476
    :cond_0
    :goto_0
    return-void

    .line 1474
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnDone:Landroid/widget/TextView;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public insertAppInfo(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;)J
    .locals 4
    .param p1, "aInfo"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .prologue
    .line 1422
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1424
    .local v0, "initialValues":Landroid/content/ContentValues;
    const-string v1, "personaId"

    iget v2, p1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPersonaId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1425
    const-string v1, "packageName"

    iget-object v2, p1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1426
    const-string v1, "shortcutName"

    iget-object v2, p1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1427
    const-string v1, "listOrder"

    iget v2, p1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->position:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1428
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    iget-object v3, p1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->writeBitmap(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V

    .line 1429
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    invoke-virtual {v1, v0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->createShortcut(Landroid/content/ContentValues;)J

    move-result-wide v2

    return-wide v2
.end method

.method public loadAppList()V
    .locals 45

    .prologue
    .line 1178
    sget-object v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Time:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1179
    sget-object v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v40

    .line 1185
    .local v40, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1186
    const/4 v14, 0x0

    .line 1188
    .local v14, "c":Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->fetchAllShortcuts(I)Landroid/database/Cursor;

    move-result-object v14

    .line 1189
    :cond_0
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 1190
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    invoke-virtual {v4, v14}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->getShortcutInfo(Landroid/database/Cursor;)Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v34

    .line 1194
    .local v34, "m":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    :try_start_2
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move-object/from16 v0, v34

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->icon:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1195
    .local v3, "icon":Landroid/graphics/drawable/Drawable;
    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    const/4 v4, 0x0

    move-object/from16 v0, v34

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->packageName:Ljava/lang/String;

    move-object/from16 v0, v34

    iget v6, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    move-object/from16 v0, v34

    iget v7, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->_id:I

    move-object/from16 v0, v34

    iget-object v8, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->uri:Ljava/lang/String;

    move-object/from16 v0, v34

    iget v9, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->badgeCount:I

    invoke-direct/range {v2 .. v9}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    .line 1196
    .local v2, "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->position:I

    .line 1197
    move-object/from16 v0, v34

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    iput-object v4, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    .line 1198
    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getPkgName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.knox.shortcutsms"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1199
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1201
    .end local v2    # "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v3    # "icon":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v4

    goto :goto_0

    .line 1207
    .end local v34    # "m":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    :cond_1
    if-eqz v14, :cond_2

    .line 1208
    :try_start_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1211
    :cond_2
    :goto_1
    new-instance v31, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    move-object/from16 v0, v31

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1212
    .local v31, "launcherIntent":Landroid/content/Intent;
    const-string v4, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1213
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iPackageManager:Landroid/content/pm/IPackageManager;

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    move-object/from16 v0, v31

    invoke-interface {v4, v0, v5, v7, v8}, Landroid/content/pm/IPackageManager;->queryIntentActivities(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    move-result-object v15

    .line 1214
    .local v15, "containerApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iPackageManager:Landroid/content/pm/IPackageManager;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v31

    invoke-interface {v4, v0, v5, v7, v8}, Landroid/content/pm/IPackageManager;->queryIntentActivities(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    move-result-object v38

    .line 1216
    .local v38, "ownerApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v16

    .line 1217
    .local v16, "count":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDevicePolicyManager:Landroid/app/admin/IDevicePolicyManager;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-interface {v4, v5, v7}, Landroid/app/admin/IDevicePolicyManager;->getAllowBrowser(Landroid/content/ComponentName;I)Z

    move-result v26

    .line 1219
    .local v26, "isAllowedBrowser":Z
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_2
    move/from16 v0, v23

    move/from16 v1, v16

    if-ge v0, v1, :cond_e

    .line 1221
    move/from16 v0, v23

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Landroid/content/pm/ResolveInfo;

    .line 1222
    .local v42, "rInfo":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v42

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1223
    .local v6, "name":Ljava/lang/String;
    move-object/from16 v0, v42

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v39, v0

    .line 1225
    .local v39, "pkgName":Ljava/lang/String;
    const-string v4, "com.android.settings"

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.sec.knox.switcher"

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.sec.knox.containeragent2"

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.sec.android.app.music"

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.samsung.everglades.video"

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.sec.knox.shortcutsms"

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1219
    :cond_3
    :goto_3
    add-int/lit8 v23, v23, 0x1

    goto :goto_2

    .line 1204
    .end local v6    # "name":Ljava/lang/String;
    .end local v15    # "containerApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v16    # "count":I
    .end local v23    # "i":I
    .end local v26    # "isAllowedBrowser":Z
    .end local v31    # "launcherIntent":Landroid/content/Intent;
    .end local v38    # "ownerApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v39    # "pkgName":Ljava/lang/String;
    .end local v42    # "rInfo":Landroid/content/pm/ResolveInfo;
    :catch_1
    move-exception v4

    .line 1207
    if-eqz v14, :cond_2

    .line 1208
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    .line 1380
    .end local v14    # "c":Landroid/database/Cursor;
    :catch_2
    move-exception v20

    .line 1382
    .local v20, "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    .line 1385
    .end local v20    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_4
    const/16 v23, 0x0

    .restart local v23    # "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v23

    if-ge v0, v4, :cond_1e

    .line 1386
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1385
    add-int/lit8 v23, v23, 0x1

    goto :goto_5

    .line 1207
    .end local v23    # "i":I
    .restart local v14    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v4

    if-eqz v14, :cond_5

    .line 1208
    :try_start_4
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v4

    .line 1233
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v15    # "containerApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v16    # "count":I
    .restart local v23    # "i":I
    .restart local v26    # "isAllowedBrowser":Z
    .restart local v31    # "launcherIntent":Landroid/content/Intent;
    .restart local v38    # "ownerApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v39    # "pkgName":Ljava/lang/String;
    .restart local v42    # "rInfo":Landroid/content/pm/ResolveInfo;
    :cond_6
    if-nez v26, :cond_8

    .line 1235
    const/16 v21, 0x0

    .line 1236
    .local v21, "findBrowser":Z
    sget-object v13, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->defaultHideBrowserPkg:[Ljava/lang/String;

    .local v13, "arr$":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v32, v0

    .local v32, "len$":I
    const/16 v24, 0x0

    .local v24, "i$":I
    :goto_6
    move/from16 v0, v24

    move/from16 v1, v32

    if-ge v0, v1, :cond_7

    aget-object v22, v13, v24

    .line 1237
    .local v22, "gPkg":Ljava/lang/String;
    move-object/from16 v0, v39

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_a

    .line 1238
    const/16 v21, 0x1

    .line 1242
    .end local v22    # "gPkg":Ljava/lang/String;
    :cond_7
    if-nez v21, :cond_3

    .line 1246
    .end local v13    # "arr$":[Ljava/lang/String;
    .end local v21    # "findBrowser":Z
    .end local v24    # "i$":I
    .end local v32    # "len$":I
    :cond_8
    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    const/4 v5, 0x0

    move-object/from16 v0, v42

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v4, v2

    invoke-direct/range {v4 .. v11}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    .line 1247
    .restart local v2    # "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->position:I

    .line 1248
    move-object/from16 v0, v42

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iput-object v4, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    .line 1251
    const/16 v25, -0x1

    .line 1253
    .local v25, "index":I
    const/16 v41, 0x0

    .line 1254
    .local v41, "pmAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    const/16 v30, 0x0

    .local v30, "j":I
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v30

    if-ge v0, v4, :cond_9

    .line 1256
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move/from16 v0, v30

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v41

    .end local v41    # "pmAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    check-cast v41, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 1258
    .restart local v41    # "pmAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    move-object/from16 v0, v42

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_b

    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    iget-object v5, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_b

    .line 1262
    iget-object v4, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mName:Ljava/lang/String;

    move-object/from16 v0, v41

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mName:Ljava/lang/String;

    .line 1263
    move/from16 v25, v30

    .line 1268
    :cond_9
    const/4 v4, -0x1

    move/from16 v0, v25

    if-ne v0, v4, :cond_c

    .line 1270
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserHandle:Landroid/os/UserHandle;

    move-object/from16 v0, v42

    invoke-virtual {v4, v0, v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getBadgedIcon(Landroid/content/pm/ResolveInfo;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 1271
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->insertAppInfo(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mDbId:J

    .line 1272
    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->isContained:Z

    .line 1273
    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getPkgName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.knox.shortcutsms"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1274
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 1236
    .end local v2    # "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v25    # "index":I
    .end local v30    # "j":I
    .end local v41    # "pmAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .restart local v13    # "arr$":[Ljava/lang/String;
    .restart local v21    # "findBrowser":Z
    .restart local v22    # "gPkg":Ljava/lang/String;
    .restart local v24    # "i$":I
    .restart local v32    # "len$":I
    :cond_a
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_6

    .line 1254
    .end local v13    # "arr$":[Ljava/lang/String;
    .end local v21    # "findBrowser":Z
    .end local v22    # "gPkg":Ljava/lang/String;
    .end local v24    # "i$":I
    .end local v32    # "len$":I
    .restart local v2    # "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .restart local v25    # "index":I
    .restart local v30    # "j":I
    .restart local v41    # "pmAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    :cond_b
    add-int/lit8 v30, v30, 0x1

    goto :goto_7

    .line 1279
    :cond_c
    if-eqz v41, :cond_3

    .line 1280
    const/4 v4, 0x1

    move-object/from16 v0, v41

    iput-boolean v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->isContained:Z

    .line 1282
    invoke-interface/range {v38 .. v38}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    .local v24, "i$":Ljava/util/Iterator;
    :cond_d
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Landroid/content/pm/ResolveInfo;

    .line 1283
    .local v37, "ownerApp":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v42

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v37

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    move-object/from16 v0, v42

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v37

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1285
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserHandle:Landroid/os/UserHandle;

    move-object/from16 v0, v37

    invoke-virtual {v4, v0, v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getBadgedIcon(Landroid/content/pm/ResolveInfo;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, v41

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mIcon:Landroid/graphics/drawable/Drawable;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_3

    .line 1295
    .end local v2    # "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v6    # "name":Ljava/lang/String;
    .end local v24    # "i$":Ljava/util/Iterator;
    .end local v25    # "index":I
    .end local v30    # "j":I
    .end local v37    # "ownerApp":Landroid/content/pm/ResolveInfo;
    .end local v39    # "pkgName":Ljava/lang/String;
    .end local v41    # "pmAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v42    # "rInfo":Landroid/content/pm/ResolveInfo;
    :cond_e
    const/16 v41, 0x0

    .line 1297
    .restart local v41    # "pmAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    const/16 v30, 0x0

    .restart local v30    # "j":I
    :goto_8
    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v30

    if-ge v0, v4, :cond_10

    .line 1299
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move/from16 v0, v30

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    move-object/from16 v41, v0

    .line 1300
    move-object/from16 v0, v41

    iget-boolean v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->isContained:Z

    if-nez v4, :cond_f

    .line 1302
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    move-object/from16 v0, v41

    iget-wide v8, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mDbId:J

    invoke-virtual {v4, v5, v8, v9}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->deleteShortcut(IJ)I

    .line 1303
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move/from16 v0, v30

    invoke-interface {v4, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 1304
    add-int/lit8 v30, v30, -0x1

    .line 1297
    :cond_f
    add-int/lit8 v30, v30, 0x1

    goto :goto_8

    .line 1309
    :catch_3
    move-exception v4

    .line 1317
    :cond_10
    const/4 v4, 0x0

    :try_start_6
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->existRemoveableApp:Z

    .line 1318
    const/16 v27, 0x0

    .line 1319
    .local v27, "isDefaultApp":Z
    new-instance v43, Ljava/util/ArrayList;

    invoke-direct/range {v43 .. v43}, Ljava/util/ArrayList;-><init>()V

    .line 1321
    .local v43, "removableList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    const/16 v30, 0x0

    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v30

    if-ge v0, v4, :cond_1c

    .line 1323
    const/16 v28, 0x0

    .line 1324
    .local v28, "isMdmPackage":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move/from16 v0, v30

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v41

    .end local v41    # "pmAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    check-cast v41, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 1325
    .restart local v41    # "pmAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iPackageManager:Landroid/content/pm/IPackageManager;

    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    const/16 v7, 0x80

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-interface {v4, v5, v7, v8}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 1327
    .local v2, "appInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v13, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->mdmPackages:[Ljava/lang/String;

    .restart local v13    # "arr$":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v32, v0

    .restart local v32    # "len$":I
    const/16 v24, 0x0

    .local v24, "i$":I
    :goto_a
    move/from16 v0, v24

    move/from16 v1, v32

    if-ge v0, v1, :cond_11

    aget-object v22, v13, v24

    .line 1328
    .restart local v22    # "gPkg":Ljava/lang/String;
    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_14

    .line 1329
    const/16 v28, 0x1

    .line 1334
    .end local v22    # "gPkg":Ljava/lang/String;
    :cond_11
    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    const-string v5, "com.sec.knox.containeragent2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_15

    .line 1335
    const/4 v4, 0x1

    move-object/from16 v0, v41

    iput v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->removeableFlag:I

    .line 1336
    const/16 v27, 0x1

    .line 1367
    :cond_12
    :goto_b
    if-nez v27, :cond_13

    .line 1368
    move-object/from16 v0, v43

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1369
    :cond_13
    const/16 v27, 0x0

    .line 1321
    add-int/lit8 v30, v30, 0x1

    goto :goto_9

    .line 1327
    .restart local v22    # "gPkg":Ljava/lang/String;
    :cond_14
    add-int/lit8 v24, v24, 0x1

    goto :goto_a

    .line 1338
    .end local v22    # "gPkg":Ljava/lang/String;
    :cond_15
    iget v4, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v4, v4, 0x1

    const/4 v5, 0x1

    if-eq v4, v5, :cond_16

    iget v4, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_19

    .line 1341
    :cond_16
    const/16 v29, 0x0

    .line 1343
    .local v29, "isSetValue":Z
    sget-object v13, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->defaultPreloadedApps:[Ljava/lang/String;

    array-length v0, v13

    move/from16 v32, v0

    const/16 v24, 0x0

    :goto_c
    move/from16 v0, v24

    move/from16 v1, v32

    if-ge v0, v1, :cond_17

    aget-object v22, v13, v24

    .line 1344
    .restart local v22    # "gPkg":Ljava/lang/String;
    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_18

    .line 1345
    const/4 v4, 0x1

    move-object/from16 v0, v41

    iput v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->removeableFlag:I

    .line 1346
    const/16 v29, 0x1

    .line 1347
    const/16 v27, 0x1

    .line 1352
    .end local v22    # "gPkg":Ljava/lang/String;
    :cond_17
    if-nez v29, :cond_12

    .line 1353
    const/4 v4, 0x2

    move-object/from16 v0, v41

    iput v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->removeableFlag:I

    .line 1354
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->existRemoveableApp:Z

    goto :goto_b

    .line 1343
    .restart local v22    # "gPkg":Ljava/lang/String;
    :cond_18
    add-int/lit8 v24, v24, 0x1

    goto :goto_c

    .line 1358
    .end local v22    # "gPkg":Ljava/lang/String;
    .end local v29    # "isSetValue":Z
    :cond_19
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v4

    sget-object v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v4, v5, v7}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getKnoxContainerManager(Landroid/content/Context;I)Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v12

    .line 1359
    .local v12, "appPolicy":Landroid/app/enterprise/ApplicationPolicy;
    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    invoke-virtual {v12, v4}, Landroid/app/enterprise/ApplicationPolicy;->getApplicationUninstallationEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDPM:Landroid/app/admin/DevicePolicyManager;

    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/app/admin/DevicePolicyManager;->packageHasActiveAdmins(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1a

    if-eqz v28, :cond_1b

    .line 1360
    :cond_1a
    const/4 v4, 0x1

    move-object/from16 v0, v41

    iput v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->removeableFlag:I

    .line 1361
    const/16 v27, 0x1

    goto/16 :goto_b

    .line 1363
    :cond_1b
    const/4 v4, 0x3

    move-object/from16 v0, v41

    iput v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->removeableFlag:I

    .line 1364
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->existRemoveableApp:Z

    goto/16 :goto_b

    .line 1371
    .end local v2    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v12    # "appPolicy":Landroid/app/enterprise/ApplicationPolicy;
    .end local v13    # "arr$":[Ljava/lang/String;
    .end local v24    # "i$":I
    .end local v28    # "isMdmPackage":Z
    .end local v32    # "len$":I
    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    if-eqz v4, :cond_1d

    .line 1372
    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    .line 1374
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_4

    .line 1375
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mEmptyListText:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setVisibility(I)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_4

    .line 1389
    .end local v14    # "c":Landroid/database/Cursor;
    .end local v15    # "containerApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v16    # "count":I
    .end local v26    # "isAllowedBrowser":Z
    .end local v27    # "isDefaultApp":Z
    .end local v30    # "j":I
    .end local v31    # "launcherIntent":Landroid/content/Intent;
    .end local v38    # "ownerApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v41    # "pmAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v43    # "removableList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mFolderArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v33

    .line 1390
    .local v33, "lparam":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    int-to-double v4, v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridXCount:I

    int-to-double v8, v7

    div-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    move/from16 v35, v0

    .line 1391
    .local v35, "numberOfLines":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridYCount:I

    move/from16 v0, v35

    if-le v0, v4, :cond_1f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridYCount:I

    move/from16 v35, v0

    .line 1392
    :cond_1f
    if-nez v35, :cond_20

    const/16 v35, 0x1

    .line 1393
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    if-nez v4, :cond_21

    .line 1394
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->matrixLayoutInfos:[I

    add-int/lit8 v7, v35, -0x1

    add-int/lit8 v7, v7, 0x4

    aget v5, v5, v7

    invoke-virtual {v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getPixelbyDp(I)I

    move-result v4

    move-object/from16 v0, v33

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1397
    :cond_21
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v17

    .line 1398
    .local v17, "displayMetrics":Landroid/util/DisplayMetrics;
    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    move-object/from16 v0, v17

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v19, v4, v5

    .line 1399
    .local v19, "dpWidth":F
    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    move-object/from16 v0, v17

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v18, v4, v5

    .line 1401
    .local v18, "dpHeight":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v0, v4, Landroid/content/res/Configuration;->orientation:I

    move/from16 v36, v0

    .line 1402
    .local v36, "orientation":I
    const/16 v44, 0x0

    .line 1403
    .local v44, "strOrientation":Ljava/lang/String;
    const/4 v4, 0x2

    move/from16 v0, v36

    if-ne v0, v4, :cond_23

    .line 1404
    const-string v44, "Landscape"

    .line 1408
    :goto_d
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isOrientationSupported:Z

    if-eqz v4, :cond_22

    const-string v4, "Landscape"

    move-object/from16 v0, v44

    if-ne v0, v4, :cond_22

    .line 1411
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    const/high16 v5, 0x40400000    # 3.0f

    div-float v5, v18, v5

    sub-float v5, v19, v5

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getPixelbyDp(I)I

    move-result v4

    move-object/from16 v0, v33

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1415
    :cond_22
    sget-object v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "numberOfLines= "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1416
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mFolderArea:Landroid/widget/RelativeLayout;

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1417
    sget-object v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Time2:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1418
    return-void

    .line 1406
    :cond_23
    const-string v44, "Portrait"

    goto :goto_d
.end method

.method public moveTaskToBackground()V
    .locals 1

    .prologue
    .line 1539
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    .line 1540
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->fadeOutFolderContainerWithFinish()V

    .line 1549
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 18
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 376
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 377
    sget-object v13, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Time:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->isLauncherOrientationSupported()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isOrientationSupported:Z

    .line 380
    const v13, 0x7f030003

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setContentView(I)V

    .line 381
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxFolderActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .line 382
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxFolderActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getBaseContext()Landroid/content/Context;

    move-result-object v13

    sput-object v13, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    .line 383
    sget-object v13, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    .line 384
    sget-object v13, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getInstance(Landroid/content/Context;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    .line 386
    new-instance v3, Landroid/content/IntentFilter;

    const-string v13, "com.sec.knox.container.action.containerremovalstarted"

    invoke-direct {v3, v13}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 387
    .local v3, "filter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->LWCFolderReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 389
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mEkm:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    .line 390
    const-string v13, "device_policy"

    invoke-static {v13}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v13

    invoke-static {v13}, Landroid/app/admin/IDevicePolicyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/admin/IDevicePolicyManager;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDevicePolicyManager:Landroid/app/admin/IDevicePolicyManager;

    .line 391
    sget-object v13, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    const-string v14, "device_policy"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/app/admin/DevicePolicyManager;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDPM:Landroid/app/admin/DevicePolicyManager;

    .line 393
    const v13, 0x7f0f0027

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mImgDivider:Landroid/widget/ImageView;

    .line 394
    const v13, 0x7f0f0017

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLock:Landroid/widget/ImageView;

    .line 395
    const v13, 0x7f0f0014

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    .line 396
    const v13, 0x7f0f0011

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCompanyIcon:Landroid/widget/ImageView;

    .line 397
    const v13, 0x7f0f0019

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnMore:Landroid/widget/ImageView;

    .line 399
    const v13, 0x7f0f001f

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mLayoutMore:Landroid/widget/LinearLayout;

    .line 400
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isMoreLayout:Z

    .line 401
    const v13, 0x7f0f0020

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLockUnlock:Landroid/widget/TextView;

    .line 402
    const v13, 0x7f0f0021

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSeparator:Landroid/widget/ImageView;

    .line 403
    const v13, 0x7f0f0022

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnAdd:Landroid/widget/TextView;

    .line 404
    const v13, 0x7f0f0023

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnRemove:Landroid/widget/TextView;

    .line 405
    const v13, 0x7f0f0024

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnSetting:Landroid/widget/TextView;

    .line 407
    const v13, 0x7f0f001a

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnDone:Landroid/widget/TextView;

    .line 408
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0200b2

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->defaultbg:Landroid/graphics/drawable/Drawable;

    .line 409
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0200b3

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->presseBg:Landroid/graphics/drawable/Drawable;

    .line 410
    const v13, 0x7f0f001d

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/GridView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;

    .line 411
    const v13, 0x7f0f0015

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;

    .line 412
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->defaultbg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 413
    invoke-direct/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getPopupDisplay()Landroid/widget/PopupWindow;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;

    .line 414
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setSpinnerItems(I)V

    .line 416
    const v13, 0x7f0f000f

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGrayArea:Landroid/widget/RelativeLayout;

    .line 417
    const v13, 0x7f0f0025

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGrayAreaBg:Landroid/widget/RelativeLayout;

    .line 419
    const v13, 0x7f0f0010

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mFolderArea:Landroid/widget/RelativeLayout;

    .line 422
    const v13, 0x7f0f001c

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/GridView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    .line 423
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    new-instance v14, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 430
    const v13, 0x7f0f001b

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDragDropBG:Landroid/widget/ImageView;

    .line 431
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 432
    .local v8, "res":Landroid/content/res/Resources;
    const v13, 0x7f070001

    invoke-virtual {v8, v13}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->matrixLayoutInfos:[I

    .line 433
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->matrixLayoutInfos:[I

    const/4 v14, 0x0

    aget v13, v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridXCount:I

    .line 434
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->matrixLayoutInfos:[I

    const/4 v14, 0x1

    aget v13, v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridYCount:I

    .line 435
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->matrixLayoutInfos:[I

    const/4 v14, 0x2

    aget v13, v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->topPosition:I

    .line 436
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->matrixLayoutInfos:[I

    const/4 v14, 0x3

    aget v13, v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->bottomPosition:I

    .line 437
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridXCount:I

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 439
    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    .line 440
    .local v10, "typedValue":Landroid/util/TypedValue;
    const v13, 0x7f0a000f

    const/4 v14, 0x1

    invoke-virtual {v8, v13, v10, v14}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 441
    invoke-virtual {v10}, Landroid/util/TypedValue;->getFloat()F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->folderOffsetX:F

    .line 442
    const v13, 0x7f0a0010

    const/4 v14, 0x1

    invoke-virtual {v8, v13, v10, v14}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 443
    invoke-virtual {v10}, Landroid/util/TypedValue;->getFloat()F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->gridMainScale:F

    .line 444
    const v13, 0x7f0a0011

    const/4 v14, 0x1

    invoke-virtual {v8, v13, v10, v14}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 445
    invoke-virtual {v10}, Landroid/util/TypedValue;->getFloat()F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iconScale:F

    .line 447
    const v13, 0x7f0f001e

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mEmptyListText:Landroid/widget/TextView;

    .line 449
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 451
    .local v5, "intent":Landroid/content/Intent;
    if-eqz v5, :cond_0

    .line 452
    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 453
    .local v2, "extras":Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 456
    :try_start_0
    const-string v13, "personaId"

    invoke-virtual {v2, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentPersonaId:I

    .line 457
    const-string v13, "folderMode"

    invoke-virtual {v2, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentModeString:Ljava/lang/String;

    .line 458
    const-string v13, "userId"

    invoke-virtual {v2, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    .line 459
    new-instance v13, Landroid/os/UserHandle;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-direct {v13, v14}, Landroid/os/UserHandle;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserHandle:Landroid/os/UserHandle;

    .line 461
    const-string v13, "lwc"

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentModeString:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_0

    .line 462
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isFolderType:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    .end local v2    # "extras":Landroid/os/Bundle;
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    if-nez v13, :cond_1

    .line 472
    const/16 v13, 0x64

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    .line 476
    :cond_1
    const-string v13, "package"

    invoke-static {v13}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v13

    invoke-static {v13}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iPackageManager:Landroid/content/pm/IPackageManager;

    .line 478
    sget-object v13, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    const-string v14, "persona"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/os/PersonaManager;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    .line 481
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    invoke-virtual {v13}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v7

    .line 483
    .local v7, "pList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    .line 485
    .local v9, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v9, :cond_2

    .line 487
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/PersonaInfo;

    .line 488
    .local v6, "pInfo":Landroid/content/pm/PersonaInfo;
    const-string v13, "user"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/os/UserManager;

    .line 489
    .local v12, "um":Landroid/os/UserManager;
    iget v13, v6, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v12, v13}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v11

    .line 491
    .local v11, "ui":Landroid/content/pm/UserInfo;
    if-eqz v11, :cond_4

    iget-object v13, v11, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    const-string v14, "KNOX"

    invoke-virtual {v13, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_4

    iget v13, v6, Landroid/content/pm/PersonaInfo;->id:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    if-ne v13, v14, :cond_4

    .line 492
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentPersonaId:I

    .line 501
    .end local v6    # "pInfo":Landroid/content/pm/PersonaInfo;
    .end local v11    # "ui":Landroid/content/pm/UserInfo;
    .end local v12    # "um":Landroid/os/UserManager;
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-static {v13}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getConfigurationType(I)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->typeObj:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 503
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;

    new-instance v14, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 537
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setCallBack()V

    .line 538
    invoke-direct/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->initAnimation()V

    .line 540
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    if-eqz v13, :cond_7

    .line 541
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v13, v14}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v6

    .line 542
    .restart local v6    # "pInfo":Landroid/content/pm/PersonaInfo;
    if-nez v6, :cond_6

    .line 543
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->finish()V

    .line 550
    .end local v6    # "pInfo":Landroid/content/pm/PersonaInfo;
    :cond_3
    :goto_3
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    .line 551
    return-void

    .line 494
    .restart local v6    # "pInfo":Landroid/content/pm/PersonaInfo;
    .restart local v11    # "ui":Landroid/content/pm/UserInfo;
    .restart local v12    # "um":Landroid/os/UserManager;
    :cond_4
    if-eqz v11, :cond_5

    iget-object v13, v11, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    const-string v14, "KNOX II"

    invoke-virtual {v13, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_5

    iget v13, v6, Landroid/content/pm/PersonaInfo;->id:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    if-ne v13, v14, :cond_5

    .line 495
    const/4 v13, 0x2

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentPersonaId:I

    goto :goto_2

    .line 485
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 544
    .end local v11    # "ui":Landroid/content/pm/UserInfo;
    .end local v12    # "um":Landroid/os/UserManager;
    :cond_6
    iget-boolean v13, v6, Landroid/content/pm/PersonaInfo;->shownFolderHelp:Z

    if-nez v13, :cond_3

    .line 545
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->displayhHelpPopup(IZ)V

    goto :goto_3

    .line 547
    .end local v6    # "pInfo":Landroid/content/pm/PersonaInfo;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->finish()V

    goto :goto_3

    .line 463
    .end local v4    # "i":I
    .end local v7    # "pList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    .end local v9    # "size":I
    .restart local v2    # "extras":Landroid/os/Bundle;
    :catch_0
    move-exception v13

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1492
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 1498
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->LWCFolderReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1499
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->LWCFolderReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1500
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1501
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    .line 1518
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    .line 1520
    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    if-eqz v1, :cond_0

    .line 1521
    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    .line 1522
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1523
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1524
    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setSpinnerItems(I)V

    .line 1525
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->onResume()V

    .line 1526
    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentPersonaId:I

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setFolderTitle(I)V

    .line 1527
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1528
    const/4 v0, 0x1

    .line 1534
    :goto_0
    return v0

    .line 1530
    :cond_0
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->moveTaskToBackground()V

    goto :goto_0

    .line 1534
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 1505
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1510
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    if-eqz v0, :cond_0

    .line 1511
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getUninstallSelectedItems()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mListSelectedAppInfo:Ljava/util/List;

    .line 1513
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 614
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    iget v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v3, v6}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v3

    iget-boolean v3, v3, Landroid/content/pm/PersonaInfo;->isLightWeightContainer:Z

    if-nez v3, :cond_0

    .line 615
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->finish()V

    .line 616
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 721
    :goto_0
    return-void

    .line 620
    :cond_0
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->fadeInFolderContainer()V

    .line 622
    iget-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mUnlockReq:Z

    if-ne v3, v4, :cond_7

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    iget v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v3, v6}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v3

    sget-object v6, Landroid/content/pm/PersonaState;->ACTIVE:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v6}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 623
    iget-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddApp:Z

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mRemoveApp:Z

    if-eqz v3, :cond_2

    .line 624
    :cond_1
    iget-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddApp:Z

    if-nez v3, :cond_4

    move v3, v4

    :goto_1
    iput-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    .line 625
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->loadAppList()V

    .line 626
    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxFolderActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-direct {v3, v6, v7}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Ljava/util/List;)V

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    .line 627
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    iget-boolean v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    invoke-virtual {v3, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->setUninstallMode(Z)V

    .line 628
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v3, v6}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 631
    :cond_2
    invoke-virtual {p0, v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showMoreLayout(Z)V

    .line 632
    iget-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddApp:Z

    if-eqz v3, :cond_5

    .line 633
    new-instance v1, Landroid/content/Intent;

    sget-object v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 634
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 635
    const-string v3, "userid"

    iget v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 636
    sget-object v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 637
    iput-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddApp:Z

    .line 654
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_3
    :goto_2
    iput-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mUnlockReq:Z

    .line 655
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showUnlockStatus()V

    .line 694
    :goto_3
    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;

    sget-object v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->addIconAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;

    .line 695
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->addIconAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridXCount:I

    invoke-virtual {v3, v4, v6}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->setIconCount(II)V

    .line 696
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;

    iget v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridXCount:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 697
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->addIconAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 698
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setVisibility(I)V

    .line 700
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    iget v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v3

    sget-object v4, Landroid/content/pm/PersonaState;->LOCKED:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 702
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showLockStatus()V

    .line 713
    :goto_4
    iput-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isDragMode:Z

    .line 714
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setOriginalGridView()V

    .line 715
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDragDropBG:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 716
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v3, v9}, Landroid/widget/GridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 718
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 719
    sget-object v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Time:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    move v3, v5

    .line 624
    goto/16 :goto_1

    .line 638
    :cond_5
    iget-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mRemoveApp:Z

    if-eqz v3, :cond_6

    .line 639
    invoke-direct {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showRemoveLayout(Z)V

    .line 640
    iput-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mRemoveApp:Z

    goto :goto_2

    .line 642
    :cond_6
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->tPkgName:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->tActivityName:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 643
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 644
    .restart local v1    # "intent":Landroid/content/Intent;
    new-instance v0, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->tPkgName:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->tActivityName:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    .local v0, "component":Landroid/content/ComponentName;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 646
    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 647
    const/high16 v3, 0x10200000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 648
    new-instance v3, Landroid/os/UserHandle;

    iget v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {p0, v1, v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 649
    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->tPkgName:Ljava/lang/String;

    .line 650
    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->tActivityName:Ljava/lang/String;

    goto/16 :goto_2

    .line 659
    .end local v0    # "component":Landroid/content/ComponentName;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_7
    iput-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mUnlockReq:Z

    .line 660
    iput-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCallMoveToDeskBack:Z

    .line 662
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->loadAppList()V

    .line 664
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_8

    .line 666
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mEmptyListText:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 667
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setVisibility(I)V

    .line 669
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mFolderArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 670
    .local v2, "lparam":Landroid/view/ViewGroup$LayoutParams;
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->matrixLayoutInfos:[I

    const/4 v6, 0x4

    aget v4, v4, v6

    invoke-virtual {v3, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getPixelbyDp(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 671
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mFolderArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 691
    .end local v2    # "lparam":Landroid/view/ViewGroup$LayoutParams;
    :goto_5
    iget-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    invoke-direct {p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showRemoveLayout(Z)V

    goto/16 :goto_3

    .line 675
    :cond_8
    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-direct {v3, p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Ljava/util/List;)V

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    .line 676
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    iget-boolean v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    invoke-virtual {v3, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->setUninstallMode(Z)V

    .line 677
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v3, v6}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 680
    iget-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    if-eqz v3, :cond_9

    .line 681
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mListSelectedAppInfo:Ljava/util/List;

    invoke-virtual {v3, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->setUninstallSelectedItems(Ljava/util/List;)I

    move-result v3

    if-eqz v3, :cond_a

    .line 682
    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->enableDoneBtn(Z)V

    .line 685
    :goto_6
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->notifyDataSetChanged()V

    .line 688
    :cond_9
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mEmptyListText:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 689
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v3, v5}, Landroid/widget/GridView;->setVisibility(I)V

    goto :goto_5

    .line 684
    :cond_a
    invoke-virtual {p0, v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->enableDoneBtn(Z)V

    goto :goto_6

    .line 704
    :cond_b
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    iget v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v3

    sget-object v4, Landroid/content/pm/PersonaState;->ACTIVE:Landroid/content/pm/PersonaState;

    invoke-virtual {v3, v4}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 706
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showUnlockStatus()V

    goto/16 :goto_4

    .line 710
    :cond_c
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showLockStatus()V

    goto/16 :goto_4
.end method

.method public setCallBack()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 882
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v2, v5}, Landroid/widget/GridView;->setLongClickable(Z)V

    .line 883
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v2, v5}, Landroid/widget/GridView;->setClickable(Z)V

    .line 884
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$4;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$4;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 896
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 898
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnSetting:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 899
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnSetting:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$5;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$5;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 940
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLockUnlock:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 941
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLockUnlock:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 965
    :cond_1
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnMore:Landroid/widget/ImageView;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$7;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$7;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 975
    const/4 v0, 0x0

    .line 976
    .local v0, "containerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mEkm:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    if-eqz v2, :cond_2

    .line 977
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mEkm:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxFolderActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getKnoxContainerManager(Landroid/content/Context;I)Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    move-result-object v0

    .line 979
    :cond_2
    if-eqz v0, :cond_3

    .line 981
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getRCPPolicy()Lcom/sec/enterprise/knox/container/RCPPolicy;

    move-result-object v1

    .line 982
    .local v1, "rcpPolicy":Lcom/sec/enterprise/knox/container/RCPPolicy;
    if-eqz v1, :cond_3

    .line 984
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/RCPPolicy;->isMoveAppsToContainerAllowed()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isMoveAppsToContainerAllowed:Z

    .line 985
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnAdd:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1018
    .end local v1    # "rcpPolicy":Lcom/sec/enterprise/knox/container/RCPPolicy;
    :cond_3
    :goto_0
    iget-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isMoveAppsToContainerAllowed:Z

    if-eqz v2, :cond_4

    .line 1020
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnAdd:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1027
    :goto_1
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnRemove:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1060
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnDone:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1078
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGrayArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1079
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGrayArea:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$11;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$11;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1089
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mFolderArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1090
    return-void

    .line 1024
    :cond_4
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnAdd:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    .line 1013
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public setFolderTitle(I)V
    .locals 9
    .param p1, "personaId"    # I

    .prologue
    const v8, 0x7f02001f

    const v7, 0x7f02001e

    .line 756
    const/4 v4, 0x0

    .line 757
    .local v4, "title":Ljava/lang/String;
    const/4 v2, 0x0

    .line 759
    .local v2, "folderIconPath":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->typeObj:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    instance-of v5, v5, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    if-eqz v5, :cond_0

    .line 760
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->typeObj:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    check-cast v5, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    invoke-virtual {v5}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->getFolderHeaderIcon()Ljava/lang/String;

    move-result-object v2

    .line 761
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->typeObj:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    check-cast v5, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    invoke-virtual {v5}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->getFolderHeaderTitle()Ljava/lang/String;

    move-result-object v4

    .line 766
    :cond_0
    iget v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentPersonaId:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    .line 768
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 772
    :try_start_0
    invoke-static {v2}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 773
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCompanyIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 783
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :goto_0
    const-string v5, "com.sec.knox.bridge/com.sec.knox.bridge.activity.SwitchB2BActivity"

    invoke-static {v5, p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 784
    .local v3, "newPersonaName":Ljava/lang/String;
    sget-object v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "newPersonaName= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_3

    .line 786
    if-eqz v3, :cond_2

    .line 787
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 818
    .end local v3    # "newPersonaName":Ljava/lang/String;
    :goto_1
    return-void

    .line 775
    :catch_0
    move-exception v1

    .line 777
    .local v1, "e":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCompanyIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 781
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCompanyIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 789
    .restart local v3    # "newPersonaName":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 792
    :cond_3
    if-eqz v3, :cond_4

    .line 793
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 795
    :cond_4
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    sget-object v6, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00f5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 798
    .end local v3    # "newPersonaName":Ljava/lang/String;
    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_6

    .line 802
    :try_start_1
    invoke-static {v2}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 803
    .restart local v0    # "d":Landroid/graphics/drawable/Drawable;
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCompanyIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 813
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :goto_2
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_7

    .line 814
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 805
    :catch_1
    move-exception v1

    .line 807
    .restart local v1    # "e":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCompanyIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 811
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCompanyIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 816
    :cond_7
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    sget-object v6, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00f6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setOriginalGridView()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1553
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setScaleX(F)V

    .line 1554
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setScaleY(F)V

    .line 1555
    return-void
.end method

.method public setSpinnerItems(I)V
    .locals 5
    .param p1, "count"    # I

    .prologue
    const/4 v4, 0x0

    .line 746
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    const v2, 0x7f0c00f7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 747
    .local v0, "selectedCount":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 748
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 749
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 750
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->defaultbg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 751
    iput v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->allAppsSelected:I

    .line 752
    return-void
.end method

.method public showLockStatus()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 733
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLock:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLock:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 735
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLockUnlock:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 736
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLockUnlock:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 737
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSeparator:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 738
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSeparator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 739
    :cond_2
    return-void
.end method

.method public showMoreLayout(Z)V
    .locals 2
    .param p1, "bShow"    # Z

    .prologue
    .line 851
    iput-boolean p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isMoreLayout:Z

    .line 852
    if-eqz p1, :cond_0

    .line 853
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mLayoutMore:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 856
    :goto_0
    return-void

    .line 855
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mLayoutMore:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public showUnlockStatus()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 724
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLock:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 725
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLock:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 726
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLockUnlock:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 727
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mBtnLockUnlock:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 728
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSeparator:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 729
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSeparator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 730
    :cond_2
    return-void
.end method
