.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$3;
.super Ljava/lang/Object;
.source "SetupWizardFingerPlusActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->setCheckedEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 189
    const-string v0, "KnoxSetupWizardClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 201
    :goto_0
    const-string v0, "KnoxSetupWizardClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checked index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->access$000()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->setConfirmButton()V
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V

    .line 203
    return-void

    .line 192
    :pswitch_0
    const/4 v0, 0x0

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->access$002(I)I

    goto :goto_0

    .line 195
    :pswitch_1
    const/4 v0, 0x2

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->access$002(I)I

    goto :goto_0

    .line 198
    :pswitch_2
    const/4 v0, 0x3

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->access$002(I)I

    goto :goto_0

    .line 190
    :pswitch_data_0
    .packed-switch 0x7f0f008a
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
