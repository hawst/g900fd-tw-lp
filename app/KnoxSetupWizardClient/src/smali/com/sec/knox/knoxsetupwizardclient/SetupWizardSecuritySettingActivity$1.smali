.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$1;
.super Ljava/lang/Object;
.source "SetupWizardSecuritySettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setEventCallBack()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 178
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 181
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setFingerprintPlus(Z)V

    .line 182
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$000()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 211
    const-string v1, "LOCK_QUALITY"

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$000()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 214
    :goto_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setResult(ILandroid/content/Intent;)V

    .line 215
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->finish()V

    .line 216
    return-void

    .line 184
    :pswitch_0
    const-string v1, "password selected"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 186
    const-string v1, "LOCK_QUALITY"

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$000()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 189
    :pswitch_1
    const-string v1, "fingerprint selected"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 191
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setFingerprintSupplement(Z)V

    .line 192
    const-string v1, "LOCK_QUALITY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 196
    :pswitch_2
    const-string v1, "PIN selected"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 198
    const-string v1, "LOCK_QUALITY"

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$000()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 201
    :pswitch_3
    const-string v1, "pattern selected"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 203
    const-string v1, "LOCK_QUALITY"

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$000()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 206
    :pswitch_4
    const-string v1, "fingerprints plus selected"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 207
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setFingerprintPlus(Z)V

    .line 208
    const-string v1, "LOCK_QUALITY"

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$000()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
