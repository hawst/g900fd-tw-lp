.class public final enum Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;
.super Ljava/lang/Enum;
.source "SetupWizardChooseLockPassword.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "Stage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

.field public static final enum ConfirmWrong:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

.field public static final enum Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

.field public static final enum NeedToConfirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;


# instance fields
.field public final alphaHint:I

.field public final buttonText:I

.field public final cacNumeric:I

.field public final numericHint:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 203
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    const-string v1, "Introduction"

    const/4 v2, 0x0

    const v3, 0x7f0c00c8

    const v4, 0x7f0c00ca

    const v5, 0x7f0c00cb

    const v6, 0x7f0c0083

    invoke-direct/range {v0 .. v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    .line 208
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    const-string v1, "NeedToConfirm"

    const/4 v2, 0x1

    const v3, 0x7f0c0084

    const v4, 0x7f0c0085

    const v5, 0x7f0c00cc

    const v6, 0x7f0c00cd

    invoke-direct/range {v0 .. v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->NeedToConfirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    .line 213
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    const-string v1, "ConfirmWrong"

    const/4 v2, 0x2

    const v3, 0x7f0c00ce

    const v4, 0x7f0c00cf

    const v5, 0x7f0c00d0

    const v6, 0x7f0c0083

    invoke-direct/range {v0 .. v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->ConfirmWrong:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    .line 201
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->NeedToConfirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->ConfirmWrong:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->$VALUES:[Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0
    .param p3, "hintInAlpha"    # I
    .param p4, "hintInNumeric"    # I
    .param p5, "hintCacNumeric"    # I
    .param p6, "nextButtonText"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII)V"
        }
    .end annotation

    .prologue
    .line 221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 222
    iput p3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->alphaHint:I

    .line 223
    iput p4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->numericHint:I

    .line 224
    iput p5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->cacNumeric:I

    .line 225
    iput p6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->buttonText:I

    .line 226
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 201
    const-class v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    return-object v0
.end method

.method public static values()[Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;
    .locals 1

    .prologue
    .line 201
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->$VALUES:[Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    invoke-virtual {v0}, [Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    return-object v0
.end method
