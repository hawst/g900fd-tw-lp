.class public Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;
.super Landroid/widget/BaseAdapter;
.source "KnoxFolderAppInfoAdapter.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

.field private mContext:Landroid/content/Context;

.field private mIsScrollMode:I

.field private mIsUninstallMode:Z

.field private mListAppInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;


# direct methods
.method public constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Ljava/util/List;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .line 94
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mContext:Landroid/content/Context;

    .line 95
    iput-object p2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    .line 96
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getInstance(Landroid/content/Context;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    .line 97
    return-void
.end method

.method static synthetic access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mIsUninstallMode:Z

    return v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 140
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedItemCount()I
    .locals 6

    .prologue
    .line 317
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 318
    .local v2, "size":I
    const/4 v3, 0x0

    .line 321
    .local v3, "totalCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 323
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 324
    .local v1, "kInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIsSelected()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 325
    add-int/lit8 v3, v3, 0x1

    .line 321
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 327
    .end local v1    # "kInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    :cond_1
    return v3
.end method

.method public getSelectedItemList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 333
    .local v2, "rList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 335
    .local v3, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 337
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 338
    .local v1, "kInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIsSelected()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 339
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342
    .end local v1    # "kInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    :cond_1
    return-object v2
.end method

.method public getUninstallSelectedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 146
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 157
    .local v2, "entry":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    if-eqz p2, :cond_0

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-boolean v8, v8, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isDragMode:Z

    if-eqz v8, :cond_2

    .line 158
    :cond_0
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 159
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v8, 0x7f03000b

    const/4 v9, 0x0

    invoke-virtual {v3, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    iput-object p2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    .line 167
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    .restart local p2    # "convertView":Landroid/view/View;
    :goto_0
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    const v9, 0x7f0f005c

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 168
    .local v5, "ivIcon":Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v10, v10, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserHandle:Landroid/os/UserHandle;

    invoke-virtual {v8, v9, v10}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 170
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    const v9, 0x7f0f0060

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 171
    .local v7, "tvName":Landroid/widget/TextView;
    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    const v9, 0x7f0f005d

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 174
    .local v6, "ivUninstallIcon":Landroid/widget/ImageView;
    iget-boolean v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mIsUninstallMode:Z

    if-eqz v8, :cond_5

    .line 175
    iget v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->removeableFlag:I

    const/4 v9, 0x1

    if-eq v8, v9, :cond_4

    .line 176
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 179
    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIsSelected()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    .line 180
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020034

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 184
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    :goto_1
    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 191
    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    :goto_2
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    const v9, 0x7f0f005e

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 192
    .local v4, "ivBadgeIcon":Landroid/widget/ImageView;
    iget-boolean v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mIsUninstallMode:Z

    if-nez v8, :cond_7

    .line 193
    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getBadgeCount()I

    move-result v0

    .line 196
    .local v0, "badgeCount":I
    if-eqz v0, :cond_6

    .line 197
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v8, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->createBadgeDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 199
    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 212
    .end local v0    # "badgeCount":I
    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    :goto_3
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 213
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    new-instance v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;

    invoke-direct {v9, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    new-instance v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$2;

    invoke-direct {v9, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 291
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-boolean v8, v8, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isDragMode:Z

    if-eqz v8, :cond_1

    .line 293
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    const v9, 0x3f59999a    # 0.85f

    invoke-virtual {v8, v9}, Landroid/view/View;->setScaleX(F)V

    .line 294
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    const v9, 0x3f59999a    # 0.85f

    invoke-virtual {v8, v9}, Landroid/view/View;->setScaleY(F)V

    .line 297
    :cond_1
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    invoke-virtual {v8, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 300
    iget-object v8, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    return-object v8

    .line 163
    .end local v4    # "ivBadgeIcon":Landroid/widget/ImageView;
    .end local v5    # "ivIcon":Landroid/widget/ImageView;
    .end local v6    # "ivUninstallIcon":Landroid/widget/ImageView;
    .end local v7    # "tvName":Landroid/widget/TextView;
    :cond_2
    iput-object p2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    goto/16 :goto_0

    .line 182
    .restart local v5    # "ivIcon":Landroid/widget/ImageView;
    .restart local v6    # "ivUninstallIcon":Landroid/widget/ImageView;
    .restart local v7    # "tvName":Landroid/widget/TextView;
    :cond_3
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020033

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    goto :goto_1

    .line 187
    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    :cond_4
    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 189
    :cond_5
    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 202
    .restart local v0    # "badgeCount":I
    .restart local v4    # "ivBadgeIcon":Landroid/widget/ImageView;
    :cond_6
    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 209
    .end local v0    # "badgeCount":I
    :cond_7
    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "me"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    .line 101
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 102
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 104
    .local v1, "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    const v3, 0x7f0f005c

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 105
    .local v2, "ivIcon":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-boolean v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isMoreLayout:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-boolean v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isDragMode:Z

    if-eqz v3, :cond_1

    .line 124
    :cond_0
    :goto_0
    return v5

    .line 110
    :cond_1
    if-nez v0, :cond_2

    .line 111
    iget-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mIsUninstallMode:Z

    if-nez v3, :cond_0

    .line 113
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->visualizeIcon(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 116
    :cond_2
    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    .line 117
    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 118
    :cond_3
    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    .line 119
    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 121
    :cond_4
    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public scrollStateChanged(I)V
    .locals 0
    .param p1, "status"    # I

    .prologue
    .line 305
    iput p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mIsScrollMode:I

    .line 306
    return-void
.end method

.method public setAppSelectionStatus(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 347
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 348
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    invoke-virtual {v1, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->setIsSelected(I)V

    .line 347
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 350
    :cond_0
    return-void
.end method

.method public setUninstallMode(Z)V
    .locals 2
    .param p1, "mode"    # Z

    .prologue
    .line 310
    iput-boolean p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mIsUninstallMode:Z

    .line 311
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mIsUninstallMode:Z

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->enableDoneBtn(Z)V

    .line 313
    :cond_0
    return-void
.end method

.method public setUninstallSelectedItems(Ljava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 357
    .local p1, "listSelectedAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    const-string v5, "KnoxFolderAppInfoAdapter"

    const-string v6, "setUninstallSelectedItmes start"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    if-nez p1, :cond_1

    .line 359
    const/4 v4, 0x0

    .line 381
    :cond_0
    return v4

    .line 363
    :cond_1
    const/4 v4, 0x0

    .line 364
    .local v4, "result":I
    const-string v5, "KnoxFolderAppInfoAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mListSelectedAppInfo.size()= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const-string v5, "KnoxFolderAppInfoAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mListAppInfo.size()= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 368
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 369
    .local v1, "iInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 370
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mListAppInfo:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 371
    .local v3, "jInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    iget-object v5, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    iget-object v6, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    iget-object v6, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 372
    const-string v5, "KnoxFolderAppInfoAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "pkgName= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", activityName= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->setIsSelected(I)V

    .line 374
    add-int/lit8 v4, v4, 0x1

    .line 367
    .end local v3    # "jInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 369
    .restart local v3    # "jInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
