.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;
.super Landroid/app/Activity;
.source "SetupWizardKnoxNameSettingActivity.java"


# static fields
.field private static currentNumOfKnox:I

.field private static isCreationInProgress:Z


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private final DEFAULT_KNOX_NAME:Ljava/lang/String;

.field private final SECOND_KNOX_NAME:Ljava/lang/String;

.field private btn_next:Landroid/widget/LinearLayout;

.field private btn_restore:Landroid/widget/LinearLayout;

.field private et_knoxNameField:Landroid/widget/TextView;

.field private img_prev:Landroid/widget/ImageView;

.field private initialiseHandler:Landroid/os/Handler;

.field private isPhone:Z

.field private isTablet:Z

.field private iv_knox:Landroid/widget/ImageView;

.field private iv_knoxIcon:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mIsDestroyed:Z

.field private mPersona:Landroid/os/PersonaManager;

.field private pd:Landroid/app/ProgressDialog;

.field private personaName:Ljava/lang/String;

.field private personaPolicy:Landroid/os/PersonaPolicyManager;

.field private prefs:Landroid/content/SharedPreferences;

.field private receiver:Landroid/content/BroadcastReceiver;

.field private ssa:Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;

.field private text_next:Landroid/widget/TextView;

.field private text_restore:Landroid/widget/TextView;

.field private tv_knoxNameMessage:Landroid/widget/TextView;

.field private tv_knoxNameMessage2:Landroid/widget/TextView;

.field private wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->currentNumOfKnox:I

    .line 80
    sput-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->isCreationInProgress:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->ACTIVITY_NAME:Ljava/lang/String;

    .line 50
    const-string v0, "KNOX"

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->DEFAULT_KNOX_NAME:Ljava/lang/String;

    .line 51
    const-string v0, "KNOX II"

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->SECOND_KNOX_NAME:Ljava/lang/String;

    .line 65
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mContext:Landroid/content/Context;

    .line 66
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaPolicy:Landroid/os/PersonaPolicyManager;

    .line 68
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mPersona:Landroid/os/PersonaManager;

    .line 70
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knox:Landroid/widget/ImageView;

    .line 71
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage:Landroid/widget/TextView;

    .line 72
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage2:Landroid/widget/TextView;

    .line 73
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    .line 74
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->et_knoxNameField:Landroid/widget/TextView;

    .line 75
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_next:Landroid/widget/LinearLayout;

    .line 76
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_restore:Landroid/widget/LinearLayout;

    .line 77
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    .line 81
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;

    .line 82
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->prefs:Landroid/content/SharedPreferences;

    .line 83
    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mIsDestroyed:Z

    .line 86
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->img_prev:Landroid/widget/ImageView;

    .line 87
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->text_restore:Landroid/widget/TextView;

    .line 89
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->text_next:Landroid/widget/TextView;

    .line 91
    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->isTablet:Z

    .line 93
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 95
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->receiver:Landroid/content/BroadcastReceiver;

    .line 485
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->initialiseHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_next:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->lockRotation()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;Landroid/content/Context;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Z

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->createPersona(Landroid/content/Context;Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->initialiseHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->lockStatusBar(Z)V

    return-void
.end method

.method static synthetic access$602(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 45
    sput-boolean p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->isCreationInProgress:Z

    return p0
.end method

.method static synthetic access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mIsDestroyed:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/os/PersonaManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mPersona:Landroid/os/PersonaManager;

    return-object v0
.end method

.method private createPersona(Landroid/content/Context;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isRestore"    # Z

    .prologue
    const/4 v3, 0x1

    .line 452
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 453
    .local v0, "pm":Landroid/os/PowerManager;
    const/16 v1, 0xa

    const-string v2, "My wakelook"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 454
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 456
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_next:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 457
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_next:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 459
    :cond_0
    sget-boolean v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->isCreationInProgress:Z

    if-nez v1, :cond_1

    .line 460
    sput-boolean v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->isCreationInProgress:Z

    .line 461
    invoke-direct {p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->lockStatusBar(Z)V

    .line 463
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setName(Ljava/lang/String;)V

    .line 464
    invoke-static {p2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setRestore(Z)V

    .line 466
    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;

    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$4;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V

    invoke-direct {v1, p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;-><init>(Landroid/content/Context;Lcom/sec/knox/knoxsetupwizardclient/ContainerCreationListener;)V

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->start()V

    .line 483
    :cond_1
    return-void
.end method

.method private getOtherPersonaName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "personaName"    # Ljava/lang/String;

    .prologue
    .line 112
    const-string v0, "KNOX"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    const-string v0, "KNOX II"

    .line 116
    :goto_0
    return-object v0

    .line 114
    :cond_0
    const-string v0, "KNOX II"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    const-string v0, "KNOX"

    goto :goto_0

    .line 116
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 150
    const/4 v3, 0x0

    .line 151
    .local v3, "newPersonaName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 153
    .local v2, "newPersonaIcon":Landroid/graphics/drawable/Drawable;
    const-string v7, "persona"

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/PersonaManager;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mPersona:Landroid/os/PersonaManager;

    .line 154
    const-string v7, "PERSONA_INFO"

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->prefs:Landroid/content/SharedPreferences;

    .line 156
    const v7, 0x7f0c0001

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 157
    const v7, 0x7f0f008d

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knox:Landroid/widget/ImageView;

    .line 158
    const v7, 0x7f0f008f

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage:Landroid/widget/TextView;

    .line 159
    const v7, 0x7f0f0090

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage2:Landroid/widget/TextView;

    .line 160
    const v7, 0x7f0f008e

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->et_knoxNameField:Landroid/widget/TextView;

    .line 161
    const v7, 0x7f0f0073

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    .line 162
    const v7, 0x7f0f006c

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_next:Landroid/widget/LinearLayout;

    .line 163
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_next:Landroid/widget/LinearLayout;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 164
    const v7, 0x7f0f006b

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_restore:Landroid/widget/LinearLayout;

    .line 166
    const v7, 0x7f0f0036

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->img_prev:Landroid/widget/ImageView;

    .line 167
    const v7, 0x7f0f0072

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->text_restore:Landroid/widget/TextView;

    .line 169
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->img_prev:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 170
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->text_restore:Landroid/widget/TextView;

    const v8, 0x7f0c00c1

    invoke-virtual {p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    const v7, 0x7f0f006d

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->text_next:Landroid/widget/TextView;

    .line 173
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->text_next:Landroid/widget/TextView;

    const v8, 0x7f0c000a

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 175
    invoke-static {p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->isTablet:Z

    .line 177
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getContainerType()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->isRestoreFile()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 179
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_restore:Landroid/widget/LinearLayout;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 185
    :goto_0
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getCOMEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 186
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage:Landroid/widget/TextView;

    const v8, 0x7f0c0107

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 189
    :cond_0
    if-eqz p1, :cond_9

    .line 190
    const-string v7, "KEY_CURRENT_NUMBER_OF_KNOX"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    sput v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->currentNumOfKnox:I

    .line 191
    const-string v7, "KEY_KNOX_NAME"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    .line 192
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->et_knoxNameField:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    const-string v8, "KNOX"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 194
    const-string v7, "com.sec.knox.bridge/com.sec.knox.bridge.activity.SwitchB2BActivity"

    invoke-static {v7, p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 195
    if-eqz v3, :cond_1

    .line 196
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->et_knoxNameField:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    :cond_1
    invoke-static {p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->hasKnoxIconChanged(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 199
    if-eqz v2, :cond_4

    .line 200
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 215
    :goto_1
    sget v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->currentNumOfKnox:I

    if-lez v7, :cond_8

    .line 216
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage2:Landroid/widget/TextView;

    const v8, 0x7f0c00b7

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    invoke-direct {p0, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getOtherPersonaName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage2:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 285
    :cond_2
    :goto_2
    new-instance v7, Landroid/app/ProgressDialog;

    new-instance v8, Landroid/view/ContextThemeWrapper;

    const v9, 0x103012b

    invoke-direct {v8, p0, v9}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v7, v8}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;

    .line 289
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 290
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c0005

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 291
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 294
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_next:Landroid/widget/LinearLayout;

    new-instance v8, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$2;

    invoke-direct {v8, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 315
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_restore:Landroid/widget/LinearLayout;

    new-instance v8, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$3;

    invoke-direct {v8, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$3;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    return-void

    .line 181
    :cond_3
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_restore:Landroid/widget/LinearLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 182
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->text_restore:Landroid/widget/TextView;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 202
    :cond_4
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020019

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 204
    :cond_5
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    const-string v8, "KNOX II"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 205
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f02001a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 208
    :cond_6
    invoke-static {p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->hasKnoxIconChanged(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 209
    if-eqz v2, :cond_7

    .line 210
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 212
    :cond_7
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020019

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 219
    :cond_8
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage2:Landroid/widget/TextView;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 222
    :cond_9
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mPersona:Landroid/os/PersonaManager;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    sput v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->currentNumOfKnox:I

    .line 223
    const/4 v0, 0x0

    .line 224
    .local v0, "countRemovePersona":I
    sget v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->currentNumOfKnox:I

    if-lez v7, :cond_f

    .line 225
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage2:Landroid/widget/TextView;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 226
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mPersona:Landroid/os/PersonaManager;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PersonaInfo;

    .line 227
    .local v4, "pi":Landroid/content/pm/PersonaInfo;
    iget-boolean v7, v4, Landroid/content/pm/PersonaInfo;->removePersona:Z

    if-nez v7, :cond_c

    .line 228
    const-string v7, "user"

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/UserManager;

    .line 229
    .local v6, "um":Landroid/os/UserManager;
    iget v7, v4, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v6, v7}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v5

    .line 230
    .local v5, "ui":Landroid/content/pm/UserInfo;
    if-eqz v5, :cond_b

    iget-object v7, v5, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    const-string v8, "KNOX"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 231
    const-string v7, "KNOX II"

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    .line 232
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f02001a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 239
    :goto_4
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->et_knoxNameField:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage2:Landroid/widget/TextView;

    const v8, 0x7f0c00b7

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    invoke-direct {p0, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getOtherPersonaName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage2:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 249
    .end local v4    # "pi":Landroid/content/pm/PersonaInfo;
    .end local v5    # "ui":Landroid/content/pm/UserInfo;
    .end local v6    # "um":Landroid/os/UserManager;
    :cond_a
    sget v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->currentNumOfKnox:I

    if-ne v0, v7, :cond_2

    .line 250
    const-string v7, "KNOX"

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    .line 251
    const-string v7, "com.sec.knox.bridge/com.sec.knox.bridge.activity.SwitchB2BActivity"

    invoke-static {v7, p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 252
    invoke-static {p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->hasKnoxIconChanged(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 253
    if-eqz v2, :cond_d

    .line 254
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 258
    :goto_5
    if-eqz v3, :cond_e

    .line 259
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->et_knoxNameField:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    :goto_6
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage2:Landroid/widget/TextView;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 235
    .restart local v4    # "pi":Landroid/content/pm/PersonaInfo;
    .restart local v5    # "ui":Landroid/content/pm/UserInfo;
    .restart local v6    # "um":Landroid/os/UserManager;
    :cond_b
    const-string v7, "KNOX"

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    .line 236
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020019

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 245
    .end local v5    # "ui":Landroid/content/pm/UserInfo;
    .end local v6    # "um":Landroid/os/UserManager;
    :cond_c
    add-int/lit8 v0, v0, 0x1

    .line 247
    goto/16 :goto_3

    .line 256
    .end local v4    # "pi":Landroid/content/pm/PersonaInfo;
    :cond_d
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020019

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_5

    .line 261
    :cond_e
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->et_knoxNameField:Landroid/widget/TextView;

    const-string v8, "KNOX"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 266
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_f
    const-string v7, "KNOX"

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    .line 268
    const-string v7, "com.sec.knox.bridge/com.sec.knox.bridge.activity.SwitchB2BActivity"

    invoke-static {v7, p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->hasKnoxTitleChanged(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 269
    invoke-static {p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->hasKnoxIconChanged(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 270
    if-eqz v2, :cond_10

    .line 271
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 275
    :goto_7
    if-eqz v3, :cond_11

    .line 276
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->et_knoxNameField:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    :goto_8
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->tv_knoxNameMessage2:Landroid/widget/TextView;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 273
    :cond_10
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->iv_knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020019

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7

    .line 278
    :cond_11
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->et_knoxNameField:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->personaName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_8
.end method

.method private isRestoreFile()Z
    .locals 3

    .prologue
    .line 537
    const-string v0, "/mnt/sdcard/knox_backup/KnoxBackup_KNOX.zip"

    .line 538
    .local v0, "backupFolder":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    const-string v2, "/mnt/sdcard/knox_backup/KnoxBackup_KNOX.zip"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 539
    .local v1, "fileSource":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 540
    const/4 v2, 0x1

    .line 541
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private lockRotation()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 343
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v0, v2, Landroid/content/res/Configuration;->orientation:I

    .line 344
    .local v0, "orientation":I
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 346
    .local v1, "rotation":I
    packed-switch v1, :pswitch_data_0

    .line 376
    if-ne v0, v3, :cond_4

    .line 377
    const/4 v0, 0x0

    .line 383
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->setRequestedOrientation(I)V

    .line 384
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 385
    return-void

    .line 348
    :pswitch_0
    if-ne v0, v3, :cond_0

    .line 349
    const/4 v0, 0x0

    goto :goto_0

    .line 351
    :cond_0
    const/4 v0, 0x1

    .line 353
    goto :goto_0

    .line 355
    :pswitch_1
    if-ne v0, v3, :cond_1

    .line 356
    const/4 v0, 0x0

    goto :goto_0

    .line 358
    :cond_1
    const/16 v0, 0x9

    .line 360
    goto :goto_0

    .line 362
    :pswitch_2
    if-ne v0, v3, :cond_2

    .line 363
    const/16 v0, 0x8

    goto :goto_0

    .line 365
    :cond_2
    const/16 v0, 0x9

    .line 367
    goto :goto_0

    .line 369
    :pswitch_3
    if-ne v0, v3, :cond_3

    .line 370
    const/16 v0, 0x8

    goto :goto_0

    .line 372
    :cond_3
    const/4 v0, 0x1

    .line 374
    goto :goto_0

    .line 379
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 346
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private lockStatusBar(Z)V
    .locals 4
    .param p1, "lock"    # Z

    .prologue
    .line 545
    const-string v2, "statusbar"

    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    .line 546
    .local v1, "mStatusBarManager":Landroid/app/StatusBarManager;
    if-nez v1, :cond_0

    .line 547
    const-string v2, "KnoxSetupWizardClient"

    const-string v3, "Cloud not get status bar manager"

    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    :goto_0
    return-void

    .line 549
    :cond_0
    const/4 v0, 0x0

    .line 550
    .local v0, "flags":I
    if-eqz p1, :cond_1

    .line 551
    const/high16 v2, 0x1010000

    or-int/2addr v0, v2

    .line 555
    :cond_1
    invoke-virtual {v1, v0}, Landroid/app/StatusBarManager;->disable(I)V

    goto :goto_0
.end method

.method private setBlanksVisibility()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    .line 330
    invoke-static {p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->getDeviceInch(Landroid/content/Context;)D

    move-result-wide v2

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 331
    const v2, 0x7f0f0085

    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 332
    .local v0, "leftBlank":Landroid/view/View;
    const v2, 0x7f0f0086

    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 333
    .local v1, "rightBlank":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 334
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 336
    :cond_0
    if-eqz v1, :cond_1

    .line 337
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 340
    .end local v0    # "leftBlank":Landroid/view/View;
    .end local v1    # "rightBlank":Landroid/view/View;
    :cond_1
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 397
    const v0, 0x4dc80

    if-ne p1, v0, :cond_1

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getContainerType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->ssa:Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->saveSamsungAccount()V

    .line 405
    :goto_0
    return-void

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->ssa:Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->showConfirmUnuseDialog()V

    goto :goto_0

    .line 403
    :cond_1
    const-string v0, "KnoxSetupWizardClient"

    const-string v1, "SaveSamsungAccount Error"

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 415
    const-string v1, "onBackPressed"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 416
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_next:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 418
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_next:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 420
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 421
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "CREATE_PERSONA_FAIL"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 422
    invoke-virtual {p0, v3, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->setResult(ILandroid/content/Intent;)V

    .line 423
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->finish()V

    .line 425
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 427
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x400

    .line 122
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onCreate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 124
    const v0, 0x7f03001c

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->setContentView(I)V

    .line 125
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 127
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mContext:Landroid/content/Context;

    .line 129
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->isPhone:Z

    .line 130
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->isPhone:Z

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 135
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 136
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 138
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->setBlanksVisibility()V

    .line 140
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->init(Landroid/os/Bundle;)V

    .line 142
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getContainerType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->ssa:Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;

    .line 144
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->ssa:Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->saveSamsungAccount()V

    .line 146
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 443
    const-string v0, "NameSettings onDestroy"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 444
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 446
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mIsDestroyed:Z

    .line 448
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 449
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2
    .param p1, "featureId"    # I
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 432
    const v0, 0x102002c

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 433
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->onBackPressed()V

    .line 434
    const/4 v0, 0x1

    .line 437
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onResume"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 410
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 411
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 389
    const-string v0, "onSaveInstanceState"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 390
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 391
    const-string v0, "KEY_CURRENT_NUMBER_OF_KNOX"

    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->currentNumOfKnox:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 392
    const-string v0, "KEY_KNOX_NAME"

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->et_knoxNameField:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    return-void
.end method
