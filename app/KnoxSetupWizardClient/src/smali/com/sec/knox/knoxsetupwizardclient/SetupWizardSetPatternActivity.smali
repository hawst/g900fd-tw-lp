.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;
.super Landroid/preference/PreferenceActivity;
.source "SetupWizardSetPatternActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$1;,
        Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;
    }
.end annotation


# instance fields
.field private isPhone:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 99
    return-void
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 69
    .local v0, "modIntent":Landroid/content/Intent;
    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const-string v1, ":android:no_headers"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 71
    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    const v1, 0x7f0c0086

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 79
    .local v0, "msg":Ljava/lang/CharSequence;
    invoke-virtual {p0, v0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;->showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 80
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x2000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f080000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;->isPhone:Z

    .line 83
    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;->isPhone:Z

    if-eqz v1, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 85
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 88
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 90
    :cond_1
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 96
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
