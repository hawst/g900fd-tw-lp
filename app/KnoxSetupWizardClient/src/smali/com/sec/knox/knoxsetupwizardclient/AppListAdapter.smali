.class public Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;
.super Landroid/widget/BaseAdapter;
.source "AppListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$1;,
        Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field appInstallationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field context:Landroid/app/Activity;

.field final grayscaleFilter:Landroid/graphics/ColorFilter;

.field isChecked:[Z

.field packageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field packageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;Landroid/content/pm/PackageManager;)V
    .locals 4
    .param p1, "context"    # Landroid/app/Activity;
    .param p3, "packageManager"    # Landroid/content/pm/PackageManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;",
            ">;",
            "Landroid/content/pm/PackageManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    .local p2, "packageList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->context:Landroid/app/Activity;

    .line 36
    iput-object p2, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->packageList:Ljava/util/List;

    .line 37
    iput-object p3, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->packageManager:Landroid/content/pm/PackageManager;

    .line 38
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Z

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->isChecked:[Z

    .line 39
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getContainerAppList()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->appInstallationList:Ljava/util/List;

    .line 41
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 42
    .local v0, "colorMatrix":Landroid/graphics/ColorMatrix;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 43
    invoke-virtual {v0}, Landroid/graphics/ColorMatrix;->getArray()[F

    move-result-object v1

    .line 44
    .local v1, "matrix":[F
    const/16 v2, 0x12

    const/high16 v3, 0x3f000000    # 0.5f

    aput v3, v1, v2

    .line 45
    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v2, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->grayscaleFilter:Landroid/graphics/ColorFilter;

    .line 46
    return-void
.end method


# virtual methods
.method public getChecked()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v1, "mList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->isChecked:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 74
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->isChecked:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    .line 75
    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->packageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_1
    return-object v1
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->packageList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->packageList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 64
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 83
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->context:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    .line 85
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f030017

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 86
    new-instance v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;

    const/4 v9, 0x0

    invoke-direct {v6, v9}, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;-><init>(Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$1;)V

    .line 88
    .local v6, "holder":Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;
    const v9, 0x7f0f007b

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mIcon:Landroid/widget/TextView;

    .line 89
    const v9, 0x7f0f007d

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mName:Landroid/widget/TextView;

    .line 90
    const v9, 0x7f0f007e

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mPackage:Landroid/widget/TextView;

    .line 91
    const v9, 0x7f0f007f

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/CheckBox;

    iput-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mCB:Landroid/widget/CheckBox;

    .line 93
    invoke-virtual/range {p0 .. p1}, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    .line 95
    .local v1, "app":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    iget-object v3, v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    check-cast v3, Ljava/lang/String;

    .line 96
    .local v3, "appName":Ljava/lang/String;
    iget-object v4, v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->packageName:Ljava/lang/String;

    .line 97
    .local v4, "appPackage":Ljava/lang/String;
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mName:Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v9, v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->masterEntry:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    if-eqz v9, :cond_0

    .line 100
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mPackage:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mPackage:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->context:Landroid/app/Activity;

    const v11, 0x7f0c00c3

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->masterEntry:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    iget-object v14, v14, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    :cond_0
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mCB:Landroid/widget/CheckBox;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 105
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mCB:Landroid/widget/CheckBox;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 107
    iget-object v2, v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->icon:Landroid/graphics/drawable/Drawable;

    .line 110
    .local v2, "appIcon":Landroid/graphics/drawable/Drawable;
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->appInstallationList:Ljava/util/List;

    invoke-interface {v9, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 111
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mCB:Landroid/widget/CheckBox;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 112
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mCB:Landroid/widget/CheckBox;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 113
    const/4 v5, 0x0

    .line 131
    .local v5, "curFilter":Landroid/graphics/ColorFilter;
    :goto_0
    invoke-virtual {v2, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 132
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x96

    const/16 v12, 0x96

    invoke-virtual {v2, v9, v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 133
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mIcon:Landroid/widget/TextView;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v9, v2, v10, v11, v12}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 134
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mIcon:Landroid/widget/TextView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 136
    return-object p2

    .line 115
    .end local v5    # "curFilter":Landroid/graphics/ColorFilter;
    :cond_1
    iget-object v9, v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->masterEntry:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    if-eqz v9, :cond_5

    .line 116
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mCB:Landroid/widget/CheckBox;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 118
    const/4 v8, 0x0

    .local v8, "location":I
    :goto_1
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->packageList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v8, v9, :cond_2

    .line 119
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->packageList:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    iget-object v9, v9, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 123
    :cond_2
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mCB:Landroid/widget/CheckBox;

    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->isChecked:[Z

    aget-boolean v10, v10, v8

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 124
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->isChecked:[Z

    aget-boolean v9, v9, v8

    if-eqz v9, :cond_4

    const/4 v5, 0x0

    .line 125
    .restart local v5    # "curFilter":Landroid/graphics/ColorFilter;
    :goto_2
    goto :goto_0

    .line 118
    .end local v5    # "curFilter":Landroid/graphics/ColorFilter;
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 124
    :cond_4
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->grayscaleFilter:Landroid/graphics/ColorFilter;

    goto :goto_2

    .line 126
    .end local v8    # "location":I
    :cond_5
    iget-object v9, v6, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter$ViewHolder;->mCB:Landroid/widget/CheckBox;

    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->isChecked:[Z

    aget-boolean v10, v10, p1

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 127
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->isChecked:[Z

    aget-boolean v9, v9, p1

    if-eqz v9, :cond_6

    const/4 v5, 0x0

    .restart local v5    # "curFilter":Landroid/graphics/ColorFilter;
    :goto_3
    goto :goto_0

    .end local v5    # "curFilter":Landroid/graphics/ColorFilter;
    :cond_6
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->grayscaleFilter:Landroid/graphics/ColorFilter;

    goto :goto_3
.end method

.method public setCheckBox(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 68
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->isChecked:[Z

    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->isChecked:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    aput-boolean v0, v1, p1

    .line 69
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
