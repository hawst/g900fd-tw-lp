.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;
.super Landroid/app/Activity;
.source "SetupWizardBackupPINActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$BackupPINPolicy;
    }
.end annotation


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private actionbar:Landroid/app/ActionBar;

.field private img_prev:Landroid/widget/ImageView;

.field private isPhone:Z

.field private mFirstPIN:Ljava/lang/String;

.field private mNextButton:Landroid/widget/LinearLayout;

.field private mPINField:Landroid/widget/EditText;

.field private mPINInformation:Landroid/widget/TextView;

.field private mPINInformationView:Landroid/widget/TextView;

.field private mPINState:I

.field private mStage:I

.field private text_prev:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 33
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->ACTIVITY_NAME:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mStage:I

    .line 57
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINState:I

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->actionbar:Landroid/app/ActionBar;

    .line 294
    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->updateUI()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->nextStage()V

    return-void
.end method

.method private hideKeyboard()V
    .locals 3

    .prologue
    .line 265
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    if-nez v1, :cond_0

    .line 266
    const v1, 0x7f0f0081

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    .line 268
    :cond_0
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 269
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 270
    return-void
.end method

.method private nextStage()V
    .locals 2

    .prologue
    .line 213
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mStage:I

    packed-switch v0, :pswitch_data_0

    .line 238
    :goto_0
    return-void

    .line 215
    :pswitch_0
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINState:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 216
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mStage:I

    .line 217
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mFirstPIN:Ljava/lang/String;

    .line 218
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 220
    :cond_0
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->updateUI()V

    goto :goto_0

    .line 223
    :pswitch_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mFirstPIN:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 224
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getPasswordType()I

    move-result v0

    if-nez v0, :cond_2

    .line 225
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mFirstPIN:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->saveBackupPINCode(Ljava/lang/String;)V

    .line 230
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->hideKeyboard()V

    .line 231
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->finish()V

    goto :goto_0

    .line 226
    :cond_2
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getPasswordType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 227
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mFirstPIN:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->saveBackupPINForPattern(Ljava/lang/String;)V

    goto :goto_1

    .line 233
    :cond_3
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINState:I

    .line 234
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->updateUI()V

    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private reset()V
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mStage:I

    .line 168
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->updateUI()V

    .line 169
    return-void
.end method

.method private saveBackupPINCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "PIN"    # Ljava/lang/String;

    .prologue
    .line 241
    invoke-static {p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setBackupPin(Ljava/lang/String;)V

    .line 242
    return-void
.end method

.method private saveBackupPINForPattern(Ljava/lang/String;)V
    .locals 0
    .param p1, "PIN"    # Ljava/lang/String;

    .prologue
    .line 245
    invoke-static {p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setBackupPinForPattern(Ljava/lang/String;)V

    .line 246
    return-void
.end method

.method private showPassword(Z)V
    .locals 3
    .param p1, "isShow"    # Z

    .prologue
    .line 253
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    .line 255
    .local v1, "position":I
    if-eqz p1, :cond_0

    .line 256
    invoke-static {}, Landroid/text/method/SingleLineTransformationMethod;->getInstance()Landroid/text/method/SingleLineTransformationMethod;

    move-result-object v0

    .line 260
    .local v0, "method":Landroid/text/method/TransformationMethod;
    :goto_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 261
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 262
    return-void

    .line 258
    .end local v0    # "method":Landroid/text/method/TransformationMethod;
    :cond_0
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v0

    .restart local v0    # "method":Landroid/text/method/TransformationMethod;
    goto :goto_0
.end method

.method private updateState()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$BackupPINPolicy;->checkPolicy(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINState:I

    .line 250
    return-void
.end method

.method private updateUI()V
    .locals 6

    .prologue
    const v5, 0x7f0c00ad

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 172
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mStage:I

    if-nez v0, :cond_0

    .line 173
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->updateState()V

    .line 175
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINState:I

    sparse-switch v0, :sswitch_data_0

    .line 210
    :goto_0
    :sswitch_0
    return-void

    .line 179
    :sswitch_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINInformationView:Landroid/widget/TextView;

    const v1, 0x7f0c00a9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 180
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0

    .line 183
    :sswitch_2
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINInformationView:Landroid/widget/TextView;

    const v1, 0x7f0c00aa

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 186
    :sswitch_3
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINInformationView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 189
    :sswitch_4
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINInformationView:Landroid/widget/TextView;

    const v1, 0x7f0c00f1

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    const/16 v3, 0x10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0

    .line 193
    :sswitch_5
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINInformationView:Landroid/widget/TextView;

    const v1, 0x7f0c00ab

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 194
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINInformationView:Landroid/widget/TextView;

    const v1, 0x7f0c00a1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 199
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINState:I

    if-ne v0, v3, :cond_1

    .line 200
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINInformationView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 201
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 203
    :cond_1
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->updateState()V

    .line 204
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 205
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto/16 :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto/16 :goto_0

    .line 175
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_4
        0x4 -> :sswitch_3
        0x64 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->setResult(I)V

    .line 303
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->finish()V

    .line 304
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 305
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x400

    const/4 v3, 0x4

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onCreate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 68
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    const v0, 0x7f030018

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->setContentView(I)V

    .line 71
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->isPhone:Z

    .line 74
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->isPhone:Z

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 81
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->actionbar:Landroid/app/ActionBar;

    .line 82
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->actionbar:Landroid/app/ActionBar;

    const v1, 0x7f0c00a9

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 84
    const v0, 0x7f0f0080

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINInformationView:Landroid/widget/TextView;

    .line 86
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getPasswordType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 87
    const v0, 0x7f0f0082

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINInformation:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINInformation:Landroid/widget/TextView;

    const v1, 0x7f0c00a8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 91
    :cond_1
    const v0, 0x7f0f0081

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    .line 92
    const v0, 0x7f0f006c

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mNextButton:Landroid/widget/LinearLayout;

    .line 94
    const v0, 0x7f0f0036

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->img_prev:Landroid/widget/ImageView;

    .line 95
    const v0, 0x7f0f0072

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->text_prev:Landroid/widget/TextView;

    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->img_prev:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->text_prev:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 101
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 129
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 143
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mNextButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$3;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->reset()V

    .line 154
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onResume"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 160
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 161
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->showPassword(Z)V

    .line 162
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->updateUI()V

    .line 163
    return-void
.end method
