.class public Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;
.super Ljava/lang/Thread;
.source "StubPackageThread.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPersonaID:I

.field private mRequestType:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mContext:Landroid/content/Context;

    .line 34
    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mRequestType:Ljava/lang/String;

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mPersonaID:I

    .line 38
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mContext:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mRequestType:Ljava/lang/String;

    .line 40
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "personaID"    # I

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mContext:Landroid/content/Context;

    .line 34
    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mRequestType:Ljava/lang/String;

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mPersonaID:I

    .line 43
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mContext:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mRequestType:Ljava/lang/String;

    .line 45
    iput p3, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mPersonaID:I

    .line 46
    return-void
.end method

.method private disablePackage(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 135
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x2

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v1, p2, v2, v3}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private processInternal()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    .line 53
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 54
    .local v5, "pm":Landroid/content/pm/PackageManager;
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mRequestType:Ljava/lang/String;

    if-eqz v6, :cond_0

    if-nez v5, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mRequestType:Ljava/lang/String;

    const-string v7, "android.intent.action.USER_ADDED"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 58
    :try_start_0
    const-string v6, "com.sec.knox.setupwizardstub"

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 59
    const-string v6, "android.intent.action.PACKAGE_CHANGED"

    const-string v7, "com.sec.knox.setupwizardstub"

    invoke-direct {p0, v6, v7}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->sendPackageNotificationIntent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 63
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mRequestType:Ljava/lang/String;

    const-string v7, "android.intent.action.USER_REMOVED"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 64
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mContext:Landroid/content/Context;

    const-string v7, "persona"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PersonaManager;

    .line 65
    .local v3, "persona":Landroid/os/PersonaManager;
    if-eqz v3, :cond_0

    .line 66
    invoke-virtual {v3, v8}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 67
    invoke-virtual {v3, v8}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v4

    .line 69
    .local v4, "personaInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_0

    .line 71
    :try_start_1
    const-string v6, "com.sec.knox.setupwizardstub"

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 72
    :catch_1
    move-exception v0

    .line 73
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 77
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v3    # "persona":Landroid/os/PersonaManager;
    .end local v4    # "personaInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    :cond_3
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mRequestType:Ljava/lang/String;

    const-string v7, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 78
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mContext:Landroid/content/Context;

    const-string v7, "persona"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PersonaManager;

    .line 79
    .restart local v3    # "persona":Landroid/os/PersonaManager;
    if-eqz v3, :cond_0

    .line 80
    invoke-virtual {v3, v8}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 81
    invoke-virtual {v3, v8}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v4

    .line 83
    .restart local v4    # "personaInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->isknoxPackageInstalled(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 85
    :try_start_2
    const-string v6, "com.sec.knox.setupwizardstub"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 86
    :catch_2
    move-exception v0

    .line 88
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    const-string v6, "com.sec.knox.setupwizardstub"

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 89
    const-string v6, "android.intent.action.PACKAGE_CHANGED"

    const-string v7, "com.sec.knox.setupwizardstub"

    invoke-direct {p0, v6, v7}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->sendPackageNotificationIntent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 90
    :catch_3
    move-exception v2

    .line 91
    .local v2, "ie":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    .line 96
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v2    # "ie":Ljava/lang/IllegalArgumentException;
    :cond_4
    :try_start_4
    const-string v6, "com.sec.knox.setupwizardstub"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_5

    .line 98
    :try_start_5
    const-string v6, "com.sec.knox.setupwizardstub"

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 99
    const-string v6, "android.intent.action.PACKAGE_CHANGED"

    const-string v7, "com.sec.knox.setupwizardstub"

    invoke-direct {p0, v6, v7}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->sendPackageNotificationIntent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    .line 100
    :catch_4
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_0

    .line 103
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_5
    move-exception v0

    .line 104
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 108
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v3    # "persona":Landroid/os/PersonaManager;
    .end local v4    # "personaInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    :cond_5
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mRequestType:Ljava/lang/String;

    const-string v7, "com.sec.knox.migrationagent.REMOVE_MIGRATION_AGENT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 109
    const/4 v1, 0x0

    .line 110
    .local v1, "iPackageManager":Landroid/content/pm/IPackageManager;
    const-string v6, "package"

    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_6

    :try_start_7
    iget v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mPersonaID:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_6

    .line 113
    const-string v6, "com.sec.knox.containeragent"

    const/4 v7, 0x0

    iget v8, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mPersonaID:I

    const/4 v9, 0x4

    invoke-interface {v1, v6, v7, v8, v9}, Landroid/content/pm/IPackageManager;->deletePackageAsUser(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;II)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_6

    .line 118
    :cond_6
    :goto_1
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->mContext:Landroid/content/Context;

    const-string v7, "com.sec.knox.containeragent"

    invoke-direct {p0, v6, v7}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->disablePackage(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 115
    :catch_6
    move-exception v0

    .line 116
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method private sendPackageNotificationIntent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "intent"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 130
    return-void
.end method


# virtual methods
.method public run()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->processInternal()V

    .line 50
    return-void
.end method
