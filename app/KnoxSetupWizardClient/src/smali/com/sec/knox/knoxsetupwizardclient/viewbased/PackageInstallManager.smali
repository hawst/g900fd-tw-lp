.class public Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;
.super Ljava/lang/Object;
.source "PackageInstallManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$AppLabelComparator;,
        Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    }
.end annotation


# instance fields
.field private approvedInstallerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private approvedPackageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private currentUserID:I

.field private excludePackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excludedPermissionsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private iPackageManager:Landroid/content/pm/IPackageManager;

.field private knoxAlreadyInstalledApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private knoxApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private knoxInstallableApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mdmPackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private packageManager:Landroid/content/pm/PackageManager;

.field private personaManager:Landroid/os/PersonaManager;

.field private personalApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private typeObj:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userID"    # I

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->mContext:Landroid/content/Context;

    .line 88
    iput p2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->currentUserID:I

    .line 90
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->packageManager:Landroid/content/pm/PackageManager;

    .line 91
    const-string v9, "package"

    invoke-static {v9}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->iPackageManager:Landroid/content/pm/IPackageManager;

    .line 92
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->mContext:Landroid/content/Context;

    const-string v10, "persona"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/PersonaManager;

    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->personaManager:Landroid/os/PersonaManager;

    .line 94
    invoke-static {p2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getConfigurationType(I)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->typeObj:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 96
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->excludePackages:Ljava/util/HashSet;

    .line 97
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->mdmPackages:Ljava/util/HashSet;

    .line 98
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->excludedPermissionsList:Ljava/util/List;

    .line 99
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->approvedInstallerList:Ljava/util/List;

    .line 100
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->approvedPackageList:Ljava/util/List;

    .line 101
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->readEnterpriseXML()Ljava/util/List;

    move-result-object v9

    invoke-direct {v2, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 103
    .local v2, "enterpriseDataXml":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->excludedPackages:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v3, v1, v5

    .line 104
    .local v3, "excludedApp":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->excludePackages:Ljava/util/HashSet;

    invoke-virtual {v9, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 107
    .end local v3    # "excludedApp":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->mdmPackages:[Ljava/lang/String;

    array-length v7, v1

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_1

    aget-object v8, v1, v5

    .line 108
    .local v8, "mdmApp":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->mdmPackages:Ljava/util/HashSet;

    invoke-virtual {v9, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 107
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 111
    .end local v8    # "mdmApp":Ljava/lang/String;
    :cond_1
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->excludedPermissions:[Ljava/lang/String;

    array-length v7, v1

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v7, :cond_2

    aget-object v4, v1, v5

    .line 112
    .local v4, "excludedPermission":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->excludedPermissionsList:Ljava/util/List;

    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 115
    .end local v4    # "excludedPermission":Ljava/lang/String;
    :cond_2
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->approvedInstallers:[Ljava/lang/String;

    array-length v7, v1

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v7, :cond_3

    aget-object v6, v1, v5

    .line 116
    .local v6, "installer":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->approvedInstallerList:Ljava/util/List;

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 119
    .end local v6    # "installer":Ljava/lang/String;
    :cond_3
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->typeObj:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->typeObj:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v9}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->isUserManaged()Z

    move-result v9

    if-nez v9, :cond_4

    .line 120
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/viewbased/KnoxSettingsConfig;->approvedPackages:[Ljava/lang/String;

    array-length v7, v1

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v7, :cond_4

    aget-object v0, v1, v5

    .line 121
    .local v0, "approveApp":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->approvedPackageList:Ljava/util/List;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 129
    .end local v0    # "approveApp":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private readEnterpriseXML()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->currentUserID:I

    invoke-static {v1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getConfigurationType(I)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v0

    .line 134
    .local v0, "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    if-nez v0, :cond_0

    .line 135
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 138
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getAppInstallationList()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public getPackagesToInstall()Ljava/util/List;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v21, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 144
    .local v21, "launcherIntent":Landroid/content/Intent;
    const-string v4, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxInstallableApps:Ljava/util/List;

    .line 147
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxAlreadyInstalledApps:Ljava/util/List;

    .line 148
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->currentUserID:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getKnoxContainerManager(Landroid/content/Context;I)Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v12

    .line 152
    .local v12, "appPolicy":Landroid/app/enterprise/ApplicationPolicy;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->iPackageManager:Landroid/content/pm/IPackageManager;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v21

    invoke-interface {v4, v0, v5, v6, v7}, Landroid/content/pm/IPackageManager;->queryIntentActivities(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->personalApps:Ljava/util/List;

    .line 153
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->iPackageManager:Landroid/content/pm/IPackageManager;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->currentUserID:I

    move-object/from16 v0, v21

    invoke-interface {v4, v0, v5, v6, v7}, Landroid/content/pm/IPackageManager;->queryIntentActivities(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxApps:Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->personalApps:Ljava/util/List;

    if-eqz v4, :cond_3

    .line 161
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->personalApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/content/pm/ResolveInfo;

    .line 162
    .local v27, "personalApp":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v27

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_0

    move-object/from16 v0, v27

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v4, :cond_0

    .line 163
    move-object/from16 v0, v27

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 165
    .local v24, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->excludePackages:Ljava/util/HashSet;

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->mdmPackages:Ljava/util/HashSet;

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 173
    move-object/from16 v0, v27

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v5, 0x40000

    and-int/2addr v4, v5

    if-nez v4, :cond_0

    .line 176
    move-object/from16 v0, v27

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v15, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    .line 178
    .local v15, "flags":I
    and-int/lit8 v4, v15, 0x1

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->approvedPackageList:Ljava/util/List;

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 180
    :cond_1
    if-eqz v12, :cond_2

    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Landroid/app/enterprise/ApplicationPolicy;->getApplicationStateEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 183
    :cond_2
    new-instance v18, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    invoke-direct/range {v18 .. v18}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;-><init>()V

    .line 184
    .local v18, "info":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    move-object/from16 v0, v27

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->packageName:Ljava/lang/String;

    .line 185
    move-object/from16 v0, v27

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v4, v5}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->appName:Ljava/lang/CharSequence;

    .line 186
    move-object/from16 v0, v27

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v4, v5}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->icon:Landroid/graphics/drawable/Drawable;

    .line 187
    move-object/from16 v0, v27

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    .line 189
    new-instance v22, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;)V

    .line 205
    .local v22, "mStatsObserver":Landroid/content/pm/IPackageStatsObserver$Stub;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->iPackageManager:Landroid/content/pm/IPackageManager;

    const/4 v5, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-interface {v4, v0, v5, v1}, Landroid/content/pm/IPackageManager;->getPackageSizeInfo(Ljava/lang/String;ILandroid/content/pm/IPackageStatsObserver;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 210
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxInstallableApps:Ljava/util/List;

    move-object/from16 v0, v18

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 155
    .end local v15    # "flags":I
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v18    # "info":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    .end local v22    # "mStatsObserver":Landroid/content/pm/IPackageStatsObserver$Stub;
    .end local v24    # "packageName":Ljava/lang/String;
    .end local v27    # "personalApp":Landroid/content/pm/ResolveInfo;
    :catch_0
    move-exception v14

    .line 156
    .local v14, "e":Landroid/os/RemoteException;
    invoke-virtual {v14}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 206
    .end local v14    # "e":Landroid/os/RemoteException;
    .restart local v15    # "flags":I
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v18    # "info":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    .restart local v22    # "mStatsObserver":Landroid/content/pm/IPackageStatsObserver$Stub;
    .restart local v24    # "packageName":Ljava/lang/String;
    .restart local v27    # "personalApp":Landroid/content/pm/ResolveInfo;
    :catch_1
    move-exception v14

    .line 207
    .restart local v14    # "e":Landroid/os/RemoteException;
    invoke-virtual {v14}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 215
    .end local v14    # "e":Landroid/os/RemoteException;
    .end local v15    # "flags":I
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v18    # "info":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    .end local v22    # "mStatsObserver":Landroid/content/pm/IPackageStatsObserver$Stub;
    .end local v24    # "packageName":Ljava/lang/String;
    .end local v27    # "personalApp":Landroid/content/pm/ResolveInfo;
    :cond_3
    const-string v4, "PackageInstallManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "knoxInstallableApps= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxInstallableApps:Ljava/util/List;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxInstallableApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    .line 219
    .local v20, "knoxInstallableApp":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/content/pm/ResolveInfo;

    .line 220
    .local v19, "knoxApp":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->packageName:Ljava/lang/String;

    move-object/from16 v26, v0

    .line 221
    .local v26, "packageName_personal":Ljava/lang/String;
    move-object/from16 v0, v20

    iget-object v10, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    check-cast v10, Ljava/lang/String;

    .line 222
    .local v10, "activityName_personal":Ljava/lang/String;
    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v25, v0

    .line 223
    .local v25, "packageName_knox":Ljava/lang/String;
    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 225
    .local v9, "activityName_knox":Ljava/lang/String;
    if-eqz v26, :cond_5

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    if-eqz v10, :cond_5

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 227
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxAlreadyInstalledApps:Ljava/util/List;

    move-object/from16 v0, v20

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 231
    .end local v9    # "activityName_knox":Ljava/lang/String;
    .end local v10    # "activityName_personal":Ljava/lang/String;
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v19    # "knoxApp":Landroid/content/pm/ResolveInfo;
    .end local v20    # "knoxInstallableApp":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    .end local v25    # "packageName_knox":Ljava/lang/String;
    .end local v26    # "packageName_personal":Ljava/lang/String;
    :cond_6
    const-string v4, "PackageInstallManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "knoxAlreadyInstalledApps= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxAlreadyInstalledApps:Ljava/util/List;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxAlreadyInstalledApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .restart local v16    # "i$":Ljava/util/Iterator;
    :cond_7
    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    .line 235
    .local v11, "app":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxInstallableApps:Ljava/util/List;

    invoke-interface {v4, v11}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 236
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxInstallableApps:Ljava/util/List;

    invoke-interface {v4, v11}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 240
    .end local v11    # "app":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxInstallableApps:Ljava/util/List;

    new-instance v5, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$AppLabelComparator;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$AppLabelComparator;-><init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;)V

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 243
    new-instance v23, Ljava/util/HashMap;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashMap;-><init>()V

    .line 244
    .local v23, "packageMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxInstallableApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    .line 245
    .restart local v18    # "info":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 246
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->masterEntry:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    goto :goto_5

    .line 248
    :cond_9
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 253
    .end local v18    # "info":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    :cond_a
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 254
    .local v13, "appsToInstall":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->knoxInstallableApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_6
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    .line 255
    .restart local v18    # "info":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->icon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->appName:Ljava/lang/CharSequence;

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->currentUserID:I

    invoke-direct/range {v3 .. v8}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 256
    .local v3, "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 259
    .end local v3    # "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v18    # "info":Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    :cond_b
    return-object v13
.end method
