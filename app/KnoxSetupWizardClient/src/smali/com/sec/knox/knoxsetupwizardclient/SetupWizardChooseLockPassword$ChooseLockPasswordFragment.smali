.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;
.super Landroid/app/Fragment;
.source "SetupWizardChooseLockPassword.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChooseLockPasswordFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;
    }
.end annotation


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private hasPwdPatternRestriction:Z

.field private mCancelButton:Landroid/widget/LinearLayout;

.field private mCheckSimplePassword:Z

.field private mFirstPin:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mHeaderText:Landroid/widget/TextView;

.field private mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private mIsAlphaMode:Z

.field private mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

.field private mKeyboardView:Landroid/inputmethodservice/KeyboardView;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mNextButton:Landroid/widget/LinearLayout;

.field private mNextTextView:Landroid/widget/TextView;

.field private mOldPassword:Ljava/lang/String;

.field private mPasswordEntry:Landroid/widget/TextView;

.field private mPasswordMaxLength:I

.field private mPasswordMinLength:I

.field private mPasswordMinLetters:I

.field private mPasswordMinLowerCase:I

.field private mPasswordMinNonLetter:I

.field private mPasswordMinNumeric:I

.field private mPasswordMinSymbols:I

.field private mPasswordMinUpperCase:I

.field private mRequestedQuality:I

.field private mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 235
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 144
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->ACTIVITY_NAME:Ljava/lang/String;

    .line 149
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    .line 150
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMaxLength:I

    .line 151
    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLetters:I

    .line 152
    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinUpperCase:I

    .line 153
    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLowerCase:I

    .line 154
    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinSymbols:I

    .line 155
    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNumeric:I

    .line 156
    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNonLetter:I

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mOldPassword:Ljava/lang/String;

    .line 159
    const/high16 v0, 0x20000

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mRequestedQuality:I

    .line 161
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    .line 175
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->hasPwdPatternRestriction:Z

    .line 177
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHandler:Landroid/os/Handler;

    .line 193
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mCheckSimplePassword:Z

    .line 237
    return-void
.end method

.method private checkRepeatingChars(Ljava/lang/String;)Z
    .locals 7
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 643
    const/4 v2, 0x0

    .line 645
    .local v2, "nLoop":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v3, :cond_2

    .line 646
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 647
    .local v0, "beforeChar":C
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 648
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v0, v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    .line 650
    :goto_1
    const/4 v5, 0x3

    if-lt v2, v5, :cond_1

    .line 651
    const-string v4, "ChooseLockPassword"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "has internal loop password : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    .end local v0    # "beforeChar":C
    .end local v1    # "i":I
    :goto_2
    return v3

    .line 649
    .restart local v0    # "beforeChar":C
    .restart local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 654
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 647
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "beforeChar":C
    .end local v1    # "i":I
    :cond_2
    move v3, v4

    .line 657
    goto :goto_2
.end method

.method private checkSequentialChars(Ljava/lang/String;)Z
    .locals 7
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 619
    const-string v5, "0123456789"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v1, v5, -0x4

    .line 620
    .local v1, "maxLoop":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-gt v0, v1, :cond_2

    .line 621
    const-string v5, "0123456789"

    add-int/lit8 v6, v0, 0x4

    invoke-virtual {v5, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 622
    .local v2, "pattern":Ljava/lang/String;
    const-string v5, "9876543210"

    add-int/lit8 v6, v0, 0x4

    invoke-virtual {v5, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 623
    .local v3, "reversePattern":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 624
    :cond_0
    const-string v5, "ChooseLockPassword"

    const-string v6, "has Sequential password"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    .end local v2    # "pattern":Ljava/lang/String;
    .end local v3    # "reversePattern":Ljava/lang/String;
    :goto_1
    return v4

    .line 620
    .restart local v2    # "pattern":Ljava/lang/String;
    .restart local v3    # "reversePattern":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 629
    .end local v2    # "pattern":Ljava/lang/String;
    .end local v3    # "reversePattern":Ljava/lang/String;
    :cond_2
    const-string v5, "abcdefghijklmnopqrstuvwxyz"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v1, v5, -0x4

    .line 630
    const/4 v0, 0x0

    :goto_2
    if-gt v0, v1, :cond_5

    .line 631
    const-string v5, "abcdefghijklmnopqrstuvwxyz"

    add-int/lit8 v6, v0, 0x4

    invoke-virtual {v5, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 632
    .restart local v2    # "pattern":Ljava/lang/String;
    const-string v5, "zyxwvutsrqponmlkjihgfedcba"

    add-int/lit8 v6, v0, 0x4

    invoke-virtual {v5, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 633
    .restart local v3    # "reversePattern":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 634
    :cond_3
    const-string v5, "ChooseLockPassword"

    const-string v6, "has Sequential password"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 630
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 639
    .end local v2    # "pattern":Ljava/lang/String;
    .end local v3    # "reversePattern":Ljava/lang/String;
    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private handleNext()V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 664
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 665
    .local v8, "pin":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 711
    :cond_0
    :goto_0
    return-void

    .line 668
    :cond_1
    const/4 v7, 0x0

    .line 669
    .local v7, "errorMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    sget-object v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    if-ne v1, v3, :cond_3

    .line 670
    const-string v1, "KnoxSetupWizardClient"

    const-string v3, "Stage Introduction: "

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    invoke-direct {p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->validatePassword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 672
    if-nez v7, :cond_2

    .line 673
    iput-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mFirstPin:Ljava/lang/String;

    .line 674
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 675
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->NeedToConfirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;)V

    .line 676
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->requestFocus()Z

    .line 704
    :cond_2
    :goto_1
    if-eqz v7, :cond_0

    .line 705
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    invoke-direct {p0, v7, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->showError(Ljava/lang/String;Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;)V

    .line 707
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Changing/creating screen lock password failed"

    move v1, v0

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 678
    :cond_3
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    sget-object v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->NeedToConfirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    if-ne v1, v3, :cond_2

    .line 679
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mFirstPin:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 680
    invoke-static {v8}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setPassword(Ljava/lang/String;)V

    .line 684
    invoke-direct {p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->checkSequentialChars(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->checkRepeatingChars(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 685
    :cond_4
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setSimplePassword(Z)V

    .line 691
    :goto_2
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 692
    .local v6, "data":Landroid/content/Intent;
    const-string v1, "knoxPin"

    invoke-virtual {v6, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 693
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v3, -0x1

    invoke-virtual {v1, v3, v6}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 695
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 687
    .end local v6    # "data":Landroid/content/Intent;
    :cond_5
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setSimplePassword(Z)V

    goto :goto_2

    .line 697
    :cond_6
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    .line 698
    .local v9, "tmp":Ljava/lang/CharSequence;
    if-eqz v9, :cond_7

    move-object v1, v9

    .line 699
    check-cast v1, Landroid/text/Spannable;

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-static {v1, v2, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 701
    :cond_7
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->ConfirmWrong:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;)V

    goto :goto_1
.end method

.method private setPasswordPolicy()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 825
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v0

    const/high16 v1, 0x60000

    if-ne v0, v1, :cond_3

    .line 826
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinSymbols()I

    move-result v0

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinSymbols:I

    .line 827
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinNumeric()I

    move-result v0

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNumeric:I

    .line 828
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinUpperCase()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinUpperCase:I

    .line 829
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinLowerCase()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLowerCase:I

    .line 830
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinNonLetter()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNonLetter:I

    .line 831
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinLetters()I

    move-result v0

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLetters:I

    .line 832
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinSymbols:I

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNumeric:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNonLetter:I

    if-le v0, v1, :cond_0

    .line 833
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinSymbols:I

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNumeric:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNonLetter:I

    .line 835
    :cond_0
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinUpperCase:I

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLowerCase:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLetters:I

    if-le v0, v1, :cond_1

    .line 836
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinUpperCase:I

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLowerCase:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLetters:I

    .line 846
    :cond_1
    :goto_0
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinimumLength()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    .line 847
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNonLetter:I

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLetters:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    if-le v0, v1, :cond_2

    .line 848
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNonLetter:I

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLetters:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    .line 851
    :cond_2
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMaximumLength()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMaxLength:I

    .line 852
    return-void

    .line 839
    :cond_3
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinSymbols:I

    .line 840
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNumeric:I

    .line 841
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinUpperCase:I

    .line 842
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLowerCase:I

    .line 843
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNonLetter:I

    .line 844
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLetters:I

    goto :goto_0
.end method

.method private showError(Ljava/lang/String;Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "next"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    .prologue
    const/4 v3, 0x1

    .line 729
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 730
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 731
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 732
    .local v0, "mesg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 733
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 734
    return-void
.end method

.method private updateUi()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 754
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 755
    .local v3, "password":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    .line 757
    .local v1, "length":I
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    sget-object v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    if-ne v4, v7, :cond_0

    .line 764
    :goto_0
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    sget-object v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    if-ne v4, v7, :cond_5

    if-lez v1, :cond_5

    .line 765
    iget-boolean v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->hasPwdPatternRestriction:Z

    if-eqz v4, :cond_1

    .line 766
    const-string v4, "KnoxSetupWizardClient"

    const-string v6, "CAC registration not locked enable next: "

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 768
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 801
    :goto_1
    return-void

    .line 760
    :cond_0
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mCancelButton:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 761
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mCancelButton:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    goto :goto_0

    .line 772
    :cond_1
    iget v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    if-ge v1, v4, :cond_3

    .line 773
    iget-boolean v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    if-eqz v4, :cond_2

    const v4, 0x7f0c00d5

    :goto_2
    new-array v5, v5, [Ljava/lang/Object;

    iget v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 775
    .local v2, "msg":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 776
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 800
    .end local v2    # "msg":Ljava/lang/String;
    :goto_3
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mNextTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    iget v5, v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->buttonText:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 773
    :cond_2
    const v4, 0x7f0c00d4

    goto :goto_2

    .line 778
    :cond_3
    invoke-direct {p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->validatePassword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 779
    .local v0, "error":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 780
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 781
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_3

    .line 784
    :cond_4
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    const v6, 0x7f0c00d3

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    .line 785
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_3

    .line 788
    .end local v0    # "error":Ljava/lang/String;
    :cond_5
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword;->mIsFromFinger:Z
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword;->access$000()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 790
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    sget-object v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->NeedToConfirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    if-ne v4, v5, :cond_6

    .line 791
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    const v5, 0x7f0c0084

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 793
    :cond_6
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    const v5, 0x7f0c00c9

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 797
    :cond_7
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    iget-boolean v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    iget v4, v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->alphaHint:I

    :goto_4
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(I)V

    .line 798
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mNextButton:Landroid/widget/LinearLayout;

    if-lez v1, :cond_9

    move v4, v5

    :goto_5
    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_3

    .line 797
    :cond_8
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    iget v4, v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->numericHint:I

    goto :goto_4

    :cond_9
    move v4, v6

    .line 798
    goto :goto_5
.end method

.method private validatePassword(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 462
    iget-boolean v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->hasPwdPatternRestriction:Z

    if-eqz v10, :cond_7

    .line 463
    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v10, p1}, Lcom/android/internal/widget/LockPatternUtils;->checkPasswordHistory(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 464
    const v10, 0x7f0c00f2

    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 613
    :goto_0
    return-object v10

    .line 466
    :cond_0
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->hasForbiddenNumericSequence(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 467
    const v10, 0x7f0c00e2

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumNumericSequenceLength()I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 469
    :cond_1
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->hasForbiddenCharacterSequence(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 470
    const v10, 0x7f0c00e1

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumCharacterSequenceLength()I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 472
    :cond_2
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mOldPassword:Ljava/lang/String;

    invoke-virtual {v10, p1, v11}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->hasForbiddenStringDistance(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 473
    const v10, 0x7f0c00e3

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMinimumCharacterChangeLength()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 475
    :cond_3
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->hasForbiddenData(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 476
    const v10, 0x7f0c00df

    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 478
    :cond_4
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->hasMaxRepeatedCharacters(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 479
    const v10, 0x7f0c00e0

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumCharacterOccurences()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 481
    :cond_5
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isPasswordPatternMatched(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 482
    const v10, 0x7f0c00de

    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 484
    :cond_6
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 488
    :cond_7
    iget-boolean v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mCheckSimplePassword:Z

    if-eqz v10, :cond_b

    .line 489
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->checkSequentialChars(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 490
    iget-boolean v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    if-eqz v10, :cond_8

    const v10, 0x7f0c00ed

    :goto_1
    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    :cond_8
    const v10, 0x7f0c00ec

    goto :goto_1

    .line 494
    :cond_9
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->checkRepeatingChars(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 495
    iget-boolean v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    if-eqz v10, :cond_a

    const v10, 0x7f0c00ea

    :goto_2
    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    :cond_a
    const v10, 0x7f0c00eb

    goto :goto_2

    .line 501
    :cond_b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    iget v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    if-ge v10, v11, :cond_d

    .line 502
    iget-boolean v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    if-eqz v10, :cond_c

    const v10, 0x7f0c00d5

    :goto_3
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    :cond_c
    const v10, 0x7f0c00d4

    goto :goto_3

    .line 506
    :cond_d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    iget v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMaxLength:I

    if-le v10, v11, :cond_f

    .line 507
    iget-boolean v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    if-eqz v10, :cond_e

    const v10, 0x7f0c00f0

    :goto_4
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMaxLength:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    :cond_e
    const v10, 0x7f0c00f1

    goto :goto_4

    .line 511
    :cond_f
    const/4 v4, 0x0

    .line 512
    .local v4, "letters":I
    const/4 v7, 0x0

    .line 513
    .local v7, "numbers":I
    const/4 v5, 0x0

    .line 514
    .local v5, "lowercase":I
    const/4 v8, 0x0

    .line 515
    .local v8, "symbols":I
    const/4 v9, 0x0

    .line 516
    .local v9, "uppercase":I
    const/4 v6, 0x0

    .line 517
    .local v6, "nonletter":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v3, v10, :cond_15

    .line 518
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 520
    .local v2, "c":C
    const/16 v10, 0x20

    if-lt v2, v10, :cond_10

    const/16 v10, 0x7f

    if-le v2, v10, :cond_11

    .line 521
    :cond_10
    const v10, 0x7f0c009c

    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 523
    :cond_11
    const/16 v10, 0x30

    if-lt v2, v10, :cond_12

    const/16 v10, 0x39

    if-gt v2, v10, :cond_12

    .line 524
    add-int/lit8 v7, v7, 0x1

    .line 525
    add-int/lit8 v6, v6, 0x1

    .line 517
    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 526
    :cond_12
    const/16 v10, 0x41

    if-lt v2, v10, :cond_13

    const/16 v10, 0x5a

    if-gt v2, v10, :cond_13

    .line 527
    add-int/lit8 v4, v4, 0x1

    .line 528
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 529
    :cond_13
    const/16 v10, 0x61

    if-lt v2, v10, :cond_14

    const/16 v10, 0x7a

    if-gt v2, v10, :cond_14

    .line 530
    add-int/lit8 v4, v4, 0x1

    .line 531
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 533
    :cond_14
    add-int/lit8 v8, v8, 0x1

    .line 534
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 538
    .end local v2    # "c":C
    :cond_15
    const/high16 v10, 0x20000

    iget v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mRequestedQuality:I

    if-ne v10, v11, :cond_17

    if-gtz v4, :cond_16

    if-lez v8, :cond_17

    .line 542
    :cond_16
    const v10, 0x7f0c00e9

    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 544
    :cond_17
    const/high16 v10, 0x60000

    iget v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mRequestedQuality:I

    if-ne v10, v11, :cond_1d

    .line 546
    iget v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLetters:I

    if-ge v4, v10, :cond_18

    .line 547
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0d0005

    iget v12, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLetters:I

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLetters:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 550
    :cond_18
    iget v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNumeric:I

    if-ge v7, v10, :cond_19

    .line 551
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0d0006

    iget v12, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNumeric:I

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNumeric:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 554
    :cond_19
    iget v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLowerCase:I

    if-ge v5, v10, :cond_1a

    .line 555
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0d0007

    iget v12, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLowerCase:I

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLowerCase:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 558
    :cond_1a
    iget v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinUpperCase:I

    if-ge v9, v10, :cond_1b

    .line 559
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0d000b

    iget v12, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinUpperCase:I

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinUpperCase:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 562
    :cond_1b
    iget v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinSymbols:I

    if-ge v8, v10, :cond_1c

    .line 563
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0d0008

    iget v12, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinSymbols:I

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinSymbols:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 566
    :cond_1c
    iget v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNonLetter:I

    if-ge v6, v10, :cond_22

    .line 567
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0d0009

    iget v12, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNonLetter:I

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinNonLetter:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 572
    :cond_1d
    const/high16 v10, 0x40000

    iget v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mRequestedQuality:I

    if-ne v10, v11, :cond_1f

    const/4 v0, 0x1

    .line 574
    .local v0, "alphabetic":Z
    :goto_7
    const/high16 v10, 0x50000

    iget v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mRequestedQuality:I

    if-ne v10, v11, :cond_20

    const/4 v1, 0x1

    .line 576
    .local v1, "alphanumeric":Z
    :goto_8
    if-nez v0, :cond_1e

    if-eqz v1, :cond_21

    :cond_1e
    if-nez v4, :cond_21

    .line 577
    const v10, 0x7f0c00e7

    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 572
    .end local v0    # "alphabetic":Z
    .end local v1    # "alphanumeric":Z
    :cond_1f
    const/4 v0, 0x0

    goto :goto_7

    .line 574
    .restart local v0    # "alphabetic":Z
    :cond_20
    const/4 v1, 0x0

    goto :goto_8

    .line 579
    .restart local v1    # "alphanumeric":Z
    :cond_21
    if-eqz v1, :cond_22

    if-nez v7, :cond_22

    .line 580
    const v10, 0x7f0c00e6

    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 583
    .end local v0    # "alphabetic":Z
    .end local v1    # "alphanumeric":Z
    :cond_22
    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v10, p1}, Lcom/android/internal/widget/LockPatternUtils;->checkPasswordHistory(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_24

    .line 584
    iget-boolean v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    if-eqz v10, :cond_23

    const v10, 0x7f0c00e5

    :goto_9
    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    :cond_23
    const v10, 0x7f0c00e4

    goto :goto_9

    .line 588
    :cond_24
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumNumericSequenceLength()I

    move-result v10

    if-nez v10, :cond_25

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getForbiddenStrings()Ljava/util/List;

    move-result-object v10

    if-nez v10, :cond_25

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumCharacterOccurences()I

    move-result v10

    if-nez v10, :cond_25

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumCharacterSequenceLength()I

    move-result v10

    if-nez v10, :cond_25

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMinimumCharacterChangeLength()I

    move-result v10

    if-eqz v10, :cond_2b

    .line 593
    :cond_25
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->hasForbiddenNumericSequence(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_26

    .line 594
    const v10, 0x7f0c00e2

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumNumericSequenceLength()I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 596
    :cond_26
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->hasForbiddenCharacterSequence(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_27

    .line 597
    const v10, 0x7f0c00e1

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumCharacterSequenceLength()I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 599
    :cond_27
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mOldPassword:Ljava/lang/String;

    invoke-virtual {v10, p1, v11}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->hasForbiddenStringDistance(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_28

    .line 600
    const v10, 0x7f0c00e3

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMinimumCharacterChangeLength()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 602
    :cond_28
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->hasForbiddenData(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_29

    .line 603
    const v10, 0x7f0c00df

    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 605
    :cond_29
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->hasMaxRepeatedCharacters(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2a

    .line 606
    const v10, 0x7f0c00e0

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumCharacterOccurences()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 608
    :cond_2a
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isPasswordPatternMatched(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2b

    .line 609
    const v10, 0x7f0c00de

    invoke-virtual {p0, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 613
    :cond_2b
    const/4 v10, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 809
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->ConfirmWrong:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    if-ne v0, v1, :cond_0

    .line 810
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->NeedToConfirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    .line 812
    :cond_0
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->updateUi()V

    .line 813
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 817
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 433
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 434
    packed-switch p1, :pswitch_data_0

    .line 442
    :cond_0
    :goto_0
    return-void

    .line 436
    :pswitch_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 437
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 438
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 434
    :pswitch_data_0
    .packed-switch 0x3a
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 714
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 726
    :goto_0
    return-void

    .line 716
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->handleNext()V

    goto :goto_0

    .line 721
    :pswitch_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 722
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 723
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 714
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f006b
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 395
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 396
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 397
    .local v1, "pin":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    check-cast v2, Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    .line 398
    .local v0, "cursorPos":I
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->onDestroyView()V

    .line 408
    const-string v2, ""

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 409
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    check-cast v2, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 413
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x1

    const/4 v4, 0x4

    .line 241
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : onCreate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 242
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 243
    new-instance v2, Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    .line 244
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 245
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword;

    if-nez v2, :cond_0

    .line 246
    new-instance v2, Ljava/lang/SecurityException;

    const-string v3, "Fragment contained in wrong activity"

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 254
    :cond_0
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 255
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.app.action.CHANGE_DEVICE_PASSWORD"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword;->isChangePwdRequired:Z

    .line 258
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mCheckSimplePassword:Z

    .line 259
    const-string v2, "ChooseLockPassword"

    const-string v3, "mCheckSimplePassword = false"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getSimplePasswordEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 261
    iput-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mCheckSimplePassword:Z

    .line 262
    const-string v2, "ChooseLockPassword"

    const-string v3, "mCheckSimplePassword = true"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_2
    const-string v2, "lockscreen.password_type"

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mRequestedQuality:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mRequestedQuality:I

    .line 269
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->setPasswordPolicy()V

    .line 272
    const-string v2, "lockscreen.password_old"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mOldPassword:Ljava/lang/String;

    .line 273
    const/4 v0, 0x4

    .line 274
    .local v0, "MIN_PWD_LENGTH":I
    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    if-lt v2, v4, :cond_7

    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMaxLength:I

    if-ge v2, v3, :cond_7

    .line 282
    :cond_3
    :goto_0
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getRequiredPwdPatternRestrictions()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 283
    iput-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->hasPwdPatternRestriction:Z

    .line 286
    :cond_4
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 288
    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    if-ge v2, v4, :cond_5

    .line 289
    iput v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    .line 291
    :cond_5
    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMaxLength:I

    if-le v2, v6, :cond_6

    .line 292
    iput v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMaxLength:I

    .line 295
    :cond_6
    return-void

    .line 276
    :cond_7
    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    if-ge v2, v4, :cond_8

    .line 277
    iput v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    goto :goto_0

    .line 278
    :cond_8
    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMaxLength:I

    if-le v2, v3, :cond_3

    .line 279
    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMaxLength:I

    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordMinLength:I

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 302
    const v7, 0x7f030019

    const/4 v8, 0x0

    invoke-virtual {p1, v7, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 306
    .local v6, "view":Landroid/view/View;
    const/4 v7, 0x0

    invoke-static {v7}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 307
    instance-of v7, p2, Landroid/preference/PreferenceFrameLayout;

    if-eqz v7, :cond_0

    .line 308
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    const/4 v8, 0x1

    iput-boolean v8, v7, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    .line 311
    :cond_0
    const v7, 0x7f0f006b

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mCancelButton:Landroid/widget/LinearLayout;

    .line 312
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mCancelButton:Landroid/widget/LinearLayout;

    invoke-virtual {v7, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 314
    sget-boolean v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword;->isChangePwdRequired:Z

    if-eqz v7, :cond_1

    .line 315
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mCancelButton:Landroid/widget/LinearLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 316
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mCancelButton:Landroid/widget/LinearLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 321
    :cond_1
    const v7, 0x7f0f006c

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mNextButton:Landroid/widget/LinearLayout;

    .line 322
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v7, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    const v7, 0x7f0f006d

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mNextTextView:Landroid/widget/TextView;

    .line 325
    const/high16 v7, 0x40000

    iget v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mRequestedQuality:I

    if-eq v7, v8, :cond_2

    const/high16 v7, 0x50000

    iget v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mRequestedQuality:I

    if-eq v7, v8, :cond_2

    const/high16 v7, 0x60000

    iget v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mRequestedQuality:I

    if-ne v7, v8, :cond_5

    :cond_2
    const/4 v7, 0x1

    :goto_0
    iput-boolean v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    .line 329
    const v7, 0x7f0f0084

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/internal/widget/PasswordEntryKeyboardView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    .line 330
    const v7, 0x7f0f0083

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    .line 331
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 332
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 333
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    const/high16 v8, 0x2000000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setImeOptions(I)V

    .line 335
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 336
    .local v0, "activity":Landroid/app/Activity;
    new-instance v7, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-direct {v7, v0, v8, v9}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;-><init>(Landroid/content/Context;Landroid/inputmethodservice/KeyboardView;Landroid/view/View;)V

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    .line 338
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    iget-boolean v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    if-eqz v7, :cond_6

    const/4 v7, 0x0

    :goto_1
    invoke-virtual {v8, v7}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->setKeyboardMode(I)V

    .line 342
    const v7, 0x7f0f006f

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    .line 343
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    invoke-virtual {v7}, Landroid/inputmethodservice/KeyboardView;->requestFocus()Z

    .line 345
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getInputType()I

    move-result v1

    .line 346
    .local v1, "currentType":I
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    iget-boolean v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    if-eqz v8, :cond_7

    .end local v1    # "currentType":I
    :goto_2
    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setInputType(I)V

    .line 349
    if-nez p3, :cond_8

    .line 350
    sget-object v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;)V

    .line 364
    :cond_3
    :goto_3
    instance-of v7, v0, Landroid/preference/PreferenceActivity;

    if-eqz v7, :cond_4

    move-object v3, v0

    .line 365
    check-cast v3, Landroid/preference/PreferenceActivity;

    .line 368
    .local v3, "preferenceActivity":Landroid/preference/PreferenceActivity;
    iget-boolean v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mIsAlphaMode:Z

    if-eqz v7, :cond_9

    const v2, 0x7f0c00c8

    .line 372
    .local v2, "id":I
    :goto_4
    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    .line 373
    .local v5, "title":Ljava/lang/CharSequence;
    invoke-virtual {v3, v5, v5}, Landroid/preference/PreferenceActivity;->showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 374
    const/4 v7, 0x0

    invoke-static {v7}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 375
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 377
    .end local v2    # "id":I
    .end local v3    # "preferenceActivity":Landroid/preference/PreferenceActivity;
    .end local v5    # "title":Ljava/lang/CharSequence;
    :cond_4
    return-object v6

    .line 325
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_5
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 338
    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_6
    const/4 v7, 0x1

    goto :goto_1

    .line 346
    .restart local v1    # "currentType":I
    :cond_7
    const/16 v1, 0x12

    goto :goto_2

    .line 356
    .end local v1    # "currentType":I
    :cond_8
    const-string v7, "first_pin"

    invoke-virtual {p3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mFirstPin:Ljava/lang/String;

    .line 357
    const-string v7, "ui_stage"

    invoke-virtual {p3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 358
    .local v4, "state":Ljava/lang/String;
    if-eqz v4, :cond_3

    .line 359
    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->valueOf(Ljava/lang/String;)Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    .line 360
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;)V

    goto :goto_3

    .line 368
    .end local v4    # "state":Ljava/lang/String;
    .restart local v3    # "preferenceActivity":Landroid/preference/PreferenceActivity;
    :cond_9
    const v2, 0x7f0c00ca

    goto :goto_4
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 738
    if-eqz p2, :cond_0

    const/4 v0, 0x6

    if-eq p2, v0, :cond_0

    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    .line 741
    :cond_0
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->handleNext()V

    .line 742
    const/4 v0, 0x1

    .line 744
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 416
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 417
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 418
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 420
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 421
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 382
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : onResume"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 383
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 385
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;)V

    .line 386
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 387
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mPasswordEntry:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 390
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    invoke-virtual {v1}, Landroid/inputmethodservice/KeyboardView;->requestFocus()Z

    .line 391
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 425
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 426
    const-string v0, "ui_stage"

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    const-string v0, "first_pin"

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mFirstPin:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 821
    return-void
.end method

.method protected updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;)V
    .locals 3
    .param p1, "stage"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    .prologue
    .line 445
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    .line 446
    .local v0, "previousStage":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment$Stage;

    .line 447
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->updateUi()V

    .line 451
    if-eq v0, p1, :cond_0

    .line 452
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword$ChooseLockPasswordFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 454
    :cond_0
    return-void
.end method
