.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;
.super Landroid/app/Activity;
.source "SetupWizardSetFingerPrintActivity.java"


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private btnNext:Landroid/widget/LinearLayout;

.field private fingerResult:Z

.field private isPhone:Z

.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

.field private textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 27
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->ACTIVITY_NAME:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->btnNext:Landroid/widget/LinearLayout;

    .line 31
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)Lcom/samsung/android/fingerprint/FingerprintManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->showSensorErrorDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method private showSensorErrorDialog()V
    .locals 4

    .prologue
    .line 161
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0c00bf

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0c00c0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$4;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$3;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 176
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 177
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/16 v5, 0x3f6

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 122
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 124
    const-string v2, "KnoxSetupWizardClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Result code : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Request code : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    .line 126
    if-ne p1, v5, :cond_0

    .line 127
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 128
    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->fingerResult:Z

    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mActionBarSwitch.setChecked(true) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3}, Landroid/widget/Switch;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 138
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 129
    goto :goto_0

    .line 131
    :cond_2
    if-nez p2, :cond_0

    .line 132
    if-ne p1, v5, :cond_0

    .line 133
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 134
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->fingerResult:Z

    .line 135
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mActionBarSwitch.setChecked(false) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3}, Landroid/widget/Switch;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 142
    const-string v0, "onBackPressed"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->btnNext:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->btnNext:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 147
    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->setResult(I)V

    .line 148
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->finish()V

    .line 150
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 152
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/16 v5, 0x400

    const/16 v6, 0x10

    const/4 v7, -0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 39
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " : onCreate"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/samsung/android/fingerprint/FingerprintManager;->getInstance(Landroid/content/Context;I)Lcom/samsung/android/fingerprint/FingerprintManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    .line 44
    const v1, 0x7f030010

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->setContentView(I)V

    .line 45
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v4, 0x2000

    invoke-virtual {v1, v4}, Landroid/view/Window;->addFlags(I)V

    .line 47
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v4, 0x7f080000

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->isPhone:Z

    .line 48
    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->isPhone:Z

    if-eqz v1, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v5, v5}, Landroid/view/Window;->setFlags(II)V

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09000d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const v4, 0x7f0c0037

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setTitle(I)V

    .line 56
    const v1, 0x7f0f006c

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->btnNext:Landroid/widget/LinearLayout;

    .line 58
    new-instance v1, Landroid/widget/Switch;

    new-instance v4, Landroid/view/ContextThemeWrapper;

    const v5, 0x1030128

    invoke-direct {v4, p0, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v1, v4}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    .line 59
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0a0013

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 60
    .local v0, "padding":I
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 61
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v5, Landroid/app/ActionBar$LayoutParams;

    const/16 v6, 0x15

    invoke-direct {v5, v7, v7, v6}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v4, v5}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 65
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 67
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mActionBarSwitch.setChecked(false) : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 69
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)V

    invoke-virtual {v1, v4}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 94
    const v1, 0x7f0f006a

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->textView:Landroid/widget/TextView;

    .line 95
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->textView:Landroid/widget/TextView;

    const v4, 0x7f0c0104

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 96
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->textView:Landroid/widget/TextView;

    const-string v4, "line.separator"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->textView:Landroid/widget/TextView;

    const-string v4, "line.separator"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->textView:Landroid/widget/TextView;

    const v4, 0x7f0c0105

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    const/16 v5, 0x14

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->btnNext:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    return-void

    :cond_1
    move v1, v3

    .line 68
    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 156
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 157
    const-string v0, "KnoxSetupWizardClient"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2
    .param p1, "featureId"    # I
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 182
    const v0, 0x102002c

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 183
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->onBackPressed()V

    .line 184
    const/4 v0, 0x1

    .line 186
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 114
    const-string v0, "KnoxSetupWizardClient"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 116
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->fingerResult:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mActionBarSwitch.setChecked : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 118
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
