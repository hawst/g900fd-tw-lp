.class public Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
.super Ljava/lang/Object;
.source "KnoxAppInfo.java"


# instance fields
.field public activityName:Ljava/lang/String;

.field public isContained:Z

.field public isDragedItem:Z

.field public isSelected:I

.field public mBadgeCount:I

.field public mDbId:J

.field public mIcon:Landroid/graphics/drawable/Drawable;

.field public mName:Ljava/lang/String;

.field public mPersonaId:I

.field public mPkgName:Ljava/lang/String;

.field public mUri:Ljava/lang/String;

.field public pView:Landroid/view/View;

.field public position:I

.field public removeableFlag:I

.field public tempName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V
    .locals 2
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "pkgName"    # Ljava/lang/String;
    .param p4, "personaId"    # I
    .param p5, "dbId"    # I
    .param p6, "uri"    # Ljava/lang/String;
    .param p7, "badgeCount"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->tempName:Ljava/lang/String;

    .line 46
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 47
    iput-object p2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mName:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    .line 49
    iput p4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPersonaId:I

    .line 50
    int-to-long v0, p5

    iput-wide v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mDbId:J

    .line 51
    iput-object p6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mUri:Ljava/lang/String;

    .line 52
    iput p7, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mBadgeCount:I

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "pkgName"    # Ljava/lang/String;
    .param p4, "activityName"    # Ljava/lang/String;
    .param p5, "personaId"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->tempName:Ljava/lang/String;

    .line 38
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 39
    iput-object p2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mName:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    .line 41
    iput-object p4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    .line 42
    iput p5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPersonaId:I

    .line 43
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 123
    instance-of v2, p1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    if-nez v2, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 126
    check-cast v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 127
    .local v0, "aInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mName:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 129
    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 132
    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getBadgeCount()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mBadgeCount:I

    return v0
.end method

.method public getDbId()J
    .locals 2

    .prologue
    .line 111
    iget-wide v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mDbId:J

    return-wide v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getIsSelected()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->isSelected:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPersonaId()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPersonaId:I

    return v0
.end method

.method public getPkgName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    return-object v0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 66
    return-void
.end method

.method public setIsSelected(I)V
    .locals 0
    .param p1, "v"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->isSelected:I

    .line 95
    return-void
.end method
