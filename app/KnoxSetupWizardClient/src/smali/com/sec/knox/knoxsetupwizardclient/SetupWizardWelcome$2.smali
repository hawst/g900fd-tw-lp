.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;
.super Landroid/os/CountDownTimer;
.source "SetupWizardWelcome.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 185
    const-string v0, "pd_timer\'s onFinish is called"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 187
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x103012b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0c002e

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 194
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isPdShown:Z
    invoke-static {v0, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$802(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;Z)Z

    .line 198
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I
    invoke-static {v0, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$602(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;I)I

    .line 199
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->finish()V

    .line 200
    return-void
.end method

.method public onTick(J)V
    .locals 1
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 180
    const-string v0, "pd_timer\'s onTick is called"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # operator++ for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$608(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)I

    .line 182
    return-void
.end method
